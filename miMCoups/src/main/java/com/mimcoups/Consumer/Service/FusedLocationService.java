package com.mimcoups.Consumer.Service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.google.android.gms.location.LocationClient;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;

public class FusedLocationService extends IntentService {

	private WebService_Call web_Service = new WebService_Call();
	private String TAG = this.getClass().getSimpleName();
	CommonData commonData;
	Context context;

	public FusedLocationService() {
		super("Fused Location");
		context = FusedLocationService.this;
	}

	public FusedLocationService(String name) {
		super("Fused Location");
		context = FusedLocationService.this;
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		commonData = (CommonData) getApplicationContext();
		Location location = intent
				.getParcelableExtra(LocationClient.KEY_LOCATION_CHANGED);
		if (location != null) {
			if (commonData.currentLatitude == 0.0
					&& commonData.currentLongitude == 0.0) {
				Log.e("caleld service", "1 IF");
				// Log.d("fused location service>>", "getting 0.0");
				// get_set_From_SharePrefrerence();
				if (commonData.currentLatitude == 0.0
						&& commonData.currentLongitude == 0.0) {
					commonData.currentLatitude = location.getLatitude();
					commonData.currentLongitude = location.getLongitude();
					// Log.e("caleld service", "2 IF");
					SharedPreferences preference = context
							.getSharedPreferences("UserDetail",
									Context.MODE_PRIVATE);
					preference.edit()
							.putString("latitude", "" + location.getLatitude())
							.commit();
					preference
							.edit()
							.putString("longitude",
									"" + location.getLongitude()).commit();
					CommonData globaldata = (CommonData) getApplicationContext();
					if ((globaldata.CustomerId != null)
							&& (globaldata.CustomerId.trim().length() != 0)) {

						// Log.e("caleld service", "3 IF");
						new LocationUpdate_Service().execute();
					}
				}
			} else {
				Log.e("caleld service", "4 else");
				// Log.d("fused location service>>", "else part");

				Location commonlat = new Location("");
				commonlat.setLatitude(commonData.currentLatitude);
				commonlat.setLongitude(commonData.currentLongitude);
				Location receivlat = new Location("");
				receivlat.setLatitude(location.getLatitude());
				receivlat.setLongitude(location.getLongitude());
				// Log.e("caleld service", "temp 1");

				// Log.d("lat lon", "" + receivlat.getLatitude() + " - "
				// + receivlat.getLongitude());

				float result = commonlat.distanceTo(receivlat);

				commonData.currentLatitude = location.getLatitude();
				commonData.currentLongitude = location.getLongitude();
				// / Log.e("caleld service", "temp 2");
				//
				// Log.d("fused location service",
				// "latitude>>" + location.getLatitude()
				// + ">>>>> Longitude" + location.getLongitude());

				SharedPreferences preference = context.getSharedPreferences(
						"UserDetail", Context.MODE_PRIVATE);
				preference.edit()
						.putString("latitude", "" + location.getLatitude())
						.commit();
				preference.edit()
						.putString("longitude", "" + location.getLongitude())
						.commit();

				// int res = (int) result;
				// Log.e("caleld service", "temp 3" + result);
				if (result > 100) {

					// Log.e("caleld service", "5 IF");

					new LocationUpdate_Service().execute();

					// Log.i(TAG, "Send Location" + location.getLatitude() + ","
					// + location.getLongitude());
				}
				// Save_in_SharePrefrerence();
			}

//			 NotificationManager notificationManager = (NotificationManager)
//			 getSystemService(NOTIFICATION_SERVICE);
//			 Builder noti = new NotificationCompat.Builder(this);
//			 noti.setContentTitle("Fused Location");
//			 noti.setContentText(location.getLatitude() + ","
//			 + location.getLongitude());
//			 noti.setSmallIcon(R.drawable.ic_launcher);
//			
//			 notificationManager.notify(1234, noti.build());

		} else {

			// Log.e("caleld service", "else");
			Toast.makeText(context, "Location null", Toast.LENGTH_SHORT).show();
		}

	}

	public class LocationUpdate_Service extends AsyncTask<Void, Void, Void> {
		String url_response = "";

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// your network operation
			SendLocation();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// Log.d("on post", "on post");

		}

		private void SendLocation() {
			// TODO Auto-generated method stub
			try {
				// Log.d("send location", "sed location");
				final CommonData globaldata = (CommonData) getApplicationContext();
				if (globaldata.checkInternetConnection()) {
					async_deel_feed_list as = new async_deel_feed_list(
							new asynctaskloaddata() {

								@Override
								public void onPreExecute() {
									// TODO Auto-generated method stub
									// Log.d("on pre", "on pre");
								}

								@Override
								public void doInBackground() {
									// TODO Auto-generated method stub
									String geturl = "CustomerService.svc/UpdateCurrentLocation?languageid="
											+ globaldata.languageId
											+ "&customerid="
											+ globaldata.CustomerId
											+ "&latitude="
											+ globaldata.currentLatitude
											+ "&longitude="
											+ globaldata.currentLongitude;
									Log.d("location>>>>", "" + geturl);
									url_response = web_Service
											.makeServicegetRequest(geturl);
									Log.d("response", "" + url_response);

									// NotificationManager notificationManager =
									// (NotificationManager)
									// getSystemService(NOTIFICATION_SERVICE);
									// NotificationCompat.Builder noti = new
									// NotificationCompat.Builder(
									// context);
									// noti.setContentTitle(url_response);
									// noti.setContentText(commonData.currentLatitude
									// + " - "
									// + commonData.currentLongitude);
									// noti.setSmallIcon(R.drawable.app_icon);
									// notificationManager.notify(1234,
									// noti.build());

								}

								@Override
								public void onPostExecute() {
									// TODO Auto-generated method stub
								}

							});
					as.execute();
				}

			} catch (Exception e) {
				Log.i("Exception", "SEND GPS DATA TO SERVER");

			}

		}

	}
	// public void Save_in_SharePrefrerence() {
	//
	// SharedPreferences mPref = context.getSharedPreferences("location",
	// Context.MODE_PRIVATE);
	//
	// SharedPreferences.Editor editor = mPref.edit();
	//
	// editor.putString("curlat", "" + commonData.currentLatitude);
	// editor.putString("cuelong", "" + commonData.currentLongitude);
	//
	// editor.commit();
	//
	// SharedPreferences preference = context.getSharedPreferences(
	// "UserDetail", Context.MODE_PRIVATE);
	// preference.edit()
	// .putString("latitude", "" + commonData.currentLatitude)
	// .commit();
	// preference.edit()
	// .putString("longitude", "" + commonData.currentLongitude)
	// .commit();
	//
	// }
	//
	// public void get_set_From_SharePrefrerence() {
	//
	// SharedPreferences mPref = context.getSharedPreferences("location",
	// Context.MODE_PRIVATE);
	// commonData.currentLatitude = Double.parseDouble(mPref.getString(
	// "curlat", "" + 0.0));
	// commonData.currentLongitude = Double.parseDouble(mPref.getString(
	// "cuelong", "" + 0.0));
	//
	// SharedPreferences preference = context.getSharedPreferences(
	// "UserDetail", Context.MODE_PRIVATE);
	//
	// commonData.currentLatitude = Double.valueOf(preference.getString(
	// "latitude", "0.0"));
	// commonData.currentLongitude = Double.valueOf(preference.getString(
	// "longitude", "0.0"));
	//
	// }
}
