package com.mimcoups.Consumer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.mimcoups.R;
import com.mimcoups.Consumer.commonClass.TouchImageView;
import com.squareup.picasso.Picasso;

public class DeelZoomActivity extends Activity {
	
	TouchImageView deelimage;
	RelativeLayout relprofileback;
	String imgurl="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.zoomactivity);
		
		deelimage=(TouchImageView)findViewById(R.id.deelimage);
		relprofileback=(RelativeLayout)findViewById(R.id.relprofileback);
		
		if(getIntent().hasExtra("deelimage")){
			imgurl=getIntent().getExtras().getString("deelimage");
			
			Picasso.with(getApplicationContext()).load(imgurl).placeholder(R.drawable.noimage_deal)
			.into(deelimage);
			
			
		}
		
		
		
		relprofileback.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				onBackPressed();
			}
		});
		
	}

}
