package com.mimcoups.Consumer;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.mimcoups.R;

public class FacebookShare extends Activity {

	private static final List<String> PERMISSIONS = Arrays
			.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;

	private UiLifecycleHelper uiHelper;

	boolean isShow = false;
	String str_newsimg, str_dealname, str_dealdesc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_facebook);

		str_newsimg = getIntent().getExtras().getString("Image");
		str_dealname = getIntent().getExtras().getString("dealname");
		str_dealdesc = getIntent().getExtras().getString("desc");

		// Add code to print out the key hash
		// try {
		// PackageInfo info = getPackageManager().getPackageInfo(
		// "com.zealousweb.facebookintegration",
		// PackageManager.GET_SIGNATURES);
		// for (Signature signature : info.signatures) {
		// MessageDigest md = MessageDigest.getInstance("SHA");
		// md.update(signature.toByteArray());
		// Log.d("KeyHash:",
		// Base64.encodeToString(md.digest(), Base64.DEFAULT));
		// }
		// } catch (NameNotFoundException e) {
		//
		// } catch (NoSuchAlgorithmException e) {
		//
		// }

		if (savedInstanceState != null) {
			pendingPublishReauthorization = savedInstanceState.getBoolean(
					PENDING_PUBLISH_KEY, false);
		}

		boolean installed = appInstalledOrNot("com.facebook.katana");
		if (installed) {

			callFacebookLogout(getApplicationContext());
			Session.openActiveSession(this, true, new Session.StatusCallback() {

				// callback when session changes state
				@Override
				public void call(Session session, final SessionState state,
						Exception exception) {

					if (session.isOpened()) {
						// make request to the /me API
						Request.newMeRequest(session,
								new Request.GraphUserCallback() {

									// callback after Graph API response with
									// user
									// object
									@Override
									public void onCompleted(GraphUser user,
											Response response) {
										if (user != null) {
											publishFeedDialog();
										}
									}
								}).executeAsync();

					}

				}
			});
		} else {

			Session.openActiveSession(this, true, new Session.StatusCallback() {

				// callback when session changes state
				@Override
				public void call(Session session, final SessionState state,
						Exception exception) {

					if (session.isOpened()) {
						// make request to the /me API
						Request.newMeRequest(session,
								new Request.GraphUserCallback() {

									// callback after Graph API response with
									// user
									// object
									@Override
									public void onCompleted(GraphUser user,
											Response response) {
										if (user != null) {
											publishFeedDialog();
										}
									}
								}).executeAsync();

					}

				}
			});
		}

		// try{
		// ApplicationInfo info = getPackageManager().
		// getApplicationInfo("com.facebook.katana", 0 );
		// Session.openActiveSession(this, true, new Session.StatusCallback() {
		//
		// // callback when session changes state
		// @Override
		// public void call(Session session, final SessionState state,
		// Exception exception) {
		//
		// if (session.isOpened()) {
		// // make request to the /me API
		// Request.newMeRequest(session,
		// new Request.GraphUserCallback() {
		//
		// // callback after Graph API response with user
		// // object
		// @Override
		// public void onCompleted(GraphUser user,
		// Response response) {
		// if (user != null) {
		// Log.i("we are in>>", "in");
		//
		// publishFeedDialog();
		// }
		// }
		// }).executeAsync();
		//
		// }
		//
		// }
		// });
		// } catch( PackageManager.NameNotFoundException e ){
		// // return false;
		// }

		// The user doesn't have the Facebook Messenger app for Android app
		// installed.
		// Session.openActiveSession(this, true, new Session.StatusCallback() {
		//
		// // callback when session changes state
		// @Override
		// public void call(Session session, final SessionState state,
		// Exception exception) {
		//
		// if (session.isOpened()) {
		// // make request to the /me API
		// Request.newMeRequest(session,
		// new Request.GraphUserCallback() {
		//
		// // callback after Graph API response with user
		// // object
		// @Override
		// public void onCompleted(GraphUser user,
		// Response response) {
		// if (user != null) {
		// Log.i("we are in>>", "in");
		//
		// publishFeedDialog();
		// }
		// }
		// }).executeAsync();
		//
		// }
		//
		// }
		// });

	}

	// start Facebook Login
	// Session.openActiveSession(this, true, new Session.StatusCallback() {
	//
	// // callback when session changes state
	// @Override
	// public void call(Session session, final SessionState state,
	// Exception exception) {
	//
	// if (session.isOpened()) {
	// // make request to the /me API
	// Request.newMeRequest(session,
	// new Request.GraphUserCallback() {
	//
	// // callback after Graph API response with user
	// // object
	// @Override
	// public void onCompleted(GraphUser user,
	// Response response) {
	// if (user != null) {
	// // TextView welcome = (TextView)
	// // findViewById(R.id.txtfb);
	// // welcome.setText("Hello "
	// // + user.getName() + "!");
	// Log.i("we are in>>", "in");
	//
	// publishFeedDialog();
	// //
	// // if (pendingPublishReauthorization
	// // &&
	// // state.equals(SessionState.OPENED_TOKEN_UPDATED))
	// // {
	// // pendingPublishReauthorization =
	// // false;
	// // publishStory();
	// // }
	// }
	// }
	// }).executeAsync();
	//
	// }
	//
	// }
	// });

	private boolean appInstalledOrNot(String uri) {
		PackageManager pm = getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	private void publishFeedDialog() {
		Bundle params = new Bundle();
		params.putString("name", getResources().getString(R.string.app_name));
		params.putString("caption", str_dealname);
		params.putString("description", str_dealdesc);
		params.putString("link", str_newsimg);
//		params.putString("link","http://iswwwup.com/t/f950914f65e3/facebook-share-dialog-always-shows-captcha-only-on-android.html");
		
		
		
		// params.putString("picture", str_newsimg);

		WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(this,
				Session.getActiveSession(), params)).setOnCompleteListener(
				new OnCompleteListener() {

					@Override
					public void onComplete(Bundle values,
							FacebookException error) {
						if (error == null) {
							// When the story is posted, echo the success
							// and the post Id.
							final String postId = values.getString("post_id");
							if (postId != null) {
								Toast.makeText(
										FacebookShare.this,
										getResources().getString(
												R.string.facebookmsg),
										Toast.LENGTH_SHORT).show();
								onBackPressed();
							} else {
								// User clicked the Cancel button
								onBackPressed();
								// finish();
								// Toast.makeText(FacebookActivity.this,
								// "Publish cancelled", Toast.LENGTH_SHORT)
								// .show();
							}
						} else if (error instanceof FacebookOperationCanceledException) {
							// User clicked the "x" button
							Toast.makeText(getApplicationContext(),
									"Publish cancelled", Toast.LENGTH_SHORT)
									.show();
							onBackPressed();
						} else {
							// Generic, ex: network error
							Toast.makeText(getApplicationContext(),
									"Error posting story", Toast.LENGTH_SHORT)
									.show();
						}
					}

				}).build();
		feedDialog.show();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);

		if (Session.getActiveSession().isOpened()) {

		} else {
			onBackPressed();
		}

		// if(!isReach){
		// onBackPressed();
		// }

		// onBackPressed();
	}

	private boolean isSubsetOf(Collection<String> subset,
			Collection<String> superset) {
		for (String string : subset) {
			if (!superset.contains(string)) {
				return false;
			}
		}
		return true;
	}

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			Log.i("session state", "OPEN");
		} else if (state.isClosed()) {
			Log.i("session close", "CLOSE");
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent ii = new Intent();
		setResult(RESULT_OK, ii);
		super.onBackPressed();
	}

	public static void callFacebookLogout(Context context) {
		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

		}

	}

}
