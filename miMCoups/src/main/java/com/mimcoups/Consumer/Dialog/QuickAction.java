package com.mimcoups.Consumer.Dialog;

import android.content.Context;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.DealCategoryPopupAdapter;

public class QuickAction extends PopupWindows {
	private ImageView mArrowUp;
	private ImageView mArrowDown;
	private Animation mTrackAnim;
	private LayoutInflater inflater;
	private OnActionItemClickListener mListener;

	private int mChildPos;
	public static final int ANIM_GROW_FROM_LEFT = 1;
	public static final int ANIM_GROW_FROM_RIGHT = 2;
	public static final int ANIM_GROW_FROM_CENTER = 3;
	public static final int ANIM_AUTO = 4;
	private Context context;

	public QuickAction(Context context) {

		super(context);
		this.context = context;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mTrackAnim = AnimationUtils.loadAnimation(context, R.anim.rail);

		mTrackAnim.setInterpolator(new Interpolator() {
			public float getInterpolation(float t) {
				final float inner = (t * 1.55f) - 1.1f;

				return 1.2f - inner * inner;
			}
		});
		mChildPos = 0;
	}

	public void setRootViewId(int id) {
		mRootView = (ViewGroup) inflater.inflate(id, null);
		mArrowDown = (ImageView) mRootView.findViewById(R.id.arrow_down);
		mArrowUp = (ImageView) mRootView.findViewById(R.id.arrow_up);
		setContentView(mRootView);
	}

	public void animateTrack(boolean animateTrack) {

	}

	public void setAnimStyle(int animStyle) {
	}

	public void setOnActionItemClickListener(OnActionItemClickListener listener) {
		mListener = listener;
	}

	public void show(View anchor, final int id) {
		if (id == 1) {
			setRootViewId(R.layout.popupfriend);
			RelativeLayout relPurchase = (RelativeLayout) mRootView
					.findViewById(R.id.relPurchase);
			relPurchase.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 0, context.getResources()
							.getString(R.string.popupfriendpurchased));
					dismiss();
				}
			});

			RelativeLayout relFavourite = (RelativeLayout) mRootView
					.findViewById(R.id.relFavourite);

			relFavourite.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 1, context.getResources()
							.getString(R.string.popupfriendfavorites));
					dismiss();
				}
			});

			RelativeLayout relRationReview = (RelativeLayout) mRootView
					.findViewById(R.id.relRationReview);
			relRationReview.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 2, context.getResources()
							.getString(R.string.popupfriendratingandreviews));
					dismiss();
				}
			});
		} else if (id == 2) {
			setRootViewId(R.layout.popupdate);
			RelativeLayout relpoupToday = (RelativeLayout) mRootView
					.findViewById(R.id.relpoupToday);
			relpoupToday.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 0, context.getResources()
							.getString(R.string.popupdatetoday));
					dismiss();
				}
			});
			RelativeLayout relPoupUp7day = (RelativeLayout) mRootView
					.findViewById(R.id.relPoupUp7day);
			relPoupUp7day.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 7, context.getResources()
							.getString(R.string.popupdatelast7days));
					dismiss();
				}
			});
			RelativeLayout relpoup30day = (RelativeLayout) mRootView
					.findViewById(R.id.relpoup30day);
			relpoup30day.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 30, context.getResources()
							.getString(R.string.popupdatelast30days));
					dismiss();
				}
			});
			RelativeLayout relpuoupClose = (RelativeLayout) mRootView
					.findViewById(R.id.relpuoupClose);
			relpuoupClose.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, -1, context.getResources()
							.getString(R.string.popupdateabouttoclose));
					dismiss();
				}
			});
		} else if (id == 3) {
			setRootViewId(R.layout.popupcategory);

			ListView lst_popupcategory = (ListView) mRootView
					.findViewById(R.id.lst_popupcategory);
			DealCategoryPopupAdapter adapter = new DealCategoryPopupAdapter(
					mContext, ((DeelFeedActivity) context).arrCategory);
			lst_popupcategory.setAdapter(adapter);
			lst_popupcategory.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					mListener.onItemClick(
							id,
							Integer.parseInt(((DeelFeedActivity) context).arrCategory
									.get(arg2).categoryId),
							((DeelFeedActivity) context).arrCategory.get(arg2).categoryName);
					dismiss();
				}
			});
		} else if (id == 4) {
			setRootViewId(R.layout.popupratings);
			RelativeLayout relPoupBestreviews = (RelativeLayout) mRootView
					.findViewById(R.id.relPoupBestreviews);
			relPoupBestreviews.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 0, context.getResources()
							.getString(R.string.popupratingreviews));
					dismiss();
				}
			});

			RelativeLayout relPoupBestdeals = (RelativeLayout) mRootView
					.findViewById(R.id.relPoupBestdeals);
			relPoupBestdeals.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 1, context.getResources()
							.getString(R.string.popupratingdeal));
					dismiss();
				}
			});
			RelativeLayout relPoupBestProduct = (RelativeLayout) mRootView
					.findViewById(R.id.relPoupBestProduct);
			relPoupBestProduct.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 2, context.getResources()
							.getString(R.string.popupratingproduct));
					dismiss();
				}
			});
		} else if (id == 5) {
			setRootViewId(R.layout.popuplocation);
			RelativeLayout relPopupNearest = (RelativeLayout) mRootView
					.findViewById(R.id.relPopupNearest);
			relPopupNearest.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 0, context.getResources()
							.getString(R.string.popuplocationnearesttome));
					dismiss();
				}
			});

			RelativeLayout relPoupRadious = (RelativeLayout) mRootView
					.findViewById(R.id.relPoupRadious);
			relPoupRadious.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 1, context.getResources()
							.getString(R.string.popuplocationbdwmr));
					dismiss();
				}
			});
		} else if (id == 6) {
			setRootViewId(R.layout.popupprice);
			RelativeLayout relPoupupHigesttolowest = (RelativeLayout) mRootView
					.findViewById(R.id.relPoupupHigesttolowest);
			relPoupupHigesttolowest.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 0, context.getResources()
							.getString(R.string.popuppricehtol));
					dismiss();
				}
			});

			RelativeLayout relPoupLowesttohigest = (RelativeLayout) mRootView
					.findViewById(R.id.relPoupLowesttohigest);
			relPoupLowesttohigest.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 1, context.getResources()
							.getString(R.string.popuppriceltoh));
					dismiss();
				}
			});
			RelativeLayout relPoupHigestDiscount = (RelativeLayout) mRootView
					.findViewById(R.id.relPoupHigestDiscount);
			relPoupHigestDiscount.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					mListener.onItemClick(id, 2, context.getResources()
							.getString(R.string.popuppricehighestperdiscount));
					dismiss();
				}
			});
		}
		preShow();
		int[] location = new int[2];

		anchor.getLocationOnScreen(location);

		Rect anchorRect = new Rect(location[0], location[1], location[0]
				+ anchor.getWidth(), location[1] + anchor.getHeight());

		mRootView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		mRootView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		int rootWidth = mRootView.getMeasuredWidth();
		int rootHeight = mRootView.getMeasuredHeight();

		int screenWidth = mWindowManager.getDefaultDisplay().getWidth();

		int xPos = (screenWidth - rootWidth) / 2;
		int yPos = anchorRect.top - rootHeight;

		boolean onTop = true;
		if (rootHeight > anchor.getTop()) {
			yPos = anchorRect.bottom;
			onTop = false;
		}

		showArrow(((onTop) ? R.id.arrow_down : R.id.arrow_up),
				anchorRect.centerX());

		// setAnimationStyle(screenWidth, anchorRect.centerX(), onTop);

		mWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, xPos, yPos);

		// if (animateTrack) mTrack.startAnimation(mTrackAnim);
	}

	private void showArrow(int whichArrow, int requestedX) {
		final View showArrow = (whichArrow == R.id.arrow_up) ? mArrowUp
				: mArrowDown;
		final View hideArrow = (whichArrow == R.id.arrow_up) ? mArrowDown
				: mArrowUp;

		final int arrowWidth = mArrowUp.getMeasuredWidth();

		showArrow.setVisibility(View.VISIBLE);

		ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) showArrow
				.getLayoutParams();

		param.leftMargin = requestedX - arrowWidth / 2;

		hideArrow.setVisibility(View.INVISIBLE);
	}

	public interface OnActionItemClickListener {
		public abstract void onItemClick(int id, int pos, String title);
	}
}