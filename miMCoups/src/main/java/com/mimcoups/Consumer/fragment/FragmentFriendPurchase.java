package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.FriendPurchaseDealFeedAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;

public class FragmentFriendPurchase extends Fragment {

	private int preLast;
	private topBarInteface topInterface;
	private ListView lstFriendPurchase;
	private FriendPurchaseDealFeedAdapter adapterFriendPurchase;
	private ArrayList<DealFeedDetail> arrDealFeeds = new ArrayList<DealFeedDetail>();
	private ImageView imgFriendPurchaseQuestions, imgFriendPurchaseSearch;
	private LinearLayout txtFrienPurchaseNoData, lnLoadMore;
	private SwipeRefreshLayout swipeLayout;
	private String strStatusCode = "", strMessageResponse, urlResponse = "",
			StrPageIndex = "1", strLastRunDate = "";
	String strGeturl = "";
	private CommonData commonClass;
	private WebService_Call web_service = new WebService_Call();
	private ArrayList<Integer> arrselectedCategory = new ArrayList<Integer>();
	private boolean isPullToRefresh = false;
	private boolean loadmore = false;
	RelativeLayout relmainfragment_friend_purchase;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	View views;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_friend_purchase,
				container, false);

		initControls(view);

		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);

		} else {

			StrPageIndex = "1";
			// strGeturl = "DealService.svc/GetAllDealsByFriend?customerid="
			// + commonClass.CustomerId + "&languageid="
			// + commonClass.languageId + "&latitude="
			// + commonClass.currentLatitude + "&longitude="
			// + commonClass.currentLongitude
			// + "&mode=0&id=7&type=1&startindex=1";

			strGeturl = "FriendService.svc/GetAllFriendPurchase?customerid="
					+ commonClass.CustomerId + "&languageid="
					+ commonClass.languageId + "&latitude="
					+ commonClass.currentLatitude + "&longitude="
					+ commonClass.currentLongitude;

			Log.e("called purchase", strGeturl);

			views = inflater.inflate(R.layout.loadmore, null);

			isPullToRefresh = true;

			lstFriendPurchase.addFooterView(views);
			loadDeals();
		}

		Loadmore_functionality();
		AllClickevents();
		return view;
	}

	private void AllClickevents() {

		relmainfragment_friend_purchase
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});

	}

	private void Loadmore_functionality() {

		lstFriendPurchase.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

				final int lastItem = firstVisibleItem + visibleItemCount;

				if (lastItem == totalItemCount) {

					if ((preLast != lastItem) && loadmore) {

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							// lnLoadMore.setVisibility(View.VISIBLE);

							loadmore = false;

							loadmore_moredeals();
						}
					}
				}
			}
		});

	}

	@SuppressLint("NewApi")
	private void initControls(View view) {
		commonClass = (CommonData) getActivity().getApplicationContext();

		relmainfragment_friend_purchase = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_friend_purchase);

		txtFrienPurchaseNoData = (LinearLayout) view
				.findViewById(R.id.txtFrienPurchaseNoData);

		imgFriendPurchaseQuestions = (ImageView) view
				.findViewById(R.id.imgFriendPurchaseQuestions);
		imgFriendPurchaseSearch = (ImageView) view
				.findViewById(R.id.imgFriendPurchaseSearch);
		lstFriendPurchase = (ListView) view
				.findViewById(R.id.lstFriendPurchase);

		swipeLayout = (SwipeRefreshLayout) view
				.findViewById(R.id.swipe_container);
		lnLoadMore = (LinearLayout) view.findViewById(R.id.lnLoadMore);

		imgFriendPurchaseSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("search");
			}
		});

		imgFriendPurchaseQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					topInterface.topbarClick("questions");
				}
				// topInterface.topbarClick("questions");
			}
		});

		swipeLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				swipeLayout.setRefreshing(true);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (isPullToRefresh) {
							swipeLayout.setRefreshing(false);
							strGeturl = "DealService.svc/GetAllDeals?customerid="
									+ commonClass.CustomerId
									+ "&languageid="
									+ commonClass.languageId
									+ "&latitude="
									+ commonClass.currentLatitude
									+ "&longitude="
									+ commonClass.currentLongitude;
							strGeturl = commonClass
									.Url_EncodeWithBlank(strGeturl);
							pullToRefreshDeals();
						}
					}
				}, 500);
			}
		});
		swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void loadDeals() {
		String str = strGeturl + "&startindex=" + StrPageIndex
				+ "&dLastRunDate=" + strLastRunDate;

		final String strTempUrl = commonClass.Url_EncodeWithBlank(str);
		Log.e("FriendPurchase", strTempUrl);
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {
						if (!DeelFeedActivity.progressbarshowing) {
							topInterface.showDialog("Loading");
						}

					}

					@Override
					public void doInBackground() {

						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);
						try {

							JSONObject json_Object = new JSONObject(urlResponse);
							JSONArray json_result = json_Object
									.getJSONArray("FriendList");
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");
							// Log.e("FriendPurchase", urlResponse);

							if (strStatusCode.equals("100")) {

								StrPageIndex = json_Object
										.getString("iLastIndex");
								strLastRunDate = json_Object
										.getString("dLastRunDate");
								arrDealFeeds.clear();

								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.FriendsDeal(json_deal);
									arrDealFeeds.add(dealDetail);

								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();

						if (!commonClass.checkInternetConnection()) {

							try {
								lstFriendPurchase.removeFooterView(views);

							} catch (Exception e) {
								// TODO: handle exception
							}

							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						} else {
							if (strStatusCode.equals("101")) {

								// txtFrienPurchaseNoData.setText(strMessageResponse);
								try {
									lstFriendPurchase.removeFooterView(views);
									loadmore = false;

								} catch (Exception e) {
									// TODO: handle exception
								}

								if (lstFriendPurchase.getVisibility() == View.VISIBLE) {
									swipeLayout.setVisibility(View.GONE);
									lstFriendPurchase.setVisibility(View.GONE);
									txtFrienPurchaseNoData
											.setVisibility(View.VISIBLE);
								}
							} else if (strStatusCode.equals("118")) {
								try {
									lstFriendPurchase.removeFooterView(views);

								} catch (Exception e) {
									// TODO: handle exception
								}

								loadmore = false;

							} else {
								// if (Integer.parseInt(StrPageIndex) < 11) {
								// loadmore = false;
								// } else {
								loadmore = true;
								// }

								if (lstFriendPurchase.getVisibility() == View.GONE) {
									swipeLayout.setVisibility(View.VISIBLE);
									lstFriendPurchase
											.setVisibility(View.VISIBLE);
									txtFrienPurchaseNoData
											.setVisibility(View.GONE);
								}
								setblankAdapter();
							}
						}

					}
				});
		as.execute();
	}

	private void pullToRefreshDeals() {
		final String strTempUrl = commonClass.Url_EncodeWithBlank(strGeturl
				+ "&startindex=1" + "&dLastRunDate=" + strLastRunDate);

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {
					}

					@Override
					public void doInBackground() {
						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);
						try {
							JSONObject json_Object = new JSONObject(urlResponse);
							JSONArray json_result = json_Object
									.getJSONArray("FriendList");
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");
							if (strStatusCode.equals("100")) {
								StrPageIndex = json_Object
										.getString("iLastIndex");
								strLastRunDate = json_Object
										.getString("dLastRunDate");
								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.FriendsDeal(json_deal);
									arrDealFeeds.add(0, dealDetail);
								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {
						if (strStatusCode.equals("101")) {
							// txtFrienPurchaseNoData.setText(strMessageResponse);
							swipeLayout.setVisibility(View.GONE);
							lstFriendPurchase.setVisibility(View.GONE);
							txtFrienPurchaseNoData.setVisibility(View.VISIBLE);
						} else if (strStatusCode.equals("118")) {

						} else {

							// strGeturl = strGeturl + "&startindex="
							// + StrPageIndex + "&dLastRunDate="
							// + strLastRunDate;
							// strGeturl = commonClass
							// .Url_EncodeWithBlank(strGeturl);
							swipeLayout.setVisibility(View.VISIBLE);
							lstFriendPurchase.setVisibility(View.VISIBLE);
							txtFrienPurchaseNoData.setVisibility(View.GONE);
							adapterFriendPurchase.notifyDataSetChanged();
						}
					}
				});
		as.execute();
	}

	public void setblankAdapter() {
		adapterFriendPurchase = new FriendPurchaseDealFeedAdapter(
				getActivity(), arrDealFeeds);
		lstFriendPurchase.setAdapter(adapterFriendPurchase);
	}

	private void loadmore_moredeals() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					// String strTempUrl =
					// "DealService.svc/GetAllDeals?customerid="
					// + commonClass.CustomerId
					// + "&languageid="
					// + commonClass.languageId
					// + "&latitude="
					// + commonClass.currentLatitude
					// + "&longitude="
					// + commonClass.currentLongitude
					// + "&startindex="
					// + StrPageIndex;

					String strTempUrl = "FriendService.svc/GetAllFriendPurchase?customerid="
							+ commonClass.CustomerId
							+ "&languageid="
							+ commonClass.languageId
							+ "&latitude="
							+ commonClass.currentLatitude
							+ "&longitude="
							+ commonClass.currentLongitude
							+ "&startindex="
							+ StrPageIndex;

					// dealservice.svc/GetAllDeals?customerid=17&languageid=1&latitude=23.0&longitude=72.0&startindex=1&dLastRunDate=

					@Override
					public void onPreExecute() {

					}

					@Override
					public void doInBackground() {

						//
						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);

						try {

							JSONObject json_Object = new JSONObject(urlResponse);
							JSONArray json_result = json_Object
									.getJSONArray("FriendList");
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");
							if (strStatusCode.equals("100")) {

								StrPageIndex = json_Object
										.getString("iLastIndex");

								strLastRunDate = json_Object
										.getString("dLastRunDate");

								if (!StrPageIndex.equals("0")) {

									for (int i = 0; i < json_result.length(); i++) {
										JSONObject json_deal = json_result
												.getJSONObject(i);
										DealFeedDetail dealDetail = new DealFeedDetail();
										dealDetail = dealDetail
												.FriendsDeal(json_deal);
										arrDealFeeds.add(dealDetail);
									}
								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {

						// lnLoadMore.setVisibility(View.GONE);
						//

						if (strStatusCode.equals("101")) {

							try {
								lstFriendPurchase.removeFooterView(views);
								loadmore = false;

							} catch (Exception e) {
								// TODO: handle exception
							}

							// txtFrienPurchaseNoData.setText(strMessageResponse);
							swipeLayout.setVisibility(View.GONE);

							lstFriendPurchase.setVisibility(View.GONE);
							txtFrienPurchaseNoData.setVisibility(View.VISIBLE);
							preLast = 0;
						} else if (strStatusCode.equals("118")) {
							try {
								lstFriendPurchase.removeFooterView(views);
								loadmore = false;

							} catch (Exception e) {
								// TODO: handle exception
							}

						} else {

							loadmore = true;
							strGeturl = commonClass
									.Url_EncodeWithBlank(strGeturl);
							swipeLayout.setVisibility(View.VISIBLE);
							lstFriendPurchase.setVisibility(View.VISIBLE);
							txtFrienPurchaseNoData.setVisibility(View.GONE);

							adapterFriendPurchase.notifyDataSetChanged();
						}

					}
				});
		as.execute();

	}

}