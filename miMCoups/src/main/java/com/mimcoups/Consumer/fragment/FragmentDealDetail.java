package com.mimcoups.Consumer.fragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.google.android.gms.plus.PlusShare;
import com.mimcoups.Consumer.commonClass.CircleTransform;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.Consumer.FacebookShare;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.mimcoups.Consumer.inteface.shareingdeal;
import com.squareup.picasso.Picasso;

public class FragmentDealDetail extends Fragment {

	ImageView imgDealDetailshare, imgDealDetailBack, imgDealImage,productImage;
	Button btnBuyDeal;
	DealFeedDetailInterface dealInterface;
	private topBarInteface topInterface;
	private LinearLayout lnDealDetailMedia, lnDealDetailDiscuss,
			lnDealDetailReview, lnDealDetailFavourite, lngooglepluscontainer;
//	private TextView  txt_dealcondition;
	private TextView txtCountdownTime;
	private TextView dealDescription,
			txtDealdetailDiscountedAmount, DealPurchasedCount,txtLocAddress_LocationDetail,
			txt_reviewcount_dealdetail;
	private LinearLayout lninstagramcontainer, dealdetailsharingoption,
			lnDealNumberOfReviews;
	Context context;
	LinearLayout lnfacebookcontainer, lntwittercontainer, lnmimcupshare,
			lnMailShare;
	private CommonData globalClass;
	private String serverResponse = "";
	DealFeedDetail dealdetail;
	private RatingBar ratingbar;
//	ViewPager pager;
	private CountDownTimer counter;
	private WebService_Call webservicecall = new WebService_Call();
	private String statusCode = "", statusMessage = "";
	private ImageView imgFavourite;
	int seconds;
	Calendar calendar = Calendar.getInstance();
	private Timer timer;
	TextView txtDeelFeed;
	shareingdeal sharedeal;
	String share = "";
	public static ArrayList<String> ListofFriendsEmailtoshare = new ArrayList<String>();
	private WebService_Call webService = new WebService_Call();
	private String url_response = "", getUrl = "", vStatusCode = "",
			vMessageResponse = "";
	// public static final String[] PERMISSIONS = new String[] {
	// "publish_stream",
	// "read_stream", "offline_access" };
	private static final List<String> PERMISSIONS = Arrays
			.asList("publish_actions");
	LinearLayout relmainfragment_deal_detail;
//	LinearLayout lnMainScrollLayout;
	Animation animFadein, animFadeOut;
	private File FileImage;
	ScrollView dealdetailscroll;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	LinearLayout lnBottom;
	TextView  txtrow;
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;
	TextView merchantstorename;
	String str_newsimg, str_dealname, str_dealdesc;
	boolean isShow = false;
	LinearLayout lnRegister, lnbuynow, lnbought, lnmaplink;
	String quantity = "0";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_deal_detail_test, container,
				false);

		dealdetail = (DealFeedDetail) getArguments().getSerializable(
				"DealFeedDetail");
		if (CommonData.languageId.equalsIgnoreCase("1")) {
			share = CommonData.dealshareUrl_au + dealdetail.DealId;

		} else {
			share = CommonData.dealshareUrl_no + dealdetail.DealId;
		}
		initialization(view);
		if (!globalClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			getDealDetailAsync();

		}

		onClickEvents(view);
//		txtDealDetailTotalPrice.setPaintFlags(txtDealDetailTotalPrice
//				.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		return view;
	}

	// Initialitions
	@SuppressLint("NewApi")
	public void initialization(View view) {

		String dir = Environment.getExternalStorageDirectory() + File.separator
				+ "." + getResources().getString(R.string.app_name);
		FileImage = new File(dir, "2.jpg");
		relmainfragment_deal_detail=(LinearLayout)view.findViewById(R.id.relmainfragment_deal_detail);

//		relmainfragment_deal_detail = (LinearLayout) view
//				.findViewById(R.id.relmainfragment_deal_detail);

		txtrow = (TextView) view.findViewById(R.id.txtrow);
		animFadein = AnimationUtils
				.loadAnimation(getActivity(), R.anim.fade_in);
		lnRegister = (LinearLayout) view.findViewById(R.id.lnRegister);
		lnbought = (LinearLayout) view.findViewById(R.id.lnbought);
//		txtdealquantity = (TextView) view.findViewById(R.id.txtdealquantity);

		dealdetailscroll = (ScrollView) view
				.findViewById(R.id.dealdetailscroll);
		animFadeOut = AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_out);
		timer = new Timer();
//		txtDealDetailTotalPrice = (TextView) view.findViewById(R.id.txtDealDetailTotalPrice);
		lnDealNumberOfReviews = (LinearLayout) view
				.findViewById(R.id.lnDealNumberOfReviews);
		dealdetailsharingoption = (LinearLayout) view
				.findViewById(R.id.dealdeatailsharingoption);
		txtDeelFeed = (TextView) view.findViewById(R.id.txtDeelFeed);
		btnBuyDeal = (Button) view.findViewById(R.id.buydeal);
		imgDealDetailshare = (ImageView) view
				.findViewById(R.id.imgDealDetailshare);
		imgDealDetailBack = (ImageView) view
				.findViewById(R.id.imgDealDetailBack);
//		lnMainScrollLayout = (LinearLayout) view.findViewById(R.id.ll_topmain);
		lnDealDetailMedia = (LinearLayout) view
				.findViewById(R.id.lnDealDetailMedia);
		txtLocAddress_LocationDetail = (TextView) view
				.findViewById(R.id.txtLocAddress_LocationDetail);

		lnBottom = (LinearLayout) view.findViewById(R.id.lnBottom);
		merchantstorename = (TextView) view
				.findViewById(R.id.merchantstorename);

		lnDealDetailDiscuss = (LinearLayout) view
				.findViewById(R.id.lnDealDetailDiscuss);

		lnDealDetailReview = (LinearLayout) view
				.findViewById(R.id.lnDealDetailReview);

		lnDealDetailFavourite = (LinearLayout) view
				.findViewById(R.id.lnDealDetailFavourite);
		imgFavourite = (ImageView) view.findViewById(R.id.imgFavourite);
		lninstagramcontainer = (LinearLayout) view
				.findViewById(R.id.lninstagramcontainer);

		lnfacebookcontainer = (LinearLayout) view
				.findViewById(R.id.lnfacebookcontainer);
		lntwittercontainer = (LinearLayout) view
				.findViewById(R.id.lntwittercontainer);
		lngooglepluscontainer = (LinearLayout) view
				.findViewById(R.id.lngooglepluscontainer);
		productImage=(ImageView)view
				.findViewById(R.id.productImage);
		imgDealImage = (ImageView) view
				.findViewById(R.id.img_dealdetail_DealImage);
//		txt_dealcondition = (TextView) view
//				.findViewById(R.id.txt_dealcondition);
//		txtDealName_dealdetail = (TextView) view
//				.findViewById(R.id.txtDealName_dealdetail);
		txtDealdetailDiscountedAmount = (TextView) view
				.findViewById(R.id.txtDealdetailDiscountedAmount);
		DealPurchasedCount = (TextView) view
				.findViewById(R.id.txtDealPurchasedCount_dealdetail);
		txt_reviewcount_dealdetail = (TextView) view
				.findViewById(R.id.txt_reviewcount_dealdetail);
//		dealname_dealdetail = (TextView) view
//				.findViewById(R.id.txtdealname_dealdetail);
//		txtdealdescription_dealDetail = (TextView) view
//				.findViewById(R.id.txtdealdescription_dealDetail);
//		txt_dealHighlights_dealDetail = (TextView) view
//				.findViewById(R.id.txt_dealHighlights_dealDetail);

		txtCountdownTime = (TextView) view.findViewById(R.id.txtCountdownTime);
		dealDescription = (TextView) view
				.findViewById(R.id.txt_dealdescriptionDealDetail);

		ratingbar = (RatingBar) view.findViewById(R.id.ratingBar_dealdetail);
		globalClass = (CommonData) getActivity().getApplicationContext();
		lnmimcupshare = (LinearLayout) view.findViewById(R.id.lnmimcupshare);
		lnMailShare = (LinearLayout) view.findViewById(R.id.lnMailShare);
		lnmaplink = (LinearLayout) view.findViewById(R.id.lnmaplink);
		lnbuynow = (LinearLayout) view.findViewById(R.id.lnbuynow);

		try {

			if (dealdetail.dealDiscountPrice.trim().toString().contains("$0")
					|| dealdetail.dealDiscountPrice.trim().toString()
							.contains("kr0")) {
				String price="";
				if(dealdetail.dealDiscountPrice.trim().toString().contains("$0")){
					
					price=dealdetail.dealDiscountPrice.trim().toString().replace("$", "").trim();
					
					
				}else if(dealdetail.dealDiscountPrice.trim().toString().contains("kr0")){
					price=dealdetail.dealDiscountPrice.trim().toString().replace("kr", "").trim();
					
				}
				
				Log.e("price", price);
				

			
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@SuppressLint("NewApi")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {

		super.onViewCreated(view, savedInstanceState);
//		pager = (ViewPager) view.findViewById(R.id.myViewPager);

	}

	class MyAdapter extends PagerAdapter {
		ArrayList<String> IMAGES;

		MyAdapter() {

			IMAGES = new ArrayList<String>();

			if (!dealdetail.vDealDetailImage1_A_500X500.equals("no image")) {
				IMAGES.add(dealdetail.vDealDetailImage1_A_500X500);
			}
			if (!dealdetail.vDealDetailImage2_A_500X500.equals("no image")) {
				IMAGES.add(dealdetail.vDealDetailImage2_A_500X500);
			}
			if (!dealdetail.vDealDetailImage3_A_500X500.equals("no image")) {
				IMAGES.add(dealdetail.vDealDetailImage3_A_500X500);
			}
			if (!dealdetail.vDealDetailImage4_A_500X500.equals("no image")) {
				IMAGES.add(dealdetail.vDealDetailImage4_A_500X500);
			}

		}

		@Override
		public int getCount() {
			return IMAGES.size();
		}

		@Override
		public Object instantiateItem(View container, int position) {

			LinearLayout lLayour = new LinearLayout(getActivity());
			lLayour.setPadding(5, 5, 5, 5);

			ImageView iv = new ImageView(getActivity());
			iv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT));

			// 240dp
			iv.setBackgroundResource(R.drawable.blue_dotted_line);
			iv.setPadding(0, 10, 0, 10);

			Picasso.with(context).load(IMAGES.get(position))
					.placeholder(R.drawable.noimage_dealslide).into(iv);

			// iv.setPadding(5, 5, 5, 5);

			lLayour.addView(iv);

			((ViewPager) container).addView(lLayour, 0);
			return lLayour;
		}

		@Override
		public float getPageWidth(int position) {

			float fval = 0.90f;
			if (IMAGES.size() == 1) {
				fval = 1.00f;
			} else {
				fval = 0.90f;
			}
			return fval;
		}

		@Override
		public void destroyItem(View container, int position, Object obj) {
			((ViewPager) container).removeView((View) obj);
		}

		@Override
		public boolean isViewFromObject(View container, Object obj) {
			return container == obj;
			//
		}

	}

	public void onClickEvents(View view) {

		 lnBottom.setOnClickListener(new OnClickListener() {

		 @Override
		 public void onClick(View v) {
		 // TODO Auto-generated method stub

		 long currentTime = SystemClock.elapsedRealtime();
		 if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
		 lastClickTime = currentTime;
		 }

		 }
		 });



		lnmaplink.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dealInterface.dealFeedClick("dealmap", dealdetail);
			}
		});

		imgDealImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentConsumerMerchantDeals();
				Bundle bundle = new Bundle();
				bundle.putSerializable("LocationDetail", dealdetail);
				bundle.putString("from", "dealsdetail");
				newFragment.setArguments(bundle);
				String strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
					}
		});

		dealdetailscroll.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				if (dealdetailsharingoption.getVisibility() == View.VISIBLE) {

					imgDealDetailshare
							.setImageResource(R.drawable.header_icon_share);
					dealdetailsharingoption.setAnimation(animFadeOut);
					dealdetailsharingoption.startAnimation(animFadeOut);
					dealdetailsharingoption.setVisibility(View.GONE);
				}

				return false;
			}
		});

//		lnMainScrollLayout.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//
//				if (dealdetailsharingoption.getVisibility() == View.VISIBLE) {
//
//					imgDealDetailshare
//							.setImageResource(R.drawable.header_icon_share);
//					dealdetailsharingoption.setAnimation(animFadeOut);
//					dealdetailsharingoption.startAnimation(animFadeOut);
//					dealdetailsharingoption.setVisibility(View.GONE);
//				}
//
//			}
//		});

		relmainfragment_deal_detail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		lnmimcupshare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					ListofFriendsEmailtoshare.clear();

					sharedeal.onsharedealclicked("mimcup", dealdetail);

				}

			}
		});

		lnMailShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					sharedeal.onsharedealclicked("mail", dealdetail);

				}

			}
		});

		imgDealDetailshare.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (dealdetailsharingoption.getVisibility() == View.GONE) {
					imgDealDetailshare
							.setImageResource(R.drawable.header_icon_share_selected);
					dealdetailsharingoption.setVisibility(View.VISIBLE);
					dealdetailsharingoption.setAnimation(animFadein);
					dealdetailsharingoption.startAnimation(animFadein);

				} else {
					imgDealDetailshare
							.setImageResource(R.drawable.header_icon_share);
					dealdetailsharingoption.setAnimation(animFadeOut);
					dealdetailsharingoption.startAnimation(animFadeOut);
					dealdetailsharingoption.setVisibility(View.GONE);
				}
			}
		});
		btnBuyDeal.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (dealdetailsharingoption.getVisibility() == View.VISIBLE) {

					imgDealDetailshare
							.setImageResource(R.drawable.header_icon_share);
					dealdetailsharingoption.setAnimation(animFadeOut);
					dealdetailsharingoption.startAnimation(animFadeOut);
					dealdetailsharingoption.setVisibility(View.GONE);
				}

				if (!dealdetail.bBuyNowStatus) {

					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									topInterface.topbarClick("back");
								}
							}, dealdetail.vBuyNowMessage);
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);

				} else {

					

						dealInterface.dealFeedClick("DealPurchase", dealdetail);
					
				}
			}
		});
		imgDealDetailBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				topInterface.topbarClick("back");
			}
		});
		lnDealDetailMedia.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.e("Media Clicked","Media Clicked");
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (!dealdetail.vMediaURL.equals("")
							&& dealdetail.vMediaURL != null) {
						Intent browserIntent = new Intent(Intent.ACTION_VIEW,
								Uri.parse(dealdetail.vMediaURL));
						startActivity(browserIntent);
					} else {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {

									}
								}, getActivity().getResources()
										.getText(R.string.mediaurlvalidation)
										.toString());
						ds.show(getFragmentManager(), "");
					}
				}
			}
		});
		lnDealDetailDiscuss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					dealInterface.dealFeedClick("dealBuy", dealdetail);
				}
			}
		});
		lnDealDetailReview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					dealInterface.dealFeedClick("writereview", dealdetail);
				}
			}
		});
		lnDealDetailFavourite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					((DeelFeedActivity) getActivity()).isDealListingFavourite = false;

					if (dealdetail.bIsFavorite.equals("true")) {

						((DeelFeedActivity) getActivity()).isDealfavourite = true;

						imgFavourite
								.setImageResource(R.drawable.ic_star_empty);
						dealdetail.bIsFavorite = "false";
					} else {
						imgFavourite
								.setImageResource(R.drawable.ic_star_filled);
						((DeelFeedActivity) getActivity()).isDealfavourite = false;
						dealdetail.bIsFavorite = "true";
					}
					dealFavourite(dealdetail.bIsFavorite);
				}
			}
		});
		lnDealNumberOfReviews.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dealInterface.dealFeedClick("writereview", dealdetail);
			}
		});

		lninstagramcontainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Intent i = new Intent(getActivity(), InstaActivity.class);
				// i.putExtra("sharingfrom", 0);
				// startActivity(i);

				// if (!appInstalledOrNot()) {
				// Toast.makeText(
				// getActivity(),
				// "Please install Instagram Application from Play Store"
				// + "", Toast.LENGTH_LONG).show();
				// return;
				// } else {
				// Intent shareIntent = new Intent(
				// android.content.Intent.ACTION_SEND);
				// shareIntent.setType("image/*"); // set mime type
				// Uri path = Uri
				// .parse("android.resource://com.android.MimCoup/"
				// + R.drawable.app_icon);
				//
				// // Uri path = Uri
				// //
				// .parse("file:///storage/sdcard1/RotateImageDemo/res/drawable-hdpi/nirmal.jpg");
				//
				// shareIntent.putExtra(Intent.EXTRA_STREAM, path); // set uri
				// shareIntent.setPackage("com.instagram.android");
				// startActivity(shareIntent);
				// }

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					new getbitmap().execute(dealdetail.DealImageUrl);

				}
			}
		});

		lnfacebookcontainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Intent i = new Intent(getActivity(), facebookshare.class);
				// i.putExtra("sharingfrom", 0);
				// startActivity(i);
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					imgDealDetailshare
							.setImageResource(R.drawable.header_icon_share);
					dealdetailsharingoption.setAnimation(animFadeOut);
					dealdetailsharingoption.startAnimation(animFadeOut);
					dealdetailsharingoption.setVisibility(View.GONE);

					onFacebookClick();
				}

			}
		});

		lngooglepluscontainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Intent iv = new Intent(getActivity(), SignInActivity.class);
				// // iv.putExtra("sharingfrom", 0);
				// iv.addFlags(1);
				// startActivity(iv);

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					Intent shareIntent = new PlusShare.Builder(getActivity())
							.setType("text/plain")
							.setText(
									"MiMCoups APP:: \n " + dealdetail.dealName
											+ "\n "
											+ dealdetail.vShortDescription)

							.setContentUrl(Uri.parse(share))
							// .setContentUrl(Uri.parse(dealdetail.DealImageUrl))
							.getIntent();
					startActivityForResult(shareIntent, 0);

				}

			}
		});

		lntwittercontainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					try {
						// Log.e("cdeeel feedadapter", ""
						// + dealdetail.DealImageUrl);
						Uri uri = Uri
								.fromFile(new File(dealdetail.DealImageUrl));
						Intent tweetIntent = new Intent(Intent.ACTION_SEND);
						// tweetIntent.putExtra(Intent.EXTRA_TEXT,
						// dealdetail.dealName + "\n "
						// + dealdetail.vShortDescription);
						tweetIntent.putExtra(Intent.EXTRA_TEXT,
								dealdetail.dealName + "\n " + share);

						tweetIntent.putExtra(Intent.EXTRA_STREAM, uri);
						tweetIntent.setType("*/*");
						PackageManager packManager = context
								.getPackageManager();
						List<ResolveInfo> resolvedInfoList = packManager
								.queryIntentActivities(tweetIntent,
										PackageManager.MATCH_DEFAULT_ONLY);

						boolean resolved = false;
						for (ResolveInfo resolveInfo : resolvedInfoList) {
							if (resolveInfo.activityInfo.packageName
									.startsWith("com.twitter.android")) {
								tweetIntent.setClassName(
										resolveInfo.activityInfo.packageName,
										resolveInfo.activityInfo.name);
								resolved = true;
								break;
							}
						}
						if (resolved) {
							context.startActivity(tweetIntent);
						} else {
							try {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("market://details?id=com.twitter.android")));
							} catch (android.content.ActivityNotFoundException anfe) {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android")));
							}
						}

						// Uri uri = Uri.parse(dealdetail.DealImageUrl);
						// // Uri uri =
						// //
						// Uri.parse("android.resource://com.gobaby.app/drawable/back");
						// Intent intent = new Intent(Intent.ACTION_SEND);
						// intent.setType("/*");
						// intent.setClassName("com.twitter.android",
						// "com.twitter.android.PostActivity");
						// intent.putExtra(Intent.EXTRA_TEXT,
						// dealdetail.dealName
						// + " With " + dealdetail.vSavingamount + "\n\n "
						// + dealdetail.vShortDescription);
						// intent.putExtra(Intent.EXTRA_STREAM, uri);
						// context.startActivity(intent);

					} catch (Exception e) {

					}

					// Intent tp2 = new Intent(getActivity(),
					// PrepareRequestTokenActivity.class);
					// tp2.putExtra("sharingfrom", 0);
					// tp2.putExtra("dealname", dealdetail.dealName);
					// tp2.putExtra("desc", dealdetail.vShortDescription);
					// tp2.putExtra("Image", dealdetail.DealImageUrl);
					//
					// startActivity(tp2);

				}
			}
		});
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
		// if (counter != null) {
		// counter.cancel();
		// counter = null;
		// }
	}

	@Override
	public void onStop() {

		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		context = activity;
		topInterface = (topBarInteface) activity;
		dealInterface = (DealFeedDetailInterface) activity;

		sharedeal = (shareingdeal) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {

		super.onDetach();
	}

	@Override
	public void onResume() {

		super.onResume();

		IntentFilter filter = new IntentFilter();
		filter.addAction("com.mimcoups.fragment.responselistener");
		filter.addCategory("com.mimcoups.fragment");

		// Toast.makeText(getActivity(), "On Resume",
		// Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onPause() {

		super.onPause();
	}

	private void getDealDetailAsync() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {

						DeelFeedActivity deal = (DeelFeedActivity) context;

						if (deal.progressbarshowing) {
							topInterface.hideDialog();
						}

						topInterface.showDialog("Loading");
					}

					@Override
					public void doInBackground() {
						String serverUrl = "DealService.svc/GetDealDetail?dealid="
								+ dealdetail.DealId
								+ "&customerid="
								+ globalClass.CustomerId
								+ "&languageid="
								+ globalClass.languageId
								+ "&locationid="
								+ dealdetail.locationId
								+ "&categoryid="
								+ dealdetail.categoryId;
						Log.e("Url==", serverUrl);

						serverResponse = webservicecall
								.makeServicegetRequest(serverUrl);

						serverResponse = serverResponse.replace("\\n", "\n");

						Log.d("server response>>", "" + serverResponse);

						try {
							JSONObject js_getres = new JSONObject(
									serverResponse);
							statusCode = js_getres.getString("vStatusCode");
							statusMessage = js_getres
									.getString("vMessageResponse");
							if (statusCode.equals("100")) {
								JSONArray js_arr_data = js_getres
										.getJSONArray("DealDetail");
								for (int i = 0; i < js_arr_data.length(); i++) {
									JSONObject jsobj = js_arr_data
											.getJSONObject(i);

									dealdetail.bIsFavorite = jsobj
											.getString("bIsFavorite");
									dealdetail.dLatitude = jsobj
											.getDouble("dLatitude");

									dealdetail.dLongitude = jsobj
											.getDouble("dLongitude");

									dealdetail.dDealDiscountedAmount = jsobj
											.getString("dDealDiscountedAmount");

									dealdetail.dDealOrigianlAmount = jsobj
											.getString("dDealOrigianlAmount");

									dealdetail.iDealPurchasedCount = jsobj
											.getString("iDealPurchasedCount");

									dealdetail.iLocationId = jsobj
											.getString("iLocationId");

									dealdetail.iMerchantDealId = jsobj
											.getString("iMerchantDealId");

									dealdetail.vAddress = jsobj
											.getString("vAddress");

									dealdetail.iRating = jsobj
											.getString("iRating");

									dealdetail.iReviews = jsobj
											.getString("iReviews");

									dealdetail.vCategoryName = jsobj
											.getString("vCategoryName");

									dealdetail.vDealCondition = jsobj
											.getString("vDealCondition");

									dealdetail.vDealDetailImage1_A_500X500 = jsobj
											.getString("vDealDetailImage1_A_500X500");

									dealdetail.vDealDetailImage2_A_500X500 = jsobj
											.getString("vDealDetailImage2_A_500X500");

									dealdetail.vDealDetailImage3_A_500X500 = jsobj
											.getString("vDealDetailImage3_A_500X500");

									dealdetail.vDealDetailImage4_A_500X500 = jsobj
											.getString("vDealDetailImage4_A_500X500");

									dealdetail.vDealListImage_A_350X350 = jsobj
											.getString("vDealListImage_A_350X350");

									dealdetail.vDeal_Completion_time = jsobj
											.getString("vDeal_Completion_time");

									dealdetail.vHighlights = jsobj
											.getString("vHighlights");

									dealdetail.bBuyNowStatus = jsobj
											.getBoolean("bBuyNowStatus");
									dealdetail.vBuyNowMessage = jsobj
											.getString("vBuyNowMessage");

									dealdetail.vMediaURL = jsobj
											.getString("vMediaURL");

									dealdetail.vMerchantDealName = jsobj
											.getString("vMerchantDealName");
									dealdetail.vMerchantStoreName = jsobj
											.getString("vMerchantStoreName");
									dealdetail.vRemainingSeconds = jsobj
											.getString("vRemainingSeconds");
									dealdetail.vShortDescription = jsobj
											.getString("vShortDescription");
									dealdetail.vShortDescriptionHeading = jsobj
											.getString("vShortDescriptionHeading");
									dealdetail.vStoreListImage_A_70X70 = jsobj
											.getString("vStoreListImage_I_120X120");
									dealdetail.vDescription = jsobj
											.getString("vDescription");

									quantity = jsobj
											.getString("iDealRemainingQuantity");

									// //////nirmal
									dealdetail.StoreName = dealdetail.vMerchantStoreName;
									dealdetail.dealName = dealdetail.vMerchantDealName;
									dealdetail.shortDescription = dealdetail.vShortDescription;
									dealdetail.shortDescriptionHeading = dealdetail.vShortDescriptionHeading;
									dealdetail.DealImageUrl = dealdetail.vDealListImage_A_350X350;

									dealdetail.DealOrignalPrice = dealdetail.dDealOrigianlAmount;
									dealdetail.dealDiscountPrice = dealdetail.dDealDiscountedAmount;
									// dealdetail.dealDistanse=dealdetail.vDealListImage_A_350X350;

									dealdetail.locationId = dealdetail.iLocationId;
									dealdetail.categoryId = dealdetail.categoryId;
									dealdetail.DealId = dealdetail.iMerchantDealId;

									dealdetail.dealDiscountPrice = dealdetail.dDealDiscountedAmount;
									// dealdetail.vMerchantStoreName

								}

							} else {
								statusMessage = js_getres
										.getString("vMessageResponse");
							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();

						if (!globalClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {

							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (statusCode.equals("100")) {

//								txtdealquantity.setText(quantity);

//								pager.setAdapter(new MyAdapter());
								SetDatafromServer();
								seconds = Integer
										.valueOf(dealdetail.vRemainingSeconds);
								try {

									timer.schedule(new TimerTask() {
										@Override
										public void run() {

											if (seconds == 0) {

												timer.cancel();
												// topInterface.topbarClick("back");

												getActivity().runOnUiThread(
														new Runnable() {

															@Override
															public void run() {

																try {
																	txtCountdownTime
																			.setText("00 "
																					+ getResources()
																							.getString(
																									R.string.txtDay)
																					+ ", "
																					+ "00"
																					+ ":"
																					+ "00"
																					+ ":"
																					+ "00");
																} catch (Exception e) {

																}
															}
														});

											} else {
												try {
													calculateTime();
												} catch (Exception e) {
													// TODO: handle exception
												}

											}
										}
									}, 1000, 1000);
								} catch (Exception e) {
									// TODO: handle exception
								}

							} else {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");

											}
										}, statusMessage);
								ds.show(getFragmentManager(), "");
							}
						}

					}

					private void SetDatafromServer() {

						if (dealdetail.bIsFavorite.equals("true")) {
							imgFavourite
									.setImageResource(R.drawable.ic_star_filled);
						} else {
							imgFavourite
									.setImageResource(R.drawable.ic_star_empty);
						}

						if (!dealdetail.vDealDetailImage1_A_500X500.equals("no image")) {
							Picasso.with(context).load(dealdetail.vDealDetailImage1_A_500X500).into(productImage);
						}
						Picasso.with(context)
								.load(dealdetail.vStoreListImage_A_70X70)
								.transform(new CircleTransform())
								.into(imgDealImage);
//						txt_dealcondition.setText(Html
//								.fromHtml(dealdetail.vDealCondition.trim()));
//						txtDealName_dealdetail
//								.setText(dealdetail.vMerchantDealName);

						DealPurchasedCount
								.setText(dealdetail.iDealPurchasedCount);
						ratingbar.setRating(Float
								.parseFloat(dealdetail.iRating));
//
						merchantstorename
								.setText(dealdetail.vMerchantStoreName);

						txtLocAddress_LocationDetail
								.setText(dealdetail.vAddress);

						txt_reviewcount_dealdetail.setText(dealdetail.iReviews);

//						dealname_dealdetail
//								.setText(dealdetail.vShortDescriptionHeading);

						dealDescription.setText(Html
								.fromHtml(dealdetail.vDescription));

						dealdetail.vShortDescription = dealdetail.vShortDescription
								.replace("\\n", "\n");
						dealdetail.vShortDescription = dealdetail.vShortDescription
								.replace(">", "●");
//						txtdealdescription_dealDetail
//								.setText(dealdetail.vShortDescription);

//						txt_dealHighlights_dealDetail.setText(Html
//								.fromHtml(dealdetail.vHighlights));

						txtDeelFeed.setText("" + dealdetail.vCategoryName);

						try {

							if (dealdetail.dDealDiscountedAmount.trim()
									.toString().contains("$0")
									|| dealdetail.dealDiscountPrice.trim()
											.toString().contains("kr0")) {
								
								
								
								
								String price="";
								if(dealdetail.dealDiscountPrice.trim().toString().contains("$0")){
									
									price=dealdetail.dealDiscountPrice.trim().toString().replace("$", "").trim();
									
									
								}else if(dealdetail.dealDiscountPrice.trim().toString().contains("kr0")){
									price=dealdetail.dealDiscountPrice.trim().toString().replace("kr", "").trim();
									
								}
								
								Log.e("price", price);
								
								if(Double.parseDouble(price)>0){
//									btnBuyDeal.setVisibility(View.VISIBLE);
//									lnbuynow.setVisibility(View.VISIBLE);
									txtrow.setVisibility(View.VISIBLE);

//									txtDealDetailTotalPrice
//											.setVisibility(View.VISIBLE);
//
//									txtDealDetailTotalPrice
//											.setText(dealdetail.dDealOrigianlAmount);
									txtDealdetailDiscountedAmount
											.setText(dealdetail.dDealDiscountedAmount);
								}else{
									
//									btnBuyDeal.setVisibility(View.GONE);
//									lnbuynow.setVisibility(View.GONE);

									txtrow.setVisibility(View.GONE);

//									txtDealDetailTotalPrice
//											.setVisibility(View.GONE);
									txtDealdetailDiscountedAmount
											.setText(getActivity().getResources()
													.getString(
															R.string.txtdealprice));
									
								}
								
								
								
								

							} else {
//								btnBuyDeal.setVisibility(View.VISIBLE);
//								lnbuynow.setVisibility(View.VISIBLE);
								txtrow.setVisibility(View.VISIBLE);

//								txtDealDetailTotalPrice
//										.setVisibility(View.VISIBLE);
//
//								txtDealDetailTotalPrice
//										.setText(dealdetail.dDealOrigianlAmount);
								txtDealdetailDiscountedAmount
										.setText(dealdetail.dDealDiscountedAmount);
							}
						} catch (Exception e) {
							// TODO: handle exception
						}

					}
				});
		as.execute();
	}

	public void calculateTime() {
		calendar = Calendar.getInstance();
		final String temp = timeCalculate(seconds);
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {

				try {
					txtCountdownTime.setText("Time left" + " "+temp);
				} catch (Exception e) {

				}
			}
		});
		seconds -= 1;
	}

	public String timeCalculate(int millisUntilFinished) {
		int day = (int) TimeUnit.SECONDS.toDays(millisUntilFinished);
		long hours = TimeUnit.SECONDS.toHours(millisUntilFinished)
				- TimeUnit.DAYS.toHours(day);
		long minute = TimeUnit.SECONDS.toMinutes(millisUntilFinished)
				- TimeUnit.DAYS.toMinutes(day)
				- TimeUnit.HOURS.toMinutes(hours);
		long second = TimeUnit.SECONDS.toSeconds(millisUntilFinished)
				- TimeUnit.DAYS.toSeconds(day)
				- TimeUnit.HOURS.toSeconds(hours)
				- TimeUnit.MINUTES.toSeconds(minute);

		return twoDigitString(day) + " "
				+ getResources().getString(R.string.txtDay) + ", "
				+ twoDigitString(hours) + ":" + twoDigitString(minute) + ":"
				+ twoDigitString(second);
	}

	private String twoDigitString(long number) {

		if (number == 0) {
			return "00";
		}
		if (number / 10 == 0) {
			return "0" + number;
		}

		return String.valueOf(number);
	}

	private void dealFavourite(final String isChecked) {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						serverResponse = "";
					}

					@Override
					public void doInBackground() {

						try {

							String getUrl = "DealService.svc/UpdateFavoriteDeals";

							getUrl = globalClass.Url_EncodeWithBlank(getUrl);
							String categoryurl = dealdetail.DealId + "|"
									+ isChecked;
							serverResponse = webservicecall
									.makeServiceFavouriteDeals(getUrl,
											categoryurl,
											globalClass.languageId,
											globalClass.CustomerId);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						serverResponse = "";
					}
				});
		as.execute();
	}

	@Override
	public void onDestroyView() {

		super.onDestroyView();
		// if (timer != null) {
		// timer.cancel();
		// timer = null;
		// }
	}

	private void Run_Asyn_To_Share_Deel_With_MimCup() {
		final JSONObject jsonMain = new JSONObject();
		JSONArray array = new JSONArray();
		if (ListofFriendsEmailtoshare.size() == 0) {

			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

						}
					}, getResources().getString(R.string.nofriendsselected));
			dsp.show(getFragmentManager(), "");

			return;
		}
		for (int i = 0; i < ListofFriendsEmailtoshare.size(); i++) {
			JSONObject temPObjects = new JSONObject();
			try {
				temPObjects.put("iFriendId", ListofFriendsEmailtoshare.get(i));
				array.put(temPObjects);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// //////////////
		// RUN BACKGROUND
		try {
			jsonMain.put("iLanguageId", globalClass.languageId);
			jsonMain.put("iCustomerId", globalClass.CustomerId);
			jsonMain.put("iLocationId", dealdetail.iLocationId);
			jsonMain.put("iMerchantDealId", dealdetail.iMerchantDealId);
			jsonMain.put("vMessage", "Hey checkout theawosome deal.");
			jsonMain.put("SharewithFriends", array);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						topInterface.showDialog("Loading");
					}

					@Override
					public void doInBackground() {
						try {
							getUrl = "DealService.svc/ShareDealViaMIMS";
							url_response = webService
									.makeServiceShareViaMimCup(getUrl,
											jsonMain.toString());
							JSONObject json_object = new JSONObject(
									url_response);
							vStatusCode = json_object.getString("vStatusCode");
							vMessageResponse = json_object
									.getString("vMessageResponse");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							vMessageResponse = getResources().getString(
									R.string.errormesssage);

						}
					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();

						if (!globalClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							if (vStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {
											@Override
											public void dialogClick(String tag) {
												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (vStatusCode.equals("100")) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {
											@Override
											public void dialogClick(String tag) {
												// topInterface
												// .topbarClick("sendInvites");
											}
										}, vMessageResponse);
								dsp.show(getFragmentManager(), "");
							} else {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {
											@Override
											public void dialogClick(String tag) {
											}
										}, vMessageResponse);
								dsp.show(getFragmentManager(), "");
							}
						}

					}
				});
		as.execute();

	}

	private void onFacebookClick() {

		if (counter != null) {
			counter.cancel();
			counter = null;
		}

		if (timer != null) {
			timer.cancel();
			timer = null;
		}

		// FragmentTransaction transaction = getFragmentManager()
		// .beginTransaction();
		// Fragment newFragment = new FragmentFBShare();
		// String strFragmentTag = newFragment.toString();
		// Bundle bundle = new Bundle();
		// bundle.putSerializable("DealFeedDetail", dealdetail);
		// newFragment.setArguments(bundle);
		// transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
		// transaction.addToBackStack(strFragmentTag);
		// transaction.commit();

		Intent ii = new Intent(getActivity(), FacebookShare.class);
		ii.putExtra("dealname", dealdetail.dealName);
		ii.putExtra("desc", dealdetail.vShortDescription);
		ii.putExtra("Image", share);
		startActivityForResult(ii, 100);

		// str_newsimg =
		// "http://2.bp.blogspot.com/-kCgRC0xuXtw/UYiu1kHQDcI/AAAAAAAAD3E/ukCg3DVcmDI/s1600/Magic+of+nature.+Beautiful+nature.The+beauty+of+nature.+Wonderful+nature.+Nature+%25E2%2580%25AB%2528139777973%2529%25E2%2580%25AC+%25E2%2580%25AB%25E2%2580%25AC.jpg";
		// str_dealname = "HEllo";
		// str_dealdesc = "Androidapp";
		//
		// ensureOpenSession();

	}

	// private void onFacebookClick() {
	//
	// if (mFaceBook.isSessionValid()) {
	// getActivity().runOnUiThread(new Runnable() {
	// @Override
	// public void run() {
	// final AlertDialog.Builder builder = new AlertDialog.Builder(
	// getActivity());
	//
	// builder.setMessage("Delete current Facebook connection?")
	// .setCancelable(false)
	// .setPositiveButton("Yes",
	// new DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(
	// DialogInterface dialog, int id) {
	// fbLogout();
	// }
	// })
	// .setNegativeButton("No",
	// new DialogInterface.OnClickListener() {
	// @Override
	// public void onClick(
	// DialogInterface dialog, int id) {
	// dialog.cancel();
	// }
	// });
	//
	// final AlertDialog alert = builder.create();
	//
	// alert.show();
	//
	// }
	// });
	//
	// } else {
	// // mFaceBook.authorize(getActivity(),new String[]
	// // {"user_birthday"},-1,
	// // new FbLoginDialogListener());
	//
	// mFaceBook.authorize(getActivity(), PERMISSIONS, -1,
	// new FbLoginDialogListener());
	//
	// }
	//
	// }

	private boolean appInstalledOrNot() {

		boolean app_installed = false;
		try {
			ApplicationInfo info = getActivity().getPackageManager()
					.getApplicationInfo("com.instagram.android", 0);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	public void ratingupdated(float ratingcount, int reviewcount) {

		txt_reviewcount_dealdetail.setText("" + reviewcount);
		ratingbar.setRating(ratingcount);

	}

	public Bitmap getBitmapFromURL(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public class getbitmap extends AsyncTask<String, String, String> {
		Bitmap bm;

		@Override
		protected String doInBackground(String... params) {
			bm = getBitmapFromURL(params[0]);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// imgDealImage.setImageBitmap(bm);
			convertFileToBitmap(bm);
			if (!appInstalledOrNot()) {
				DialogFragment ds = new SingleButtonAlert(
						new DialogInterfaceClick() {

							@Override
							public void dialogClick(String tag) {
								// TODO Auto-generated method stub
								// topinteface.topbarClick("back");
							}
						}, getResources().getString(R.string.InstallInstagram));
				ds.show(getFragmentManager(), "");
				ds.setCancelable(false);
				return;
			} else {
				Intent shareIntent = new Intent(
						android.content.Intent.ACTION_SEND);
				shareIntent.setType("image/*"); // set mime type
				// Uri path = Uri
				// .parse("android.resource://com.android.MimCoup/"
				// + R.drawable.app_icon);
				Uri path = Uri.parse("file:///" + FileImage.getPath());
				shareIntent.putExtra(Intent.EXTRA_TEXT, "MiMCoups APP:: \n "
						+ dealdetail.dealName

						+ "\n " + dealdetail.shortDescription + "\n" + share);
				shareIntent.putExtra(Intent.EXTRA_STREAM, path);
				shareIntent.setPackage("com.instagram.android");
				startActivity(shareIntent);
			}
		}
	}

	public void convertFileToBitmap(Bitmap resized) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(FileImage);
			resized.compress(Bitmap.CompressFormat.PNG, 90, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Throwable ignore) {
			}
		}
	}

	// FACEBOOK

	private boolean ensureOpenSession() {

		boolean installed = appInstalledOrNot("com.facebook.katana");
		if (installed) {
			callFacebookLogout(context.getApplicationContext());
		}

		if (Session.getActiveSession() == null
				|| !Session.getActiveSession().isOpened()) {
			Session.openActiveSession(getActivity(), true,
					new Session.StatusCallback() {
						@Override
						public void call(Session session, SessionState state,
								Exception exception) {
							onSessionStateChanged(session, state, exception);
						}
					});
			return false;
		}
		return true;
	}

	private boolean sessionHasNecessaryPerms(Session session) {
		if (session != null && session.getPermissions() != null) {
			for (String requestedPerm : PERMISSIONS) {
				if (!session.getPermissions().contains(requestedPerm)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	private List<String> getMissingPermissions(Session session) {
		List<String> missingPerms = new ArrayList<String>(PERMISSIONS);
		if (session != null && session.getPermissions() != null) {
			for (String requestedPerm : PERMISSIONS) {
				if (session.getPermissions().contains(requestedPerm)) {
					missingPerms.remove(requestedPerm);
				}
			}
		}
		return missingPerms;
	}

	@SuppressWarnings("deprecation")
	private void onSessionStateChanged(final Session session,
			SessionState state, Exception exception) {
		final Session sessions = Session.getActiveSession();
		// if (state.isOpened() && !sessionHasNecessaryPerms(session)) {
		// AlertDialog.Builder builder = new AlertDialog.Builder(this);
		// builder.setMessage("Facebook");
		// builder.setPositiveButton("OK",
		// new DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// session.requestNewReadPermissions(new NewPermissionsRequest(
		// FacebookShare.this,
		// getMissingPermissions(session)));
		// }
		// });
		// builder.setNegativeButton("no",
		// new DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// finish();
		// }
		// });
		// builder.show();
		// }
		// if (sessions != null && sessions.isOpened()) {
		// // Get the user's data
		//
		// publishFeedDialog();
		// }

	}

	public static void callFacebookLogout(Context context) {
		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

		}

	}

	private boolean appInstalledOrNot(String uri) {
		PackageManager pm = getActivity().getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	private void publishFeedDialog() {
		Bundle params = new Bundle();
		params.putString("name", getResources().getString(R.string.app_name));
		params.putString("caption", str_dealname);
		params.putString("description", str_dealdesc);
		// params.putString("link", "http://54.148.117.181/features.html");
		params.putString("picture", str_newsimg);

		WebDialog feedDialog = (new WebDialog.FeedDialogBuilder(getActivity(),
				Session.getActiveSession(), params)).setOnCompleteListener(
				new OnCompleteListener() {

					@Override
					public void onComplete(Bundle values,
							FacebookException error) {

						if (error == null) {
							// When the story is posted, echo the success
							// and the post Id.
							final String postId = values.getString("post_id");
							if (postId != null) {
								// onBackPressed();
							} else {
								// onBackPressed();
							}
						} else

						if (error instanceof FacebookOperationCanceledException) {
							// User clicked the "x" button
							Toast.makeText(
									getActivity(),
									"Publish cancelled on back pressed call disable",
									Toast.LENGTH_SHORT).show();
							// onBackPressed();
						} else {
							// Generic, ex: network error
							Toast.makeText(getActivity(),
									"Error posting story", Toast.LENGTH_SHORT)
									.show();
						}
					}

				}).build();
		feedDialog.show();

	}

	//
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
			if (!globalClass.checkInternetConnection()) {
				DialogFragment ds = new SingleButtonAlert(
						new DialogInterfaceClick() {

							@Override
							public void dialogClick(String tag) {

								topInterface.topbarClick("back");
							}
						}, getResources().getString(R.string.InternetConnect));
				ds.show(getFragmentManager(), "");
				ds.setCancelable(false);
			} else {
				timer = new Timer();
				getDealDetailAsync();
			}

		}

		super.onActivityResult(requestCode, resultCode, data);

	}

}
