package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.CampaignListAdpater;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.CampaignDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.topBarInteface;

public class FragmentSliderCampaign extends Fragment {
	private ArrayList<CampaignDetail> arrCampaign = new ArrayList<CampaignDetail>();
	private ImageView imgSliderCampaignAdd, imgSliderCampaignQuestions;
	private CampaignListAdpater adapterCampaignList;
	private topBarInteface topInterface;
	private CommonData commonData;
	private String getUrl = "";
	private ListView lstcampain;
	private LinearLayout empty;
	private WebService_Call web_service = new WebService_Call();
	private String url_response = "";
	private String statusCode = "", messageResponse = "", strTotalCount = "";
	private TextView txtSliderCampaignPoints;
	RelativeLayout relmainfragment_slider_campaign;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_campaign,
				container, false);
		commonData = (CommonData) getActivity().getApplicationContext();
		initControls(view);
		clickEvents();
		if (commonData.checkInternetConnection()) {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {
						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInterface.topbarClick("Loading");

						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub

							try {
								getUrl = "CustomerService.svc/GetAllCampaigns?customerid="
										+ commonData.CustomerId
										+ "&languageid="
										+ commonData.languageId;
								url_response = web_service
										.makeServicegetRequest(getUrl);
								JSONObject josnObject = new JSONObject(
										url_response);
								statusCode = josnObject
										.getString("vStatusCode");
								messageResponse = josnObject
										.getString("vMessageResponse");

								if (statusCode.equals("100")) {
									strTotalCount = josnObject
											.getString("dCampaignUnusedAmount");
									JSONArray jsonArray = josnObject
											.getJSONArray("FriendCampaigns");
									for (int i = 0; i < jsonArray.length(); i++) {
										JSONObject jsonObject = jsonArray
												.getJSONObject(i);
										CampaignDetail campaign = new CampaignDetail();
										campaign.dConvertedDate = jsonObject
												.getString("dConvertedDate");
										campaign.dRewardAmount = jsonObject
												.getString("dRewardAmount");
										campaign.vCustomerFriendEmail = jsonObject
												.getString("vCustomerFriendEmail");
										campaign.dRewardUseLimitDate = jsonObject
												.getString("dRewardUseLimitDate");
										arrCampaign.add(campaign);
									}
								}
							} catch (Exception e) {
								messageResponse = getResources().getString(
										R.string.errormesssage);
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topInterface.hideDialog();

							if (!commonData.checkInternetConnection()) {
								DialogFragment dialogFragment = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub
												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.InternetConnect));
								dialogFragment.show(getFragmentManager(), "");
								dialogFragment.setCancelable(false);
							}

							else {
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topInterface
															.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("101")) {
									// viewempty.setVisibility(View.VISIBLE);
									empty.setVisibility(View.VISIBLE);
								} else if (statusCode.equals("100")) {

									txtSliderCampaignPoints
											.setText(strTotalCount);
									adapterCampaignList = new CampaignListAdpater(
											arrCampaign);
									lstcampain.setAdapter(adapterCampaignList);

								} else {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													// TODO Auto-generated
													// method
													// stub
													topInterface
															.topbarClick("back");
												}
											}, messageResponse);
									ds.show(getFragmentManager(), "");
								}

								// if (arrCampaign.size() == 0) {
								// txtSliderCampaignPoints.setText(getResources()
								// .getString(R.string.Campaign_Rewards));
								// }
								try {
									if (arrCampaign.size() == 0) {
										
										empty.setVisibility(View.VISIBLE);
										txtSliderCampaignPoints
												.setText(getResources()
														.getString(
																R.string.Campaign_Rewards));
									}
								} catch (Exception e) {
									// TODO: handle exception
								}
							}

						}
					});
			as.execute();
		} else {
			DialogFragment dialogFragment = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			dialogFragment.show(getFragmentManager(), "");
			dialogFragment.setCancelable(false);
		}
		return view;
	}

	private void initControls(View view) {

		relmainfragment_slider_campaign = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_campaign);
		imgSliderCampaignAdd = (ImageView) view
				.findViewById(R.id.imgSliderCampaignAdd);
		imgSliderCampaignQuestions = (ImageView) view
				.findViewById(R.id.imgSliderCampaignQuestions);
		txtSliderCampaignPoints = (TextView) view
				.findViewById(R.id.txtSliderCampaignPoints);
		empty = (LinearLayout) view.findViewById(R.id.empty);
		lstcampain = (ListView) view.findViewById(R.id.listcampain);
	}

	private void clickEvents() {

		relmainfragment_slider_campaign
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});

		imgSliderCampaignAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("campaignAdd");
			}
		});
		imgSliderCampaignQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("questions");
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

}
