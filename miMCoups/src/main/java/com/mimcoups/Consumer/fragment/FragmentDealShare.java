package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.DialogFragment.ListDialog;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.FontFitTextView;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.squareup.picasso.Picasso;

public class FragmentDealShare extends Fragment {

	private ImageView imgDealShareBack, imgDealFeedImage, ivaddfriedtoshare;
	private TextView txtDealShareDone, txtfriend;
	private topBarInteface topInteface;
	private DealFeedDetail dealsliderPassDetail;
	private TextView txtDealName, txtDealType, txtDealDetail,
			txtDealFeedDistance;
	private CommonData commonData;
	private EditText ettosharetoshare_with_friends;
	private WebService_Call webService = new WebService_Call();
	private String url_response = "", getUrl = "", vStatusCode = "",
			vMessageResponse = "";
	public static ArrayList<FriendsDetail> ListofFriendsEmailtoshare;
	private RelativeLayout relmainfragment_sharedeal_deal;
	private LinearLayout lnaddfriendsshare;
	FontFitTextView txttotalprice, txttotalprice_2;
	long MIN_CLICK_INTERVAL = 1000; // in millis
	long lastClickTime = 0;
	TextView txtrow;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_sharedeal_deal,
				container, false);
		dealsliderPassDetail = (DealFeedDetail) getArguments().getSerializable(
				"DealFeedDetail");
		initControls(view);
		Set_Data();
		clickEvents();
		return view;
	}

	private void Set_Data() {

		dealsliderPassDetail.shortDescription = dealsliderPassDetail.shortDescription
				.replace("\\n", "\n");
		dealsliderPassDetail.shortDescription = dealsliderPassDetail.shortDescription
				.replace(">", "●");
		txtDealName.setText(dealsliderPassDetail.StoreName);
		txtDealType.setText(dealsliderPassDetail.dealName);
		txtDealDetail.setText(dealsliderPassDetail.shortDescription);
		txtDealFeedDistance.setText(dealsliderPassDetail.dealDistanse + " MI");
		Picasso.with(getActivity()).load(dealsliderPassDetail.DealImageUrl)
				.into(imgDealFeedImage);

		if (dealsliderPassDetail.dealDiscountPrice.trim().toString()
				.contains("$0")
				|| dealsliderPassDetail.dealDiscountPrice.trim().toString()
						.contains("kr0")) {
			String price = "";

			if (dealsliderPassDetail.dealDiscountPrice.trim().toString()
					.contains("$0")) {

				price = dealsliderPassDetail.dealDiscountPrice.trim()
						.toString().replace("$", "").trim();

			} else if (dealsliderPassDetail.dealDiscountPrice.trim().toString()
					.contains("kr0")) {
				price = dealsliderPassDetail.dealDiscountPrice.trim()
						.toString().replace("kr", "").trim();

			}

			if (Double.parseDouble(price) > 0) {
				txtrow.setVisibility(View.VISIBLE);
				txttotalprice.setVisibility(View.VISIBLE);

				txttotalprice_2.setText(""
						+ dealsliderPassDetail.dealDiscountPrice);
				txttotalprice.setText(dealsliderPassDetail.DealOrignalPrice);
				txttotalprice.setPaintFlags(txttotalprice.getPaintFlags()
						| Paint.STRIKE_THRU_TEXT_FLAG);

			} else {

				txtrow.setVisibility(View.GONE);
				txttotalprice.setVisibility(View.GONE);
				txttotalprice_2.setText(getActivity().getString(
						R.string.txtdealprice));
			}

		} else {

			txtrow.setVisibility(View.VISIBLE);
			txttotalprice.setVisibility(View.VISIBLE);

			txttotalprice_2
					.setText("" + dealsliderPassDetail.dealDiscountPrice);
			txttotalprice.setText(dealsliderPassDetail.DealOrignalPrice);
			txttotalprice.setPaintFlags(txttotalprice.getPaintFlags()
					| Paint.STRIKE_THRU_TEXT_FLAG);

		}
	}

	private void initControls(View view) {

		ListofFriendsEmailtoshare = new ArrayList<FriendsDetail>();
		commonData = (CommonData) getActivity().getApplicationContext();
		relmainfragment_sharedeal_deal = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_sharedeal_deal);
		imgDealShareBack = (ImageView) view.findViewById(R.id.imgDealShareBack);
		txtDealShareDone = (TextView) view.findViewById(R.id.txtDealShareDone);
		lnaddfriendsshare = (LinearLayout) view
				.findViewById(R.id.lnaddfriendsshare);
		ivaddfriedtoshare = (ImageView) view
				.findViewById(R.id.ivaddfriedtoshare);
		txtfriend = (TextView) view.findViewById(R.id.txtfriend);
		ettosharetoshare_with_friends = (EditText) view
				.findViewById(R.id.ettosharetoshare_with_friends);
		txttotalprice_2 = (FontFitTextView) view
				.findViewById(R.id.txttotalprice_2);
		txtrow = (TextView) view.findViewById(R.id.txtrow);
		// /////
		imgDealFeedImage = (ImageView) view.findViewById(R.id.imgDealFeedImage);
		txtDealName = (TextView) view.findViewById(R.id.txtDealName);
		txtDealType = (TextView) view.findViewById(R.id.txtDealType);
		txtDealDetail = (TextView) view.findViewById(R.id.txtDealDetail);
		txttotalprice = (FontFitTextView) view.findViewById(R.id.txttotalprice);
		txtDealFeedDistance = (TextView) view
				.findViewById(R.id.txtDealFeedDistance);

	}

	private void clickEvents() {

		relmainfragment_sharedeal_deal
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});

		lnaddfriendsshare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (!commonData.checkInternetConnection()) {
						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										topInteface.topbarClick("back");
									}
								}, getResources().getString(
										R.string.InternetConnect));
						dsp.show(getFragmentManager(), "");
						dsp.setCancelable(false);
					} else {

						DialogFragment listdialid = new ListDialog(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {

										if (tag.equals("OK")) {

											if (ListofFriendsEmailtoshare
													.size() > 0) {

												txtfriend.setText(""
														+ ListofFriendsEmailtoshare
																.get(0).freindsName);
												for (int i = 1; i < ListofFriendsEmailtoshare
														.size(); i++) {
													txtfriend
															.append(","
																	+ ListofFriendsEmailtoshare
																			.get(i).freindsName);
												}
											} else {

												txtfriend.setText("");
												// DialogFragment dsp = new
												// SingleButtonAlert(
												// new DialogInterfaceClick() {
												//
												// @Override
												// public void dialogClick(
												// String tag) {
												// }
												// },
												// getResources()
												// .getString(
												// R.string.nofriendsselected));
												// dsp.setCancelable(false);
												// dsp.show(getFragmentManager(),
												// "");

											}

										} else {

										}
									}

								}, topInteface, "deal");

						listdialid.setCancelable(false);

						listdialid.show(getFragmentManager(), "");
					}
				}
			}
		});

		imgDealShareBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				topInteface.topbarClick("back");
			}
		});
		txtDealShareDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!commonData.checkInternetConnection()) {

					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									// method stub
									topInteface.topbarClick("back");
								}
							}, getResources().getString(
									R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				} else if (ListofFriendsEmailtoshare.size() == 0) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									// method stub
								}
							}, getResources().getString(
									R.string.nofriendsselected));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				}
				// else if (ettosharetoshare_with_friends.getText().toString()
				// .trim().length() == 0) {
				//
				// DialogFragment ds = new SingleButtonAlert(
				// new DialogInterfaceClick() {
				//
				// @Override
				// public void dialogClick(String tag) {
				//
				// // method stub
				// }
				// }, getResources().getString(R.string.etvalidation));
				// ds.show(getFragmentManager(), "");
				// ds.setCancelable(false);
				//
				// }
				else {
					Run_Asyn_To_Share_Deel_With_MimCup();
				}

			}
		});
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@Override
	public void onStop() {

		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		topInteface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {

		super.onDetach();
	}

	private void Run_Asyn_To_Share_Deel_With_MimCup() {
		final JSONObject jsonMain = new JSONObject();
		JSONArray array = new JSONArray();

		for (int i = 0; i < ListofFriendsEmailtoshare.size(); i++) {
			JSONObject temPObjects = new JSONObject();
			try {
				temPObjects.put("iFriendId",
						ListofFriendsEmailtoshare.get(i).friendsId);
				array.put(temPObjects);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// //////////////
		// RUN BACKGROUND
		try {
			jsonMain.put("iLanguageId", commonData.languageId);
			jsonMain.put("iCustomerId", commonData.CustomerId);
			jsonMain.put("iLocationId", dealsliderPassDetail.locationId);
			jsonMain.put("iMerchantDealId", dealsliderPassDetail.DealId);
			jsonMain.put("vMessage", ""
					+ ettosharetoshare_with_friends.getText().toString());
			jsonMain.put("SharewithFriends", array);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						//
						topInteface.showDialog("Loading");
					}

					@Override
					public void doInBackground() {
						//
						try {

							getUrl = "DealService.svc/ShareDealViaMIMS";
							url_response = webService
									.makeServiceShareViaMimCup(getUrl,
											jsonMain.toString());
							JSONObject json_object = new JSONObject(
									url_response);

							vStatusCode = json_object.getString("vStatusCode");
							vMessageResponse = json_object
									.getString("vMessageResponse");
						} catch (Exception e) {
							// TODO Auto-generated catch block

							vMessageResponse = getResources().getString(
									R.string.errormesssage);

						}
					}

					@Override
					public void onPostExecute() {
						topInteface.hideDialog();

						if (vStatusCode.equals("420")) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInteface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.PoorInternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						} else if (vStatusCode.equals("100")) {

							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											ettosharetoshare_with_friends
													.setText("");
											txtfriend.setText("");

											topInteface.topbarClick("back");

										}
									}, vMessageResponse);
							dsp.show(getFragmentManager(), "");
							dsp.setCancelable(false);
						} else {
							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											topInteface.topbarClick("back");

										}
									}, vMessageResponse);
							dsp.show(getFragmentManager(), "");
							dsp.setCancelable(false);
						}

					}
				});
		as.execute();

	}
}
