package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.SearchResultLocationListAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.MyItem;
import com.mimcoups.Consumer.customClass.Person;
import com.mimcoups.Consumer.customClass.SearchResultLocationDealCategory;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.LocationListInterface;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;

public class FragmentSearchResult extends Fragment implements
		ClusterManager.OnClusterClickListener<MyItem>,
		ClusterManager.OnClusterInfoWindowClickListener<MyItem>,
		ClusterManager.OnClusterItemClickListener<MyItem>,
		ClusterManager.OnClusterItemInfoWindowClickListener<MyItem>,
		OnMarkerClickListener {

	int preLast = 0;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	private GoogleMap googleMap;
	private ListView lstSearchListLocations;
	private ImageView imgSearchResultback, ImgSearchResultQuestions;
	private topBarInteface topInterface;
	private SearchResultLocationListAdapter adapterSearchResultLocations;
	private ArrayList<SearchResultLocationDealCategory> arrSearchMapLocation = new ArrayList<SearchResultLocationDealCategory>();
	public TextView txtSearchResultCategoryName, txtSearchResultName;
	String seachfrom, searchkeyword;
	public CommonData commonClass;
	private String strGeturl = "", strStatusCode = "", strMessageResponse,
			urlResponse = "", StrPageIndex = "1", strLastRunDate = "";
	private WebService_Call web_service = new WebService_Call();
	String iNextIndex = "", iLastIndex = "", vType = "";
	private ClusterManager<Person> mClusterManager;
	LocationListInterface locationlistinterface;
	DealFeedDetailInterface dealfeeddetailinterface;
	boolean loadmore = true;
	private Random mRandom = new Random(1984);
	RelativeLayout relmainfragment_search_result;
	LinearLayout lnLoadMore;
	View views;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_search_result,
				container, false);

		seachfrom = this.getArguments().getString("search");
		searchkeyword = this.getArguments().getString("searchkeyword");
		initControls(view);
		views = inflater.inflate(R.layout.loadmore, null);
		// lstSearchListLocations.addFooterView(views);
		Loadmore_Functionality();

		clickEvents();

		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			blankAdapter();

			getdata();
		}

		if (googleMap == null) {

			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();

			mClusterManager = new ClusterManager<Person>(getActivity(),
					googleMap);
			mClusterManager.setRenderer(new PersonRenderer());
			googleMap.setOnCameraChangeListener(mClusterManager);
			googleMap.setOnMarkerClickListener(this);
			googleMap
					.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

						@Override
						public void onInfoWindowClick(Marker arg0) {
							// TODO Auto-generated method stub

							String title = arg0.getTitle();

							char[] charr = title.toCharArray();

							char character = charr[0];

							int ascii = (int) character;

							int position = ascii - 65;

							// Character.getNumericValue(title);

							lstSearchListLocations.setSelection(position);

						}
					});

			// addItems();
			// mClusterManager.cluster();

		}

		return view;
	}

	private void initControls(View view) {
		commonClass = (CommonData) getActivity().getApplicationContext();

		relmainfragment_search_result = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_search_result);
		imgSearchResultback = (ImageView) view
				.findViewById(R.id.imgSearchResultback);
		ImgSearchResultQuestions = (ImageView) view
				.findViewById(R.id.ImgSearchResultQuestions);
		lstSearchListLocations = (ListView) view
				.findViewById(R.id.lstSearchListLocations);
		txtSearchResultCategoryName = (TextView) view
				.findViewById(R.id.txtSearchResultCategoryName);
		txtSearchResultName = (TextView) view
				.findViewById(R.id.txtSearchResultName);
		lnLoadMore = (LinearLayout) view.findViewById(R.id.lnLoadMore);

		txtSearchResultCategoryName.setText(getString(R.string.SearchTitle)
				+ " " + searchkeyword);
	}

	private void clickEvents() {

		relmainfragment_search_result.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		imgSearchResultback.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				topInterface.topbarClick("back");
			}
		});

		ImgSearchResultQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					topInterface.topbarClick("questions");
				}
			}
		});

	}

	private void getdata() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						if (StrPageIndex.equalsIgnoreCase("0")) {
							topInterface.showDialog("Loading");
						}
					}

					@Override
					public void doInBackground() {

						double latitude = commonClass.currentLatitude;
						double longiutde = commonClass.currentLongitude;

						// ArrayList<Double> arrLatitude = ((DeelFeedActivity)
						// getActivity())
						// .getLatitudeLongitude();
						//
						// latitude = arrLatitude.get(0);
						// longiutde = arrLatitude.get(1);

						String strTempUrl = "DealService.svc/GlobalSearchResult?languageid="
								+ commonClass.languageId
								+ "&customerid="
								+ commonClass.CustomerId
								+ "&keyword="
								+ searchkeyword
								+ "&latitude="
								+ latitude
								+ "&longitude="
								+ longiutde
								+ "&startindex="
								+ StrPageIndex + "&type=" + seachfrom;
						strTempUrl = commonClass.Url_EncodeWithBlank(strTempUrl
								.replace("%", "%25"));
						// Log.e("SearchREsult==	", strTempUrl);
						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);
						Log.e("called searchresult", urlResponse);
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub

						lnLoadMore.setVisibility(View.GONE);
						// try {
						// lstSearchListLocations.removeFooterView(views);
						// } catch (Exception e) {
						// // TODO: handle exception
						// }

						topInterface.hideDialog();
						try {

							JSONObject json_Object = new JSONObject(urlResponse);

							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							strStatusCode = json_Object
									.getString("vStatusCode");
							iNextIndex = json_Object.getString("iNextIndex");
							StrPageIndex = json_Object.getString("iLastIndex");
							vType = json_Object.getString("vType");

							if (StrPageIndex.equalsIgnoreCase("0")) {
								loadmore = false;
							}

							strMessageResponse = json_Object
									.getString("vMessageResponse");

							if (strStatusCode.equals("420")) {
								loadmore = false;
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (strStatusCode.equals("100")) {
								loadmore = true;

								// lstSearchListLocations.addFooterView(views);

								// List<MyItem> items = new ArrayList<MyItem>();
								for (int i = 0; i < json_result.length(); i++) {

									JSONObject json_deal = json_result
											.getJSONObject(i);
									SearchResultLocationDealCategory s = new SearchResultLocationDealCategory();

									s.iMerchantDealId = json_deal
											.getString("iMerchantDealId");
									s.vName = json_deal.getString("vName");

									s.dDealDiscountedAmount = json_deal
											.getString("dDealDiscountedAmount");
									s.vMerchantStoreMapImage_A_90X90 = json_deal
											.getString("vMerchantStoreMapImage_A_90X90");
									s.vMerchantStoreMapImage_I_60X60 = json_deal
											.getString("vMerchantStoreMapImage_I_60X60");
									s.dLatitude = json_deal
											.getString("dLatitude");
									s.dLongitude = json_deal
											.getString("dLongitude");
									s.iMerchantStoreId = json_deal
											.getString("iMerchantStoreId");
									s.iCategoryId = json_deal
											.getString("iCategoryId");
									s.iRowNumber = json_deal
											.getString("iRowNumber");
									s.iNextIndex = json_deal
											.getString("iNextIndex");
									s.pinLocation = "" + i;

									double latitude = Double
											.valueOf(s.dLatitude);
									double longitude = Double
											.valueOf(s.dLongitude);

									// Log.e("LatLong", "" + latitude + "==="
									// + longitude);

									arrSearchMapLocation.add(s);

									int j = Integer.valueOf(i) + 65;

									LatLng latlng = new LatLng(Double
											.valueOf(s.dLatitude), Double
											.valueOf(s.dLongitude));
									mClusterManager.addItem(new Person(latlng,
											Character.toString((char) j),
											R.drawable.icon_map_pin));

									adapterSearchResultLocations
											.notifyDataSetChanged();

								}

								if (arrSearchMapLocation.size() > 0) {

									Log.e("called if", "If call");
									googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
											new LatLng(
													Double.parseDouble(arrSearchMapLocation
															.get(0).dLatitude),
													Double.parseDouble(arrSearchMapLocation
															.get(0).dLongitude)),
											7.5f));
								}
								mClusterManager.cluster();
							} else {

								if (arrSearchMapLocation.size() == 0) {

									googleMap.moveCamera(CameraUpdateFactory
											.newLatLngZoom(
													new LatLng(
															commonClass.currentLatitude,
															commonClass.currentLongitude),
													5f));
								}

							}
						} catch (Exception e) {
						}
					}
				});

		as.execute();

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		MapFragment f = (MapFragment) getFragmentManager().findFragmentById(
				R.id.map);
		try {
			if (f != null)
				getFragmentManager().beginTransaction().remove(f).commit();
		} catch (Exception e) {

		}

	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
		locationlistinterface = (LocationListInterface) activity;
		dealfeeddetailinterface = (DealFeedDetailInterface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onClusterItemInfoWindowClick(MyItem item) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onClusterItemClick(MyItem item) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onClusterInfoWindowClick(Cluster<MyItem> cluster) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onClusterClick(Cluster<MyItem> cluster) {
		// TODO Auto-generated method stub
		return false;
	}

	private void addItems() {
		// // http://www.flickr.com/photos/sdasmarchives/5036248203/
		mClusterManager.addItem(new Person(position(), "1",
				R.drawable.icon_map_pin));
		mClusterManager.addItem(new Person(position(), "2",
				R.drawable.icon_map_pin));
		mClusterManager.addItem(new Person(position(), "3",
				R.drawable.icon_map_pin));
		mClusterManager.addItem(new Person(position(), "4",
				R.drawable.icon_map_pin));
		mClusterManager.addItem(new Person(position(), "5",
				R.drawable.icon_map_pin));
		mClusterManager.addItem(new Person(position(), "6",
				R.drawable.icon_map_pin));

	}

	private LatLng position() {
		return new LatLng(random(51.6723432, 51.38494009999999), random(
				0.148271, -0.3514683));
	}

	private double random(double min, double max) {
		return mRandom.nextDouble() * (max - min) + min;
	}

	private class PersonRenderer extends DefaultClusterRenderer<Person> {
		private final IconGenerator mIconGenerator = new IconGenerator(
				getActivity().getApplicationContext());
		private final IconGenerator mClusterIconGenerator = new IconGenerator(
				getActivity().getApplicationContext());
		private final ImageView mImageView;
		private final ImageView mClusterImageView;

		// private final int mDimension;

		public PersonRenderer() {
			super(getActivity().getApplicationContext(), googleMap,
					mClusterManager);

			View multiProfile = getActivity().getLayoutInflater().inflate(
					R.layout.multi_profile, null);
			mClusterIconGenerator.setContentView(multiProfile);
			mClusterImageView = (ImageView) multiProfile
					.findViewById(R.id.image);

			mImageView = new ImageView(getActivity().getApplicationContext());

			// mDimension = (int) getResources().getDimension(
			// R.dimen.custom_profile_image);
			// mImageView.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
			// int padding = (int) getResources().getDimension(
			// R.dimen.custom_profile_padding);
			// mImageView.setPadding(padding, padding, padding, padding);

			mIconGenerator.setContentView(mImageView);
		}

		@Override
		protected void onBeforeClusterItemRendered(Person person,
				MarkerOptions markerOptions) {
			// Draw a single person.
			// Set the info window to show their name.
			try {
				View marker = ((LayoutInflater) getActivity().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE)).inflate(
						R.layout.custom_marker_layout, null);
				final TextView numTxt = (TextView) marker
						.findViewById(R.id.num_txt);
				numTxt.setText(person.name);
				LinearLayout linerpin = (LinearLayout) marker
						.findViewById(R.id.linerpin);
				mImageView.setImageResource(person.profilePhoto);
				Bitmap icon = mIconGenerator.makeIcon();

				markerOptions.icon(
						BitmapDescriptorFactory
								.fromBitmap(createDrawableFromView(
										getActivity(), marker))).title(
						person.name);
			} catch (Exception e) {

			}

			// markerOptions.icon(BitmapDescriptorFactory.fromResource(icon).title(
			// person.name);

		}

		@Override
		protected void onBeforeClusterRendered(Cluster<Person> cluster,
				MarkerOptions markerOptions) {
			// Draw multiple people.
			// Note: this method runs on the UI thread. Don't spend too much
			// time in here (like in this example).
			// List<Drawable> profilePhotos = new
			// ArrayList<Drawable>(Math.min(4,
			// cluster.getSize()));
			// int width = mDimension;
			// int height = mDimension;
			//
			// for (Person p : cluster.getItems()) {
			// // Draw 4 at most.
			// if (profilePhotos.size() == 4)
			// break;
			// Drawable drawable = getResources().getDrawable(p.profilePhoto);
			// drawable.setBounds(0, 0, width, height);
			// profilePhotos.add(drawable);
			// }
			// MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
			// multiDrawable.setBounds(0, 0, width, height);

			// mClusterImageView.setImageDrawable(multiDrawable);
			Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster
					.getSize()));
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
		}

		@Override
		protected boolean shouldRenderAsCluster(Cluster cluster) {
			// Always render clusters.
			return cluster.getSize() > 1;
		}
	}

	private void Load_More() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
					}

					@Override
					public void doInBackground() {
						double latitude = commonClass.currentLatitude;
						double longiutde = commonClass.currentLongitude;

						ArrayList<Double> arrLatitude = ((DeelFeedActivity) getActivity())
								.getLatitudeLongitude();

						latitude = arrLatitude.get(0);
						longiutde = arrLatitude.get(1);
						String strTempUrl = "DealService.svc/GlobalSearchResult?languageid="
								+ commonClass.languageId
								+ "&keyword=enjoy"
								+ "&latitude="
								+ latitude
								+ "&longitude="
								+ longiutde
								+ "&startindex="
								+ StrPageIndex
								+ "&type=deal";

						strTempUrl = commonClass
								.Url_EncodeWithBlank(strTempUrl);
						// Log.e("SearchREsult====", strTempUrl);

						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);

					}

					@Override
					public void onPostExecute() {

						preLast = 0;

						try {

							JSONObject json_Object = new JSONObject(urlResponse);

							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							strStatusCode = json_Object
									.getString("vStatusCode");

							if (strStatusCode.equals("100")) {

								iNextIndex = json_Object
										.getString("iNextIndex");
								iLastIndex = json_Object
										.getString("iLastIndex");
								vType = json_Object.getString("vType");

								strMessageResponse = json_Object
										.getString("vMessageResponse");

								loadmore = true;
								// List<MyItem> items = new ArrayList<MyItem>();

								for (int i = 0; i < json_result.length(); i++) {

									JSONObject json_deal = json_result
											.getJSONObject(i);
									SearchResultLocationDealCategory s = new SearchResultLocationDealCategory();

									s.iMerchantDealId = json_deal
											.getString("iMerchantDealId");
									s.vName = json_deal.getString("vName");

									s.dDealDiscountedAmount = json_deal
											.getString("dDealDiscountedAmount");
									s.vMerchantStoreMapImage_A_90X90 = json_deal
											.getString("vMerchantStoreMapImage_A_90X90");
									s.vMerchantStoreMapImage_I_60X60 = json_deal
											.getString("vMerchantStoreMapImage_I_60X60");
									s.dLatitude = json_deal
											.getString("dLatitude");
									s.dLongitude = json_deal
											.getString("dLongitude");
									s.iMerchantStoreId = json_deal
											.getString("iMerchantStoreId");
									s.iCategoryId = json_deal
											.getString("iCategoryId");
									s.iRowNumber = json_deal
											.getString("iRowNumber");
									s.iNextIndex = json_deal
											.getString("iNextIndex");
									s.pinLocation = "" + i;

									double latitude = Double
											.valueOf(s.dLatitude);
									double longitude = Double
											.valueOf(s.dLongitude);

									arrSearchMapLocation.add(s);

									adapterSearchResultLocations
											.notifyDataSetChanged(); // googleMap
								}

							}
						} catch (Exception e) {
						}
						loadmore = true;
					}
				});
		as.execute();
	}

	private void Loadmore_Functionality() {
		lstSearchListLocations.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

				final int lastItem = firstVisibleItem + visibleItemCount;

				if (lastItem == totalItemCount) {

					if ((preLast != lastItem) && loadmore) {

						if (!commonClass.checkInternetConnection()) {

						} else {
							lnLoadMore.setVisibility(View.VISIBLE);

							// lstSearchListLocations.addFooterView(views);
							loadmore = false;

							getdata();
							// Load_More();
						}
					}
				}

			}
		});

	}

	@Override
	public boolean onMarkerClick(Marker v) {
		// TODO Auto-generated method stub
		String title = v.getTitle();

		if (title != null) {
			char[] charr = title.toCharArray();

			char character = charr[0];

			int ascii = (int) character;

			int position = ascii - 65;

			// Character.getNumericValue(title);

			SearchResultLocationListAdapter.selected = position;
			adapterSearchResultLocations.notifyDataSetChanged();
			lstSearchListLocations.setSelection(position);
		}

		return true;

	}

	public void blankAdapter() {
		adapterSearchResultLocations = new SearchResultLocationListAdapter(
				getActivity(), arrSearchMapLocation, seachfrom,
				locationlistinterface, dealfeeddetailinterface);
		lstSearchListLocations.setAdapter(adapterSearchResultLocations);
	}

	public static Bitmap createDrawableFromView(Context context, View view) {

		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(displayMetrics);
		view.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
		view.layout(0, 0, displayMetrics.widthPixels,
				displayMetrics.heightPixels);
		view.buildDrawingCache();
		Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
				view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(bitmap);
		view.draw(canvas);

		return bitmap;

	}

}
