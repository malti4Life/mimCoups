package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mimcoups.Consumer.customClass.ChatUser;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.DiscussdealSendRequestInterface;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.DealShareDiscussAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.FontFitTextView;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.inteface.discusschatinteface;
import com.squareup.picasso.Picasso;

public class FragmentDiscussDeal extends Fragment implements
        DiscussdealSendRequestInterface {

	private ImageView imgDiscussDealQuestions, imgDiscussDealBack,
			imgmerchantstore;
	private topBarInteface topInterface;
	private ListView lstDiscussDeal;
	private ArrayList<ChatUser> arrAllChats = new ArrayList<ChatUser>();
	private DealShareDiscussAdapter adapter;
	private String serverResponse = "";
	private WebService_Call webservicecall = new WebService_Call();
	private CommonData globalClass;
	ArrayList<FriendsDetail> arrfriendsDetail = new ArrayList<FriendsDetail>();
	private TextView txtmerchantstorename;
	private RelativeLayout relMerchatChat;
	private CommonData commonData;
	private DealFeedDetail dealsliderPassDetail;
	private String strMerchantLogoUrl;
	private discusschatinteface discussChatinteface;
	LinearLayout headerbg, tempheaderbg, lnMainDiscuss;
	private WebService_Call webServiceCall = new WebService_Call();
	private DiscussdealSendRequestInterface discussdealinterfaceobj = this;
	private String url_response = "", statusCode, statusMessage = "",
			strStatusCode = "", strMessageResponse;
	topBarInteface topinterface;
	LinearLayout headerbgs;
	// TextView txtDeelFeed;
	RelativeLayout discussdealheader;
	RelativeLayout relamainfragment_discuss_deal;

	Context context;
	private ImageView imgDealFeedImage;
	private TextView txtDealFeedName, txtDealfeedType,
			txtDealfeedTypeDescription, txtDealfeedDescription,
			txtDealFeedDistance, txtrow;
	private View ListHeaderView;
	LinearLayout txtDiscussDealNoData;
	FontFitTextView txtdealfeedtotalprice, txtDealFeedDiscoutPrice;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	CommonData commonClass;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);

		View view = inflater.inflate(R.layout.fragment_discuss_deal, container,
				false);

		context = getActivity();
		commonClass = (CommonData) getActivity().getApplicationContext();

		commonClass.connectCount = 0;

		dealsliderPassDetail = (DealFeedDetail) getArguments().getSerializable(
				"DealFeedDetail");

		lstDiscussDeal = (ListView) view.findViewById(R.id.lstDiscussDeal);

		LayoutInflater inflate1 = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		ListHeaderView = inflate1.inflate(
				R.layout.fragment_discuss_deal_header, null);

		lstDiscussDeal.addHeaderView(ListHeaderView);

		adapter = new DealShareDiscussAdapter(getActivity(), arrfriendsDetail,
				discussdealinterfaceobj, context);

		lstDiscussDeal.setAdapter(adapter);

		initControls(ListHeaderView, view);
		clickEvents();
		((DeelFeedActivity) getActivity()).is_chatscreen_neddto_refresh = false;
		return view;

	}

	private void clickEvents() {

		discussdealheader.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			}
		});

		imgDiscussDealBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("back");
			}
		});
		imgDiscussDealQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("questions");
				}
			}
		});
		relMerchatChat.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				discussChatinteface.dicusschatclick(
						dealsliderPassDetail.DealImageUrl,
						dealsliderPassDetail.dealName);
			}
		});

		relamainfragment_discuss_deal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

	}

	private void initControls(View view, View mainview) {
		commonData = (CommonData) getActivity().getApplicationContext();
		relamainfragment_discuss_deal = (RelativeLayout) mainview
				.findViewById(R.id.relamainfragment_discuss_deal);
		txtDiscussDealNoData = (LinearLayout) view
				.findViewById(R.id.txtDiscussDealNoData);
		// txtDeelFeed = (TextView) mainview.findViewById(R.id.txtDeelFeed);
		// txtDeelFeed.setText("" + dealsliderPassDetail.vCategoryName);

		txtrow = (TextView) view.findViewById(R.id.txtrow);

		// ///
		imgDiscussDealBack = (ImageView) mainview
				.findViewById(R.id.imgDiscussDealBack);
		imgDiscussDealQuestions = (ImageView) mainview
				.findViewById(R.id.imgDiscussDealQuestions);
		imgmerchantstore = (ImageView) view.findViewById(R.id.imgmerchantstore);
		relMerchatChat = (RelativeLayout) view
				.findViewById(R.id.relMerchatChat);
		discussdealheader = (RelativeLayout) view
				.findViewById(R.id.discussdealheader);
		txtmerchantstorename = (TextView) view
				.findViewById(R.id.txtmerchantstorename);
		imgmerchantstore = (ImageView) view.findViewById(R.id.imgmerchantstore);

		headerbg = (LinearLayout) view.findViewById(R.id.headerbg);
		tempheaderbg = (LinearLayout) view.findViewById(R.id.headerbgs);
		lnMainDiscuss = (LinearLayout) view.findViewById(R.id.lnMainDiscuss);

		imgDealFeedImage = (ImageView) view.findViewById(R.id.imgDealFeedImage);
		txtDealFeedName = (TextView) view.findViewById(R.id.txtDealFeedName);
		txtDealfeedType = (TextView) view.findViewById(R.id.txtDealfeedType);
		txtDealfeedTypeDescription = (TextView) view
				.findViewById(R.id.txtDealfeedTypeDescription);
		txtDealfeedDescription = (TextView) view
				.findViewById(R.id.txtDealfeedDescription);
		txtdealfeedtotalprice = (FontFitTextView) view
				.findViewById(R.id.txtdealfeedtotalprice);
		txtDealFeedDiscoutPrice = (FontFitTextView) view
				.findViewById(R.id.txtDealFeedDiscoutPrice);
		txtDealFeedDistance = (TextView) view
				.findViewById(R.id.txtDealFeedDistance);
		globalClass = (CommonData) getActivity().getApplicationContext();
		if (dealsliderPassDetail.DealImageUrl != null) {
			Picasso.with(context).load(dealsliderPassDetail.DealImageUrl)
					.into(imgDealFeedImage);
		}

		txtDealFeedName.setText(dealsliderPassDetail.StoreName);
		txtDealfeedType.setText(dealsliderPassDetail.dealName);
		txtDealfeedTypeDescription
				.setText(dealsliderPassDetail.shortDescriptionHeading);

		dealsliderPassDetail.shortDescription = dealsliderPassDetail.shortDescription
				.replace(">", "●");

		dealsliderPassDetail.shortDescription = dealsliderPassDetail.shortDescription
				.replace("\\n", "\n");

		txtDealfeedDescription.setText(dealsliderPassDetail.shortDescription);

		txtDealFeedDistance.setText(dealsliderPassDetail.dealDistanse + " MI");

		if (dealsliderPassDetail.dealDiscountPrice.trim().toString()
				.contains("$0")
				|| dealsliderPassDetail.dealDiscountPrice.trim().toString()
						.contains("kr0")) {
			String price = "";

			if (dealsliderPassDetail.dealDiscountPrice.trim().toString()
					.contains("$0")) {

				price = dealsliderPassDetail.dealDiscountPrice.trim()
						.toString().replace("$", "").trim();

			} else if (dealsliderPassDetail.dealDiscountPrice.trim().toString()
					.contains("kr0")) {
				price = dealsliderPassDetail.dealDiscountPrice.trim()
						.toString().replace("kr", "").trim();

			}

			if (Double.parseDouble(price) > 0) {
				

				txtrow.setVisibility(View.VISIBLE);
				txtdealfeedtotalprice.setVisibility(View.VISIBLE);

				txtdealfeedtotalprice
						.setText(dealsliderPassDetail.DealOrignalPrice);

				txtdealfeedtotalprice.setPaintFlags(txtdealfeedtotalprice
						.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

				txtDealFeedDiscoutPrice
						.setText(dealsliderPassDetail.dealDiscountPrice);

			} else {
				txtrow.setVisibility(View.GONE);
				txtdealfeedtotalprice.setVisibility(View.GONE);
				txtDealFeedDiscoutPrice.setText(getActivity().getResources()
						.getString(R.string.txtdealprice));
			}

			

		} else {

			txtrow.setVisibility(View.VISIBLE);
			txtdealfeedtotalprice.setVisibility(View.VISIBLE);

			txtdealfeedtotalprice
					.setText(dealsliderPassDetail.DealOrignalPrice);

			txtdealfeedtotalprice.setPaintFlags(txtdealfeedtotalprice
					.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

			txtDealFeedDiscoutPrice
					.setText(dealsliderPassDetail.dealDiscountPrice);

		}

		if (!globalClass.checkInternetConnection()) {

			headerbg.setVisibility(View.GONE);
			tempheaderbg.setVisibility(View.GONE);
			relMerchatChat.setVisibility(View.GONE);
			lnMainDiscuss.setVisibility(View.GONE);
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub

						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {
						JSONArray js_arr_data;

						String vLocationName = "";

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							if (!DeelFeedActivity.progressbarshowing) {
								topInterface.showDialog("Loading");
							}

						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub

							String serverUrl = "CustomerService.svc/GetAllPurchasersAndMerchantsForDealDiscuss?customerid="
									+ commonData.CustomerId
									+ "&languageid="
									+ commonData.languageId
									+ "&dealid="
									+ dealsliderPassDetail.DealId
									+ "&locationid="
									+ dealsliderPassDetail.locationId;
							// Log.e("Discuss Deal", serverUrl);

							serverResponse = webservicecall
									.makeServicegetRequest(serverUrl);
							try {

								JSONObject js_getres = new JSONObject(
										serverResponse);

								statusCode = js_getres.getString("vStatusCode");

								if (statusCode.equals("100")
										|| statusCode.equals("101")) {

									arrfriendsDetail = new ArrayList<FriendsDetail>();

									strMerchantLogoUrl = js_getres
											.getString("vStoreListImage_A_90X90");

									vLocationName = js_getres
											.getString("vLocationName");

									js_arr_data = js_getres
											.getJSONArray("Customers");

									for (int i = 0; i < js_arr_data.length(); i++) {
										FriendsDetail fs = new FriendsDetail();
										JSONObject jsobj = js_arr_data
												.getJSONObject(i);
										fs.friendsId = jsobj
												.getString("iCustomerId");

										fs.freindsName = jsobj
												.getString("vCustomerName");

										fs.profileImageName = jsobj
												.getString("vProfileImageName");

										fs.userName = jsobj
												.getString("vUserName");

										fs.isfriend = jsobj
												.getBoolean("IsFriend");

										arrfriendsDetail.add(fs);

									}

									globalClass.arrRpListStore = new ArrayList<String>();
									JSONArray js_merchantarr_data = js_getres
											.getJSONArray("Merchants");

									for (int i = 0; i < js_merchantarr_data
											.length(); i++) {

										JSONObject jsobj = js_merchantarr_data
												.getJSONObject(i);

										String vUserName = jsobj
												.getString("vUserName");

										globalClass.arrRpListStore
												.add(vUserName);

									}

								} else {
									statusMessage = js_getres
											.getString("vMessageResponse");
								}

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub

							topInterface.hideDialog();

							if (!globalClass.checkInternetConnection()) {

								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub

											}
										}, getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							} else {

								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													// topInterface
													// .topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {

									headerbg.setVisibility(View.VISIBLE);
									tempheaderbg.setVisibility(View.VISIBLE);
									relMerchatChat.setVisibility(View.VISIBLE);
									lnMainDiscuss.setVisibility(View.VISIBLE);
									txtDiscussDealNoData
											.setVisibility(View.GONE);

									txtmerchantstorename.setText(vLocationName);

									adapter = new DealShareDiscussAdapter(
											getActivity(), arrfriendsDetail,
											discussdealinterfaceobj, context);
									lstDiscussDeal.setAdapter(adapter);
									Picasso.with(getActivity())
											.load(strMerchantLogoUrl)
											.into(imgmerchantstore);
								} else if (statusCode.equals("101")) {

									if (globalClass.arrRpListStore.size() == 0) {
										lnMainDiscuss.setVisibility(View.GONE);
										tempheaderbg.setVisibility(View.GONE);

										// tempheaderbg.setVisibility(View.GONE);
										// txtDiscussDealNoData
										// .setVisibility(View.VISIBLE);
										// tempheaderbg.setVisibility(View.GONE);
										// txtmerchantstorename.setText(vLocationName);

									} else {
										tempheaderbg.setVisibility(View.GONE);
										txtDiscussDealNoData
												.setVisibility(View.VISIBLE);

										txtmerchantstorename
												.setText(vLocationName);
									}

									if (arrfriendsDetail.size() == 0) {
										txtDiscussDealNoData
												.setVisibility(View.GONE);

									}

									Picasso.with(getActivity())
											.load(strMerchantLogoUrl)
											.into(imgmerchantstore);
								} else {
									tempheaderbg.setVisibility(View.GONE);
									lnMainDiscuss.setVisibility(View.GONE);
								}
							}

						}
					});
			as.execute();
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
		discussChatinteface = (discusschatinteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void discussdealsendRequest(final FriendsDetail friendsdetail) {
		// TODO Auto-generated method stub

		if (!commonData.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			Toast.makeText(getActivity(), "called", Toast.LENGTH_SHORT).show();
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInterface.showDialog("Loading");
							// arrFriends.clear();
						}

						@Override
						public void doInBackground() {
							String getUrl = "FriendService.svc/SendFriendRequest?languageid="
									+ commonData.languageId
									+ "&customerid="
									+ commonData.CustomerId
									+ "&friendid="
									+ friendsdetail.friendsId;

							// Log.d("url>>>", "" + getUrl);
							url_response = webServiceCall
									.makeServicegetRequest(getUrl);
							try {
								JSONObject jsonObject = new JSONObject(
										url_response);
								strStatusCode = jsonObject
										.getString("vStatusCode");
								strMessageResponse = jsonObject
										.getString("vMessageResponse");

							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topInterface.hideDialog();
							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (strStatusCode.equals("100")) {
								Toast.makeText(getActivity(),
										"" + strMessageResponse, 9000).show();
							} else {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO
												// Auto-generated
												// method stub
											}
										}, strMessageResponse);
								dsp.show(getFragmentManager(), "");
							}
						}
					});
			as.execute();
		}
	}
}
