package com.mimcoups.Consumer.fragment;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.squareup.picasso.Picasso;

public class FragmentSLiderLocationContact extends Fragment {
	private ImageView imgLocationContactBack, imgLocationContactQuestion;
	private Button btnLocationContactSubmit;
	private EditText edtLocationContactEmailAddress,
			edtLocationContactUsMessage;
	private topBarInteface topInterface;
	private CommonData commonData;
	private WebService_Call webService = new WebService_Call();
	private String url_response = "", strStatusCode = "",
			strMessageResponse = "";
	LocationList LocationsliderPassDetail;
	ImageView locationlogo;
	TextView txtLocationContactLocationName;

	RelativeLayout relmainfragment_sliderlocationcontact;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_sliderlocationcontact,
				container, false);
		LocationsliderPassDetail = (LocationList) getArguments()
				.getSerializable("locationlist");
		initControls(view);
		clickEvents();
		return view;
	}

	private void initControls(View view) {

		commonData = (CommonData) getActivity().getApplicationContext();

		txtLocationContactLocationName = (TextView) view
				.findViewById(R.id.txtLocationContactLocationName);
		relmainfragment_sliderlocationcontact = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_sliderlocationcontact);

		locationlogo = (ImageView) view.findViewById(R.id.locationlogo);
		imgLocationContactBack = (ImageView) view
				.findViewById(R.id.imgLocationContactBack);
		imgLocationContactQuestion = (ImageView) view
				.findViewById(R.id.imgLocationContactQuestion);
		btnLocationContactSubmit = (Button) view
				.findViewById(R.id.btnLocationContactSubmit);
		edtLocationContactEmailAddress = (EditText) view
				.findViewById(R.id.edtLocationContactEmailAddress);
		edtLocationContactUsMessage = (EditText) view
				.findViewById(R.id.edtLocationContactUsMessage);
		Picasso.with(getActivity())
				.load(LocationsliderPassDetail.vStoreListImage_A_70X70)
				.into(locationlogo);

		txtLocationContactLocationName
				.setText(LocationsliderPassDetail.vMerchantStoreName);

		SharedPreferences preference = getActivity().getSharedPreferences(
				"UserDetail", Context.MODE_PRIVATE);
		String emailid = preference.getString("emailid", "");
		edtLocationContactEmailAddress.setText(emailid);
	}

	private void clickEvents() {

		relmainfragment_sliderlocationcontact
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgLocationContactBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("back");
			}
		});
		imgLocationContactQuestion.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("questions");
				}
			}
		});
		btnLocationContactSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (edtLocationContactEmailAddress.getText().toString().trim()
						.equals("")) {
					DialogFragment errorDialog = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, getResources().getString(
									R.string.emailvalidation));
					errorDialog.show(getFragmentManager(), "");
				} else if (!Patterns.EMAIL_ADDRESS.matcher(
						edtLocationContactEmailAddress.getText().toString()
								.trim()).matches()) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub
								}
							}, getResources().getText(
									R.string.validemailvalidation).toString());
					ds.show(getFragmentManager(), "");

				} else if (edtLocationContactUsMessage.getText().toString()
						.trim().equals("")) {
					DialogFragment errorDialog = new SingleButtonAlert(
							new DialogInterfaceClick() {
								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, getResources().getString(
									R.string.contactusmessage));
					errorDialog.show(getFragmentManager(), "");
				} else if (!commonData.checkInternetConnection()) {
					DialogFragment errorDialog = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, "");
					errorDialog.show(getFragmentManager(), getResources()
							.getString(R.string.InternetConnect));
					errorDialog.setCancelable(false);
				} else {
					async_deel_feed_list as = new async_deel_feed_list(
							new asynctaskloaddata() {
								String message = edtLocationContactUsMessage
										.getText().toString().trim();
								String emailid = edtLocationContactEmailAddress
										.getText().toString().trim();

								@Override
								public void onPreExecute() {
									// TODO Auto-generated method stub
									topInterface.showDialog("Loading");
								}

								@Override
								public void doInBackground() {
									// TODO Auto-generated method stub
									try {
										String getUrl = "LocationService.svc/AddContactRequest";
										url_response = webService
												.makeServicePostLocationContactUs(
														getUrl,
														commonData.CustomerId,
														commonData.languageId,
														LocationsliderPassDetail.iMerchantStoreId,
														emailid, message);
										JSONObject js_obj = new JSONObject(
												url_response);
										strStatusCode = js_obj
												.getString("vStatusCode");
										strMessageResponse = js_obj
												.getString("vMessageResponse");
									} catch (Exception e) {
									}
								}

								@Override
								public void onPostExecute() {
									topInterface.hideDialog();
									// commonData.toastDisplay(strStatusCode);
									if (strStatusCode.equals("420")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {

														topInterface
																.topbarClick("back");
													}
												},
												getResources()
														.getString(
																R.string.PoorInternetConnect));
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);

									} else if (strStatusCode.equals("100")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														topInterface
																.topbarClick("back");
													}
												}, strMessageResponse);
										ds.show(getFragmentManager(), "");
									} else {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {

													}
												}, strMessageResponse);
										ds.show(getFragmentManager(), "");
									}

								}
							});
					as.execute();
				}

			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}
}
