package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.DialogFragment.ListDialog;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.customClass.LocationList;
import com.squareup.picasso.Picasso;

public class FragmentLocationShare extends Fragment {

	LocationList locationdetail;
	CommonData globalClass;
	ImageView imgLocationShareBack;
	TextView txtLocationShareDone;
	ImageView ivaddfriedtoshare;
	TextView txtfriend;
	private CommonData commonData;
	EditText ettosharetoshare_with_friends;
	topBarInteface topInterface;
	public static ArrayList<FriendsDetail> ListofFriendsEmailtoshare;
	// ImageView imglocImage_locationdetail;
	private WebService_Call webService = new WebService_Call();
	private String url_response = "", getUrl = "", vStatusCode = "",
			vMessageResponse = "";
	RelativeLayout relmainfragment_sharelocation_location;
	ImageView imgLocationImage_row_location;
	TextView txtDealName_rowlocation, txtDealDistane_rowLocation,
			txtLocationDeals_rowlocation, txtloc_desc_rowlocation;

	long MIN_CLICK_INTERVAL = 1000; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_sharelocation_location,
				container, false);

		locationdetail = (LocationList) getArguments().getSerializable(
				"LocationDetail");
		initcontrol(view);
		clickevents();

		return view;
	}

	private void clickevents() {

		relmainfragment_sharelocation_location
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});

		ivaddfriedtoshare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (!commonData.checkInternetConnection()) {
						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										topInterface.topbarClick("back");
									}
								}, getResources().getString(
										R.string.InternetConnect));
						dsp.show(getFragmentManager(), "");
						dsp.setCancelable(false);
					} else {
						DialogFragment listdialid = new ListDialog(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {

										if (tag.equals("OK")) {

											if (ListofFriendsEmailtoshare
													.size() > 0) {

												txtfriend.setText(""
														+ ListofFriendsEmailtoshare
																.get(0).freindsName);
												for (int i = 1; i < ListofFriendsEmailtoshare
														.size(); i++) {
													txtfriend
															.append(","
																	+ ListofFriendsEmailtoshare
																			.get(i).freindsName);
												}
											} else {

												txtfriend.setText("");
												// DialogFragment dsp = new
												// SingleButtonAlert(
												// new DialogInterfaceClick() {
												//
												// @Override
												// public void dialogClick(
												// String tag) {
												// //
												//
												// }
												// },
												// getResources()
												// .getString(
												// R.string.nofriendsselected));
												// dsp.show(getFragmentManager(),
												// "");

											}

										} else {
										}

									}

								}, topInterface, "location");

						listdialid.show(getFragmentManager(), "");
					}
				}

			}
		});

		imgLocationShareBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				topInterface.topbarClick("back");
			}
		});

		txtLocationShareDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!globalClass.checkInternetConnection()) {

					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									// method stub
									topInterface.topbarClick("back");
								}
							}, getResources().getString(
									R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				} else if (ListofFriendsEmailtoshare.size() == 0) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									// method stub
								}
							}, getResources().getString(
									R.string.nofriendsselected));
					ds.show(getFragmentManager(), "");
				}
				// else if (ettosharetoshare_with_friends.getText().toString()
				// .trim().length() == 0) {
				//
				// DialogFragment ds = new SingleButtonAlert(
				// new DialogInterfaceClick() {
				//
				// @Override
				// public void dialogClick(String tag) {
				//
				// // method stub
				// }
				// }, getResources().getString(R.string.etvalidation));
				// ds.show(getFragmentManager(), "");
				//
				// }
				else {
					Run_Asyn_To_Share_Deel_With_MimCup();
				}

			}
		});

	}

	private void initcontrol(View view) {

		globalClass = (CommonData) getActivity().getApplicationContext();
		ListofFriendsEmailtoshare = new ArrayList<FriendsDetail>();
		relmainfragment_sharelocation_location = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_sharelocation_location);

		imgLocationShareBack = (ImageView) view
				.findViewById(R.id.imgLocationShareBack);

		imgLocationImage_row_location = (ImageView) view
				.findViewById(R.id.imgLocationImage_row_location);

		txtDealName_rowlocation = (TextView) view
				.findViewById(R.id.txtDealName_rowlocation);

		txtDealDistane_rowLocation = (TextView) view
				.findViewById(R.id.txtDealDistane_rowLocation);

		txtLocationDeals_rowlocation = (TextView) view
				.findViewById(R.id.txtLocationDeals_rowlocation);

		txtloc_desc_rowlocation = (TextView) view
				.findViewById(R.id.txtloc_desc_rowlocation);

		Picasso.with(getActivity())
				.load(locationdetail.vStoreListImage_A_70X70)
				.into(imgLocationImage_row_location);

		txtDealName_rowlocation.setText(locationdetail.vMerchantStoreName);

		txtLocationDeals_rowlocation.setText(locationdetail.iDealCount);

		txtDealDistane_rowLocation.setText(locationdetail.dDistance);

		txtloc_desc_rowlocation.setText(locationdetail.vShortDescription);

		// imglocImage_locationdetail = (ImageView) view
		// .findViewById(R.id.imglocImage_locationdetail);
		// locationdetail.vStoreDetailImage_A_650X325
		// if (locationdetail.vStoreDetailImage_A_650X325 != null) {
		// Picasso.with(getActivity())
		// .load(locationdetail.vStoreDetailImage_A_650X325)
		// .placeholder(R.drawable.noimage_locationdetail)
		// .into(imglocImage_locationdetail);
		// } else {
		// imglocImage_locationdetail
		// .setImageResource(R.drawable.noimage_locationdetail);
		//
		// }

		commonData = (CommonData) getActivity().getApplicationContext();

		txtLocationShareDone = (TextView) view
				.findViewById(R.id.txtLocationShareDone);
		ivaddfriedtoshare = (ImageView) view
				.findViewById(R.id.ivaddfriedtoshare);
		txtfriend = (TextView) view.findViewById(R.id.txtfriend);
		ettosharetoshare_with_friends = (EditText) view
				.findViewById(R.id.ettosharetoshare_with_friends);

	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	private void Run_Asyn_To_Share_Deel_With_MimCup() {
		final JSONObject jsonMain = new JSONObject();
		JSONArray array = new JSONArray();

		for (int i = 0; i < ListofFriendsEmailtoshare.size(); i++) {
			JSONObject temPObjects = new JSONObject();
			try {
				temPObjects.put("iFriendId",
						ListofFriendsEmailtoshare.get(i).friendsId);
				array.put(temPObjects);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// //////////////
		// RUN BACKGROUND
		try {
			jsonMain.put("iLanguageId", globalClass.languageId);
			jsonMain.put("iCustomerId", globalClass.CustomerId);
			jsonMain.put("iLocationId", locationdetail.iMerchantStoreId);
			jsonMain.put("vMessage", ""
					+ ettosharetoshare_with_friends.getText().toString());
			jsonMain.put("SharewithFriends", array);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						//
						topInterface.showDialog("Loading");
					}

					@Override
					public void doInBackground() {
						//

						// jsonMain.toString()

						try {

							getUrl = "LocationService.svc/ShareLocationViaMims";
							url_response = webService
									.makeServiceShareViaMimCup(getUrl,
											jsonMain.toString());
							JSONObject json_object = new JSONObject(
									url_response);

							vStatusCode = json_object.getString("vStatusCode");
							vMessageResponse = json_object
									.getString("vMessageResponse");
						} catch (Exception e) {
							// TODO Auto-generated catch block

							vMessageResponse = getResources().getString(
									R.string.errormesssage);

						}
					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();
						if (vStatusCode.equals("420")) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.PoorInternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						} else if (vStatusCode.equals("100")) {

							ettosharetoshare_with_friends.setText("");
							txtfriend.setText("");
							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");

										}
									}, vMessageResponse);
							dsp.show(getFragmentManager(), "");
						} else {
							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											topInterface.topbarClick("back");

										}
									}, vMessageResponse);
							dsp.show(getFragmentManager(), "");
						}

					}
				});
		as.execute();

	}

}
