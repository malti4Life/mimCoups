package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mimcoups.Consumer.inteface.LocationListInterface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.LocationList;

public class FragmentDealDetailsMap extends Fragment implements
		OnInfoWindowClickListener {

	private topBarInteface topInterface;
	private LocationListInterface locationListInterface;
	private GoogleMap googleMap;
	private ImageView imgback, ImgLocationListing;
	private ArrayList<LocationList> arrLocationList = new ArrayList<LocationList>();
	RelativeLayout relmainfragment_slider_locationmap;
	long MIN_CLICK_INTERVAL = 1000; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_dealdetailsmap,
				container, false);
		initControls(view);
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.maps)).getMap();

		}

		Handler mhandler = new Handler();
		mhandler.postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				// topinteface.hideDialog();

			}
		}, 500);

		googleMap.setOnInfoWindowClickListener(this);

		DealFeedDetail current_location = (DealFeedDetail) getArguments()
				.getSerializable("location_map");

		LatLng point = new LatLng(current_location.dLatitude,
				current_location.dLongitude);

		drawMarker(point, current_location.vMerchantStoreName);

		CameraPosition cameraPosition = new CameraPosition.Builder()
				.target(point).zoom(9).build();

		googleMap.animateCamera(CameraUpdateFactory
				.newCameraPosition(cameraPosition));

		return view;
	}

	private void initControls(View view) {
		relmainfragment_slider_locationmap = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_locationmap);
		imgback = (ImageView) view.findViewById(R.id.imgback);
		ImgLocationListing = (ImageView) view
				.findViewById(R.id.ImgLocationListing);

		relmainfragment_slider_locationmap
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("back");
			}
		});
		ImgLocationListing.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("Locations");

				}

				// getFragmentManager().popBackStack("fragA",
				// getFragmentManager().POP_BACK_STACK_INCLUSIVE);
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		googleMap.clear();
		// Log.e("called ondestroy fragmnet", "Destroy ");
		// MapFragment f = (MapFragment) getFragmentManager().findFragmentById(
		// R.id.maps);
		// if (f != null)
		// getFragmentManager().beginTransaction().remove(f).commit();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
		locationListInterface = (LocationListInterface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void onDestroyView() {

		try {

			if (!getActivity().isFinishing()) {

				MapFragment f = (MapFragment) getActivity()
						.getFragmentManager().findFragmentById(R.id.maps);
				if (f != null) {
					// Log.e("called ondestroy fragmnet", "Destroy commit");
					getActivity().getFragmentManager().beginTransaction()
							.remove(f).commit();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		// Log.e("called ondestroy fragmnet", "Destroy view");

		super.onDestroyView();
	}

	private void drawMarker(LatLng point, String name) {
		// Creating an instance of MarkerOptions
		MarkerOptions markerOptions = new MarkerOptions();

		// Setting latitude and longitude for the marker
		markerOptions.position(point);
		markerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.icon_map_pin_flag));
		markerOptions.title("" + name);

		// Adding marker on the Google Map
		googleMap.addMarker(markerOptions);
	}

	@Override
	public void onInfoWindowClick(Marker marker) {

	}

}
