package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.NotificationFriendRequestAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.FriendsDetail;

public class FragmentSliderFriendNotificationRequest extends Fragment {

	ListView lstSliderFriendsRequest;
	topBarInteface topInterface;
	CommonData commonClass;
	private String serverResponse = "";
	private WebService_Call webservicecall = new WebService_Call();
	private String statusCode = "", statusMessage = "";
	ArrayList<FriendsDetail> arrNotificationFriends;
	ImageView imgSliderFriendback, imgSliderFriendsRefresh;
	RelativeLayout friendcontainerroot;
	LinearLayout txtNotificationDetailNoData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		commonClass = (CommonData) getActivity().getApplicationContext();
		View view = inflater.inflate(
				R.layout.fragment_slider_friendnotification, container, false);

		initialization(view);
		return view;
	}

	private void initialization(View view) {
		// TODO Auto-generated method stub

		friendcontainerroot = (RelativeLayout) view
				.findViewById(R.id.friendcontainerroot);
		imgSliderFriendback = (ImageView) view
				.findViewById(R.id.imgSliderFriendback);
		imgSliderFriendsRefresh = (ImageView) view
				.findViewById(R.id.imgSliderFriendsRefresh);
		lstSliderFriendsRequest = (ListView) view
				.findViewById(R.id.lstSliderFriendsRequest);
		txtNotificationDetailNoData=(LinearLayout)view.findViewById(R.id.txtNotificationDetailNoData);

		friendcontainerroot.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		imgSliderFriendsRefresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(arrNotificationFriends!=null){
					arrNotificationFriends.clear();	
				}
				
				get_FriendRequest();

			}
		});
		imgSliderFriendback.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((DeelFeedActivity) getActivity()).commonClass.getObserver()
						.setValue("updatenotificationsettings");
				topInterface.topbarClick("back");
			}
		});

	

		
		get_FriendRequest();

	}

	public void get_FriendRequest() {
		if (commonClass.checkInternetConnection()) {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						String vLocationName = "";

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInterface.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub

							String serverUrl = "NotificationService.svc/GetAllFriendRequests?languageid="
									+ commonClass.languageId
									+ "&customerid="
									+ commonClass.CustomerId;

							serverResponse = webservicecall
									.makeServicegetRequest(serverUrl);

//							Log.e("called URL and Response", serverUrl + "==="
//									+ serverResponse.toString());
							try {
								JSONObject js_getres = new JSONObject(
										serverResponse);
								statusCode = js_getres.getString("vStatusCode");
								if (statusCode.equals("100")) {

									arrNotificationFriends = new ArrayList<FriendsDetail>();

									JSONArray js_arr_data = js_getres
											.getJSONArray("FriendRequests");

									for (int i = 0; i < js_arr_data.length(); i++) {
										FriendsDetail fs = new FriendsDetail();
										JSONObject jsobj = js_arr_data
												.getJSONObject(i);
										fs.iCustomerRecievedNotificationId = jsobj
												.getString("iCustomerRecievedNotificationId");

										fs.iRequestFromId = jsobj
												.getString("iRequestFromId");

										fs.freindsName = jsobj
												.getString("vCustomerName");

										fs.vNotificationMessage = jsobj
												.getString("vNotificationMessage");

										fs.profileImageName = jsobj
												.getString("vProfileImageName");

										fs.userName = jsobj
												.getString("vUserName");
										fs.vstatus = jsobj.getString("vStatus");

										arrNotificationFriends.add(fs);

									}

								} else {
									statusMessage = js_getres
											.getString("vMessageResponse");
								}

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub

							topInterface.hideDialog();

							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (statusCode.equals("100")) {
								lstSliderFriendsRequest.setVisibility(View.VISIBLE);
								txtNotificationDetailNoData.setVisibility(View.GONE);

								NotificationFriendRequestAdapter adapter = new NotificationFriendRequestAdapter(
										getActivity(), arrNotificationFriends);
								lstSliderFriendsRequest.setAdapter(adapter);

							} else if (statusCode.equals("101")) {
								
								txtNotificationDetailNoData.setVisibility(View.VISIBLE);
								lstSliderFriendsRequest.setVisibility(View.GONE);
								
//						

//								DialogFragment ds = new SingleButtonAlert(
//										new DialogInterfaceClick() {
//
//											@Override
//											public void dialogClick(String tag) {
//												// TODO Auto-generated method
//												// stub
//
//												((DeelFeedActivity) getActivity()).commonClass
//														.getObserver()
//														.setValue(
//																"updatenotificationsettings");
//
//												topInterface
//														.topbarClick("back");
//
//											}
//										}, statusMessage);
//								ds.show(getFragmentManager(), "");
//								ds.setCancelable(false);
							}
							
							
							
						}
					});
			as.execute();
			
		} else {

			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topInterface.topbarClick("back");

						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);

		}
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
		commonClass = (CommonData) getActivity().getApplicationContext();
	}
}
