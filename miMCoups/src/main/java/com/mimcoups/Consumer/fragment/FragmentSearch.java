package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.SearchResultInterface;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.SearchListAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.SearchLocationListCateogry;

public class FragmentSearch extends Fragment {

	private ListView lstSearchList;
	private ArrayList<SearchLocationListCateogry> arrSearchlist = new ArrayList<SearchLocationListCateogry>();
	private SearchListAdapter adapterSearchList;
	private TextView txtSearchCancel;
	public static EditText edtSearchSearch;
	private topBarInteface topInterface;
	private String strGeturl = "", strStatusCode = "", strMessageResponse,
			urlResponse = "", StrPageIndex = "1", strLastRunDate = "";
	private WebService_Call web_service = new WebService_Call();
	String seachfrom;
	SearchResultInterface searchInteface;
	public CommonData commonClass;
	ImageView imgclick;
	RelativeLayout relmainfragment_search;
	LinearLayout empty;
	public static boolean issearch = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);

		View view = inflater
				.inflate(R.layout.fragment_search, container, false);

		seachfrom = this.getArguments().getString("search");

		initControls(view);

		clickEvents();

		if (!commonClass.checkInternetConnection()) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInterface.showDialog("Loading");
						}

						@Override
						public void doInBackground() {

							String type = "";

							double latitude = commonClass.currentLatitude;
							double longiutde = commonClass.currentLongitude;

							ArrayList<Double> arrLatitude = ((DeelFeedActivity) getActivity())
									.getLatitudeLongitude();

							latitude = arrLatitude.get(0);
							longiutde = arrLatitude.get(1);

							if (seachfrom.equals("location")) {
								type = "store";
							} else {
								type = "deal";
							}
							String strTempUrl = "DealService.svc/GlobalSearchAutoComplete?languageid="
									+ commonClass.languageId
									+ "&customerid="
									+ commonClass.CustomerId
									+ "&latitude="
									+ latitude
									+ "&longitude="
									+ longiutde
									+ "&type=" + type;

							urlResponse = web_service
									.makeServicegetRequest(strTempUrl);

							try {

								JSONObject json_Object = new JSONObject(
										urlResponse);

								JSONArray json_result = json_Object
										.getJSONArray("Keywords");
								strStatusCode = json_Object
										.getString("vStatusCode");
								strMessageResponse = json_Object
										.getString("vMessageResponse");

								if (strStatusCode.equals("100")) {

									arrSearchlist.clear();

									for (int i = 0; i < json_result.length(); i++) {

										JSONObject json_deal = json_result
												.getJSONObject(i);
										SearchLocationListCateogry s = new SearchLocationListCateogry();
										String vKeywordName = json_deal
												.getString("vKeywordName");
										s.vKeywordName = vKeywordName;
										arrSearchlist.add(s);

										// dealDetail = dealDetail
										// .DelFeedDetail(json_deal);
										// arrDealFeeds.add(dealDetail);

									}
								}
							} catch (Exception e) {
							}

						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topInterface.hideDialog();

							if (!commonClass.checkInternetConnection()) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

											}
										}, getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							} else {
								adapterSearchList = new SearchListAdapter(
										getActivity(), arrSearchlist, seachfrom);
								lstSearchList.setAdapter(adapterSearchList);
							}

							// setListAdapter(adapterSearchList);
						}
					});
			as.execute();
		}

		return view;
	}

	private void initControls(View view) {
		commonClass = (CommonData) getActivity().getApplicationContext();

		relmainfragment_search = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_search);

		imgclick = (ImageView) view.findViewById(R.id.imgclick);
		txtSearchCancel = (TextView) view.findViewById(R.id.txtSearchCancel);
		edtSearchSearch = (EditText) view.findViewById(R.id.edtSearchSearch);
		lstSearchList = (ListView) view.findViewById(R.id.list_search);
		empty = (LinearLayout) view.findViewById(R.id.empty);

		if (seachfrom.equals("location")) {
			edtSearchSearch.setHint(getResources().getString(
					R.string.searchstore));
		} else {
			edtSearchSearch.setHint(getResources().getString(
					R.string.searchdeal));
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		if (!issearch) {
			edtSearchSearch.setText("");
		}
	}

	private void clickEvents() {

		relmainfragment_search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		imgclick.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String searchkeyword = edtSearchSearch.getText().toString();
				InputMethodManager imm = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtSearchSearch.getWindowToken(), 0);
				if (searchkeyword.length() != 0) {

					searchInteface.searchDetail(seachfrom, searchkeyword);
				}

			}
		});

		txtSearchCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(edtSearchSearch.getWindowToken(), 0);
				topInterface.topbarClick("searchCancel");

			}
		});
		edtSearchSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				try {

					adapterSearchList.getFilter().filter(s);
				} catch (Exception e) {
					// TODO: handle exception
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		edtSearchSearch.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {

				if (actionId == EditorInfo.IME_ACTION_SEARCH) {

					issearch = false;
					// do something
					InputMethodManager imm = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(
							edtSearchSearch.getWindowToken(), 0);

					String searchkeyword = edtSearchSearch.getText().toString();
					searchInteface.searchDetail(seachfrom, searchkeyword);
					// if (adapterSearchList.getCount() != 0) {
					//
					// String searchkeyword = edtSearchSearch.getText()
					// .toString();
					// searchInteface.searchDetail(seachfrom, searchkeyword);
					//
					// } else {
					// edtSearchSearch.setFocusable(true);
					// edtSearchSearch.setCursorVisible(true);
					// edtSearchSearch.setSelection(edtSearchSearch.getText()
					// .length());
					// }

				}
				return false;
			}
		});

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
		searchInteface = (SearchResultInterface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}
}
