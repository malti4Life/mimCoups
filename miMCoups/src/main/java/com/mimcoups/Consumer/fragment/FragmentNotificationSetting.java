package com.mimcoups.Consumer.fragment;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mimcoups.R;
import com.mimcoups.R.color;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.MasterNotification;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;

public class FragmentNotificationSetting extends Fragment {

	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	private topBarInteface topInterface;
	private ImageView imgNotificationSettingSearch,
			imgNotificationSettingQuestions, imgNotificationSettingsetting;
	private ToggleButton tglNotificationListOnsiteNotification,
			tglNotificationListLoyaltyNotification,
			tglNotificationListNearByNotification,
			tglNotificationListWishlistNotification,
			tglNotificationListFriendsNotification,
			tglNotificationListMimcoupsNotification;

	private TextView txtOnsiteOnOff, txtLoyaltyOnOff, txtNearByOnOff,
			txtWishlistOnOff, txtFriendlistOnOff, txtMimcoupOnOff;

	private CommonData commonClass;

	private String url_response = "", statusCode = "", messageResponse = "";

	private WebService_Call web_service = new WebService_Call();

	private MasterNotification masterNotificaiton = new MasterNotification();
	RelativeLayout relmainfragment_notificationsetting;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_notificationsetting,
				container, false);
		initControls(view);
		clickEvents();

		return view;
	}

	private void initControls(View view) {

		relmainfragment_notificationsetting = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_notificationsetting);

		commonClass = (CommonData) getActivity().getApplicationContext();

		imgNotificationSettingQuestions = (ImageView) view
				.findViewById(R.id.imgNotificationSettingQuestions);
		imgNotificationSettingSearch = (ImageView) view
				.findViewById(R.id.imgNotificationSettingSearch);
		imgNotificationSettingsetting = (ImageView) view
				.findViewById(R.id.imgNotificationSettingsetting);

		tglNotificationListFriendsNotification = (ToggleButton) view
				.findViewById(R.id.tglNotificationListFriendsNotification);

		tglNotificationListOnsiteNotification = (ToggleButton) view
				.findViewById(R.id.tglNotificationListOnsiteNotification);

		tglNotificationListMimcoupsNotification = (ToggleButton) view
				.findViewById(R.id.tglNotificationListMimcoupsNotification);

		tglNotificationListLoyaltyNotification = (ToggleButton) view
				.findViewById(R.id.tglNotificationListLoyaltyNotification);
		tglNotificationListNearByNotification = (ToggleButton) view
				.findViewById(R.id.tglNotificationListNearByNotification);
		tglNotificationListWishlistNotification = (ToggleButton) view
				.findViewById(R.id.tglNotificationListWishlistNotification);

		txtOnsiteOnOff = (TextView) view.findViewById(R.id.txtOnsiteOnOff);

		txtLoyaltyOnOff = (TextView) view.findViewById(R.id.txtLoyaltyOnOff);

		txtNearByOnOff = (TextView) view.findViewById(R.id.txtNearByOnOff);

		txtWishlistOnOff = (TextView) view.findViewById(R.id.txtWishlistOnOff);

		txtFriendlistOnOff = (TextView) view
				.findViewById(R.id.txtFriendlistOnOff);

		txtMimcoupOnOff = (TextView) view.findViewById(R.id.txtMimsoupOnOff);

		if (!commonClass.checkInternetConnection()) {

		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInterface.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub

							String url = "CustomerService.svc/GetMasterNotificationSetting?languageid="
									+ commonClass.languageId
									+ "&customerid="
									+ commonClass.CustomerId;
							url = commonClass.Url_EncodeWithBlank(url);
							url_response = web_service
									.makeServicegetRequest(url);
							try {

								JSONObject json_result = new JSONObject(
										url_response);
								statusCode = json_result
										.getString("vStatusCode");
								messageResponse = json_result
										.getString("vMessageResponse");

								if (statusCode.equals("100")) {

									masterNotificaiton.bIsOnSiteOffer = json_result
											.getBoolean("bIsOnSiteOffer");
									masterNotificaiton.bNearByOffer = json_result
											.getBoolean("bNearByOffer");
									masterNotificaiton.bLoyaltyOffer = json_result
											.getBoolean("bLoyaltyOffer");
									masterNotificaiton.bWishlistOffer = json_result
											.getBoolean("bWishlistOffer");
									masterNotificaiton.bFrinedNotification = json_result
											.getBoolean("bFrinedNotification");
									masterNotificaiton.bMIMSNotification = json_result
											.getBoolean("bMIMSNotification");

									masterNotificaiton.vOnsiteTag = json_result
											.getString("vOnsiteTag");
									masterNotificaiton.vNearByTag = json_result
											.getString("vNearByTag");
									masterNotificaiton.vLoyaltyTag = json_result
											.getString("vLoyaltyTag");
									masterNotificaiton.vWishlistTag = json_result
											.getString("vWishlistTag");
									masterNotificaiton.vFriendTag = json_result
											.getString("vFriendTag");
									masterNotificaiton.vMIMSTag = json_result
											.getString("vMIMSTag");

								}

							} catch (Exception e) {
							}
						}

						@Override
						public void onPostExecute() {
							topInterface.hideDialog();
							// commonClass
							// .toastDisplay(masterNotificaiton.bIsOnSiteOffer
							// + "");

							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (statusCode.equals("100")) {
								tglNotificationListOnsiteNotification
										.setChecked(masterNotificaiton.bIsOnSiteOffer);
								tglNotificationListNearByNotification
										.setChecked(masterNotificaiton.bNearByOffer);
								tglNotificationListLoyaltyNotification
										.setChecked(masterNotificaiton.bLoyaltyOffer);
								tglNotificationListWishlistNotification
										.setChecked(masterNotificaiton.bWishlistOffer);
								tglNotificationListFriendsNotification
										.setChecked(masterNotificaiton.bFrinedNotification);
								tglNotificationListMimcoupsNotification
										.setChecked(masterNotificaiton.bMIMSNotification);

								if (masterNotificaiton.bIsOnSiteOffer) {

									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtOnsiteOnOff.setText("ON");
									} else {
										txtOnsiteOnOff.setText("på");
									}

									txtOnsiteOnOff
											.setTextColor(color.Enable_lable);
								} else {
									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtOnsiteOnOff.setText("OFF");
									} else {
										txtOnsiteOnOff.setText("av");
									}
									txtOnsiteOnOff
											.setTextColor(color.disable_lable);
								}

								if (masterNotificaiton.bNearByOffer) {
									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtNearByOnOff.setText("ON");
									} else {
										txtNearByOnOff.setText("på");
									}
									txtNearByOnOff
											.setTextColor(color.Enable_lable);

								} else {

									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtNearByOnOff.setText("OFF");
									} else {
										txtNearByOnOff.setText("av");
									}
									txtNearByOnOff
											.setTextColor(color.disable_lable);
								}

								if (masterNotificaiton.bLoyaltyOffer) {
									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtLoyaltyOnOff.setText("ON");
									} else {
										txtLoyaltyOnOff.setText("på");
									}
									txtLoyaltyOnOff
											.setTextColor(color.Enable_lable);
								} else {
									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtLoyaltyOnOff.setText("OFF");
									} else {
										txtLoyaltyOnOff.setText("av");
									}
									txtLoyaltyOnOff
											.setTextColor(color.disable_lable);
								}

								if (masterNotificaiton.bWishlistOffer) {
									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtWishlistOnOff.setText("ON");
									} else {
										txtWishlistOnOff.setText("på");
									}
									txtWishlistOnOff
											.setTextColor(color.Enable_lable);
								} else {
									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtWishlistOnOff.setText("OFF");
									} else {
										txtWishlistOnOff.setText("av");
									}
									txtWishlistOnOff
											.setTextColor(color.disable_lable);
								}

								if (masterNotificaiton.bFrinedNotification) {

									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtFriendlistOnOff.setText("ON");
									} else {
										txtFriendlistOnOff.setText("på");
									}
									txtFriendlistOnOff
											.setTextColor(color.Enable_lable);
								} else {
									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtFriendlistOnOff.setText("OFF");
									} else {
										txtFriendlistOnOff.setText("av");
									}
									txtFriendlistOnOff
											.setTextColor(color.disable_lable);
								}

								if (masterNotificaiton.bMIMSNotification) {
									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtMimcoupOnOff.setText("ON");
									} else {
										txtMimcoupOnOff.setText("på");
									}
									txtMimcoupOnOff
											.setTextColor(color.Enable_lable);
								} else {
									if (commonClass.languageId
											.equalsIgnoreCase("1")) {
										txtMimcoupOnOff.setText("OFF");
									} else {
										txtMimcoupOnOff.setText("av");
									}
									txtMimcoupOnOff
											.setTextColor(color.disable_lable);
								}

								// tglNotificationListFriendNotification.setChecked(masterNotificaiton.bFrinedNotification);
								// tglNotificationListFriendNotification.setChecked(masterNotificaiton.bMIMSNotification);

							} else {

								if (messageResponse.trim().length() > 0) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													topInterface
															.topbarClick("back");
												}
											}, messageResponse);
									ds.show(getFragmentManager(), "");
								}
							}

						}
					});
			as.execute();
		}

	}

	private void clickEvents() {

		relmainfragment_notificationsetting
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});

		imgNotificationSettingQuestions
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							lastClickTime = currentTime;
							topInterface.topbarClick("questions");
						}
					}
				});
		imgNotificationSettingSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("search");
			}
		});
		imgNotificationSettingsetting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("notificationsettingsetting");
			}
		});

		tglNotificationListOnsiteNotification
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							callAsynctaskNotificationChange(
									tglNotificationListOnsiteNotification
											.isChecked(),
									masterNotificaiton.vOnsiteTag,
									tglNotificationListOnsiteNotification,
									txtOnsiteOnOff);
						}
					}
				});
		tglNotificationListWishlistNotification
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							callAsynctaskNotificationChange(
									tglNotificationListWishlistNotification
											.isChecked(),
									masterNotificaiton.vWishlistTag,
									tglNotificationListWishlistNotification,
									txtWishlistOnOff);
						}
					}
				});
		tglNotificationListFriendsNotification
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							callAsynctaskNotificationChange(
									tglNotificationListFriendsNotification
											.isChecked(),
									masterNotificaiton.vFriendTag,
									tglNotificationListFriendsNotification,
									txtFriendlistOnOff);
						}

					}
				});

		tglNotificationListMimcoupsNotification
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							callAsynctaskNotificationChange(
									tglNotificationListMimcoupsNotification
											.isChecked(),
									masterNotificaiton.vMIMSTag,
									tglNotificationListMimcoupsNotification,
									txtMimcoupOnOff);
						}

					}
				});

		tglNotificationListNearByNotification
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							callAsynctaskNotificationChange(
									tglNotificationListNearByNotification
											.isChecked(),
									masterNotificaiton.vNearByTag,
									tglNotificationListNearByNotification,
									txtNearByOnOff);
						}
					}
				});
		tglNotificationListLoyaltyNotification
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							callAsynctaskNotificationChange(
									tglNotificationListLoyaltyNotification
											.isChecked(),
									masterNotificaiton.vLoyaltyTag,
									tglNotificationListLoyaltyNotification,
									txtLoyaltyOnOff);
						}
					}
				});
	}

	private void callAsynctaskNotificationChange(final boolean isChecked,
			final String strNotificationTYpe, final ToggleButton tg,
			final TextView txt) {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topInterface.showDialog("Loading");
						statusCode = "";
						messageResponse = "";
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub

						String url = "CustomerService.svc/UpdateMasterNotificationSetting?languageid="
								+ commonClass.languageId
								+ "&customerid="
								+ commonClass.CustomerId
								+ "&sendnotification="
								+ isChecked
								+ "&notificationtag="
								+ strNotificationTYpe;

						url = commonClass.Url_EncodeWithBlank(url);

						url_response = web_service.makeServicegetRequest(url);
						// Log.e("Notification Setting====",
						// url+"==="+url_response);

					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();
						try {
							JSONObject json_result = new JSONObject(
									url_response);
							statusCode = json_result.getString("vStatusCode");
							messageResponse = json_result
									.getString("vMessageResponse");

							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (statusCode.equals("100")) {
								// Toast.makeText(getActivity(),
								//
								// "" + messageResponse, 9000).show();
								tg.setChecked(isChecked);
								if (isChecked) {
									if (CommonData.languageId
											.equalsIgnoreCase("1")) {
										txt.setText("ON");
									} else {
										txt.setText("på");
									}

									txt.setTextColor(color.Enable_lable);

								} else {

									if (CommonData.languageId
											.equalsIgnoreCase("1")) {
										txt.setText("OFF");
									} else {
										txt.setText("av");
									}
									txt.setTextColor(color.disable_lable);
								}

							} else {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub

											}
										}, messageResponse);
								ds.show(getFragmentManager(), "");
							}

						} catch (Exception e) {
						}

					}
				});
		as.execute();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

}