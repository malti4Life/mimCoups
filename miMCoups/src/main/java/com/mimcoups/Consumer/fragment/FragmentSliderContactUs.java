package com.mimcoups.Consumer.fragment;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentSliderContactUs extends Fragment {
	private topBarInteface topInterface;
	private ImageView imgSliderContactusSearch, imgSliderContactUsQuestion;
	private EditText edtSliderContactUsEmailAddress, edtSliderContactUsMessage;
	private Button btnSliderContactUsSubmit, btnSliderContactUsRateMimCoup;
	private CommonData globalClass;
	private WebService_Call webService = new WebService_Call();
	private String url_response = "", strStatusCode = "",
			strMessageResponse = "";
	RelativeLayout relmainfragment_slider_contact_us;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_contact_us,
				container, false);
		initControls(view);
		clickEvents();
		return view;
	}

	private void initControls(View view) {
		// global

		relmainfragment_slider_contact_us = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_contact_us);
		globalClass = (CommonData) getActivity().getApplicationContext();
		imgSliderContactusSearch = (ImageView) view
				.findViewById(R.id.imgSliderContactusSearch);
		imgSliderContactUsQuestion = (ImageView) view
				.findViewById(R.id.imgSliderContactUsQuestion);
		edtSliderContactUsEmailAddress = (EditText) view
				.findViewById(R.id.edtSliderContactUsEmailAddress);
		edtSliderContactUsMessage = (EditText) view
				.findViewById(R.id.edtSliderContactUsMessage);
		btnSliderContactUsRateMimCoup = (Button) view
				.findViewById(R.id.btnSliderContactUsRateMimCoup);
		btnSliderContactUsSubmit = (Button) view
				.findViewById(R.id.btnSliderContactUsSubmit);

		SharedPreferences preference = getActivity().getSharedPreferences(
				"UserDetail", Context.MODE_PRIVATE);
		String emailid = preference.getString("emailid", "");
		edtSliderContactUsEmailAddress.setText(emailid);

	}

	private void clickEvents() {

		relmainfragment_slider_contact_us
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgSliderContactusSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("search");
			}
		});
		imgSliderContactUsQuestion.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("questions");
			}
		});
		btnSliderContactUsRateMimCoup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!globalClass.checkInternetConnection()) {
					DialogFragment errorDialog = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, getResources().getString(
									R.string.InternetConnect));
					errorDialog.show(getFragmentManager(), "");
					errorDialog.setCancelable(false);
				} else {
					
					String link = "market://details?id=";
                    try {
                        // play market available
                    	getActivity().getPackageManager()
                                .getPackageInfo("com.android.vending", 0);
                    // not available
                    } catch (Exception e) {
                        e.printStackTrace();
                        // should use browser
                        link = "https://play.google.com/store/apps/details?id=";
                    }
                    // starts external action
                    getActivity().startActivity(new Intent(Intent.ACTION_VIEW, 
                            Uri.parse(link + getActivity().getPackageName())));
					
//					String url = "https://play.google.com/store/apps/details?id="+getActivity().getPackageName();
//					Intent i = new Intent(Intent.ACTION_VIEW);
//					i.setData(Uri.parse(url));
//					startActivity(i);
				}
			}
		});
		btnSliderContactUsSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (edtSliderContactUsEmailAddress.getText().toString().trim()
						.equals("")) {
					DialogFragment errorDialog = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, getResources().getString(
									R.string.emailvalidation));
					errorDialog.show(getFragmentManager(), "");
				} else if (!Patterns.EMAIL_ADDRESS.matcher(
						edtSliderContactUsEmailAddress.getText().toString()
								.trim()).matches()) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub
								}
							}, getResources().getText(
									R.string.validemailvalidation).toString());
					ds.show(getFragmentManager(), "");
				} else if (edtSliderContactUsMessage.getText().toString()
						.equals("")) {
					DialogFragment errorDialog = new SingleButtonAlert(
							new DialogInterfaceClick() {
								@Override
								public void dialogClick(String tag) {

								}
							}, getResources().getString(
									R.string.contactusmessage));
					errorDialog.show(getFragmentManager(), "");

				} else if (!globalClass.checkInternetConnection()) {
					DialogFragment errorDialog = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, "");
					errorDialog.show(getFragmentManager(), getResources()
							.getString(R.string.InternetConnect));
				} else {
					async_deel_feed_list as = new async_deel_feed_list(
							new asynctaskloaddata() {
								@Override
								public void onPreExecute() {
									// TODO Auto-generated method stub
									topInterface.showDialog("Loading");
								}

								@Override
								public void doInBackground() {
									// TODO Auto-generated method stub
									try {
										String getUrl = "CustomerService.svc/AddContactRequest";
										url_response = webService
												.makeServicePostContactUs(
														getUrl,
														edtSliderContactUsEmailAddress
																.getText()
																.toString()
																.trim(),
														edtSliderContactUsMessage
																.getText()
																.toString()
																.trim(),
														globalClass.CustomerId,
														globalClass.languageId);
										JSONObject js_obj = new JSONObject(
												url_response);
										strStatusCode = js_obj
												.getString("vStatusCode");
										strMessageResponse = js_obj
												.getString("vMessageResponse");
									} catch (Exception e) {
									}
								}

								@Override
								public void onPostExecute() {
									topInterface.hideDialog();
									// globalClass.toastDisplay(strStatusCode);

									if (strStatusCode.equals("420")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {

														topInterface
																.topbarClick("back");
													}
												},
												getResources()
														.getString(
																R.string.PoorInternetConnect));
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);

									} else if (strStatusCode.equals("100")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														topInterface
																.topbarClick("back");
													}
												}, strMessageResponse);
										ds.show(getFragmentManager(), "");
									} else {

										if (strMessageResponse.trim().length() != 0) {
											DialogFragment ds = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {

														}
													}, strMessageResponse);
											ds.show(getFragmentManager(), "");
										}
									}

								}
							});
					as.execute();
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

}
