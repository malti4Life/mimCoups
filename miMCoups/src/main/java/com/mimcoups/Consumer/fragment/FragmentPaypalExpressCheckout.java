package com.mimcoups.Consumer.fragment;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.topBarInteface;

public class FragmentPaypalExpressCheckout extends Fragment {

	private topBarInteface topInterface;
	private WebService_Call web_service = new WebService_Call();
	CommonData globalClass;
	WebService_Call web_Service;
	WebView web_payment;
	ProgressDialog progressDialog;
	String vPayPalAPIUserName, vPayPalAPIPassword, vPayPalAPISignature,
			vPayPalAPIVersion, vPayPalAPIEndPointURL,
			vPayPalExpressCheckoutRedirectURL, vDealPurchaseCancelMessage;
	String AccessToken = "";
	String TransactionId = "";
	String PayerId = "";
	String ReturnUrl = "";
	String CancelUrl = "";
	String vMessageResponse = "", vStatusCode = "";
	String Currency = "AUD", CountryCode = "AU";
	String PaypalLogo = "http://www.mimcoups.com/images/paypallogo.png";
	String Username, Email;

	DealFeedDetail dealdetail;
	SharedPreferences sp;
	String shippingstate = "";
	String billingstate = "";
	ImageView imgPurchaseBack;
	RelativeLayout relmainfragment_purchase;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.layout_payment, container, false);
		globalClass = (CommonData) getActivity().getApplicationContext();
		dealdetail = (DealFeedDetail) getArguments().getSerializable(
				"DealFeedDetail");
		ReturnUrl = globalClass.Get_ReturnUrl(globalClass.languageId);
		CancelUrl = globalClass.Get_CancelUrl(globalClass.languageId);

		sp = getActivity().getSharedPreferences("UserDetail",
				Context.MODE_PRIVATE);

		Username = sp.getString("customername", "");
		Email = sp.getString("emailid", "");

		if (globalClass.languageId.equalsIgnoreCase("2")) {
			Currency = "NOK";
			CountryCode = "NO";
		}

		web_Service = new WebService_Call();
		initControls(view);

		clickEvents();
		return view;
	}

	private void initControls(View view) {
		web_payment = (WebView) view.findViewById(R.id.web_payment);
		relmainfragment_purchase=(RelativeLayout)view.findViewById(R.id.relmainfragment_purchase);
		imgPurchaseBack=(ImageView)view.findViewById(R.id.imgPurchaseBack);

		if (!globalClass.checkInternetConnection()) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							topInterface.topbarClick("back");
						}
					}, getActivity().getResources().getString(
							R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);
		} else {
			new Get_PaymentDetails(globalClass.Payment_details
					+ globalClass.languageId).execute();
		}

	}

	private void clickEvents() {
		relmainfragment_purchase.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		
		imgPurchaseBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(globalClass.checkInternetConnection()){
					
					CancelOrder();
				}
				
				
				topInterface.topbarClick("back");
				
				
			}
		});

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if(globalClass.checkInternetConnection()){
			
			CancelOrder();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	public void OrderApi(final String orderquantityval,
			final String streetnumberval, final String streetaddressval,
			final String citynameval, final String zipcodeval,
			final String billingstreetnumberval,
			final String billingcitynameval,
			final String billingstreetaddressval,
			final String billingzipcodeval, final String cardtype,
			final String nameoncard, final String creditcardno,
			final String monthexp, final String yearexp, final String cvv,
			final String Types, final String shippingstatename,
			final String billingstatename) {

		shippingstate = shippingstatename;
		billingstate = billingstatename;

		if (globalClass.languageId.equalsIgnoreCase("2")) {
			shippingstate = "";
			billingstate = "";

		}

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					String urlResponsereward = "";

					@Override
					public void onPreExecute() {
						topInterface.showDialog("Loading");
					}

					@SuppressWarnings("deprecation")
					@Override
					public void doInBackground() {

						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						nameValuePairs.add(new BasicNameValuePair("customerid",
								"" + globalClass.CustomerId));
						nameValuePairs.add(new BasicNameValuePair("languageid",
								"" + globalClass.languageId));
						nameValuePairs.add(new BasicNameValuePair("dealid", ""
								+ dealdetail.DealId));
						nameValuePairs.add(new BasicNameValuePair("locationid",
								"" + dealdetail.locationId));
						nameValuePairs.add(new BasicNameValuePair("quantity",
								"" + orderquantityval));
						nameValuePairs.add(new BasicNameValuePair(
								"ordertempid", "" + dealdetail.iOrderTempId));

						nameValuePairs
								.add(new BasicNameValuePair("purchasedamount",
										"" + dealdetail.TotalAmount));

						// edttotalprice.getText().toString().replaceAll("$",
						// "");

						nameValuePairs.add(new BasicNameValuePair(
								"shippingstreetnumber", "" + streetnumberval));
						nameValuePairs
								.add(new BasicNameValuePair(
										"shippingstreetaddress", ""
												+ streetaddressval));
						nameValuePairs.add(new BasicNameValuePair(
								"shippingcityname", "" + citynameval));
						nameValuePairs.add(new BasicNameValuePair(
								"shippingzipcode", "" + zipcodeval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingstreetnumber", ""
										+ billingstreetnumberval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingstreetcityname", ""
										+ billingcitynameval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingstreetaddress", ""
										+ billingstreetaddressval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingzipcode", "" + billingzipcodeval));

						nameValuePairs.add(new BasicNameValuePair(
								"billingstatename", "" + billingstate));

						nameValuePairs.add(new BasicNameValuePair(
								"shippingstatename", "" + shippingstate));

						if (Types.equalsIgnoreCase("free")) {
							CommonData.Type = "";
							CommonData.credit_card_holder = "";
							CommonData.credit_card = "";
							CommonData.Expiry_month = "";
							CommonData.Expiry_Year = "";
							CommonData.CVV = "";

						}

						Log.e("customerid", "" + globalClass.CustomerId);
						Log.e("languageid", "" + globalClass.languageId);
						Log.e("dealid", "" + dealdetail.DealId);
						Log.e("locationid", "" + dealdetail.locationId);
						Log.e("quantity", "" + orderquantityval);
						Log.e("purchasedamount", "" + dealdetail.TotalAmount);
						Log.e("shippingstreetnumber", "" + streetnumberval);
						Log.e("shippingstreetaddress", "" + streetaddressval);
						Log.e("shippingcityname", "" + citynameval);
						Log.e("shippingzipcode", "" + zipcodeval);
						Log.e("billingstreetnumber", ""
								+ billingstreetnumberval);
						Log.e("billingstreetcityname", "" + billingcitynameval);
						Log.e("billingstreetaddress", ""
								+ billingstreetaddressval);
						Log.e("billingzipcode", "" + billingzipcodeval);
						Log.e("transactionid", "" + TransactionId);
						Log.e("Shippingstate", shippingstate);
						Log.e("Billing state", billingstate);
						//
						// Log.e("cardtype 1233", "hardik" + CommonData.Type);
						// Log.e("nameoncard", "" +
						// CommonData.credit_card_holder);
						// Log.e("creditcardno", "" + CommonData.credit_card);
						// Log.e("monthexp", "" + CommonData.Expiry_month);
						// Log.e("yearexp", "" + CommonData.Expiry_Year);
						// Log.e("cvv", "" + CommonData.CVV);

						nameValuePairs.add(new BasicNameValuePair(
								"transactionid", "" + TransactionId));

						urlResponsereward = web_Service.makeorder_local(
								globalClass.Base_Url
										+ "OrderService.svc/AddOrder",
								nameValuePairs);
						Log.e("called url response===", urlResponsereward);

						try {
							JSONObject jobj = new JSONObject(urlResponsereward);

							vStatusCode = jobj.getString("vStatusCode");
							if (vStatusCode.equals("100")) {

								vMessageResponse = jobj
										.getString("vMessageResponse");

							} else if (vStatusCode.equals("122")
									|| (vStatusCode.equals("121"))
									|| (vStatusCode.equals("123"))) {
								vMessageResponse = jobj
										.getString("vMessageResponse");

							} else if (vStatusCode.equals("102")) {
								vMessageResponse = jobj
										.getString("vMessageResponse");

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {

						topInterface.hideDialog();

						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {

										topInterface.topbarClick("back");

									}
								}, vMessageResponse);
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					}
				});
		as.execute();
	}

	public class Get_PaymentDetails extends AsyncTask<Void, Void, String> {

		String url = "";

		public Get_PaymentDetails(String Url) {
			this.url = Url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method
			// showProgressDialog("Loading...");
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getActivity().getResources().getString(
					R.string.loading));

			progressDialog.setCancelable(false);
			progressDialog.show();

			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {

			String repsonse = "";

			try {

				repsonse = web_Service.Get_Payment_Details(url);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// return getResponsewithUrlEncoding();
			return repsonse;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				JSONObject obj = new JSONObject(result);
				if (obj.getString("vStatusCode").equalsIgnoreCase("100")) {

					JSONArray arr_payment = obj.getJSONArray("PaypalDetails");

					JSONObject paypal = arr_payment.getJSONObject(0);

					vPayPalAPIUserName = paypal.getString("vPayPalAPIUserName");
					vPayPalAPIPassword = paypal.getString("vPayPalAPIPassword");
					vPayPalAPISignature = paypal
							.getString("vPayPalAPISignature");
					vPayPalAPIVersion = paypal.getString("vPayPalAPIVersion");
					vPayPalAPIEndPointURL = paypal
							.getString("vPayPalAPIEndPointURL");
					vPayPalExpressCheckoutRedirectURL = paypal
							.getString("vPayPalExpressCheckoutRedirectURL");
					vDealPurchaseCancelMessage = paypal
							.getString("vDealPurchaseCancelMessage");
					dealdetail.Dealdesc = dealdetail.Dealdesc.replace("\\n",
							" ");

					Double TotalAmounts = (Double
							.parseDouble(dealdetail.DealAmount) * (Double
							.parseDouble(dealdetail.Quantity)));
					Double rewardpoint = ((Double
							.parseDouble(dealdetail.TotalAmount)) - TotalAmounts);


//					Double TotalAmounts = round((Double
//							.parseDouble(dealdetail.DealAmount) * (Double
//							.parseDouble(dealdetail.Quantity))),2);
//
//					Double rewardpoint = round(((Double
//							.parseDouble(dealdetail.TotalAmount)) - TotalAmounts),2);

					Log.e("Checkout Total amount", "" + TotalAmounts);
					Log.e("Checkout deal amount", dealdetail.DealAmount + "=="
							+ dealdetail.Quantity);
					Log.e("Checkout  total amount", ""
							+ dealdetail.TotalAmount);

					if (globalClass.languageId.equalsIgnoreCase("2")) {
						dealdetail.shippingstatename = "";
						dealdetail.billingstatename = "";
					}
					
					
					
//					String chain_payment=""+"&receiverList.receiver0.email="+"&receiverList.receiver0.amount="+"&receiverList.receiver0.primary="+true+"";
					
					

					String url = vPayPalAPIEndPointURL
							+ "?USER="
							+ vPayPalAPIUserName
							+ "&METHOD=SetExpressCheckout&PAYMENTREQUEST_0_PAYMENTACTION=SALE&PAYMENTREQUEST_0_HANDLINGAMT=0&PAYMENTREQUEST_0_INSURANCEAMT=0&ALLOWNOTE=1&PAYMENTREQUEST_0_CURRENCYCODE="
							+ Currency
							+ "&PWD="
							+ vPayPalAPIPassword
							+ "&SIGNATURE="
							+ vPayPalAPISignature
							+ "&VERSION="
							+ vPayPalAPIVersion
							+ "&RETURNURL="
							+ ReturnUrl
							+ "&CANCELURL="
							+ CancelUrl
							+ "&L_PAYMENTREQUEST_0_NUMBER0=MIMDEAL#"
							+ dealdetail.DealId
							+ "&L_PAYMENTREQUEST_0_AMT0="
							+ dealdetail.DealAmount
							+ "&PAYMENTREQUEST_0_ITEMAMT="
							+ TotalAmounts
							+ "&PAYMENTREQUEST_0_SHIPDISCAMT="
							+ rewardpoint
							+ "&L_PAYMENTREQUEST_0_NAME0="+URLEncoder.encode(dealdetail.Dealname,"UTF-8")
							
							+ "&L_PAYMENTREQUEST_0_DESC0="
							+URLEncoder.encode(dealdetail.Dealdesc,"UTF-8")
							+ "&L_PAYMENTREQUEST_0_QTY0="
							+ dealdetail.Quantity
							+ "&LOGOIMG=http://www.mimcoups.com/images/paypallogo.png&PAYMENTREQUEST_0_AMT="
							+ dealdetail.TotalAmount
							+ "&CARTBORDERCOLOR=f4b40d&EMAIL="
							+ Email
							+ "&LANDINGPAGE=Billing&ADDROVERRIDE=1&PAYMENTREQUEST_0_SHIPTONAME="
							+ Username
							+ "&PAYMENTREQUEST_0_SHIPTOSTREET="+URLEncoder.encode(dealdetail.streetnumberval+ " " + dealdetail.streetaddressval,"UTF-8")
							+ "&PAYMENTREQUEST_0_SHIPTOCITY="
							+ URLEncoder.encode(dealdetail.citynameval,"UTF-8")
							+ "&PAYMENTREQUEST_0_SHIPTOSTATE="
							+  URLEncoder.encode(dealdetail.shippingstatename,"UTF-8")
							+ "&PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE="
							+ CountryCode + "&PAYMENTREQUEST_0_SHIPTOZIP="
							+ dealdetail.zipcodeval;

										

					new payment_request(globalClass.Url_EncodeWithBlank(url))
							.execute();

				} else {
					progressDialog.dismiss();
					
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									topInterface.topbarClick("back");

								}
							}, getActivity().getResources().getString(R.string.txterror));
					ds.setCancelable(false);
					ds.show(getFragmentManager(), "");

					

				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			// http://www.zealousweb.com/?token=EC-3WE87855CR216614T&PayerID=3NMC4VMJXMM2S

		}
	}

	public class payment_request extends AsyncTask<Void, Void, String> {

		String url = "";

		public payment_request(String Url) {
			this.url = Url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method
			// showProgressDialog("Loading...");

			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {

			String repsonse = "";

			try {

				repsonse = web_Service.Get_Payment(url);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// return getResponsewithUrlEncoding();
			return repsonse;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {

				Log.e("called response1 payment request===", result);

				if (result.contains("Success")) {

					String message[] = result.split("&");
					String firsttoken[] = message[0].split("=");

					AccessToken = firsttoken[1];
					Log.e("Token responses====", firsttoken[1]);

					WebSettings settings = web_payment.getSettings();
					settings.setJavaScriptEnabled(true);
					web_payment
							.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

					setwebview();

				} else {
					
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									topInterface.topbarClick("back");

								}
							}, getActivity().getResources().getString(R.string.txterror));
					ds.setCancelable(false);
					ds.show(getFragmentManager(), "");
				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			// http://www.zealousweb.com/?token=EC-3WE87855CR216614T&PayerID=3NMC4VMJXMM2S

		}
	}

	public void setwebview() {

		final ProgressDialog progressBar = ProgressDialog.show(getActivity(),
				"Payment Process", "Loading...");
		web_payment.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {

				if (url.contains(ReturnUrl)) {
					// url.substring(url.lastIndexOf("?") + 1);
					String mainurl = url.substring(url.lastIndexOf("?") + 1);

					Log.e("Url==", url.substring(url.lastIndexOf("?") + 1));

					// token=EC-3WE87855CR216614T&PayerID=3NMC4VMJXMM2S

					String[] strs = mainurl.split("&");
					String tokens[] = strs[0].split("=");
					// AccessToken = tokens[1];

					String payer[] = strs[1].split("=");
					PayerId = payer[1];
					String acceptrequest = "";

					acceptrequest = vPayPalAPIEndPointURL + "?USER="
							+ vPayPalAPIUserName + "&PWD=" + vPayPalAPIPassword
							+ "&SIGNATURE=" + vPayPalAPISignature
							+ "&METHOD=DoExpressCheckoutPayment&VERSION="
							+ vPayPalAPIVersion
							+ "&PAYMENTREQUEST_0_PAYMENTACTION=SALE"
							+ "&PAYMENTREQUEST_0_AMT=" + dealdetail.TotalAmount
							+ "&PAYMENTREQUEST_0_CURRENCYCODE=" + Currency
							+ "&ALLOWNOTE=1" + "&PAYERID=" + PayerId
							+ "&TOKEN=" + AccessToken;

					//
					// String acceptrequest =
					// "https://api-3t.paypal.com/nvp?USER=dion_api1.roemin.com&PWD=47JT34U8TYW2QR4Y&SIGNATURE=AAtG-1Z5cnR.I0nral8S3I.DyBDtA3hM0eD21nbvNCLpP6dxJiBIvEYx&METHOD=DoExpressCheckoutPayment&VERSION=124&TOKEN="
					// + TokenAccess
					// + "&PAYERID="
					// + PayerId
					// +
					// "&PAYMENTREQUEST_0_PAYMENTACTION=SALE&PAYMENTREQUEST_0_AMT=100.0&PAYMENTREQUEST_0_CURRENCYCODE=AUD";

					new Getrequest(globalClass
							.Url_EncodeWithBlank(acceptrequest)).execute();

				} else if (url.contains(CancelUrl)) {
					
					CancelOrder();

					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									topInterface.topbarClick("back");

								}
							}, vDealPurchaseCancelMessage);
					ds.setCancelable(false);
					ds.show(getFragmentManager(), "");
				}

				web_payment.loadUrl(url);
				return true;
			}

			@Override
			public void onPageFinished(WebView view, String url) {

				if (progressBar.isShowing()) {
					progressBar.dismiss();
				}

				Log.e("Page Finish==", "" + url);

			}
		});

		web_payment.loadUrl(vPayPalExpressCheckoutRedirectURL + AccessToken);

	}
	
public void CancelOrder(){

		
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					String urlResponsereward = "";

					@Override
					public void onPreExecute() {
						
					}

					@SuppressWarnings("deprecation")
					@Override
					public void doInBackground() {

						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						nameValuePairs.add(new BasicNameValuePair("customerid",
								"" + globalClass.CustomerId));
						nameValuePairs.add(new BasicNameValuePair("languageid",
								"" + globalClass.languageId));
						
						nameValuePairs.add(new BasicNameValuePair("ordertempid",
								"" + dealdetail.iOrderTempId));

						

						urlResponsereward = web_service.makeorder(
								"OrderService.svc/DeleteOrderTemp", nameValuePairs);
						Log.e("called url response===", urlResponsereward);

						try {
							JSONObject jobj = new JSONObject(urlResponsereward);

							vStatusCode = jobj.getString("vStatusCode");
							if (vStatusCode.equals("100")) {

								

								vMessageResponse = jobj
										.getString("vMessageResponse");

							} else {
								vMessageResponse = jobj
										.getString("vMessageResponse");

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						

					}
				});
		as.execute();
	
	}

	public class Getrequest extends AsyncTask<Void, Void, String> {

		ProgressDialog progressDialog;
		String url = "";

		public Getrequest(String Url) {
			this.url = Url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method
			// showProgressDialog("Loading...");
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage("Loading..");

			progressDialog.setCancelable(false);
			progressDialog.show();

			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {

			String repsonse = "";

			try {

				Log.e("Url===", globalClass.Url_EncodeWithBlank(url));

				repsonse = web_Service.Get_Payment(globalClass
						.Url_EncodeWithBlank(url));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// return getResponsewithUrlEncoding();
			return repsonse;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {

				Log.e("called response Request===", result);

				if (result.contains("Success")) {

					String transId[] = result.split("&");
					String tranId = "";

					int i = 0;
					for (String item : transId) {

						String splitted[] = item.split("=");

						tranId = splitted[1];

						if (i == 9) {
							break;
						}
						i++;
					}

					// String payment[] = transId[9].split("=");
					//
					// String TransactionId = payment[2];
					TransactionId = tranId;

					OrderApi(dealdetail.orderquantityval,
							dealdetail.streetnumberval,
							dealdetail.streetaddressval,
							dealdetail.citynameval, dealdetail.zipcodeval,
							dealdetail.billingstreetnumberval,
							dealdetail.billingcitynameval,
							dealdetail.billingstreetaddressval,
							dealdetail.billingzipcodeval, "", "", "", "", "",
							"", "", dealdetail.shippingstatename,
							dealdetail.billingstatename);

					// showDialog(tranId);

					//

				} else {
					
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									topInterface.topbarClick("back");

								}
							}, getActivity().getResources().getString(R.string.txterror));
					ds.setCancelable(false);
					ds.show(getFragmentManager(), "");
				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			// http://www.zealousweb.com/?token=EC-3WE87855CR216614T&PayerID=3NMC4VMJXMM2S

		}
	}

	public void showDialog(String transId) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		// set title
		alertDialogBuilder.setTitle("Payment Completed");

		// set dialog message
		alertDialogBuilder
				.setMessage(
						"Payment recieved successfully. \n Your TransactionID :: "
								+ transId).setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.dismiss();

						OrderApi(dealdetail.orderquantityval,
								dealdetail.streetnumberval,
								dealdetail.streetaddressval,
								dealdetail.citynameval, dealdetail.zipcodeval,
								dealdetail.billingstreetnumberval,
								dealdetail.billingcitynameval,
								dealdetail.billingstreetaddressval,
								dealdetail.billingzipcodeval, "", "", "", "",
								"", "", "", dealdetail.shippingstatename,
								dealdetail.billingstatename);
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

}
