package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.SliderFriendsListAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.inteface.unFriendInterface;

public class FragmentSliderFriendsList extends Fragment implements
		unFriendInterface {

	private ListView lstSliderFriends;
	private ImageView imgSliderFriendsSearch, imgSliderFriendsQuestions;
	private LinearLayout lnFriendsListSearchByName, lnFriendListSearchByEmail;
	private topBarInteface topInteface;
	private ArrayList<FriendsDetail> arrFriends = new ArrayList<FriendsDetail>();
	private SliderFriendsListAdapter adapterFriendList;
	private CommonData commonData;
	private String getUrl = "", url_response = "", strStatusCode = "",
			strMessageResponse = "";
	private WebService_Call web_service = new WebService_Call();
	private unFriendInterface unFriend = this;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	RelativeLayout relmainfragment_slider_friends;
	private ArrayList<FriendsDetail> arrFriendDetail = new ArrayList<FriendsDetail>();
	LinearLayout txtNotificationDetailNoData;

	String Searchtype = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_friends,
				container, false);
		initControls(view);
		clickEvents();
		if (!commonData.checkInternetConnection()) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							topInteface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);
		} else {
			getData();
		}
		return view;
	}

	private void initControls(View view) {
		commonData = (CommonData) getActivity().getApplicationContext();
		relmainfragment_slider_friends = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_friends);
		lstSliderFriends = (ListView) view.findViewById(R.id.lstSliderFriends);
		imgSliderFriendsQuestions = (ImageView) view
				.findViewById(R.id.imgSliderFriendsQuestions);
		imgSliderFriendsSearch = (ImageView) view
				.findViewById(R.id.imgSliderFriendsSearch);
		lnFriendsListSearchByName = (LinearLayout) view
				.findViewById(R.id.lnFriendsListSearchByName);
		lnFriendListSearchByEmail = (LinearLayout) view
				.findViewById(R.id.lnFriendListSearchByEmail);
		txtNotificationDetailNoData=(LinearLayout)view.findViewById(R.id.txtNotificationDetailNoData);

	}

	public void getData() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topInteface.showDialog("Loading");
						arrFriends = new ArrayList<FriendsDetail>();
						arrFriends.clear();
					}

					@Override
					public void doInBackground() {
						getUrl = "FriendService.svc/GetAllFriends?customerid="
								+ commonData.CustomerId + "&languageid="
								+ commonData.languageId;
						url_response = web_service
								.makeServicegetRequest(getUrl);
						try {
							JSONObject jsonObject = new JSONObject(url_response);
							strStatusCode = jsonObject.getString("vStatusCode");
							strMessageResponse = jsonObject
									.getString("vMessageResponse");
							if (strStatusCode.equals("100")) {
								JSONArray jsonarray = jsonObject
										.getJSONArray("Customers");
								for (int i = 0; i < jsonarray.length(); i++) {
									JSONObject tempObject = jsonarray
											.getJSONObject(i);
									FriendsDetail friendDetail = new FriendsDetail();
									friendDetail.friendsId = tempObject
											.getString("iCustomerId");
									friendDetail.profileImageName = tempObject
											.getString("vProfileImageName");
									friendDetail.userName = tempObject
											.getString("vUserName");
									friendDetail.freindsName = tempObject
											.getString("vCustomerName");
									arrFriends.add(friendDetail);
								}
							}
						} catch (Exception e) {
							strMessageResponse = e.getMessage();
						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						topInteface.hideDialog();

						if (!commonData.checkInternetConnection()) {
							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											topInteface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							dsp.show(getFragmentManager(), "");
							dsp.setCancelable(false);
						} else {

							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInteface.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (strStatusCode.equals("100")) {
								
								txtNotificationDetailNoData.setVisibility(View.GONE);
								lstSliderFriends.setVisibility(View.VISIBLE);
								adapterFriendList = new SliderFriendsListAdapter(
										getActivity(), arrFriends, unFriend);
								lstSliderFriends.setAdapter(adapterFriendList);
							} else {
								txtNotificationDetailNoData.setVisibility(View.VISIBLE);
								lstSliderFriends.setVisibility(View.GONE);

//								DialogFragment dsp = new SingleButtonAlert(
//										new DialogInterfaceClick() {
//
//											@Override
//											public void dialogClick(String tag) {
//												// TODO Auto-generated method
//												// stub
//
//											}
//										}, strMessageResponse);
//								dsp.show(getFragmentManager(), "");
							}
						}

					}
				});
		as.execute();
	}

	private void clickEvents() {

		relmainfragment_slider_friends
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgSliderFriendsQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInteface.topbarClick("questions");
				}
			}
		});
		imgSliderFriendsSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInteface.topbarClick("search");
			}
		});
		lnFriendsListSearchByName.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInteface.topbarClick("searchByName");

				// Searchtype="searchByName";
				// String strWerServiceUrl =
				// "FriendService.svc/CustomersSearchByName?languageid="
				// + commonData.languageId
				// + "&customerid="
				// + commonData.CustomerId + "&name=";
				//
				// arrFriendDetail=new ArrayList<FriendsDetail>();
				// searchByNameEmail(strWerServiceUrl);
			}
		});
		lnFriendListSearchByEmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInteface.topbarClick("searchByEmail");

				// Searchtype="searchByEmail";
				// String strWerServiceUrl =
				// "FriendService.svc/CustomersSearchByEmail?languageid="
				// + commonData.languageId
				// + "&customerid="
				// + commonData.CustomerId + "&email=";
				// arrFriendDetail=new ArrayList<FriendsDetail>();
				// searchByNameEmail(strWerServiceUrl);
			}
		});
	}


	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInteface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void unFriendClick(final String username, final int pos) {

		if (commonData.checkInternetConnection()) {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							topInteface.showDialog("loading");
							url_response = "";
							getUrl = "";
							strMessageResponse = "";
						}

						@Override
						public void doInBackground() {
							getUrl = "FriendService.svc/MakeUnFriend?languageid="
									+ commonData.languageId
									+ "&username1="
									+ commonData.strUsername
									+ "&username2="
									+ username;

							// Log.d("url repsonse>>", "" + getUrl);
							try {
								url_response = web_service
										.makeServicegetRequest(getUrl);
								JSONObject jsonObject = new JSONObject(
										url_response);
								strMessageResponse = jsonObject
										.getString("vMessageResponse");
								// Log.e("callde unfriend", strMessageResponse);
							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topInteface.hideDialog();

							JSONObject json_Object;
							try {
								json_Object = new JSONObject(strMessageResponse);
								strStatusCode = json_Object
										.getString("vStatusCode");
								if (strStatusCode.equals("100")) {

									removefriend();

									//
									// adapterFriendList = new
									// SliderFriendsListAdapter(
									// getActivity(), arrFriends, unFriend);
									// lstSliderFriends
									// .setAdapter(adapterFriendList);

								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
											removefriend();

										}
									}, strMessageResponse);
							dsp.show(getFragmentManager(), "");
							dsp.setCancelable(false);
						}
					});
			as.execute();
		} else {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
		}

	}

	public void removefriend() {
		// getData();
		if (commonData.checkInternetConnection()) {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							arrFriends = new ArrayList<FriendsDetail>();
							arrFriends.clear();
						}

						@Override
						public void doInBackground() {
							getUrl = "FriendService.svc/GetAllFriends?customerid="
									+ commonData.CustomerId
									+ "&languageid="
									+ commonData.languageId;
							// Log.e("called remove", getUrl);
							url_response = web_service
									.makeServicegetRequest(getUrl);
							try {
								JSONObject jsonObject = new JSONObject(
										url_response);
								strStatusCode = jsonObject
										.getString("vStatusCode");
								strMessageResponse = jsonObject
										.getString("vMessageResponse");
								if (strStatusCode.equals("100")) {
									JSONArray jsonarray = jsonObject
											.getJSONArray("Customers");
									for (int i = 0; i < jsonarray.length(); i++) {
										JSONObject tempObject = jsonarray
												.getJSONObject(i);
										FriendsDetail friendDetail = new FriendsDetail();
										friendDetail.friendsId = tempObject
												.getString("iCustomerId");
										friendDetail.profileImageName = tempObject
												.getString("vProfileImageName");
										friendDetail.userName = tempObject
												.getString("vUserName");
										friendDetail.freindsName = tempObject
												.getString("vCustomerName");
										arrFriends.add(friendDetail);
									}
								}
							} catch (Exception e) {
								strMessageResponse = e.getMessage();
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInteface.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (strStatusCode.equals("100")) {
								adapterFriendList = new SliderFriendsListAdapter(
										getActivity(), arrFriends, unFriend);
								lstSliderFriends.setAdapter(adapterFriendList);
							} else {

								adapterFriendList = new SliderFriendsListAdapter(
										getActivity(), arrFriends, unFriend);
								lstSliderFriends.setAdapter(adapterFriendList);

								// DialogFragment dsp = new SingleButtonAlert(
								// new DialogInterfaceClick() {
								//
								// @Override
								// public void dialogClick(String tag) {
								// // TODO Auto-generated method
								// // stub
								// }
								// }, strMessageResponse);
								// dsp.show(getFragmentManager(), "");
							}
							
							if(lstSliderFriends.getCount()>0){
								txtNotificationDetailNoData.setVisibility(View.GONE);
								lstSliderFriends.setVisibility(View.VISIBLE);
							}else{
								txtNotificationDetailNoData.setVisibility(View.VISIBLE);
								lstSliderFriends.setVisibility(View.GONE);
							}
						}
					});
			as.execute();
		}
	}

}
