package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.customClass.WishListDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.inteface.wishlistadpaterToFragment;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.WishListAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentSliderWishlist extends Fragment implements
        wishlistadpaterToFragment {

	private ImageView imgSliderWishlistAdd, imgSliderWishlistQuestions;
	private ListView lstSliderWishlist;
	LinearLayout emptyview;
	private ArrayList<WishListDetail> arrWishlist;
	private WishListAdapter adapterWishList;
	private topBarInteface topInteface;
	private CommonData commonData;
	private WebService_Call web_service = new WebService_Call();
	private String url_response;
	private String StatusCOode = "", MessageResponse = "";
	// private TextView txtSliderWishlistNoData;
	private wishlistadpaterToFragment wishlistadpaterInterface;
	RelativeLayout relmainfragment_slider_wishlist;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_wishlist,
				container, false);
		initControls(view);
		clickEvents();
		if (!commonData.checkInternetConnection()) {

			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
							topInteface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			wishlistadpaterInterface = this;
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInteface.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							try {

								String getUrl = "WishlistService.svc/GetAllWishlistItem?languageid="
										+ commonData.languageId
										+ "&customerid="
										+ commonData.CustomerId;
								getUrl = commonData.Url_EncodeWithBlank(getUrl);
								url_response = web_service
										.makeServicegetRequest(getUrl);
								JSONObject json_result = new JSONObject(
										url_response);
								StatusCOode = json_result
										.getString("vStatusCode");
								if (StatusCOode.equals("100")) {
									JSONArray json_array = json_result
											.getJSONArray("Wishlists");

									for (int i = 0; i < json_array.length(); i++) {
										JSONObject json_wishlist = json_array
												.getJSONObject(i);
										WishListDetail wishlist = new WishListDetail();
										wishlist.categoryName = json_wishlist
												.getString("vCategoryName");
										wishlist.categoryId = json_wishlist
												.getString("iCategoryId");
										wishlist.sendNotificaion = json_wishlist
												.getString("bSendNotification");
										wishlist.Keyword = json_wishlist
												.getString("vDealKeyword");
										wishlist.customerWishlistid = json_wishlist
												.getString("iCustomerWishlistId");
										wishlist.zipCode = json_wishlist
												.getString("vZipCode");

										arrWishlist.add(wishlist);
									}
								} else {
									MessageResponse = json_result
											.getString("vMessageResponse");
								}
							} catch (Exception e) {
								MessageResponse = getResources().getString(
										R.string.InternetConnect);
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topInteface.hideDialog();

							if (!commonData.checkInternetConnection()) {

								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub
												topInteface.topbarClick("back");
											}
										}, getResources().getString(
												R.string.InternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);
							} else {
								// commonData.toastDisplay(StatusCOode);
								if (StatusCOode.equals("100")) {
									adapterWishList = new WishListAdapter(
											getActivity(), arrWishlist,
											wishlistadpaterInterface);
									// setListAdapter(adapterWishList);
									lstSliderWishlist
											.setAdapter(adapterWishList);
									// lstSliderWishlist.setVisibility(View.VISIBLE);
									// txtSliderWishlistNoData
									// .setVisibility(View.GONE);

								} else if (StatusCOode.equals("101")) {
									emptyview.setVisibility(View.VISIBLE);

									// txtSliderWishlistNoData
									// .setText(MessageResponse);
									// txtSliderWishlistNoData
									// .setVisibility(View.VISIBLE);
									// lstSliderWishlist.setVisibility(View.GONE);

								} else {

									if (MessageResponse.trim().length() != 0) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														topInteface
																.topbarClick("back");
													}
												}, MessageResponse);
										ds.show(getFragmentManager(), "");
									}
								}
							}

						}
					});
			as.execute();
		}
		return view;
	}

	private void initControls(View view) {

		relmainfragment_slider_wishlist = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_wishlist);
		emptyview = (LinearLayout) view.findViewById(R.id.empty);

		// txtSliderWishlistNoData = (TextView) view
		// .findViewById(R.id.txtSliderWishlistNoData);
		arrWishlist = new ArrayList<WishListDetail>();
		imgSliderWishlistAdd = (ImageView) view
				.findViewById(R.id.imgSliderWishlistAdd);
		imgSliderWishlistQuestions = (ImageView) view
				.findViewById(R.id.imgSliderWishlistQuestions);
		lstSliderWishlist = (ListView) view.findViewById(R.id.list_wishlist);
		commonData = (CommonData) getActivity().getApplicationContext();
	}

	private void clickEvents() {

		relmainfragment_slider_wishlist
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgSliderWishlistAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInteface.topbarClick("wishlistadd");
			}
		});

		imgSliderWishlistQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInteface.topbarClick("questions");
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInteface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void wishlistDelete(final WishListDetail wishlist) {
		if (!commonData.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							topInteface.showDialog("Loading");
							url_response = "";
						}

						@Override
						public void doInBackground() {
							try {
								String geturl = "WishlistService.svc/DeleteWishlistItem?languageid="
										+ commonData.languageId
										+ "&customerwishlistid="
										+ wishlist.customerWishlistid;

								url_response = web_service
										.makeServicegetRequest(geturl);
							} catch (Exception e) {

							}

						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated
							// method stub
							topInteface.hideDialog();
							try {
								JSONObject js_deleteobj = new JSONObject(
										url_response);
								String vStatusCode = js_deleteobj
										.getString("vStatusCode");
								String vMessageResponse = js_deleteobj
										.getString("vMessageResponse");
								if (vStatusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topInteface
															.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (vStatusCode.equals("100")) {
									arrWishlist.remove(wishlist);
									adapterWishList.notifyDataSetChanged();
									if (arrWishlist.size() == 0) {

										emptyview.setVisibility(View.VISIBLE);
										lstSliderWishlist
												.setVisibility(View.GONE);
									}
								}

							} catch (Exception e) {
							}

						}
					});
			as.execute();
		}
	}
}
