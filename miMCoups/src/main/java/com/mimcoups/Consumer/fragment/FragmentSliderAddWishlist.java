package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.mimcoups.Consumer.DialogFragment.DialogSpinnerFragment;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.mimcoups.Consumer.customClass.WishListDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.DialogNotificationPoupInterface;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentSliderAddWishlist extends Fragment {

	private ImageView imgAddWishlistBack, imgAddwishlistQuestion;
	private Button btnAddWishlistAdd;
	private topBarInteface topInterface;
	private TextView txtAddWishlistTitle, txtAddWishlistCategory;
	private EditText edtAddWishlistKeyword, edtAddWishlistZipCode;
	private CommonData commonData;
	private ToggleButton tgladdwishlistNotification;
	private WebService_Call webServiceCall = new WebService_Call();
	private String url_response = "", strCategoryId = "1",
			strcustomerwishlistid = "1";
	private WishListDetail wishlistDetail;
	private TextView textView1;
	RelativeLayout relmainfragment_slider_add_wishlist;
	LinearLayout linearaddwishlist;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_add_wishlist,
				container, false);

		initControls(view);

		clickEvents();

		wishlistDetail = (WishListDetail) getArguments().getSerializable(
				"UpdateWishlist");

		if (wishlistDetail != null) {
			txtAddWishlistTitle.setText(getResources().getString(
					R.string.UpdateWishlist_title));
			textView1.setText(getResources().getString(
					R.string.updatewishlistMsg));
			txtAddWishlistCategory.setText(wishlistDetail.categoryName);
			tgladdwishlistNotification.setChecked(Boolean
					.parseBoolean(wishlistDetail.sendNotificaion));
			edtAddWishlistKeyword.setText(wishlistDetail.Keyword);
			edtAddWishlistZipCode.setText(wishlistDetail.zipCode);
			btnAddWishlistAdd.setText(getResources().getString(
					R.string.addwishlist_Update));
			strcustomerwishlistid = wishlistDetail.customerWishlistid;
			strCategoryId = wishlistDetail.categoryId;
		}
		return view;
	}

	private void initControls(View view) {
		commonData = (CommonData) getActivity().getApplicationContext();

		linearaddwishlist = (LinearLayout) view
				.findViewById(R.id.linearaddwishlist);
		relmainfragment_slider_add_wishlist = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_add_wishlist);

		textView1 = (TextView) view.findViewById(R.id.txtNoData);
		imgAddWishlistBack = (ImageView) view
				.findViewById(R.id.imgAddWishlistBack);
		imgAddwishlistQuestion = (ImageView) view
				.findViewById(R.id.imgAddwishlistQuestion);
		btnAddWishlistAdd = (Button) view.findViewById(R.id.btnAddWishlistAdd);

		txtAddWishlistTitle = (TextView) view
				.findViewById(R.id.txtAddWishlistTitle);
		txtAddWishlistCategory = (TextView) view
				.findViewById(R.id.txtAddWishlistCategory);

		edtAddWishlistKeyword = (EditText) view
				.findViewById(R.id.edtAddWishlistKeyword);
		edtAddWishlistZipCode = (EditText) view
				.findViewById(R.id.edtAddWishlistZipCode);

		tgladdwishlistNotification = (ToggleButton) view
				.findViewById(R.id.tgladdwishlistNotification);

	}

	private void clickEvents() {

		relmainfragment_slider_add_wishlist
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});

		linearaddwishlist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// DialogFragment dsp = new DialogSpinnerFragment(
				// new DialogNotificationPoupInterface() {
				//
				// @Override
				// public void DialogNotificationClick(
				// CategoryDetail nm) {
				// txtAddWishlistCategory.setText(nm.categoryName);
				// }
				// }, ((DeelFeedActivity) getActivity()).arrCategory);
				// dsp.show(getFragmentManager(), "");

				ArrayList<CategoryDetail> getarray = new ArrayList<CategoryDetail>();

				getarray = ((DeelFeedActivity) getActivity()).arrCategory;

				for (int i = 0; i < getarray.size(); i++) {

					CategoryDetail current = getarray.get(i);

					if (current.categoryName.contains(txtAddWishlistCategory
							.getText().toString())) {
						current.CategoryFavourite = true;
					} else {
						current.CategoryFavourite = false;

					}

				}

				DialogFragment dsp = new DialogSpinnerFragment(
						new DialogNotificationPoupInterface() {

							@Override
							public void DialogNotificationClick(
									CategoryDetail nm) {
								txtAddWishlistCategory.setText(nm.categoryName);
								strCategoryId = nm.categoryId;
							}
						}, getarray);
				dsp.show(getFragmentManager(), "");
			}
		});
		imgAddwishlistQuestion.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("questions");
				}
			}
		});
		imgAddWishlistBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("back");
			}
		});
		btnAddWishlistAdd.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (txtAddWishlistCategory
						.getText()
						.toString()
						.trim()
						.equals(getResources().getString(
								R.string.dealfeedCategory))) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, getResources().getString(
									R.string.addWishlistCategory));
					ds.show(getFragmentManager(), "");
				} else if (edtAddWishlistKeyword.getText().toString().trim()
						.equals("")) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, getResources().getString(
									R.string.addWishlistKeyword));
					ds.show(getFragmentManager(), "");
				} else if (edtAddWishlistZipCode.getText().toString().trim()
						.equals("")) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, getResources().getString(
									R.string.addWishlistZipCode));
					ds.show(getFragmentManager(), "");
				} else if (!commonData.checkInternetConnection()) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

								}
							}, getResources().getString(
									R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				} else {

					View view = getActivity().getCurrentFocus();
					if (view != null) {
						InputMethodManager inputManager = (InputMethodManager) getActivity()
								.getSystemService(Context.INPUT_METHOD_SERVICE);
						inputManager.hideSoftInputFromWindow(
								view.getWindowToken(),
								InputMethodManager.HIDE_NOT_ALWAYS);
					}
					String url = "";
					if (wishlistDetail != null) {
						url = "WishlistService.svc/UpdateWishlistItem?languageid="
								+ commonData.languageId
								+ "&customerid="
								+ commonData.CustomerId
								+ "&customerwishlistid="
								+ strcustomerwishlistid
								+ "&categoryid="
								+ strCategoryId
								+ "&dealkeyword="
								+ edtAddWishlistKeyword.getText().toString()
										.trim()
								+ "&zipcode="
								+ edtAddWishlistZipCode.getText().toString()
										.trim()
								+ "&sendnotification="
								+ tgladdwishlistNotification.isChecked();

					} else {
						url = "WishlistService.svc/AddWishlistItem?languageid="
								+ commonData.languageId
								+ "&customerid="
								+ commonData.CustomerId
								+ "&categoryid="
								+ strCategoryId
								+ "&dealkeyword="
								+ edtAddWishlistKeyword.getText().toString()
										.trim()
								+ "&zipcode="
								+ edtAddWishlistZipCode.getText().toString()
										.trim() + "&sendnotification="
								+ tgladdwishlistNotification.isChecked();
					}
					addUpdateAsynctask(url);
				}

			}
		});
	}

	public void addUpdateAsynctask(final String url) {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topInterface.showDialog("Loading");
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub

						String geturl = url;

						geturl = commonData.Url_EncodeWithBlank(geturl);

						url_response = webServiceCall
								.makeServicegetRequest(geturl);
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						topInterface.hideDialog();
						try {
							JSONObject js_obj_addwishlistitemresult = new JSONObject(
									url_response);

							String VStatusCode = js_obj_addwishlistitemresult
									.getString("vStatusCode");
							String vMessageResponse = js_obj_addwishlistitemresult
									.getString("vMessageResponse");

							if (VStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (VStatusCode.equals("100")) {

								String iCustomerWishlistId = js_obj_addwishlistitemresult
										.getString("iCustomerWishlistId");

								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												((DeelFeedActivity) getActivity()).commonClass
														.getObserver()
														.setValue(
																"updatedwishlist");

												topInterface
														.topbarClick("wishlist Sucessfully");

											}
										}, vMessageResponse);
								dsp.setCancelable(false);
								dsp.show(getFragmentManager(), vMessageResponse);

								//

							} else {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

											}
										}, vMessageResponse);
								dsp.show(getFragmentManager(), "");

							}

						} catch (Exception e) {

						}

					}
				});
		as.execute();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}
}
