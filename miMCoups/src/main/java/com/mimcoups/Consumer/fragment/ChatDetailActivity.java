package com.mimcoups.Consumer.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mimcoups.Consumer.customClass.supportchat;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.ChatDetailAdapter;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.Database_Helper;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class ChatDetailActivity extends Fragment {

	Handler mHandler = new Handler();
	TextView chatdetailusername;
	EditText edtChatDetailMessage;
	static ArrayList<supportchat> arrmsg;
	static ListView lstChatDetailList;
	static Context context;
	String to_idval, to_id = "";
	String from_idval;
	SharedPreferences preferences;
	private CommonData commonData;
	private LinearLayout lnSendChat;
	private topBarInteface topInteface;
	static String to_profileurl;
	ImageView imgChatDetailBack;
	static ChatDetailAdapter adapter;
	public final String RECEIVEBROADCAST = "ChatDetailActivity.android.action.broadcast";
	public final String PRESENCEBROADCAST = "ChatDetailActivity.android.action.broadcast";
	LinearLayout relmainfragment_chat_detail;
	int counter = 0;
	Context c;
	boolean isshowing;
	LinearLayout lnChatListSend;
	TextView tvsend;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_chat_detail, container,
				false);
		c = getActivity();
		commonData = (CommonData) getActivity().getApplicationContext();
		Initialization(view);
		OnClickEvents();
		return view;
	}

	public void Initialization(View view) {

		try {
			getActivity().unregisterReceiver(presencelistener);
		} catch (Exception e) {

		}

		lnChatListSend = (LinearLayout) view.findViewById(R.id.lnChatListSend);
		tvsend = (TextView) view.findViewById(R.id.tvsend);
		relmainfragment_chat_detail = (LinearLayout) view
				.findViewById(R.id.relmainfragment_chat_detail);
		imgChatDetailBack = (ImageView) view
				.findViewById(R.id.imgChatDetailBack);

		IntentFilter intentFilter = new IntentFilter(PRESENCEBROADCAST);
		getActivity().registerReceiver(presencelistener, intentFilter);

		to_id = "";
		arrmsg = new ArrayList<supportchat>();
		lstChatDetailList = (ListView) view
				.findViewById(R.id.lstChatDetailList);
		chatdetailusername = (TextView) view
				.findViewById(R.id.txtChatDetailUserName);
		edtChatDetailMessage = (EditText) view
				.findViewById(R.id.edtChatDetailMessage);
		lnSendChat = (LinearLayout) view.findViewById(R.id.linearLayout2);

		// Bundle bundle = getActivity().getIntent().getExtras();

		to_profileurl = getArguments().getString("dealimagepath");

		to_id = getArguments().getString("chatid");

		to_idval = to_id + "@" + commonData.strServerName;

		preferences = PreferenceManager
				.getDefaultSharedPreferences(getActivity());

		String customerid = preferences.getString("customerid", "");

		adapter = new ChatDetailAdapter(context, arrmsg, to_profileurl);

		from_idval = customerid;

	}

	public void OnClickEvents() {

		relmainfragment_chat_detail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

			}
		});

		imgChatDetailBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				hideKeyboard();
//				rejectAsynctask();
				topInteface.topbarClick("back");

				// onBackPressed();
			}
		});

		edtChatDetailMessage.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				if (edtChatDetailMessage.getText().toString().trim().length() == 0) {
					tvsend.setTextColor(Color.parseColor("#909093"));

				} else {
					tvsend.setTextColor(Color.parseColor("#178ccc"));

				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		lnChatListSend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String chatlen = edtChatDetailMessage.getText().toString()
						.trim();
				if ((chatlen.trim().length() != 0)) {
					if (to_id != "") {
						String chatmsg = edtChatDetailMessage.getText()
								.toString().trim();
						// String customerid =
						// preferences.getString("customerid",
						// "");

						// Log.i("message id>>", "" + customerid);

						Message msg = new Message(to_idval, Message.Type.chat);
						msg.setBody(chatmsg);
						msg.setProperty("support_team_id", "1");
						msg.setProperty("msg_id", commonData.CustomerId);
						msg.setProperty("type_of_message", "support_team_chat");

						if (commonData.xmppconnection != null
								&& commonData.xmppconnection.isConnected()) {
							commonData.xmppconnection.sendPacket(msg);
							SimpleDateFormat sdf = new SimpleDateFormat(
									"MMMM dd yyyy HH:mm:ss aa", Locale.US);
							String currentDateandTime = sdf.format(new Date());

							supportchat chatobj = new supportchat();
							chatobj.from_id = commonData.CustomerId;
							chatobj.to_id = to_idval;
							chatobj.message = chatmsg;
							chatobj.Msg_id = msg.getPacketID();
							chatobj.flag = 0;
							chatobj.time = currentDateandTime;

							arrmsg.add(chatobj);

							adapter = new ChatDetailAdapter(context, arrmsg,
									to_profileurl);
							lstChatDetailList.setAdapter(adapter);
							lstChatDetailList.setSelection(adapter.getCount());
							edtChatDetailMessage.setText("");
						}
					}

				}
			}
		});
	}

	public static void MessageReceive(supportchat chatobj) {
		// Log.d("message recieve in chat", "chatdetail activity");

		arrmsg.add(chatobj);

		ChatDetailAdapter adapter = new ChatDetailAdapter(context, arrmsg,
				to_profileurl);

		Log.d("udpate before", "update before");
		lstChatDetailList.setAdapter(adapter);
		Log.d("udpate after", "update after");
		// adapter.notifyDataSetChanged();
		lstChatDetailList.setSelection(adapter.getCount());

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		if (commonData != null) {
			if (commonData.arrpausechat.size() > 0) {
				for (int i = 0; i < commonData.arrpausechat.size(); i++) {
					supportchat support = commonData.arrpausechat.get(i);
					arrmsg.add(support);

				}
				adapter.notifyDataSetChanged();
				commonData.arrpausechat.clear();

				lstChatDetailList.setSelection(adapter.getCount());
			}

			if (commonData.ismerchantoffline) {

				Log.d("merchant offline resume", "merchant offline resume");
				commonData.ismerchantoffline = false;
				getActivity().onBackPressed();

			}
		}
		super.onResume();
	}

	public class reject_message extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			Thread th = new Thread(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					Log.e("we are in doin reject", "doin reject");
					long currenttimestamp = System.currentTimeMillis();

					String rpmemberid = to_idval;
					Message msg = new Message(rpmemberid, Message.Type.chat);
					msg.setBody("");
					msg.setProperty("support_team_id", "1");
					msg.setProperty("type_of_message",
							"reject_request_for_chat");

					// String customerid = preferences.getString("customerid",
					// "");
					msg.setProperty("msg_id", commonData.strUsername);

					if (commonData.xmppconnection != null
							&& commonData.xmppconnection.isConnected()
							&& commonData.checkInternetConnection()) {
						commonData.xmppconnection.sendPacket(msg);
					}
				}
			});

			th.start();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub

			if (commonData.xmppconnection == null
					|| (!commonData.xmppconnection.isConnected())
					|| (!commonData.checkInternetConnection())) {

				Database_Helper db = new Database_Helper(context);
				db.Open();
				db.add_queue(to_idval, commonData.strUsername);
				db.close();
			}
			super.onPostExecute(result);
		}

	}

	public void rejectAsynctask() {
		removeFriend(to_id, commonData.strUsername);
		new reject_message().execute();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		Log.e("on destroy call", "on destroy call");
		 rejectAsynctask();
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInteface = (topBarInteface) activity;
		context = activity;
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private BroadcastReceiver incomingbroadcast = new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {

			supportchat supportchat = (com.mimcoups.Consumer.customClass.supportchat) intent
					.getSerializableExtra("supportchat");

			arrmsg.add(supportchat);

			ChatDetailAdapter adapter = new ChatDetailAdapter(context, arrmsg,
					to_profileurl);
			lstChatDetailList.setAdapter(adapter);

			lstChatDetailList.setSelection(adapter.getCount());

		}
	};

	private void hideKeyboard() {

		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edtChatDetailMessage.getWindowToken(), 0);
	}

	private BroadcastReceiver presencelistener = new BroadcastReceiver() {

		public void onReceive(Context context, Intent intent) {

			if (counter == 0) {

				counter++;
				String merchantusername = intent
						.getStringExtra("merchantusername");
				if (to_id.trim().equals(merchantusername)
						&& commonData.checkInternetConnection()) {

					Roster roster = commonData.xmppconnection.getRoster();

					Presence availability = roster
							.getPresence(merchantusername);
					Presence.Type type = availability.getType();

					if ((type == Presence.Type.unavailable)) {

						// if (fragment != null) {
						// if (fragment instanceof ChatDetailActivity) {

						if (counter <= 1) {
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
									c);
							alertDialogBuilder
									.setTitle(
											c.getResources().getString(
													R.string.app_name))
									.setMessage(
											c.getResources()
													.getString(
															R.string.merchantofflinemsg))
									.setPositiveButton(
											"OK",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int which) {

													// topInteface
													// .topbarClick("back");
												}
											});

							AlertDialog alertDialog = alertDialogBuilder
									.create();

							// show it

							if (alertDialog != null && alertDialog.isShowing()) {

							} else {
								alertDialog.show();
							}
						}

						// }
						// }

					}

					// AlertDialog.Builder alertdialog = new
					// AlertDialog.Builder(getActivity());
					//
					// alertdialog.setTitle(getResources().getString(R.string.app_name));
					// alertdialog.setMessage(c.getResources().getString(
					// R.string.merchantofflinemsg).setCancelable(false)
					//
					// .setNegativeButton("OK", new
					// DialogInterface.OnClickListener() {
					// public void onClick(DialogInterface dialog, int id) {
					// dialogClick.dialogClick("Ok");
					// dialog.cancel();
					// }
					// });
					// AlertDialog alertDialog = alertdialog.create();
					// alertDialog.setCancelable(false);

					// DialogFragment ds = new SingleButtonAlert(
					// new DialogInterfaceClick() {
					//
					// @Override
					// public void dialogClick(String tag) {
					// try {
					//
					// hideKeyboard();
					// topInteface.topbarClick("back");
					// } catch (Exception e) {
					//
					// }
					// }
					// }, c.getResources().getString(
					// R.string.merchantofflinemsg));
					//
					// if (!ds.isVisible()) {
					// ds.show(((Activity) c).getFragmentManager(), "");
					// ds.setCancelable(false);
					// }

				}

			}
		}
	};

	private void removeFriend(final String to_id, final String username) {
		// TODO Auto-generated method stub

		class friendhandshake extends AsyncTask<Void, Void, Void> {

			String response = "", response1 = "";

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub

				if (commonData.checkInternetConnection()) {
					String url = "http://"
							+ commonData.strServerName
							+ ":9090/plugins/userService/userservice?type=delete_roster&secret=YhzIocug&username="
							+ to_id + "&item_jid=" + username + "@"
							+ commonData.strServerName
							+ "&name=&subscription=3";

					WebService_Call web = new WebService_Call();
					response = web.makeServicegetRequestFriend(url);

					String url1 = "http://"
							+ commonData.strServerName
							+ ":9090/plugins/userService/userservice?type=delete_roster&secret=YhzIocug&username="
							+ username + "&item_jid=" + to_id + "@"
							+ commonData.strServerName
							+ "&name=&subscription=3";

					response1 = web.makeServicegetRequestFriend(url1);

				}

				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub

				super.onPostExecute(result);
				// topInteface.topbarClick("back");
			}

		}

		new friendhandshake().execute();

	}

	public void offlinepresence(String merchantusername) {

		if (to_id.trim().equals(merchantusername)
				&& commonData.checkInternetConnection()) {

			Roster roster = commonData.xmppconnection.getRoster();
			Presence availability = roster.getPresence(merchantusername);
			Presence.Type type = availability.getType();

			if ((type == Presence.Type.unavailable)) {

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						c);
				alertDialogBuilder
						.setTitle(c.getResources().getString(R.string.app_name))
						.setMessage(
								c.getResources().getString(
										R.string.merchantofflinemsg))
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// continue with delete
										// hideKeyboard();
										// topInteface.topbarClick("back");
									}
								});

				AlertDialog alertDialog = alertDialogBuilder.create();

				if (alertDialog != null && alertDialog.isShowing()) {

				} else {
					alertDialog.show();
				}

			}

		}
	}
	
	

}
