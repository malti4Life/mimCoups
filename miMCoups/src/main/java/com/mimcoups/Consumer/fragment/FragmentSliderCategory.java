package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.favouriteCategoryInteface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.CategoryAdpater;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentSliderCategory extends Fragment implements
        favouriteCategoryInteface {
	private ImageView imgSliderCategorySearch, imgSliderCategoryQuestions;
	private ListView lstSliderCategories;
	LinearLayout emptyview;
	private topBarInteface topinteface;
	private ArrayList<CategoryDetail> arrCategory = new ArrayList<CategoryDetail>();
	private CategoryAdpater adapterCategories;
	private CommonData commonData;
	private WebService_Call web_Service = new WebService_Call();
	private String url_response = "";
	private String statusCode = "", message = "";
	private favouriteCategoryInteface favouriteCategory = this;
	RelativeLayout relmainfragment_slider_category;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_category,
				container, false);
		commonData = (CommonData) getActivity().getApplicationContext();
		initControls(view);
		clickEvents();
		return view;
	}

	private void initControls(View view) {

		relmainfragment_slider_category = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_category);
		imgSliderCategorySearch = (ImageView) view
				.findViewById(R.id.imgSliderCategorySearch);
		imgSliderCategoryQuestions = (ImageView) view
				.findViewById(R.id.imgSliderCategoryQuestions);
		lstSliderCategories = (ListView) view.findViewById(R.id.list_category);
		emptyview = (LinearLayout) view.findViewById(R.id.empty);

		if (!commonData.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
							topinteface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));

			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topinteface.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							String get_url = "CategoryService.svc/GetAllCategories?customerid="
									+ commonData.CustomerId
									+ "&languageid="
									+ commonData.languageId;
							url_response = web_Service
									.makeServicegetRequest(get_url);
							// Log.e("called All category", url_response);
							try {
								JSONObject json_result = new JSONObject(
										url_response);
								statusCode = json_result
										.getString("vStatusCode");
								if (statusCode.equals("100")) {
									JSONArray json_allDetails = json_result
											.getJSONArray("Categories");
									for (int i = 0; i < json_allDetails
											.length(); i++) {
										CategoryDetail category = new CategoryDetail();
										JSONObject json_Category = json_allDetails
												.getJSONObject(i);
										category = category
												.parseJson(json_Category);
										if (category != null) {
											arrCategory.add(category);
										}
									}
									((DeelFeedActivity) getActivity()).arrCategory = arrCategory;
								} else {
									message = json_result
											.getString("vMessageResponse");
								}
							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							topinteface.hideDialog();

							if (!commonData.checkInternetConnection()) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub
												topinteface.topbarClick("back");
											}
										}, getResources().getString(
												R.string.InternetConnect));

								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);
							} else {
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topinteface
															.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {
									adapterCategories = new CategoryAdpater(
											getActivity(), arrCategory,
											favouriteCategory);
									lstSliderCategories
											.setAdapter(adapterCategories);
									// setListAdapter(adapterCategories);
									// lstSliderCategories
									// .setAdapter(adapterCategories);
								} else {

									emptyview.setVisibility(View.VISIBLE);
									if (message.trim().length() != 0) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// TODO Auto-generated
														// method
														// stub

													}
												}, message);
										ds.show(getFragmentManager(), "");
									}
								}
							}

						}
					});
			as.execute();
		}

	}

	private void clickEvents() {

		relmainfragment_slider_category
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgSliderCategorySearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topinteface.topbarClick("search");
			}
		});
		imgSliderCategoryQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					topinteface.topbarClick("questions");
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topinteface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void favouriteCategoryClick(final String id,
			final CheckBox checkbox, final boolean isChecked, int positoin) {
		// TODO Auto-generated method stub

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						url_response = "";
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						try {

							String getUrl = "CategoryService.svc/UpdateFavoriteCategories";

							getUrl = commonData.Url_EncodeWithBlank(getUrl);
							String categoryurl = id + "|" + isChecked;
							url_response = web_Service
									.makeServiceFavouriteCategory(getUrl,
											categoryurl, commonData.languageId,
											commonData.CustomerId);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						checkbox.setClickable(true);
						url_response = "";
					}
				});
		as.execute();
	}

}
