package com.mimcoups.Consumer.fragment;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.DialogFragment.AddressListDialog;
import com.mimcoups.Consumer.DialogFragment.ContinueButtonAlert;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.DialogFragment.StatesListDialog;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.Address;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.FontFitTextView;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.mimcoups.Consumer.inteface.DialogAddressInterfaceClick;
import com.squareup.picasso.Picasso;

public class FragmentPurchase extends Fragment {

    private ImageView imgPurchaseBack, imgPurchaseQuestions;
    private Button btnPurchaseBuyNow;
    LinearLayout lnPaymentDetail, lnshippingselect, lnbillingselect;
    DealFeedDetailInterface dealInterface;
    private topBarInteface topInterface;
    private String url_response = "", statusCode, statusMessage = "",
            strStatusCode = "", strMessageResponse;
    private CommonData globalClass;
    private String serverResponse = "";
    private WebService_Call webservicecall = new WebService_Call();
    DealFeedDetail dealdetail;
    TextView txtstorename, txtdealname, dealviewdescriptionheader,
            txtDealDetail;
    ImageView imgDealImage;
    Context context;
    EditText orderquantity;
    EditText streetnumber, cityname, zipcode, billingstreetnumber,
            billingcityname, billingzipcode;
    SharedPreferences prefs;
    long MIN_CLICK_INTERVAL = 800; // in millis
    long lastClickTime = 0;
    FontFitTextView txttotalprice, txtdiscountprice;
    TextView rewardpointslbl;
    private WebService_Call web_service = new WebService_Call();
    private String strGeturlreward = "", strStatusCodereward = "",
            strMessageResponsereward, urlResponsereward = "";
    // double rewardamount;
    LinearLayout rewardpriceblog;
    EditText rewardprice, discountprice, edttotalprice, billingstreetaddress,
            streetaddress;
    public boolean bRewardAvailable;
    View rewardpricedevider;
    int deallimit = 0, remainingdeallimit = 0;
    String rewardamountprice = "0.0", reward = "0.0";
    String vStatusCode = "";
    String vMessageResponse = "";
    CheckBox rewardcheck, cboxsameasabove;
    RelativeLayout rewardpointsblog, relmainfragment_purchase;
    String symbol = " $";
    String deelimage;
    boolean bBillingAddress = false, bShippingAddress = false;
    ImageView billingicon, shippingicon;
    String Types = "";
    View viewstate, billingviewstate;
    EditText lblstate, txtbillingstate;
    LinearLayout lnshippingstate, lnbillingstate;
    ArrayList<String> arr_states = new ArrayList<String>();
    String orderType = "";

    String shippingstatename = "", billingstatename = "";
    String strdealmsg = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_purchase, container,
                false);
        dealdetail = (DealFeedDetail) getArguments().getSerializable(
                "DealFeedDetail");

        CommonData.ispayment = false;

        initialization(view);
        setDefaultPrice("1");
        ClickEvents();
        // orderquantity.setText("1");
        // get_set_From_SharePrefrerence();
        return view;
    }

    public void initialization(View view) {

        context = getActivity();
        bRewardAvailable = false;
        relmainfragment_purchase = (RelativeLayout) view
                .findViewById(R.id.relmainfragment_purchase);
        rewardcheck = (CheckBox) view.findViewById(R.id.rewardcheck);
        rewardpriceblog = (LinearLayout) view
                .findViewById(R.id.rewardpriceblog);
        discountprice = (EditText) view.findViewById(R.id.discountprice);
        rewardprice = (EditText) view.findViewById(R.id.rewardprice);
        globalClass = (CommonData) getActivity().getApplicationContext();
        lnPaymentDetail = (LinearLayout) view
                .findViewById(R.id.lnPaymentDetail);
        rewardpointsblog = (RelativeLayout) view
                .findViewById(R.id.rewardpointsblog);
        rewardpricedevider = (View) view.findViewById(R.id.rewardpricedevider);
        imgPurchaseBack = (ImageView) view.findViewById(R.id.imgPurchaseBack);
        imgPurchaseQuestions = (ImageView) view
                .findViewById(R.id.imgPurchaseQuestions);
        btnPurchaseBuyNow = (Button) view.findViewById(R.id.btnPurchaseBuyNow);
        txtstorename = (TextView) view.findViewById(R.id.txtstorename);
        cboxsameasabove = (CheckBox) view.findViewById(R.id.cboxsameasabove);
        txtdealname = (TextView) view.findViewById(R.id.txtdealname);
        dealviewdescriptionheader = (TextView) view
                .findViewById(R.id.dealviewdescriptionheader);
        txtDealDetail = (TextView) view.findViewById(R.id.txtDealDetail);
        imgDealImage = (ImageView) view.findViewById(R.id.imgDealImage);
        txttotalprice = (FontFitTextView) view.findViewById(R.id.txttotalprice);
        txtdiscountprice = (FontFitTextView) view
                .findViewById(R.id.txtdiscountprice);
        orderquantity = (EditText) view.findViewById(R.id.orderquantity);

        lnshippingselect = (LinearLayout) view
                .findViewById(R.id.lnshippingaddress);
        lnbillingselect = (LinearLayout) view
                .findViewById(R.id.lnbillingaddress);

        streetnumber = (EditText) view.findViewById(R.id.streetnumber);
        streetaddress = (EditText) view.findViewById(R.id.streetaddress);
        cityname = (EditText) view.findViewById(R.id.cityname);
        zipcode = (EditText) view.findViewById(R.id.zipcode);
        billingstreetnumber = (EditText) view
                .findViewById(R.id.billingstreetnumber);
        billingcityname = (EditText) view.findViewById(R.id.billingcityname);
        billingzipcode = (EditText) view.findViewById(R.id.billingzipcode);
        rewardpointslbl = (TextView) view.findViewById(R.id.rewardpointslbl);
        edttotalprice = (EditText) view.findViewById(R.id.totalprice);
        billingstreetaddress = (EditText) view
                .findViewById(R.id.billingstreetaddress);

        shippingicon = (ImageView) view.findViewById(R.id.shippingicons);
        billingicon = (ImageView) view.findViewById(R.id.billingicons);

        viewstate = (View) view.findViewById(R.id.viewstate);
        billingviewstate = (View) view.findViewById(R.id.billingviewstate);
        lblstate = (EditText) view.findViewById(R.id.txtstate);
        txtbillingstate = (EditText) view.findViewById(R.id.txtbillingstate);

        lnbillingstate = (LinearLayout) view.findViewById(R.id.lnbillingstate);

        lnshippingstate = (LinearLayout) view
                .findViewById(R.id.lnshippingstate);

        if (globalClass.languageId.equalsIgnoreCase("1")) {

            viewstate.setVisibility(View.VISIBLE);
            lblstate.setVisibility(View.VISIBLE);
            lnshippingstate.setVisibility(View.VISIBLE);
            txtbillingstate.setVisibility(View.VISIBLE);

            billingviewstate.setVisibility(View.VISIBLE);
            lnbillingstate.setVisibility(View.VISIBLE);

        } else {
            viewstate.setVisibility(View.GONE);
            lblstate.setVisibility(View.GONE);
            lnshippingstate.setVisibility(View.GONE);

            txtbillingstate.setVisibility(View.GONE);

            billingviewstate.setVisibility(View.GONE);
            lnbillingstate.setVisibility(View.GONE);

        }

        LinearLayout lnshippingstate;

        discountprice.setText(getActivity().getResources().getString(
                R.string.currencySymbol)
                + "0.0");

        orderquantity.setSelection(orderquantity.getText().toString().length());

        prefs = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();

        edit.putString("cardCVV", "");
        edit.commit();

        CommonData.Type = "";
        CommonData.credit_card_holder = "";
        CommonData.credit_card = "";
        CommonData.Expiry_month = "";
        CommonData.Expiry_Year = "";
        CommonData.CVV = "";

        getdata();

    }

    public void getdata() {
        if (!globalClass.checkInternetConnection()) {
            DialogFragment dsp = new SingleButtonAlert(
                    new DialogInterfaceClick() {

                        @Override
                        public void dialogClick(String tag) {
                            topInterface.topbarClick("back");
                        }
                    }, getActivity().getResources().getString(
                    R.string.InternetConnect));
            dsp.show(getFragmentManager(), "");
            dsp.setCancelable(false);
        } else {
            async_deel_feed_list as = new async_deel_feed_list(
                    new asynctaskloaddata() {

                        JSONObject js_getres;

                        String vLocationName = "";

                        @Override
                        public void onPreExecute() {
                            // TODO Auto-generated method stub
                            topInterface.showDialog("Loading");
                        }

                        @Override
                        public void doInBackground() {
                            // TODO Auto-generated method stub

                            String serverUrl = "OrderService.svc/GetDealDetailsForOrder?languageid="
                                    + globalClass.languageId
                                    + "&customerid="
                                    + globalClass.CustomerId
                                    + "&dealid="
                                    + dealdetail.DealId
                                    + "&locationid="
                                    + dealdetail.locationId;

                            Log.e("caleld purchase", serverUrl);

                            serverResponse = webservicecall
                                    .makeServicegetRequest(serverUrl);

                            try {

                                js_getres = new JSONObject(serverResponse);
                                statusCode = js_getres.getString("vStatusCode");
                                if (statusCode.equals("100")
                                        || statusCode.equals("102")) {

                                    vMessageResponse = js_getres
                                            .getString("vMessageResponse");

                                } else {
                                    statusMessage = js_getres
                                            .getString("vMessageResponse");
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onPostExecute() {

                            topInterface.hideDialog();

                            if (!globalClass.checkInternetConnection()) {
                                DialogFragment dsp = new SingleButtonAlert(
                                        new DialogInterfaceClick() {

                                            @Override
                                            public void dialogClick(String tag) {
                                                topInterface
                                                        .topbarClick("back");
                                            }
                                        },
                                        getActivity().getResources().getString(
                                                R.string.InternetConnect));
                                dsp.show(getFragmentManager(), "");
                                dsp.setCancelable(false);
                            } else {
                                if (statusCode.equals("420")) {
                                    DialogFragment ds = new SingleButtonAlert(
                                            new DialogInterfaceClick() {

                                                @Override
                                                public void dialogClick(
                                                        String tag) {

                                                    // topInterface
                                                    // .topbarClick("back");
                                                }
                                            },
                                            getActivity()
                                                    .getResources()
                                                    .getString(
                                                            R.string.PoorInternetConnect));
                                    ds.show(getFragmentManager(), "");
                                    ds.setCancelable(false);

                                } else if (statusCode.equals("100")
                                        || statusCode.equals("121")) {
                                    try {
                                        vMessageResponse = js_getres
                                                .getString("vMessageResponse");

                                        bShippingAddress = js_getres
                                                .getBoolean("bShippingAddress");
                                        bBillingAddress = js_getres
                                                .getBoolean("bBillingAddress");

                                        if (!bShippingAddress) {
                                            shippingicon
                                                    .setVisibility(View.GONE);

                                        }
                                        if (!bBillingAddress) {
                                            billingicon
                                                    .setVisibility(View.GONE);
                                        }

                                        String vStorename = js_getres
                                                .getString("vStorename");

                                        String vDealName = js_getres
                                                .getString("vDealName");

                                        String vOriginalPrice = js_getres
                                                .getString("vOriginalPrice");

                                        String vDiscountPrice = js_getres
                                                .getString("vDiscountPrice");

                                        String vDealShotDescriptionHeading = js_getres
                                                .getString("vDealShotDescriptionHeading");

                                        String vDealShotDescription = js_getres
                                                .getString("vDealShotDescription");
                                        dealdetail.vStripeMerchantAccountId=js_getres.getString("vStripeMerchantAccountId");



                                        dealdetail.vMaximumRewardsUsablePercentage = js_getres
                                                .getString("vMaximumRewardsUsablePercentage");
                                        dealdetail.vMaximumRewardsUsablePercentageMessage = js_getres
                                                .getString("vMaximumRewardsUsablePercentageMessage");

                                        if (js_getres
                                                .has("dCurrentSalesCommission")) {
                                            dealdetail.dCurrentSalesCommission = js_getres
                                                    .getString("dCurrentSalesCommission");
                                        }

                                        dealdetail.Dealname = vStorename;

                                        dealdetail.Dealdesc = vDealName;
                                        // dealdetail.Dealdesc = (vDealName
                                        // + " "
                                        // + (vDealShotDescription
                                        // .replace(">", ""))
                                        // + " " + (vDealShotDescriptionHeading
                                        // .replace(">", "")));
                                        dealdetail.DealAmount = vDiscountPrice;

                                        // String vIphoneimage = js_getres
                                        // .getString("vIphoneimage");

                                        String vAndroidimage = js_getres
                                                .getString("vDealListImage_A_350X350");

                                        deelimage = vAndroidimage;

                                        String vDealLimit = "", vDealLimitExceedMessage = "";

                                        remainingdeallimit = Integer.parseInt(js_getres
                                                .getString("iDealRemainingQuantity"));

                                        if (js_getres.getString("vDealLimit")
                                                .trim().equalsIgnoreCase("N/A")) {

                                            vDealLimit = js_getres
                                                    .getString("iDealRemainingQuantity");

                                            vDealLimitExceedMessage = js_getres
                                                    .getString("vDealLimitedQuantityAvailableMessage");

                                            deallimit = remainingdeallimit;

                                        } else {

                                            vDealLimit = js_getres
                                                    .getString("vDealLimit");

                                            deallimit = 0;

                                            deallimit = Integer
                                                    .parseInt(vDealLimit);

                                            vDealLimitExceedMessage = getActivity()
                                                    .getResources()
                                                    .getText(
                                                            R.string.quantitylimit)
                                                    .toString()
                                                    + "\n\n"
                                                    + getActivity()
                                                    .getResources()
                                                    .getText(
                                                            R.string.orderquantity)
                                                    .toString()
                                                    + " "
                                                    + deallimit + "\n";

                                        }
                                        strdealmsg = vDealLimitExceedMessage;

                                        bRewardAvailable = js_getres
                                                .getBoolean("bRewardAvailable");

                                        if (globalClass.languageId
                                                .equalsIgnoreCase("1")) {
                                            symbol = "$";
                                        } else {
                                            symbol = "kr";
                                        }

                                        if (bRewardAvailable) {
                                            rewardpointsblog
                                                    .setVisibility(View.VISIBLE);

                                            rewardamountprice = js_getres
                                                    .getString("vRewardAmount");
                                            reward = rewardamountprice;

                                            rewardpointslbl
                                                    .setText(rewardpointslbl
                                                            .getText()
                                                            .toString()
                                                            + " "
                                                            + symbol
                                                            + rewardamountprice);

                                        } else {
                                            rewardpointsblog
                                                    .setVisibility(View.GONE);

                                        }
                                        String vShippingAddressStreetNumber = js_getres
                                                .getString("vShippingAddressStreetNumber");

                                        String vShippingAddressStreetAddress = js_getres
                                                .getString("vShippingAddressStreetAddress");

                                        String vShippingAddressCityName = js_getres
                                                .getString("vShippingAddressCityName");

                                        String vShippingAddressZipCode = js_getres
                                                .getString("vShippingAddressZipCode");

                                        String vBillingAddressStreetNumber = js_getres
                                                .getString("vBillingAddressStreetNumber");

                                        String vBillingAddressStreetAddress = js_getres
                                                .getString("vBillingAddressStreetAddress");

                                        String vBillingAddressCityName = js_getres
                                                .getString("vBillingAddressCityName");

                                        String vBillingAddressZipCode = js_getres
                                                .getString("vBillingAddressZipCode");
                                        String billingstate = js_getres
                                                .getString("vBillingAddressStateName");
                                        String shippingstate = js_getres
                                                .getString("vShippingAddressStateName");

                                        if (billingstate.contains("null")) {
                                            billingstate = "";
                                        }

                                        if (shippingstate.contains("null")) {
                                            shippingstate = "";
                                        }

                                        txtbillingstate.setText(billingstate);
                                        lblstate.setText(shippingstate);

                                        streetnumber
                                                .setText(vShippingAddressStreetNumber);
                                        streetaddress
                                                .setText(vShippingAddressStreetAddress);
                                        cityname.setText(vShippingAddressCityName);
                                        zipcode.setText(vShippingAddressZipCode);
                                        billingstreetnumber
                                                .setText(vBillingAddressStreetNumber);
                                        billingstreetaddress
                                                .setText(vBillingAddressStreetAddress);
                                        billingcityname
                                                .setText(vBillingAddressCityName);
                                        billingzipcode
                                                .setText(vBillingAddressZipCode);
                                        txtstorename.setText(vStorename);
                                        txtdealname.setText(vDealName);
                                        dealviewdescriptionheader
                                                .setText(vDealShotDescriptionHeading);
                                        // Log.d("emnem==", ""
                                        // + vDealShotDescription);
                                        if (vDealShotDescription != null
                                                && vDealShotDescription != "") {

                                            vDealShotDescription = vDealShotDescription
                                                    .replace("\\n", "\n");
                                            vDealShotDescription = vDealShotDescription
                                                    .replace(">", "●");
                                            // Log.d("IF==", ""
                                            // + vDealShotDescription);
                                        }

                                        txtDealDetail
                                                .setText(vDealShotDescription);

                                        txttotalprice.setText(symbol
                                                + vOriginalPrice);

                                        txttotalprice.setPaintFlags(txttotalprice
                                                .getPaintFlags()
                                                | Paint.STRIKE_THRU_TEXT_FLAG);

                                        txtdiscountprice.setText(symbol
                                                + vDiscountPrice);

                                        Picasso.with(context)
                                                .load(vAndroidimage)
                                                .placeholder(
                                                        R.drawable.noimage_dealslide)
                                                .into(imgDealImage);

                                        orderquantity
                                                .setFocusableInTouchMode(true);
                                        orderquantity.requestFocus();

                                        Log.e("Deal Limit", ""
                                                + remainingdeallimit + "==="
                                                + deallimit);

                                        if (remainingdeallimit <= 0) {

                                            String msg = js_getres
                                                    .getString("vDealOutofStockMessage");

                                            DialogFragment ds = new SingleButtonAlert(
                                                    new DialogInterfaceClick() {

                                                        @Override
                                                        public void dialogClick(
                                                                String tag) {

                                                            topInterface
                                                                    .topbarClick("back");
                                                        }
                                                    }, msg);

                                            ds.setCancelable(false);
                                            ds.show(getFragmentManager(), "");

                                        } else {

                                            if (deallimit <= 0) {
                                                String msg = "";

                                                if (statusCode.equals("121")) {

                                                    msg = js_getres
                                                            .getString("vMessageResponse");

                                                } else {

                                                    msg = js_getres
                                                            .getString("vDealOutofStockMessage");

                                                }
                                                DialogFragment ds = new SingleButtonAlert(
                                                        new DialogInterfaceClick() {

                                                            @Override
                                                            public void dialogClick(
                                                                    String tag) {

                                                                topInterface
                                                                        .topbarClick("back");
                                                            }
                                                        }, msg);

                                                ds.setCancelable(false);
                                                ds.show(getFragmentManager(),
                                                        "");

                                            } else {
                                                final InputMethodManager inputMethodManager = (InputMethodManager) getActivity()
                                                        .getSystemService(
                                                                Context.INPUT_METHOD_SERVICE);
                                                inputMethodManager
                                                        .showSoftInput(
                                                                orderquantity,
                                                                InputMethodManager.SHOW_IMPLICIT);
                                            }
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    setDefaultPrice(orderquantity.getText()
                                            .toString().trim());

                                } else if (statusCode.equals("102")) {

                                    DialogFragment ds = new SingleButtonAlert(
                                            new DialogInterfaceClick() {

                                                @Override
                                                public void dialogClick(
                                                        String tag) {

                                                    topInterface
                                                            .topbarClick("back");
                                                }
                                            }, vMessageResponse);
                                    ds.show(getFragmentManager(), "");
                                    ds.setCancelable(false);

                                    // Toast.makeText(getActivity(),
                                    // "Response 101",
                                    // 9000).show();
                                } else {
                                    // Toast.makeText(getActivity(),
                                    // "Response else",
                                    // 9000).show();
                                }
                            }

                            // Toast.makeText(getActivity(), "Response 100",
                            // 9000)
                            // .show();

                        }
                    });
            as.execute();
        }

    }

    public void ClickEvents() {

        lnshippingselect.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (bShippingAddress) {

                    long currentTime = SystemClock.elapsedRealtime();
                    if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                        lastClickTime = currentTime;

                        if (!globalClass.checkInternetConnection()) {
                            DialogFragment dsp = new SingleButtonAlert(
                                    new DialogInterfaceClick() {

                                        @Override
                                        public void dialogClick(String tag) {
                                            topInterface.topbarClick("back");
                                        }
                                    }, getResources().getString(
                                    R.string.InternetConnect));
                            dsp.show(getFragmentManager(), "");
                            dsp.setCancelable(false);
                        } else {

                            DialogFragment shippingdialog = new AddressListDialog(
                                    new DialogAddressInterfaceClick() {

                                        @Override
                                        public void dialogClick(String tag,
                                                                Address address) {
                                            // TODO Auto-generated method stub

                                            if (tag.equals("OK")) {

                                                streetnumber
                                                        .setText(address.vShippingAddressStreetNumber);
                                                streetaddress
                                                        .setText(address.vShippingAddressStreetAddress);
                                                cityname.setText(address.vShippingAddressCityName);
                                                zipcode.setText(address.vShippingAddressZipCode);
                                                lblstate.setText(address.vShippingAddressStateName);

                                                cboxsameasabove
                                                        .setChecked(false);

                                                lnbillingselect
                                                        .setVisibility(View.VISIBLE);

                                                billingstreetnumber
                                                        .setEnabled(true);
                                                billingstreetaddress
                                                        .setEnabled(true);
                                                billingcityname
                                                        .setEnabled(true);
                                                billingzipcode.setEnabled(true);

                                            } else {

                                            }

                                        }

                                        @Override
                                        public void stateDialogClick(
                                                String tag, String state) {
                                            // TODO Auto-generated method stub

                                        }
                                    }, topInterface, "Shipping");

                            shippingdialog.show(getFragmentManager(), "");

                            // DialogFragment listdialid = new
                            // AddressListDialog(
                            // new DialogAddressInterfaceClick() {
                            //
                            // @Override
                            // public void dialogClick(String tag) {
                            //
                            // if (tag.equals("OK")) {
                            //
                            // } else {
                            //
                            // }
                            // }
                            //
                            // }, topInterface, "Shipping");
                            //
                            // listdialid.setCancelable(false);
                            //
                            // listdialid.show(getFragmentManager(), "");
                        }
                    }

                }

            }
        });

        txtbillingstate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                long currentTime = SystemClock.elapsedRealtime();
                if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                    lastClickTime = currentTime;

                    if (!globalClass.checkInternetConnection()) {
                        DialogFragment dsp = new SingleButtonAlert(
                                new DialogInterfaceClick() {

                                    @Override
                                    public void dialogClick(String tag) {
                                        topInterface.topbarClick("back");
                                    }
                                }, getResources().getString(
                                R.string.InternetConnect));
                        dsp.show(getFragmentManager(), "");
                        dsp.setCancelable(false);
                    } else {

                        DialogFragment statedialog = new StatesListDialog(
                                new DialogAddressInterfaceClick() {

                                    @Override
                                    public void dialogClick(String tag,
                                                            Address address) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void stateDialogClick(String tag,
                                                                 String state) {
                                        // TODO Auto-generated method stub

                                        if (tag.equals("OK")) {
                                            txtbillingstate.setText(state);

                                        }

                                    }
                                }, topInterface, "Billing");

                        statedialog.show(getFragmentManager(), "");
                    }
                }

            }

        });

        lblstate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                long currentTime = SystemClock.elapsedRealtime();
                if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                    lastClickTime = currentTime;

                    if (!globalClass.checkInternetConnection()) {
                        DialogFragment dsp = new SingleButtonAlert(
                                new DialogInterfaceClick() {

                                    @Override
                                    public void dialogClick(String tag) {
                                        topInterface.topbarClick("back");
                                    }
                                }, getResources().getString(
                                R.string.InternetConnect));
                        dsp.show(getFragmentManager(), "");
                        dsp.setCancelable(false);
                    } else {

                        DialogFragment shippingdialog = new StatesListDialog(
                                new DialogAddressInterfaceClick() {

                                    @Override
                                    public void dialogClick(String tag,
                                                            Address address) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void stateDialogClick(String tag,
                                                                 String state) {
                                        // TODO Auto-generated method stub

                                        if (tag.equals("OK")) {

                                            lblstate.setText(state);

                                        }

                                    }
                                }, topInterface, "Shipping");

                        shippingdialog.show(getFragmentManager(), "");

                    }
                }

            }

        });

        lnshippingstate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                long currentTime = SystemClock.elapsedRealtime();
                if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                    lastClickTime = currentTime;

                    if (!globalClass.checkInternetConnection()) {
                        DialogFragment dsp = new SingleButtonAlert(
                                new DialogInterfaceClick() {

                                    @Override
                                    public void dialogClick(String tag) {
                                        topInterface.topbarClick("back");
                                    }
                                }, getResources().getString(
                                R.string.InternetConnect));
                        dsp.show(getFragmentManager(), "");
                        dsp.setCancelable(false);
                    } else {

                        DialogFragment shippingdialog = new StatesListDialog(
                                new DialogAddressInterfaceClick() {

                                    @Override
                                    public void dialogClick(String tag,
                                                            Address address) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void stateDialogClick(String tag,
                                                                 String state) {
                                        // TODO Auto-generated method stub

                                        if (tag.equals("OK")) {

                                            lblstate.setText(state);

                                        }

                                    }
                                }, topInterface, "Shipping");

                        shippingdialog.show(getFragmentManager(), "");

                    }
                }

            }

        });
        lnbillingselect.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (bBillingAddress) {

                    long currentTime = SystemClock.elapsedRealtime();
                    if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                        lastClickTime = currentTime;

                        if (!globalClass.checkInternetConnection()) {
                            DialogFragment dsp = new SingleButtonAlert(
                                    new DialogInterfaceClick() {

                                        @Override
                                        public void dialogClick(String tag) {
                                            topInterface.topbarClick("back");
                                        }
                                    }, getResources().getString(
                                    R.string.InternetConnect));
                            dsp.show(getFragmentManager(), "");
                            dsp.setCancelable(false);
                        } else {

                            DialogFragment shippingdialog = new AddressListDialog(
                                    new DialogAddressInterfaceClick() {

                                        @Override
                                        public void dialogClick(String tag,
                                                                Address address) {
                                            // TODO Auto-generated method stub

                                            if (tag.equals("OK")) {

                                                billingstreetnumber
                                                        .setText(address.vBillingAddressStreetNumber);
                                                billingstreetaddress
                                                        .setText(address.vBillingAddressStreetAddress);
                                                billingcityname
                                                        .setText(address.vBillingAddressCityName);
                                                billingzipcode
                                                        .setText(address.vBillingAddressZipCode);
                                                txtbillingstate
                                                        .setText(address.vBillingAddressStateName);

                                            } else {

                                            }

                                        }

                                        @Override
                                        public void stateDialogClick(
                                                String tag, String state) {
                                            // TODO Auto-generated method stub

                                        }
                                    }, topInterface, "Billing");

                            shippingdialog.show(getFragmentManager(), "");
                        }
                    }

                }

            }
        });

        lnbillingstate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                long currentTime = SystemClock.elapsedRealtime();
                if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                    lastClickTime = currentTime;

                    if (!globalClass.checkInternetConnection()) {
                        DialogFragment dsp = new SingleButtonAlert(
                                new DialogInterfaceClick() {

                                    @Override
                                    public void dialogClick(String tag) {
                                        topInterface.topbarClick("back");
                                    }
                                }, getResources().getString(
                                R.string.InternetConnect));
                        dsp.show(getFragmentManager(), "");
                        dsp.setCancelable(false);
                    } else {

                        DialogFragment statedialog = new StatesListDialog(
                                new DialogAddressInterfaceClick() {

                                    @Override
                                    public void dialogClick(String tag,
                                                            Address address) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void stateDialogClick(String tag,
                                                                 String state) {
                                        // TODO Auto-generated method stub

                                        if (tag.equals("OK")) {
                                            txtbillingstate.setText(state);

                                        }

                                    }
                                }, topInterface, "Billing");

                        statedialog.show(getFragmentManager(), "");
                    }
                }

            }

        });

        imgDealImage.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // Intent zoomintent = new Intent(getActivity(),
                // DeelZoomActivity.class);
                // zoomintent.putExtra("deelimage", deelimage);
                // getActivity().startActivity(zoomintent);

            }
        });

        relmainfragment_purchase.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });

        orderquantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (!(s.toString().trim().equals(""))) {

                    int quantity = Integer.parseInt(s.toString().trim());

                    if (deallimit >= quantity && quantity != 0) {

                        double totalamount = 0.0;

                        String discountpriceval = txtdiscountprice.getText()
                                .toString().trim();
                        int discprice = Integer.valueOf(s.toString());

                        discountprice.setText("" + discprice + " X "
                                + txtdiscountprice.getText().toString());

                        double quantitycount = Double.parseDouble(s.toString());
                        String discountval = discountpriceval.replace(symbol,
                                "");
                        double discounttotalprice = Double
                                .parseDouble(discountval);
                        double totalpriceval = quantitycount
                                * discounttotalprice;
                        DecimalFormat df = new DecimalFormat("############.##");
                        rewardamountprice = reward;


                        if (rewardcheck.isChecked()) {

                            if (bRewardAvailable) {


                                if (Double.parseDouble(rewardamountprice) > ((totalpriceval * (Double.parseDouble(dealdetail.vMaximumRewardsUsablePercentage))) / 100)) {

                                    totalamount = totalpriceval
                                            - ((totalpriceval * (Double.parseDouble(dealdetail.vMaximumRewardsUsablePercentage))) / 100);
                                    rewardamountprice = ""
                                            + (Double
                                            .parseDouble(rewardamountprice) - (totalpriceval * (Double.parseDouble(dealdetail.vMaximumRewardsUsablePercentage))) / 100);

                                } else {
                                    totalamount = totalpriceval
                                            - (Double
                                            .parseDouble(rewardamountprice));
                                    rewardamountprice = "0";
                                }
                                Double rewarss = totalpriceval - totalamount;
                                rewardprice.setText(symbol + df.format(rewarss));
                                rewardpointslbl.setText(getActivity()
                                        .getResources().getString(
                                                R.string.rewardpoints)
                                        + " " + symbol + round(Double.parseDouble(rewardamountprice), 2));

                                if (totalamount < 0) {
                                    totalamount = 0.0;
                                }
                            } else {
                                totalamount = totalpriceval;
                            }

                        } else {
                            totalamount = totalpriceval;
                        }

                        // Log.e("Reward points", "+" + rewardamountprice +
                        // "==="
                        // + totalpriceval);
                        if (Double.parseDouble(rewardamountprice) > totalpriceval) {
                            rewardprice.setText(symbol
                                    + df.format(totalpriceval));

                        }

                        edttotalprice.setText("" + df.format(totalamount));
                    } else {

                        if (quantity == 0) {

                        } else {
                            DialogFragment ds = new SingleButtonAlert(
                                    new DialogInterfaceClick() {

                                        @Override
                                        public void dialogClick(String tag) {
                                            // TODO Auto-generated method stub
                                            orderquantity.setText("1");
                                            orderquantity
                                                    .setSelection(orderquantity
                                                            .getText()
                                                            .toString()
                                                            .length());

                                            setDefaultPrice(orderquantity
                                                    .getText().toString()
                                                    .trim());

                                        }
                                    }, strdealmsg);
                            ds.show(getFragmentManager(), "");

                            ds.setCancelable(false);
                        }

                    }

                } else {
                    Log.d("we are here", "here");
                    edttotalprice.setText("");
                    edttotalprice.setText("0.0");
                    // edttotalprice.setText(getActivity().getResources()
                    // .getText(R.string.currencySymbol).toString()
                    // + "0.0");
                    discountprice.setText(getActivity().getResources()
                            .getText(R.string.currencySymbol).toString()
                            + "0.0");
                }

                // TODO Auto-generated method stub
            }
        });
        lnPaymentDetail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                final String streetnumberval = streetnumber.getText()
                        .toString().trim();
                final String streetaddressval = streetaddress.getText()
                        .toString().trim();

                final String citynameval = cityname.getText().toString().trim();

                final String zipcodeval = zipcode.getText().toString().trim();

                if (streetnumberval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.streetnumbervalidation)
                            .toString());

                } else if (streetaddressval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.streetaddressvalidation)
                            .toString());

                } else if (citynameval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.citytownvalidation).toString());

                } else if (zipcodeval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.zipcodevalidation).toString());

                } else if (zipcodeval.length() < 4) {

                    show_message(getActivity().getResources()
                            .getText(R.string.zipcodevalidate).toString());

                } else if (billingstreetnumber.getText().toString().trim()
                        .length() == 0) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingstreetnumbervalidation)
                            .toString());

                } else if (billingstreetaddress.getText().toString().trim()
                        .length() == 0) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingstreetaddressvalidation)
                            .toString());

                } else if (billingcityname.getText().toString().trim().length() == 0) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingcitytownvalidation)
                            .toString());

                } else if (billingzipcode.getText().toString().trim().length() == 0) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingzipcodevalidation)
                            .toString());
                } else if (billingzipcode.getText().toString().trim().length() < 4) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingzipcodevalidate)
                            .toString());

                } else {

                    SharedPreferences.Editor edit = prefs.edit();

                    edit.putString("billingstreetnumber", billingstreetnumber
                            .getText().toString().trim());
                    edit.putString("billingstreetaddress", billingstreetaddress
                            .getText().toString().trim());
                    edit.putString("billingcity", billingcityname.getText()
                            .toString().trim());
                    edit.putString("billingzip", billingzipcode.getText()
                            .toString().trim());
                    edit.commit();
                    getActivity()
                            .getWindow()
                            .setSoftInputMode(
                                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                    dealInterface.dealFeedClick("DealCreditcard", null);
                }

            }
        });

        rewardpointslbl.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });

        imgPurchaseBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                topInterface.topbarClick("back");
            }
        });
        imgPurchaseQuestions.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager inputManager = (InputMethodManager) getActivity()
                            .getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }

                long currentTime = SystemClock.elapsedRealtime();
                if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                    lastClickTime = currentTime;

                    topInterface.topbarClick("questions");

                }
            }
        });
        btnPurchaseBuyNow.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                // Toast.makeText(getActivity(), "" + dealdetail.iLocationId,
                // Toast.LENGTH_SHORT).show();

                final String orderquantityval = orderquantity.getText()
                        .toString().trim();
                final String streetnumberval = streetnumber.getText()
                        .toString().trim();
                final String streetaddressval = streetaddress.getText()
                        .toString().trim();

                final String citynameval = cityname.getText().toString().trim();

                final String zipcodeval = zipcode.getText().toString().trim();

                final String billingstreetnumberval = billingstreetnumber
                        .getText().toString().trim();

                final String billingcitynameval = billingcityname.getText()
                        .toString().trim();

                final String billingzipcodeval = billingzipcode.getText()
                        .toString().trim();

                final String billingstreetaddressval = billingstreetaddress
                        .getText().toString().trim();

                final String amount = edttotalprice.getText().toString().trim();

                if (orderquantityval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.orderquantityvalidation)
                            .toString());

                } else if ((Integer.parseInt(orderquantityval)) <= 0) {
                    show_message(getActivity().getResources()
                            .getText(R.string.orderquantityvalidation)
                            .toString());
                } else if (streetnumberval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.streetnumbervalidation)
                            .toString());

                } else if (streetaddressval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.streetaddressvalidation)
                            .toString());

                } else if (citynameval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.citytownvalidation).toString());

                } else if (lblstate.getText().toString().trim().length() == 0
                        && CommonData.languageId.trim().equalsIgnoreCase("1")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.shippingstatevalidate).toString());
                } else if (zipcodeval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.zipcodevalidation).toString());

                } else if (zipcodeval.length() < 4) {

                    show_message(getActivity().getResources()
                            .getText(R.string.zipcodevalidate).toString());

                } else if (billingstreetnumberval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingstreetnumbervalidation)
                            .toString());

                } else if (billingstreetaddressval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingstreetaddressvalidation)
                            .toString());

                } else if (billingcitynameval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingcitytownvalidation)
                            .toString());

                } else if (txtbillingstate.getText().toString().trim().length() == 0
                        && CommonData.languageId.trim().equalsIgnoreCase("1")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingstatevalidate).toString());
                } else if (billingzipcodeval.equals("")) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingzipcodevalidation)
                            .toString());

                } else if (billingzipcodeval.length() < 4) {

                    show_message(getActivity().getResources()
                            .getText(R.string.billingzipcodevalidate)
                            .toString());

                } else {

                    if(txtbillingstate.getText().toString().trim().length()==0 && CommonData.languageId.trim().equalsIgnoreCase("1")){
                        show_message(getActivity().getResources().getString(R.string.billingstatevalidate));
                    } else if(!globalClass.ispayment){
                        show_message(getActivity().getResources().getString(R.string.paymentValidation));
                    }
                    else{

                        View view = getActivity().getCurrentFocus();
                        if (view != null) {
                            InputMethodManager inputManager = (InputMethodManager) getActivity()
                                    .getSystemService(
                                            Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(
                                    view.getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                        }
                        long currentTime = SystemClock.elapsedRealtime();
                        if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                            lastClickTime = currentTime;

                            final String totalprice = edttotalprice.getText()
                                    .toString().trim().replaceAll(symbol, "");

                            dealdetail.Quantity = orderquantityval;
                            dealdetail.TotalAmount = totalprice;

                            if (!globalClass.checkInternetConnection()) {
                                DialogFragment ds = new SingleButtonAlert(
                                        new DialogInterfaceClick() {

                                            @Override
                                            public void dialogClick(String tag) {

                                                topInterface
                                                        .topbarClick("back");
                                            }
                                        }, getResources().getString(
                                        R.string.InternetConnect));
                                ds.show(getFragmentManager(), "");
                                ds.setCancelable(false);
                            } else {

                                async_deel_feed_list as = new async_deel_feed_list(
                                        new asynctaskloaddata() {

                                            String urlResponsereward = "";

                                            @Override
                                            public void onPreExecute() {
                                                topInterface
                                                        .showDialog("Loading");
                                            }

                                            @SuppressWarnings("deprecation")
                                            @Override
                                            public void doInBackground() {

                                                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                                                nameValuePairs
                                                        .add(new BasicNameValuePair(
                                                                "customerid",
                                                                ""
                                                                        + globalClass.CustomerId));
                                                nameValuePairs
                                                        .add(new BasicNameValuePair(
                                                                "languageid",
                                                                ""
                                                                        + globalClass.languageId));
                                                nameValuePairs
                                                        .add(new BasicNameValuePair(
                                                                "dealid",
                                                                ""
                                                                        + dealdetail.DealId));
                                                nameValuePairs
                                                        .add(new BasicNameValuePair(
                                                                "locationid",
                                                                ""
                                                                        + dealdetail.locationId));
                                                nameValuePairs
                                                        .add(new BasicNameValuePair(
                                                                "quantity",
                                                                ""
                                                                        + orderquantityval));

                                                urlResponsereward = web_service
                                                        .makeorder(
                                                                "OrderService.svc/AddOrderTemp",
                                                                nameValuePairs);
                                                Log.e("called url response Add ordertemp===",
                                                        urlResponsereward);

                                                try {
                                                    JSONObject jobj = new JSONObject(
                                                            urlResponsereward);

                                                    vStatusCode = jobj
                                                            .getString("vStatusCode");
                                                    if (vStatusCode
                                                            .equals("100")) {

                                                        dealdetail.iOrderTempId = jobj
                                                                .getString("ordertempid");

                                                        vMessageResponse = jobj
                                                                .getString("vMessageResponse");

                                                        // AddOrderAPI(
                                                        // orderquantityval,
                                                        // streetnumberval,
                                                        // streetaddressval,
                                                        // citynameval,
                                                        // billingzipcodeval,
                                                        // billingstreetnumberval,
                                                        // billingcitynameval,
                                                        // billingstreetaddressval,
                                                        // billingzipcodeval,
                                                        // "",
                                                        // "",
                                                        // "",
                                                        // "",
                                                        // "",
                                                        // "",
                                                        // Types,
                                                        // shippingstatename,
                                                        // billingstatename,amount);

                                                    } else {

                                                        vMessageResponse = jobj
                                                                .getString("vMessageResponse");

                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                    Log.e("Exceptions",
                                                            e.getMessage());
                                                }
                                            }

                                            @Override
                                            public void onPostExecute() {

                                                topInterface.hideDialog();

                                                if (vMessageResponse.trim()
                                                        .length() <= 0) {
                                                    vMessageResponse = urlResponsereward;
                                                }

                                                if (vStatusCode.equals("100")) {

                                                    AddOrderAPI(
                                                            orderquantityval,
                                                            streetnumberval,
                                                            streetaddressval,
                                                            citynameval,
                                                            billingzipcodeval,
                                                            billingstreetnumberval,
                                                            billingcitynameval,
                                                            billingstreetaddressval,
                                                            billingzipcodeval,
                                                            "", "", "", "", "",
                                                            "", Types,
                                                            shippingstatename,
                                                            billingstatename,
                                                            amount);

                                                } else {
                                                    DialogFragment ds = new SingleButtonAlert(
                                                            new DialogInterfaceClick() {

                                                                @Override
                                                                public void dialogClick(
                                                                        String tag) {

                                                                    View view = getActivity()
                                                                            .getCurrentFocus();
                                                                    if (view != null) {
                                                                        InputMethodManager inputManager = (InputMethodManager) getActivity()
                                                                                .getSystemService(
                                                                                        Context.INPUT_METHOD_SERVICE);
                                                                        inputManager
                                                                                .hideSoftInputFromWindow(
                                                                                        view.getWindowToken(),
                                                                                        InputMethodManager.HIDE_NOT_ALWAYS);
                                                                    }
                                                                    getFragmentManager()
                                                                            .popBackStack();

                                                                }
                                                            }, vMessageResponse);
                                                    ds.setCancelable(false);
                                                    ds.show(getFragmentManager(),
                                                            "");
                                                }

                                            }
                                        });
                                as.execute();

                                // AddOrderAPI();
                            }

                        }

                    }
                }
                // else if (!(CommonData.ispayment)) {
                //
                // show_message(getActivity().getResources()
                // .getText(R.string.paymentValidation).toString());
                //
                // }

                // topInterface.topbarClick("back");
            }
        });

        cboxsameasabove.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // if (cboxsameasabove.isChecked()) {
                //
                // billingstreetnumber.setText(streetnumber.getText()
                // .toString());
                // billingstreetaddress.setText(streetaddress.getText()
                // .toString());
                // billingcityname.setText(cityname.getText().toString());
                // billingzipcode.setText(zipcode.getText().toString());
                //
                // } else {
                //
                // billingstreetnumber.setText("");
                // billingstreetaddress.setText("");
                // billingcityname.setText("");
                // billingzipcode.setText("");
                //
                // }

                if (cboxsameasabove.isChecked()) {

                    lnbillingselect.setVisibility(View.GONE);
                    if (streetnumber.getText().toString().trim().length() == 0) {
                        cboxsameasabove.setChecked(false);
                        lnbillingselect.setVisibility(View.VISIBLE);
                        show_message(getActivity().getResources()
                                .getText(R.string.streetnumbervalidation)
                                .toString());

                    } else if (streetaddress.getText().toString().trim()
                            .length() == 0) {
                        cboxsameasabove.setChecked(false);
                        lnbillingselect.setVisibility(View.VISIBLE);
                        show_message(getActivity().getResources()
                                .getText(R.string.streetaddressvalidation)
                                .toString());

                    } else if (cityname.getText().toString().trim().length() == 0) {
                        cboxsameasabove.setChecked(false);
                        lnbillingselect.setVisibility(View.VISIBLE);
                        show_message(getActivity().getResources()
                                .getText(R.string.citytownvalidation)
                                .toString());

                    } else if (lblstate.getText().toString().trim().length() == 0
                            && globalClass.languageId.equalsIgnoreCase("1")) {
                        cboxsameasabove.setChecked(false);
                        lnbillingselect.setVisibility(View.VISIBLE);
                        show_message(getActivity().getResources()
                                .getText(R.string.shippingstatevalidate)
                                .toString());
                    } else if (zipcode.getText().toString().trim().length() == 0) {
                        cboxsameasabove.setChecked(false);
                        lnbillingselect.setVisibility(View.VISIBLE);
                        show_message(getActivity().getResources()
                                .getText(R.string.zipcodevalidation).toString());
                    } else if (zipcode.getText().toString().trim().length() < 4) {
                        cboxsameasabove.setChecked(false);
                        lnbillingselect.setVisibility(View.VISIBLE);
                        show_message(getActivity().getResources()
                                .getText(R.string.zipcodevalidate).toString());
                    } else if (globalClass.languageId.equalsIgnoreCase("1")) {

                        if (lblstate.getText().toString().trim().length() == 0) {
                            cboxsameasabove.setChecked(false);
                            lnbillingselect.setVisibility(View.VISIBLE);
                            show_message(getActivity().getResources()
                                    .getText(R.string.shippingstatevalidate)
                                    .toString());
                        } else {

                            Save_in_SharePrefrerence();

                            billingstreetnumber.setText(streetnumber.getText()
                                    .toString());
                            billingstreetaddress.setText(streetaddress
                                    .getText().toString());
                            billingcityname.setText(cityname.getText()
                                    .toString());
                            billingzipcode
                                    .setText(zipcode.getText().toString());
                            txtbillingstate.setText(lblstate.getText()
                                    .toString().trim());

                            billingstreetnumber.setEnabled(false);
                            billingstreetaddress.setEnabled(false);
                            billingcityname.setEnabled(false);
                            billingzipcode.setEnabled(false);

                            txtbillingstate.setEnabled(false);
                            lnbillingstate.setEnabled(false);

                        }

                    } else {

                        Save_in_SharePrefrerence();

                        billingstreetnumber.setText(streetnumber.getText()
                                .toString());
                        billingstreetaddress.setText(streetaddress.getText()
                                .toString());
                        billingcityname.setText(cityname.getText().toString());
                        billingzipcode.setText(zipcode.getText().toString());
                        txtbillingstate.setText(lblstate.getText().toString()
                                .trim());

                        billingstreetnumber.setEnabled(false);
                        billingstreetaddress.setEnabled(false);
                        billingcityname.setEnabled(false);
                        billingzipcode.setEnabled(false);

                        txtbillingstate.setEnabled(false);
                        lnbillingstate.setEnabled(false);
                    }
                } else {

                    lnbillingselect.setVisibility(View.VISIBLE);

                    billingstreetnumber.setText("");
                    billingstreetaddress.setText("");
                    billingcityname.setText("");
                    billingzipcode.setText("");
                    txtbillingstate.setText("");

                    billingstreetnumber.setEnabled(true);
                    billingstreetaddress.setEnabled(true);
                    billingcityname.setEnabled(true);
                    billingzipcode.setEnabled(true);

                    txtbillingstate.setEnabled(true);
                    lnbillingstate.setEnabled(true);

                }

            }
        });

        rewardcheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                // TODO Auto-generated method stub

                // Toast.makeText(getActivity(), "" + rewardamountprice, 9000)
                // .show();
                if (isChecked) {

                    if (bRewardAvailable) {
                        double totalamount = 0.0, totalpriceval = 0.0;

                        DecimalFormat df = new DecimalFormat("############.##");

                        double rewardsamount = 0.0;
                        if (rewardamountprice.equals("")) {
                            rewardsamount = 0.0;
                        } else {
                            rewardsamount = Double
                                    .parseDouble(rewardamountprice);
                        }

                        rewardprice.setText(symbol + df.format(rewardsamount));

                        if (!((orderquantity.getText().toString().trim()
                                .equals("")))) {

                            int quantity = Integer.parseInt(orderquantity
                                    .getText().toString().trim());

                            if (deallimit >= quantity) {

                                String discountpriceval = txtdiscountprice
                                        .getText().toString().trim();

                                discountprice
                                        .setText(orderquantity.getText()
                                                .toString()
                                                + " X "
                                                + txtdiscountprice.getText()
                                                .toString());

                                double quantitycount = Double
                                        .parseDouble(orderquantity.getText()
                                                .toString());
                                String discountval = discountpriceval.replace(
                                        symbol, "");
                                double discounttotalprice = Double
                                        .parseDouble(discountval);
                                totalpriceval = quantitycount
                                        * discounttotalprice;
                                totalamount = totalpriceval;

                                if (bRewardAvailable) {

                                    Log.e("called", "yes");

                                    if (Double.parseDouble(rewardamountprice) > ((totalpriceval * (Double.parseDouble(dealdetail.vMaximumRewardsUsablePercentage))) / 100)) {

                                        totalamount = totalpriceval
                                                - ((totalpriceval * (Double.parseDouble(dealdetail.vMaximumRewardsUsablePercentage))) / 100);
                                        rewardamountprice = ""
                                                + (Double
                                                .parseDouble(rewardamountprice) - (totalpriceval * (Double.parseDouble(dealdetail.vMaximumRewardsUsablePercentage))) / 100);

                                    } else {
                                        totalamount = totalpriceval
                                                - (Double
                                                .parseDouble(rewardamountprice));
                                        rewardamountprice = "0";
                                    }


                                    if (totalamount < 0) {
                                        totalamount = 0.0;
                                    }
                                } else {
                                    Log.e("called", "no");
                                    totalamount = totalpriceval;
                                }

                                Log.e("Total Amount=", "" + totalamount
                                        + "=====" + totalpriceval);

                                edttotalprice.setText(""
                                        + df.format(totalamount));

                                Double rewarss = totalpriceval - totalamount;

                                rewardprice.setText(symbol + df.format(rewarss));
                                rewardpointslbl.setText(getActivity()
                                        .getResources().getString(
                                                R.string.rewardpoints)
                                        + " " + symbol + "" + round(Double.parseDouble(rewardamountprice), 2));

                            }

                            DialogFragment ds = new SingleButtonAlert(
                                    new DialogInterfaceClick() {

                                        @Override
                                        public void dialogClick(String tag) {


                                        }
                                    }, dealdetail.vMaximumRewardsUsablePercentageMessage);
                            ds.setCancelable(false);
                            ds.show(getFragmentManager(), "");

                        } else {
                            edttotalprice.setText("0.0");
                            discountprice
                                    .setText(getActivity().getResources()
                                            .getString(R.string.currencySymbol)
                                            + "0.0");
                        }

                        if (bRewardAvailable) {

                            if (Double.parseDouble(rewardamountprice) > totalpriceval) {

                            }
                            rewardpriceblog.setVisibility(View.VISIBLE);
                            rewardprice.setVisibility(View.VISIBLE);
                            rewardpricedevider.setVisibility(View.VISIBLE);
                        }

                    } else {
                        // Toast.makeText(
                        // getActivity(),
                        // getResources().getString(
                        // R.string.norewardspoints), 9000).show();

                    }
                } else {
                    rewardpriceblog.setVisibility(View.GONE);
                    rewardprice.setVisibility(View.GONE);
                    rewardpricedevider.setVisibility(View.GONE);


                    if (!((orderquantity.getText().toString().trim().equals("")))) {

                        int quantity = Integer.parseInt(orderquantity.getText()
                                .toString().trim());

                        if (deallimit >= quantity) {

                            double totalamount = 0.0;

                            String discountpriceval = txtdiscountprice
                                    .getText().toString().trim();

                            discountprice.setText(orderquantity.getText()
                                    .toString()
                                    + " X "
                                    + txtdiscountprice.getText().toString());

                            double quantitycount = Double
                                    .parseDouble(orderquantity.getText()
                                            .toString());
                            String discountval = discountpriceval.replace(
                                    symbol, "");
                            double discounttotalprice = Double
                                    .parseDouble(discountval);
                            double totalpriceval = quantitycount
                                    * discounttotalprice;
                            totalamount = totalpriceval;

                            String rawards = rewardprice.getText().toString()
                                    .replace(symbol, "");

                            rewardamountprice = ""
                                    + (Double.parseDouble(rewardamountprice) + (Double
                                    .parseDouble(rawards)));

                            rewardamountprice = reward;

                            rewardpointslbl.setText(getActivity()
                                    .getResources().getString(
                                            R.string.rewardpoints)
                                    + " " + symbol + round(Double.parseDouble(rewardamountprice), 2));

                            DecimalFormat df = new DecimalFormat(
                                    "############.##");

                            edttotalprice.setText("" + df.format(totalamount));
                        }

                    } else {
                        edttotalprice.setText("0.0");
                        discountprice.setText(getActivity().getResources()
                                .getString(R.string.currencySymbol) + "0.0");
                    }
                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        dealInterface = (DealFeedDetailInterface) activity;
        topInterface = (topBarInteface) activity;
    }

    public void show_message(String message) {

        DialogFragment ds = new SingleButtonAlert(new DialogInterfaceClick() {

            @Override
            public void dialogClick(String tag) {
                // TODO Auto-generated method stub
            }
        }, message);
        ds.show(getFragmentManager(), "");
        ds.setCancelable(false);

    }

    public void Save_in_SharePrefrerence() {

        SharedPreferences.Editor editor = getActivity().getPreferences(
                Context.MODE_PRIVATE).edit();
        editor.putString("shippingstreetnumber", streetnumber.getText()
                .toString());
        editor.putString("shippingstreetaddress", streetaddress.getText()
                .toString());
        editor.putString("shippingcity", cityname.getText().toString());
        editor.putString("shippingzip", zipcode.getText().toString());
        editor.putString("shippingstate", lblstate.getText().toString().trim());

        editor.putString("billingstreetnumber", billingstreetnumber.getText()
                .toString());
        editor.putString("billingstreetaddress", billingstreetaddress.getText()
                .toString());
        editor.putString("billingcity", billingcityname.getText().toString());
        editor.putString("billingzip", billingzipcode.getText().toString());
        editor.putString("billingstate", txtbillingstate.getText().toString()
                .trim());

        editor.commit();

    }

    public void get_set_From_SharePrefrerence() {

        prefs = getActivity().getPreferences(Context.MODE_PRIVATE);

        streetnumber.setText(prefs.getString("shippingstreetnumber", ""));
        streetaddress.setText(prefs.getString("shippingstreetaddress", ""));
        cityname.setText(prefs.getString("shippingcity", ""));
        zipcode.setText(prefs.getString("shippingzip", ""));
        lblstate.setText(prefs.getString("shippingstate", ""));

        billingstreetnumber.setText(prefs.getString("billingstreetnumber", ""));
        billingstreetaddress.setText(prefs
                .getString("billingstreetaddress", ""));
        billingcityname.setText(prefs.getString("billingcity", ""));
        billingzipcode.setText(prefs.getString("billingzip", ""));
        txtbillingstate.setText(prefs.getString("billingstate", ""));

    }

    public void OrderApi(final String orderquantityval,
                         final String streetnumberval, final String streetaddressval,
                         final String citynameval, final String zipcodeval,
                         final String billingstreetnumberval,
                         final String billingcitynameval,
                         final String billingstreetaddressval,
                         final String billingzipcodeval, final String cardtype,
                         final String nameoncard, final String creditcardno,
                         final String monthexp, final String yearexp, final String cvv,
                         final String Types, final String shippingstatename,
                         final String billingstatename) {

        async_deel_feed_list as = new async_deel_feed_list(
                new asynctaskloaddata() {

                    String urlResponsereward = "";

                    @Override
                    public void onPreExecute() {
                        topInterface.showDialog("Loading");
                    }

                    @SuppressWarnings("deprecation")
                    @Override
                    public void doInBackground() {

                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                        nameValuePairs.add(new BasicNameValuePair("customerid",
                                "" + globalClass.CustomerId));
                        nameValuePairs.add(new BasicNameValuePair("languageid",
                                "" + globalClass.languageId));
                        nameValuePairs.add(new BasicNameValuePair("dealid", ""
                                + dealdetail.DealId));
                        nameValuePairs.add(new BasicNameValuePair("locationid",
                                "" + dealdetail.locationId));
                        nameValuePairs.add(new BasicNameValuePair("quantity",
                                "" + orderquantityval));

                        nameValuePairs.add(new BasicNameValuePair(
                                "ordertempid", "" + dealdetail.iOrderTempId));

                        nameValuePairs.add(new BasicNameValuePair(
                                "purchasedamount", ""
                                + edttotalprice.getText().toString()));

                        // edttotalprice.getText().toString().replaceAll("$",
                        // "");

                        nameValuePairs.add(new BasicNameValuePair(
                                "shippingstreetnumber", "" + streetnumberval));
                        nameValuePairs
                                .add(new BasicNameValuePair(
                                        "shippingstreetaddress", ""
                                        + streetaddressval));
                        nameValuePairs.add(new BasicNameValuePair(
                                "shippingcityname", "" + citynameval));
                        nameValuePairs.add(new BasicNameValuePair(
                                "shippingzipcode", "" + zipcodeval));
                        nameValuePairs.add(new BasicNameValuePair(
                                "billingstreetnumber", ""
                                + billingstreetnumberval));
                        nameValuePairs.add(new BasicNameValuePair(
                                "billingstreetcityname", ""
                                + billingcitynameval));
                        nameValuePairs.add(new BasicNameValuePair(
                                "billingstreetaddress", ""
                                + billingstreetaddressval));
                        nameValuePairs.add(new BasicNameValuePair(
                                "billingzipcode", "" + billingzipcodeval));

                        nameValuePairs.add(new BasicNameValuePair(
                                "billingstatename", "" + billingstatename));

                        nameValuePairs.add(new BasicNameValuePair(
                                "shippingstatename", "" + shippingstatename));

                        nameValuePairs.add(new BasicNameValuePair(
                                "transactionid", ""));

                        if (Types.equalsIgnoreCase("free")) {
                            CommonData.Type = "";
                            CommonData.credit_card_holder = "";
                            CommonData.credit_card = "";
                            CommonData.Expiry_month = "";
                            CommonData.Expiry_Year = "";
                            CommonData.CVV = "";

                        }

                        Log.e("customerid", "" + globalClass.CustomerId);
                        Log.e("languageid", "" + globalClass.languageId);
                        Log.e("dealid", "" + dealdetail.DealId);
                        Log.e("locationid", "" + dealdetail.locationId);
                        Log.e("quantity", "" + orderquantityval);
                        Log.e("purchasedamount", ""
                                + edttotalprice.getText().toString());
                        Log.e("shippingstreetnumber", "" + streetnumberval);
                        Log.e("shippingstreetaddress", "" + streetaddressval);
                        Log.e("shippingcityname", "" + citynameval);
                        Log.e("shippingzipcode", "" + zipcodeval);
                        Log.e("billingstreetnumber", ""
                                + billingstreetnumberval);
                        Log.e("billingstreetcityname", "" + billingcitynameval);
                        Log.e("billingstreetaddress", ""
                                + billingstreetaddressval);
                        Log.e("billingzipcode", "" + billingzipcodeval);



                        urlResponsereward = web_service.makeorder(
                                "OrderService.svc/AddOrder", nameValuePairs);
                        Log.e("called url response===", urlResponsereward);

                        try {
                            JSONObject jobj = new JSONObject(urlResponsereward);

                            vStatusCode = jobj.getString("vStatusCode");
                            if (vStatusCode.equals("100")) {

                                Save_in_SharePrefrerence();

                                vMessageResponse = jobj
                                        .getString("vMessageResponse");

                            } else if (vStatusCode.equals("122")
                                    || (vStatusCode.equals("121"))
                                    || (vStatusCode.equals("123"))
                                    || (vStatusCode.equals("124"))
                                    || (vStatusCode.equals("125"))) {
                                vMessageResponse = jobj
                                        .getString("vMessageResponse");

                            } else if (vStatusCode.equals("102")) {
                                vMessageResponse = jobj
                                        .getString("vMessageResponse");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onPostExecute() {

                        topInterface.hideDialog();

                        if (vMessageResponse.trim().length() <= 0) {
                            vMessageResponse = urlResponsereward;
                        }
                        DialogFragment ds = new SingleButtonAlert(
                                new DialogInterfaceClick() {

                                    @Override
                                    public void dialogClick(String tag) {

                                        View view = getActivity()
                                                .getCurrentFocus();
                                        if (view != null) {
                                            InputMethodManager inputManager = (InputMethodManager) getActivity()
                                                    .getSystemService(
                                                            Context.INPUT_METHOD_SERVICE);
                                            inputManager
                                                    .hideSoftInputFromWindow(
                                                            view.getWindowToken(),
                                                            InputMethodManager.HIDE_NOT_ALWAYS);
                                        }
                                        getFragmentManager().popBackStack();

                                    }
                                }, vMessageResponse);
                        ds.setCancelable(false);
                        ds.show(getFragmentManager(), "");

                    }
                });
        as.execute();
    }

    public void setDefaultPrice(String s) {

        int quantity = Integer.parseInt(s.toString().trim());
        // Log.e("Deafult price===", s.toString().trim() + deallimit);

        if (deallimit >= quantity) {

            double totalamount = 0.0;

            String discountpriceval = txtdiscountprice.getText().toString()
                    .trim();
            int discprice = Integer.valueOf(s.toString());

            discountprice.setText("" + 1 + " X "
                    + txtdiscountprice.getText().toString());

            double quantitycount = Double.parseDouble(s.toString());
            String discountval = discountpriceval.replace(symbol, "");
            double discounttotalprice = Double.parseDouble(discountval);
            double totalpriceval = quantitycount * discounttotalprice;

            if (rewardcheck.isChecked()) {
                if (bRewardAvailable) {

                    totalamount = totalpriceval
                            - ((Double.parseDouble(rewardamountprice) * (Double.parseDouble(dealdetail.vMaximumRewardsUsablePercentage))) / 100);
                    if (totalamount < 0) {
                        totalamount = 0.0;
                    }
                } else {
                    totalamount = totalpriceval;
                }
            } else {
                totalamount = totalpriceval;
            }

            DecimalFormat df = new DecimalFormat("############.##");
            Log.e("Reward points", "+" + rewardamountprice + "==="
                    + totalpriceval);
            if (Double.parseDouble(rewardamountprice) > totalpriceval) {
                rewardprice.setText(symbol + df.format(totalpriceval));
            }
            Log.d("set total amount>>", "" + totalamount);
            edttotalprice.setText("" + df.format(totalamount));
        }
    }

    public void AddOrderAPI(final String orderquantityval,
                            final String streetnumberval, final String streetaddressval,
                            final String citynameval, final String zipcodeval,
                            final String billingstreetnumberval,
                            final String billingcitynameval,
                            final String billingstreetaddressval,
                            final String billingzipcodeval, final String cardtype,
                            final String nameoncard, final String creditcardno,
                            final String monthexp, final String yearexp, final String cvv,
                            final String Typess, final String shippingstatename,
                            final String billingstatename, final String amount) {
        String msg = "";

        if (Double.parseDouble(amount) > 0) {

            msg = getResources().getString(R.string.txtmsg_paiddeal);
            orderType = "paid";

            DialogFragment ds = new ContinueButtonAlert(
                    new DialogInterfaceClick() {

                        @Override
                        public void dialogClick(String tag) {

                            if (tag.equalsIgnoreCase("continue")) {

                                dealdetail.orderquantityval = orderquantityval;

                                dealdetail.streetnumberval = streetnumberval;
                                dealdetail.streetaddressval = streetaddressval;
                                dealdetail.citynameval = citynameval;
                                dealdetail.billingzipcodeval = billingzipcodeval;
                                dealdetail.billingstreetnumberval = billingstreetnumberval;
                                dealdetail.billingcitynameval = billingcitynameval;

                                dealdetail.billingstreetaddressval = billingstreetaddressval;
                                dealdetail.zipcodeval = zipcodeval;

                                try {

                                    dealdetail.shippingstatename = lblstate
                                            .getText().toString().trim();
                                    dealdetail.billingstatename = txtbillingstate
                                            .getText().toString().trim();

                                } catch (Exception e) {
                                    // TODO: handle
                                    // exception
                                }

                                if (Double.parseDouble(dealdetail.TotalAmount) > 0) {

                                    dealInterface.dealFeedClick("StripePayment",dealdetail);
//                                    dealInterface.dealFeedClick("Payment",
//                                            dealdetail);

                                } else {
                                    OrderApi(orderquantityval, streetnumberval,
                                            streetaddressval, citynameval,
                                            billingzipcodeval,
                                            billingstreetnumberval,
                                            billingcitynameval,
                                            billingstreetaddressval,
                                            billingzipcodeval, "", "", "", "",
                                            "", "", Types, shippingstatename,
                                            billingstatename);
                                }

                                // OrderApi(
                                // orderquantityval,
                                // streetnumberval,
                                // streetaddressval,
                                // citynameval,
                                // billingzipcodeval,
                                // billingstreetnumberval,
                                // billingcitynameval,
                                // billingstreetaddressval,
                                // billingzipcodeval,
                                // CommonData.Type,
                                // CommonData.credit_card_holder,
                                // CommonData.credit_card,
                                // CommonData.Expiry_month,
                                // CommonData.Expiry_Year,
                                // CommonData.CVV,
                                // Types);

                            } else {
                                CancelOrder();

                            }
                        }
                    }, msg);
            ds.show(getFragmentManager(), "");
            ds.setCancelable(false);

        } else {
            msg = getResources().getString(R.string.txtmsg_freedeal);
            orderType = "free";

            DialogFragment ds = new ContinueButtonAlert(
                    new DialogInterfaceClick() {

                        @Override
                        public void dialogClick(String tag) {

                            if (tag.equalsIgnoreCase("continue")) {

                                OrderApi(orderquantityval, streetnumberval,
                                        streetaddressval, citynameval,
                                        billingzipcodeval,
                                        billingstreetnumberval,
                                        billingcitynameval,
                                        billingstreetaddressval,
                                        billingzipcodeval, "", "", "", "", "",
                                        "", Types, shippingstatename,
                                        billingstatename);

                            } else {
                                CancelOrder();
                            }
                        }
                    }, msg);
            ds.show(getFragmentManager(), "");
            ds.setCancelable(false);

        }

    }

    public void CancelOrder() {

        async_deel_feed_list as = new async_deel_feed_list(
                new asynctaskloaddata() {

                    String urlResponsereward = "";

                    @Override
                    public void onPreExecute() {

                    }

                    @SuppressWarnings("deprecation")
                    @Override
                    public void doInBackground() {

                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                        nameValuePairs.add(new BasicNameValuePair("customerid",
                                "" + globalClass.CustomerId));
                        nameValuePairs.add(new BasicNameValuePair("languageid",
                                "" + globalClass.languageId));

                        nameValuePairs.add(new BasicNameValuePair(
                                "ordertempid", "" + dealdetail.iOrderTempId));

                        urlResponsereward = web_service.makeorder(
                                "OrderService.svc/DeleteOrderTemp",
                                nameValuePairs);
                        Log.e("called url response===", urlResponsereward);

                        try {
                            JSONObject jobj = new JSONObject(urlResponsereward);

                            vStatusCode = jobj.getString("vStatusCode");
                            if (vStatusCode.equals("100")) {

                                vMessageResponse = jobj
                                        .getString("vMessageResponse");

                            } else {
                                vMessageResponse = jobj
                                        .getString("vMessageResponse");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onPostExecute() {
                        topInterface.topbarClick("back");

                    }
                });
        as.execute();

    }

    public static double round(double value, int places) {
        if (places < 0)
            throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


}
