package com.mimcoups.Consumer.fragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.Facebook;
import com.google.android.gms.plus.PlusShare;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.Consumer.customClass.LocationDetail;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.inteface.LocationDetailInterface;
import com.mimcoups.Consumer.inteface.locationmapinterface;
import com.mimcoups.R;
import com.mimcoups.Consumer.Dialog.QuickAction;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.DealFeedAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.inteface.sharinglocation;
import com.squareup.picasso.Picasso;

public class FragmentSliderLocationDetail extends Fragment {

	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	ImageView IMG_Share;
	Dialog ImageDialog;
	private LinearLayout ll_LocDetail_Discuss, ll_LocDetail_Contact,
			ll_LocDetail_Connect, ll_LocDetail_Showdeal,
			dealdetailsharingoption;
	private TextView txt_LocDetail_writereview,
			txt_LocationName_LocationDetail, txt_Locationdesk_LocationDetail,
			txtLocAddress_LocationDetail, txt_locEmail_LocationDetail,
			txt_LocDistance_LocationDetail, txt_DealCount_LocationDetail,
			txtLocationName;
	private ImageView imgLocationdetailBack, imgLocationdetailMap,
			imglocImage_locationdetail;
	private CheckBox chk_locationfav;
	private topBarInteface topinteface;
	private LocationDetailInterface locationDetailClick;
	LinearLayout lnldfacebookcontainer, lnldtwittercontainer,
			lnldgooglepluscontainer, lnldinstagramcontainer,lnshowdeals;
	String serverResponse;
	String share = "";

	WebService_Call call_webservice_SliderLocationDetail;
	Context context;
	LocationDetail locationdetail;
	CommonData globalClass;
	private LocationList LocationsliderPassDetail;
	private String statusCode = "", statusMessage = "";
	LinearLayout lnldmimcoupcontainer;
	sharinglocation sharinglocation;
	LinearLayout lnldmailcontainer;
	private Facebook mFaceBook;
	RatingBar ratingBar_locationdetail;
	private locationmapinterface locationinterface;
	RelativeLayout relmainfragment_slider_location_detail;
	LinearLayout lnMainScrollLayout;
	Animation animFadein, animFadeOut;
	ScrollView locationdetailscorll;
	String locationStoreId;
	DealFeedDetail dealfeed;
	String from;
	private File FileImage;

	private static final String[] PERMISSIONS = new String[] {
			"publish_stream", "read_stream", "offline_access" };


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_location_detail,
				container, false);
		globalClass = (CommonData) getActivity().getApplicationContext();


		String dir = Environment.getExternalStorageDirectory() + File.separator
				+ "." + context.getResources().getString(R.string.app_name);
		FileImage = new File(dir, "2.jpg");

		initControls(view);

		from = getArguments().getString("from");

		Check_from_where_i_come(from);
		if (from.equalsIgnoreCase("dealsdetail")) {

			dealfeed = (DealFeedDetail) getArguments().getSerializable(
					"LocationDetail");

			LocationsliderPassDetail = new LocationList();

			locationStoreId = dealfeed.iLocationId;

		} else {
			LocationsliderPassDetail = (LocationList) getArguments()
					.getSerializable("LocationDetail");
			locationStoreId = LocationsliderPassDetail.iMerchantStoreId;
		}

		if (CommonData.languageId.equalsIgnoreCase("1")) {
			share = CommonData.locationshareUrl_au
					+ locationStoreId;

		} else {
			share = CommonData.locationshareUrl_no
					+ locationStoreId;
		}

		// txtLocationName.setText(LocationsliderPassDetail.vMerchantStoreName);
		if (!globalClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
							topinteface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			GetData();
		}
		OnClickEvents(view);
		return view;
	}

	private void Check_from_where_i_come(String from) {
		if (from.equals("map")) {
			imgLocationdetailMap.setVisibility(View.GONE);
		} else {
			imgLocationdetailMap.setVisibility(View.VISIBLE);
		}
	}

	private void OnClickEvents(View view) {
		// TODO Auto-generated method stub


		lnshowdeals.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (from.equalsIgnoreCase("dealsdetail")) {

						locationDetailClick.loncationClick(
								"showDeals_DealDetails",
								LocationsliderPassDetail);

					} else {

						locationDetailClick.loncationClick("showDeals",
								LocationsliderPassDetail);
					}

				}
			}
		});

		locationdetailscorll.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub

				if (dealdetailsharingoption.getVisibility() == View.VISIBLE) {
					dealdetailsharingoption.setAnimation(animFadeOut);
					dealdetailsharingoption.startAnimation(animFadeOut);
					dealdetailsharingoption.setVisibility(View.GONE);
				}
				return false;
			}
		});

		relmainfragment_slider_location_detail
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		lnMainScrollLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (dealdetailsharingoption.getVisibility() == View.VISIBLE) {
					dealdetailsharingoption.setAnimation(animFadeOut);
					dealdetailsharingoption.startAnimation(animFadeOut);
					dealdetailsharingoption.setVisibility(View.GONE);
				}
			}
		});

		lnldmimcoupcontainer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				sharinglocation.onsharelocationclick("mimcup",
						LocationsliderPassDetail);

			}
		});

		IMG_Share.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (dealdetailsharingoption.getVisibility() == View.GONE) {
					// IMG_Share
					// .setImageResource(R.drawable.header_icon_share_selected);
					dealdetailsharingoption.setVisibility(View.VISIBLE);
					dealdetailsharingoption.setAnimation(animFadein);
					dealdetailsharingoption.startAnimation(animFadein);
				} else {
					// IMG_Share.setImageResource(R.drawable.header_icon_share);

					dealdetailsharingoption.setAnimation(animFadeOut);
					dealdetailsharingoption.startAnimation(animFadeOut);
					dealdetailsharingoption.setVisibility(View.GONE);
				}

			}
		});
		ll_LocDetail_Discuss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();

				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					locationDetailClick.loncationClick("discuss",
							LocationsliderPassDetail);
				}
			}
		});

		ll_LocDetail_Connect.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					ImageDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_PopUpMenu_Center;
					ImageDialog.show();
				}

			}
		});
		ll_LocDetail_Contact.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					locationDetailClick.loncationClick("locationContact",
							LocationsliderPassDetail);
				}
			}
		});
		ll_LocDetail_Showdeal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// FragmentTransaction transaction = getFragmentManager()
				// .beginTransaction();
				// getFragmentManager().popBackStack(null,
				// FragmentManager.POP_BACK_STACK_INCLUSIVE);
				// Fragment newFragment = new FragmentDealFeed();
				// String strFragmentTag = newFragment.toString();
				// transaction.replace(R.id.frmDealFeed, newFragment,
				// strFragmentTag);
				//
				// transaction.commit();

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (from.equalsIgnoreCase("dealsdetail")) {

						locationDetailClick.loncationClick(
								"showDeals_DealDetails",
								LocationsliderPassDetail);

					} else {

						locationDetailClick.loncationClick("showDeals",
								LocationsliderPassDetail);
					}

				}
			}
		});
		txt_LocDetail_writereview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					locationDetailClick.loncationClick("writereview",
							LocationsliderPassDetail);
				}
			}
		});

		imgLocationdetailBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topinteface.topbarClick("back");
			}
		});

		imgLocationdetailMap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// locationDetailClick.loncationClick("locationMap",
				// LocationsliderPassDetail);

				// Log.d("LocationsliderPassDetail", "" +
				// LocationsliderPassDetail);
				topinteface.showDialog("Loading");
				Handler mhandler = new Handler();
				mhandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						// topinteface.hideDialog();
						// Log.d("move screen", "move screen");
						locationinterface
								.go_to_map_screen_single_icon(LocationsliderPassDetail);
						topinteface.hideDialog();
					}
				}, 3000);

			}
		});

		lnldfacebookcontainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent i = new Intent(getActivity(), facebookshare.class);
				// i.putExtra("sharingfrom", 2);
				// startActivity(i);

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					if (dealdetailsharingoption.getVisibility() == View.VISIBLE) {
						dealdetailsharingoption.setAnimation(animFadeOut);
						dealdetailsharingoption.startAnimation(animFadeOut);
						dealdetailsharingoption.setVisibility(View.GONE);
					}

					onFacebookClick();

				}
			}
		});

		lnldgooglepluscontainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Intent iv = new Intent(getActivity(), SignInActivity.class);
				// iv.putExtra("sharingfrom", 2);
				// startActivity(iv);
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					Intent shareIntent = new PlusShare.Builder(getActivity())
							.setType("text/plain")
							.setText(
									"MiMCoups APP:: \n "
											+ LocationsliderPassDetail.vMerchantStoreName
											+ "\n "
											+ LocationsliderPassDetail.vShortDescription)
							.setContentUrl(Uri.parse(share))
							// .setContentUrl(
							// Uri.parse(locationdetail.vStoreDetailImage_A_650X325))
							.getIntent();
					startActivityForResult(shareIntent, 0);

				}

			}
		});

		lnldinstagramcontainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Intent i = new Intent(getActivity(), InstaActivity.class);
				// i.putExtra("sharingfrom", 2);
				// startActivity(i);

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (!appInstalledOrNot()) {

						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
										// topinteface.topbarClick("back");
									}
								}, getResources().getString(
								R.string.InstallInstagram));
						ds.show(getFragmentManager(), "");
						ds.setCancelable(false);

						return;
					} else {

						new getbitmap().execute();


//						Intent shareIntent = new Intent(
//								android.content.Intent.ACTION_SEND);
//						shareIntent.setType("image/*"); // set mime type
//						Uri path = Uri.parse("android.resource://com.mimcoups/"
//								+ R.drawable.app_icon);
//						shareIntent
//								.putExtra(
//										Intent.EXTRA_TEXT,
//										"MimCoup APP:: \n "
//												+ LocationsliderPassDetail.vMerchantStoreName
//												+ "\n " + share);
//						shareIntent.putExtra(Intent.EXTRA_STREAM, path); // set
//																			// uri
//						shareIntent.setPackage("com.instagram.android");
//						startActivity(shareIntent);

					}
				}

			}
		});

		lnldtwittercontainer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					try {

						Uri uri = Uri
								.fromFile(new File(
										LocationsliderPassDetail.vStoreDetailImage_A_650X325));
						Intent tweetIntent = new Intent(Intent.ACTION_SEND);
						// tweetIntent
						// .putExtra(
						// Intent.EXTRA_TEXT,
						// LocationsliderPassDetail.vMerchantStoreName
						// + "\n "
						// + LocationsliderPassDetail.vShortDescription);
						tweetIntent.putExtra(Intent.EXTRA_TEXT,
								LocationsliderPassDetail.vMerchantStoreName
										+ "\n " + share);
						tweetIntent.putExtra(Intent.EXTRA_STREAM, uri);
						tweetIntent.setType("*/*");

						PackageManager packManager = context
								.getPackageManager();
						List<ResolveInfo> resolvedInfoList = packManager
								.queryIntentActivities(tweetIntent,
										PackageManager.MATCH_DEFAULT_ONLY);

						boolean resolved = false;
						for (ResolveInfo resolveInfo : resolvedInfoList) {
							if (resolveInfo.activityInfo.packageName
									.startsWith("com.twitter.android")) {
								tweetIntent.setClassName(
										resolveInfo.activityInfo.packageName,
										resolveInfo.activityInfo.name);
								resolved = true;
								break;
							}
						}
						if (resolved) {
							context.startActivity(tweetIntent);
						} else {
							try {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("market://details?id=com.twitter.android")));
							} catch (android.content.ActivityNotFoundException anfe) {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android")));
							}
						}

						// Uri uri = Uri.parse(dealdetail.DealImageUrl);
						// // Uri uri =
						// //
						// Uri.parse("android.resource://com.gobaby.app/drawable/back");
						// Intent intent = new Intent(Intent.ACTION_SEND);
						// intent.setType("/*");
						// intent.setClassName("com.twitter.android",
						// "com.twitter.android.PostActivity");
						// intent.putExtra(Intent.EXTRA_TEXT,
						// dealdetail.dealName
						// + " With " + dealdetail.vSavingamount + "\n\n "
						// + dealdetail.vShortDescription);
						// intent.putExtra(Intent.EXTRA_STREAM, uri);
						// context.startActivity(intent);

					} catch (Exception e) {

					}

					// Intent tp2 = new Intent(getActivity(),
					// PrepareRequestTokenActivity.class);
					// tp2.putExtra("sharingfrom", 2);
					// tp2.putExtra("dealname",
					// LocationsliderPassDetail.vMerchantStoreName);
					// tp2.putExtra("desc",
					// LocationsliderPassDetail.vShortDescription);
					// tp2.putExtra(
					// "Image",
					// LocationsliderPassDetail.vStoreDetailImage_A_650X325);
					// startActivity(tp2);

				}

			}
		});

		lnldmailcontainer.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				sharinglocation.onsharelocationclick("mail",
						LocationsliderPassDetail);

			}
		});
	}

	private void initControls(View view) {
		Sharing_Dialog();

		animFadein = AnimationUtils
				.loadAnimation(getActivity(), R.anim.fade_in);
		animFadeOut = AnimationUtils.loadAnimation(getActivity(),
				R.anim.fade_out);

		relmainfragment_slider_location_detail = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_location_detail);
		locationdetailscorll = (ScrollView) view
				.findViewById(R.id.locationdetailscorll);
		ratingBar_locationdetail = (RatingBar) view
				.findViewById(R.id.ratingBar_locationdetail);
		lnMainScrollLayout = (LinearLayout) view.findViewById(R.id.ll_topmain);
		txtLocationName = (TextView) view.findViewById(R.id.txtLocationName);
		lnldmimcoupcontainer = (LinearLayout) view
				.findViewById(R.id.lnldmimcoupcontainer);
		lnldmailcontainer = (LinearLayout) view
				.findViewById(R.id.lnldmailcontainer);
		call_webservice_SliderLocationDetail = new WebService_Call();
		imgLocationdetailBack = (ImageView) view
				.findViewById(R.id.imgLocationdetailBack);
		imgLocationdetailMap = (ImageView) view
				.findViewById(R.id.imgLocationdetailmap);
		dealdetailsharingoption = (LinearLayout) view
				.findViewById(R.id.locationdetailshare);
		IMG_Share = (ImageView) view.findViewById(R.id.imglocationdetail_share);

		ll_LocDetail_Discuss = (LinearLayout) view
				.findViewById(R.id.ll_LocDetail_Discuss);
		ll_LocDetail_Contact = (LinearLayout) view
				.findViewById(R.id.ll_LocDetail_Contact);
		ll_LocDetail_Connect = (LinearLayout) view
				.findViewById(R.id.ll_LocDetail_Connect);
		ll_LocDetail_Showdeal = (LinearLayout) view
				.findViewById(R.id.ll_LocDetail_Showdeal);
		txt_LocDetail_writereview = (TextView) view
				.findViewById(R.id.txt_LocDetail_writereview);
		lnldfacebookcontainer = (LinearLayout) view
				.findViewById(R.id.lnldfacebookcontainer);
		lnldgooglepluscontainer = (LinearLayout) view
				.findViewById(R.id.lnldgooglepluscontainer);
		lnldinstagramcontainer = (LinearLayout) view
				.findViewById(R.id.lnldinstagramcontainer);
		lnldtwittercontainer = (LinearLayout) view
				.findViewById(R.id.lnldtwittercontainer);
		imglocImage_locationdetail = (ImageView) view
				.findViewById(R.id.imglocImage_locationdetail);
		txt_LocationName_LocationDetail = (TextView) view
				.findViewById(R.id.txt_LocationName_LocationDetail);
		txt_Locationdesk_LocationDetail = (TextView) view
				.findViewById(R.id.txt_Locationdesk_LocationDetail);
		txtLocAddress_LocationDetail = (TextView) view
				.findViewById(R.id.txtLocAddress_LocationDetail);
		txt_locEmail_LocationDetail = (TextView) view
				.findViewById(R.id.txt_locEmail_LocationDetail);
		txt_LocDistance_LocationDetail = (TextView) view
				.findViewById(R.id.txt_LocDistance_LocationDetail);
		txt_DealCount_LocationDetail = (TextView) view
				.findViewById(R.id.txt_DealCount_LocationDetail);
		chk_locationfav = (CheckBox) view
				.findViewById(R.id.cbLocationFavourite);

		lnshowdeals=(LinearLayout)view.findViewById(R.id.lnshowdeals);
		mFaceBook = new Facebook(globalClass.FACEBOOK_APPID);
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		context = activity;
		topinteface = (topBarInteface) activity;
		locationDetailClick = (LocationDetailInterface) activity;
		sharinglocation = (sharinglocation) activity;
		locationinterface = (locationmapinterface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void Sharing_Dialog() {

		try{
			ImageDialog = new Dialog(getActivity());
			ImageDialog.setContentView(R.layout.share_white_vertical_dialog);
			ImageDialog.setCancelable(true);
			ImageDialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			int divierId = ImageDialog.getContext().getResources()
					.getIdentifier("android:id/titleDivider", null, null);
			View divider = ImageDialog.findViewById(divierId);
			divider.setBackgroundColor(android.graphics.Color.TRANSPARENT);

			Button facebookcontainer = (Button) ImageDialog
					.findViewById(R.id.btnfacebookcontainer);
			Button twittercontainer = (Button) ImageDialog
					.findViewById(R.id.btntwittercontainer);
			Button googlepluscontainer = (Button) ImageDialog
					.findViewById(R.id.btngooglepluscontainer);
			Button instagamcontainer = (Button) ImageDialog
					.findViewById(R.id.btninstagramcontainer);
			Button mimcoupsharing = (Button) ImageDialog
					.findViewById(R.id.btnmimcoupsharing);
			Button emailsharing = (Button) ImageDialog
					.findViewById(R.id.btnemailsharing);

			mimcoupsharing.setVisibility(View.GONE);
			emailsharing.setVisibility(View.GONE);

			facebookcontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (!locationdetail.vFacebookPageURL.equals("")) {
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri
								.parse(locationdetail.vFacebookPageURL));
						startActivity(intent);
					} else {
						Toast.makeText(context, "Sorry, no media link available !",
								Toast.LENGTH_LONG).show();
					}

				}
			});

			twittercontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					if (!locationdetail.vTwitterPageURL.equals("")) {
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri
								.parse(locationdetail.vTwitterPageURL));
						startActivity(intent);
					} else {
						Toast.makeText(context, "Sorry, no media link available !",
								Toast.LENGTH_LONG).show();
					}

				}
			});

			googlepluscontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!locationdetail.vGooglePlusURL.equals("")) {
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri
								.parse(locationdetail.vGooglePlusURL));
						startActivity(intent);
					} else {
						Toast.makeText(context, "Sorry, no media link available !",
								Toast.LENGTH_LONG).show();
					}

				}
			});

			instagamcontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!locationdetail.vInstagramURL.equals("")) {
						Intent intent = new Intent(Intent.ACTION_VIEW, Uri
								.parse(locationdetail.vInstagramURL));
						startActivity(intent);
					} else {
						Toast.makeText(context, "Sorry, no media link available !",
								Toast.LENGTH_LONG).show();
					}

				}
			});

			Button btncancel = (Button) ImageDialog.findViewById(R.id.btnCancel);
			btncancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ImageDialog.dismiss();

				}
			});
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(ImageDialog.getWindow().getAttributes());
			DisplayMetrics displaymetrics = new DisplayMetrics();
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.BOTTOM;
			getActivity().getWindowManager().getDefaultDisplay()
					.getMetrics(displaymetrics);
			ImageDialog.getWindow().setAttributes(lp);

		}catch (Exception e){

		}


	}

	private void GetData() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub

						DeelFeedActivity deal = (DeelFeedActivity) context;

						if (deal.progressbarshowing) {
							topinteface.hideDialog();
						}

						topinteface.showDialog("Loading");
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub

						try {

							String serverUrl = "LocationService.svc/GetLocationDetail?customerid="
									+ globalClass.CustomerId
									+ "&languageid="
									+ globalClass.languageId
									+ "&merchantstoreid="
									+ locationStoreId
									+ "&latitude="
									+ globalClass.currentLatitude
									+ "&longitude="
									+ globalClass.currentLongitude;
							serverResponse = call_webservice_SliderLocationDetail
									.makeServicegetRequest(serverUrl);
							serverResponse = serverResponse
									.replace("\\n", "\n");

							JSONObject js_getres = new JSONObject(
									serverResponse);
							statusCode = js_getres.getString("vStatusCode");
							if (statusCode.equals("100")) {
								JSONObject js = new JSONObject(js_getres
										.getString("Location"));
								locationdetail = new LocationDetail();
								locationdetail.bIsFavorite = js
										.getString("bIsFavorite");
								locationdetail.dDistance = js
										.getString("dDistance");
								locationdetail.dLatitude = js
										.getString("dLatitude");
								locationdetail.dLongitude = js
										.getString("dLongitude");
								locationdetail.iDealCount = js
										.getString("iDealCount");
								locationdetail.iMerchantStoreId = js
										.getString("iMerchantStoreId");
								locationdetail.iMerchantStoreRatings = js
										.getString("iMerchantStoreRatings");
								locationdetail.vAddress = js
										.getString("vAddress");
								locationdetail.vFacebookPageURL = js
										.getString("vFacebookPageURL");
								locationdetail.vGooglePlusURL = js
										.getString("vGooglePlusURL");
								locationdetail.vInstagramURL = js
										.getString("vInstagramURL");
								locationdetail.vMerchantStoreEmail = js
										.getString("vMerchantStoreEmail");
								locationdetail.vMerchantStoreName = js
										.getString("vMerchantStoreName");
								locationdetail.vShortDescription = js
										.getString("vShortDescription");
								locationdetail.vStoreDetailImage_A_650X325 = js
										.getString("vStoreDetailImage_A_650X325");
								locationdetail.vTwitterPageURL = js
										.getString("vTwitterPageURL");
								LocationsliderPassDetail.vStoreDetailImage_A_650X325 = js
										.getString("vStoreDetailImage_A_650X325");
								LocationsliderPassDetail.vStoreListImage_A_70X70 = js
										.getString("vStoreListImage_A_70X70");

								// //location list set Nirmal

								LocationsliderPassDetail.bIsFavorite = locationdetail.bIsFavorite;
								LocationsliderPassDetail.dDistance = locationdetail.dDistance;
								LocationsliderPassDetail.dLatitude = locationdetail.dLatitude;
								LocationsliderPassDetail.dLongitude = locationdetail.dLongitude;
								LocationsliderPassDetail.iDealCount = locationdetail.iDealCount;
								LocationsliderPassDetail.iMerchantStoreId = locationdetail.iMerchantStoreId;
								// LocationsliderPassDetail.iNextIndex=locationdetail.i
								// LocationsliderPassDetail.iRowNumber=locationdetail.i

								LocationsliderPassDetail.vAddress = locationdetail.vAddress;
								LocationsliderPassDetail.vMerchantStoreName = locationdetail.vMerchantStoreName;
								LocationsliderPassDetail.vShortDescription = locationdetail.vShortDescription;
								// LocationsliderPassDetail.vStoreListImage_A_70X70=locationdetail.vSt
								LocationsliderPassDetail.vStoreDetailImage_A_650X325 = locationdetail.vStoreDetailImage_A_650X325;

							} else {
								statusMessage = js_getres
										.getString("vMessageResponse");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						topinteface.hideDialog();

						if (!globalClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
											topinteface.topbarClick("back");
										}
									}, getResources().getString(
									R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topinteface.topbarClick("back");
											}
										}, getResources().getString(
										R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (statusCode.equals("100")) {
								SetData();
							} else {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub

											}
										}, statusMessage);
								ds.show(getFragmentManager(), "");
							}
						}

					}

					private void SetData() {

						ratingBar_locationdetail.setRating(Float
								.parseFloat(locationdetail.iMerchantStoreRatings));
						Picasso.with(context)
								.load(locationdetail.vStoreDetailImage_A_650X325)
								.placeholder(R.drawable.noimage_locationdetail)
								.into(imglocImage_locationdetail);
						txt_LocationName_LocationDetail
								.setText(locationdetail.vMerchantStoreName);
						txt_Locationdesk_LocationDetail
								.setText(locationdetail.vShortDescription);

						// locationdetail.vAddress=locationdetail.vAddress.replaceAll("\\n",
						// "\n");
						txtLocAddress_LocationDetail
								.setText(locationdetail.vAddress);
						txt_locEmail_LocationDetail
								.setText(locationdetail.vMerchantStoreEmail);
						txt_LocDistance_LocationDetail
								.setText(locationdetail.dDistance);
						txt_DealCount_LocationDetail
								.setText(locationdetail.iDealCount);

						if (locationdetail.bIsFavorite.equals("false")) {
							chk_locationfav.setChecked(false);
						} else {
							chk_locationfav.setChecked(true);
						}
						chk_locationfav
								.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

									@Override
									public void onCheckedChanged(
											CompoundButton buttonView,
											boolean isChecked) {
										// TODO Auto-generated method stub
										// locationdetail.bIsFavorite = ""
										// + isChecked;
										// locationFavourite(locationdetail.bIsFavorite);
										((DeelFeedActivity) getActivity()).isDealListingFavourite = false;
										if (locationdetail.bIsFavorite
												.equals("true")) {

											((DeelFeedActivity) getActivity()).isLocationFavourite = true;

										} else {
											((DeelFeedActivity) getActivity()).isLocationFavourite = false;
										}
										locationdetail.bIsFavorite = ""
												+ isChecked;
										locationFavourite(locationdetail.bIsFavorite);
									}
								});
					}
				});
		as.execute();
	}

	private void locationFavourite(final String isChecked) {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						serverResponse = "";
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						try {

							String getUrl = "LocationService.svc/UpdateFavoriteLocations";
							getUrl = globalClass.Url_EncodeWithBlank(getUrl);
							String categoryurl = LocationsliderPassDetail.iMerchantStoreId
									+ "|" + Boolean.parseBoolean(isChecked);
							serverResponse = call_webservice_SliderLocationDetail
									.makeServiceFavouriteLocations(getUrl,
											categoryurl,
											globalClass.languageId,
											globalClass.CustomerId);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						serverResponse = "";
					}
				});
		as.execute();
	}

	private void onFacebookClick() {

		Intent ii = new Intent(getActivity(), com.mimcoups.Consumer.FacebookShare.class);
		ii.putExtra("dealname", LocationsliderPassDetail.vMerchantStoreName);
		ii.putExtra("desc", LocationsliderPassDetail.vShortDescription);
		ii.putExtra("Image", share);
		startActivity(ii);
	}
	public Bitmap getBitmapFromURL(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	public void convertFileToBitmap(Bitmap resized) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(FileImage);
			resized.compress(Bitmap.CompressFormat.PNG, 90, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Throwable ignore) {
			}
		}
	}

	public class getbitmap extends AsyncTask<String, String, String> {
		Bitmap bm;

		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... params) {
			bm = getBitmapFromURL(locationdetail.vStoreDetailImage_A_650X325);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// imgDealImage.setImageBitmap(bm);
			convertFileToBitmap(bm);
			Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
			shareIntent.setType("image/*"); // set mime type
			// Uri path = Uri
			// .parse("android.resource://com.android.MimCoup/"
			// + R.drawable.app_icon);
			Uri path = Uri.parse("file:///" + FileImage.getPath());
			shareIntent
					.putExtra(
							Intent.EXTRA_TEXT,
							"MiMCoups APP:: \n "
									+ LocationsliderPassDetail.vMerchantStoreName
									+ "\n " + share);
			shareIntent.putExtra(Intent.EXTRA_STREAM, path);
			shareIntent.setPackage("com.instagram.android");
			context.startActivity(shareIntent);



		}
	}

	private boolean appInstalledOrNot() {

		boolean app_installed = false;
		try {
			ApplicationInfo info = getActivity().getPackageManager()
					.getApplicationInfo("com.instagram.android", 0);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	public void ratingupdated(float rateing) {

		ratingBar_locationdetail.setRating(rateing);

	}

}
