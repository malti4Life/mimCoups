package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.ReviewDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.DealReviewAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.squareup.picasso.Picasso;

public class FragmentWriteReviewDeel extends Fragment {

	private ImageView imgReviewBack, imgReviewQuestions, imgDealImage;
	private topBarInteface topInterface;
	private ListView lstReview;
	private ArrayList<ReviewDetail> arrReviews = new ArrayList<ReviewDetail>();
	private DealReviewAdapter adapter;
	private DealFeedDetail dealdetail;
	private CommonData commondata;
	private String url_response = "", geturl = "", statusCode = "",
			messgeResponse = "", strPageIndex = "1";
	private RatingBar RtRatings;
	private EditText edtReviews;
	private Button btnSubmit;
	private TextView txtdealfeeddicountprice, txtStoreName, txtDealType,
			txtDealDetail;
	private WebService_Call web_Service = new WebService_Call();
	ReviewDetail Reviewcustomer = new ReviewDetail();
	private TextView txt_writereview_star;
	View ListHeaderView;
	RatingBar ratingBar1;
	TextView txttopcategoryname;
	private boolean loadmore = true;
	RelativeLayout relmainfragment_writereview_deal;
	private int preLast;
	private WebService_Call web_service = new WebService_Call();
	private String strGeturl = "", strStatusCode = "", strMessageResponse,
			urlResponse = "", strLastRunDate = "";
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	LinearLayout txtReviewsNoData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_writereview_deal,
				container, false);
		dealdetail = (DealFeedDetail) getArguments().getSerializable(
				"DealFeedDetail");

		lstReview = (ListView) view.findViewById(R.id.lstReviews);
		LayoutInflater inflate1 = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ListHeaderView = inflate1.inflate(
				R.layout.fragment_writereview_deal_header, null);
		lstReview.addHeaderView(ListHeaderView);

		initControls(ListHeaderView, view);

		clickEvents();
		adapter = new DealReviewAdapter(getActivity(), arrReviews);
		lstReview.setAdapter(adapter);

		//
		setblankAdapter();
		getAllRatings();

		return view;
	}

	private void initControls(View view, View MainView) {
		relmainfragment_writereview_deal = (RelativeLayout) MainView
				.findViewById(R.id.relmainfragment_writereview_deal);

		txttopcategoryname = (TextView) MainView
				.findViewById(R.id.txttopcategoryname);
		imgDealImage = (ImageView) view.findViewById(R.id.imgDealImage);
		ratingBar1 = (RatingBar) view.findViewById(R.id.ratingBar1);
		txtdealfeeddicountprice = (TextView) view
				.findViewById(R.id.txtdealfeeddicountprice);
		txtReviewsNoData = (LinearLayout) view
				.findViewById(R.id.txtReviewsNoData);
		txtStoreName = (TextView) view.findViewById(R.id.txtStoreName);
		txtDealType = (TextView) view.findViewById(R.id.txtDealType);
		txtDealDetail = (TextView) view.findViewById(R.id.txtDealDetail);
		imgReviewBack = (ImageView) MainView.findViewById(R.id.imgReviewBack);
		imgReviewQuestions = (ImageView) MainView
				.findViewById(R.id.imgReviewQuestions);
		// lstReview = (ListView) view.findViewById(R.id.lstReviews);
		RtRatings = (RatingBar) MainView
				.findViewById(R.id.Rt_writereview_rating);
		txt_writereview_star = (TextView) MainView
				.findViewById(R.id.txt_writereview_star);
		edtReviews = (EditText) MainView.findViewById(R.id.edtReviews);
		btnSubmit = (Button) MainView.findViewById(R.id.btn_writereview_submit);
		commondata = (CommonData) getActivity().getApplicationContext();

		// set data
		Picasso.with(getActivity()).load(dealdetail.DealImageUrl)
				.placeholder(R.drawable.noimage_deal).into(imgDealImage);
		if (dealdetail.dDealDiscountedAmount.trim().contains("$0")
				|| dealdetail.dDealDiscountedAmount.trim().toString()
						.contains("kr0")) {
			String price = "";

			if (dealdetail.dealDiscountPrice.trim().toString().contains("$0")) {

				price = dealdetail.dealDiscountPrice.trim().toString()
						.replace("$", "").trim();

			} else if (dealdetail.dealDiscountPrice.trim().toString()
					.contains("kr0")) {
				price = dealdetail.dealDiscountPrice.trim().toString()
						.replace("kr", "").trim();

			}

			if (Double.parseDouble(price) > 0) {
				txtdealfeeddicountprice.setText(dealdetail.dDealDiscountedAmount);

			} else {
				

				txtdealfeeddicountprice.setText(getActivity().getString(
						R.string.txtdealprice));

			}

		} else {
			txtdealfeeddicountprice.setText(dealdetail.dDealDiscountedAmount);
		}

		txtStoreName.setText(dealdetail.StoreName);
		txtDealType.setText(dealdetail.dealName);

		dealdetail.shortDescription = dealdetail.shortDescription.replace(
				"\\n", "\n");

		dealdetail.shortDescription = dealdetail.shortDescription.replace(">",
				"●");

		txtDealDetail.setText(dealdetail.shortDescription);
		ratingBar1.setEnabled(false);
		ratingBar1.setRating(Float.parseFloat(dealdetail.iRating));

		txttopcategoryname.setText(dealdetail.vCategoryName);
	}

	private void clickEvents() {

		lstReview.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

				final int lastItem = firstVisibleItem + visibleItemCount;
				if (lastItem == totalItemCount) {

					if ((preLast != lastItem) && loadmore) {

						if (!commondata.checkInternetConnection()) {
							// DialogFragment ds = new SingleButtonAlert(
							// new DialogInterfaceClick() {
							//
							// @Override
							// public void dialogClick(String tag) {
							//
							// topInterface.topbarClick("back");
							// }
							// }, getResources().getString(
							// R.string.InternetConnect));
							// ds.show(getFragmentManager(), "");
						} else {

							// Log.i("we are in load more funnn>>", "" +
							// loadmore);
							loadmore = false;
							laodmore();
						}
					}
				}
			}
		});

		relmainfragment_writereview_deal
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		RtRatings.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					float touchPositionX = event.getX();
					float width = RtRatings.getWidth();
					float starsf = (touchPositionX / width) * 5.0f;
					int stars = (int) starsf + 1;

					if (stars >= 1 && stars <= 5) {
						txt_writereview_star.setText(String.valueOf(stars)
								+ " Stars");
						RtRatings.setRating(stars);
					}
					if (stars == 0) {
						txt_writereview_star.setText(String.valueOf(stars)
								+ " Star");
						RtRatings.setRating(stars);
					}

					v.setPressed(false);

				}
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					v.setPressed(true);
				}

				if (event.getAction() == MotionEvent.ACTION_CANCEL) {
					v.setPressed(false);
				}

				return true;
			}
		});

		imgReviewBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("back");
			}
		});
		imgReviewQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				View view = getActivity().getCurrentFocus();
				if (view != null) {
					InputMethodManager inputManager = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
				}
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("questions");
				}
			}
		});

		btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (txt_writereview_star.getText().toString()
							.equals("0 Star")) {

						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {

									}
								}, getResources().getString(
										R.string.starvalidation));
						dsp.show(getFragmentManager(), "");

					} else if (edtReviews.getText().toString().trim()
							.equals("")) {
						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub

									}
								}, getResources().getString(
										R.string.reviewValidation));
						dsp.show(getFragmentManager(), "");
					} else if (!(commondata.checkInternetConnection())) {
						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub

									}
								}, getResources().getString(
										R.string.InternetConnect));
						dsp.show(getFragmentManager(), "");
						dsp.setCancelable(false);
					} else {

						View view = getActivity().getCurrentFocus();
						if (view != null) {
							InputMethodManager inputManager = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							inputManager.hideSoftInputFromWindow(
									view.getWindowToken(),
									InputMethodManager.HIDE_NOT_ALWAYS);
						}

						if (!commondata.checkInternetConnection()) {
							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							dsp.show(getFragmentManager(), "");
							dsp.setCancelable(false);
						} else {

							addRatingWebservice();
						}
					}

				}
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void addRatingWebservice() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						if (!DeelFeedActivity.progressbarshowing) {
							topInterface.showDialog("Loading");
						}

						url_response = "";
						geturl = "";
						statusCode = "";
						messgeResponse = "";
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						geturl = "DealService.svc/AddRatingAndReview";
						url_response = web_Service
								.makeServiceAddDealFavourites(geturl,
										dealdetail.DealId,
										commondata.CustomerId,
										commondata.languageId,
										"" + Math.round(RtRatings.getRating()),
										edtReviews.getText().toString().trim());
						try {
							JSONObject mainObject = new JSONObject(url_response);
							statusCode = mainObject.getString("vStatusCode");
							messgeResponse = mainObject
									.getString("vMessageResponse");
						} catch (Exception e) {

						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						topInterface.hideDialog();
						if (strStatusCode.equals("420")) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.PoorInternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						} else if (statusCode.equals("100")) {
							ratingBar1.setRating(Float
									.parseFloat(dealdetail.iRating));
							strPageIndex = "1";
							arrReviews.clear();
							loadmore = true;
							getAllRatings();
							edtReviews.setText("");

							((DeelFeedActivity) getActivity()).isReviewUpdated = true;
						} else {
							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
											edtReviews.setText("");
										}
									}, messgeResponse);
							dsp.show(getFragmentManager(), "");
						}

					}
				});
		as.execute();
	}

	public void getAllloadmoreRating() {
		if (!(commondata.checkInternetConnection())) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub

						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						String strTempUrl = strGeturl + "&startindex="
								+ strPageIndex;

						Float totalrating = 0f;
						float averagerating = 0f;

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							// Toast.makeText(getActivity(),
							// "loadmore index" + strTempUrl, 9000).show();
							// Log.i("starpage index", "" + strPageIndex);
							// Log.i("strtempurl", "" + strTempUrl);
							// topInterface.showDialog("Loading");
							// arrReviews.clear();
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							// geturl =
							// "DealService.svc/GetAllRatingsAndReviews?customerid="
							// + commondata.CustomerId
							// + "&languageid="
							// + commondata.languageId
							// + "&dealid="
							// + dealdetail.DealId
							// + "&startindex="
							// + strPageIndex;

							// Log.i("write review deal", "" + strTempUrl);
							url_response = web_Service
									.makeServicegetRequest(strTempUrl);
							try {
								JSONObject mainObject = new JSONObject(
										url_response);
								statusCode = mainObject
										.getString("vStatusCode");
								JSONArray arrayFriend = mainObject
										.getJSONArray("Ratings");
								strPageIndex = mainObject
										.getString("iLastIndex");

								if (statusCode.equals("100")) {
									JSONObject jsonObject = mainObject
											.getJSONObject("CustomerReview");
									Reviewcustomer.strRatingCount = jsonObject
											.getString("iRating");

									Reviewcustomer.strCustomerName = jsonObject
											.getString("vCustomerName");
									Reviewcustomer.strProfileImageUrl = jsonObject
											.getString("vProfileImageName");
									Reviewcustomer.strReviewMessage = jsonObject
											.getString("vReviewMessage");

									for (int i = 0; i < arrayFriend.length(); i++) {
										Log.i("for loop", "" + i);
										JSONObject tempObject = arrayFriend
												.getJSONObject(i);
										ReviewDetail reviewDetail = new ReviewDetail();
										reviewDetail.strCustomerName = tempObject
												.getString("vCustomerName");
										reviewDetail.strProfileImageUrl = tempObject
												.getString("vProfileImageName");
										reviewDetail.strRatingCount = tempObject
												.getString("iRating");
										totalrating = totalrating
												+ Float.parseFloat(reviewDetail.strRatingCount);
										reviewDetail.strReviewMessage = tempObject
												.getString("vReviewMessage");
										arrReviews.add(reviewDetail);
									}

									averagerating = totalrating
											/ arrayFriend.length();

								}

							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							// topInterface.hideDialog();

							if (statusCode.equals("100")) {
								adapter.notifyDataSetChanged();

								// Toast.makeText(getActivity(),
								// "on post" + strPageIndex, 9000).show();
								//
								// int averagerating_int = (int) averagerating;
								//
								// ((DeelFeedActivity)
								// getActivity()).flDealRatingCount = Float
								// .parseFloat("" + averagerating_int);
								//
								// ((DeelFeedActivity)
								// getActivity()).flDealReviewCount = arrReviews
								// .size();
								// ratingBar1.setRating(averagerating_int);
								// //
								// // Toast.makeText(
								// // getActivity(),
								// // "rating "
								// // + Reviewcustomer.strRatingCount,
								// // 9000).show();
								//
								// // Log.i("webservice floating point>>", ""
								// // + Reviewcustomer.strRatingCount);
								//
								// RtRatings.setRating(Float
								// .parseFloat(Reviewcustomer.strRatingCount));
								//
								// int rating = Integer
								// .parseInt(Reviewcustomer.strRatingCount);
								//
								// if (rating > 1) {
								// txt_writereview_star
								// .setText(Reviewcustomer.strRatingCount
								// + " Stars");
								// } else {
								// txt_writereview_star
								// .setText(Reviewcustomer.strRatingCount
								// + " Star");
								// }
								// edtReviews
								// .setText(Reviewcustomer.strReviewMessage);
								//
								// adapter = new
								// DealReviewAdapter(getActivity(),
								// arrReviews);
								// lstReview.setAdapter(adapter);

							}

							else if (strStatusCode.equals("101")) {
								// txtDeelFeedNoData.setText(strMessageResponse);

								preLast = 0;
								loadmore = false;

							} else {

								loadmore = true;
							}

						}
					});
			as.execute();
		}
	}

	public void setblankAdapter() {
		arrReviews.clear();
		adapter = new DealReviewAdapter(getActivity(), arrReviews);
		lstReview.setAdapter(adapter);
	}

	public void getAllRatings() {
		if (!(commondata.checkInternetConnection())) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub

						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						Float totalrating = 0f;
						float averagerating = 0f;

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub

							topInterface.showDialog("Loading");

						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							geturl = "DealService.svc/GetAllRatingsAndReviews?customerid="
									+ commondata.CustomerId
									+ "&languageid="
									+ commondata.languageId
									+ "&dealid="
									+ dealdetail.DealId
									+ "&startindex="
									+ strPageIndex;

							// Log.e("URl review", geturl);

							url_response = web_Service
									.makeServicegetRequest(geturl);
							try {
								JSONObject mainObject = new JSONObject(
										url_response);
								statusCode = mainObject
										.getString("vStatusCode");
								JSONArray arrayFriend = mainObject
										.getJSONArray("Ratings");
								strPageIndex = mainObject
										.getString("iLastIndex");

								if (statusCode.equals("100")) {
									JSONObject jsonObject = mainObject
											.getJSONObject("CustomerReview");
									Reviewcustomer.strRatingCount = jsonObject
											.getString("iRating");

									Reviewcustomer.strCustomerName = jsonObject
											.getString("vCustomerName");
									Reviewcustomer.strProfileImageUrl = jsonObject
											.getString("vProfileImageName");
									Reviewcustomer.strReviewMessage = jsonObject
											.getString("vReviewMessage");

									for (int i = 0; i < arrayFriend.length(); i++) {
										JSONObject tempObject = arrayFriend
												.getJSONObject(i);
										ReviewDetail reviewDetail = new ReviewDetail();
										reviewDetail.strCustomerName = tempObject
												.getString("vCustomerName");
										reviewDetail.strProfileImageUrl = tempObject
												.getString("vProfileImageName");
										reviewDetail.strRatingCount = tempObject
												.getString("iRating");
										totalrating = totalrating
												+ Float.parseFloat(reviewDetail.strRatingCount);
										reviewDetail.strReviewMessage = tempObject
												.getString("vReviewMessage");
										arrReviews.add(reviewDetail);
									}

									averagerating = totalrating
											/ arrayFriend.length();

								}

							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topInterface.hideDialog();

							if (!(commondata.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub

											}
										}, getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
							} else {
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topInterface
															.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {

									int averagerating_int = (int) averagerating;

									((DeelFeedActivity) getActivity()).flDealRatingCount = Float
											.parseFloat("" + averagerating_int);

									((DeelFeedActivity) getActivity()).flDealReviewCount = arrReviews
											.size();
									ratingBar1.setRating(averagerating_int);
									//

									RtRatings.setRating(Float
											.parseFloat(Reviewcustomer.strRatingCount));

									int rating = Integer
											.parseInt(Reviewcustomer.strRatingCount);

									if (rating > 1) {
										txt_writereview_star
												.setText(Reviewcustomer.strRatingCount
														+ " Stars");
									} else {
										txt_writereview_star
												.setText(Reviewcustomer.strRatingCount
														+ " Star");
									}
									edtReviews
											.setText(Reviewcustomer.strReviewMessage);

									adapter.notifyDataSetChanged();
									if (adapter.getCount() == 0) {
										txtReviewsNoData
												.setVisibility(View.VISIBLE);
									} else {
										txtReviewsNoData
												.setVisibility(View.GONE);
									}

								} else if (statusCode.equals("101")) {
									loadmore = false;
								}
							}

						}
					});
			as.execute();
		}
	}

	public void laodmore() {

		if ((commondata.checkInternetConnection())) {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						Float totalrating = 0f;
						float averagerating = 0f;

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub

						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							geturl = "DealService.svc/GetAllRatingsAndReviews?customerid="
									+ commondata.CustomerId
									+ "&languageid="
									+ commondata.languageId
									+ "&dealid="
									+ dealdetail.DealId
									+ "&startindex="
									+ strPageIndex;

							// Log.e("URl review", geturl);

							url_response = web_Service
									.makeServicegetRequest(geturl);
							try {
								JSONObject mainObject = new JSONObject(
										url_response);
								statusCode = mainObject
										.getString("vStatusCode");
								JSONArray arrayFriend = mainObject
										.getJSONArray("Ratings");
								strPageIndex = mainObject
										.getString("iLastIndex");

								if (statusCode.equals("100")) {
									JSONObject jsonObject = mainObject
											.getJSONObject("CustomerReview");
									Reviewcustomer.strRatingCount = jsonObject
											.getString("iRating");

									Reviewcustomer.strCustomerName = jsonObject
											.getString("vCustomerName");
									Reviewcustomer.strProfileImageUrl = jsonObject
											.getString("vProfileImageName");
									Reviewcustomer.strReviewMessage = jsonObject
											.getString("vReviewMessage");

									for (int i = 0; i < arrayFriend.length(); i++) {
										JSONObject tempObject = arrayFriend
												.getJSONObject(i);
										ReviewDetail reviewDetail = new ReviewDetail();
										reviewDetail.strCustomerName = tempObject
												.getString("vCustomerName");
										reviewDetail.strProfileImageUrl = tempObject
												.getString("vProfileImageName");
										reviewDetail.strRatingCount = tempObject
												.getString("iRating");
										totalrating = totalrating
												+ Float.parseFloat(reviewDetail.strRatingCount);
										reviewDetail.strReviewMessage = tempObject
												.getString("vReviewMessage");
										arrReviews.add(reviewDetail);
									}

									averagerating = totalrating
											/ arrayFriend.length();

								}

							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (statusCode.equals("100")) {

								int averagerating_int = (int) averagerating;

								((DeelFeedActivity) getActivity()).flDealRatingCount = Float
										.parseFloat("" + averagerating_int);

								((DeelFeedActivity) getActivity()).flDealReviewCount = arrReviews
										.size();
								ratingBar1.setRating(averagerating_int);
								//

								RtRatings.setRating(Float
										.parseFloat(Reviewcustomer.strRatingCount));

								int rating = Integer
										.parseInt(Reviewcustomer.strRatingCount);

								if (rating > 1) {
									txt_writereview_star
											.setText(Reviewcustomer.strRatingCount
													+ " Stars");
								} else {
									txt_writereview_star
											.setText(Reviewcustomer.strRatingCount
													+ " Star");
								}
								edtReviews
										.setText(Reviewcustomer.strReviewMessage);

								adapter.notifyDataSetChanged();

							} else if (statusCode.equals("101")) {
								loadmore = false;
							}
						}
					});
			as.execute();
		}

	}
}
