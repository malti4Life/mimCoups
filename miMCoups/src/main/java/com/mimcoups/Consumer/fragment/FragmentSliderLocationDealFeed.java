package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.Dialog.QuickAction;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.DealFeedAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentSliderLocationDealFeed extends Fragment {

	private int preLast = 0;
	private topBarInteface topInterface;
	private ListView lstDealFeedDeals;
	private DealFeedAdapter adapterDealFeed;
	private ArrayList<DealFeedDetail> arrDealFeeds = new ArrayList<DealFeedDetail>();
	private LinearLayout lnDealFeedFriend, lnDealFeedDate, lnDealFeedCategory,
			lnDealFeedRating, lnDealFeedLocation, lnDealFeedPrice,
			txtDeelFeedNoData;
	private ImageView imgDealFeedQuestions, imgDealFeedFriend, imgDealFeedDate,
			imgDealFeedCategory, imgDealFeedRating, imgDealFeedLocation,
			imgDealFeedPrice, imgDealFeedBack, imgDealFeedClose;
	private TextView txtDealFeedFriend, txtDealFeedDate, txtDealFeedCategory,
			txtDealFeedRating, txtDealFeedLocation, txtDealFeedPrice,
			txtDoneDealFeed;
	private FrameLayout dealfeedbgcontainer;
	private SwipeRefreshLayout swipeLayout;
	private String strGeturl = "", strStatusCode = "", strMessageResponse,
			urlResponse = "", StrPageIndex = "1", strLastRunDate = "";
	private CommonData commonClass;
	private WebService_Call web_service = new WebService_Call();
	private ArrayList<Integer> arrSelectedCategory = new ArrayList<Integer>();
	private String locationId = "", locationName = "", type = "0";
	private boolean loadmore = true;
	RelativeLayout relmainfragment_slider_location_dealfeed;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	String alert_message = "";
	boolean isfriend = false;
	View views;
	String CategoryId;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(
				R.layout.fragment_slider_location_dealfeed, container, false);
		initControls(view);
		LIstview_OnScroll_Functionality();
		createDialog();

		locationId = getArguments().getString("locationId");
		locationName = getArguments().getString("locationName");
		lnDealFeedLocation.setEnabled(false);

		imgDealFeedLocation
				.setImageResource(R.drawable.icon_topbar_location_selected);
		txtDealFeedLocation.setTextColor(Color.WHITE);
		arrSelectedCategory.add(R.drawable.icon_topbar_location_selected);
		txtDoneDealFeed.setText(locationName);
		views = inflater.inflate(R.layout.loadmore, null);
		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
		} else {

			StrPageIndex = "1";
			strGeturl = "LocationService.svc/GetDealsByLocation?customerid="
					+ commonClass.CustomerId + "&languageid="
					+ commonClass.languageId + "&merchantstoreid=" + locationId;
			strGeturl = commonClass.Url_EncodeWithBlank(strGeturl);

			loadDeals();

		}
		return view;
	}

	@SuppressLint("NewApi")
	private void initControls(View view) {
		// init Controls
		commonClass = (CommonData) getActivity().getApplicationContext();

		relmainfragment_slider_location_dealfeed = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_location_dealfeed);

		imgDealFeedClose = (ImageView) view.findViewById(R.id.imgDealFeedClose);
		txtDeelFeedNoData = (LinearLayout) view
				.findViewById(R.id.txtReviewsNoData);
		lnDealFeedFriend = (LinearLayout) view
				.findViewById(R.id.lnDealFeedFriend);
		lnDealFeedDate = (LinearLayout) view.findViewById(R.id.lnDealFeedDate);
		lnDealFeedCategory = (LinearLayout) view
				.findViewById(R.id.lnDealFeedCategory);
		lnDealFeedRating = (LinearLayout) view
				.findViewById(R.id.lnDealFeedRating);
		lnDealFeedLocation = (LinearLayout) view
				.findViewById(R.id.lnDealFeedLocation);
		lnDealFeedPrice = (LinearLayout) view
				.findViewById(R.id.lnDealFeedPrice);

		imgDealFeedQuestions = (ImageView) view
				.findViewById(R.id.imgDealFeedQuestions);
		imgDealFeedBack = (ImageView) view.findViewById(R.id.imgDealFeedBack);
		lstDealFeedDeals = (ListView) view.findViewById(R.id.lstDealFeedDeals);

		// image view android
		imgDealFeedFriend = (ImageView) view
				.findViewById(R.id.imgDealFeedFriend);

		imgDealFeedDate = (ImageView) view.findViewById(R.id.imgDealFeedDate);
		imgDealFeedCategory = (ImageView) view
				.findViewById(R.id.imgDealFeedCategory);
		imgDealFeedRating = (ImageView) view
				.findViewById(R.id.imgDealFeedRating);
		imgDealFeedLocation = (ImageView) view
				.findViewById(R.id.imgDealFeedLocation);
		imgDealFeedPrice = (ImageView) view.findViewById(R.id.imgDealFeedPrice);

		txtDealFeedFriend = (TextView) view
				.findViewById(R.id.txtDealFeedFriend);
		txtDealFeedDate = (TextView) view.findViewById(R.id.txtDealFeedDate);
		txtDealFeedCategory = (TextView) view
				.findViewById(R.id.txtDealFeedCategory);
		txtDealFeedRating = (TextView) view
				.findViewById(R.id.txtDealFeedRating);
		txtDealFeedLocation = (TextView) view
				.findViewById(R.id.txtDealFeedLocation);
		txtDealFeedPrice = (TextView) view.findViewById(R.id.txtDealFeedPrice);
		txtDoneDealFeed = (TextView) view.findViewById(R.id.txtDoneDealFeed);
		dealfeedbgcontainer = (FrameLayout) view
				.findViewById(R.id.dealfreedbgcontainer);

		swipeLayout = (SwipeRefreshLayout) view
				.findViewById(R.id.swipe_container);

		swipeLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				swipeLayout.setRefreshing(false);
			}
		});
		swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		relmainfragment_slider_location_dealfeed
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgDealFeedClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				clearBlackImages();

				arrDealFeeds.clear();
				arrSelectedCategory.clear();
				lnDealFeedLocation.setEnabled(false);
				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location_selected);
				txtDealFeedLocation.setTextColor(Color.WHITE);
				arrSelectedCategory
						.add(R.drawable.icon_topbar_location_selected);
				imgDealFeedClose.setVisibility(View.GONE);
				imgDealFeedQuestions.setVisibility(View.VISIBLE);
				txtDoneDealFeed.setText(locationName);
				StrPageIndex = "1";
				strGeturl = "LocationService.svc/GetDealsByLocation?customerid="
						+ commonClass.CustomerId
						+ "&languageid="
						+ commonClass.languageId
						+ "&merchantstoreid="
						+ locationId;
				strGeturl = commonClass.Url_EncodeWithBlank(strGeturl);
				try {
					lstDealFeedDeals.removeFooterView(views);
				} catch (Exception e) {
				}
				swipeLayout.setEnabled(true);
				loadDealsClose();
			}
		});
	}

	private void createDialog() {
		final QuickAction mQuickAction = new QuickAction(getActivity());
		mQuickAction
				.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
					@Override
					public void onItemClick(int id, int pos, String title) {
						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub

										}
									}, getResources().getString(
											R.string.InternetConnect));

							ds.setCancelable(false);
						} else {
							txtDoneDealFeed.setText(locationName);
							imgDealFeedQuestions.setVisibility(View.GONE);
							imgDealFeedClose.setVisibility(View.VISIBLE);
							arrSelectedCategory.clear();

							double latitude = commonClass.currentLatitude;
							double longiutde = commonClass.currentLongitude;

							ArrayList<Double> arrLatitude = ((DeelFeedActivity) getActivity())
									.getLatitudeLongitude();

							latitude = arrLatitude.get(0);
							longiutde = arrLatitude.get(1);
							if (id == 1) {
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByFriend?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ latitude
										+ "&longitude="
										+ longiutde
										+ "&mode="
										+ pos
										+ "&id="
										+ locationId
										+ "&type=" + type;

								// try {
								//
								// lstDealFeedDeals.removeFooterView(views);
								// } catch (Exception e) {
								// // TODO: handle exception
								// }

								loadSortDeals();
								arrSelectedCategory
										.add(R.drawable.icon_topbar_friend_selected);
							} else if (id == 2) {
								// Date wise sorting
								//

								isfriend = false;
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByDate?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ latitude
										+ "&longitude="
										+ longiutde
										+ "&mode="
										+ pos
										+ "&id="
										+ locationId
										+ "&type=" + type;
								// try {
								//
								// lstDealFeedDeals.removeFooterView(views);
								// } catch (Exception e) {
								// // TODO: handle exception
								// }
								loadSortDeals();
								arrSelectedCategory
										.add(R.drawable.icon_topbar_date_selected);
							} else if (id == 3) {
								isfriend = false;
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByCategory?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&categoryid="
										+ pos
										+ "&latitude="
										+ latitude
										+ "&longitude="
										+ longiutde
										+ "&id=" + locationId + "&type=" + type;

								CategoryId = "" + pos;
								// Log.d("strgeturllllllll>>>", "" + strGeturl);
								// try {
								//
								// lstDealFeedDeals.removeFooterView(views);
								// } catch (Exception e) {
								// // TODO: handle exception
								// }
								loadSortDeals();
								arrSelectedCategory
										.add(R.drawable.icon_topbar_category_selected);
							} else if (id == 4) {

								isfriend = false;
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByRating?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ latitude
										+ "&longitude="
										+ longiutde
										+ "&mode="
										+ pos
										+ "&id="
										+ locationId
										+ "&type=" + type;
								Log.e("rating", strGeturl);
								// try {
								//
								// lstDealFeedDeals.removeFooterView(views);
								// } catch (Exception e) {
								// // TODO: handle exception
								// }
								loadSortDeals();
								arrSelectedCategory
										.add(R.drawable.icon_topbar_rating_selected);
							} else if (id == 5) {
								isfriend = false;
								// distancte
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByDistance?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ latitude
										+ "&longitude="
										+ longiutde
										+ "&mode="
										+ pos
										+ "&id="
										+ locationId
										+ "&type=" + type;
								// try {
								//
								// lstDealFeedDeals.removeFooterView(views);
								// } catch (Exception e) {
								// // TODO: handle exception
								// }
								loadSortDeals();
								arrSelectedCategory
										.add(R.drawable.icon_topbar_location_selected);
							} else if (id == 6) {
								isfriend = false;
								// err
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByPrice?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ latitude
										+ "&longitude="
										+ longiutde
										+ "&mode="
										+ pos
										+ "&id="
										+ locationId
										+ "&type=" + type;
								Log.e("Price deel feed", strGeturl);
								// try {
								//
								// lstDealFeedDeals.removeFooterView(views);
								// } catch (Exception e) {
								// // TODO: handle exception
								// }
								loadSortDeals();
								arrSelectedCategory
										.add(R.drawable.icon_topbar_price_selected);
							}
						}
					}
				});

		mQuickAction.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				topbarblackImages();
			}
		});
		imgDealFeedBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("back");
			}
		});
		imgDealFeedQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("questions");
				}
			}
		});
		lnDealFeedFriend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				clearBlackImages();
				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend_selected);
				txtDealFeedFriend.setTextColor(Color.WHITE);
				dealfeedbgcontainer.setVisibility(View.VISIBLE);
				dealfeedbgcontainer.getBackground().setAlpha(160);
				mQuickAction.show(view, 1);

			}
		});
		lnDealFeedDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				clearBlackImages();
				imgDealFeedDate
						.setImageResource(R.drawable.icon_topbar_date_selected);
				txtDealFeedDate.setTextColor(Color.WHITE);
				dealfeedbgcontainer.setVisibility(View.VISIBLE);
				dealfeedbgcontainer.getBackground().setAlpha(160);
				mQuickAction.show(view, 2);
			}
		});
		lnDealFeedCategory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				clearBlackImages();
				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category_selected);
				txtDealFeedCategory.setTextColor(Color.WHITE);
				dealfeedbgcontainer.setVisibility(View.VISIBLE);
				dealfeedbgcontainer.getBackground().setAlpha(160);
				mQuickAction.show(view, 3);

			}
		});
		lnDealFeedRating.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				clearBlackImages();
				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating_selected);
				txtDealFeedRating.setTextColor(Color.WHITE);
				dealfeedbgcontainer.setVisibility(View.VISIBLE);
				dealfeedbgcontainer.getBackground().setAlpha(160);
				mQuickAction.show(view, 4);

			}
		});
		lnDealFeedLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				clearBlackImages();
				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location_selected);
				txtDealFeedLocation.setTextColor(Color.WHITE);
				dealfeedbgcontainer.setVisibility(View.VISIBLE);
				dealfeedbgcontainer.getBackground().setAlpha(160);
				mQuickAction.show(view, 5);

			}
		});
		lnDealFeedPrice.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				clearBlackImages();
				imgDealFeedPrice
						.setImageResource(R.drawable.icon_topbar_price_selected);
				txtDealFeedPrice.setTextColor(Color.WHITE);
				dealfeedbgcontainer.setVisibility(View.VISIBLE);
				dealfeedbgcontainer.getBackground().setAlpha(160);
				mQuickAction.show(view, 6);

			}
		});
	}

	protected void topbarblackImages() {
		// TODO Auto-generated method stub
		dealfeedbgcontainer.setVisibility(View.GONE);
		if (arrSelectedCategory.size() > 0) {
			if (arrSelectedCategory.get(arrSelectedCategory.size() - 1) == R.drawable.icon_topbar_friend_selected) {
				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend_selected);
				txtDealFeedFriend.setTextColor(Color.WHITE);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);

			} else if (arrSelectedCategory.get(arrSelectedCategory.size() - 1) == R.drawable.icon_topbar_date_selected) {
				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate
						.setImageResource(R.drawable.icon_topbar_date_selected);
				txtDealFeedDate.setTextColor(Color.WHITE);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);
			} else if (arrSelectedCategory.get(arrSelectedCategory.size() - 1) == R.drawable.icon_topbar_category_selected) {
				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);
				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category_selected);
				txtDealFeedCategory.setTextColor(Color.WHITE);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);
			} else if (arrSelectedCategory.get(arrSelectedCategory.size() - 1) == R.drawable.icon_topbar_rating_selected) {

				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating_selected);
				txtDealFeedRating.setTextColor(Color.WHITE);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);
			} else if (arrSelectedCategory.get(arrSelectedCategory.size() - 1) == R.drawable.icon_topbar_price_selected) {
				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedPrice
						.setImageResource(R.drawable.icon_topbar_price_selected);
				txtDealFeedPrice.setTextColor(Color.WHITE);
				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);
			} else if (arrSelectedCategory.get(arrSelectedCategory.size() - 1) == R.drawable.icon_topbar_location_selected) {
				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location_selected);
				txtDealFeedLocation.setTextColor(Color.WHITE);
			}
		} else {
			clearBlackImages();
		}
	}

	public void clearBlackImages() {

		imgDealFeedFriend.setImageResource(R.drawable.icon_topbar_friend);
		txtDealFeedFriend.setTextColor(Color.BLACK);

		imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
		txtDealFeedDate.setTextColor(Color.BLACK);

		imgDealFeedCategory.setImageResource(R.drawable.icon_topbar_category);
		txtDealFeedCategory.setTextColor(Color.BLACK);

		imgDealFeedRating.setImageResource(R.drawable.icon_topbar_rating);
		txtDealFeedRating.setTextColor(Color.BLACK);

		imgDealFeedLocation.setImageResource(R.drawable.icon_topbar_location);
		txtDealFeedLocation.setTextColor(Color.BLACK);

		imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
		txtDealFeedPrice.setTextColor(Color.BLACK);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void loadSortDeals() {

		// Toast.makeText(getActivity(), "Short Deal running", 5000).show();

		arrDealFeeds.clear();
		arrDealFeeds = new ArrayList<DealFeedDetail>();

		setblankAdapter();

		StrPageIndex = "1";
		swipeLayout.setEnabled(false);

		final String strTempUrl = commonClass.Url_EncodeWithBlank(strGeturl
				+ "&startindex=" + StrPageIndex);

		Log.e("strtempurl>>>>>>>", "" + strGeturl + "&startindex="
				+ StrPageIndex);

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {
						// topInterface.showDialog("Loading");
						// arrDealFeeds = new ArrayList<DealFeedDetail>();

						loadmore = false;
						arrDealFeeds.clear();
						arrDealFeeds = new ArrayList<DealFeedDetail>();
					}

					@Override
					public void doInBackground() {
						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);
						try {
							JSONObject json_Object = new JSONObject(urlResponse);
							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");

							if (strStatusCode.equals("100")) {

								StrPageIndex = json_Object
										.getString("iLastIndex");

								arrDealFeeds.clear();
								arrDealFeeds = new ArrayList<DealFeedDetail>();

								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.DelFeedDetail(json_deal);

									if (strTempUrl
											.contains("GetAllDealsByCategory")) {
										dealDetail.categoryId = CategoryId;
									}
									arrDealFeeds.add(dealDetail);
								}
							} else if (strStatusCode.equals("101")) {

								arrDealFeeds.clear();
								arrDealFeeds = new ArrayList<DealFeedDetail>();

								StrPageIndex = json_Object
										.getString("iLastIndex");

								if (json_Object.has("Types")) {

									if (!json_Object.getBoolean("Types")) {

										isfriend = true;

										alert_message = json_Object
												.getString("AlertMessage");
									} else {
										isfriend = false;
									}
								}

								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.DelFeedDetail(json_deal);
									arrDealFeeds.add(dealDetail);
								}

							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();

						Log.e("onposted called", "onpost");

						try {
							lstDealFeedDeals.addFooterView(views);
						} catch (Exception e) {
							// TODO: handle exception
						}

						if (strStatusCode.equals("420")) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.PoorInternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						}

						else if (strStatusCode.equals("101")) {

							// txtDeelFeedNoData.setText(strMessageResponse);

							if (isfriend) {

								if (arrDealFeeds.size() == 0) {

									try {
										lstDealFeedDeals
												.removeFooterView(views);
									} catch (Exception e) {
										// TODO: handle exception
									}

									if (lstDealFeedDeals.getVisibility() == View.VISIBLE) {

										swipeLayout.setVisibility(View.GONE);

										lstDealFeedDeals
												.setVisibility(View.GONE);
										txtDeelFeedNoData
												.setVisibility(View.VISIBLE);
									}

								} else {

									if (lstDealFeedDeals.getVisibility() == View.GONE) {

										swipeLayout.setVisibility(View.VISIBLE);
										lstDealFeedDeals
												.setVisibility(View.VISIBLE);
										txtDeelFeedNoData
												.setVisibility(View.GONE);
									}

									loadmore = true;

									adapterDealFeed = new DealFeedAdapter(
											getActivity(), arrDealFeeds);
									lstDealFeedDeals
											.setAdapter(adapterDealFeed);
								}
								try {

									if (alert_message.trim().toString()
											.length() != 0) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {

													}
												}, alert_message);
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);
									}

								} catch (Exception e) {
									// TODO: handle exception
								}

							} else {

								try {
									lstDealFeedDeals.removeFooterView(views);
								} catch (Exception e) {
									// TODO: handle exception
								}

								if (lstDealFeedDeals.getVisibility() == View.VISIBLE) {
									lstDealFeedDeals.setVisibility(View.GONE);
									txtDeelFeedNoData
											.setVisibility(View.VISIBLE);
								}
							}

						} else if (strStatusCode.equals("118")) {
							loadmore = false;

						} else if (strStatusCode.equalsIgnoreCase("100")) {

							loadmore = true;

							if (txtDeelFeedNoData.getVisibility() == View.VISIBLE) {

								swipeLayout.setVisibility(View.VISIBLE);
								lstDealFeedDeals.setVisibility(View.VISIBLE);
								txtDeelFeedNoData.setVisibility(View.GONE);
							}
							adapterDealFeed = new DealFeedAdapter(
									getActivity(), arrDealFeeds);
							lstDealFeedDeals.setAdapter(adapterDealFeed);
						}
					}
				});
		as.execute();
	}

	public void loadDeals() {

		// GPSTracker gps = new GPSTracker();
		// Location currentloc = gps.getCurrentLocation();
		// double latitude = 0.0;
		// double longiutde = 0.0;
		//
		// if (currentloc != null) {
		// latitude = currentloc.getLatitude();
		// longiutde = currentloc.getLongitude();
		// } else {
		// latitude = commonClass.currentLatitude;
		// longiutde = commonClass.currentLongitude;
		// }

		double latitude = commonClass.currentLatitude;
		double longiutde = commonClass.currentLongitude;

		ArrayList<Double> arrLatitude = ((DeelFeedActivity) getActivity())
				.getLatitudeLongitude();

		latitude = arrLatitude.get(0);
		longiutde = arrLatitude.get(1);

		final String strTempUrl = commonClass.Url_EncodeWithBlank(strGeturl
				+ "&startindex=" + StrPageIndex + "&latitude=" + latitude
				+ "&longitude=" + longiutde + "&dlastrundate=");

		arrDealFeeds.clear();
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {
						topInterface.showDialog("Loading");
						strStatusCode = "";
					}

					@Override
					public void doInBackground() {

						try {

							urlResponse = web_service
									.makeServicegetRequest(strTempUrl);
							Log.e("urlllll>>>", "" + strTempUrl);

							JSONObject json_Object = new JSONObject(urlResponse);
							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");

							if (strStatusCode.equals("100")) {

								StrPageIndex = json_Object
										.getString("iLastIndex");
								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.DelFeedDetail(json_deal);
									arrDealFeeds.add(dealDetail);
								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();

						Log.e("onposted called", "onpost without filter");
						lstDealFeedDeals.addFooterView(views);

						if (!commonClass.checkInternetConnection()) {
							try {
								lstDealFeedDeals.removeFooterView(views);
							} catch (Exception e) {
								// TODO: handle exception
							}
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
						} else {
							if (strStatusCode.equals("420")) {

								try {
									lstDealFeedDeals.removeFooterView(views);
								} catch (Exception e) {
									// TODO: handle exception
								}
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (strStatusCode.equals("101")) {
								// txtDeelFeedNoData.setText(strMessageResponse);

								try {
									lstDealFeedDeals.removeFooterView(views);
								} catch (Exception e) {
									// TODO: handle exception
								}
								swipeLayout.setVisibility(View.GONE);
								lstDealFeedDeals.setVisibility(View.GONE);
								txtDeelFeedNoData.setVisibility(View.VISIBLE);
							} else if (strStatusCode.equals("118")) {
								try {
									lstDealFeedDeals.removeFooterView(views);
								} catch (Exception e) {
									// TODO: handle exception
								}

							} else {
								// strGeturl = strGeturl + "&startindex="
								// + StrPageIndex + "&dLastRunDate="
								// + strLastRunDate;
								// strGeturl = commonClass
								// .Url_EncodeWithBlank(strGeturl);

								lstDealFeedDeals.setVisibility(View.VISIBLE);
								swipeLayout.setVisibility(View.VISIBLE);
								txtDeelFeedNoData.setVisibility(View.GONE);
								setblankAdapter();

							}
						}

					}
				});
		as.execute();
	}

	public void loadDealsClose() {

		// GPSTracker gps = new GPSTracker();
		// Location currentloc = gps.getCurrentLocation();
		// double latitude = 0.0;
		// double longiutde = 0.0;
		//
		// if (currentloc != null) {
		// latitude = currentloc.getLatitude();
		// longiutde = currentloc.getLongitude();
		// } else {
		// latitude = commonClass.currentLatitude;
		// longiutde = commonClass.currentLongitude;
		// }

		double latitude = commonClass.currentLatitude;
		double longiutde = commonClass.currentLongitude;

		ArrayList<Double> arrLatitude = ((DeelFeedActivity) getActivity())
				.getLatitudeLongitude();

		latitude = arrLatitude.get(0);
		longiutde = arrLatitude.get(1);

		final String strTempUrl = commonClass.Url_EncodeWithBlank(strGeturl
				+ "&startindex=" + StrPageIndex + "&latitude=" + latitude
				+ "&longitude=" + longiutde + "&dlastrundate=");

		arrDealFeeds.clear();
		arrDealFeeds = new ArrayList<DealFeedDetail>();
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {
						topInterface.showDialog("Loading");
						strStatusCode = "";
						loadmore = false;

					}

					@Override
					public void doInBackground() {

						try {

							urlResponse = web_service
									.makeServicegetRequest(strTempUrl);
							Log.e("urlllll>>>", "" + urlResponse);

							JSONObject json_Object = new JSONObject(urlResponse);
							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");

							if (strStatusCode.equals("100")) {

								StrPageIndex = json_Object
										.getString("iLastIndex");
								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.DelFeedDetail(json_deal);
									arrDealFeeds.add(dealDetail);
								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();

						try {
							lstDealFeedDeals.removeFooterView(views);
						} catch (Exception e) {
							// TODO: handle exception
						}

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
						} else {
							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (strStatusCode.equals("101")) {
								// txtDeelFeedNoData.setText(strMessageResponse);
								swipeLayout.setVisibility(View.GONE);
								lstDealFeedDeals.setVisibility(View.GONE);
								txtDeelFeedNoData.setVisibility(View.VISIBLE);
							} else if (strStatusCode.equals("118")) {

							} else {

								try {

									lstDealFeedDeals.addFooterView(views);

									loadmore = true;

									lstDealFeedDeals
											.setVisibility(View.VISIBLE);
									swipeLayout.setVisibility(View.VISIBLE);
									txtDeelFeedNoData.setVisibility(View.GONE);

									adapterDealFeed = new DealFeedAdapter(
											getActivity(), arrDealFeeds);
									lstDealFeedDeals
											.setAdapter(adapterDealFeed);

								} catch (Exception e) {
									// TODO: handle exception
								}

							}
						}

					}
				});
		as.execute();
	}

	public void setblankAdapter() {
		adapterDealFeed = new DealFeedAdapter(getActivity(), arrDealFeeds);
		lstDealFeedDeals.setAdapter(adapterDealFeed);
	}

	private void LIstview_OnScroll_Functionality() {

		lstDealFeedDeals.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

				Log.e("called scroll", "" + totalItemCount);

				final int lastItem = firstVisibleItem + visibleItemCount;

				if (lastItem == totalItemCount) {

					if ((preLast != lastItem) && loadmore) {

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
						} else {

							loadmore = false;

							loadMoreDeals();
						}
					}
				}
			}
		});

	}

	private void loadMoreDeals() {

		final String strTempUrl = commonClass.Url_EncodeWithBlank(strGeturl
				+ "&startindex=" + StrPageIndex);
		Log.e("called loadmore called", strTempUrl);

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {

					}

					@Override
					public void doInBackground() {
						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);

						try {
							JSONObject json_Object = new JSONObject(urlResponse);
							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");
							if (strStatusCode.equals("100")) {

								StrPageIndex = json_Object
										.getString("iLastIndex");

								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.DelFeedDetail(json_deal);
									if (strTempUrl
											.contains("GetAllDealsByCategory")) {
										dealDetail.categoryId = CategoryId;
									}
									arrDealFeeds.add(dealDetail);
								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();

						Log.e("onposted called", "onpost loadmore");
						try {
							lstDealFeedDeals.removeFooterView(views);
						} catch (Exception e) {
							// TODO: handle exception
						}

						if (strStatusCode.equals("101")) {

							loadmore = false;
							// txtDeelFeedNoData.setText(strMessageResponse);
							// swipeLayout.setVisibility(View.GONE);
							// lstDealFeedDeals.setVisibility(View.GONE);
							// txtDeelFeedNoData.setVisibility(View.VISIBLE);
						} else if (strStatusCode.equals("118")) {

							loadmore = false;

						} else if (strStatusCode.equals("100")) {
							// strGeturl = strGeturl + "&startindex="
							// + StrPageIndex + "&dLastRunDate="
							// + strLastRunDate;
							// strGeturl = commonClass
							// .Url_EncodeWithBlank(strGeturl);

							// Toast.makeText(getActivity(),
							// "StrPageIndex==" + StrPageIndex,
							// Toast.LENGTH_SHORT).show();

							try {

								loadmore = true;
								lstDealFeedDeals.addFooterView(views);
								swipeLayout.setVisibility(View.VISIBLE);
								lstDealFeedDeals.setVisibility(View.VISIBLE);
								txtDeelFeedNoData.setVisibility(View.GONE);
								adapterDealFeed.notifyDataSetChanged();

							} catch (Exception e) {
								// TODO: handle exception
							}

						}

					}
				});
		as.execute();
	}
}