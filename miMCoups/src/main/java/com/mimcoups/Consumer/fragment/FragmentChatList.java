package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.packet.Presence;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.customClass.ChatUser;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.Setchatconnection;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.ChatListSingleAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentChatList extends Fragment {

	private ListView lstChatListUsers;
	public ChatListSingleAdapter adapterChatList;
	private ImageView imgChatListSearch, imgChatListQuestions;
	private topBarInteface topbarInterface;
	CommonData commonClass;
	private WebService_Call webservicecall = new WebService_Call();
	private String serverResponse = "";
	private String statusCode = "", statusMessage = "",
			strMessageResponse = "";
	// private ArrayList<ChatUser> arrOnlineChats;
	// private ArrayList<ChatUser> arrOfflineChats;
	// private ArrayList<ChatUser> arrchats;
	LinearLayout txtFriendschatNoData;
	RelativeLayout relmainfragment_chat_list;
	Setchatconnection setchatconnection;
	boolean isonpause;
	boolean isuserAdded;
	boolean resumeloaddata = false;
	async_deel_feed_list as;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_chat_list, container,
				false);

		initControls(view);
		clickEvents();
		try {
			getData();
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Exceptions===", e.getMessage().toString());
		}

		((DeelFeedActivity) getActivity()).is_chatscreen_neddto_refresh = true;

		return view;
	}

	private void initControls(View view) {

		commonClass = (CommonData) getActivity().getApplicationContext();
		isonpause = false;
		relmainfragment_chat_list = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_chat_list);

		// arrOnlineChats = new ArrayList<ChatUser>();
		// arrOfflineChats = new ArrayList<ChatUser>();
		commonClass.arrchats = new ArrayList<ChatUser>();

		txtFriendschatNoData = (LinearLayout) view
				.findViewById(R.id.txtFriendschatNoData);
		lstChatListUsers = (ListView) view.findViewById(R.id.lstChatListUsers);
		imgChatListSearch = (ImageView) view
				.findViewById(R.id.imgChatListSearch);
		imgChatListQuestions = (ImageView) view
				.findViewById(R.id.imgChatListQuestions);
	}

	private void clickEvents() {

		relmainfragment_chat_list.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		imgChatListSearch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topbarInterface.topbarClick("search");
			}
		});

		imgChatListQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topbarInterface.topbarClick("questions");
				}
			}
		});

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topbarInterface = (topBarInteface) activity;
		setchatconnection = (Setchatconnection) activity;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub

		if (as != null) {
			// Log.d("async cancel", "async canel");
			as.cancel(true);
		}
		super.onDetach();
	}

	public void MessageReceive() {
		// adapterChatList = new ChatListAdapter(getActivity(), arrOnlineChats,
		// arrOfflineChats);
		adapterChatList = new ChatListSingleAdapter(getActivity(),
				commonClass.arrchats);
		lstChatListUsers.setAdapter(adapterChatList);
	}

	public void Presencechange(String username, String presence) {
		isuserAdded = false;

		for (int i = 0; i < commonClass.arrchats.size(); i++) {
			ChatUser ch = commonClass.arrchats.get(i);
			if (ch.userName.equalsIgnoreCase(username)) {
				isuserAdded = true;

				if (presence.equals("available")) {
					ch.islonlie = true;

				}
				if (presence.equals("unavailable")) {
					ch.islonlie = false;
				}

				commonClass.arrchats.set(i, ch);
			}
		}


		Log.d("isuseradded", "" + isuserAdded);
		if (!(isuserAdded)) {
			Log.d("load data", "load data");
			if (!isonpause) {
				Log.d("get data", "get data");
				getData();
			} else {
				resumeloaddata = true;
			}

		} else {
			Log.d("set adapter", "set adapter");
			adapterChatList = new ChatListSingleAdapter(getActivity(),
					commonClass.arrchats);
			lstChatListUsers.setAdapter(adapterChatList);
		}

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		// Log.d("on pause fragment", "on pause");
		isonpause = true;
		super.onPause();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		// Log.d("on resume fragment", "on resume");
		isonpause = false;

		if (resumeloaddata) {
			// Log.d("load data again", "load data agin");
			resumeloaddata = false;
			getData();
		} else {
			adapterChatList = new ChatListSingleAdapter(getActivity(),
					commonClass.arrchats);
			lstChatListUsers.setAdapter(adapterChatList);
		}
		super.onResume();
	}

	public void getData() {

		if (!(commonClass.checkInternetConnection())) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub

						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);
		} else {
			as = new async_deel_feed_list(new asynctaskloaddata() {

				@Override
				public void onPreExecute() {
					// TODO Auto-generated method stub
					if (!DeelFeedActivity.progressbarshowing) {
						topbarInterface.showDialog("Loading");
					}

					commonClass.arrchats = new ArrayList<ChatUser>();

					// arrOfflineChats = new ArrayList<ChatUser>();
					// arrOnlineChats = new ArrayList<ChatUser>();
				}

				@Override
				public void doInBackground() {
					// TODO Auto-generated method stub

					String serverUrl = "FriendService.svc/GetAllFriends?languageid="
							+ commonClass.languageId
							+ "&customerid="
							+ commonClass.CustomerId;

					serverResponse = webservicecall
							.makeServicegetRequest(serverUrl);

					Log.d("server response>>", "server response>>");

					try {
						JSONObject js_getres = new JSONObject(serverResponse);

						statusCode = js_getres.getString("vStatusCode");

						strMessageResponse = js_getres
								.getString("vMessageResponse");

						if (statusCode.equals("100")) {

							JSONArray js_arr_data = js_getres
									.getJSONArray("Customers");

							commonClass.arrchats = new ArrayList<ChatUser>();

							// arrOfflineChats = new ArrayList<ChatUser>();

							for (int i = 0; i < js_arr_data.length(); i++) {

								JSONObject jsobj = js_arr_data.getJSONObject(i);

								String icustomerid = jsobj
										.getString("iCustomerId");

								String vUserName = jsobj.getString("vUserName");

								String vCustomerName = jsobj
										.getString("vCustomerName");

								String vProfileImageName = jsobj
										.getString("vProfileImageName");

								if (!(vUserName.equals(commonClass.strUsername))) {

									ChatUser chatuser = new ChatUser();

									chatuser.userName = vUserName;
									chatuser.customername = vCustomerName;
									chatuser.profileUrl = vProfileImageName;
									chatuser.id = icustomerid;

									if (commonClass.xmppconnection
											.isConnected()) {

										Roster roster = commonClass.xmppconnection
												.getRoster();

										Presence presence = roster.getPresence(vUserName
												.toLowerCase()
												+ "@"
												+ commonClass.strServerName
														.toLowerCase());

										SharedPreferences preference = getActivity()
												.getSharedPreferences(
														"FriendList",
														Context.MODE_PRIVATE);

										// preference.edit()
										// .putInt(vUserName, 0)
										// .commit();

										if (presence.isAvailable()) {
											chatuser.islonlie = true;

										} else {
											chatuser.islonlie = false;

										}

										commonClass.arrchats.add(chatuser);

									} else {
										// Toast.makeText(getActivity(),
										// "not connected", 9000)
										// .show();
										chatuser.islonlie = false;
										commonClass.arrchats.add(chatuser);
										// arrOfflineChats.add(chatuser);
										setchatconnection.setchatconnection();
										// Log.d("arr offline chat size>>", ""
										// + arrOfflineChats.size());

									}
								}
							}

						}

					} catch (Exception e) {
					}

				}

				@Override
				public void onPostExecute() {
					// TODO Auto-generated method stub
					topbarInterface.hideDialog();
					if (!(commonClass.checkInternetConnection())) {
						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub

									}
								}, getResources().getString(
										R.string.InternetConnect));
						dsp.show(getFragmentManager(), "");
						dsp.setCancelable(false);
					} else {
						if (statusCode.equals("420")) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topbarInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.PoorInternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						} else if (statusCode.equals("101")) {
							// txtFriendschatNoData
							// .setText(strMessageResponse);

							lstChatListUsers.setVisibility(View.GONE);
							txtFriendschatNoData.setVisibility(View.VISIBLE);

						} else {

							lstChatListUsers.setVisibility(View.VISIBLE);
							txtFriendschatNoData.setVisibility(View.GONE);

							try {

								// ((DeelFeedActivity) getActivity())
								// .initopenfireconnection();

								// if (commonClass.xmppconnection != null
								// && commonClass.xmppconnection
								// .isConnected()) {
								Log.d("in>>>>", "innn set");
								adapterChatList = new ChatListSingleAdapter(
										getActivity(), commonClass.arrchats);
								lstChatListUsers.setAdapter(adapterChatList);
								// }

								((DeelFeedActivity) getActivity())
										.initopenfireconnection();
							} catch (Exception e) {

							}

							// adapterChatList = new ChatListAdapter(
							// getActivity(), arrOnlineChats,
							// arrOfflineChats);
							// lstChatListUsers.setAdapter(adapterChatList);
						}
					}

				}
			});
			as.execute();
		}
	}

	public void deletefriend(String username) {
		for (int i = 0; i < commonClass.arrchats.size(); i++) {
			ChatUser ch = commonClass.arrchats.get(i);
			if (ch.userName.equals(username)) {

				commonClass.arrchats.remove(i);
				adapterChatList = new ChatListSingleAdapter(getActivity(),
						commonClass.arrchats);
				lstChatListUsers.setAdapter(adapterChatList);
			}
		}
	}
}