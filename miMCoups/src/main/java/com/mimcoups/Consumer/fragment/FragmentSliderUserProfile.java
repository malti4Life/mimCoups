package com.mimcoups.Consumer.fragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.mimcoups.Consumer.DialogFragment.CameraDialogFragment;
import com.mimcoups.Consumer.DialogFragment.CurrencyDailogFragment;
import com.mimcoups.Consumer.DialogFragment.DatePickerFragment;
import com.mimcoups.Consumer.DialogFragment.DialogSpinnerFragment;
import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.mimcoups.Consumer.customClass.DateCustomeClass;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.SplashActivity;
import com.mimcoups.Consumer.DialogFragment.SelectionButtonAlert;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.FontFitTextView;
import com.mimcoups.Consumer.customClass.UserDetail;
import com.mimcoups.Consumer.inteface.DialogNotificationPoupInterface;
import com.mimcoups.Consumer.inteface.wrongdatedialoginterface;

public class FragmentSliderUserProfile extends Fragment {

	long MIN_CLICK_INTERVAL = 1000;
	long lastClickTime = 0;
	private ImageView imgUserProfileBack, imgUserProfileUserImage;
	private topBarInteface topInterface;
	private EditText edtUserProfileUserName, edtUserProfileZipCode,
			edtUserProfileEmailAddress, edtUserProfileName,
			edtUserProfilePassword;
	private LinearLayout lnUserProfileMale, lnUserProfileFemale,
			lnUserProfileChangeLanguage, lnUserProfileRadious,
			lnUserProfileSelectNotification;
	private TextView txtuserProfileLanguage, txtUserProfileCurrency,
			txtUserProfileRadious, txtUserProfileNotificationNumber;
	FontFitTextView txtUserProfileBirthDate;
	private Button btnUserProfileUpdate, btnUserProfileChangePassword;
	private ToggleButton tglNotification;
	private int selectedlanguage;
	Locale myLocale;
	private WebService_Call webServiceCall = new WebService_Call();
	CommonData commonClass;
	Context mcontext;
	LinearLayout lnUpdateProfile;
	private Drawable drUserProfile;
	private String strGender = "Male";
	private File FileImage;
	private SharedPreferences preference;
	private String url_response = "", VStatusCode = "", vMessageResponse = "";
	private UserDetail userDetail = new UserDetail();
	private ArrayList<CategoryDetail> arrNotification;
	private ArrayList<CategoryDetail> arrMiles = new ArrayList<CategoryDetail>();
	int clicklanguage;
	String current_language = "";
	boolean showlocationinformdialog = false;
	boolean ischangeprofile = false;
	RelativeLayout relprofileback;

	// Unbindservice unbind;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.userprofile_test, container,
				false);

		initControls(view);
		ControlsClickEvents(view);

		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {
						@Override
						public void onPreExecute() {
							topInterface.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							try {
								String geturl = "CustomerService.svc/GetProfile?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId;
								geturl = commonClass
										.Url_EncodeWithBlank(geturl);
								 Log.e("called userprofile", geturl);
								url_response = webServiceCall
										.makeServicegetRequest(geturl);
								
								Log.e("called userprofile", url_response);
								JSONObject js_obj_getprofileresult = new JSONObject(
										url_response);
								VStatusCode = js_obj_getprofileresult
										.getString("vStatusCode");
								vMessageResponse = js_obj_getprofileresult
										.getString("vMessageResponse");

								if (VStatusCode.equals("100")) {

									userDetail = new UserDetail();

									userDetail.password = js_obj_getprofileresult
											.getString("vPassword");
									userDetail.birthDate = js_obj_getprofileresult
											.getString("dBirthDate");

									userDetail.radious = js_obj_getprofileresult
											.getString("dRadius");

									userDetail.emailAddress = js_obj_getprofileresult
											.getString("vEmail");
									userDetail.gender = js_obj_getprofileresult
											.getString("vGender");

									userDetail.profileUrl = js_obj_getprofileresult
											.getString("vProfileImageName");
									userDetail.customerName = js_obj_getprofileresult
											.getString("vCustomerName");
									userDetail.username = js_obj_getprofileresult
											.getString("vUserName");
									userDetail.zipCode = js_obj_getprofileresult
											.getString("vZipcode");
									userDetail.notification = js_obj_getprofileresult
											.getString("iPerDayNotification");

									userDetail.bIsMasterNotification = js_obj_getprofileresult
											.getBoolean("bIsMasterNotification");

									userDetail.iMaxRadius = js_obj_getprofileresult
											.getString("iMaxRadius");

									userDetail.iMaxPerDayNotificationLimit = js_obj_getprofileresult
											.getString("iMaxPerDayNotificationLimit");

								}
							} catch (Exception e) {
							}
						}

						@Override
						public void onPostExecute() {
							topInterface.hideDialog();
							try {

								if (!commonClass.checkInternetConnection()) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													topInterface
															.topbarClick("back");
												}
											}, getResources().getString(
													R.string.InternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);
								} else {
									if (VStatusCode.equals("420")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {

														topInterface
																.topbarClick("back");
													}
												},
												getResources()
														.getString(
																R.string.PoorInternetConnect));
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);

									} else if (VStatusCode.equals("100")) {
										txtUserProfileNotificationNumber
												.setText(userDetail.notification
														+ " Notifications");
										edtUserProfileZipCode
												.setText(userDetail.zipCode);

										txtUserProfileBirthDate
												.setText(getgenerater_birthdate(userDetail.birthDate));

										if (userDetail.gender.equals("Male")) {
											strGender = "Male";
											lnUserProfileMale.setBackgroundColor(Color
													.parseColor("#72B600"));
											lnUserProfileFemale.setBackgroundColor(Color
													.parseColor("#1068AF"));
										} else if (userDetail.gender
												.equals("Female")) {
											strGender = "Female";
											lnUserProfileFemale.setBackgroundColor(Color
													.parseColor("#72B600"));
											lnUserProfileMale.setBackgroundColor(Color
													.parseColor("#1068AF"));
										}

										edtUserProfileName
												.setText(userDetail.customerName);

										edtUserProfileEmailAddress
												.setText(userDetail.emailAddress);

										edtUserProfileUserName
												.setText(userDetail.username);

										setLanguageAndCurrency();

										tglNotification
												.setChecked(userDetail.bIsMasterNotification);

										txtUserProfileRadious
												.setText(userDetail.radious
														+ " kilometer");
										arrNotification = new ArrayList<CategoryDetail>();
										// int notificationint = Integer
										// .parseInt(userDetail.iMaxPerDayNotificationLimit);
										// int current = Integer
										// .parseInt(userDetail.notification);
										// int temp = notificationint;
										// for (int i = 10; i <= temp; i = i +
										// 10) {
										//
										// CategoryDetail notification = new
										// CategoryDetail();
										//
										// if (current == i) {
										// notification.CategoryFavourite =
										// true;
										// } else {
										// notification.CategoryFavourite =
										// false;
										// }
										// notification.categoryName = i
										// + " Notifications";
										//
										// arrNotification.add(notification);
										//
										// }

										selectNotificationLimit_mamnagedata();

										async_deel_feed_list as = new async_deel_feed_list(
												new asynctaskloaddata() {
													Bitmap loadbitmapfromurl = null;


													@Override
													public void onPreExecute() {

													}

													@Override
													public void doInBackground() {
														try {

															loadbitmapfromurl = getBitmapFromURL(userDetail.profileUrl
																	.trim());

														} catch (Exception e) {
															e.printStackTrace();
														}

													}

													@Override
													public void onPostExecute() {
														if (loadbitmapfromurl != null) {

															imgUserProfileUserImage
																	.setImageBitmap(setimagebitmap(loadbitmapfromurl));
															setimagebitmap(setimagebitmap(loadbitmapfromurl));
															// convertFileToBitmap(loadbitmapfromurl);
															userprofileradious();
														}

													}
												});
										as.execute();

										// imgUserProfileUserImage.buildDrawingCache();
										//
										// Bitmap bmap =
										// setimagebitmap(imgUserProfileUserImage
										// .getDrawingCache());
										// bmap = imgUserProfileUserImage
										// .getDrawingCache();

									} else {

										if (!vMessageResponse
												.equalsIgnoreCase("")) {
											DialogFragment ds = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {
														}
													}, vMessageResponse);
											ds.show(getFragmentManager(), "");
										}

									}
								}

							} catch (Exception e) {
							}
						}
					});
			as.execute();
		}

		return view;
	}

	private void initControls(View view) {

		mcontext = getActivity();
		commonClass = (CommonData) getActivity().getApplicationContext();
		current_language = commonClass.languageId;
		clicklanguage = Integer.valueOf(commonClass.languageId);

		preference = getActivity().getSharedPreferences("UserDetail",
				Context.MODE_PRIVATE);

		btnUserProfileChangePassword = (Button) view
				.findViewById(R.id.btnChangePassword);

		tglNotification = (ToggleButton) view
				.findViewById(R.id.tglNotification);

		imgUserProfileBack = (ImageView) view
				.findViewById(R.id.imgUserProfileBack);
		imgUserProfileUserImage = (ImageView) view
				.findViewById(R.id.imgUserProfileUserImage);

		relprofileback = (RelativeLayout) view
				.findViewById(R.id.relprofileback);

		edtUserProfileZipCode = (EditText) view
				.findViewById(R.id.edtUserProfileZipCode);
		edtUserProfileEmailAddress = (EditText) view
				.findViewById(R.id.edtUserProfileEmailAddress);

		edtUserProfileName = (EditText) view
				.findViewById(R.id.edtUserProfileName);
		edtUserProfileUserName = (EditText) view
				.findViewById(R.id.edtUserProfileUserName);

		edtUserProfilePassword = (EditText) view
				.findViewById(R.id.edtUserProfilePassword);

		txtUserProfileBirthDate = (FontFitTextView) view
				.findViewById(R.id.txtUserProfileBirthDate);

		txtUserProfileCurrency = (TextView) view
				.findViewById(R.id.txtUserProfileCurrency);

		txtUserProfileRadious = (TextView) view
				.findViewById(R.id.txtUserProfileRadious);

		txtUserProfileNotificationNumber = (TextView) view
				.findViewById(R.id.txtUserProfileNotificationNumber);

		lnUserProfileMale = (LinearLayout) view
				.findViewById(R.id.lnUserProfileMale);

		lnUserProfileFemale = (LinearLayout) view
				.findViewById(R.id.lnUserProfileFemale);

		lnUserProfileChangeLanguage = (LinearLayout) view
				.findViewById(R.id.lnUserProfileChangeLanguage);

		lnUserProfileRadious = (LinearLayout) view
				.findViewById(R.id.lnUserProfileRadious);

		lnUserProfileSelectNotification = (LinearLayout) view
				.findViewById(R.id.lnUserProfileSelectNotification);

		btnUserProfileUpdate = (Button) view
				.findViewById(R.id.btnUserProfileUpdate);

		txtuserProfileLanguage = (TextView) view
				.findViewById(R.id.txtuserProfileLanguage);

		lnUpdateProfile = (LinearLayout) view
				.findViewById(R.id.lnUpdateProfile);

		String dir = Environment.getExternalStorageDirectory() + File.separator
				+ "." + getResources().getString(R.string.app_name);

		FileImage = new File(dir, "1.jpg");
		showlocationinformdialog = false;

	}

	private void ControlsClickEvents(View view) {

		imgUserProfileUserImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment ds = new CameraDialogFragment(
						new DialogInterfaceClick() {

							@Override
							public void dialogClick(String tag) {

								if (tag.equals("Camera")) {
									Intent cameraIntent = new Intent(
											android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
									startActivityForResult(cameraIntent, 0);
								} else {

									Intent intent = new Intent(
											Intent.ACTION_PICK,
											android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
									startActivityForResult(intent, 1);

									// Intent intent = new Intent();
									// intent.setType("image/*");
									// intent.setAction(Intent.ACTION_GET_CONTENT);
									// startActivityForResult(Intent
									// .createChooser(intent,
									// "Choose Profile Pic"), 1);
								}
							}
						});
				ds.show(getFragmentManager(), "");
			}
		});
		relprofileback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("back");
			}
		});

		txtUserProfileBirthDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int int_year, int_month, int_date;

				if (txtUserProfileBirthDate.getText().toString().trim()
						.length() != 0) {
					String year = null, month = null, date = null;

					if (txtUserProfileBirthDate.getText().toString()
							.contains("/")) {
						String split[] = txtUserProfileBirthDate.getText()
								.toString().split("/");

						date = split[0];
						month = split[1];
						year = split[2];
					} else {
						String split[] = txtUserProfileBirthDate.getText()
								.toString().split("\\.");

						date = split[0];
						month = split[1];
						year = split[2];
					}

					int_year = Integer.parseInt(year);
					int_month = Integer.parseInt(month);
					int_date = Integer.parseInt(date);

				} else {
					String year = null, month = null, date = null;

					String birthdatenorwain = txtUserProfileBirthDate.getText()
							.toString().trim();
					String[] split22 = birthdatenorwain.split("\\.");

					date = split22[0];
					month = split22[1];
					year = split22[2];
					int_year = Integer.parseInt(year);
					int_month = Integer.parseInt(month);
					int_date = Integer.parseInt(date);

				}

				final Calendar c = Calendar.getInstance();
				final int year = c.get(Calendar.YEAR);
				final int month = c.get(Calendar.MONTH);
				final int day = c.get(Calendar.DAY_OF_MONTH);

				DialogFragment newFragment = new DatePickerFragment(
						new wrongdatedialoginterface() {

							@Override
							public void selecteddateiswrongedialog(
									String message) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

											}
										}, message);
								ds.show(getFragmentManager(), "");
							}

							@Override
							public void ondateselectedevent(
									DateCustomeClass dateclass) {
								// TODO Auto-generated method stub

								String selected_date = convert_proper_date(dateclass.month)
										+ "/"
										+ dateclass.date
										+ "/"
										+ convert_proper_date(dateclass.year);
								Calendar now = Calendar.getInstance();
								Calendar dob = Calendar.getInstance();
								dob.setTime(new Date(selected_date));

								int year1 = now.get(Calendar.YEAR);
								int year2 = dob.get(Calendar.YEAR);
								int age = year1 - year2;
								int month1 = (now.get(Calendar.MONTH));

								int month2 = (dob.get(Calendar.MONTH));

								if (month2 > month1) {
									age--;
								} else if (month1 == month2) {
									int day1 = now.get(Calendar.DAY_OF_MONTH);
									int day2 = dob.get(Calendar.DAY_OF_MONTH);
									// Log.e("Day 1", "" + day1);
									// Log.e("Day 2", "" + day2);
									if (day2 > day1) {
										age--;
									}
								}

								// Age age = calculateAge(new
								// Date(selected_date));

								if (age < 13) {
									txtUserProfileBirthDate
											.setText(getgenerater_birthdate(userDetail.birthDate));
									Toast.makeText(
											getActivity(),
											getResources().getString(
													R.string.agelimit), 3000)
											.show();
									;

								}

								// s
								//
								// SimpleDateFormat sdf = new SimpleDateFormat(
								// "dd/MM/yyyy");
								// Date birthDate = null;
								// try {
								// String my_birthdate = day + "/" + month
								// + "/" + year;
								// try {
								// birthDate = sdf.parse(my_birthdate);
								// } catch (java.text.ParseException e) {
								// // TODO Auto-generated catch block
								// e.printStackTrace();
								// }
								// AgeCalculator agecalculator = new
								// AgeCalculator();
								// Age age = agecalculator
								// .calculateAge(birthDate);
								// Log.d("vv age difference",
								// "" + age.getYears());
								// // int agediff = age.getYears();
								// // if (agediff < 16) {
								// // Toast.makeText(
								// // getActivity(),
								// // getResources()
								// // .getString(
								// // R.string.agelimit),
								// // 9000).show();
								// // }
								// } catch (ParseException e) {
								// e.printStackTrace();
								// }

							}
						}, getActivity(), txtUserProfileBirthDate,
						commonClass.languageId, int_year, int_month, int_date);
				newFragment.show(getFragmentManager(), "datePicker");
			}
		});

		lnUserProfileRadious.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// DialogFragment dsp = new DialogSpinnerFragment(
				// new DialogNotificationPoupInterface() {
				//
				// @Override
				// public void DialogNotificationClick(
				// CategoryDetail nm) {
				// txtUserProfileRadious.setText(nm.categoryName);
				// }
				// }, arrMiles);
				// dsp.show(getFragmentManager(), "");
				userprofileradious_dialog();
			}

		});

		lnUserProfileSelectNotification
				.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// DialogFragment dsp = new DialogSpinnerFragment(
						// new DialogNotificationPoupInterface() {
						//
						// @Override
						// public void DialogNotificationClick(
						// CategoryDetail nm) {
						// txtUserProfileNotificationNumber
						// .setText(nm.categoryName);
						// }
						// }, arrNotification);
						// dsp.show(getFragmentManager(), "");

						selectNotificationLimit();
					}
				});
		lnUserProfileMale.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				strGender = "Male";
				lnUserProfileMale.setBackgroundColor(Color
						.parseColor("#72B600"));
				lnUserProfileFemale.setBackgroundColor(Color
						.parseColor("#1068AF"));
			}
		});
		lnUserProfileFemale.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				strGender = "Female";
				lnUserProfileFemale.setBackgroundColor(Color
						.parseColor("#72B600"));
				lnUserProfileMale.setBackgroundColor(Color
						.parseColor("#1068AF"));
			}
		});
		lnUserProfileChangeLanguage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					DialogFragment ds = new CurrencyDailogFragment(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									if (tag.equals("en")) {
										clicklanguage = 1;

										// if
										// (commonClass.languageId.equals("2"))
										// {
										// commonClass.languageId = "1";
										// changeLanguage(tag);
										// }
										txtuserProfileLanguage
												.setText("Australia");
										txtUserProfileCurrency
												.setText("Australian Dollar(AUD)");
									} else {
										clicklanguage = 2;
										// if
										// (commonClass.languageId.equals("1"))
										// {
										// commonClass.languageId = "2";
										// changeLanguage(tag);
										// }
										txtuserProfileLanguage
												.setText("Norway");
										txtUserProfileCurrency
												.setText("Norwegian Kron(NOK)");
									}
								}
							}, String.valueOf(clicklanguage));
					ds.show(getFragmentManager(), "");
				}
			}
		});

		btnUserProfileUpdate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (edtUserProfileZipCode.getText().toString().trim()
							.equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
										.getText(R.string.zipvalidation)
										.toString());
						ds.show(getFragmentManager(), "");

					}

					else if (txtUserProfileBirthDate
							.getText()
							.toString()
							.trim()
							.equals(""
									+ getActivity().getResources().getString(
											R.string.registerBirthDate))) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
										.getText(R.string.birthdatevalidation)
										.toString());
						ds.show(getFragmentManager(), "");
					}

					else if (edtUserProfileName.getText().toString().trim()
							.equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {
									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
										.getText(R.string.namevalidation)
										.toString());
						ds.show(getFragmentManager(), "");
					}

					else if (edtUserProfileEmailAddress.getText().toString()
							.trim().equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
										.getText(R.string.emailvalidation)
										.toString());
						ds.show(getFragmentManager(), "");
					} else if (!Patterns.EMAIL_ADDRESS.matcher(
							edtUserProfileEmailAddress.getText().toString()
									.trim()).matches()) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
										.getText(R.string.validemailvalidation)
										.toString());
						ds.show(getFragmentManager(), "");
					} else if (edtUserProfileUserName.getText().toString()
							.trim().equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
										.getText(R.string.usernamevalidation)
										.toString());
						ds.show(getFragmentManager(), "");
					} else if (edtUserProfilePassword.getText().toString()
							.trim().equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
										.getText(R.string.passvalidation)
										.toString());
						ds.show(getFragmentManager(), "");
					} else if (edtUserProfilePassword.getText().length() < 6
							|| edtUserProfilePassword.getText().length() > 30) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								},
								getActivity()
										.getResources()
										.getText(
												R.string.passwordlengthvalidation)
										.toString());
						ds.show(getFragmentManager(), "");

					}

					else if (!commonClass.checkInternetConnection()) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {
									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
										.getText(R.string.InternetConnect)
										.toString());
						ds.show(getFragmentManager(), "");
						ds.setCancelable(false);
					} else {

						drUserProfile = imgUserProfileUserImage.getDrawable();
						if (drUserProfile == getResources().getDrawable(
								R.drawable.icon_camera)) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {
										@Override
										public void dialogClick(String tag) {
										}
									}, getActivity().getResources()
											.getText(R.string.UserCaptureImage)
											.toString());
							ds.show(getFragmentManager(), "");
						} else {
							if(commonClass.currentLatitude==0 && commonClass.currentLongitude==0 ){
								
								if (commonClass.languageId.equals("1")) {

									commonClass.currentLatitude = -33.7969235;
									commonClass.currentLongitude = 150.9224326;

									
								} else {

									commonClass.currentLatitude = 59.8938549;
									commonClass.currentLongitude = 10.7851165;

									
								}
								
							}
							
							
							
							async_deel_feed_list as = new async_deel_feed_list(
									new asynctaskloaddata() {

										String url_response = "";
										String password = "";

										@Override
										public void onPreExecute() {
											topInterface.showDialog("Loading");
										}

										@Override
										public void doInBackground() {
											try {

												Boolean sendnotificationval = tglNotification
														.isChecked();

												String userprofileradius = txtUserProfileRadious
														.getText().toString();
												String[] userprofileradiusarr = userprofileradius
														.split(" ");
												String userprofileradiusval = userprofileradiusarr[0];

												String perdaynotification = txtUserProfileNotificationNumber
														.getText().toString();
												String[] perdaynotificationarr = perdaynotification
														.split(" ");
												String perdaynotificationval = perdaynotificationarr[0];

												if (edtUserProfilePassword
														.getText().toString()
														.trim()
														.equals("DionMims#")) {
													password = commonClass.strPassword;

												} else {
													password = commonClass
															.getMD5EncryptedString(edtUserProfilePassword
																	.getText()
																	.toString()
																	.trim());
												}

												String getUrl = "CustomerService.svc/UpdateCustomer?customerid="
														+ commonClass.CustomerId
														+ "&languageid="
														+ clicklanguage
														+ "&customername="
														+ edtUserProfileName
																.getText()
																.toString()
																.trim()
														+ "&birthdate="
														+ DatePickerFragment.text_to_upload
														+ "&gender="
														+ strGender
														+ "&zipcode="
														+ edtUserProfileZipCode
																.getText()
																.toString()
																.trim()
														+ "&email="
														+ edtUserProfileEmailAddress
																.getText()
																.toString()
																.trim()
														+ "&username="
														+ edtUserProfileUserName
																.getText()
																.toString()
																.trim()
														+ "&password="
														+ password
														+ "&latitude="
														+ commonClass.currentLatitude
														+ "&longitude="
														+ commonClass.currentLongitude
														+ "&sendnotification="
														+ sendnotificationval
														+ "&radius="
														+ userprofileradiusval
														+ "&perdaynotification="
														+ perdaynotificationval;

												getUrl = commonClass
														.Url_EncodeWithBlank(getUrl);

												// Log.d("geturll>>>", "" +
												// getUrl);

												url_response = webServiceCall
														.makeServiceImageUpload(
																getUrl,
																FileImage,
																ischangeprofile);

											} catch (Exception e) {
												e.printStackTrace();
												Log.e("Upload Image",e.getMessage());
											}
										}

										@Override
										public void onPostExecute() {
											topInterface.hideDialog();
											try {
												JSONObject js_obj = new JSONObject(
														url_response);
												String VStatusCode = js_obj
														.getString("vStatusCode");
												String vMessageResponse = js_obj
														.getString("vMessageResponse");

												if (VStatusCode.equals("420")) {
													DialogFragment ds = new SingleButtonAlert(
															new DialogInterfaceClick() {

																@Override
																public void dialogClick(
																		String tag) {

																	// topInterface
																	// .topbarClick("back");
																}
															},
															getResources()
																	.getString(
																			R.string.PoorInternetConnect));
													ds.show(getFragmentManager(),
															"");
													ds.setCancelable(false);

												} else if (VStatusCode
														.equals("100")) {

													commonClass.ProfiileImagePath = js_obj
															.getString("vProfileImageName");

													commonClass.languageId = String
															.valueOf(clicklanguage);

													if (commonClass.languageId
															.equals("1")) {
														changeLanguage("en");
														txtuserProfileLanguage
																.setText("Australia");
														txtUserProfileCurrency
																.setText("Australian Dollar(AUD)");

													}

													if (commonClass.languageId
															.equals("2")) {
														changeLanguage("no");

														txtuserProfileLanguage
																.setText("Norway");
														txtUserProfileCurrency
																.setText("Norwegian Kron(NOK)");
													}

													Editor edt = preference
															.edit();

													edt.putString(
															"customerid",
															commonClass.CustomerId);
													edt.putBoolean("firsttime",
															false);

													edt.putString("emailid",
															edtUserProfileEmailAddress
																	.getText()
																	.toString()
																	.trim());

													edt.putString("password",
															password);

													edt.putString(
															"latitude",
															""
																	+ commonClass.currentLatitude)
															.commit();
													edt.putString(
															"longitude",
															""
																	+ commonClass.currentLongitude)
															.commit();

													edt.putString(
															"languageid",
															commonClass.languageId);
													edt.putString(
															"username",
															commonClass.strUsername);
													commonClass.ProfileName = edtUserProfileName
															.getText()
															.toString().trim();

													edt.putString(
															"customername",
															commonClass.ProfileName);
													edt.commit();

													FragmentSlider.isUserProfile_Updated = true;
													DialogFragment ds = new SingleButtonAlert(
															new DialogInterfaceClick() {
																@Override
																public void dialogClick(
																		String tag) {
																	if (!current_language
																			.equals(""
																					+ clicklanguage)) {

																		// unbind.unbindservice();

																		if (commonClass.xmppconnection != null
																				&& commonClass.xmppconnection
																						.isConnected()) {
																			commonClass.xmppconnection
																					.disconnect();
																		}

																		Intent intent = new Intent(
																				getActivity(),
																				SplashActivity.class);

																		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
																				|

																				Intent.FLAG_ACTIVITY_CLEAR_TASK);
																		getActivity()
																				.startActivity(
																						intent);
																		getActivity()
																				.finish();

																	}

																}
															}, vMessageResponse);
													ds.show(getFragmentManager(),
															"");
													ds.setCancelable(false);

												} else {
													DialogFragment ds = new SingleButtonAlert(
															new DialogInterfaceClick() {
																@Override
																public void dialogClick(
																		String tag) {

																}
															}, vMessageResponse);
													ds.show(getFragmentManager(),
															"");
													ds.setCancelable(false);
												}
											} catch (Exception e) {
											}
										}
									});

							if ((!current_language.equals("" + clicklanguage))
									&& (!checkLocationService())) {
								if (showlocationinformdialog) {
									as.execute();
								} else {
									String locationmessage = "";
									showlocationinformdialog = true;

									if (clicklanguage == 1) {

										commonClass.currentLatitude = -33.7969235;
										commonClass.currentLongitude = 150.9224326;
										locationmessage = getResources()
												.getString(
														R.string.DefaultLocationMessagesydneyupdate);
									} else {

										commonClass.currentLatitude = 59.8938549;
										commonClass.currentLongitude = 10.7851165;

										locationmessage = getResources()
												.getString(
														R.string.DefaultLocationMessageosloupdate);
									}

									Log.d("location service",
											"location service");
									DialogFragment dsp = new SelectionButtonAlert(
											new DialogInterfaceClick() {
												@Override
												public void dialogClick(
														String tag) {
													// TODO Auto-generated
													// method
													// stub

													Intent myIntent = new Intent(
															Settings.ACTION_LOCATION_SOURCE_SETTINGS);
													startActivity(myIntent);

												}
											}, locationmessage);
									dsp.show(getFragmentManager(), "");
								}

							} else {
								as.execute();
							}
						}
					}
				}

			}
		});
		btnUserProfileChangePassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// ((InputMethodManager) getActivity().getSystemService(
				// Context.INPUT_METHOD_SERVICE)).toggleSoftInput(
				// InputMethodManager.SHOW_FORCED,
				// InputMethodManager.HIDE_IMPLICIT_ONLY);
				// InputMethodManager imm = (InputMethodManager) getActivity()
				// .getSystemService(Context.INPUT_METHOD_SERVICE);
				// imm.showSoftInput(edtUserProfilePassword,
				// InputMethodManager.SHOW_FORCED);
				showpassworddialog();

				//
				// edtUserProfilePassword.setEnabled(true);
				// edtUserProfilePassword.setText("");
				// edtUserProfilePassword.setFocusable(true);
				// edtUserProfilePassword.setCursorVisible(true);
				// edtUserProfilePassword.requestFocus();

			}
		});

	}

	public void showpassworddialog() {

		try{

			final Dialog passworddialog = new Dialog(getActivity());
			passworddialog.setContentView(R.layout.rawpassworddialog);
			passworddialog.setCancelable(true);
			passworddialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			int divierId = passworddialog.getContext().getResources()
					.getIdentifier("android:id/titleDivider", null, null);
			View divider = passworddialog.findViewById(divierId);
			divider.setBackgroundColor(Color.TRANSPARENT);
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(passworddialog.getWindow().getAttributes());
			DisplayMetrics displaymetrics = new DisplayMetrics();
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.CENTER;
			getActivity().getWindowManager().getDefaultDisplay()
					.getMetrics(displaymetrics);
			passworddialog.getWindow().setAttributes(lp);

			Button btnCancel = (Button) passworddialog.findViewById(R.id.btnCancel);
			Button btnOk = (Button) passworddialog.findViewById(R.id.btnOk);

			final EditText edtoldpass = (EditText) passworddialog
					.findViewById(R.id.edtoldpassword);

			final EditText edtnewpass = (EditText) passworddialog
					.findViewById(R.id.edtnewpassword);
			final EditText edtconfpass = (EditText) passworddialog
					.findViewById(R.id.edtconfonfirmpassword);

			edtoldpass.setTypeface(Typeface.DEFAULT);
			edtoldpass.setTransformationMethod(new PasswordTransformationMethod());

			edtnewpass.setTypeface(Typeface.DEFAULT);
			edtnewpass.setTransformationMethod(new PasswordTransformationMethod());

			edtconfpass.setTypeface(Typeface.DEFAULT);
			edtconfpass.setTransformationMethod(new PasswordTransformationMethod());

			btnOk.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					String oldpass = commonClass.getMD5EncryptedString(edtoldpass
							.getText().toString().trim());

					String newpass = commonClass.getMD5EncryptedString(edtnewpass
							.getText().toString().trim());

					Log.e("password", "" + newpass);

					if (edtoldpass.getText().toString().trim().length() == 0) {

						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
								.getText(R.string.oldpassvalidation)
								.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					} else if (!oldpass.equalsIgnoreCase(userDetail.password)) {

						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
								.getText(R.string.oldpasscomparevalidation)
								.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					} else if (edtnewpass.getText().toString().trim().length() == 0) {

						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
								.getText(R.string.newpassvalidation)
								.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					} else if (edtnewpass.getText().length() < 6
							|| edtnewpass.getText().length() > 30) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								},
								getActivity()
										.getResources()
										.getText(
												R.string.newpasswordlengthvalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					}


					else if (edtconfpass.getText().toString().trim().length() == 0) {

						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
								.getText(R.string.conformpass).toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					}

					else if ((!edtconfpass.getText().toString().trim()
							.equals(edtnewpass.getText().toString().trim()))) {

						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								}, getActivity().getResources()
								.getText(R.string.txt_confirm_password)
								.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					}
					else if (oldpass.equalsIgnoreCase(newpass)) {

						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
									}
								},
								getActivity()
										.getResources()
										.getText(
												R.string.txt_comapre_old_new_password)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					}
					else {

						userDetail.password = newpass;

						edtUserProfilePassword.setText(edtnewpass.getText()
								.toString().trim());
						passworddialog.dismiss();
						edtUserProfileZipCode.clearFocus();
						edtUserProfileZipCode.setSelected(false);

					}

				}
			});
			btnCancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					passworddialog.dismiss();
					edtUserProfileZipCode.clearFocus();
					edtUserProfileZipCode.setSelected(false);

				}
			});
			edtoldpass.setFocusableInTouchMode(true);
			passworddialog.show();

		}catch (Exception e){

		}



		// Use the current date as the default date in the picker

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
		// unbind = (Unbindservice) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 0 && resultCode == Activity.RESULT_OK) {

			// camera
			Bitmap photo = (Bitmap) data.getExtras().get("data");

			int width = photo.getWidth();
			int height = photo.getHeight();
			int newWidth = 200;
			int newHeight = 200;

			// calculate the scale - in this case = 0.4f
			float scaleWidth = ((float) newWidth) / width;
			float scaleHeight = ((float) newHeight) / height;

			// createa matrix for the manipulation
			Matrix matrix = new Matrix();
			// resize the bit map
			matrix.postScale(scaleWidth, scaleHeight);

			// recreate the new Bitmap
			Bitmap resizedBitmap = Bitmap.createBitmap(photo, 0, 0, width,
					height, matrix, true);

			convertFileToBitmap(resizedBitmap);
			imgUserProfileUserImage.setImageBitmap(resizedBitmap);
			ischangeprofile = true;
		} else if (requestCode == 1 && resultCode == Activity.RESULT_OK
				&& null != data) {

			// gallery
			Uri selectedImage = data.getData();
			try {
				Bitmap bitmap = scaleImage(getActivity(), selectedImage);
				bitmap = setimagebitmap(bitmap);
				imgUserProfileUserImage.setImageBitmap(bitmap);
				convertFileToBitmap(bitmap);
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			ischangeprofile = true;

			// Toast.makeText(getActivity(), "path>>" + selectedImage.getPath(),
			// 9000).show();
			// ExifInterface exif = null;
			// try {
			// exif = new ExifInterface(selectedImage.getPath());
			// } catch (IOException e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// }
			// int exifOrientation = exif.getAttributeInt(
			// ExifInterface.TAG_ORIENTATION,
			// ExifInterface.ORIENTATION_NORMAL);
			//
			// int rotate = 0;
			//
			// switch (exifOrientation) {
			// case ExifInterface.ORIENTATION_ROTATE_90:
			// rotate = 90;
			// break;
			//
			// case ExifInterface.ORIENTATION_ROTATE_180:
			// rotate = 180;
			// break;
			//
			// case ExifInterface.ORIENTATION_ROTATE_270:
			// rotate = 270;
			// break;
			// }
			//
			// Toast.makeText(getActivity(), "rotate>>" + rotate, 9000).show();
			// try {
			// Bitmap photo = MediaStore.Images.Media.getBitmap(getActivity()
			// .getContentResolver(), selectedImage);
			// // Bitmap resized = Bitmap.createScaledBitmap(photo, 200, 200,
			// // true);
			//
			// // createa matrix for the manipulation
			// Matrix matrix = new Matrix();
			// // resize the bit map
			// matrix.postRotate(rotate);
			//
			// // recreate the new Bitmap
			// // Bitmap resizedBitmap = Bitmap.createBitmap(photo, 0, 0, 200,
			// // 200, matrix, true);
			//
			// Bitmap scalebm = Bitmap.createScaledBitmap(photo, 200, 200,
			// true);
			// Bitmap _bitmapScaled = Bitmap.createBitmap(scalebm, 0, 0, 200,
			// 200, matrix, true);
			//
			// convertFileToBitmap(_bitmapScaled);
			// imgUserProfileUserImage.setImageBitmap(_bitmapScaled);
			// } catch (FileNotFoundException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }

			// Picasso.with(mcontext).load(selectedImage)
			// .into(imgUserProfileUserImage);
		}
	}

	public void convertFileToBitmap(Bitmap resized) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(FileImage);
			resized.compress(Bitmap.CompressFormat.PNG, 90, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Throwable ignore) {
			}
		}
	}

	private void changeLanguage(String languageName) {
		myLocale = new Locale(languageName);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);
		// Intent intent = new Intent(getActivity(), RegisterActivity.class);
		// startActivity(intent);
		// getActivity().finish();
	}

	private void setLanguageAndCurrency() {
		// TODO Auto-generated method stub
		if (commonClass.languageId.equals("1")) {
			txtuserProfileLanguage.setText("Australia");
			txtUserProfileCurrency.setText("Australian Dollar(AUD)");
		} else if (commonClass.languageId.equals("2")) {
			txtuserProfileLanguage.setText("Norway");
			txtUserProfileCurrency.setText("Norwegian Kron(NOK)");
		}
	}

	private Bitmap setimagebitmap(Bitmap loadbitmap) {

		int width = loadbitmap.getWidth();
		int height = loadbitmap.getHeight();
		int newWidth = 200;
		int newHeight = 200;
		// calculate the scale - in this case = 0.4f
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;

		// createa matrix for the manipulation
		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(scaleWidth, scaleHeight);

		// recreate the new Bitmap
		final Bitmap resizedBitmap = Bitmap.createBitmap(loadbitmap, 0, 0,
				width, height, matrix, true);
		convertFileToBitmap(resizedBitmap);
		return resizedBitmap;

	}

	public Bitmap getBitmapFromURL(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Bitmap scaleImage(Context context, Uri photoUri) throws IOException {

		int MAX_IMAGE_DIMENSION = 1028;

		InputStream is = context.getContentResolver().openInputStream(photoUri);
		BitmapFactory.Options dbo = new BitmapFactory.Options();
		dbo.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(is, null, dbo);
		is.close();

		int rotatedWidth, rotatedHeight;
		int orientation = getOrientation(context, photoUri);

		if (orientation == 90 || orientation == 270) {
			rotatedWidth = dbo.outHeight;
			rotatedHeight = dbo.outWidth;
		} else {
			rotatedWidth = dbo.outWidth;
			rotatedHeight = dbo.outHeight;
		}

		Bitmap srcBitmap;
		is = context.getContentResolver().openInputStream(photoUri);
		if (rotatedWidth > MAX_IMAGE_DIMENSION
				|| rotatedHeight > MAX_IMAGE_DIMENSION) {
			float widthRatio = ((float) rotatedWidth)
					/ ((float) MAX_IMAGE_DIMENSION);
			float heightRatio = ((float) rotatedHeight)
					/ ((float) MAX_IMAGE_DIMENSION);
			float maxRatio = Math.max(widthRatio, heightRatio);

			// Create the bitmap from file
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = (int) maxRatio;
			srcBitmap = BitmapFactory.decodeStream(is, null, options);
		} else {
			srcBitmap = BitmapFactory.decodeStream(is);
		}
		is.close();

		/*
		 * if the orientation is not 0 (or -1, which means we don't know), we
		 * have to do a rotation.
		 */
		if (orientation > 0) {
			Matrix matrix = new Matrix();
			matrix.postRotate(orientation);

			srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0,
					srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
		}

		String type = context.getContentResolver().getType(photoUri);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (type.equals("image/png")) {
			srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
		} else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
			srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		}
		byte[] bMapArray = baos.toByteArray();
		baos.close();
		return BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
	}

	public static int getOrientation(Context context, Uri photoUri) {
		/* it's on the external media. */
		Cursor cursor = context.getContentResolver().query(photoUri,
				new String[] { MediaStore.Images.ImageColumns.ORIENTATION },
				null, null, null);

		if (cursor.getCount() != 1) {
			return -1;
		}

		cursor.moveToFirst();
		return cursor.getInt(0);
	}

	private CharSequence getgenerater_birthdate(String birthDate) {

		DatePickerFragment.text_to_upload = birthDate;

		String real_date = "";
		try {
			String split[] = birthDate.split("-");

			String year = split[0];
			String month = split[1];
			String date = split[2];

			if (commonClass.languageId.equals("1")) {
				real_date = date + "/" + month + "/" + year;
			} else {
				real_date = date + "." + month + "." + year;

			}
			// 14/08/2014
		} catch (Exception e) {

		}

		return real_date;
	}

	private void userprofileradious() {
		arrMiles.clear();

		int radius = Integer.parseInt(userDetail.iMaxRadius);
		int current = Integer.parseInt(userDetail.radious);
		if (radius > 10) {

			for (int i = 10; i <= radius; i = i + 10) {

				CategoryDetail notification = new CategoryDetail();

				if (current == i) {
					notification.CategoryFavourite = true;
				} else {
					notification.CategoryFavourite = false;
				}
				notification.categoryName = i + " kilometer";

				arrMiles.add(notification);

			}

		} else {

			for (int i = 1; i <= radius; i++) {

				CategoryDetail notification = new CategoryDetail();

				if (current == i) {
					notification.CategoryFavourite = true;
				} else {
					notification.CategoryFavourite = false;
				}
				notification.categoryName = i + " kilometer";

				arrMiles.add(notification);

			}
		}

	}

	private void userprofileradious_dialog() {

		DialogFragment dsp = new DialogSpinnerFragment(
				new DialogNotificationPoupInterface() {

					@Override
					public void DialogNotificationClick(CategoryDetail nm) {

						txtUserProfileRadious.setText(nm.categoryName);

					}
				}, arrMiles);

		dsp.show(getFragmentManager(), "");

	}

	private void selectNotificationLimit() {

		DialogFragment dsp = new DialogSpinnerFragment(
				new DialogNotificationPoupInterface() {

					@Override
					public void DialogNotificationClick(CategoryDetail nm) {

						txtUserProfileNotificationNumber.setText(nm.categoryName);
					}
				}, arrNotification);
		dsp.show(getFragmentManager(), "");

	}

	private void selectNotificationLimit_mamnagedata() {
		int notificationint = Integer
				.parseInt(userDetail.iMaxPerDayNotificationLimit);
		int current = Integer.parseInt(userDetail.notification);

		int temp = notificationint;

		if (notificationint > 10) {
			for (int i = 10; i <= temp; i = i + 10) {
				CategoryDetail notification = new CategoryDetail();
				if (current == i) {
					notification.CategoryFavourite = true;
				} else {
					notification.CategoryFavourite = false;
				}
				notification.categoryName = i + " Notifications";
				arrNotification.add(notification);
			}
		} else {
			for (int i = 1; i <= temp; i++) {
				CategoryDetail notification = new CategoryDetail();
				if (current == i) {
					notification.CategoryFavourite = true;
				} else {
					notification.CategoryFavourite = false;
				}
				notification.categoryName = i + " Notifications";
				arrNotification.add(notification);
			}
		}
	}

	private int getAge(String selectedMilli) {

		Date dateOfBirth = new Date(selectedMilli);
		Calendar dob = Calendar.getInstance();
		dob.setTime(dateOfBirth);
		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			age--;
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
				&& today.get(Calendar.DAY_OF_MONTH) < dob
						.get(Calendar.DAY_OF_MONTH)) {
			age--;
		}

		if (age < 18) {

		} else {

		}

		String str_age = age + "";
		// Log.d("", getClass().getSimpleName() + ": Age in year= " + age);
		return age;
	}

	public String convert_proper_date(int value) {

		String converted = null;
		if (value > 0 && value < 10) {
			converted = "0" + value;
			return converted;
		} else {
			converted = "" + value;
			return converted;
		}

	}

	public boolean checkLocationService() {
		LocationManager lm = null;
		boolean gps_enabled = false, network_enabled = false;
		if (lm == null)
			lm = (LocationManager) getActivity().getSystemService(
					Context.LOCATION_SERVICE);
		try {
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}

		if (!gps_enabled && !network_enabled) {
			return false;

		}
		return true;
	}

}