package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.QuestionListAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.QuestionList;

public class FragmentQuestionList extends Fragment {

	private ListView lstQuestionsList;
	private QuestionListAdapter Questionlistadapter;
	private topBarInteface topbar;
	private ImageView imgQuestionGlobalBack;
	private ArrayList<QuestionList> arrAllQuestions = new ArrayList<QuestionList>();
	private CommonData commonData;
	private WebService_Call web_Service = new WebService_Call();
	private String url_response = "";
	RelativeLayout relmainfragment_question;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_question, container,
				false);
		initControls(view);
		clickEvents();
		if (!commonData.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub

						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topbar.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							// TODO Auto-generated method stub
							String geturl = "CustomerService.svc/GetFAQCategoryWiseForList?languageid="
									+ commonData.languageId;

							url_response = web_Service
									.makeServicegetRequest(geturl);
							try {
								JSONObject json_Obj = new JSONObject(
										url_response);

								JSONArray json_result = json_Obj
										.getJSONArray("Categories");
								for (int i = 0; i < json_result.length(); i++) {
									JSONObject obj = json_result
											.getJSONObject(i);
									JSONArray json_innerarray = obj
											.getJSONArray("FAQs");

									QuestionList question = new QuestionList();
									question.isCategory = true;
									question.categoryName = obj
											.getString("vCategoryName");
									arrAllQuestions.add(question);

									for (int j = 0; j < json_innerarray
											.length(); j++) {
										QuestionList nestedQuestions = new QuestionList();
										JSONObject innerobject = json_innerarray
												.getJSONObject(j);
										nestedQuestions.questionId = innerobject
												.getString("iFAQId");
										nestedQuestions.questionName = innerobject
												.getString("vQuestion");
										nestedQuestions.isCategory = false;
										arrAllQuestions.add(nestedQuestions);
									}
								}
							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							topbar.hideDialog();

							if (!commonData.checkInternetConnection()) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub

											}
										}, getResources().getString(
												R.string.InternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);
							} else {

								if (arrAllQuestions.size() != 0) {
									Questionlistadapter = new QuestionListAdapter(
											getActivity(), arrAllQuestions);
									lstQuestionsList
											.setAdapter(Questionlistadapter);
								}
							}

						}
					});
			as.execute();
		}
		return view;
	}

	private void initControls(View view) {

		relmainfragment_question = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_question);
		lstQuestionsList = (ListView) view.findViewById(R.id.lstQuestionsList);
		imgQuestionGlobalBack = (ImageView) view
				.findViewById(R.id.imgQuestionGlobalBack);
		commonData = (CommonData) getActivity().getApplicationContext();

	}

	public void clickEvents() {

		relmainfragment_question.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		imgQuestionGlobalBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topbar.topbarClick("back");
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topbar = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

}