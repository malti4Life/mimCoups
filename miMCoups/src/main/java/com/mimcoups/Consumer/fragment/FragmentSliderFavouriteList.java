package com.mimcoups.Consumer.fragment;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.FavouriteListInterface;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentSliderFavouriteList extends Fragment {

	private ImageView imgFavouriteListQuestions, imgFavouriteListSearch;
	private RelativeLayout rlFavouriteListLocations, rlFavouriteListDeals,
			rlFavouriteListCategory;
	private TextView txtFavouriteLocationCount, txtFavouriteDealCount,
			txtFavouriteCategoryCount;
	private topBarInteface topInteface;
	private FavouriteListInterface favouriteInteface;
	private CommonData commonData;
	private String vStatusCode = "", getUrl = "", url_response = "";
	private WebService_Call web_Service = new WebService_Call();
	private String iFavoriteCategoryCount = "", iFavoriteDealCount = "",
			iFavoriteLocationCount = "", vMessageResponse = "";

	RelativeLayout relmainfragment_favouritelist;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_favouritelist,
				container, false);
		initControls(view);
		clickEvents();
		getAllData();
		return view;
	}

	public void initControls(View view) {
		commonData = (CommonData) getActivity().getApplicationContext();

		relmainfragment_favouritelist = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_favouritelist);

		imgFavouriteListQuestions = (ImageView) view
				.findViewById(R.id.imgFavouriteListQuestions);
		imgFavouriteListSearch = (ImageView) view
				.findViewById(R.id.imgFavouriteListSearch);
		rlFavouriteListLocations = (RelativeLayout) view
				.findViewById(R.id.rlFavouriteListLocations);
		rlFavouriteListDeals = (RelativeLayout) view
				.findViewById(R.id.rlFavouriteListDeals);
		rlFavouriteListCategory = (RelativeLayout) view
				.findViewById(R.id.rlFavouriteListCategory);
		txtFavouriteCategoryCount = (TextView) view
				.findViewById(R.id.txtFavouriteCategoryCount);
		txtFavouriteDealCount = (TextView) view
				.findViewById(R.id.txtFavouriteDealCount);
		txtFavouriteLocationCount = (TextView) view
				.findViewById(R.id.txtFavouriteLocationCount);
	}

	private void clickEvents() {

		relmainfragment_favouritelist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		imgFavouriteListQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					topInteface.topbarClick("questions");
				}
			}
		});
		imgFavouriteListSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInteface.topbarClick("search");
			}
		});
		rlFavouriteListLocations.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				favouriteInteface.favouriteDealClick("Location");
			}
		});
		rlFavouriteListCategory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				favouriteInteface.favouriteDealClick("Category");
			}
		});
		rlFavouriteListDeals.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				favouriteInteface.favouriteDealClick("Deal");
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInteface = (topBarInteface) activity;
		favouriteInteface = (FavouriteListInterface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void getAllData() {
		if (!commonData.checkInternetConnection()) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
							topInteface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);
		} else {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub

						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							getUrl = "CustomerService.svc/GetAllFavoriteCount?customerid="
									+ commonData.CustomerId
									+ "&languageid="
									+ commonData.languageId;
							url_response = web_Service
									.makeServicegetRequest(getUrl);
							try {
								JSONObject json_Obj = new JSONObject(
										url_response);
								vStatusCode = json_Obj.getString("vStatusCode");
								if (vStatusCode.equals("100")) {
									iFavoriteCategoryCount = json_Obj
											.getString("iFavoriteCategoryCount")
											+ " "
											+ getResources().getString(
													R.string.favourite_title);
									iFavoriteDealCount = json_Obj
											.getString("iFavoriteDealCount")
											+ " "
											+ getResources().getString(
													R.string.favourite_title);
									iFavoriteLocationCount = json_Obj
											.getString("iFavoriteLocationCount")
											+ " "
											+ getResources().getString(
													R.string.favourite_title);
								} else {
									vMessageResponse = json_Obj
											.getString("vMessageResponse");
								}
							} catch (Exception e) {

							}

						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub

							if (!commonData.checkInternetConnection()) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub
												topInteface.topbarClick("back");
											}
										}, getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							} else {
								if (vStatusCode.equals("100")) {
									txtFavouriteCategoryCount
											.setText(iFavoriteCategoryCount);
									txtFavouriteDealCount
											.setText(iFavoriteDealCount);
									txtFavouriteLocationCount
											.setText(iFavoriteLocationCount);
								} else {

									if (vMessageResponse.trim().length() != 0) {
										DialogFragment dsp = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// TODO Auto-generated
														// method
														// stub
														topInteface
																.topbarClick("back");
													}
												}, vMessageResponse);
										dsp.show(getFragmentManager(), "");
									}
								}
							}

						}
					});
			as.execute();
		}
	}
}
