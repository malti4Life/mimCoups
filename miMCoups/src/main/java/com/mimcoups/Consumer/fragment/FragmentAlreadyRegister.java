package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.DialogFragment.ForgotPasswordFragment;
import com.mimcoups.Consumer.Facebook.FacebookLoginActivity;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.alreadyRegisterInteface;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.RegisterActivity;
import com.mimcoups.Consumer.SignInActivity;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.UserDetail;

public class FragmentAlreadyRegister extends Fragment {

	private TextView txtAlreadyForgotPassword, txtNewUserRegister;
	private Button btnAlreadyRegister;
	private ImageView imgAlreadyRegisterBack;
	private LinearLayout lnAlreadyRegisterFacebook,
			lnAlreadyRegisterGooglePlus;
	private EditText edtAlreadyRegisterEmailAddress,
			edtAlreadyRegisterPassword;
	private topBarInteface topbar;
	private alreadyRegisterInteface alreadyCLick;
	private CommonData commonClass;
	private WebService_Call webService = new WebService_Call();
	private String url_response, strMimsLoginWith = "Mims",
			strEmailAddress = "", strEmailPassword = "";
	private Context context;
	private UserDetail userdetail;
	private SharedPreferences preference;
	private String strStatusCode = "";
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_already_register,
				container, false);
		initControls(view);
		clickEvents();
		return view;
	}

	public void initControls(View view) {

		imgAlreadyRegisterBack = (ImageView) view
				.findViewById(R.id.imgAlreadyRegisterBack);
		txtAlreadyForgotPassword = (TextView) view
				.findViewById(R.id.txtAlreadyForgotPassword);
		txtNewUserRegister = (TextView) view.findViewById(R.id.txtNewUser);
		btnAlreadyRegister = (Button) view
				.findViewById(R.id.btnAlreadyRegister);
		edtAlreadyRegisterEmailAddress = (EditText) view
				.findViewById(R.id.edtAlreadyRegisterEmailAddress);
		edtAlreadyRegisterPassword = (EditText) view
				.findViewById(R.id.edtAlreadyRegisterPassword);
		lnAlreadyRegisterFacebook = (LinearLayout) view
				.findViewById(R.id.lnAlreadyRegisterFacebook);
		lnAlreadyRegisterGooglePlus = (LinearLayout) view
				.findViewById(R.id.lnAlreadyRegisterGooglePlus);
		commonClass = (CommonData) getActivity().getApplicationContext();

	}

	public void clickEvents() {

		txtNewUserRegister.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				View view = getActivity().getCurrentFocus();
				if (view != null) {
					InputMethodManager inputManager = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
				}

				alreadyCLick.alreadyRegisterClick("Register");

			}
		});

		imgAlreadyRegisterBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				topbar.topbarClick("back");
			}
		});

		txtAlreadyForgotPassword.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogFragment ds = new ForgotPasswordFragment(
						new DialogInterfaceClick() {
							@Override
							public void dialogClick(String tag) {

								if (!commonClass.checkInternetConnection()) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {
												@Override
												public void dialogClick(
														String tag) {
													// TODO Auto-generated
													// method stub
												}
											},
											getActivity()
													.getResources()
													.getText(
															R.string.InternetConnect)
													.toString());
									ds.show(getActivity().getFragmentManager(),
											"");
									ds.setCancelable(false);
								} else {
									forgotAsync(tag);
								}

								// commonClass.toastDisplay(tag);
							}
						});
				ds.show(getActivity().getFragmentManager(), "");
			}
		});

		btnAlreadyRegister.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (edtAlreadyRegisterEmailAddress.getText().toString()
							.trim().equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {
									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								},
								getActivity()
										.getResources()
										.getText(
												R.string.usernameemailvalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getActivity().getFragmentManager(), "");
					} else if (edtAlreadyRegisterEmailAddress.getText()
							.toString().trim().contains(" ")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								},
								getActivity()
										.getResources()
										.getText(
												R.string.usernamespacevalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getActivity().getFragmentManager(), "");

					}

					else if (edtAlreadyRegisterPassword.getText().toString()
							.trim().equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {
									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.passvalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getActivity().getFragmentManager(), "");
					} else if (!commonClass.checkInternetConnection()) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {
									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.InternetConnect)
										.toString());
						ds.show(getActivity().getFragmentManager(), "");
						ds.setCancelable(false);
					} else {
						View view = getActivity().getCurrentFocus();
						if (view != null) {
							InputMethodManager inputManager = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							inputManager.hideSoftInputFromWindow(
									view.getWindowToken(),
									InputMethodManager.HIDE_NOT_ALWAYS);
						}
						strEmailAddress = edtAlreadyRegisterEmailAddress
								.getText().toString().trim();
						strEmailPassword = edtAlreadyRegisterPassword.getText()
								.toString().trim();
						alreadyLoginAsync();
					}
				}
			}
		});
		lnAlreadyRegisterFacebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				edtAlreadyRegisterEmailAddress.getText().clear();
				edtAlreadyRegisterPassword.getText().clear();
				edtAlreadyRegisterEmailAddress.setText("");
				edtAlreadyRegisterPassword.setText("");

				if (!commonClass.checkInternetConnection()) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {
								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub
								}
							}, getActivity().getResources()
									.getText(R.string.InternetConnect)
									.toString());
					ds.show(getActivity().getFragmentManager(), "");
					ds.setCancelable(false);
				} else {

					Intent intent = new Intent(context,
							FacebookLoginActivity.class);
					startActivityForResult(intent, 4);
				}

			}
		});
		lnAlreadyRegisterGooglePlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				edtAlreadyRegisterEmailAddress.getText().clear();
				edtAlreadyRegisterPassword.getText().clear();
				edtAlreadyRegisterEmailAddress.setText("");
				edtAlreadyRegisterPassword.setText("");

				if (!commonClass.checkInternetConnection()) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {
								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub
								}
							}, getActivity().getResources()
									.getText(R.string.InternetConnect)
									.toString());
					ds.show(getActivity().getFragmentManager(), "");
					ds.setCancelable(false);
				} else {

					Intent intent = new Intent(context, SignInActivity.class);
					startActivityForResult(intent, 5);
				}

			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topbar = (topBarInteface) activity;
		alreadyCLick = (alreadyRegisterInteface) activity;
		context = activity;
		// unbind = (Unbindservice) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub

		if (requestCode == 4) {

			if (resultCode == Activity.RESULT_OK) {

				userdetail = (UserDetail) data
						.getSerializableExtra("userDetail");
				if (userdetail != null) {
					userdetail.type = "Facebook";

					strMimsLoginWith = "Facebook";
					strEmailAddress = userdetail.facebookId;
					strEmailPassword = "";

					if (strEmailAddress != null) {
						alreadyLoginAsync();
					} else {
						strMimsLoginWith = "Mims";
					}
				}

			}

		} else if (requestCode == 5) {

			if (resultCode == Activity.RESULT_OK) {

				userdetail = (UserDetail) data
						.getSerializableExtra("userDetail");
				if (userdetail != null) {
					userdetail.type = "GooglePlus";
					strMimsLoginWith = "GooglePlus";
					strEmailAddress = userdetail.googleplusId;
					strEmailPassword = "";

					if (strEmailAddress != null) {
						alreadyLoginAsync();
					} else {
						strMimsLoginWith = "Mims";
					}
				}

			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void alreadyLoginAsync() {


		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topbar.showDialog("Loading");
						url_response = "";
					}

					@Override
					public void doInBackground() {

						try {

							double latitude = commonClass.currentLatitude;
							double longiutde = commonClass.currentLongitude;
							ArrayList<Double> arrLatitude = ((RegisterActivity) getActivity())
									.getLatitudeLongitude();
							latitude = arrLatitude.get(0);
							longiutde = arrLatitude.get(1);
							String getUrl = "CustomerService.svc/Login?languageid="
									+ commonClass.languageId
									+ "&loginwith="
									+ strMimsLoginWith
									+ "&id="
									+ strEmailAddress
									+ "&password="
									+ commonClass
											.getMD5EncryptedString(strEmailPassword)
									+ "&deviceid="
									+ CommonData.deviceId
									+ "&device=android&latitude="
									+ latitude
									+ "&longitude=" + longiutde;

							Log.e("Login URl=",getUrl);
							getUrl = commonClass.Url_EncodeWithBlank(getUrl);

							url_response = webService
									.makeServicegetRequest(getUrl);

							JSONObject js_obj = new JSONObject(url_response);
							String VStatusCode = js_obj
									.getString("vStatusCode");
							if (VStatusCode.equals("100")) {

								commonClass.CustomerId = js_obj
										.getString("iCustomerId");
								commonClass.languageId = js_obj
										.getString("iLanguageId");

								String emailaddress = js_obj
										.getString("vEmail");

								preference = getActivity()
										.getSharedPreferences("UserDetail",
												Context.MODE_PRIVATE);

								preference
										.edit()
										.putString(
												"latitude",
												""
														+ (js_obj
																.getDouble("dLatitude")))
										.commit();

								preference
										.edit()
										.putString(
												"longitude",
												""
														+ (js_obj
																.getDouble("dLongitude")))
										.commit();

							}

						} catch (Exception e) {
							// TODO: handle exception
						}

					}

					@Override
					public void onPostExecute() {
						if (!commonClass.checkInternetConnection()) {
							topbar.hideDialog();
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {
										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
										}
									}, getActivity().getResources()
											.getText(R.string.InternetConnect)
											.toString());
							ds.show(getActivity().getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							try {
								JSONObject js_obj = new JSONObject(url_response);
								String VStatusCode = js_obj
										.getString("vStatusCode");
								String vMessageResponse = js_obj
										.getString("vMessageResponse");

								VStatusCode = VStatusCode.trim();
								if (VStatusCode.equals("420")) {
									topbar.hideDialog();

									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													// topbar.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getActivity().getFragmentManager(),
											"");
									ds.setCancelable(false);

								}

								else if (VStatusCode.equals("100")) {

									commonClass.currentLatitude = Double.valueOf(preference
											.getString(
													"latitude",
													""
															+ commonClass.currentLatitude));
									commonClass.currentLongitude = Double.valueOf(preference
											.getString(
													"longitude",
													""
															+ commonClass.currentLongitude));


									getUserDetail();

								} else if (VStatusCode.equals("115")) {

									SyncDataAsync();

								} else if (VStatusCode.equals("102")) {
									topbar.hideDialog();

									if (strMimsLoginWith
											.equalsIgnoreCase("Facebook")
											|| strMimsLoginWith
													.equalsIgnoreCase("GooglePlus")) {
										strMimsLoginWith = "Mims";

										FragmentTransaction transaction = getFragmentManager()
												.beginTransaction();

										Fragment newFragment = new FragmentRegister();
										Bundle bundler = new Bundle();
										bundler.putSerializable("login",
												userdetail);
										newFragment.setArguments(bundler);
										String tag = newFragment.toString();
										transaction.replace(R.id.frmRegister,
												newFragment, tag);
										transaction.addToBackStack(tag);
										transaction.commit();
									} else {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tagg) {
														strMimsLoginWith = "Mims";

													}
												}, vMessageResponse);
										ds.setCancelable(false);
										ds.show(getActivity()
												.getFragmentManager(), "");
									}

								}

								else {
									topbar.hideDialog();
									if (strMimsLoginWith
											.equalsIgnoreCase("Facebook")
											|| strMimsLoginWith
													.equalsIgnoreCase("GooglePlus")) {
										if (userdetail.facebookId != null) {
											strMimsLoginWith = "Mims";

											FragmentTransaction transaction = getFragmentManager()
													.beginTransaction();

											Fragment newFragment = new FragmentRegister();
											Bundle bundler = new Bundle();
											bundler.putSerializable("login",
													userdetail);
											newFragment.setArguments(bundler);
											String tag = newFragment.toString();
											transaction.replace(
													R.id.frmRegister,
													newFragment, tag);
											transaction.addToBackStack(tag);
											transaction.commit();
										} else {
											strMimsLoginWith = "Mims";

										}
										//
										//

									} else {

										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tagg) {
														strMimsLoginWith = "Mims";

													}
												}, vMessageResponse);
										ds.setCancelable(false);
										ds.show(getActivity()
												.getFragmentManager(), "");

									}

								}
							} catch (Exception e) {
							}
						}
					}
				});
		as.execute();
	}

	private void SyncDataAsync() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						// topbar.showDialog("Loading");
						url_response = "";

					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub

						try {
							double latitude = commonClass.currentLatitude;
							double longiutde = commonClass.currentLongitude;

							ArrayList<Double> arrLatitude = ((RegisterActivity) getActivity())
									.getLatitudeLongitude();

							latitude = arrLatitude.get(0);
							longiutde = arrLatitude.get(1);
							String getUrl = "CustomerService.svc/Synchronize?languageid="
									+ commonClass.languageId
									+ "&loginwith="
									+ strMimsLoginWith
									+ "&id="
									+ strEmailAddress
									+ "&password="
									+ commonClass
											.getMD5EncryptedString(strEmailPassword)
									+ "&deviceid="
									+ commonClass.deviceId
									+ "&device=android&latitude="
									+ latitude
									+ "&longitude=" + longiutde;

							getUrl = commonClass.Url_EncodeWithBlank(getUrl);
							url_response = webService
									.makeServicegetRequest(getUrl);

						} catch (Exception e) {
							// TODO: handle exception
						}

					}

					@Override
					public void onPostExecute() {
						try {
							topbar.hideDialog();
						} catch (Exception e) {
							// TODO: handle exception
						}

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {
										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
										}
									}, getActivity().getResources()
											.getText(R.string.InternetConnect)
											.toString());
							ds.show(getActivity().getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							try {
								JSONObject js_obj = new JSONObject(url_response);
								String VStatusCode = js_obj
										.getString("vStatusCode");
								String vMessageResponse = js_obj
										.getString("vMessageResponse");
								// commonClass.toastDisplay(VStatusCode);

								if (VStatusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topbar.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getActivity().getFragmentManager(),
											"");
									ds.setCancelable(false);

								} else if (VStatusCode.equals("100")) {
									commonClass.CustomerId = js_obj
											.getString("iCustomerId");
									commonClass.languageId = js_obj
											.getString("iLanguageId");

									getUserDetail();
								} else {
									// DialogFragment ds = new
									// SingleButtonAlert(
									// new DialogInterfaceClick() {
									//
									// @Override
									// public void dialogClick(String tag) {
									//
									// }
									// }, vMessageResponse);
									// ds.setCancelable(false);
									// ds.show(getFragmentManager(), "");
								}
							} catch (Exception e) {
								// Toast.makeText(getActivity(),
								// "exception" + e.getMessage().toString(),
								// Toast.LENGTH_LONG).show();
							}
						}
					}
				});
		as.execute();
	}

	private void forgotAsync(final String emailAddress) {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topbar.showDialog("Loading");
						url_response = "";
					}

					@Override
					public void doInBackground() {

						try {
							String getUrl = "CustomerService.svc/ForgotPassword?languageid="
									+ commonClass.languageId
									+ "&email="
									+ emailAddress;
							getUrl = commonClass.Url_EncodeWithBlank(getUrl);
							url_response = webService
									.makeServicegetRequest(getUrl);
						} catch (Exception e) {
							// TODO: handle exception
						}

					}

					@Override
					public void onPostExecute() {

						try {
							topbar.hideDialog();
						} catch (Exception e) {
							// TODO: handle exception
						}

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {
										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
										}
									}, getActivity().getResources()
											.getText(R.string.InternetConnect)
											.toString());
							ds.show(getActivity().getFragmentManager(), "");
							ds.setCancelable(false);
						} else {

							JSONObject js_obj;
							try {
								js_obj = new JSONObject(url_response);
								String VStatusCode = js_obj
										.getString("vStatusCode");
								String vMessageResponse = js_obj
										.getString("vMessageResponse");

								if (VStatusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													// topbar.topbarClick("back");
												}
											},
											getActivity()
													.getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getActivity().getFragmentManager(),
											"");
									ds.setCancelable(false);

								} else {

									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

												}
											}, vMessageResponse);
									ds.setCancelable(false);
									ds.show(getActivity().getFragmentManager(),
											"");
								}

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});
		as.execute();
	}

	private void getUserDetail() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						preference = getActivity().getSharedPreferences(
								"UserDetail", Context.MODE_PRIVATE);
						strStatusCode = "";
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						try {
							String geturl = "CustomerService.svc/GetProfile?customerid="
									+ commonClass.CustomerId
									+ "&languageid="
									+ commonClass.languageId;
							geturl = commonClass.Url_EncodeWithBlank(geturl);
							url_response = webService
									.makeServicegetRequest(geturl);
							JSONObject js_obj_getprofileresult = new JSONObject(
									url_response);
							strStatusCode = js_obj_getprofileresult
									.getString("vStatusCode");

							if (strStatusCode.equals("100")) {
								commonClass.ProfiileImagePath = js_obj_getprofileresult
										.getString("vProfileImageName");

								SharedPreferences sp = getActivity()
										.getSharedPreferences("UserDetail",
												Context.MODE_PRIVATE);

								sp.edit()
										.putString(
												"emailid",
												""
														+ js_obj_getprofileresult
																.getString("vEmail"))
										.commit();
								sp.edit()
										.putString(
												"profileimagepath",
												js_obj_getprofileresult
														.getString("vProfileImageName"))
										.commit();

								commonClass.ProfileName = js_obj_getprofileresult
										.getString("vCustomerName");

								js_obj_getprofileresult
										.getString("vCustomerName");
								commonClass.strUsername = js_obj_getprofileresult
										.getString("vUserName");
								commonClass.strPassword = preference.getString(
										"password", "");
							}
						} catch (Exception e) {

						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						try {
							topbar.hideDialog();
						} catch (Exception e) {
							// TODO: handle exception
						}

						if (commonClass.checkInternetConnection()) {

							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {
											@Override
											public void dialogClick(String tag) {
												// topbar.topbarClick("back");
											}
										},
										getActivity().getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getActivity().getFragmentManager(), "");
								ds.setCancelable(false);
							} else if (strStatusCode.equals("100")) {
								// preference =
								// getActivity().getSharedPreferences(
								// "UserDetail", Context.MODE_PRIVATE);
								Editor editior = preference.edit();
								editior.putString("customerid",
										commonClass.CustomerId);
								editior.putBoolean("firsttime", false);
								editior.putString(
										"password",
										commonClass
												.getMD5EncryptedString(strEmailPassword));

								editior.putString("languageid",
										commonClass.languageId);
								editior.putString("username",
										commonClass.strUsername);
								editior.putString("customername",
										commonClass.ProfileName);
								editior.commit();
								alreadyCLick.alreadyRegisterClick("register");
							}
						}
					}
				});
		as.execute();
	}

	public boolean checkLocationService() {
		LocationManager lm = null;
		boolean gps_enabled = false, network_enabled = false;
		if (lm == null)
			lm = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);
		try {
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}

		if (!gps_enabled && !network_enabled) {
			return false;

		}
		return true;
	}

}