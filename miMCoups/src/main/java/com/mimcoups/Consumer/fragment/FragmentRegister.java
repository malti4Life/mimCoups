package com.mimcoups.Consumer.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.Session;
import com.mimcoups.Consumer.DialogFragment.CameraDialogFragment;
import com.mimcoups.Consumer.DialogFragment.CurrencyDailogFragment;
import com.mimcoups.Consumer.DialogFragment.DatePickerFragment;
import com.mimcoups.Consumer.Facebook.FacebookLoginActivity;
import com.mimcoups.Consumer.customClass.DateCustomeClass;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.R;
import com.mimcoups.Consumer.RegisterActivity;
import com.mimcoups.Consumer.SignInActivity;
import com.mimcoups.Consumer.DialogFragment.FriendsDialogFragment;
import com.mimcoups.Consumer.DialogFragment.InvitationDialogFragment;
import com.mimcoups.Consumer.DialogFragment.SelectionButtonAlert;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.Age;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.customClass.UserDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.alreadyRegisterInteface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.inteface.wrongdatedialoginterface;
import com.squareup.picasso.Picasso;

public class FragmentRegister extends Fragment {

	long MIN_CLICK_INTERVAL = 800;
	long lastClickTime = 0;
	private ArrayList<FriendsDetail> arr_friends;
	private TextView txtRegisetrBirthDate, txtRegisterSelectLanguage,
			txtRegisterCurrency;

	private ImageView imgAlreadyRegisterBack;
	private EditText edtRegisterName, edtRegisterZipCode,
			edtRegisterEmailAddress, edtRegisterUserName, edtRegisterPassword,
			edtRegisterconfirmPassword;
	private LinearLayout lnRegisterMale, lnRegisterFemale,
			llRegisterChangeLanguage, lnRegisterSelectCurrency,
			lnRegisterFacebook, lnRegisterGooglePlus, lnRegisterButton,
			llRegisterBirthDate, lnRegisterAlready;
	private alreadyRegisterInteface alredyClick;
	private ImageView imgRegisterCamera;
	Locale myLocale;
	private ToggleButton tglRegisterNotification;
	private topBarInteface topInterface;
	LinearLayout lnRegisterCamera;
	private CommonData commonClass;
	private String url_response = "";
	private String strGender = "Male";
	private WebService_Call webServiceCall = new WebService_Call();
	private String isNotification = "true";
	private String strFromRegister = "MIMS";
	private String strGooglePlusId = "";
	private String strFacebookId = "";
	private Drawable drUserProfile;
	private Context context;
	private File FileImage;
	private UserDetail userdetail;
	private SharedPreferences preference;
	String invitationid = "";

	String friendscustomerid = "";
	private TextView txtLabelZipCode, txtRegister, txtGender, txtMale,
			txtFemale, tvusername, tvuseremail, tvmainusername, tvpassword,
			txtRegisterNotification, txtloginFacebook, txtGooglePlus, txtLogin,
			txtAlredyRegister, tvSelectlanguage, txtconfirmpass;
	private TextView tvbirthdate, txtRegisterCurrency_show;
	RelativeLayout relmainfragment_register;
	String strEmailAddress = "", strEmailPassword = "",
			strMimsLoginWith = "Mims";
	String strStatusCode = "";
	private WebService_Call webService = new WebService_Call();
	boolean showlocationinformdialog = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		View view = inflater.inflate(R.layout.fragment_register, container,
				false);

		initControls(view);
		OnClickEvents(view);
		return view;
	}

	public static void callFacebookLogout(Context context) {
		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

		}

	}

	private void initControls(View view) {
		imgAlreadyRegisterBack = (ImageView) view
				.findViewById(R.id.imgAlreadyRegisterBack);

		relmainfragment_register = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_register);

		tvbirthdate = (TextView) view.findViewById(R.id.tvbirthdate);
		txtRegisterCurrency_show = (TextView) view
				.findViewById(R.id.txtRegisterCurrency_show);
		txtRegisterSelectLanguage = (TextView) view
				.findViewById(R.id.txtRegisterSelectLanguage);
		txtRegisterCurrency = (TextView) view
				.findViewById(R.id.txtRegisterCurrency);
		commonClass = (CommonData) getActivity().getApplicationContext();
		edtRegisterZipCode = (EditText) view
				.findViewById(R.id.edtRegisterZipCode);
		edtRegisterName = (EditText) view.findViewById(R.id.edtRegisterName);
		edtRegisterEmailAddress = (EditText) view
				.findViewById(R.id.edtRegisterEmailAddress);
		edtRegisterUserName = (EditText) view
				.findViewById(R.id.edtRegisterUserName);
		edtRegisterPassword = (EditText) view
				.findViewById(R.id.edtRegisterPassword);
		edtRegisterconfirmPassword = (EditText) view
				.findViewById(R.id.edtRegisterconfirmPassword);
		lnRegisterCamera = (LinearLayout) view
				.findViewById(R.id.lnRegisterCamera);
		txtconfirmpass = (TextView) view.findViewById(R.id.txtconfirmpass);

		// textview
		txtRegisetrBirthDate = (TextView) view
				.findViewById(R.id.txtRegisetrBirthDate);

		// linearlayout
		llRegisterBirthDate = (LinearLayout) view
				.findViewById(R.id.llRegisterBirthDate);
		lnRegisterMale = (LinearLayout) view.findViewById(R.id.lnRegisterMale);
		lnRegisterFemale = (LinearLayout) view
				.findViewById(R.id.lnRegisterFemale);

		lnRegisterSelectCurrency = (LinearLayout) view
				.findViewById(R.id.lnRegisterSelectCurrency);
		llRegisterChangeLanguage = (LinearLayout) view
				.findViewById(R.id.llRegisterChangeLanguage);
		lnRegisterFacebook = (LinearLayout) view
				.findViewById(R.id.lnRegisterFacebook);
		lnRegisterGooglePlus = (LinearLayout) view
				.findViewById(R.id.lnRegisterGooglePlus);
		lnRegisterButton = (LinearLayout) view
				.findViewById(R.id.lnRegisterButton);
		lnRegisterAlready = (LinearLayout) view
				.findViewById(R.id.lnRegisterAlready);

		// imageview
		imgRegisterCamera = (ImageView) view
				.findViewById(R.id.imgRegisterCamera);

		// toggle
		tglRegisterNotification = (ToggleButton) view
				.findViewById(R.id.tglRegisterNotification);

		String dir = Environment.getExternalStorageDirectory() + File.separator
				+ "." + getResources().getString(R.string.app_name);

		FileImage = new File(dir, "1.jpg");

		txtLabelZipCode = (TextView) view.findViewById(R.id.txtLabelZipCode);
		txtRegister = (TextView) view.findViewById(R.id.txtRegister);
		txtGender = (TextView) view.findViewById(R.id.txtGender);
		txtMale = (TextView) view.findViewById(R.id.txtMale);
		txtFemale = (TextView) view.findViewById(R.id.txtFemale);
		tvusername = (TextView) view.findViewById(R.id.tvusername);
		tvuseremail = (TextView) view.findViewById(R.id.tvuseremail);
		tvmainusername = (TextView) view.findViewById(R.id.tvmainusername);
		tvpassword = (TextView) view.findViewById(R.id.tvpassword);
		txtRegisterNotification = (TextView) view
				.findViewById(R.id.txtRegisterNotification);
		txtloginFacebook = (TextView) view.findViewById(R.id.txtloginFacebook);
		txtGooglePlus = (TextView) view.findViewById(R.id.txtGooglePlus);
		txtLogin = (TextView) view.findViewById(R.id.txtLogin);
		txtAlredyRegister = (TextView) view
				.findViewById(R.id.txtAlredyRegister);
		tvSelectlanguage = (TextView) view.findViewById(R.id.tvSelectlanguage);

		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		showlocationinformdialog = false;

		// View views = getActivity().getCurrentFocus();
		// if (views != null) {
		// InputMethodManager inputManager = (InputMethodManager) getActivity()
		// .getSystemService(Context.INPUT_METHOD_SERVICE);
		// inputManager.hideSoftInputFromWindow(views.getWindowToken(),
		// InputMethodManager.HIDE_NOT_ALWAYS);
		// }

		Bundle bundle = getArguments();
		if (bundle != null) {
			userdetail = (UserDetail) bundle.getSerializable("login");
			if (userdetail.type.equalsIgnoreCase("Facebook")) {
				setFacebookData(4);
			} else if (userdetail.type.equalsIgnoreCase("GooglePlus")) {
				setFacebookData(5);

			}
		}

	}

	public void OnClickEvents(View view) {

		imgAlreadyRegisterBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				topInterface.topbarClick("back");
			}
		});

		relmainfragment_register.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		llRegisterBirthDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				final Calendar c = Calendar.getInstance();
				int year = c.get(Calendar.YEAR);
				int month = c.get(Calendar.MONTH) + 1;
				int day = c.get(Calendar.DAY_OF_MONTH);
				Log.d("text value",
						"" + c.get(Calendar.YEAR) + "=" + c.get(Calendar.MONTH)
								+ "==" + c.get(Calendar.DAY_OF_MONTH));

				Log.d("text value", ""
						+ txtRegisetrBirthDate.getText().toString().trim());

				if (txtRegisetrBirthDate.getText().toString().trim().length() != 0) {
					Log.d("if", "1");
					if (txtRegisetrBirthDate.getText().toString().contains("/")) {
						Log.d("if", "2");
						String split[] = txtRegisetrBirthDate.getText()
								.toString().split("/");
						year = Integer.parseInt(split[2]);
						month = Integer.parseInt(split[1]);
						day = Integer.parseInt(split[0]);
					} else {
						Log.d("if", "3");
						String split[] = txtRegisetrBirthDate.getText()
								.toString().split("\\.");
						Log.d("split size", "" + split.length);
						year = Integer.parseInt(split[2]);
						month = Integer.parseInt(split[1]);
						day = Integer.parseInt(split[0]);

						Log.d("year", "" + year);
						Log.d("month", "" + month);
						Log.d("day", "" + day);
					}

				} else {
					year = year - 13;
				}

				Log.d("year", "" + year);
				Log.d("month", "" + month);
				Log.d("day", "" + day);

				DialogFragment newFragment = new DatePickerFragment(
						new wrongdatedialoginterface() {

							@Override
							public void selecteddateiswrongedialog(
									String message) {
								// TODO Auto-generated method stub
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

											}
										}, message);
								ds.show(getFragmentManager(), "");

							}

							@SuppressWarnings("deprecation")
							@Override
							public void ondateselectedevent(
									DateCustomeClass dateclass) {
								// TODO Auto-generated method stub

								String selected_date = convert_proper_date(dateclass.month)
										+ "/"
										+ dateclass.date
										+ "/"
										+ convert_proper_date(dateclass.year);

								Calendar now = Calendar.getInstance();
								Calendar dob = Calendar.getInstance();
								dob.setTime(new Date(selected_date));

								int year1 = now.get(Calendar.YEAR);
								int year2 = dob.get(Calendar.YEAR);
								int age = year1 - year2;
								int month1 = (now.get(Calendar.MONTH));

								int month2 = (dob.get(Calendar.MONTH));

								if (month2 > month1) {
									age--;
								} else if (month1 == month2) {
									int day1 = now.get(Calendar.DAY_OF_MONTH);
									int day2 = dob.get(Calendar.DAY_OF_MONTH);
									// Log.e("Day 1", "" + day1);
									// Log.e("Day 2", "" + day2);
									if (day2 > day1) {
										age--;
									}
								}

								// Age age = calculateAge(new
								// Date(selected_date));

								if (age < 13) {

									txtRegisetrBirthDate.setText("");
									Toast.makeText(
											getActivity(),
											context.getResources().getString(
													R.string.agelimit), 3000)
											.show();
									;

								}

							}
						}, getActivity(), txtRegisetrBirthDate,
						commonClass.languageId, year, month, day);
				newFragment.show(getFragmentManager(), "datePicker");
			}
		});
		imgRegisterCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogFragment ds = new CameraDialogFragment(
						new DialogInterfaceClick() {
							@Override
							public void dialogClick(String tag) {
								if (tag.equals("Camera")) {
									Intent cameraIntent = new Intent(
											android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
									startActivityForResult(cameraIntent, 0);
								} else {
									Intent intent = new Intent();
									intent.setType("image/*");
									intent.setAction(Intent.ACTION_GET_CONTENT);
									startActivityForResult(Intent
											.createChooser(intent,
													"Choose Profile Pic"), 1);
								}
							}
						});
				ds.show(getFragmentManager(), "");
			}
		});
		lnRegisterMale.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				strGender = "Male";
				lnRegisterMale.setBackgroundColor(Color.parseColor("#72B600"));
				lnRegisterFemale.setBackgroundColor(Color.parseColor("#1068AF"));
			}
		});
		lnRegisterFemale.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				strGender = "Female";
				lnRegisterFemale.setBackgroundColor(Color.parseColor("#72B600"));
				lnRegisterMale.setBackgroundColor(Color.parseColor("#1068AF"));
			}
		});
		llRegisterChangeLanguage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated me
				DialogFragment ds = new CurrencyDailogFragment(
						new DialogInterfaceClick() {

							@Override
							public void dialogClick(String tag) {
								if (tag.equals("en")) {
									if (commonClass.languageId.equals("2")) {
										commonClass.languageId = "1";
										changeLanguage(tag);
									}
									txtRegisterSelectLanguage
											.setText("Australia");
									txtRegisterCurrency
											.setText("Australian Dollar(AUD)");
								} else {
									if (commonClass.languageId.equals("1")) {
										commonClass.languageId = "2";
										changeLanguage(tag);
									}
									txtRegisterSelectLanguage
											.setText("Norway");
									txtRegisterCurrency
											.setText("Norwegian Kron(NOK)");
								}
							}
						}, commonClass.languageId);
				ds.show(getFragmentManager(), "");
			}
		});
		lnRegisterFacebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (!commonClass.checkInternetConnection()) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {
									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.InternetConnect)
										.toString());
						ds.show(getFragmentManager(), "");
						ds.setCancelable(false);
					} else {
						userdetail = null;

						callFacebookLogout(getActivity());

						Intent intent = new Intent(context,
								FacebookLoginActivity.class);
						startActivityForResult(intent, 4);
					}
				}

			}
		});

		lnRegisterGooglePlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (!commonClass.checkInternetConnection()) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {
									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.InternetConnect)
										.toString());
						ds.show(getFragmentManager(), "");
						ds.setCancelable(false);
					} else {
						userdetail = null;
						Intent intent = new Intent(context,
								SignInActivity.class);
						intent.addFlags(0);
						startActivityForResult(intent, 5);
					}
				}

			}
		});

		lnRegisterButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					if (edtRegisterZipCode.getText().toString().trim().length() == 0) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.addWishlistZipCode)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					}

					else if (edtRegisterZipCode.getText().toString().trim()
							.length() < 4) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.zipvalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					}

					else if (txtRegisetrBirthDate.getText().toString().trim()
							.equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.birthdatevalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					} else if (edtRegisterName.getText().toString().trim()
							.equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {
									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.namevalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					}

					else if (edtRegisterEmailAddress.getText().toString()
							.trim().equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.emailvalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					} else if (!Patterns.EMAIL_ADDRESS
							.matcher(
									edtRegisterEmailAddress.getText()
											.toString().trim()).matches()) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.validemailvalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					} else if (edtRegisterUserName.getText().toString().trim()
							.equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.usernamevalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					} else if (edtRegisterUserName.getText().toString()
							.contains(" ")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								},
								getActivity()
										.getResources()
										.getText(
												R.string.usernamespacevalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					} else if (edtRegisterUserName.getText().toString().trim()
							.length() < 3) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.username_validation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					}

					else if (edtRegisterPassword.getText().toString().trim()
							.equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.passvalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					} else if (edtRegisterPassword.getText().length() < 6
							|| edtRegisterPassword.getText().length() > 30) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								},
								getActivity()
										.getResources()
										.getText(
												R.string.passwordlengthvalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					}

					else if (edtRegisterconfirmPassword.getText().toString()
							.trim().equals("")) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.conformpass)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					} else if ((!edtRegisterconfirmPassword
							.getText()
							.toString()
							.trim()
							.equals(edtRegisterPassword.getText().toString()
									.trim()))) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								},
								getActivity()
										.getResources()
										.getText(R.string.Confirmpassvalidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");
					} else if (txtRegisterSelectLanguage.getText().toString()
							.trim().length() == 0) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.languageValidation)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					} else if (!commonClass.checkInternetConnection()) {
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {
									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub
									}
								}, getActivity().getResources()
										.getText(R.string.InternetConnect)
										.toString());
						ds.setCancelable(false);
						ds.show(getFragmentManager(), "");

					} else {

						View view = getActivity().getCurrentFocus();
						if (view != null) {
							InputMethodManager inputManager = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							inputManager.hideSoftInputFromWindow(
									view.getWindowToken(),
									InputMethodManager.HIDE_NOT_ALWAYS);
						}
						drUserProfile = imgRegisterCamera.getDrawable();
						if (drUserProfile == getResources().getDrawable(
								R.drawable.icon_camera)) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {
										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
										}
									}, getActivity().getResources()
											.getText(R.string.UserCaptureImage)
											.toString());
							ds.setCancelable(false);
							ds.show(getFragmentManager(), "");
						} else {
							final async_deel_feed_list as = new async_deel_feed_list(
									new asynctaskloaddata() {

										@Override
										public void onPreExecute() {
											topInterface.showDialog("Loading");
										}

										@Override
										public void doInBackground() {
											// TODO Auto-generated method stub
											try {
												
												if(commonClass.currentLatitude == 0 || commonClass.currentLongitude==0 ){
													
													if (commonClass.languageId.equals("1")) {

														commonClass.currentLatitude = -33.7969235;
														commonClass.currentLongitude = 150.9224326;

														
													} else {

														commonClass.currentLatitude = 59.8938549;
														commonClass.currentLongitude = 10.7851165;

													
													}
													
												}
												
												
												double latitude = commonClass.currentLatitude;
												double longiutde = commonClass.currentLongitude;

												ArrayList<Double> arrLatitude = ((RegisterActivity) getActivity())
														.getLatitudeLongitude();

												latitude = arrLatitude.get(0);
												longiutde = arrLatitude.get(1);

												String getUrl = "CustomerService.svc/AddCustomer?languageid="
														+ commonClass.languageId
														+ "&customername="
														+ edtRegisterName
																.getText()
																.toString()
																.trim()
														+ "&birthdate="
														+ DatePickerFragment.text_to_upload
														+ "&gender="
														+ strGender
														+ "&zipcode="
														+ edtRegisterZipCode
																.getText()
																.toString()
																.trim()
														+ "&email="
														+ edtRegisterEmailAddress
																.getText()
																.toString()
																.trim()
														+ "&username="
														+ edtRegisterUserName
																.getText()
																.toString()
																.trim()
														+ "&password="
														+ commonClass
																.getMD5EncryptedString(edtRegisterPassword
																		.getText()
																		.toString()
																		.trim())
														+ "&deviceid="
														+ CommonData.deviceId
														+ "&device=android&latitude="
														+ latitude
														+ "&longitude="
														+ longiutde
														+ "&registeredfrom="
														+ strFromRegister
														+ "&sendnotification="
														+ isNotification
														+ "&facebookid="
														+ strFacebookId
														+ "&googleplusid="
														+ strGooglePlusId;

												getUrl = commonClass
														.Url_EncodeWithBlank(getUrl);
												 Log.e("Register", "" +
												 getUrl);

												if(FileImage.exists()){

													url_response = webServiceCall
															.makeServiceImageUpload(
																	getUrl,
																	FileImage, true);

												}else{

													url_response = webServiceCall
															.makeServiceImageUpload(
																	getUrl,
																	FileImage, false);

												}

												 Log.e("Register Response", ""
												 + url_response);

											} catch (Exception e) {
												e.printStackTrace();
											}
										}

										@Override
										public void onPostExecute() {
											topInterface.hideDialog();

											getActivity()
													.getWindow()
													.setSoftInputMode(
															WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
											
											if(!url_response.contains("vStatusCode")){

												DialogFragment ds = new SingleButtonAlert(
														new DialogInterfaceClick() {

															@Override
															public void dialogClick(
																	String tag) {

																
															}
														},
														getResources()
																.getString(
																		R.string.PoorInternetConnect));
												ds.show(getFragmentManager(),
														"");
												ds.setCancelable(false);

											
											}

											try {
												JSONObject js_obj = new JSONObject(
														url_response);
												String VStatusCode = js_obj
														.getString("vStatusCode");
												String vMessageResponse = js_obj
														.getString("vMessageResponse");
												
												

												if (VStatusCode.equals("420")) {
													DialogFragment ds = new SingleButtonAlert(
															new DialogInterfaceClick() {

																@Override
																public void dialogClick(
																		String tag) {

																	topInterface
																			.topbarClick("back");
																}
															},
															getResources()
																	.getString(
																			R.string.PoorInternetConnect));
													ds.show(getFragmentManager(),
															"");
													ds.setCancelable(false);

												} else if (VStatusCode
														.equals("100")) {

													commonClass.CustomerId = js_obj
															.getString("iCustomerId");

													commonClass.ProfiileImagePath = js_obj
															.getString("vProfileImageName");

													// Log.e("User name", ""
													// + edtRegisterName
													// .getText()
													// .toString()
													// .trim());

													SharedPreferences sp = getActivity()
															.getSharedPreferences(
																	"UserDetail",
																	Context.MODE_PRIVATE);
													sp.edit()
															.putString(
																	"profileimagepath",
																	js_obj.getString("vProfileImageName"))
															.commit();

													sp.edit()
															.putString(
																	"customername",
																	edtRegisterName
																			.getText()
																			.toString()
																			.trim())
															.commit();

													JSONArray arrSuggestfriend = js_obj
															.getJSONArray("SuggestFriends");

													arr_friends = new ArrayList<FriendsDetail>();

													for (int j = 0; j < arrSuggestfriend
															.length(); j++) {

														FriendsDetail fs = new FriendsDetail();

														JSONObject obj = arrSuggestfriend
																.getJSONObject(j);
														String customerid = obj
																.getString("iCustomerId");
														String vCustomerName = obj
																.getString("vCustomerName");

														fs.freindsName = vCustomerName;
														fs.friendsId = customerid;

														arr_friends.add(fs);

													}

													preference = getActivity()
															.getSharedPreferences(
																	"UserDetail",
																	Context.MODE_PRIVATE);
													Editor editor = preference
															.edit();
													editor.putString(
															"customerid",
															commonClass.CustomerId);

													editor.putString("emailid",
															edtRegisterEmailAddress
																	.getText()
																	.toString()
																	.trim());

													editor.putString(
															"languageid",
															commonClass.languageId);
													editor.putString(
															"password",
															commonClass
																	.getMD5EncryptedString(edtRegisterPassword
																			.getText()
																			.toString()
																			.trim()));
													editor.putString(
															"username",
															edtRegisterUserName
																	.getText()
																	.toString()
																	.trim());
													editor.putBoolean(
															"firsttime", false);
													editor.commit();

													if (arr_friends.size() == 0) {

														alredyClick
																.alreadyRegisterClick("register");

													} else {

														invitationid = "";

														friendscustomerid = "";

														DialogFragment ds = new InvitationDialogFragment(
																new DialogInterfaceClick() {

																	@Override
																	public void dialogClick(
																			String tag) {

																		if (tag.equalsIgnoreCase("Cancel")) {
																			alredyClick
																					.alreadyRegisterClick("register");

																		} else {
																			invitationid = tag;
																			for (int j = 0; j < arr_friends
																					.size(); j++) {

																				FriendsDetail fj = arr_friends
																						.get(j);
																				if (fj.friendsId
																						.equals(tag)) {

																					arr_friends
																							.remove(fj);
																				}
																			}
																			DialogFragment df = new FriendsDialogFragment(
																					new DialogInterfaceClick() {

																						@Override
																						public void dialogClick(
																								String tag) {
																							// TODO
																							// Auto-generated
																							// method
																							// stub
																							if (tag.equalsIgnoreCase("Cancel")) {

																								async_deel_feed_list ap = new async_deel_feed_list(
																										new asynctaskloaddata() {

																											@Override
																											public void onPreExecute() {
																												// TODO
																												// Auto-generated
																												// method
																												// stub
																												topInterface
																														.showDialog("Loading");
																											}

																											@Override
																											public void doInBackground() {
																												// TODO
																												// Auto-generated
																												// method
																												// stub

																												WebService_Call we = new WebService_Call();
																												try {
																													String getUrl = "FriendService.svc/AcceptInvitationANDAddFriends";
																													getUrl = commonClass
																															.Url_EncodeWithBlank(getUrl);
																													url_response = we
																															.makeFriendAcceptService(
																																	getUrl,
																																	commonClass.languageId,
																																	commonClass.CustomerId,
																																	invitationid,
																																	friendscustomerid);

																													// Log.e("url_response",
																													// "url_response"
																													// +
																													// url_response);
																												} catch (Exception e) {

																												}
																											}

																											@Override
																											public void onPostExecute() {
																												// TODO
																												// Auto-generated
																												// method
																												// stub
																												topInterface
																														.hideDialog();

																												try {
																													JSONObject js_obj = new JSONObject(
																															url_response);
																													String VStatusCode = js_obj
																															.getString("vStatusCode");
																													String vMessageResponse = js_obj
																															.getString("vMessageResponse");

																													if (VStatusCode
																															.equals("420")) {
																														DialogFragment ds = new SingleButtonAlert(
																																new DialogInterfaceClick() {

																																	@Override
																																	public void dialogClick(
																																			String tag) {

																																		topInterface
																																				.topbarClick("back");
																																	}
																																},
																																getResources()
																																		.getString(
																																				R.string.PoorInternetConnect));
																														ds.show(getFragmentManager(),
																																"");
																														ds.setCancelable(false);

																													}

																													else if (VStatusCode
																															.equals("100")) {
																														Toast.makeText(
																																getActivity(),
																																""
																																		+ vMessageResponse,
																																9000)
																																.show();
																														alredyClick
																																.alreadyRegisterClick("register");

																													} else {
																														DialogFragment ds = new SingleButtonAlert(
																																new DialogInterfaceClick() {
																																	@Override
																																	public void dialogClick(
																																			String tag) {
																																	}
																																},
																																vMessageResponse);
																														ds.show(getFragmentManager(),
																																"");
																													}
																												} catch (Exception e) {

																												}

																											}
																										});
																								ap.execute();

																							} else {
																								friendscustomerid = tag;

																								friendscustomerid = friendscustomerid
																										.replace(
																												"[",
																												"");
																								friendscustomerid = friendscustomerid
																										.replace(
																												"]",
																												"");

																								async_deel_feed_list ap = new async_deel_feed_list(
																										new asynctaskloaddata() {

																											@Override
																											public void onPreExecute() {
																												// TODO
																												// Auto-generated
																												// method
																												// stub
																												topInterface
																														.showDialog("Loading");
																											}

																											@Override
																											public void doInBackground() {
																												// TODO
																												// Auto-generated
																												// method
																												// stub

																												WebService_Call we = new WebService_Call();
																												try {
																													String getUrl = "FriendService.svc/AcceptInvitationANDAddFriends";
																													getUrl = commonClass
																															.Url_EncodeWithBlank(getUrl);
																													url_response = we
																															.makeFriendAcceptService(
																																	getUrl,
																																	commonClass.languageId,
																																	commonClass.CustomerId,
																																	invitationid,
																																	friendscustomerid);

																													// Log.e("url_response",
																													// "url_response"
																													// +
																													// url_response);
																												} catch (Exception e) {

																												}
																											}

																											@Override
																											public void onPostExecute() {
																												// TODO
																												// Auto-generated
																												// method
																												// stub
																												topInterface
																														.hideDialog();

																												try {
																													JSONObject js_obj = new JSONObject(
																															url_response);
																													String VStatusCode = js_obj
																															.getString("vStatusCode");
																													String vMessageResponse = js_obj
																															.getString("vMessageResponse");

																													if (VStatusCode
																															.equals("420")) {
																														DialogFragment ds = new SingleButtonAlert(
																																new DialogInterfaceClick() {

																																	@Override
																																	public void dialogClick(
																																			String tag) {

																																		topInterface
																																				.topbarClick("back");
																																	}
																																},
																																getResources()
																																		.getString(
																																				R.string.PoorInternetConnect));
																														ds.show(getFragmentManager(),
																																"");
																														ds.setCancelable(false);

																													}

																													else if (VStatusCode
																															.equals("100")) {
																														Toast.makeText(
																																getActivity(),
																																""
																																		+ vMessageResponse,
																																9000)
																																.show();
																														alredyClick
																																.alreadyRegisterClick("register");

																													} else {
																														DialogFragment ds = new SingleButtonAlert(
																																new DialogInterfaceClick() {
																																	@Override
																																	public void dialogClick(
																																			String tag) {
																																	}
																																},
																																vMessageResponse);
																														ds.show(getFragmentManager(),
																																"");
																													}
																												} catch (Exception e) {

																												}

																											}
																										});
																								ap.execute();

																							}

																						}
																					},
																					commonClass.languageId,
																					arr_friends);

																			df.setCancelable(false);

																			if ((arr_friends
																					.size() > 0)) {
																				df.show(getFragmentManager(),
																						"");
																			} else {

																				async_deel_feed_list ap = new async_deel_feed_list(
																						new asynctaskloaddata() {

																							@Override
																							public void onPreExecute() {
																								// TODO
																								// Auto-generated
																								// method
																								// stub
																								topInterface
																										.showDialog("Loading");
																							}

																							@Override
																							public void doInBackground() {
																								// TODO
																								// Auto-generated
																								// method
																								// stub

																								WebService_Call we = new WebService_Call();
																								try {
																									String getUrl = "FriendService.svc/AcceptInvitationANDAddFriends";
																									getUrl = commonClass
																											.Url_EncodeWithBlank(getUrl);
																									url_response = we
																											.makeFriendAcceptService(
																													getUrl,
																													commonClass.languageId,
																													commonClass.CustomerId,
																													invitationid,
																													"");
																								} catch (Exception e) {

																								}
																							}

																							@Override
																							public void onPostExecute() {
																								// TODO
																								// Auto-generated
																								// method
																								// stub
																								topInterface
																										.hideDialog();

																								try {
																									JSONObject js_obj = new JSONObject(
																											url_response);
																									String VStatusCode = js_obj
																											.getString("vStatusCode");
																									String vMessageResponse = js_obj
																											.getString("vMessageResponse");

																									if (VStatusCode
																											.equals("420")) {
																										DialogFragment ds = new SingleButtonAlert(
																												new DialogInterfaceClick() {

																													@Override
																													public void dialogClick(
																															String tag) {

																														topInterface
																																.topbarClick("back");
																													}
																												},
																												getResources()
																														.getString(
																																R.string.PoorInternetConnect));
																										ds.show(getFragmentManager(),
																												"");
																										ds.setCancelable(false);

																									}

																									else if (VStatusCode
																											.equals("100")) {
																										Toast.makeText(
																												getActivity(),
																												""
																														+ vMessageResponse,
																												9000)
																												.show();
																										alredyClick
																												.alreadyRegisterClick("register");

																									} else {
																										DialogFragment ds = new SingleButtonAlert(
																												new DialogInterfaceClick() {
																													@Override
																													public void dialogClick(
																															String tag) {
																													}
																												},
																												vMessageResponse);
																										ds.show(getFragmentManager(),
																												"");
																									}
																								} catch (Exception e) {

																								}

																							}
																						});
																				ap.execute();

																			}
																		}

																	}
																},
																commonClass.languageId,
																arr_friends);
														ds.show(getFragmentManager(),
																"");
													}
												} else {
													DialogFragment ds = new SingleButtonAlert(
															new DialogInterfaceClick() {

																@Override
																public void dialogClick(
																		String tag) {

																}
															}, vMessageResponse);

													ds.setCancelable(false);
													ds.show(getFragmentManager(),
															"");
												}
											} catch (Exception e) {
												// Toast.makeText(
												// getActivity(),
												// "exception"
												// + e.getMessage()
												// .toString(),
												// Toast.LENGTH_LONG).show();
											}
										}
									});
							if (!checkLocationService()) {

								if (showlocationinformdialog) {
									as.execute();
								} else {
									showlocationinformdialog = true;
									String locationmessage = "";

									SharedPreferences sp = getActivity()
											.getSharedPreferences("UserDetail",
													Context.MODE_PRIVATE);

									if (commonClass.languageId.equals("1")) {

										commonClass.currentLatitude = -33.7969235;
										commonClass.currentLongitude = 150.9224326;

										locationmessage = getResources()
												.getString(
														R.string.DefaultLocationMessagesydney);
									} else {

										commonClass.currentLatitude = 59.8938549;
										commonClass.currentLongitude = 10.7851165;

										locationmessage = getResources()
												.getString(
														R.string.DefaultLocationMessageoslo);
									}

									sp.edit()
											.putString(
													"latitude",
													""
															+ commonClass.currentLatitude)
											.commit();
									sp.edit()
											.putString(
													"longitude",
													""
															+ commonClass.currentLongitude)
											.commit();

									Log.d("location service",
											"location service");

									DialogFragment dsp = new SelectionButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													// TODO Auto-generated
													// method
													// stub

													Intent myIntent = new Intent(
															Settings.ACTION_LOCATION_SOURCE_SETTINGS);
													context.startActivity(myIntent);

												}
											}, locationmessage);
									dsp.show(getFragmentManager(), "");

								}

							} else {
								as.execute();
							}
						}
					}
				}
			}
		});
		lnRegisterAlready.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				alredyClick.alreadyRegisterClick("alreadyRegister");
			}
		});
		tglRegisterNotification
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						isNotification = "" + isChecked;
					}
				});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		alredyClick = (alreadyRegisterInteface) activity;
		topInterface = (topBarInteface) activity;
		context = activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		// TODO Auto-generated method stub

		if (commonClass.languageId.equalsIgnoreCase("1")) {
			changeLanguage("en");
		} else {
			changeLanguage("no");
		}

		if (requestCode == 4 && resultCode == Activity.RESULT_OK) {

			try {
				userdetail = (UserDetail) data
						.getSerializableExtra("userDetail");



				if (userdetail.facebookId != null) {

					// setFacebookData(4);

					strMimsLoginWith = "Facebook";
					strEmailAddress = userdetail.facebookId;
					strEmailPassword = "";
					alreadyLoginAsync();

				}
			} catch (Exception e) {
				// TODO: handle exception
			}
			// facebook

		} else if (requestCode == 5 && resultCode == Activity.RESULT_OK) {
			// gooogle plus

			try {
				userdetail = (UserDetail) data
						.getSerializableExtra("userDetail");
				if (userdetail != null) {
					// setFacebookData(5);

					strMimsLoginWith = "GooglePlus";
					strEmailAddress = userdetail.googleplusId;
					strEmailPassword = "";
					alreadyLoginAsync();

				}

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("called exceptions===", e.getMessage().toString());
			}

		} else if (requestCode == 5 && resultCode == Activity.RESULT_CANCELED) {

		}

		else if (requestCode == 0 && resultCode == Activity.RESULT_OK) {

			// camera
			Bitmap photo = (Bitmap) data.getExtras().get("data");
			int width = photo.getWidth();
			int height = photo.getHeight();
			int newWidth = 200;
			int newHeight = 200;
			// calculate the scale - in this case = 0.4f
			float scaleWidth = ((float) newWidth) / width;
			float scaleHeight = ((float) newHeight) / height;

			// createa matrix for the manipulation
			Matrix matrix = new Matrix();
			// resize the bit map
			matrix.postScale(scaleWidth, scaleHeight);

			// recreate the new Bitmap
			Bitmap resizedBitmap = Bitmap.createBitmap(photo, 0, 0, width,
					height, matrix, true);

			convertFileToBitmap(resizedBitmap);
			imgRegisterCamera.setImageBitmap(resizedBitmap);
		} else if (requestCode == 1 && resultCode == Activity.RESULT_OK
				&& null != data) {
			// gallery

			Uri selectedImage = data.getData();
			try {
				Bitmap photo = MediaStore.Images.Media.getBitmap(getActivity()
						.getContentResolver(), selectedImage);
				Bitmap resized = Bitmap.createScaledBitmap(photo, 300, 300,
						true);
				convertFileToBitmap(resized);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Picasso.with(context).load(selectedImage).into(imgRegisterCamera);
		} else {
			// Log.e("called Activity Result", commonClass.languageId);

			if (commonClass.languageId.equalsIgnoreCase("1")) {
				changeLanguage("en");
			} else {
				changeLanguage("no");
			}
		}
	}

	private void changeLanguage(String languageName) {
		myLocale = new Locale(languageName);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);
		Set_All_Text_because_Language_changed();
		// Intent intent = new Intent(getActivity(), RegisterActivity.class);
		// startActivity(intent);
		// getActivity().finish();
	}

	private void setFacebookData(int id) {

		edtRegisterEmailAddress.setText(userdetail.emailAddress);
		edtRegisterName.setText(userdetail.name);
		if (userdetail.username != null) {
			edtRegisterUserName.setText(userdetail.username);
		}
		if (userdetail.birthDate != null) {
			txtRegisetrBirthDate.setText(userdetail.birthDate);
		}
		if (userdetail.gender != null) {
			if (userdetail.gender.equals("male")) {
				strGender = "Male";
				lnRegisterMale.setBackgroundColor(Color.parseColor("#72B600"));
				lnRegisterFemale
						.setBackgroundColor(Color.parseColor("#1068AF"));
			} else {
				strGender = "Female";
				lnRegisterFemale
						.setBackgroundColor(Color.parseColor("#72B600"));
				lnRegisterMale.setBackgroundColor(Color.parseColor("#1068AF"));
			}
		}

		if (id == 4) {
			topInterface.hideDialog();
			strFromRegister = "Facebook";
			strFacebookId = userdetail.facebookId;
			// userdetail.profileUrl = userdetail.profileUrl.replace("p50x50",
			// "p200x200");

			final Handler mhandler = new Handler();
			Thread th = new Thread(new Runnable() {
				@Override
				public void run() {

					mhandler.post(new Runnable() {

						@Override
						public void run() {

							// imgRegisterCamera.setImageBitmap(resizedBitmap);
							try {
								new getProfile().execute("");
							} catch (Exception e) {
								// TODO: handle exception
								Log.d("called exception", e.getMessage()
										.toString());
							}

							// imgRegisterCamera.setImageBitmap(resizedBitmap);
						}
					});

					// Bitmap photo = null;
					// try {
					// photo = Picasso.with(context)
					// .load(userdetail.profileUrl.toString())
					// .placeholder(R.drawable.icon_camera).get();
					// } catch (Exception e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }
					// int width = photo.getWidth();
					// int height = photo.getHeight();
					// int newWidth = 200;
					// int newHeight = 200;
					// // calculate the scale - in this case = 0.4f
					// float scaleWidth = ((float) newWidth) / width;
					// float scaleHeight = ((float) newHeight) / height;
					//
					// // createa matrix for the manipulation
					// Matrix matrix = new Matrix();
					// // resize the bit map
					// matrix.postScale(scaleWidth, scaleHeight);
					//
					// // recreate the new Bitmap
					// final Bitmap resizedBitmap = Bitmap.createBitmap(photo,
					// 0,
					// 0, width, height, matrix, true);
					//
					// convertFileToBitmap(resizedBitmap);
				}
			});

			th.start();
		} else if (id == 5) {
			strFromRegister = "GooglePlus";

			if (userdetail.googleplusId != null) {
				strGooglePlusId = userdetail.googleplusId;
			}
			if (userdetail.profileUrl != null) {
				userdetail.profileUrl = userdetail.profileUrl.replace("=50",
						"=250");

				async_deel_feed_list as = new async_deel_feed_list(
						new asynctaskloaddata() {

							Bitmap bmp = null;

							@Override
							public void onPreExecute() {
								// TODO Auto-generated method stub

							}

							@Override
							public void doInBackground() {
								// TODO Auto-generated method stub
								try {
									bmp = Picasso
											.with(context)
											.load(userdetail.profileUrl
													.toString())
											.placeholder(R.drawable.icon_camera)
											.get();
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}

							@Override
							public void onPostExecute() {
								// TODO Auto-generated method stub
								topInterface.hideDialog();
								if (bmp != null) {

									bmp = setimagebitmap(bmp);
									imgRegisterCamera.setImageBitmap(bmp);
									convertFileToBitmap(bmp);
								}

							}
						});

				as.execute();

			}

		}

		// imgRegisterCamera.buildDrawingCache();

		// commonClass.toastDisplay(userdetail.gender);
	}

	private class getProfile extends AsyncTask<String, Void, Bitmap> {

		@Override
		protected Bitmap doInBackground(String... params) {
			Bitmap bmp = null;
			try {

				// Log.e("facebook Id====", userdetail.facebookId);
				bmp = getPhotoFacebook(userdetail.facebookId);

			} catch (Exception e) {
				// TODO: handle exception
			}
			return bmp;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			// topInterface.hideDialog();
			try {
				if (result != null) {
					convertFileToBitmap(result);
					imgRegisterCamera.setImageBitmap(result);
				}

			} catch (Exception e) {
				// TODO: handle exception
				Log.e("called getprofile", e.getMessage().toString());
			}

		}

		@Override
		protected void onPreExecute() {

			// topInterface.showDialog("Loading");
		}

		@Override
		protected void onProgressUpdate(Void... values) {
		}
	}

	public Bitmap getPhotoFacebook(final String id) {

		Bitmap bitmap = null;
		final String nomimg = "https://graph.facebook.com/" + id
				+ "/picture?type=large";
		// Log.e("called facebook url===", nomimg);

		URL imageURL = null;

		try {
			imageURL = new URL(nomimg);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		try {
			HttpURLConnection connection = (HttpURLConnection) imageURL
					.openConnection();
			connection.setDoInput(true);
			connection.setInstanceFollowRedirects(true);
			connection.connect();
			InputStream inputStream = connection.getInputStream();
			// img_value.openConnection().setInstanceFollowRedirects(true).getInputStream()
			bitmap = BitmapFactory.decodeStream(inputStream);

		} catch (Exception e) {

			e.printStackTrace();
		}
		Matrix m = new Matrix();
		int bmpwidth = 0, bmpheight = 0;

		if (bitmap.getWidth() < 200) {
			bmpwidth = 200;
		} else if (bitmap.getHeight() < 200) {
			bmpheight = 200;
		} else {
			bmpwidth = bitmap.getWidth();
			bmpheight = bitmap.getHeight();
		}
		m.setRectToRect(new RectF(0, 0, bmpwidth, bmpheight), new RectF(0, 0,
				200, 200), Matrix.ScaleToFit.CENTER);
		// return Bitmap.createBitmap(bitmap, 0, 0, 200, 200, m, true);
		return Bitmap.createScaledBitmap(bitmap, 200, 200, true);

	}

	// private Bitmap scaleBitmap(Bitmap bm) {
	// int width = bm.getWidth();
	// int height = bm.getHeight();
	//
	//
	// bm = Bitmap.createScaledBitmap(bm, 200, height, true);
	// return bm;
	// }
	public void convertFileToBitmap(Bitmap resized) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(FileImage);
			resized.compress(Bitmap.CompressFormat.PNG, 90, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Throwable ignore) {
			}
		}
	}

	private Bitmap setimagebitmap(Bitmap loadbitmap) {

		int width = loadbitmap.getWidth();
		int height = loadbitmap.getHeight();
		int newWidth = 200;
		int newHeight = 200;
		// calculate the scale - in this case = 0.4f
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;

		// createa matrix for the manipulation
		Matrix matrix = new Matrix();
		// resize the bit map
		matrix.postScale(scaleWidth, scaleHeight);

		// recreate the new Bitmap
		final Bitmap resizedBitmap = Bitmap.createBitmap(loadbitmap, 0, 0,
				width, height, matrix, true);

		convertFileToBitmap(resizedBitmap);

		return resizedBitmap;
	}

	private void Set_All_Text_because_Language_changed() {

		txtLabelZipCode.setText(R.string.registerZipCode);
		txtRegister.setText(R.string.register);

		txtGender.setText(R.string.registerGender);
		txtMale.setText(R.string.registerMale);
		txtFemale.setText(R.string.registerFemale);
		tvusername.setText(R.string.registerName);
		tvuseremail.setText(R.string.registerEmail);
		tvmainusername.setText(R.string.registerUsername);
		tvpassword.setText(R.string.registerPassword);
		txtRegisterNotification.setText(R.string.registerNotification);
		txtloginFacebook.setText(R.string.registerFacebook);
		txtGooglePlus.setText(R.string.registerGooglePlus);
		txtLogin.setText(R.string.register);
		txtRegisterCurrency_show.setText(R.string.registerCurrency);
		txtAlredyRegister.setText(R.string.registerAlreadyRegisterd);
		tvSelectlanguage.setText(R.string.registerLanguage);
		tvbirthdate.setText(R.string.registerBirthDate);
		txtconfirmpass.setText(R.string.regConfirmPassword);

		String datepicker = DatePickerFragment.text_to_upload;
		if (datepicker.equals("")) {

			// txtRegisetrBirthDate.setText(R.string.registerBirthDate);

		} else {

			if (txtRegisetrBirthDate.getText().toString().trim().length() != 0) {

				txtRegisetrBirthDate
						.setText(getgenerater_birthdate(datepicker));
			}
		}

	}

	private CharSequence getgenerater_birthdate(String birthDate) {

		DatePickerFragment.text_to_upload = birthDate;

		String real_date = "";
		try {
			String split[] = birthDate.split("-");

			String year = split[0];
			String month = split[1];
			String date = split[2];

			if (commonClass.languageId.equals("1")) {
				real_date = convert_proper_date(date) + "/"
						+ convert_proper_date(month) + "/"
						+ convert_proper_date(year);
			} else {
				real_date = convert_proper_date(date) + "."
						+ convert_proper_date(month) + "."
						+ convert_proper_date(year);
			}
			// 14/08/2014
		} catch (Exception e) {

		}

		return real_date;
	}

	public int getAge(Date selectedMilli) {

		Date dateOfBirth = selectedMilli;
		Calendar dob = Calendar.getInstance();
		dob.setTime(dateOfBirth);
		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			age--;
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
				&& today.get(Calendar.DAY_OF_MONTH) < dob
						.get(Calendar.DAY_OF_MONTH)) {
			age--;
		}

		// if (age < 16) {
		// age=0;
		// } else {
		//
		// }

		String str_age = age + "";
		return age;
	}

	public String convert_proper_date(String sValue) {

		int value = Integer.parseInt(sValue);

		String converted = null;
		if (value > 0 && value < 10) {
			converted = "0" + value;
			return converted;
		} else {
			converted = "" + value;
			return converted;
		}

	}

	public String convert_proper_date(int value) {

		String converted = null;
		if (value > 0 && value < 10) {
			converted = "0" + value;
			return converted;
		} else {
			converted = "" + value;
			return converted;
		}

	}

	private static Age calculateAge(Date birthDate) {
		int years = 0;
		int months = 0;
		int days = 0;
		// create calendar object for birth day
		Calendar birthDay = Calendar.getInstance();
		birthDay.setTimeInMillis(birthDate.getTime());
		// create calendar object for current day
		long currentTime = System.currentTimeMillis();
		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(currentTime);
		// Get difference between years
		years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
		int currMonth = now.get(Calendar.MONTH) + 1;
		int birthMonth = birthDay.get(Calendar.MONTH) + 1;
		// Get difference between months
		months = currMonth - birthMonth;
		// if month difference is in negative then reduce years by one and
		// calculate the number of months.
		if (months < 0) {
			years--;
			months = 12 - birthMonth + currMonth;
			if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
				months--;
		} else if (months == 0
				&& now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
			years--;
			months = 11;
		}
		// Calculate the days
		if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
			days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
		else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
			int today = now.get(Calendar.DAY_OF_MONTH);
			now.add(Calendar.MONTH, -1);
			days = now.getActualMaximum(Calendar.DAY_OF_MONTH)
					- birthDay.get(Calendar.DAY_OF_MONTH) + today;
		} else {
			days = 0;
			if (months == 12) {
				years++;
				months = 0;
			}
		}
		// Create new Age object

		return new Age(days, months, years);
		// return years;
	}

	private int getAge(String selectedMilli) {
		Calendar today = Calendar.getInstance();

		Calendar dob = Calendar.getInstance();
		dob.setTime(new Date(selectedMilli));

		int age = today.get(Calendar.YEAR) - (dob.get(Calendar.YEAR) - 1);
		if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			age--;
		} else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
				&& (today.get(Calendar.DAY_OF_MONTH)) < (dob
						.get(Calendar.DAY_OF_MONTH))) {

			age--;
		}
		// Log.d("If DOB", "" + (today.get(Calendar.DAY_OF_MONTH)));
		// Log.d("If Current", "" + dob.get(Calendar.DAY_OF_MONTH));

		// Date dateOfBirth = new Date(selectedMilli);
		// Calendar dob = Calendar.getInstance();
		// dob.setTime(dateOfBirth);
		// Calendar today = Calendar.getInstance();
		// int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		// if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
		// age--;
		// } else if (today.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
		// && today.get(Calendar.DAY_OF_MONTH) < dob
		// .get(Calendar.DAY_OF_MONTH)) {
		// age--;
		// }
		//
		// if (age < 18) {
		//
		// } else {
		//
		// }
		//
		// String str_age = age + "";
		// Log.d("", getClass().getSimpleName() + ": Age in year= " + age);
		return age;
	}

	private void alreadyLoginAsync() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topInterface.showDialog("Loading");
						url_response = "";
					}

					@Override
					public void doInBackground() {

						double latitude = commonClass.currentLatitude;
						double longiutde = commonClass.currentLongitude;

						ArrayList<Double> arrLatitude = ((RegisterActivity) getActivity())
								.getLatitudeLongitude();

						latitude = arrLatitude.get(0);
						longiutde = arrLatitude.get(1);

						String getUrl = "CustomerService.svc/Login?languageid="
								+ commonClass.languageId
								+ "&loginwith="
								+ strMimsLoginWith
								+ "&id="
								+ strEmailAddress
								+ "&password="
								+ commonClass
										.getMD5EncryptedString(strEmailPassword)
								+ "&deviceid=" + CommonData.deviceId
								+ "&device=android&latitude=" + latitude
								+ "&longitude=" + longiutde;

						getUrl = commonClass.Url_EncodeWithBlank(getUrl);
						// Log.e("Direct Register	", "" + getUrl);
						url_response = webService.makeServicegetRequest(getUrl);
					}

					@Override
					public void onPostExecute() {

						try {
							JSONObject js_obj = new JSONObject(url_response);
							String VStatusCode = js_obj
									.getString("vStatusCode");
							String vMessageResponse = js_obj
									.getString("vMessageResponse");

							if (VStatusCode.equals("420")) {
								topInterface.hideDialog();
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (VStatusCode.equals("100")) {

								commonClass.CustomerId = js_obj
										.getString("iCustomerId");
								commonClass.languageId = js_obj
										.getString("iLanguageId");

								getUserDetail();

							} else if (VStatusCode.equals("115")) {

								SyncDataAsync();

							}

							else {

								if (strMimsLoginWith
										.equalsIgnoreCase("Facebook")) {
									setFacebookData(4);

								} else if (strMimsLoginWith
										.equalsIgnoreCase("GooglePlus")) {
									setFacebookData(5);
								}
								// DialogFragment ds = new SingleButtonAlert(
								// new DialogInterfaceClick() {
								//
								// @Override
								// public void dialogClick(String tag) {
								//
								// strMimsLoginWith = "Mims";
								//
								// }
								// }, vMessageResponse);
								// ds.setCancelable(false);
								// ds.show(getFragmentManager(), "");
							}
						} catch (Exception e) {
						}
					}
				});
		as.execute();
	}

	private void SyncDataAsync() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						// topbar.showDialog("Loading");
						url_response = "";

					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub

						double latitude = commonClass.currentLatitude;
						double longiutde = commonClass.currentLongitude;

						ArrayList<Double> arrLatitude = ((RegisterActivity) getActivity())
								.getLatitudeLongitude();

						latitude = arrLatitude.get(0);
						longiutde = arrLatitude.get(1);

						String getUrl = "CustomerService.svc/Synchronize?languageid="
								+ commonClass.languageId
								+ "&loginwith="
								+ strMimsLoginWith
								+ "&id="
								+ strEmailAddress
								+ "&password="
								+ commonClass
										.getMD5EncryptedString(strEmailPassword)
								+ "&deviceid="
								+ commonClass.deviceId
								+ "&device=android&latitude="
								+ latitude
								+ "&longitude=" + longiutde;

						getUrl = commonClass.Url_EncodeWithBlank(getUrl);
						url_response = webService.makeServicegetRequest(getUrl);
					}

					@Override
					public void onPostExecute() {
						// topbar.hideDialog();
						try {
							JSONObject js_obj = new JSONObject(url_response);
							String VStatusCode = js_obj
									.getString("vStatusCode");
							String vMessageResponse = js_obj
									.getString("vMessageResponse");
							// commonClass.toastDisplay(VStatusCode);

							if (VStatusCode.equals("420")) {
								topInterface.hideDialog();
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (VStatusCode.equals("100")) {
								commonClass.CustomerId = js_obj
										.getString("iCustomerId");
								commonClass.languageId = js_obj
										.getString("iLanguageId");

								getUserDetail();
							} else {
								topInterface.hideDialog();
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

											}
										}, vMessageResponse);
								ds.setCancelable(false);
								ds.show(getFragmentManager(), "");
							}
						} catch (Exception e) {
							// Toast.makeText(getActivity(),
							// "exception" + e.getMessage().toString(),
							// Toast.LENGTH_LONG).show();
						}
					}
				});
		as.execute();
	}

	// get user details

	private void getUserDetail() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						preference = getActivity().getSharedPreferences(
								"UserDetail", Context.MODE_PRIVATE);
						strStatusCode = "";
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						try {
							String geturl = "CustomerService.svc/GetProfile?customerid="
									+ commonClass.CustomerId
									+ "&languageid="
									+ commonClass.languageId;
							geturl = commonClass.Url_EncodeWithBlank(geturl);
							url_response = webService
									.makeServicegetRequest(geturl);
							JSONObject js_obj_getprofileresult = new JSONObject(
									url_response);
							strStatusCode = js_obj_getprofileresult
									.getString("vStatusCode");

							if (strStatusCode.equals("100")) {
								commonClass.ProfiileImagePath = js_obj_getprofileresult
										.getString("vProfileImageName");

								SharedPreferences sp = getActivity()
										.getSharedPreferences("UserDetail",
												Context.MODE_PRIVATE);
								sp.edit()
										.putString(
												"profileimagepath",
												js_obj_getprofileresult
														.getString("vProfileImageName"))
										.commit();

								commonClass.ProfileName = js_obj_getprofileresult
										.getString("vCustomerName");
								commonClass.strUsername = js_obj_getprofileresult
										.getString("vUserName");
								commonClass.strPassword = preference.getString(
										"password", "");
							}
						} catch (Exception e) {

						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						if (strStatusCode.equals("420")) {
							topInterface.hideDialog();
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.PoorInternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						} else if (strStatusCode.equals("100")) {
							try {
								preference = getActivity()
										.getSharedPreferences("UserDetail",
												Context.MODE_PRIVATE);
								Editor editior = preference.edit();
								editior.putString("customerid",
										commonClass.CustomerId);
								editior.putBoolean("firsttime", false);
								editior.putString(
										"password",
										commonClass
												.getMD5EncryptedString(strEmailPassword));
								editior.putString("languageid",
										commonClass.languageId);
								editior.putString("username",
										commonClass.strUsername);

								editior.putString("emailid",
										edtRegisterEmailAddress.getText()
												.toString().trim());
								editior.putString("customername",
										commonClass.ProfileName);
								editior.commit();
								topInterface.hideDialog();
								alredyClick.alreadyRegisterClick("register");
							} catch (Exception e) {

							}

						} else {
							topInterface.hideDialog();
						}
					}
				});
		as.execute();
	}

	public boolean checkLocationService() {
		LocationManager lm = null;
		boolean gps_enabled = false, network_enabled = false;
		if (lm == null)
			lm = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);
		try {
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}

		if (!gps_enabled && !network_enabled) {
			return false;

		}
		return true;
	}

}
