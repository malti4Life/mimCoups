package com.mimcoups.Consumer.fragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentShareViaMail extends Fragment {

	private ImageView imgAddCampaignBack, imgAddCampaignQuestions,
			imgFirstPlus;
	private Button btnAddCampignSendInvites;
	private topBarInteface topInterface;
	private int rowAdded = 0;
	private LinearLayout lnAddRow;
	private WebService_Call webService = new WebService_Call();
	private String url_response = "", getUrl = "", vStatusCode = "",
			vMessageResponse = "";
	private EditText edtFirstEmail;
	private CommonData commonData;
	private boolean isCheckValidation = false;
	DealFeedDetail dealdetail;
	LocationList locationdetail;
	String sharevia;
	TextView txtDeelFeed, description;
	RelativeLayout relmainfragment_sharevia_mail;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_sharevia_mail,
				container, false);

		initControls(view);
		clickEvents();
		commonData = (CommonData) getActivity().getApplicationContext();

		sharevia = (String) getArguments().getSerializable("sharevia");

		if (sharevia.equals("location")) {
			locationdetail = (LocationList) getArguments().getSerializable(
					"LocationDetail");
			description.setText(getResources().getString(
					R.string.sharewithelocationmail));
			txtDeelFeed.setText(getResources().getString(
					R.string.sharewithelocationmail_title));
		}

		if (sharevia.equals("deal")) {
			dealdetail = (DealFeedDetail) getArguments().getSerializable(
					"DealFeedDetail");
			description.setText(getResources().getString(
					R.string.sharewithemail));
			txtDeelFeed.setText(getResources().getString(
					R.string.sharewithemail_title));
		}

		return view;
	}

	private void initControls(View view) {

		relmainfragment_sharevia_mail = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_sharevia_mail);

		imgAddCampaignBack = (ImageView) view
				.findViewById(R.id.imgAddCampaignBack);
		description = (TextView) view.findViewById(R.id.txtNoData);
		imgAddCampaignQuestions = (ImageView) view
				.findViewById(R.id.imgAddCampaignQuestions);
		txtDeelFeed = (TextView) view.findViewById(R.id.txtDeelFeed);
		btnAddCampignSendInvites = (Button) view
				.findViewById(R.id.btnAddCampignSendInvites);
		imgFirstPlus = (ImageView) view.findViewById(R.id.imgFirstPlus);
		lnAddRow = (LinearLayout) view.findViewById(R.id.lnAddRow);
		edtFirstEmail = (EditText) view.findViewById(R.id.edtFirstEmail);
	}

	private void clickEvents() {

		relmainfragment_sharevia_mail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		imgFirstPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (rowAdded < 3) {
					rowAdded++;
					addRowDynamically();
				} else {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub
								}
							}, getActivity().getResources()
									.getText(R.string.maxinvitationmessage)
									.toString());
					ds.show(getFragmentManager(), "");
				}
			}
		});
		imgAddCampaignBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("back");
			}
		});
		imgAddCampaignQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					topInterface.topbarClick("questions");
				}
			}
		});
		btnAddCampignSendInvites.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				sendInvites();

			}
		});
	}

	protected void addRowDynamically() {
		// TODO Auto-generated method stub
		final View view = getActivity().getLayoutInflater().inflate(
				R.layout.row_addcampaign, null, false);

		EditText edtText = (EditText) view.findViewById(R.id.edtEmailAddress);
		ImageView imgDeleteRow = (ImageView) view.findViewById(R.id.imgMinus);
		imgDeleteRow.setId(rowAdded);
		edtText.setId(rowAdded);

		imgDeleteRow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				lnAddRow.removeView(view);
				// lnAddRow.invalidate();
				rowAdded--;
			}
		});
		view.setId(rowAdded);
		lnAddRow.addView(view);

	}

	protected void sendInvites() {
		// TODO Auto-generated method stub
		if (edtFirstEmail.getText().toString().trim().equals("")) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
						}
					}, getActivity().getResources()
							.getText(R.string.emailvalidation).toString());
			ds.show(getFragmentManager(), "");
		} else if (!Patterns.EMAIL_ADDRESS.matcher(
				edtFirstEmail.getText().toString().trim()).matches()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
						}
					}, getActivity().getResources()
							.getText(R.string.validemailvalidation).toString());
			ds.show(getFragmentManager(), "");
		} else if (!commonData.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			try {
				int count = lnAddRow.getChildCount();
				View v = null;
				final JSONObject jsonMain = new JSONObject();
				JSONArray array = new JSONArray();

				JSONObject temPObject = new JSONObject();
				try {
					temPObject.put("vFriendEmail", edtFirstEmail.getText()
							.toString().trim());
					array.put(temPObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				for (int i = 0; i < count; i++) {

					v = lnAddRow.getChildAt(i);
					if (v instanceof LinearLayout) {
						LinearLayout tempLainear = ((LinearLayout) v);
						isCheckValidation = false;
						int counts = tempLainear.getChildCount();
						for (int j = 0; j < counts; j++) {
							View tempView = tempLainear.getChildAt(j);
							if (tempView instanceof EditText) {

								EditText edtText = ((EditText) tempView);
								// if (checkValidation(edtText, j)) {

								if (edtText.getText().toString().trim()
										.length() != 0) {

									if (!(Patterns.EMAIL_ADDRESS
											.matcher(edtText.getText()
													.toString().trim()))
											.matches()) {
										isCheckValidation = true;
									} else {
										JSONObject temPObjects = new JSONObject();
										try {

											temPObjects.put("vFriendEmail",
													edtText.getText()
															.toString().trim());
											array.put(temPObjects);
										} catch (JSONException e) {
											// TODO Auto-generated catch
											// block
											e.printStackTrace();
										}
									}
								}
								// }

								// }
								// else {
								// isCheckValidation = true;
								// }
							}
						}
					}
				}

				if (isCheckValidation) {
					DialogFragment dsp = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub
									isCheckValidation = false;
								}
							}, getResources()
									.getString(R.string.duplicateEmail));
					dsp.show(getFragmentManager(), "");
				} else {

					if (sharevia.equals("deal")) {

						
						
						
						
						jsonMain.put("iLanguageId", commonData.languageId);
						jsonMain.put("iCustomerId", commonData.CustomerId);
						jsonMain.put("iLocationId", dealdetail.locationId);
						jsonMain.put("iMerchantDealId",
								dealdetail.iMerchantDealId);
						jsonMain.put("SharingEmails", array);

						//

						async_deel_feed_list as = new async_deel_feed_list(
								new asynctaskloaddata() {

									@Override
									public void onPreExecute() {
										// TODO Auto-generated method stub
										topInterface.showDialog("Loading");
									}

									@Override
									public void doInBackground() {
										// TODO Auto-generated method stub

										try {

											getUrl = "DealService.svc/ShareDealViaEmail";

											url_response = webService.makeServiceShareViaEmail(
													getUrl, jsonMain.toString());

											Log.e("Share Deal Request", getUrl
													+ "==" + url_response);
											// Log.e("Share Deal Response",
											// url_response);
											JSONObject json_object = new JSONObject(
													url_response);

											vStatusCode = json_object
													.getString("vStatusCode");
											vMessageResponse = json_object
													.getString("vMessageResponse");
										} catch (Exception e) {
											// TODO Auto-generated catch block

											vMessageResponse = getResources()
													.getString(
															R.string.errormesssage);

										}
									}

									@Override
									public void onPostExecute() {
										// TODO Auto-generated method stub
										topInterface.hideDialog();

										if (vStatusCode.equals("420")) {
											DialogFragment ds = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {

															topInterface
																	.topbarClick("back");
														}
													},
													getResources()
															.getString(
																	R.string.PoorInternetConnect));
											ds.show(getFragmentManager(), "");
											ds.setCancelable(false);

										} else if (vStatusCode.equals("100")) {
											DialogFragment dsp = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {
															// TODO
															// Auto-generated
															// method
															// stub
															topInterface
																	.topbarClick("sendInvites");
														}
													}, vMessageResponse);
											dsp.show(getFragmentManager(), "");
										} else {
											DialogFragment dsp = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {

														}
													}, vMessageResponse);
											dsp.show(getFragmentManager(), "");
										}

									}
								});
						as.execute();

					}
					if (sharevia.equalsIgnoreCase("location")) {

						jsonMain.put("iLanguageId", commonData.languageId);
						jsonMain.put("iCustomerId", commonData.CustomerId);
						jsonMain.put("iLocationId",
								locationdetail.iMerchantStoreId);
						jsonMain.put("SharingEmails", array);

						async_deel_feed_list as = new async_deel_feed_list(
								new asynctaskloaddata() {

									@Override
									public void onPreExecute() {
										// TODO Auto-generated method stub
										topInterface.showDialog("Loading");
									}

									@Override
									public void doInBackground() {
										try {

											getUrl = "LocationService.svc/ShareLocationViaEmail";
											url_response = webService.makeServiceShareViaEmail(
													getUrl, jsonMain.toString());
											JSONObject json_object = new JSONObject(
													url_response);

											vStatusCode = json_object
													.getString("vStatusCode");
											vMessageResponse = json_object
													.getString("vMessageResponse");
										} catch (Exception e) {
											// TODO Auto-generated catch block

											vMessageResponse = getResources()
													.getString(
															R.string.errormesssage);

										}
									}

									@Override
									public void onPostExecute() {
										// TODO Auto-generated method stub
										topInterface.hideDialog();
										if (vStatusCode.equals("420")) {
											DialogFragment ds = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {

															topInterface
																	.topbarClick("back");
														}
													},
													getResources()
															.getString(
																	R.string.PoorInternetConnect));
											ds.show(getFragmentManager(), "");
											ds.setCancelable(false);

										} else if (vStatusCode.equals("100")) {
											DialogFragment dsp = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {
															// TODO
															// Auto-generated
															// method
															// stub
															topInterface
																	.topbarClick("sendInvites");
														}
													}, vMessageResponse);
											dsp.show(getFragmentManager(), "");
										} else {
											DialogFragment dsp = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {

														}
													}, vMessageResponse);
											dsp.show(getFragmentManager(), "");
										}

									}
								});
						as.execute();

					}

				}

			} catch (Exception e) {

				Log.e("Exception share mail", e.getMessage().toString());

			}
		}

	}

	private boolean checkValidation(EditText edtText, int maxLength) {
		// TODO Auto-generated method stub

		if (maxLength == 0) {
			if (edtText.getText().toString().trim()
					.equals(edtFirstEmail.getText().toString().trim())) {
				return false;
			}
		} else {
			for (int i = 0; i < maxLength; i++) {
				View v = lnAddRow.getChildAt(i);
				if (v instanceof LinearLayout) {
					LinearLayout tempLainear = ((LinearLayout) v);
					int counts = tempLainear.getChildCount();
					for (int j = 0; j < counts; j++) {
						View tempView = tempLainear.getChildAt(j);
						if (tempView instanceof EditText) {

							EditText edtAllText = ((EditText) tempView);
							if (edtAllText
									.getText()
									.toString()
									.trim()
									.equals(edtText.getText().toString().trim())) {
								return false;
							}
						}
					}
				}
			}
		}
		return true;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}
}
