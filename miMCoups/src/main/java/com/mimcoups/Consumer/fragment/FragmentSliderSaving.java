package com.mimcoups.Consumer.fragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mimcoups.Consumer.DialogFragment.DatePickerFragment;
import com.mimcoups.Consumer.customClass.DateCustomeClass;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.SavingDealAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.inteface.wrongdatedialoginterface;

@SuppressLint("SimpleDateFormat")
public class FragmentSliderSaving extends Fragment {

	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	private int preLast;
	private boolean loadmore = true, filter = false;
	private topBarInteface topbarinterface;
	private ImageView imgSliderSavingSearch, imgSliderSavingQuestions,
			imgSlideSavingNotificationToDate,
			imgSlideSavingNotificationFromDate;
	private ListView lstSliderSavingList;
	String vTotalSavingAmount = "";
	public double dTotalSavingAmount = 0.0;
	private ArrayList<DealFeedDetail> arrSaving = new ArrayList<DealFeedDetail>();
	private ArrayList<DealFeedDetail> arrSaving_dateshorted = new ArrayList<DealFeedDetail>();
	private SavingDealAdapter adapterSavingDeals;
	private LinearLayout lnSliderSavingSearch, lnEmptyView;
	private TextView txtSlideSavingNotificationnFromDate,
			txtSlideSavingNotificationToDate, txtSliderSavingAmount;
	private CommonData commonClass;
	private WebService_Call web_service = new WebService_Call();
	private String strGeturl = "", strStatusCode = "", strMessageResponse,
			urlResponse = "", vExpiredDealMessage = "", StrPageIndex = "1";
	RelativeLayout relmainfragment_slider_saving;
	LinearLayout lnLoadMore;

	DateCustomeClass fromdate;
	String startdate = "";
	String enddate = "";
	View views;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_saving,
				container, false);

		initControls(view);
		views = inflater.inflate(R.layout.loadmore, null);

		lstSliderSavingList.addFooterView(views);

		setblankadapter();

		setData();
		clicks_events();

		return view;
	}

	private void clicks_events() {
		// TODO Auto-generated method stub

		// ListView list = (ListView) getListView();
		// list.setOnScrollListener(this);
		lstSliderSavingList.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub

				final int lastItem = firstVisibleItem + visibleItemCount;
				if (lastItem == totalItemCount) {

					if ((preLast != lastItem) && loadmore) {

						if (!commonClass.checkInternetConnection()) {
							// DialogFragment ds = new SingleButtonAlert(
							// new DialogInterfaceClick() {
							//
							// @Override
							// public void dialogClick(String tag) {
							//
							// topInterface.topbarClick("back");
							// }
							// }, getResources().getString(
							// R.string.InternetConnect));
							// ds.show(getFragmentManager(), "");
						} else {

							// lnLoadMore.setVisibility(View.VISIBLE);

							loadmore = false;
							setData();

						}
					}
				}

			}
		});

		imgSlideSavingNotificationFromDate
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							lastClickTime = currentTime;

							final Calendar c = Calendar.getInstance();
							int year = c.get(Calendar.YEAR);
							int month = c.get(Calendar.MONTH) + 1;
							int day = c.get(Calendar.DAY_OF_MONTH);

							if (txtSlideSavingNotificationnFromDate.getText()
									.toString().trim().length() != 0) {
								// Log.d("if", "1");
								if (txtSlideSavingNotificationnFromDate
										.getText().toString().contains("/")) {
									// Log.d("if", "2");
									String split[] = txtSlideSavingNotificationnFromDate
											.getText().toString().split("/");
									year = Integer.parseInt(split[2]);
									month = Integer.parseInt(split[1]);
									day = Integer.parseInt(split[0]);
								} else {
									// Log.d("if", "3");
									String split[] = txtSlideSavingNotificationnFromDate
											.getText().toString().split("\\.");
									Log.d("split size", "" + split.length);
									year = Integer.parseInt(split[2]);
									month = Integer.parseInt(split[1]);
									day = Integer.parseInt(split[0]);

									Log.d("year", "" + year);
									Log.d("month", "" + month);
									Log.d("day", "" + day);
								}

							}
							DialogFragment newFragment = new DatePickerFragment(
									new wrongdatedialoginterface() {

										@Override
										public void selecteddateiswrongedialog(
												String message) {
											DialogFragment ds = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {

														}
													}, message);
											ds.show(getFragmentManager(), "");
										}

										@Override
										public void ondateselectedevent(
												DateCustomeClass dateclass) {

											fromdate = dateclass;

										}
									}, getActivity(),
									txtSlideSavingNotificationnFromDate,
									commonClass.languageId, year, month, day);
							newFragment
									.show(getFragmentManager(), "datePicker");
						}
					}
				});
		imgSlideSavingNotificationToDate
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							lastClickTime = currentTime;

							String savingnotify = txtSlideSavingNotificationnFromDate
									.getText().toString();

							if (savingnotify.equals(getResources().getString(
									R.string.saving__fromdate))) {

								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// Toast.makeText(getActivity(),
												// "clicked", 5000).show();
											}
										},
										getActivity()
												.getResources()
												.getText(
														R.string.fromdatevalidation)
												.toString());
								ds.show(getFragmentManager(), "");

							} else {

								final Calendar c = Calendar.getInstance();
								int year = c.get(Calendar.YEAR);
								int month = c.get(Calendar.MONTH) + 1;
								int day = c.get(Calendar.DAY_OF_MONTH);

								if (txtSlideSavingNotificationToDate.getText()
										.toString().trim().length() != 0) {
									// Log.d("if", "1");
									if (txtSlideSavingNotificationToDate
											.getText().toString().contains("/")) {
										// Log.d("if", "2");
										String split[] = txtSlideSavingNotificationToDate
												.getText().toString()
												.split("/");
										year = Integer.parseInt(split[2]);
										month = Integer.parseInt(split[1]);
										day = Integer.parseInt(split[0]);
									} else {
										// Log.d("if", "3");
										String split[] = txtSlideSavingNotificationToDate
												.getText().toString()
												.split("\\.");
										// Log.d("split size", "" +
										// split.length);
										year = Integer.parseInt(split[2]);
										month = Integer.parseInt(split[1]);
										day = Integer.parseInt(split[0]);

										Log.d("year", "" + year);
										Log.d("month", "" + month);
										Log.d("day", "" + day);
									}

								}

								DialogFragment newFragment = new DatePickerFragment(
										new wrongdatedialoginterface() {

											@Override
											public void selecteddateiswrongedialog(
													String message) {

												DialogFragment ds = new SingleButtonAlert(
														new DialogInterfaceClick() {

															@Override
															public void dialogClick(
																	String tag) {

															}
														}, message);
												ds.show(getFragmentManager(),
														"");
											}

											@Override
											public void ondateselectedevent(
													DateCustomeClass dateclass) {
												// TODO Auto-generated method
												// stub

												SimpleDateFormat sdf = new SimpleDateFormat(
														"yyyy-MM-dd");
												Date current = null;
												Date date2 = null;
												try {
													current = sdf
															.parse(fromdate.year
																	+ "-"
																	+ fromdate.month
																	+ "-"
																	+ fromdate.date);
													date2 = sdf
															.parse(dateclass.year
																	+ "-"
																	+ dateclass.month
																	+ "-"
																	+ dateclass.date);

													int datecompare = current
															.compareTo(date2);

													if (datecompare == 1) {

//														DialogFragment ds = new SingleButtonAlert(
//																new DialogInterfaceClick() {
//
//																	@Override
//																	public void dialogClick(
//																			String tag) {
//																		txtSlideSavingNotificationToDate
//																				.setHint(getActivity()
//																						.getResources()
//																						.getString(
//																								R.string.saving__todate));
//																	}
//																},
//																getResources()
//																		.getString(
//																				R.string.datevalidation));
//														ds.show(getFragmentManager(),
//																"");
													}

												} catch (Exception e) {
													e.printStackTrace();
												}

												// if (datecompare == -1) {
												// Toast.makeText(getActivity(),
												// "bigger", 5000).show();
												// }

											}
										}, getActivity(),
										txtSlideSavingNotificationToDate,
										commonClass.languageId, year, month,
										day);
								newFragment.show(getFragmentManager(),
										"datePicker");

							}
						}
					}
				});
		imgSliderSavingSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topbarinterface.topbarClick("search");

			}
		});
		imgSliderSavingQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					topbarinterface.topbarClick("questions");
				}
			}
		});

		relmainfragment_slider_saving.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});

		lnSliderSavingSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Toast.makeText(getActivity(), "on clicked", 9000).show();

				String startdate = txtSlideSavingNotificationnFromDate
						.getText().toString();
				String enddate = txtSlideSavingNotificationToDate.getText()
						.toString();

				DateFormat dateFormat = new SimpleDateFormat(
						"yyyy/MM/dd HH:mm:ss");
				

				Date date_startdate = null, date_enddate = null;
				if (startdate.toString().trim().length() == 0) {

					Toast.makeText(
							getActivity(),
							getActivity().getResources().getString(
									R.string.startDate), Toast.LENGTH_SHORT)
							.show();

				}

				else if (enddate.toString().trim().length() == 0) {

					Toast.makeText(
							getActivity(),
							getActivity().getResources().getString(
									R.string.endDate), Toast.LENGTH_SHORT)
							.show();

				} else if (enddate.toString().trim().length() == 0) {

					Toast.makeText(
							getActivity(),
							getActivity().getResources().getString(
									R.string.endDate), Toast.LENGTH_SHORT)
							.show();

				} else {

					SimpleDateFormat sdf;
					if (txtSlideSavingNotificationnFromDate.getText()
							.toString().contains("/")) {
						sdf = new SimpleDateFormat("dd/MM/yyyy");
					} else {
						sdf = new SimpleDateFormat("dd.MM.yyyy");
					}

					try {
						date_startdate = sdf.parse(startdate);
						date_enddate = sdf.parse(enddate);

					} catch (ParseException e) {
						e.printStackTrace();
					}

					// Filter_Data_withdate(date_startdate, date_enddate);
					
					Date date = new Date();
					System.out.println("Current Date==="+sdf.format(date));
					System.out.println("From Date==="+sdf.format(date_startdate));
					System.out.println("To Date==="+sdf.format(date_enddate));
					
					Log.e("Current compare",""+ date.compareTo(date_enddate));
					
					if((date.compareTo(date_enddate))== -1){
						
						Toast.makeText(
								getActivity(),
								getActivity().getResources().getString(
										R.string.TovalidDate), Toast.LENGTH_SHORT)
								.show();
						
					}
					
					else if ((date_startdate.compareTo(date_enddate)) <= 0) {

						arrSaving.clear();
						arrSaving = new ArrayList<DealFeedDetail>();
						setblankadapter();

						Filter_Data_withdate(date_startdate, date_enddate);
					} else {
						Toast.makeText(
								getActivity(),
								getActivity().getResources().getString(
										R.string.validDate), Toast.LENGTH_SHORT)
								.show();
					}
				}

			}

		});

	}

	private void initControls(View view) {

		relmainfragment_slider_saving = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_saving);
		imgSliderSavingSearch = (ImageView) view
				.findViewById(R.id.imgSliderSavingSearch);
		imgSliderSavingQuestions = (ImageView) view
				.findViewById(R.id.imgSliderSavingQuestions);
		lnEmptyView = (LinearLayout) view.findViewById(R.id.empty);

		lnSliderSavingSearch = (LinearLayout) view
				.findViewById(R.id.lnSliderSavingSearch);

		lstSliderSavingList = (ListView) view.findViewById(R.id.list);

		txtSlideSavingNotificationnFromDate = (TextView) view
				.findViewById(R.id.txtSlideSavingNotificationnFromDate);
		txtSlideSavingNotificationToDate = (TextView) view
				.findViewById(R.id.txtSlideSavingNotificationToDate);
		txtSliderSavingAmount = (TextView) view
				.findViewById(R.id.txtSliderSavingAmount);
		imgSlideSavingNotificationToDate = (ImageView) view
				.findViewById(R.id.imgSlideSavingNotificationToDate);
		imgSlideSavingNotificationFromDate = (ImageView) view
				.findViewById(R.id.imgSlideSavingNotificationFromDate);
		lnLoadMore = (LinearLayout) view.findViewById(R.id.lnLoadMore);

		commonClass = (CommonData) getActivity().getApplicationContext();

	}

	public void set_savingdata(double savingamount) {

		String str = String.format("%.2f", savingamount);

		txtSliderSavingAmount.setText(vTotalSavingAmount + "" + str);
	}

	private void Filter_Data_withdate(Date date_startdate, Date date_enddate) {

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		startdate = df.format(date_startdate);
		enddate = df.format(date_enddate);

		StrPageIndex = "1";

		filter = true;
		arrSaving.clear();
		arrSaving = new ArrayList<DealFeedDetail>();
		// setblankadapter();

		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topbarinterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub

							if (StrPageIndex.equalsIgnoreCase("1")) {

								// topbarinterface.showDialog("Loading");
							}
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							// for (int i = 0; i < temp; i++) {
							// DealFeedDetail ds = new DealFeedDetail();
							// arrSaving.add(ds);
							// }
							String str = "";
							str = "OrderService.svc/GetCustomerSavingdetails?languageid="
									+ commonClass.languageId
									+ "&customerid="
									+ commonClass.CustomerId
									+ "&startindex="
									+ StrPageIndex
									+ "&startdate="
									+ startdate
									+ "&enddate=" + enddate;

							// Log.e("called ===URL", "" + str);
							urlResponse = web_service
									.makeServicegetRequest(str);

							// Log.e("called ===", "" + urlResponse);

							try {

								JSONObject json_Object = new JSONObject(
										urlResponse);

								strStatusCode = json_Object
										.getString("vStatusCode");

								strMessageResponse = json_Object
										.getString("vMessageResponse");

								vTotalSavingAmount = json_Object
										.getString("vTotalSavingAmount");
								dTotalSavingAmount = json_Object
										.getDouble("dTotalSavingAmount");
								Log.d("dtotal saving amount>>", ""
										+ dTotalSavingAmount);
								StrPageIndex = json_Object
										.getString("iLastIndex");

								if (strStatusCode.equals("100")) {

									JSONArray json_result = json_Object
											.getJSONArray("vObjCustomerDetails");
									vExpiredDealMessage = json_Object
											.getString("vExpiredDealMessage");

									for (int i = 0; i < json_result.length(); i++) {

										JSONObject json_deal = json_result
												.getJSONObject(i);

										DealFeedDetail dealDetail = new DealFeedDetail();

										dealDetail.iMerchantDealId = json_deal
												.getString("iMerchantDealId");
										dealDetail.DealId = dealDetail.iMerchantDealId;
										dealDetail.locationId = json_deal
												.getString("iMerchantStoreId");
										Log.e("location Id", "" + i + "==="
												+ dealDetail.locationId);
										dealDetail.vExpiredDealMessage = vExpiredDealMessage;

										// dealDetail.vMessage = json_deal
										// .getString("vMessage");

										dealDetail.StoreName = json_deal
												.getString("vStorename");

										dealDetail.dealName = json_deal
												.getString("vDealName");

										dealDetail.vPurchaseamount = json_deal
												.getString("vPurchaseamount");
										dealDetail.vOrderamount = json_deal
												.getString("vOrderamount");
										dealDetail.vSavingamount = json_deal
												.getString("vSavingamount");
										dealDetail.dSavingamount = json_deal
												.getDouble("dSavingamount");

										Log.d("dealDetail.dsaving post>>>", ""
												+ dealDetail.dSavingamount);
										dealDetail.iOrderId = json_deal
												.getString("iOrderId");

										dealDetail.vCustomerOrderStatus = json_deal
												.getString("vCustomerOrderStatus");

										dealDetail.DealOrignalPrice = dealDetail.vPurchaseamount;

										dealDetail.dealDiscountPrice = dealDetail.vOrderamount;

										dealDetail.shortDescriptionHeading = json_deal
												.getString("vShortDescriptionHeading");

										dealDetail.shortDescription = json_deal
												.getString("vDealShotDescription");

										dealDetail.vDealListImage_A_350X350 = json_deal
												.getString("vDealListImage_A_350X350");
										dealDetail.DealImageUrl = dealDetail.vDealListImage_A_350X350;
										dealDetail.vPurchaseDate = json_deal
												.getString("vPurchaseDate");
										dealDetail.bDealStatus = json_deal
												.getBoolean("bDealStatus");
										dealDetail.vOrderNumber = json_deal
												.getString("vOrderNumber");
										arrSaving.add(dealDetail);
									}
								}
							} catch (Exception e) {
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub

							// lnLoadMore.setVisibility(View.GONE);

							topbarinterface.hideDialog();
							try {
								lstSliderSavingList.removeFooterView(views);
							} catch (Exception e) {
								// TODO: handle exception
							}

							if (!commonClass.checkInternetConnection()) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topbarinterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.InternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);
							} else {
								if (strStatusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topbarinterface
															.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								}

								else if (strStatusCode.equals("100")) {

									loadmore = true;

									if (!vTotalSavingAmount.equals("null")) {
										// Log.d("in>>", "in");
										// DecimalFormat df = new
										// DecimalFormat("#.##");
										// dTotalSavingAmount =
										// Double.valueOf(df
										// .format(dTotalSavingAmount));
										String str = String.format("%.2f",
												dTotalSavingAmount);
										// Log.d("we are in str>>", "" + str);
										txtSliderSavingAmount
												.setText(vTotalSavingAmount
														+ " " + str);

										if (lnEmptyView.getVisibility() == View.VISIBLE) {
											setblankadapter();
										}

										lstSliderSavingList
												.addFooterView(views);

										setblankadapter();
										// setblankadapter();
									}

								} else if (strStatusCode.equals("118")) {

									loadmore = false;
								} else if (strStatusCode.equals("102")) {

									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topbarinterface
															.topbarClick("back");
												}
											}, strMessageResponse);
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								}
								if (arrSaving.size() == 0) {
									set_savingdata(0);
									lnEmptyView.setVisibility(View.VISIBLE);
									lstSliderSavingList
											.setVisibility(View.GONE);

								} else {
									lnEmptyView.setVisibility(View.GONE);
									lstSliderSavingList
											.setVisibility(View.VISIBLE);
								}
							}

							// if (adapterSavingDeals.getCount() == 0) {
							// set_savingdata(0);
							// lnEmptyView.setVisibility(View.VISIBLE);
							// lstSliderSavingList.setVisibility(View.GONE);
							// }

						}
					});
			as.execute();
		}

		// arrSaving_dateshorted.clear();
		//
		// for (int i = 0; i < arrSaving.size(); i++) {
		// DealFeedDetail current = arrSaving.get(i);
		// Date current_date = null;
		//
		// SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		// try {
		// current_date = sdf.parse(current.vPurchaseDate);
		// } catch (ParseException e) {
		// e.printStackTrace();
		// }
		//
		// //
		// // if (date_startdate.compareTo(current_date) < 0
		// // && date_enddate.compareTo(current_date) > 0) {
		// // arrSaving_dateshorted.add(current);
		// //
		// // } else {
		// //
		// // }
		// //
		// // }
		// // adapterSavingDeals = new SavingDealAdapter(getActivity(),
		// // arrSaving_dateshorted);
		// //
		// // setListAdapter(adapterSavingDeals);
		//
		// if (date_startdate.compareTo(current_date) <= 0
		// && date_enddate.compareTo(current_date) >= 0) {
		// arrSaving_dateshorted.add(current);
		//
		// } else {
		//
		// }
		//
		// }
		//
		// if (arrSaving_dateshorted.size() == 0) {
		//
		// txtSliderSavingAmount.setText(vTotalSavingAmount + "" + "0");
		// lstSliderSavingList.setVisibility(View.GONE);
		// lnEmptyView.setVisibility(View.VISIBLE);
		// } else {
		//
		// Double savingamount = 0.0;
		// for (int i = 0; i < arrSaving_dateshorted.size(); i++) {
		//
		// savingamount += arrSaving_dateshorted.get(i).dSavingamount;
		//
		// }
		//
		// String str = String.format("%.2f", savingamount);
		//
		// txtSliderSavingAmount.setText(vTotalSavingAmount + "" + str);
		//
		// adapterSavingDeals = new SavingDealAdapter(getActivity(),
		// arrSaving_dateshorted, FragmentSliderSaving.this);
		// lstSliderSavingList.setAdapter(adapterSavingDeals);
		//
		// lstSliderSavingList.setVisibility(View.VISIBLE);
		// lnEmptyView.setVisibility(View.GONE);
		// }

	}

	private void setData() {

		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topbarinterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub

							if (StrPageIndex.equalsIgnoreCase("1")) {

								// topbarinterface.showDialog("Loading");
							}
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							// for (int i = 0; i < temp; i++) {
							// DealFeedDetail ds = new DealFeedDetail();
							// arrSaving.add(ds);
							// }
							String str = "";
							str = "OrderService.svc/GetCustomerSavingdetails?languageid="
									+ commonClass.languageId
									+ "&customerid="
									+ commonClass.CustomerId
									+ "&startindex="
									+ StrPageIndex
									+ "&startdate="
									+ startdate
									+ "&enddate=" + enddate;

							// Log.e("called ===URL", "" + str);
							urlResponse = web_service
									.makeServicegetRequest(str);

							// Log.e("called ===", "" + urlResponse);

							try {

								JSONObject json_Object = new JSONObject(
										urlResponse);

								strStatusCode = json_Object
										.getString("vStatusCode");

								strMessageResponse = json_Object
										.getString("vMessageResponse");

								vTotalSavingAmount = json_Object
										.getString("vTotalSavingAmount");
								dTotalSavingAmount = json_Object
										.getDouble("dTotalSavingAmount");
								Log.d("dtotal saving amount>>", ""
										+ dTotalSavingAmount);
								StrPageIndex = json_Object
										.getString("iLastIndex");

								if (strStatusCode.equals("100")) {

									JSONArray json_result = json_Object
											.getJSONArray("vObjCustomerDetails");
									vExpiredDealMessage = json_Object
											.getString("vExpiredDealMessage");

									for (int i = 0; i < json_result.length(); i++) {

										JSONObject json_deal = json_result
												.getJSONObject(i);

										DealFeedDetail dealDetail = new DealFeedDetail();

										dealDetail.iMerchantDealId = json_deal
												.getString("iMerchantDealId");
										dealDetail.DealId = dealDetail.iMerchantDealId;
										dealDetail.locationId = json_deal
												.getString("iMerchantStoreId");
										Log.e("location Id", "" + i + "==="
												+ dealDetail.locationId);
										dealDetail.vExpiredDealMessage = vExpiredDealMessage;

										// dealDetail.vMessage = json_deal
										// .getString("vMessage");

										dealDetail.StoreName = json_deal
												.getString("vStorename");

										dealDetail.dealName = json_deal
												.getString("vDealName");

										dealDetail.vPurchaseamount = json_deal
												.getString("vPurchaseamount");
										dealDetail.vOrderamount = json_deal
												.getString("vOrderamount");
										dealDetail.vSavingamount = json_deal
												.getString("vSavingamount");
										dealDetail.dSavingamount = json_deal
												.getDouble("dSavingamount");

										Log.d("dealDetail.dsaving post>>>", ""
												+ dealDetail.dSavingamount);
										dealDetail.iOrderId = json_deal
												.getString("iOrderId");

										dealDetail.vCustomerOrderStatus = json_deal
												.getString("vCustomerOrderStatus");

										dealDetail.DealOrignalPrice = dealDetail.vPurchaseamount;

										dealDetail.dealDiscountPrice = dealDetail.vOrderamount;

										dealDetail.shortDescriptionHeading = json_deal
												.getString("vShortDescriptionHeading");

										dealDetail.shortDescription = json_deal
												.getString("vDealShotDescription");

										dealDetail.vDealListImage_A_350X350 = json_deal
												.getString("vDealListImage_A_350X350");
										dealDetail.DealImageUrl = dealDetail.vDealListImage_A_350X350;
										dealDetail.vPurchaseDate = json_deal
												.getString("vPurchaseDate");
										dealDetail.bDealStatus = json_deal
												.getBoolean("bDealStatus");
										dealDetail.vOrderNumber = json_deal
												.getString("vOrderNumber");
										arrSaving.add(dealDetail);
									}
								}
							} catch (Exception e) {
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub

							// lnLoadMore.setVisibility(View.GONE);

							topbarinterface.hideDialog();
							try {
								lstSliderSavingList.removeFooterView(views);
							} catch (Exception e) {
								// TODO: handle exception
							}

							if (!commonClass.checkInternetConnection()) {

								if (arrSaving.size() != 0) {

									loadmore = false;
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topbarinterface
															.topbarClick("back");
												}
											}, getResources().getString(
													R.string.InternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);
								}
							} else {

								if (strStatusCode.equals("420")) {

									if (arrSaving.size() != 0) {

										loadmore = false;
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {

														topbarinterface
																.topbarClick("back");
													}
												},
												getResources()
														.getString(
																R.string.PoorInternetConnect));
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);
									}

								}

								else if (strStatusCode.equals("100")) {

									if (!vTotalSavingAmount.equals("null")) {
										// Log.d("in>>", "in");
										// DecimalFormat df = new
										// DecimalFormat("#.##");
										// dTotalSavingAmount =
										// Double.valueOf(df
										// .format(dTotalSavingAmount));
										String str = String.format("%.2f",
												dTotalSavingAmount);
										// Log.d("we are in str>>", "" + str);
										txtSliderSavingAmount
												.setText(vTotalSavingAmount
														+ " " + str);

										if (lnEmptyView.getVisibility() == View.VISIBLE) {
											setblankadapter();
										}

										if (Integer.parseInt(StrPageIndex) >= 11) {

											loadmore = true;

											lstSliderSavingList
													.addFooterView(views);
										}

										adapterSavingDeals
												.notifyDataSetChanged();
										// setblankadapter();
									}

								} else if (strStatusCode.equals("118")) {

									loadmore = false;
								} else if (strStatusCode.equals("102")) {

									loadmore = false;

									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topbarinterface
															.topbarClick("back");
												}
											}, strMessageResponse);
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								}
								if (arrSaving.size() == 0) {
									set_savingdata(0);
									lnEmptyView.setVisibility(View.VISIBLE);
									lstSliderSavingList
											.setVisibility(View.GONE);

								} else {
									lnEmptyView.setVisibility(View.GONE);
									lstSliderSavingList
											.setVisibility(View.VISIBLE);
								}
							}

							// if (adapterSavingDeals.getCount() == 0) {
							// set_savingdata(0);
							// lnEmptyView.setVisibility(View.VISIBLE);
							// lstSliderSavingList.setVisibility(View.GONE);
							// }

						}
					});
			as.execute();
		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topbarinterface = (topBarInteface) activity;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void setblankadapter() {
		adapterSavingDeals = new SavingDealAdapter(getActivity(), arrSaving,
				FragmentSliderSaving.this);

		lstSliderSavingList.setAdapter(adapterSavingDeals);
	}

	// TODO Auto-generated method stub

}
