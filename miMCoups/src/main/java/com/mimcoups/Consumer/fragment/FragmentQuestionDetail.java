package com.mimcoups.Consumer.fragment;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.topBarInteface;

public class FragmentQuestionDetail extends Fragment {
	private topBarInteface topbar;
	private ImageView imgQuestionsDetailBack;
	private String questionId;
	private String questionDetail;
	private CommonData commondData;
	private WebService_Call web_service = new WebService_Call();
	private String url_response = "";
	private String strQuestions = "", strAnswers = "";
	private TextView txt_question, txtAnswer;
	RelativeLayout relmainfragment_question_detail;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_question_detail,
				container, false);
		initControls(view);
		clickEvents();

		questionId = getArguments().getString("questionId");
		if (commondData.checkInternetConnection()) {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topbar.showDialog("Loading");
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topbar.hideDialog();
							if (!commondData.checkInternetConnection()) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub
												topbar.topbarClick("back");
											}
										}, getResources().getString(
												R.string.InternetConnect));
							}

							else {
								txt_question.setText(strQuestions.toString());
								txtAnswer.setText(Html.fromHtml(strAnswers
										.toString()));
							}

						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							String geturl = "CustomerService.svc/GetFAQByFAQId?languageid="
									+ commondData.languageId
									+ "&faqid="
									+ questionId;
							url_response = web_service
									.makeServicegetRequest(geturl);
							try {
								JSONObject js_obj = new JSONObject(url_response);
								strQuestions = js_obj.getString("vQuestion");
								strAnswers = js_obj.getString("vAnswer");
							} catch (Exception e) {
							}
						}
					});
			as.execute();
		} else {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
							topbar.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
		}
		return view;
	}

	private void initControls(View view) {

		relmainfragment_question_detail = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_question_detail);
		imgQuestionsDetailBack = (ImageView) view
				.findViewById(R.id.imgQuestionDetailBack);
		txtAnswer = (TextView) view.findViewById(R.id.txtAnswer);
		txt_question = (TextView) view.findViewById(R.id.txt_question);
		commondData = (CommonData) getActivity().getApplicationContext();
	}

	public void clickEvents() {

		relmainfragment_question_detail
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgQuestionsDetailBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topbar.topbarClick("back");
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topbar = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

}
