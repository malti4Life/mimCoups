package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.favouriteDealInteface;
import com.mimcoups.Consumer.inteface.favouriteLocationInteface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.FavouriteCategoryAdapter;
import com.mimcoups.Consumer.adapter.FavouriteDoneDealFeedAdapter;
import com.mimcoups.Consumer.adapter.FavouriteLocationAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.inteface.favouriteCategoryInteface;

public class FragmentSLiderFavouriteListDeelCategory extends Fragment implements
		favouriteCategoryInteface, favouriteDealInteface,
        favouriteLocationInteface {

	private String strFromFavourite;
	private ImageView imgFavouriteListDeelCategoryBack,
			imgFavouriteListDeelCategoryQuestions;
	private topBarInteface topInterface;
	private LinearLayout lnLoadMore;
	private ListView lstFavouriteListDeelCategory;
	private FavouriteDoneDealFeedAdapter doneDealAdapter;
	private FavouriteCategoryAdapter categoryAdapter;
	private FavouriteLocationAdapter locationAdapter;
	private ArrayList<DealFeedDetail> arrFavouriteDeals = new ArrayList<DealFeedDetail>();
	private ArrayList<CategoryDetail> arrCategory = new ArrayList<CategoryDetail>();
	private ArrayList<LocationList> arrLocation = new ArrayList<LocationList>();
	private TextView txtFavouriteDetailText;
	private CommonData commonClass;
	private String url_response = "", statusCode = "", messageResponse = "";
	private WebService_Call webService = new WebService_Call();
	private String Strgeturl = "";
	private favouriteCategoryInteface favouriteCategory = this;
	private favouriteLocationInteface favouriteLocation = this;
	private favouriteDealInteface favouriteDeal = this;
	public String strpageIndex = "1";
	LinearLayout txtfavlistcategorydNoData;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	private int preLast;
	private boolean loadmore = false;
	View views;

	LinearLayout relmainfragment_slider_favourite_listdeelcategory;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(
				R.layout.fragment_slider_favourite_listdeelcategory, container,
				false);
		commonClass = (CommonData) getActivity().getApplicationContext();
		strFromFavourite = this.getArguments().getString("message");
		initContorls(view);

		views = inflater.inflate(R.layout.loadmore, null);

		clickEvents();
		Load_more_Functionality();
		return view;
	}

	private void initContorls(View view) {

		relmainfragment_slider_favourite_listdeelcategory = (LinearLayout) view
				.findViewById(R.id.relmainfragment_slider_favourite_listdeelcategory);
		imgFavouriteListDeelCategoryBack = (ImageView) view
				.findViewById(R.id.imgFavouriteListDeelCategoryBack);
		imgFavouriteListDeelCategoryQuestions = (ImageView) view
				.findViewById(R.id.imgFavouriteListDeelCategoryQuestions);
		lstFavouriteListDeelCategory = (ListView) view
				.findViewById(R.id.lstFavouriteListDeelCategory);
		txtFavouriteDetailText = (TextView) view
				.findViewById(R.id.txtFavouriteDetailText);
		txtfavlistcategorydNoData = (LinearLayout) view
				.findViewById(R.id.txtfavlistcategorydNoData);
		lnLoadMore = (LinearLayout) view.findViewById(R.id.lnLoadMore);

		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			if (strFromFavourite.equals("Deal")) {
				txtFavouriteDetailText.setText(getResources().getString(
						R.string.favourite_deel));
				getAllFavouriteDeals();
			} else if (strFromFavourite.equals("Category")) {
				txtFavouriteDetailText.setText(getResources().getString(
						R.string.favourite_category));
				getAllFavouriteCategory();
			} else if (strFromFavourite.equals("Location")) {
				txtFavouriteDetailText.setText(getResources().getString(
						R.string.favourite_location));
				getAllFavouriteLocation();
			}
		}
	}

	private void clickEvents() {

		relmainfragment_slider_favourite_listdeelcategory
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgFavouriteListDeelCategoryBack
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						topInterface.topbarClick("back");
					}
				});
		imgFavouriteListDeelCategoryBack
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						topInterface.topbarClick("back");
					}
				});
		imgFavouriteListDeelCategoryQuestions
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							lastClickTime = currentTime;
							topInterface.topbarClick("questions");
						}
					}
				});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void getAllFavouriteLocation() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {
						topInterface.showDialog("Loading");
						url_response = "";
						statusCode = "";
						messageResponse = "";
						arrLocation.clear();
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						Strgeturl = "LocationService.svc/GetAllFavoriteLocations?customerid="
								+ commonClass.CustomerId
								+ "&languageid="
								+ commonClass.languageId
								+ "&latitude="
								+ commonClass.currentLatitude
								+ "&longitude="
								+ commonClass.currentLongitude;

						String temp = Strgeturl + "&startindex=" + strpageIndex;
						// Log.e("location", temp);

						temp = commonClass.Url_EncodeWithBlank(temp);
						url_response = webService.makeServicegetRequest(temp);
						try {
							JSONObject js_getres = new JSONObject(url_response);
							statusCode = js_getres.getString("vStatusCode");
							if (statusCode.equals("100")) {

								strpageIndex = js_getres
										.getString("iLastIndex");
								if (Integer.parseInt(strpageIndex) >= 10) {
									loadmore = true;
									lstFavouriteListDeelCategory
											.addFooterView(views);
								}

								JSONArray js_arr_data = js_getres
										.getJSONArray("Locations");
								for (int i = 0; i < js_arr_data.length(); i++) {
									JSONObject jsobj = js_arr_data
											.getJSONObject(i);
									LocationList locationlist = new LocationList();
									locationlist = locationlist
											.convertObjecttoJson(jsobj);
									if (locationlist != null) {
										arrLocation.add(locationlist);
									}
								}
							} else {
								messageResponse = js_getres
										.getString("vMessageResponse");
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						topInterface.hideDialog();

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (statusCode.equals("101")) {

								// txtfavlistcategorydNoData.setText(messageResponse);
								txtfavlistcategorydNoData
										.setVisibility(View.VISIBLE);
								lstFavouriteListDeelCategory
										.setVisibility(View.GONE);

							}

							else if (statusCode.equals("100")) {
								txtfavlistcategorydNoData
										.setVisibility(View.GONE);
								lstFavouriteListDeelCategory
										.setVisibility(View.VISIBLE);
								locationAdapter = new FavouriteLocationAdapter(
										getActivity(), arrLocation,
										favouriteLocation);
								lstFavouriteListDeelCategory
										.setAdapter(locationAdapter);
							} else {
								if (messageResponse.trim().length() != 0) {
									DialogFragment dsp = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													topInterface
															.topbarClick("back");
												}
											}, messageResponse);
									dsp.show(getFragmentManager(), "");
								}
							}
						}

					}
				});
		as.execute();
	}

	private void getAllFavouriteCategory() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topInterface.showDialog("Loading");
						url_response = "";
						statusCode = "";
						messageResponse = "";
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						String get_url = "CategoryService.svc/GetAllFavoriteCategories?customerid="
								+ commonClass.CustomerId
								+ "&languageid="
								+ commonClass.languageId;
						url_response = webService
								.makeServicegetRequest(get_url);
						// Log.e("called category", url_response);
						try {
							JSONObject json_result = new JSONObject(
									url_response);
							statusCode = json_result.getString("vStatusCode");
							if (statusCode.equals("100")) {
								JSONArray json_allDetails = json_result
										.getJSONArray("Categories");
								for (int i = 0; i < json_allDetails.length(); i++) {
									CategoryDetail category = new CategoryDetail();
									JSONObject json_Category = json_allDetails
											.getJSONObject(i);
									category = category
											.parseJson(json_Category);
									if (category != null) {
										arrCategory.add(category);
									}
								}
							} else {
								messageResponse = json_result
										.getString("vMessageResponse");
							}
						} catch (Exception e) {

						}
					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {

							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (statusCode.equals("101")) {

								// txtfavlistcategorydNoData.setText(messageResponse);
								txtfavlistcategorydNoData
										.setVisibility(View.VISIBLE);
								lstFavouriteListDeelCategory
										.setVisibility(View.GONE);

							}

							else if (statusCode.equals("100")) {
								txtfavlistcategorydNoData
										.setVisibility(View.GONE);
								categoryAdapter = new FavouriteCategoryAdapter(
										getActivity(), arrCategory,
										favouriteCategory);
								lstFavouriteListDeelCategory
										.setAdapter(categoryAdapter);
							} else {
								if (messageResponse.trim().length() != 0) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													// TODO Auto-generated
													// method
													// stub
													topInterface
															.topbarClick("back");
												}
											}, messageResponse);
									ds.show(getFragmentManager(), "");
								}
							}
						}

					}
				});
		as.execute();
	}

	public void getAllFavouriteDeals() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topInterface.showDialog("Loading");
						url_response = "";
						Strgeturl = "";
						statusCode = "";
					}

					@Override
					public void doInBackground() {
						// Strgeturl =
						// "DealService.svc/GetAllFavoriteDeals?customerid="
						// + commonData.CustomerId
						// + "&languageid="
						// + commonData.languageId
						// + "&latitude="
						// + commonData.currentLatitude
						// + "&longitude="
						// + commonData.currentLongitude
						// + "&startindex="
						// + strpageIndex;

						Strgeturl = "DealService.svc/GetAllFavoriteDeals?customerid="
								+ commonClass.CustomerId
								+ "&languageid="
								+ commonClass.languageId
								+ "&latitude="
								+ commonClass.currentLatitude
								+ "&longitude="
								+ commonClass.currentLongitude;

						String tempurl = Strgeturl + "&startindex="
								+ strpageIndex;
						// Log.e("Deal all", tempurl);
						url_response = webService
								.makeServicegetRequest(tempurl);
						try {
							JSONObject json_Object = new JSONObject(
									url_response);
							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							statusCode = json_Object.getString("vStatusCode");
							messageResponse = json_Object
									.getString("vMessageResponse");

							if (statusCode.equals("100")) {

								strpageIndex = json_Object
										.getString("iLastIndex");
								if (Integer.parseInt(strpageIndex) >= 11) {
									loadmore = true;

									lstFavouriteListDeelCategory
											.addFooterView(views);
								}
								arrFavouriteDeals.clear();
								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.DelFeedDetail(json_deal);
									arrFavouriteDeals.add(dealDetail);
								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {
						topInterface.hideDialog();

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (statusCode.equals("101")) {

								// txtfavlistcategorydNoData.setText(messageResponse);
								txtfavlistcategorydNoData
										.setVisibility(View.VISIBLE);
								lstFavouriteListDeelCategory
										.setVisibility(View.GONE);

							}

							else if (statusCode.equals("100")) {
								txtfavlistcategorydNoData
										.setVisibility(View.GONE);
								doneDealAdapter = new FavouriteDoneDealFeedAdapter(
										getActivity(), arrFavouriteDeals,
										favouriteDeal);
								lstFavouriteListDeelCategory
										.setAdapter(doneDealAdapter);
							} else {

								if (messageResponse.trim().length() != 0) {
									DialogFragment dsp = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													topInterface
															.topbarClick("back");
												}
											}, messageResponse);
									dsp.show(getFragmentManager(), "");
								}
							}
						}

					}
				});
		as.execute();
	}

	@Override
	public void favouriteCategoryClick(final String id,
			final CheckBox checkbox, final boolean isChecked, int position) {

		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			arrCategory.remove(position);
			categoryAdapter.notifyDataSetChanged();
			((DeelFeedActivity) getActivity()).isDealListingFavourite = true;
			if (categoryAdapter.getCount() == 0) {
				txtfavlistcategorydNoData.setVisibility(View.VISIBLE);
				lstFavouriteListDeelCategory.setVisibility(View.GONE);
			}

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							url_response = "";
							statusCode = "";
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							try {
								String getUrl = "CategoryService.svc/UpdateFavoriteCategories";
								getUrl = commonClass
										.Url_EncodeWithBlank(getUrl);
								String categoryurl = id + "|" + isChecked;
								url_response = webService
										.makeServiceFavouriteCategory(getUrl,
												categoryurl,
												commonClass.languageId,
												commonClass.CustomerId);

								JSONObject jsonObject = new JSONObject(
										url_response);
								statusCode = jsonObject
										.getString("vStatusCode");
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							checkbox.setClickable(true);

							if (statusCode.equals("100")) {

							} else {

							}
							url_response = "";
						}
					});
			as.execute();

		}
	}

	@Override
	public void favouriteLocationClick(final String id,
			final CheckBox checkbox, final boolean isChecked,
			LocationList position) {

		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			((DeelFeedActivity) getActivity()).isDealListingFavourite = true;
			arrLocation.remove(position);
			locationAdapter.notifyDataSetChanged();

			if (locationAdapter.getCount() == 0) {
				txtfavlistcategorydNoData.setVisibility(View.VISIBLE);
				lstFavouriteListDeelCategory.setVisibility(View.GONE);
			}

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							url_response = "";
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							try {
								String getUrl = "LocationService.svc/UpdateFavoriteLocations";
								getUrl = commonClass
										.Url_EncodeWithBlank(getUrl);
								String categoryurl = id + "|" + isChecked;
								url_response = webService
										.makeServiceFavouriteLocations(getUrl,
												categoryurl,
												commonClass.languageId,
												commonClass.CustomerId);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							checkbox.setClickable(true);
							url_response = "";
						}
					});
			as.execute();

		}

	}

	@Override
	public void favouriteDealClick(final String id, final CheckBox checkbox,
			final boolean isChecked, DealFeedDetail position) {

		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			((DeelFeedActivity) getActivity()).isDealListingFavourite = true;
			arrFavouriteDeals.remove(position);
			doneDealAdapter.notifyDataSetChanged();

			if (doneDealAdapter.getCount() == 0) {
				txtfavlistcategorydNoData.setVisibility(View.VISIBLE);
				lstFavouriteListDeelCategory.setVisibility(View.GONE);
			}

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							url_response = "";
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							try {

								String getUrl = "DealService.svc/UpdateFavoriteDeals";

								getUrl = commonClass
										.Url_EncodeWithBlank(getUrl);
								String categoryurl = id + "|" + isChecked;
								url_response = webService
										.makeServiceFavouriteDeals(getUrl,
												categoryurl,
												commonClass.languageId,
												commonClass.CustomerId);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							checkbox.setClickable(true);
							url_response = "";
						}
					});
			as.execute();
		}
	}

	private void Load_more_Functionality() {

		lstFavouriteListDeelCategory
				.setOnScrollListener(new OnScrollListener() {
					@Override
					public void onScrollStateChanged(AbsListView view,
							int scrollState) {
					}

					@Override
					public void onScroll(AbsListView view,
							int firstVisibleItem, int visibleItemCount,
							int totalItemCount) {
						// Log.i("First visible item>>", "" + firstVisibleItem);
						// Log.i("Visible Count>>", "" + visibleItemCount);
						// Log.i("Total Item Count>>", "" + totalItemCount);

						final int lastItem = firstVisibleItem
								+ visibleItemCount;

						if (lastItem == totalItemCount) {

							if ((preLast != lastItem) && loadmore) {

								if (!commonClass.checkInternetConnection()) {

								} else {
									// lnLoadMore.setVisibility(View.VISIBLE);
									loadmore = false;

									if (strFromFavourite.equals("Deal")) {
										// txtFavouriteDetailText.setText(getResources().getString(
										// R.string.favourite_deel));
										// getAllFavouriteDeals();

										loadmore_moredeals();
									} else if (strFromFavourite
											.equals("Category")) {
										// txtFavouriteDetailText.setText(getResources().getString(
										// R.string.favourite_category));
										// getAllFavouriteCategory();

										loadmore_Category();

									} else if (strFromFavourite
											.equals("Location")) {
										// Log.i("go to location>>", "Go to ");
										// txtFavouriteDetailText.setText(getResources().getString(
										// R.string.favourite_location));
										// getAllFavouriteLocation();

										loadmore_Location();
									}

								}
							}
						}
					}

				});

	}

	private void loadmore_moredeals() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub

						url_response = "";
						statusCode = "";
					}

					@Override
					public void doInBackground() {
						String tempurl = Strgeturl + "&startindex="
								+ strpageIndex;
						url_response = webService
								.makeServicegetRequest(tempurl);

						try {
							JSONObject json_Object = new JSONObject(
									url_response);
							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							statusCode = json_Object.getString("vStatusCode");
							messageResponse = json_Object
									.getString("vMessageResponse");

							if (statusCode.equals("100")) {
								strpageIndex = json_Object
										.getString("iLastIndex");

								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.DelFeedDetail(json_deal);
									arrFavouriteDeals.add(dealDetail);
								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {
						// lnLoadMore.setVisibility(View.GONE);
						try {
							lstFavouriteListDeelCategory
									.removeFooterView(views);
						} catch (Exception e) {
							// TODO: handle exception
						}

						if (statusCode.equals("101")) {
							loadmore = false;

							// txtfavlistcategorydNoData.setText(messageResponse);
							txtfavlistcategorydNoData
									.setVisibility(View.VISIBLE);
							lstFavouriteListDeelCategory
									.setVisibility(View.GONE);

						}

						else if (statusCode.equals("100")) {
							loadmore = true;

							lstFavouriteListDeelCategory.addFooterView(views);

							doneDealAdapter.notifyDataSetChanged();
						} else {
							// DialogFragment dsp = new SingleButtonAlert(
							// new DialogInterfaceClick() {
							//
							// @Override
							// public void dialogClick(String tag) {
							// topInterface.topbarClick("back");
							// }
							// }, messageResponse);
							// dsp.show(getFragmentManager(), "");
						}

					}
				});
		as.execute();

	}

	private void loadmore_Location() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {

						url_response = "";
						statusCode = "";
						messageResponse = "";
					}

					@Override
					public void doInBackground() {

						String tempURL = Strgeturl + "&startindex="
								+ strpageIndex;

						// Log.e("loadmmore===location", tempURL);

						tempURL = commonClass.Url_EncodeWithBlank(tempURL);

						url_response = webService
								.makeServicegetRequest(tempURL);
						try {
							JSONObject js_getres = new JSONObject(url_response);
							statusCode = js_getres.getString("vStatusCode");
							if (statusCode.equals("100")) {
								strpageIndex = js_getres
										.getString("iLastIndex");
								if (Integer.parseInt(strpageIndex) >= 11) {
									loadmore = true;
								}

								JSONArray js_arr_data = js_getres
										.getJSONArray("Locations");
								for (int i = 0; i < js_arr_data.length(); i++) {
									JSONObject jsobj = js_arr_data
											.getJSONObject(i);
									LocationList locationlist = new LocationList();
									locationlist = locationlist
											.convertObjecttoJson(jsobj);
									if (locationlist != null) {
										arrLocation.add(locationlist);
									}
								}
							} else {
								messageResponse = js_getres
										.getString("vMessageResponse");
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub

						// lnLoadMore.setVisibility(View.GONE);
						try {
							lstFavouriteListDeelCategory
									.removeFooterView(views);
						} catch (Exception e) {

						}

						if (statusCode.equals("101")) {
							loadmore = false;

							// txtfavlistcategorydNoData.setText(messageResponse);
							txtfavlistcategorydNoData
									.setVisibility(View.VISIBLE);
							lstFavouriteListDeelCategory
									.setVisibility(View.GONE);

						}

						else if (statusCode.equals("100")) {

							lstFavouriteListDeelCategory.addFooterView(views);
							txtfavlistcategorydNoData.setVisibility(View.GONE);
							lstFavouriteListDeelCategory
									.setVisibility(View.VISIBLE);
							locationAdapter.notifyDataSetChanged();
						} else {
							// DialogFragment dsp = new SingleButtonAlert(
							// new DialogInterfaceClick() {
							//
							// @Override
							// public void dialogClick(String tag) {
							// topInterface.topbarClick("back");
							// }
							// }, messageResponse);
							// dsp.show(getFragmentManager(), "");
						}
					}
				});
		as.execute();
	}

	private void loadmore_Category() {

	}

}
