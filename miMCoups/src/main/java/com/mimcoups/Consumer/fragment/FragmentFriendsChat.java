package com.mimcoups.Consumer.fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mimcoups.Consumer.customClass.ChatUser;
import com.mimcoups.Consumer.customClass.supportchat;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.ChatDetailAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.Database_Helper;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentFriendsChat extends Fragment {

	private CommonData commonClass;
	ArrayList<supportchat> arrmsg;
	ListView lstChatDetailList;
	public TextView chatdetailusername, tvsend;
	EditText edtChatDetailMessage;
	String to_idval, to_id, from_idval;
	String to_profile_img;
	SharedPreferences preferences;
	Context context;
	Handler mHandler = new Handler();
	private topBarInteface topInteface;
	ImageView imgChatDetailBack;
	LinearLayout relmainfragment_chat_detail, lnSendChat;
	public String customerNavigationType = "";
	View view;
	String sendid = "";
	ChatUser ch;
	boolean isonline = false;
	String urlResponse, strStatusCode, strMessageResponse;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		view = inflater
				.inflate(R.layout.fragment_chat_detail, container, false);
		commonClass = (CommonData) getActivity().getApplicationContext();

		if (commonClass.pendingsupportchat != null) {
			if (commonClass.pendingsupportchat.size() > 0) {
				commonClass.pendingsupportchat.clear();
			}
		}
		Initialization(view);
		OnClickEvents();
		get_chat(from_idval, to_idval);
		return view;

	}

	private void OnClickEvents() {

		edtChatDetailMessage.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				if (edtChatDetailMessage.getText().toString().trim().length() == 0) {
					tvsend.setTextColor(Color.parseColor("#909093"));

				} else {
					tvsend.setTextColor(Color.parseColor("#178ccc"));

				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		relmainfragment_chat_detail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});

		lnSendChat.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				int chatlen = edtChatDetailMessage.length();
				if (chatlen != 0) {
					if (to_id != "") {
						String chatmsg = edtChatDetailMessage.getText()
								.toString().trim();

						if (chatmsg.toString().trim().length() != 0) {

							Message msg = new Message(to_idval,
									Message.Type.chat);
							msg.setBody(chatmsg);

							if (commonClass.xmppconnection != null
									&& commonClass.xmppconnection.isConnected()
									&& commonClass.checkInternetConnection()) {

								SimpleDateFormat sdf = new SimpleDateFormat(
										"MMMM dd yyyy HH:mm:ss aa", Locale.US);
								String currentDateandTime = sdf
										.format(new Date());

								supportchat chatobj = new supportchat();
								chatobj.from_id = from_idval;
								chatobj.to_id = to_idval;
								chatobj.message = chatmsg;
								chatobj.Msg_id = msg.getPacketID();
								chatobj.flag = 0;
								chatobj.isinternet = 1;
								chatobj.time = currentDateandTime;

								arrmsg.add(chatobj);

								Database_Helper db = new Database_Helper(
										context);
								db.Open();
								db.add_Chat(chatobj);
								db.close();

								commonClass.xmppconnection.sendPacket(msg);

								Log.e("called send chat", "" + chatobj.to_id);

								ChatDetailAdapter adapter = new ChatDetailAdapter(
										context, arrmsg, to_profile_img);
								lstChatDetailList.setAdapter(adapter);
								lstChatDetailList.setSelection(adapter
										.getCount());
								edtChatDetailMessage.setText("");

								String[] fromidemail = chatobj.to_id.split("/");
								String[] usernamearr = fromidemail[0]
										.split("@");
								String username = usernamearr[0].trim();

								Roster roster = commonClass.xmppconnection
										.getRoster();
								Log.e("username", "" + username);
								Presence availability = roster
										.getPresence(username);
								Log.e("presence is>>",
										"" + availability.isAvailable());

								Log.e("daxesh presence", "" + ch.islonlie);

								Presence.Type tye = availability.getType();

								Log.e("called ", "" + tye.name());

								if (!(ch.islonlie)) {
									Log.e("Offline send data",
											"offline send notification");

									String from[] = chatobj.from_id.split("@");
									Log.e("called name", ""
											+ commonClass.ProfileName + "===="
											+ sendid);

									Send_Notifications(sendid,
											commonClass.languageId,
											commonClass.ProfileName + " : "
													+ chatmsg);

								}

							} else {

								SimpleDateFormat sdf = new SimpleDateFormat(
										"MMMM dd yyyy HH:mm:ss aa", Locale.US);
								String currentDateandTime = sdf
										.format(new Date());

								supportchat chatobj = new supportchat();
								chatobj.from_id = from_idval;
								chatobj.to_id = to_idval;
								chatobj.message = chatmsg;
								chatobj.Msg_id = msg.getPacketID();
								chatobj.flag = 0;
								chatobj.isinternet = 0;
								chatobj.time = currentDateandTime;

								arrmsg.add(chatobj);

								Database_Helper db = new Database_Helper(
										context);
								db.Open();
								db.add_Chat(chatobj);
								db.close();

								ChatDetailAdapter adapter = new ChatDetailAdapter(
										context, arrmsg, to_profile_img);
								lstChatDetailList.setAdapter(adapter);
								lstChatDetailList.setSelection(adapter
										.getCount());
								edtChatDetailMessage.setText("");
							}
						}
					}

				}
			}
		});

		imgChatDetailBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				topInteface.topbarClick("back");
			}
		});

	}

	private void Initialization(View view) {

		to_id = "";
		arrmsg = new ArrayList<supportchat>();

		relmainfragment_chat_detail = (LinearLayout) view
				.findViewById(R.id.relmainfragment_chat_detail);

		imgChatDetailBack = (ImageView) view
				.findViewById(R.id.imgChatDetailBack);
		lstChatDetailList = (ListView) view
				.findViewById(R.id.lstChatDetailList);
		chatdetailusername = (TextView) view
				.findViewById(R.id.txtChatDetailUserName);
		edtChatDetailMessage = (EditText) view
				.findViewById(R.id.edtChatDetailMessage);
		lnSendChat = (LinearLayout) view.findViewById(R.id.linearLayout2);
		tvsend = (TextView) view.findViewById(R.id.tvsend);

		ch = (ChatUser) getArguments().getSerializable("chat");

		isonline = ch.islonlie;

		sendid = ch.id;

		Log.e("User Id", sendid);

		to_id = ch.userName.toLowerCase();
		// Log.d("init>>", "init" + ch.userName.toLowerCase());
		SharedPreferences preference = getActivity().getSharedPreferences(
				"FriendList", Context.MODE_PRIVATE);
		preference.edit().putInt(to_id, 0).commit();

		// set badge count after decrease

		Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
				.findFragmentById(R.id.bottomFragment);

		SharedPreferences badgepreference = getActivity().getSharedPreferences(
				"Badgecount", Context.MODE_PRIVATE);

		int badgecountval = badgepreference.getInt("Badge", 0);

		bottomFragment.SetBadgeCount(badgecountval);

		to_profile_img = ch.profileUrl;

		preferences = PreferenceManager.getDefaultSharedPreferences(context);

		to_idval = to_id.toLowerCase() + "@"
				+ commonClass.strServerName.toLowerCase();

		from_idval = commonClass.strUsername.toLowerCase() + "@"
				+ commonClass.strServerName.toLowerCase();

		Log.e("called users", "" + to_idval + "===" + from_idval);

		commonClass.currentfriendstoid = to_id.toLowerCase();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		topInteface = (topBarInteface) activity;
		context = activity;
	}

	public void MessageReceive(supportchat chatobj) {

		if (view != null) {
			// Log.d("listview set", "lisetview set");
			lstChatDetailList = (ListView) view
					.findViewById(R.id.lstChatDetailList);
		}

		String fromid = chatobj.from_id;
		if (fromid.contains("@")) {
			String fromarr[] = fromid.split("@");
			fromid = fromarr[0];
		}

		// Log.i("after fromid", "" + fromid);
		// Log.i("after to_id>>", "" + to_id);
		if (fromid.equals(to_id)) {
			// Log.d("if", "if");
			// Log.d("arrmsg size>>", "" + arrmsg.size());
			arrmsg.add(chatobj);
			// Log.d("after arrmsg size>>", "" + arrmsg.size());
			ChatDetailAdapter adapter = new ChatDetailAdapter(context, arrmsg,
					to_profile_img);
			lstChatDetailList.setAdapter(adapter);
			// Log.d("adapter", "setadapter");
			lstChatDetailList.setSelection(adapter.getCount());
		} else {
			// Log.d("else>>", "else");
			String[] arrfrmid = chatobj.from_id.split("@");
			String username = arrfrmid[0];
			SharedPreferences preference = getActivity().getSharedPreferences(
					"FriendList", Context.MODE_PRIVATE);

			int countval = preference.getInt(username, 0);

			countval = countval + 1;

			// Log.i("COUNT>>>>>>>>>>> USERNAME", "" + username + ">>>>>"
			// + countval);
			username = username.toLowerCase();

			preference.edit().putInt(username, countval).commit();

		}
	}

	public void get_chat(String from_id, String to_id) {

		Database_Helper db = new Database_Helper(context);
		db.Open();
		Cursor cursor = db.getMessage(from_id, to_id);
		if (cursor.moveToFirst()) {
			do {

				supportchat chatmsg = new supportchat();
				chatmsg.from_id = from_id;
				chatmsg.to_id = to_id;
				chatmsg.message = cursor
						.getString(cursor.getColumnIndex("msg"));
				chatmsg.Msg_id = cursor.getString(cursor
						.getColumnIndex("Msg_id"));
				chatmsg.time = cursor.getString(cursor.getColumnIndex("time"));
				chatmsg.flag = cursor.getInt(cursor.getColumnIndex("flag"));
				arrmsg.add(chatmsg);

			} while (cursor.moveToNext());

			mHandler.post(new Runnable() {
				public void run() {
					ChatDetailAdapter adapter = new ChatDetailAdapter(context,
							arrmsg, to_profile_img);
					lstChatDetailList.setAdapter(adapter);
					lstChatDetailList.setSelection(adapter.getCount());
					edtChatDetailMessage.setText("");
				}
			});
		}

		db.close();

	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		// Log.d("on resume friends chat",
		// "on resume friends chat" + arrmsg.size());
		// Log.d("on resume call", "on resume call");
		if (commonClass.pendingsupportchat != null) {
			if (commonClass.pendingsupportchat.size() > 0) {
				ArrayList<supportchat> arrremove = new ArrayList<supportchat>();
				for (int i = 0; i < commonClass.pendingsupportchat.size(); i++) {
					// Log.d("size>>", "" +
					// commonClass.pendingsupportchat.size());
					supportchat supportchat = commonClass.pendingsupportchat
							.get(i);
					String fromid = supportchat.from_id.toLowerCase().trim();
					String to_ids = to_id.toLowerCase().trim();
					if (fromid.contains("@")) {
						String[] arrfrmid = fromid.split("@");
						fromid = arrfrmid[0];
					}
					// Log.d("fromidmessage", "" + fromid);
					// Log.d("to_id>>", "" + to_ids);

					if (fromid.equals(to_id)) {
						// Log.d("match", "match" + supportchat.message);
						arrmsg.add(supportchat);
						arrremove.add(supportchat);

						preferences.edit().putInt(fromid, 0).commit();
					}
				}
				ChatDetailAdapter adapter = new ChatDetailAdapter(context,
						arrmsg, to_profile_img);
				// lstChatDetailList.setAdapter(adapter);
				// Log.d("adapter", "setadapter" + arrmsg.size());
				// lstChatDetailList.setSelection(adapter.getCount());
				adapter.notifyDataSetChanged();

				for (int i = 0; i < arrremove.size(); i++) {
					supportchat support = arrremove.get(i);
					if (commonClass.pendingsupportchat.contains(arrremove
							.get(i))) {
						// Log.i("remove message>>", "" + support.message);
						commonClass.pendingsupportchat.remove(support);
					}
				}

				lstChatDetailList.setSelection(adapter.getCount());
			}
		}
		super.onResume();
	}

	public void Send_Notifications(final String customerid,
			final String languageid, final String message) {

		final String notiurl = "http://54.148.117.181/NotificationService.svc/SendNotificationByCustomerIdForApp";

		final WebService_Call web_service = new WebService_Call();

		final String strTempUrl = commonClass.Url_EncodeWithBlank(notiurl);
		Log.e("called webservice", strTempUrl + "===== Message  " + message);

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {

					}

					@Override
					public void doInBackground() {

						try {
							urlResponse = web_service
									.Send_Notification(strTempUrl, customerid,
											languageid, message);

							Log.e("called send notification", "" + urlResponse);

							JSONObject json_Object = new JSONObject(urlResponse);
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");
						} catch (Exception e) {
							Log.e("Exception", "" + e.getMessage());
						}
					}

					@Override
					public void onPostExecute() {
//						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
//								getActivity());
//						alertDialogBuilder.setMessage(urlResponse);
//
//						alertDialogBuilder.setPositiveButton("OK",
//								new DialogInterface.OnClickListener() {
//									@Override
//									public void onClick(DialogInterface arg0,
//											int arg1) {
//
//									}
//								});
//
//						AlertDialog alertDialog = alertDialogBuilder.create();
//						alertDialog.show();

					}
				});
		as.execute();

	}

	public void Presencechange(String username, String presence) {

		if (ch.userName.equalsIgnoreCase(username)) {
			if (presence.equals("available")) {
				ch.islonlie = true;

			}
			if (presence.equals("unavailable")) {
				ch.islonlie = false;
			}

		}

	}

}
