package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.Dialog.QuickAction;
import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.DealFeedAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;

public class FragmentDealFeed extends Fragment {

	private int preLast;
	private topBarInteface topInterface;
	private GridView lstDealFeedDeals;
	private DealFeedAdapter adapterDealFeed;
	private ArrayList<DealFeedDetail> arrDealFeeds = new ArrayList<DealFeedDetail>();
	private LinearLayout lnDealFeedFriend, lnDealFeedDate, lnDealFeedCategory,
			lnDealFeedRating, lnDealFeedLocation, lnDealFeedPrice,
			txtDeelFeedNoData, lnLoadMore;
	private ImageView imgDealFeedQuestions, imgDealFeedFriend, imgDealFeedDate,
			imgDealFeedCategory, imgDealFeedRating, imgDealFeedLocation,
			imgDealFeedPrice, imgDealFeedSearch, imgDealFeedClose;
	private TextView txtDealFeedFriend, txtDealFeedDate, txtDealFeedCategory,
			txtDealFeedRating, txtDealFeedLocation, txtDealFeedPrice,
			txtDoneDealFeed;
	private FrameLayout dealfeedbgcontainer;
	private SwipeRefreshLayout swipeLayout;
	private String strGeturl = "", strStatusCode = "", strMessageResponse,
			urlResponse = "", StrPageIndex = "1", strLastRunDate = "";
	private CommonData commonClass;
	private WebService_Call web_service = new WebService_Call();
	private ArrayList<Integer> arrselectedCategory = new ArrayList<Integer>();
	private boolean isPullToRefresh = false;
	private boolean loadmore = true;
	RelativeLayout relmainfragment_deal_feed;
	boolean isfriend = false;
	String alert_message = "";
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	TextView txtNoData;
	View views;
	Context context;
	long start = 0;
	String CategoryId;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_deal_feed, container,
				false);

		initControls(view);

		views = inflater.inflate(R.layout.loadmore, null);

		createDialog();

		Load_more_Functionality();

		// setAdapter();

		if (!commonClass.checkInternetConnection()) {

			txtNoData.setText(getActivity().getResources().getString(
					R.string.NoDataFound));
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

						}
					}, getActivity().getResources().getString(
					R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);

		} else {

			double latitude = commonClass.currentLatitude;
			double longiutde = commonClass.currentLongitude;

			ArrayList<Double> arrLatitude = ((DeelFeedActivity) getActivity())
					.getLatitudeLongitude();

			latitude = arrLatitude.get(0);
			longiutde = arrLatitude.get(1);

			strGeturl = "DealService.svc/GetAllDeals?customerid="
					+ commonClass.CustomerId + "&languageid="
					+ commonClass.languageId + "&latitude=" + latitude
					+ "&longitude=" + longiutde;

			loadDeals();
		}
		return view;
	}

	private void Load_more_Functionality() {

		lstDealFeedDeals.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
								 int visibleItemCount, int totalItemCount) {

				final int lastItem = firstVisibleItem + visibleItemCount;
				if (lastItem == totalItemCount) {

					if ((preLast != lastItem) && loadmore) {

						if (!commonClass.checkInternetConnection()) {
							// DialogFragment ds = new SingleButtonAlert(
							// new DialogInterfaceClick() {
							//
							// @Override
							// public void dialogClick(String tag) {
							//
							// topInterface.topbarClick("back");
							// }
							// }, getResources().getString(
							// R.string.InternetConnect));
							// ds.show(getFragmentManager(), "");
						} else {

							// lnLoadMore.setVisibility(View.VISIBLE);

							loadmore = false;

							loadmore_moredeals();
						}
					}
				}
			}
		});

	}

	@SuppressLint("NewApi")
	private void initControls(final View view) {
		commonClass = (CommonData) getActivity().getApplicationContext();
		relmainfragment_deal_feed = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_deal_feed);
		imgDealFeedClose = (ImageView) view.findViewById(R.id.imgDealFeedClose);
		txtDeelFeedNoData = (LinearLayout) view
				.findViewById(R.id.txtReviewsNoData);
		lnDealFeedFriend = (LinearLayout) view
				.findViewById(R.id.lnDealFeedFriend);
		lnDealFeedDate = (LinearLayout) view.findViewById(R.id.lnDealFeedDate);
		lnDealFeedCategory = (LinearLayout) view
				.findViewById(R.id.lnDealFeedCategory);
		lnDealFeedRating = (LinearLayout) view
				.findViewById(R.id.lnDealFeedRating);
		lnDealFeedLocation = (LinearLayout) view
				.findViewById(R.id.lnDealFeedLocation);
		lnDealFeedPrice = (LinearLayout) view
				.findViewById(R.id.lnDealFeedPrice);
		lnLoadMore = (LinearLayout) view.findViewById(R.id.lnLoadMore);

		imgDealFeedQuestions = (ImageView) view
				.findViewById(R.id.imgDealFeedQuestions);
		imgDealFeedSearch = (ImageView) view
				.findViewById(R.id.imgDealFeedSearch);
		lstDealFeedDeals = (GridView) view.findViewById(R.id.lstDealFeedDealsGV);
		// image view android
		imgDealFeedFriend = (ImageView) view
				.findViewById(R.id.imgDealFeedFriend);

		imgDealFeedDate = (ImageView) view.findViewById(R.id.imgDealFeedDate);
		imgDealFeedCategory = (ImageView) view
				.findViewById(R.id.imgDealFeedCategory);
		imgDealFeedRating = (ImageView) view
				.findViewById(R.id.imgDealFeedRating);
		imgDealFeedLocation = (ImageView) view
				.findViewById(R.id.imgDealFeedLocation);
		imgDealFeedPrice = (ImageView) view.findViewById(R.id.imgDealFeedPrice);

		txtDealFeedFriend = (TextView) view
				.findViewById(R.id.txtDealFeedFriend);
		txtDealFeedDate = (TextView) view.findViewById(R.id.txtDealFeedDate);
		txtDealFeedCategory = (TextView) view
				.findViewById(R.id.txtDealFeedCategory);
		txtDealFeedRating = (TextView) view
				.findViewById(R.id.txtDealFeedRating);
		txtDealFeedLocation = (TextView) view
				.findViewById(R.id.txtDealFeedLocation);
		txtDealFeedPrice = (TextView) view.findViewById(R.id.txtDealFeedPrice);
		txtDoneDealFeed = (TextView) view.findViewById(R.id.txtDoneDealFeed);
		dealfeedbgcontainer = (FrameLayout) view
				.findViewById(R.id.dealfreedbgcontainer);
		txtNoData = (TextView) view.findViewById(R.id.txtNoData);

		swipeLayout = (SwipeRefreshLayout) view
				.findViewById(R.id.swipe_container);

		// try {
		// // Set the internal trigger distance using reflection.
		// Field field =
		// SwipeRefreshLayout.class.getDeclaredField("mDistanceToTriggerSync");
		// field.setAccessible(true);
		// field.setFloat(swipeLayout, 0);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		swipeLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				swipeLayout.setRefreshing(true);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {

						swipeLayout.setRefreshing(false);
						if (commonClass.checkInternetConnection()) {

							if (isPullToRefresh) {
								strGeturl = "DealService.svc/GetAllDeals?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ commonClass.currentLatitude
										+ "&longitude="
										+ commonClass.currentLongitude;
								strGeturl = commonClass
										.Url_EncodeWithBlank(strGeturl);
								pullToRefreshDeals();
							}
						}

					}
				}, 1000);
			}
		});

		swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		relmainfragment_deal_feed.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});

		imgDealFeedClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!commonClass.checkInternetConnection()) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									topInterface.topbarClick("back");
								}
							}, getActivity().getResources().getString(
							R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);

				} else {

					try {
//						lstDealFeedDeals.removeFooterView(views);
						view.setVisibility(View.GONE);
					} catch (Exception e) {
						// TODO: handle exception
					}
					loadmore = true;

					imgDealFeedClose.setVisibility(View.GONE);
					imgDealFeedQuestions.setVisibility(View.VISIBLE);
					txtDoneDealFeed.setText(getResources().getString(
							R.string.dealfeedname));
					StrPageIndex = "1";
					strGeturl = "DealService.svc/GetAllDeals?customerid="
							+ commonClass.CustomerId + "&languageid="
							+ commonClass.languageId + "&latitude="
							+ commonClass.currentLatitude + "&longitude="
							+ commonClass.currentLongitude;
					clearBlackImages();
					// setAdapter();
					isPullToRefresh = true;
					strLastRunDate = "";
					swipeLayout.setEnabled(true);
					loadDeals();

				}

			}
		});
	}

	private void createDialog() {
		final QuickAction mQuickAction = new QuickAction(getActivity());
		mQuickAction
				.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
					@Override
					public void onItemClick(int id, int pos, String title) {

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub

										}
									}, getActivity().getResources().getString(
									R.string.InternetConnect));

							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							imgDealFeedQuestions.setVisibility(View.GONE);
							imgDealFeedClose.setVisibility(View.VISIBLE);
							txtDoneDealFeed.setText("" + title);
							arrselectedCategory.clear();
							isPullToRefresh = false;
							if (id == 1) {
								// friend detal
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByFriend?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ commonClass.currentLatitude
										+ "&longitude="
										+ commonClass.currentLongitude
										+ "&mode=" + pos;
								Log.d("strgeturl", "" + strGeturl);
								loadmore = true;
								swipeLayout.setEnabled(false);
								loadDeals();
								arrselectedCategory
										.add(R.drawable.icon_topbar_friend_selected);
							} else if (id == 2) {
								// Date wise sorting
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByDate?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ commonClass.currentLatitude
										+ "&longitude="
										+ commonClass.currentLongitude
										+ "&mode=" + pos;

								Log.d("strgeturl", "" + strGeturl);
								loadmore = true;
								swipeLayout.setEnabled(false);
								loadDeals();
								arrselectedCategory
										.add(R.drawable.icon_topbar_date_selected);
							} else if (id == 3) {
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByCategory?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&categoryid="
										+ pos
										+ "&latitude="
										+ commonClass.currentLatitude
										+ "&longitude="
										+ commonClass.currentLongitude;

								CategoryId = "" + pos;
								Log.d("strgeturl", "" + strGeturl);
								loadmore = true;
								swipeLayout.setEnabled(false);
								loadDeals();
								arrselectedCategory
										.add(R.drawable.icon_topbar_category_selected);
							} else if (id == 4) {
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByRating?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ commonClass.currentLatitude
										+ "&longitude="
										+ commonClass.currentLongitude
										+ "&mode=" + pos;
								Log.d("strgeturl", "" + strGeturl);
								// Log.e("called rating", strGeturl);
								loadmore = true;
								swipeLayout.setEnabled(false);
								loadDeals();
								arrselectedCategory
										.add(R.drawable.icon_topbar_rating_selected);
							} else if (id == 5) {
								// distancte
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByDistance?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ commonClass.currentLatitude
										+ "&longitude="
										+ commonClass.currentLongitude
										+ "&mode=" + pos;
								Log.d("strgeturl", "" + strGeturl);
								loadmore = true;
								swipeLayout.setEnabled(false);
								loadDeals();
								arrselectedCategory
										.add(R.drawable.icon_topbar_location_selected);
							} else if (id == 6) {
								StrPageIndex = "1";
								strGeturl = "DealService.svc/GetAllDealsByPrice?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId
										+ "&latitude="
										+ commonClass.currentLatitude
										+ "&longitude="
										+ commonClass.currentLongitude
										+ "&mode=" + pos;
								Log.d("strgeturl", "" + strGeturl);
								loadmore = true;
								swipeLayout.setEnabled(false);
								loadDeals();
								arrselectedCategory
										.add(R.drawable.icon_topbar_price_selected);
							}
						}
					}
				});

		mQuickAction.setOnDismissListener(new OnDismissListener() {

			@Override
			public void onDismiss() {
				// TODO Auto-generated method stub
				topbarblackImages();
			}
		});
		imgDealFeedSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("search");
			}
		});
		imgDealFeedQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					topInterface.topbarClick("questions");
				}
			}
		});
		lnDealFeedFriend.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub

				if (commonClass.checkInternetConnection()) {
					clearBlackImages();
					imgDealFeedFriend
							.setImageResource(R.drawable.icon_topbar_friend_selected);
					txtDealFeedFriend.setTextColor(Color.WHITE);
					dealfeedbgcontainer.setVisibility(View.VISIBLE);
					dealfeedbgcontainer.getBackground().setAlpha(160);
					try {
//						lstDealFeedDeals.removeFooterView(views);
						views.setVisibility(View.GONE);
					} catch (Exception e) {
						// TODO: handle exception
					}

					mQuickAction.show(view, 1);
				} else {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getActivity().getResources().getString(
							R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				}

			}
		});
		lnDealFeedDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				if (commonClass.checkInternetConnection()) {
					clearBlackImages();
					imgDealFeedDate
							.setImageResource(R.drawable.icon_topbar_date_selected);
					txtDealFeedDate.setTextColor(Color.WHITE);
					dealfeedbgcontainer.setVisibility(View.VISIBLE);
					dealfeedbgcontainer.getBackground().setAlpha(160);
					try {
//						lstDealFeedDeals.removeFooterView(views);
						views.setVisibility(View.GONE);
					} catch (Exception e) {
						// TODO: handle exception
					}
					mQuickAction.show(view, 2);

				} else {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getActivity().getResources().getString(
							R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				}

			}
		});
		lnDealFeedCategory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				if (commonClass.checkInternetConnection()) {
					int size = ((DeelFeedActivity) context).arrCategory.size();

					if (size > 0) {
						clearBlackImages();
						imgDealFeedCategory
								.setImageResource(R.drawable.icon_topbar_category_selected);
						txtDealFeedCategory.setTextColor(Color.WHITE);
						dealfeedbgcontainer.setVisibility(View.VISIBLE);
						dealfeedbgcontainer.getBackground().setAlpha(160);
						try {
//							lstDealFeedDeals.removeFooterView(views);
							views.setVisibility(View.GONE);
						} catch (Exception e) {
							// TODO: handle exception
						}

						mQuickAction.show(view, 3);
					} else {
						getCategories(mQuickAction, view);
					}
				} else {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getActivity().getResources().getString(
							R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				}
			}
		});
		lnDealFeedRating.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				if (commonClass.checkInternetConnection()) {
					clearBlackImages();
					imgDealFeedRating
							.setImageResource(R.drawable.icon_topbar_rating_selected);
					txtDealFeedRating.setTextColor(Color.WHITE);
					dealfeedbgcontainer.setVisibility(View.VISIBLE);
					dealfeedbgcontainer.getBackground().setAlpha(160);
					try {
//						lstDealFeedDeals.removeFooterView(views);
						views.setVisibility(View.GONE);
					} catch (Exception e) {
						// TODO: handle exception
					}
					mQuickAction.show(view, 4);
				} else {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getResources().getString(
							R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				}

			}
		});
		lnDealFeedLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub

				if (commonClass.checkInternetConnection()) {
					clearBlackImages();
					imgDealFeedLocation
							.setImageResource(R.drawable.icon_topbar_location_selected);
					txtDealFeedLocation.setTextColor(Color.WHITE);
					dealfeedbgcontainer.setVisibility(View.VISIBLE);
					dealfeedbgcontainer.getBackground().setAlpha(160);
					try {
//						lstDealFeedDeals.removeFooterView(views);
						views.setVisibility(View.GONE);
					} catch (Exception e) {
						// TODO: handle exception
					}
					mQuickAction.show(view, 5);
				} else {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getActivity().getResources().getString(
							R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				}

			}
		});
		lnDealFeedPrice.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				if (commonClass.checkInternetConnection()) {

					clearBlackImages();
					imgDealFeedPrice
							.setImageResource(R.drawable.icon_topbar_price_selected);
					txtDealFeedPrice.setTextColor(Color.WHITE);
					dealfeedbgcontainer.setVisibility(View.VISIBLE);
					dealfeedbgcontainer.getBackground().setAlpha(160);
					try {
//						lstDealFeedDeals.removeFooterView(views);
						views.setVisibility(View.GONE);
					} catch (Exception e) {
						// TODO: handle exception
					}
					mQuickAction.show(view, 6);
				} else {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getActivity().getResources().getString(
							R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				}

			}
		});
	}

	protected void topbarblackImages() {
		// TODO Auto-generated method stub
		dealfeedbgcontainer.setVisibility(View.GONE);
		if (arrselectedCategory.size() > 0) {

			dealfeedbgcontainer.setVisibility(View.GONE);
			if (arrselectedCategory.get(arrselectedCategory.size() - 1) == R.drawable.icon_topbar_friend_selected) {
				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend_selected);
				txtDealFeedFriend.setTextColor(Color.WHITE);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);

			} else if (arrselectedCategory.get(arrselectedCategory.size() - 1) == R.drawable.icon_topbar_date_selected) {

				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate
						.setImageResource(R.drawable.icon_topbar_date_selected);
				txtDealFeedDate.setTextColor(Color.WHITE);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);
			} else if (arrselectedCategory.get(arrselectedCategory.size() - 1) == R.drawable.icon_topbar_category_selected) {

				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);
				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category_selected);
				txtDealFeedCategory.setTextColor(Color.WHITE);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);
			} else if (arrselectedCategory.get(arrselectedCategory.size() - 1) == R.drawable.icon_topbar_rating_selected) {

				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating_selected);
				txtDealFeedRating.setTextColor(Color.WHITE);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);
			} else if (arrselectedCategory.get(arrselectedCategory.size() - 1) == R.drawable.icon_topbar_price_selected) {

				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedPrice
						.setImageResource(R.drawable.icon_topbar_price_selected);
				txtDealFeedPrice.setTextColor(Color.WHITE);
				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location);
				txtDealFeedLocation.setTextColor(Color.BLACK);

			} else if (arrselectedCategory.get(arrselectedCategory.size() - 1) == R.drawable.icon_topbar_location_selected) {

				imgDealFeedFriend
						.setImageResource(R.drawable.icon_topbar_friend);
				txtDealFeedFriend.setTextColor(Color.BLACK);

				imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
				txtDealFeedDate.setTextColor(Color.BLACK);

				imgDealFeedCategory
						.setImageResource(R.drawable.icon_topbar_category);
				txtDealFeedCategory.setTextColor(Color.BLACK);

				imgDealFeedRating
						.setImageResource(R.drawable.icon_topbar_rating);
				txtDealFeedRating.setTextColor(Color.BLACK);

				imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
				txtDealFeedPrice.setTextColor(Color.BLACK);

				imgDealFeedLocation
						.setImageResource(R.drawable.icon_topbar_location_selected);
				txtDealFeedLocation.setTextColor(Color.WHITE);
			}
		} else {
			clearBlackImages();
		}

	}

	public void clearBlackImages() {

		imgDealFeedFriend.setImageResource(R.drawable.icon_topbar_friend);
		txtDealFeedFriend.setTextColor(Color.BLACK);

		imgDealFeedDate.setImageResource(R.drawable.icon_topbar_date);
		txtDealFeedDate.setTextColor(Color.BLACK);

		imgDealFeedCategory.setImageResource(R.drawable.icon_topbar_category);
		txtDealFeedCategory.setTextColor(Color.BLACK);

		imgDealFeedRating.setImageResource(R.drawable.icon_topbar_rating);
		txtDealFeedRating.setTextColor(Color.BLACK);

		imgDealFeedLocation.setImageResource(R.drawable.icon_topbar_location);
		txtDealFeedLocation.setTextColor(Color.BLACK);

		imgDealFeedPrice.setImageResource(R.drawable.icon_topbar_price);
		txtDealFeedPrice.setTextColor(Color.BLACK);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
		this.context = activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void loadDeals() {
		arrDealFeeds.clear();
		String str = strGeturl + "&startindex=" + StrPageIndex
				+ "&dLastRunDate=" + strLastRunDate;

		final String strTempUrl = commonClass.Url_EncodeWithBlank(str);
		Log.e("called webservice", strTempUrl);

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {

						if (!DeelFeedActivity.progressbarshowing) {
							topInterface.showDialog("Loading");
						}

						start = System.currentTimeMillis();

					}

					@Override
					public void doInBackground() {

						try {
							urlResponse = web_service
									.makeServicegetRequest(strTempUrl);

							JSONObject json_Object = new JSONObject(urlResponse);
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");
							if (strStatusCode.equals("100")) {

								JSONArray json_result = json_Object
										.getJSONArray("Deals");
								loadmore = true;
								StrPageIndex = json_Object
										.getString("iLastIndex");
								strLastRunDate = json_Object
										.getString("dLastRunDate");
								arrDealFeeds.clear();
								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.DelFeedDetail(json_deal);

									if (strTempUrl
											.contains("GetAllDealsByCategory")) {
										dealDetail.categoryId = CategoryId;
									}
									arrDealFeeds.add(dealDetail);
								}
							} else if (strStatusCode.equals("101")) {

								if (json_Object.has("Types")) {

									if (!json_Object.getBoolean("Types")) {

										isfriend = true;

										JSONArray json_result = json_Object
												.getJSONArray("Deals");
										StrPageIndex = json_Object
												.getString("iLastIndex");
										alert_message = json_Object
												.getString("AlertMessage");

										strLastRunDate = json_Object
												.getString("dLastRunDate");

										arrDealFeeds.clear();

										for (int i = 0; i < json_result
												.length(); i++) {
											JSONObject json_deal = json_result
													.getJSONObject(i);
											DealFeedDetail dealDetail = new DealFeedDetail();
											dealDetail = dealDetail
													.DelFeedDetail(json_deal);
											arrDealFeeds.add(dealDetail);

										}

									}
								} else {

									isfriend = false;

								}

								Log.e("called statuscode is false", "Code"
										+ strStatusCode + "===" + isfriend);
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {

						long elapsedTime = System.currentTimeMillis() - start;

						Log.e("Total time", "" + elapsedTime);

						try {

							if (!commonClass.checkInternetConnection()) {
								topInterface.hideDialog();
								txtNoData.setText(getActivity().getResources()
										.getString(R.string.NoDataFound));
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

											}
										},
										getActivity().getResources().getString(
												R.string.InternetConnect));
								ds.show(getActivity().getFragmentManager(), "");
								ds.setCancelable(false);

							} else {
								if (strStatusCode.equals("420")) {
									topInterface.hideDialog();
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													// topInterface
													// .topbarClick("back");
												}
											},
											getActivity()
													.getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getActivity().getFragmentManager(),
											"");
									ds.setCancelable(false);

								} else if (strStatusCode.equals("101")) {
									topInterface.hideDialog();

									// txtDeelFeedNoData.setText(strMessageResponse);

									if (!isfriend) {
										swipeLayout.setVisibility(View.GONE);
										lstDealFeedDeals
												.setVisibility(View.GONE);
										txtNoData
												.setText(getResources()
														.getString(
																R.string.NoDataFound));
										txtDeelFeedNoData
												.setVisibility(View.VISIBLE);
									} else {

										swipeLayout.setVisibility(View.VISIBLE);
										lstDealFeedDeals
												.setVisibility(View.VISIBLE);
										txtDeelFeedNoData
												.setVisibility(View.GONE);
										try {
//											lstDealFeedDeals.addFooterView(views);
											views.setVisibility(View.VISIBLE);
										} catch (Exception e) {
											// TODO: handle exception
										}

										adapterDealFeed.notifyDataSetChanged();

										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {

													}
												}, alert_message);
										ds.show(getActivity()
												.getFragmentManager(), "");
										ds.setCancelable(false);

									}

								} else if (strStatusCode.equals("118")) {
									topInterface.hideDialog();

								} else if (strStatusCode.equals("100")) {
									// strGeturl = strGeturl + "&startindex="
									// + StrPageIndex + "&dLastRunDate="
									// + strLastRunDate;

									isPullToRefresh = true;
									strGeturl = commonClass
											.Url_EncodeWithBlank(strGeturl);

									// TODO Auto-generated method stub

									topInterface.hideDialog();
									// ((DeelFeedActivity) getActivity())
									// .initopenfireconnection();
									swipeLayout.setVisibility(View.VISIBLE);
									lstDealFeedDeals
											.setVisibility(View.VISIBLE);
									txtDeelFeedNoData.setVisibility(View.GONE);

//									if (lstDealFeedDeals.getFooterViewsCount() == 0)
//										try {
//											lstDealFeedDeals
//													.addFooterView(views);
//										} catch (Exception e) {
//											// TODO: handle exception
//										}

									setAdapter();

								}
							}
						} catch (Exception e) {
							// TODO: handle exception
						}

					}
				});
		as.execute();
	}

	private void pullToRefreshDeals() {
		final String strTempUrl = commonClass.Url_EncodeWithBlank(strGeturl
				+ "&startindex=1" + "&dLastRunDate=" + strLastRunDate);

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {
					}

					@Override
					public void doInBackground() {

						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);

						try {
							JSONObject json_Object = new JSONObject(urlResponse);
							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");
							if (strStatusCode.equals("100")) {
								StrPageIndex = json_Object
										.getString("iLastIndex");
								strLastRunDate = json_Object
										.getString("dLastRunDate");
								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail = dealDetail
											.DelFeedDetail(json_deal);
									arrDealFeeds.add(0, dealDetail);
								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {

						if (strStatusCode.equals("101")) {
							// txtDeelFeedNoData.setText(strMessageResponse);
							swipeLayout.setVisibility(View.GONE);
							lstDealFeedDeals.setVisibility(View.GONE);
							txtNoData.setText(getActivity().getResources()
									.getString(R.string.NoDataFound));
							txtDeelFeedNoData.setVisibility(View.VISIBLE);
						} else if (strStatusCode.equals("118")) {

						} else {
							strGeturl = strGeturl + "&startindex="
									+ StrPageIndex + "&dLastRunDate="
									+ strLastRunDate;
							strGeturl = commonClass
									.Url_EncodeWithBlank(strGeturl);
							swipeLayout.setVisibility(View.VISIBLE);
							lstDealFeedDeals.setVisibility(View.VISIBLE);
							txtDeelFeedNoData.setVisibility(View.GONE);
							adapterDealFeed.notifyDataSetChanged();
						}
					}
				});

		as.execute();
	}

	public void setAdapter() {
		adapterDealFeed = new DealFeedAdapter(getActivity(), arrDealFeeds);
		lstDealFeedDeals.setAdapter(adapterDealFeed);
	}

	private void loadmore_moredeals() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					String strTempUrl = strGeturl + "&startindex="
							+ StrPageIndex;

					// dealservice.svc/GetAllDeals?customerid=17&languageid=1&latitude=23.0&longitude=72.0&startindex=1&dLastRunDate=

					@Override
					public void onPreExecute() {

					}

					@Override
					public void doInBackground() {

						//
						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);

						try {

							JSONObject json_Object = new JSONObject(urlResponse);
							JSONArray json_result = json_Object
									.getJSONArray("Deals");
							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");
							if (strStatusCode.equals("100")) {

								StrPageIndex = json_Object
										.getString("iLastIndex");

								strLastRunDate = json_Object
										.getString("dLastRunDate");

								if (!StrPageIndex.equals("0")) {

									for (int i = 0; i < json_result.length(); i++) {
										JSONObject json_deal = json_result
												.getJSONObject(i);
										DealFeedDetail dealDetail = new DealFeedDetail();
										dealDetail = dealDetail
												.DelFeedDetail(json_deal);

										if (strTempUrl
												.contains("GetAllDealsByCategory")) {
											dealDetail.categoryId = CategoryId;
										}
										arrDealFeeds.add(dealDetail);
									}
								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {

						// lnLoadMore.setVisibility(View.GONE);
						try {
//							lstDealFeedDeals.removeFooterView(views);
							views.setVisibility(View.GONE);
						} catch (Exception e) {
							// TODO: handle exception
						}

						if (strStatusCode.equals("101")) {
							// txtDeelFeedNoData.setText(strMessageResponse);
							swipeLayout.setVisibility(View.GONE);
							lstDealFeedDeals.setVisibility(View.GONE);
							txtDeelFeedNoData.setVisibility(View.VISIBLE);
							txtNoData.setText(getActivity().getResources()
									.getString(R.string.NoDataFound));
							preLast = 0;
						} else if (strStatusCode.equals("118")) {

						} else {

							loadmore = true;
							strGeturl = commonClass
									.Url_EncodeWithBlank(strGeturl);
							swipeLayout.setVisibility(View.VISIBLE);
							lstDealFeedDeals.setVisibility(View.VISIBLE);
							txtDeelFeedNoData.setVisibility(View.GONE);
							adapterDealFeed.notifyDataSetChanged();

//							lstDealFeedDeals.addFooterView(views);
							views.setVisibility(View.VISIBLE);
						}

					}
				});
		as.execute();

	}

	public void getCategories(final QuickAction mquick, final View view) {
		if (commonClass.checkInternetConnection()) {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						private String url_response = "", strStatusCode = "",
								strMessageResponse = "";

						@Override
						public void onPreExecute() {
							if (!DeelFeedActivity.progressbarshowing) {
								topInterface.showDialog("Loading");
							}
						}

						@Override
						public void doInBackground() {
							((DeelFeedActivity) context).arrCategory = new ArrayList<CategoryDetail>();
							String get_url = "CategoryService.svc/GetAllCategories?customerid="
									+ commonClass.CustomerId
									+ "&languageid="
									+ commonClass.languageId;
							url_response = web_service
									.makeServicegetRequest(get_url);

							try {
								JSONObject json_result = new JSONObject(
										url_response);
								strStatusCode = json_result
										.getString("vStatusCode");
								strMessageResponse = json_result
										.getString("vMessageResponse");
								JSONArray json_allDetails = json_result
										.getJSONArray("Categories");
								for (int i = 0; i < json_allDetails.length(); i++) {
									CategoryDetail category = new CategoryDetail();
									JSONObject json_Category = json_allDetails
											.getJSONObject(i);
									category = category
											.parseJson(json_Category);
									if (category != null) {
										((DeelFeedActivity) context).arrCategory
												.add(category);
									}
								}
							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							topInterface.hideDialog();

							if (!(commonClass.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub

											}
										},
										getActivity().getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							} else {
								if (strStatusCode.equals("100")) {
									clearBlackImages();
									imgDealFeedCategory
											.setImageResource(R.drawable.icon_topbar_category_selected);
									txtDealFeedCategory
											.setTextColor(Color.WHITE);
									dealfeedbgcontainer
											.setVisibility(View.VISIBLE);
									dealfeedbgcontainer.getBackground()
											.setAlpha(160);

									mquick.show(view, 3);
								} else {

									if (!strMessageResponse.equals("")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {

													}
												}, strMessageResponse);
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);
									}

								}
							}

						}
					});
			as.execute();
		}
	}

}