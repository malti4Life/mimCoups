package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.locationmapinterface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.LocationListAdpater;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentSliderLocations extends Fragment {

	private ImageView imgLocationListSearch, imgLocationListMap,
			imgLocationListNearBy;
	private ListView lstLocationList;
	private topBarInteface topinteface;
	private LocationListAdpater adapterLocationList;
	private String url_response = "";
	private LocationList locationlist;
	private WebService_Call call_webservice_SliderLocation = new WebService_Call();
	private SwipeRefreshLayout swipeLayout;
	private ArrayList<LocationList> arrLocationList = new ArrayList<LocationList>();
	private String StatusCode = "", messageResponse = "";
	private LinearLayout txtLocationNoData, lnLoadMore;
	private boolean isadapterfirsstitme = true;
	private String strPageIndex = "1", strLastDate = "";
	private CommonData commonData;
	private boolean loadmore = false;
	private int preLast;
	private locationmapinterface locationinterface;
	RelativeLayout relmainfragment_slider_locations;
	long MIN_CLICK_INTERVAL = 1000; // in millis
	long lastClickTime = 0;
	CommonData commonClass;
	View views;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_locations,
				container, false);
		initControls(view);
		views = inflater.inflate(R.layout.loadmore, null);

		lstLocationList.addFooterView(views);
		clickEvents();
		listview_Functionality();
		if (!commonData.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							topinteface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			getData();
		}
		return view;
	}

	private void initControls(View view) {

		relmainfragment_slider_locations = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_locations);

		txtLocationNoData = (LinearLayout) view
				.findViewById(R.id.txtLocationNoData);
		commonData = (CommonData) getActivity().getApplicationContext();
		imgLocationListSearch = (ImageView) view
				.findViewById(R.id.imgLocationListSearch);
		imgLocationListNearBy = (ImageView) view
				.findViewById(R.id.imgLocationListNearBy);
		imgLocationListMap = (ImageView) view
				.findViewById(R.id.imgLocationListMap);
		lstLocationList = (ListView) view.findViewById(R.id.lstLocationsList);
		lnLoadMore = (LinearLayout) view.findViewById(R.id.lnLoadMore);

		swipeLayout = (SwipeRefreshLayout) view
				.findViewById(R.id.swipe_container_lstlocation);
		swipeLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub
				swipeLayout.setRefreshing(false);

			}
		});
		swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
				android.R.color.holo_green_light,
				android.R.color.holo_orange_light,
				android.R.color.holo_red_light);

		//
	}

	private void clickEvents() {

		relmainfragment_slider_locations
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgLocationListSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topinteface.topbarClick("search");
			}
		});
		imgLocationListNearBy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getData();
			}
		});
		imgLocationListMap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Log.d("arrlocation list size>>", "arrlocation size"
				// + arrLocationList.size());
				if (arrLocationList.size() > 0) {
					topinteface.showDialog("Loading");
					Handler mhandler = new Handler();
					mhandler.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							// topinteface.hideDialog();
							locationinterface
									.go_to_map_screen_withlist(arrLocationList);
							topinteface.hideDialog();
						}
					}, 3000);

				} else {
					Log.d("click event ", "click event");
				}

			}
		});
	}

	private void getData() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topinteface.showDialog("Loading");

					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub

						String serverUrl = "LocationService.svc/GetAllLocations?customerid="
								+ commonData.CustomerId
								+ "&languageid="
								+ commonData.languageId
								+ "&latitude="
								+ commonData.currentLatitude
								+ "&longitude="
								+ commonData.currentLongitude
								+ "&startindex="
								+ strPageIndex + "&dLastRunDate=" + strLastDate;

						serverUrl = commonData.Url_EncodeWithBlank(serverUrl);
						Log.e("Location list", serverUrl);
						url_response = call_webservice_SliderLocation
								.makeServicegetRequest(serverUrl);
						try {
							JSONObject js_getres = new JSONObject(url_response);
							StatusCode = js_getres.getString("vStatusCode");
							if (StatusCode.equals("100")) {
								JSONArray js_arr_data = js_getres
										.getJSONArray("Locations");
								strPageIndex = js_getres
										.getString("iLastIndex");
								strLastDate = js_getres
										.getString("dLastRunDate");
								for (int i = 0; i < js_arr_data.length(); i++) {
									JSONObject jsobj = js_arr_data
											.getJSONObject(i);
									locationlist = new LocationList();
									locationlist = locationlist
											.convertObjecttoJson(jsobj);
									if (locationlist != null) {
										arrLocationList.add(locationlist);
									}
								}
							} else {
								messageResponse = js_getres
										.getString("vMessageResponse");
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						try {

							lstLocationList.removeFooterView(views);
						} catch (Exception e) {
							// TODO: handle exception
						}

						topinteface.hideDialog();
						if (!commonData.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											topinteface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							if (StatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topinteface.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (StatusCode.equals("100")) {

								if (Integer.parseInt(strPageIndex) < 11) {
									loadmore = false;
								} else {

									lstLocationList.addFooterView(views);
									loadmore = true;
								}

								swipeLayout.setVisibility(View.VISIBLE);
								lstLocationList.setVisibility(View.VISIBLE);
								txtLocationNoData.setVisibility(View.GONE);
								adapterLocationList = new LocationListAdpater(
										getActivity(), arrLocationList);
								lstLocationList.setAdapter(adapterLocationList);
							} else if (StatusCode.equals("101")) {
								// txtLocationNoData.setText(messageResponse);
								swipeLayout.setVisibility(View.GONE);
								lstLocationList.setVisibility(View.GONE);
								txtLocationNoData.setVisibility(View.VISIBLE);

							} else if (StatusCode.equals("118")) {

							} else {
								if (messageResponse.trim().length() != 0) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													// TODO Auto-generated
													// method
													// stub

												}
											}, messageResponse);
									ds.show(getFragmentManager(), "");
								}
							}
						}

					}
				});
		as.execute();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topinteface = (topBarInteface) activity;
		locationinterface = (locationmapinterface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void listview_Functionality() {
		lstLocationList.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {

				final int lastItem = firstVisibleItem + visibleItemCount;

				if (lastItem == totalItemCount) {

					if ((preLast != lastItem) && loadmore) {

						if (!commonData.checkInternetConnection()) {

						} else {
							// lnLoadMore.setVisibility(View.VISIBLE);

							loadmore = false;

							load_more_Data();
						}
					}
				}
			}
		});
	}

	private void load_more_Data() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {

					}

					@Override
					public void doInBackground() {

						String serverUrl = "LocationService.svc/GetAllLocations?customerid="
								+ commonData.CustomerId
								+ "&languageid="
								+ commonData.languageId
								+ "&latitude="
								+ commonData.currentLatitude
								+ "&longitude="
								+ commonData.currentLongitude
								+ "&startindex="
								+ strPageIndex + "&dLastRunDate=";
						// Log.e("Load more", serverUrl);

						serverUrl = commonData.Url_EncodeWithBlank(serverUrl);
						url_response = call_webservice_SliderLocation
								.makeServicegetRequest(serverUrl);
						try {
							JSONObject js_getres = new JSONObject(url_response);
							StatusCode = js_getres.getString("vStatusCode");
							if (StatusCode.equals("100")) {
								JSONArray js_arr_data = js_getres
										.getJSONArray("Locations");
								strPageIndex = js_getres
										.getString("iLastIndex");
								strLastDate = js_getres
										.getString("dLastRunDate");
								for (int i = 0; i < js_arr_data.length(); i++) {
									JSONObject jsobj = js_arr_data
											.getJSONObject(i);
									locationlist = new LocationList();
									locationlist = locationlist
											.convertObjecttoJson(jsobj);
									if (locationlist != null) {
										arrLocationList.add(locationlist);
									}
								}
							} else {
								messageResponse = js_getres
										.getString("vMessageResponse");
							}

						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						// lnLoadMore.setVisibility(View.GONE);
						try {
							lstLocationList.removeFooterView(views);
						} catch (Exception e) {
							// TODO: handle exception
						}

						if (strPageIndex.equalsIgnoreCase("0")) {
							loadmore = false;
						} else {
							loadmore = true;
							lstLocationList.addFooterView(views);
						}

						if (StatusCode.equals("100")) {

							swipeLayout.setVisibility(View.VISIBLE);
							lstLocationList.setVisibility(View.VISIBLE);
							txtLocationNoData.setVisibility(View.GONE);
							preLast = 0;
							adapterLocationList.notifyDataSetChanged();

						} else if (StatusCode.equals("101")) {

							loadmore = false;
							// txtLocationNoData.setText(messageResponse);
							swipeLayout.setVisibility(View.GONE);
							lstLocationList.setVisibility(View.GONE);
							txtLocationNoData.setVisibility(View.VISIBLE);

						} else if (StatusCode.equals("118")) {
							loadmore = false;
							try {
								lstLocationList.removeFooterView(views);
							} catch (Exception e) {
								// TODO: handle exception
							}

						} else {

						}

					}

				});
		as.execute();
	}

}
