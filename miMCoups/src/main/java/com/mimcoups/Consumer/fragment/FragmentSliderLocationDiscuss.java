package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mimcoups.Consumer.customClass.ChatUser;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.DealShareDiscussAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.DiscussdealSendRequestInterface;
import com.mimcoups.Consumer.inteface.discusschatinteface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.squareup.picasso.Picasso;

public class FragmentSliderLocationDiscuss extends Fragment implements
		DiscussdealSendRequestInterface {

	private ImageView imgDiscussDealQuestions, imgDiscussDealBack,
			imgmerchantstore;
	private topBarInteface topInterface;
	private ListView lstfriends;
	private ArrayList<ChatUser> arrAllChats = new ArrayList<ChatUser>();
	private DealShareDiscussAdapter adapter;
	private String serverResponse = "";
	private WebService_Call webservicecall = new WebService_Call();
	private String statusCode = "", statusMessage = "";
	private CommonData globalClass;
	ArrayList<FriendsDetail> arrfriendsDetail = new ArrayList<FriendsDetail>();
	private RelativeLayout relMerchatChat;
	private CommonData commonData;
	private String strMerchantLogoUrl;
	private LocationList LocationsliderPassDetail;
	private Activity activity;
	private discusschatinteface discussChatinteface;
	private LinearLayout tempheaderbg, headerbg;
	private DiscussdealSendRequestInterface discussdealinterfaceobj = this;
	private String url_response = "", strStatusCode = "", strMessageResponse;
	ImageView imgLocationImage_row_location;
	TextView txtmerchantstorename, txtDealName_rowlocation,
			txtDealDtane_rowLocation, txtloc_desc_rowlocation,
			txtDealDistane_rowLocation, txtLocationDeals_rowlocation;
	RelativeLayout relmainfragment_discuss_location;
	Context context;
	private LinearLayout emptyview;

	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_discuss_location,
				container, false);
		LocationsliderPassDetail = (LocationList) getArguments()
				.getSerializable("locationlist");

		initControls(view);
		clickEvents();

		setData();
		((DeelFeedActivity) getActivity()).is_chatscreen_neddto_refresh = false;
		return view;

	}

	private void setData() {

		// Picasso.with(activity)
		// .load(LocationsliderPassDetail.vStoreDetailImage_A_650X325)
		// .placeholder(R.drawable.noimage_locationdetail)
		// .into(imgLocationImage);
	}

	private void clickEvents() {
		relmainfragment_discuss_location
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgDiscussDealBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("back");
			}
		});

		imgDiscussDealQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("questions");
				}
			}
		});

		relMerchatChat.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				discussChatinteface.dicusschatclick(
						LocationsliderPassDetail.vStoreListImage_A_70X70,
						LocationsliderPassDetail.vMerchantStoreName);
			}
		});
	}

	private void initControls(View view) {
		// imgLocationImage = (ImageView)
		// view.findViewById(R.id.imgLocationImage);

		context = getActivity();

		imgLocationImage_row_location = (ImageView) view
				.findViewById(R.id.imgLocationImage_row_location);
		txtDealName_rowlocation = (TextView) view
				.findViewById(R.id.txtDealName_rowlocation);
		txtDealDistane_rowLocation = (TextView) view
				.findViewById(R.id.txtDealDistane_rowLocation);

		txtloc_desc_rowlocation = (TextView) view
				.findViewById(R.id.txtloc_desc_rowlocation);

		txtLocationDeals_rowlocation = (TextView) view
				.findViewById(R.id.txtLocationDeals_rowlocation);
		relmainfragment_discuss_location = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_discuss_location);
		lstfriends = (ListView) view.findViewById(R.id.listFriends);
		emptyview = (LinearLayout) view.findViewById(R.id.empty);

		Picasso.with(context)
				.load(LocationsliderPassDetail.vStoreListImage_A_70X70)
				.into(imgLocationImage_row_location);
		txtDealName_rowlocation
				.setText(LocationsliderPassDetail.vMerchantStoreName);

		txtDealDistane_rowLocation.setText(LocationsliderPassDetail.dDistance);

		txtloc_desc_rowlocation
				.setText(LocationsliderPassDetail.vShortDescription);

		txtLocationDeals_rowlocation
				.setText(LocationsliderPassDetail.iDealCount);

		commonData = (CommonData) getActivity().getApplicationContext();
		imgDiscussDealBack = (ImageView) view
				.findViewById(R.id.imgDiscussDealBack);
		imgDiscussDealQuestions = (ImageView) view
				.findViewById(R.id.imgDiscussDealQuestions);
		imgmerchantstore = (ImageView) view.findViewById(R.id.imgmerchantstore);
		relMerchatChat = (RelativeLayout) view
				.findViewById(R.id.relMerchatChat);
		headerbg = (LinearLayout) view.findViewById(R.id.headerbg);
		txtmerchantstorename = (TextView) view
				.findViewById(R.id.txtmerchantstorename);
		globalClass = (CommonData) getActivity().getApplicationContext();

		tempheaderbg = (LinearLayout) view.findViewById(R.id.tempheaderbg);

		if (!globalClass.checkInternetConnection()) {

			headerbg.setVisibility(View.GONE);
			tempheaderbg.setVisibility(View.GONE);
			relMerchatChat.setVisibility(View.GONE);

			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub

						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);

		} else {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						String vLocationName = "";

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInterface.showDialog("Loading");
							arrfriendsDetail.clear();
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub

							String serverUrl = "CustomerService.svc/GetAllCustomerForLocationDiscuss?customerid="
									+ commonData.CustomerId
									+ "&languageid="
									+ commonData.languageId
									+ "&locationid="
									+ LocationsliderPassDetail.iMerchantStoreId;

							// Log.e("SilderrLocationDiscuss", serverUrl);
							serverResponse = webservicecall
									.makeServicegetRequest(serverUrl);
							try {
								JSONObject js_getres = new JSONObject(
										serverResponse);
								statusCode = js_getres.getString("vStatusCode");
								arrfriendsDetail = new ArrayList<FriendsDetail>();

								strMerchantLogoUrl = js_getres
										.getString("vStoreListImage_A_90X90");

								vLocationName = js_getres
										.getString("vLocationName");

								globalClass.arrRpListStore = new ArrayList<String>();
								JSONArray js_merchantarr_data = js_getres
										.getJSONArray("Merchants");
								for (int i = 0; i < js_merchantarr_data
										.length(); i++) {

									JSONObject jsobj = js_merchantarr_data
											.getJSONObject(i);

									String vUserName = jsobj
											.getString("vUserName");
									globalClass.arrRpListStore.add(vUserName);

								}
								if (statusCode.equals("100")) {

									JSONArray js_arr_data = js_getres
											.getJSONArray("Customers");

									for (int i = 0; i < js_arr_data.length(); i++) {

										FriendsDetail fs = new FriendsDetail();
										JSONObject jsobj = js_arr_data
												.getJSONObject(i);
										fs.friendsId = jsobj
												.getString("iCustomerId");

										fs.freindsName = jsobj
												.getString("vCustomerName");

										fs.profileImageName = jsobj
												.getString("vProfileImageName");

										fs.userName = jsobj
												.getString("vUserName");

										fs.isfriend = jsobj
												.getBoolean("IsFriend");
										arrfriendsDetail.add(fs);
									}

								} else {
									statusMessage = js_getres
											.getString("vMessageResponse");
								}

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub

							topInterface.hideDialog();

							if (strMerchantLogoUrl.length() != 0) {

								Picasso.with(getActivity())
										.load(strMerchantLogoUrl)
										.into(imgmerchantstore);
							}

							if (statusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (statusCode.equals("100")) {
								txtmerchantstorename.setText(vLocationName);

								headerbg.setVisibility(View.VISIBLE);
								tempheaderbg.setVisibility(View.VISIBLE);
								relMerchatChat.setVisibility(View.VISIBLE);

								adapter = new DealShareDiscussAdapter(
										getActivity(), arrfriendsDetail,
										discussdealinterfaceobj, context);
								lstfriends.setAdapter(adapter);

							} else if (statusCode.equals("101")) {
								tempheaderbg.setVisibility(View.GONE);
								txtmerchantstorename.setText(vLocationName);

								if (arrfriendsDetail.size() == 0) {

									emptyview.setVisibility(View.INVISIBLE);
									lstfriends.setVisibility(View.GONE);

								} else {
									emptyview.setVisibility(View.GONE);
									lstfriends.setVisibility(View.VISIBLE);
								}
							} else {
								tempheaderbg.setVisibility(View.GONE);

								headerbg.setVisibility(View.GONE);
								tempheaderbg.setVisibility(View.GONE);
								relMerchatChat.setVisibility(View.GONE);
							}
						}
					});
			as.execute();
		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
		discussChatinteface = (discusschatinteface) activity;
		this.activity = activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void discussdealsendRequest(final FriendsDetail friendsdetail) {
		// TODO Auto-generated method stub
		if (!commonData.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInterface.showDialog("Loading");
							// arrFriends.clear();
						}

						@Override
						public void doInBackground() {
							String getUrl = "FriendService.svc/SendFriendRequest?languageid="
									+ commonData.languageId
									+ "&customerid="
									+ commonData.CustomerId
									+ "&friendid="
									+ friendsdetail.friendsId;
							url_response = webservicecall
									.makeServicegetRequest(getUrl);
							try {
								JSONObject jsonObject = new JSONObject(
										url_response);
								strStatusCode = jsonObject
										.getString("vStatusCode");
								strMessageResponse = jsonObject
										.getString("vMessageResponse");

							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topInterface.hideDialog();
							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (strStatusCode.equals("100")) {
								Toast.makeText(getActivity(),
										"" + strMessageResponse, 9000).show();
							} else {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO
												// Auto-generated
												// method stub
											}
										}, strMessageResponse);
								dsp.show(getFragmentManager(), "");
							}
						}
					});
			as.execute();
		}

	}
}
