package com.mimcoups.Consumer.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.inteface.bottombarClickInterface;

public class Fragmentbottom extends Fragment {

	private bottombarClickInterface bottominterface;
	private LinearLayout lnDeelfeed, lnFriendPurchase, lnDiscuss,
			lnNotification, lnMore;
	private ImageView imgDealFeed, imgFriendPurchase, imgDiscuss,
			imgNotification;
	private TextView txtDealFeed, txtFrinedPurchase, txtDiscuss,
			txtNotification;
	private TextView txtBadgeCount;
	private String username = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_bottom_fragment,
				container, false);

		initControls(view);
		clickEvent();
		return view;
	}

	private void initControls(View view) {
		txtBadgeCount = (TextView) view.findViewById(R.id.txtBadgeCount);
		lnDeelfeed = (LinearLayout) view.findViewById(R.id.lnDealfeed);
		lnFriendPurchase = (LinearLayout) view
				.findViewById(R.id.lnFriendPurcahse);
		lnDiscuss = (LinearLayout) view.findViewById(R.id.lnDiscuss);
		lnNotification = (LinearLayout) view.findViewById(R.id.lnNotification);
		lnMore = (LinearLayout) view.findViewById(R.id.lnMore);

		// init all imageview
		imgDealFeed = (ImageView) view.findViewById(R.id.imgDealfeed);
		imgFriendPurchase = (ImageView) view
				.findViewById(R.id.imgFriendPurchase);

		imgDiscuss = (ImageView) view.findViewById(R.id.imgDiscuss);
		imgNotification = (ImageView) view.findViewById(R.id.imgNotifications);

		// init all textview
		txtDealFeed = (TextView) view.findViewById(R.id.txtDealFeed);
		txtFrinedPurchase = (TextView) view
				.findViewById(R.id.txtFriendPurchase);
		txtDiscuss = (TextView) view.findViewById(R.id.txtdiscuss);
		txtNotification = (TextView) view.findViewById(R.id.txtnotifications);
	}

	private void called() {

	}

	private void clickEvent() {

		lnDeelfeed.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment fragment = getFragmentManager().findFragmentById(
						R.id.frmDealFeed);

				if (!(fragment instanceof FragmentDealFeed)) {

					imgDealFeed
							.setImageResource(R.drawable.icon_bottombar_dealfeed_selected);
					imgFriendPurchase
							.setImageResource(R.drawable.icon_bottombar_friend_purchases);
					imgDiscuss
							.setImageResource(R.drawable.icon_bottombar_discuss);
					imgNotification
							.setImageResource(R.drawable.icon_bottombar_notifications);

					txtDealFeed.setTextColor(Color.parseColor("#EAD230"));
					txtFrinedPurchase.setTextColor(Color.WHITE);
					txtDiscuss.setTextColor(Color.WHITE);
					txtNotification.setTextColor(Color.WHITE);

					bottominterface.bottombarClick("DeelFeed");

				}

				// if (txtDealFeed.getCurrentTextColor() == Color.WHITE) {
				//
				// // all imageview set
				// imgDealFeed
				// .setImageResource(R.drawable.icon_bottombar_dealfeed_selected);
				// imgFriendPurchase
				// .setImageResource(R.drawable.icon_bottombar_friend_purchases);
				// imgDiscuss
				// .setImageResource(R.drawable.icon_bottombar_discuss);
				// imgNotification
				// .setImageResource(R.drawable.icon_bottombar_notifications);
				//
				// txtDealFeed.setTextColor(Color.parseColor("#EAD230"));
				// txtFrinedPurchase.setTextColor(Color.WHITE);
				// txtDiscuss.setTextColor(Color.WHITE);
				// txtNotification.setTextColor(Color.WHITE);
				// bottominterface.bottombarClick("DeelFeed");
				//
				// }
			}
		});
		lnFriendPurchase.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment fragment = getFragmentManager().findFragmentById(
						R.id.frmDealFeed);

				if (!(fragment instanceof FragmentFriendPurchase)) {
					imgDealFeed
							.setImageResource(R.drawable.icon_bottombar_dealfeed);
					imgFriendPurchase
							.setImageResource(R.drawable.icon_bottombar_friend_purchases_selected);
					imgDiscuss
							.setImageResource(R.drawable.icon_bottombar_discuss);
					imgNotification
							.setImageResource(R.drawable.icon_bottombar_notifications);

					txtFrinedPurchase.setTextColor(Color.parseColor("#EAD230"));
					txtDealFeed.setTextColor(Color.WHITE);
					txtDiscuss.setTextColor(Color.WHITE);
					txtNotification.setTextColor(Color.WHITE);

					bottominterface.bottombarClick("FriendPurchase");
				}

				// if (txtFrinedPurchase.getCurrentTextColor() == Color.WHITE) {
				//
				// // all imageview set
				// imgDealFeed
				// .setImageResource(R.drawable.icon_bottombar_dealfeed);
				// imgFriendPurchase
				// .setImageResource(R.drawable.icon_bottombar_friend_purchases_selected);
				// imgDiscuss
				// .setImageResource(R.drawable.icon_bottombar_discuss);
				// imgNotification
				// .setImageResource(R.drawable.icon_bottombar_notifications);
				//
				// txtFrinedPurchase.setTextColor(Color.parseColor("#EAD230"));
				// txtDealFeed.setTextColor(Color.WHITE);
				// txtDiscuss.setTextColor(Color.WHITE);
				// txtNotification.setTextColor(Color.WHITE);
				//
				// bottominterface.bottombarClick("FriendPurchase");
				// }
			}
		});
		lnDiscuss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment fragment = getFragmentManager().findFragmentById(
						R.id.frmDealFeed);

				if (!(fragment instanceof FragmentChatList)) {
					imgDealFeed
							.setImageResource(R.drawable.icon_bottombar_dealfeed);
					imgFriendPurchase
							.setImageResource(R.drawable.icon_bottombar_friend_purchases);
					imgDiscuss
							.setImageResource(R.drawable.icon_bottombar_discuss_selected);
					imgNotification
							.setImageResource(R.drawable.icon_bottombar_notifications);

					txtFrinedPurchase.setTextColor(Color.WHITE);
					txtDealFeed.setTextColor(Color.WHITE);
					txtDiscuss.setTextColor(Color.parseColor("#EAD230"));
					txtNotification.setTextColor(Color.WHITE);
					bottominterface.bottombarClick("Discuss");
				}
				// if (txtDiscuss.getCurrentTextColor() == Color.WHITE) {
				//
				// // all imageview set
				// imgDealFeed
				// .setImageResource(R.drawable.icon_bottombar_dealfeed);
				// imgFriendPurchase
				// .setImageResource(R.drawable.icon_bottombar_friend_purchases);
				// imgDiscuss
				// .setImageResource(R.drawable.icon_bottombar_discuss_selected);
				// imgNotification
				// .setImageResource(R.drawable.icon_bottombar_notifications);
				//
				// txtFrinedPurchase.setTextColor(Color.WHITE);
				// txtDealFeed.setTextColor(Color.WHITE);
				// txtDiscuss.setTextColor(Color.parseColor("#EAD230"));
				// txtNotification.setTextColor(Color.WHITE);
				// bottominterface.bottombarClick("Discuss");
				//
				// }
			}
		});
		lnNotification.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment fragment = getFragmentManager().findFragmentById(
						R.id.frmDealFeed);

				if (!(fragment instanceof FragmentNotificationList)) {

					txtBadgeCount.setVisibility(View.GONE);
					if (!(username.equalsIgnoreCase(" "))) {
						SharedPreferences preference = getActivity()
								.getSharedPreferences("FriendList",
										Context.MODE_PRIVATE);
						preference.edit().putInt(username, 0).commit();
					}
					imgDealFeed
							.setImageResource(R.drawable.icon_bottombar_dealfeed);
					imgFriendPurchase
							.setImageResource(R.drawable.icon_bottombar_friend_purchases);
					imgDiscuss
							.setImageResource(R.drawable.icon_bottombar_discuss);
					imgNotification
							.setImageResource(R.drawable.icon_bottombar_notifications_selected);

					txtFrinedPurchase.setTextColor(Color.WHITE);
					txtDealFeed.setTextColor(Color.WHITE);
					txtDiscuss.setTextColor(Color.WHITE);
					txtNotification.setTextColor(Color.parseColor("#EAD230"));

					bottominterface.bottombarClick("Notification");

				}

				// if (txtNotification.getCurrentTextColor() == Color.WHITE) {
				// txtBadgeCount.setVisibility(View.GONE);
				// if (!(username.equalsIgnoreCase(" "))) {
				// SharedPreferences preference = getActivity()
				// .getSharedPreferences("FriendList",
				// Context.MODE_PRIVATE);
				// preference.edit().putInt(username, 0).commit();
				// }
				// imgDealFeed
				// .setImageResource(R.drawable.icon_bottombar_dealfeed);
				// imgFriendPurchase
				// .setImageResource(R.drawable.icon_bottombar_friend_purchases);
				// imgDiscuss
				// .setImageResource(R.drawable.icon_bottombar_discuss);
				// imgNotification
				// .setImageResource(R.drawable.icon_bottombar_notifications_selected);
				//
				// txtFrinedPurchase.setTextColor(Color.WHITE);
				// txtDealFeed.setTextColor(Color.WHITE);
				// txtDiscuss.setTextColor(Color.WHITE);
				// txtNotification.setTextColor(Color.parseColor("#EAD230"));
				//
				// bottominterface.bottombarClick("Notification");
				//
				// }

			}
		});
		lnMore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				bottominterface.bottombarClick("More");
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		bottominterface = (bottombarClickInterface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		super.onDetach();

	}

	public void unclickableButtons() {

		imgDealFeed.setImageResource(R.drawable.icon_bottombar_dealfeed);
		imgFriendPurchase
				.setImageResource(R.drawable.icon_bottombar_friend_purchases);
		imgDiscuss.setImageResource(R.drawable.icon_bottombar_discuss);
		imgNotification
				.setImageResource(R.drawable.icon_bottombar_notifications);
		txtFrinedPurchase.setTextColor(Color.WHITE);
		txtDealFeed.setTextColor(Color.WHITE);
		txtDiscuss.setTextColor(Color.WHITE);
		txtNotification.setTextColor(Color.WHITE);
	}

	public void selectNotification() {

		imgDealFeed.setImageResource(R.drawable.icon_bottombar_dealfeed);
		imgFriendPurchase
				.setImageResource(R.drawable.icon_bottombar_friend_purchases);
		imgDiscuss.setImageResource(R.drawable.icon_bottombar_discuss);
		imgNotification
				.setImageResource(R.drawable.icon_bottombar_notifications_selected);

		txtFrinedPurchase.setTextColor(Color.WHITE);
		txtDealFeed.setTextColor(Color.WHITE);
		txtDiscuss.setTextColor(Color.WHITE);
		txtNotification.setTextColor(Color.parseColor("#EAD230"));
	}

	public void selectDiscussNotification() {

		imgDealFeed.setImageResource(R.drawable.icon_bottombar_dealfeed);
		imgFriendPurchase
				.setImageResource(R.drawable.icon_bottombar_friend_purchases);
		imgDiscuss.setImageResource(R.drawable.icon_bottombar_discuss_selected);
		imgNotification
				.setImageResource(R.drawable.icon_bottombar_notifications);

		txtFrinedPurchase.setTextColor(Color.WHITE);
		txtDealFeed.setTextColor(Color.WHITE);
		txtDiscuss.setTextColor(Color.parseColor("#EAD230"));
		txtNotification.setTextColor(Color.WHITE);
	}

	public void SetBadgeCount(int count) {

		if (count <= 0) {

			txtBadgeCount.setVisibility(View.GONE);
		} else {

			txtBadgeCount.setVisibility(View.VISIBLE);
			txtBadgeCount.setText("" + count);
		}

	}

	public void selectDeelFeed() {
		imgDealFeed
				.setImageResource(R.drawable.icon_bottombar_dealfeed_selected);
		txtDealFeed.setTextColor(Color.WHITE);
	}

	public void setColor() {
		imgDealFeed
				.setImageResource(R.drawable.icon_bottombar_dealfeed_selected);
		imgFriendPurchase
				.setImageResource(R.drawable.icon_bottombar_friend_purchases);
		imgDiscuss.setImageResource(R.drawable.icon_bottombar_discuss);
		imgNotification
				.setImageResource(R.drawable.icon_bottombar_notifications);

		txtDealFeed.setTextColor(Color.parseColor("#EAD230"));
		txtFrinedPurchase.setTextColor(Color.WHITE);
		txtDiscuss.setTextColor(Color.WHITE);
		txtNotification.setTextColor(Color.WHITE);
	}

	// public void badgeReceive(int value,String username)
	// {
	// Log.e("value", "a"+value);
	// txtBadgeCount.setText(value);
	// txtBadgeCount.setVisibility(View.VISIBLE);
	// this.username=username;
	// }

	public void onDestroyView() {
		super.onDestroyView();

	}
}