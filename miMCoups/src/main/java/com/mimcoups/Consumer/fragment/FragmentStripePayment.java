package com.mimcoups.Consumer.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.R;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.model.Charge;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragmentStripePayment extends Fragment {

	private topBarInteface topInterface;
	private WebService_Call web_service = new WebService_Call();
	CommonData globalClass;
	WebService_Call web_Service;

	ProgressDialog progressDialog;
	String vStripeChargesURL,vStripeSecretKey,vStripePublishableKey,vDealPurchaseCancelMessage;
	String AccessToken = "";
	String TransactionId = "",mimcoupstransactionid="",merchanttransactionid="";
	String PayerId = "";
	String ReturnUrl = "";
	String CancelUrl = "";
	String vMessageResponse = "", vStatusCode = "";
	String Currency = "aud", CountryCode = "AU";
	String PaypalLogo = "http://www.mimcoups.com/images/paypallogo.png";
	String Username, Email;

	DealFeedDetail dealdetail;
	SharedPreferences sp;
	String shippingstate = "";
	String billingstate = "";
	ImageView imgPurchaseBack;
	String transcationId,application_fee,balance;
	RelativeLayout relmainfragment_purchase;
//	String applicationfee="100";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.layout_payment, container, false);
		globalClass = (CommonData) getActivity().getApplicationContext();
		dealdetail = (DealFeedDetail) getArguments().getSerializable(
				"DealFeedDetail");
		ReturnUrl = globalClass.Get_ReturnUrl(globalClass.languageId);
		CancelUrl = globalClass.Get_CancelUrl(globalClass.languageId);

		sp = getActivity().getSharedPreferences("UserDetail",
				Context.MODE_PRIVATE);

		Username = sp.getString("customername", "");
		Email = sp.getString("emailid", "");

		if (globalClass.languageId.equalsIgnoreCase("2")) {
			Currency = "nok";
			CountryCode = "NO";
		}

		web_Service = new WebService_Call();
		initControls(view);

		clickEvents();
		return view;
	}

	private void initControls(View view) {

		relmainfragment_purchase=(RelativeLayout)view.findViewById(R.id.relmainfragment_purchase);
		imgPurchaseBack=(ImageView)view.findViewById(R.id.imgPurchaseBack);

		if (!globalClass.checkInternetConnection()) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							topInterface.topbarClick("back");
						}
					}, getActivity().getResources().getString(
							R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);
		} else {

			Log.e("DeaLNAME",dealdetail.Dealname);



			new Get_PaymentDetails(globalClass.Payment_details
					+ globalClass.languageId).execute();
		}

	}

	private void clickEvents() {
		relmainfragment_purchase.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});
		
		imgPurchaseBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(globalClass.checkInternetConnection()){
					
					CancelOrder();
				}
				
				
				topInterface.topbarClick("back");
				
				
			}
		});

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if(globalClass.checkInternetConnection()){
			
			CancelOrder();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	public void OrderApi(final String orderquantityval,
			final String streetnumberval, final String streetaddressval,
			final String citynameval, final String zipcodeval,
			final String billingstreetnumberval,
			final String billingcitynameval,
			final String billingstreetaddressval,
			final String billingzipcodeval, final String cardtype,
			final String nameoncard, final String creditcardno,
			final String monthexp, final String yearexp, final String cvv,
			final String Types, final String shippingstatename,
			final String billingstatename) {

		shippingstate = shippingstatename;
		billingstate = billingstatename;

		if (globalClass.languageId.equalsIgnoreCase("2")) {
			shippingstate = "";
			billingstate = "";

		}

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					String urlResponsereward = "";

					@Override
					public void onPreExecute() {
						topInterface.showDialog("Loading");
					}

					@SuppressWarnings("deprecation")
					@Override
					public void doInBackground() {

						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						nameValuePairs.add(new BasicNameValuePair("customerid",
								"" + globalClass.CustomerId));
						nameValuePairs.add(new BasicNameValuePair("languageid",
								"" + globalClass.languageId));
						nameValuePairs.add(new BasicNameValuePair("dealid", ""
								+ dealdetail.DealId));
						nameValuePairs.add(new BasicNameValuePair("locationid",
								"" + dealdetail.locationId));
						nameValuePairs.add(new BasicNameValuePair("quantity",
								"" + orderquantityval));
						nameValuePairs.add(new BasicNameValuePair(
								"ordertempid", "" + dealdetail.iOrderTempId));

						nameValuePairs.add(new BasicNameValuePair(
								"stripemerchantaccountid", "" + dealdetail.vStripeMerchantAccountId));


						nameValuePairs
								.add(new BasicNameValuePair("purchasedamount",
										"" + dealdetail.TotalAmount));


						nameValuePairs.add(new BasicNameValuePair(
								"mimcoupstransactionid", ""
								+ mimcoupstransactionid));
						nameValuePairs.add(new BasicNameValuePair(
								"merchanttransactionid", ""
								+ merchanttransactionid));

						nameValuePairs.add(new BasicNameValuePair(
								"transactionid", "" + TransactionId));


						// edttotalprice.getText().toString().replaceAll("$",
						// "");

						nameValuePairs.add(new BasicNameValuePair(
								"shippingstreetnumber", "" + streetnumberval));
						nameValuePairs
								.add(new BasicNameValuePair(
										"shippingstreetaddress", ""
												+ streetaddressval));
						nameValuePairs.add(new BasicNameValuePair(
								"shippingcityname", "" + citynameval));
						nameValuePairs.add(new BasicNameValuePair(
								"shippingzipcode", "" + zipcodeval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingstreetnumber", ""
										+ billingstreetnumberval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingstreetcityname", ""
										+ billingcitynameval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingstreetaddress", ""
										+ billingstreetaddressval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingzipcode", "" + billingzipcodeval));

						nameValuePairs.add(new BasicNameValuePair(
								"billingstatename", "" + billingstate));

						nameValuePairs.add(new BasicNameValuePair(
								"shippingstatename", "" + shippingstate));

						if (Types.equalsIgnoreCase("free")) {
							CommonData.Type = "";
							CommonData.credit_card_holder = "";
							CommonData.credit_card = "";
							CommonData.Expiry_month = "";
							CommonData.Expiry_Year = "";
							CommonData.CVV = "";

						}

						Log.e("customerid", "" + globalClass.CustomerId);
						Log.e("languageid", "" + globalClass.languageId);
						Log.e("dealid", "" + dealdetail.DealId);
						Log.e("locationid", "" + dealdetail.locationId);
						Log.e("quantity", "" + orderquantityval);
						Log.e("purchasedamount", "" + dealdetail.TotalAmount);
						Log.e("shippingstreetnumber", "" + streetnumberval);
						Log.e("shippingstreetaddress", "" + streetaddressval);
						Log.e("shippingcityname", "" + citynameval);
						Log.e("shippingzipcode", "" + zipcodeval);
						Log.e("billingstreetnumber", ""
								+ billingstreetnumberval);
						Log.e("billingstreetcityname", "" + billingcitynameval);
						Log.e("billingstreetaddress", ""
								+ billingstreetaddressval);
						Log.e("billingzipcode", "" + billingzipcodeval);
						Log.e("transactionid", "" + TransactionId);
						Log.e("Shippingstate", shippingstate);
						Log.e("Billing state", billingstate);
						//
						// Log.e("cardtype 1233", "hardik" + CommonData.Type);
						// Log.e("nameoncard", "" +
						// CommonData.credit_card_holder);
						// Log.e("creditcardno", "" + CommonData.credit_card);
						// Log.e("monthexp", "" + CommonData.Expiry_month);
						// Log.e("yearexp", "" + CommonData.Expiry_Year);
						// Log.e("cvv", "" + CommonData.CVV);


						urlResponsereward = web_Service.makeorder_local(
								globalClass.Base_Url
										+ "OrderService.svc/AddOrder",
								nameValuePairs);
						Log.e("called url response===", urlResponsereward);

						try {
							JSONObject jobj = new JSONObject(urlResponsereward);

							vStatusCode = jobj.getString("vStatusCode");
							if (vStatusCode.equals("100")) {

								vMessageResponse = jobj
										.getString("vMessageResponse");

							} else if (vStatusCode.equals("122")
									|| (vStatusCode.equals("121"))
									|| (vStatusCode.equals("123"))) {
								vMessageResponse = jobj
										.getString("vMessageResponse");

							} else if (vStatusCode.equals("102")) {
								vMessageResponse = jobj
										.getString("vMessageResponse");

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {

						topInterface.hideDialog();

						if(vMessageResponse.trim().length()>0){
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");

										}
									}, vMessageResponse);
							ds.setCancelable(false);
							ds.show(getFragmentManager(), "");

						}else{
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");

										}
									}, getResources().getString(R.string.txterror));
							ds.setCancelable(false);
							ds.show(getFragmentManager(), "");
						}



					}
				});
		as.execute();
	}

	public class Get_PaymentDetails extends AsyncTask<Void, Void, String> {

		String url = "";

		public Get_PaymentDetails(String Url) {
			this.url = Url;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method
			// showProgressDialog("Loading...");
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage(getActivity().getResources().getString(
					R.string.loading));

			progressDialog.setCancelable(false);
			progressDialog.show();

			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {

			String repsonse = "";

			try {

				repsonse = web_Service.Get_Payment_Details(url);
				Log.e("Url stripe",url);
				Log.e("Response",repsonse);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// return getResponsewithUrlEncoding();
			return repsonse;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			try {

				JSONObject obj = new JSONObject(result);
				if (obj.getString("vStatusCode").equalsIgnoreCase("100")) {

					JSONArray arr_payment = obj.getJSONArray("PaypalDetails");

					JSONObject paypal = arr_payment.getJSONObject(0);

					vStripeChargesURL = paypal.getString("vStripeChargesURL");
					vStripeSecretKey = paypal.getString("vStripeSecretKey");
					vStripePublishableKey = paypal
							.getString("vStripePublishableKey");
					vDealPurchaseCancelMessage = paypal.getString("vDealPurchaseCancelMessage");


					vDealPurchaseCancelMessage = paypal
							.getString("vDealPurchaseCancelMessage");
					dealdetail.Dealdesc = dealdetail.Dealdesc.replace("\\n",
							" ");

//					Double TotalAmounts = (Double
//							.parseDouble(dealdetail.DealAmount) * (Double
//							.parseDouble(dealdetail.Quantity)));
//					Double rewardpoint = ((Double
//							.parseDouble(dealdetail.TotalAmount)) - TotalAmounts);

					final Double TotalAmounts = round((Double
							.parseDouble(dealdetail.DealAmount)) * ((Double
							.parseDouble(dealdetail.Quantity))),2);

					final Double rewardpoint = round(((Double
							.parseDouble(dealdetail.TotalAmount)) - TotalAmounts),2);

					final Double Amount=TotalAmounts-rewardpoint;



					Log.e("Checkout Total amount", "" + TotalAmounts+"==="+Amount);
					Log.e("Checkout deal amount", dealdetail.DealAmount + "=="
							+ dealdetail.Quantity);
					Log.e("Checkout  amount", ""
							+ dealdetail.TotalAmount);

					if (globalClass.languageId.equalsIgnoreCase("2")) {
						dealdetail.shippingstatename = "";
						dealdetail.billingstatename = "";
					}

					if(dealdetail.vStripeMerchantAccountId.trim().length()>0){

						try{
						Stripe stripe = new Stripe(getActivity(),vStripePublishableKey);

						// Create Card instance containing customer's payment
						// information obtained
						Card card = new Card(CommonData.credit_card, Integer.parseInt(CommonData.Expiry_month), Integer.parseInt(CommonData.Expiry_Year),
								CommonData.CVV);

						// Check if card is valid. If valid, create token
						if (card.validateCard()) {


							stripe.createToken(card, new TokenCallback() {

								@Override
								public void onSuccess(Token token) {



									final Double rewardpoints = round(TotalAmounts-((Double
											.parseDouble(dealdetail.TotalAmount)) ),2);

									Log.e("Chained Total",dealdetail.TotalAmount);

									Log.e("Commisions",dealdetail.dCurrentSalesCommission);

									Double ApplicationFee = round((TotalAmounts) * (Double.parseDouble(dealdetail.dCurrentSalesCommission)),2);

									Log.e("Application fee",""+ApplicationFee);
									Log.e("Rewards",""+(rewardpoints*100));

									Double AppFee=Math.ceil((ApplicationFee-(rewardpoints*100)));

									Double PaybleAmount=round((Double.parseDouble(dealdetail.TotalAmount)*100),2);

									String[] payAmount=String.valueOf(PaybleAmount).split("\\.");

									String []appFees=String.valueOf(AppFee).split("\\.");

									Log.e("Payble Amount=",""+PaybleAmount+"===="+Math.ceil((Double.parseDouble(dealdetail.TotalAmount)*100)));
									Log.e("Applications =",""+AppFee);


									new ChainPayment(token.getId(),""+payAmount[0],""+appFees[0]).execute();


//
								}

								@Override
								public void onError(Exception error) {
									Log.e("chjained",error.toString());
									progressDialog.dismiss();

									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(String tag) {

													topInterface.topbarClick("back");

												}
											},error.getMessage().toString());
									ds.setCancelable(false);
									ds.show(getFragmentManager(), "");





								}
							});
						} else {



							Log.e("Else ","Card not support");




						}

					} catch (AuthenticationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}


					}else{


						try {
							Stripe stripe = new Stripe(getActivity(),vStripePublishableKey);

							// Create Card instance containing customer's payment
							// information obtained
							Card card = new Card(CommonData.credit_card, Integer.parseInt(CommonData.Expiry_month), Integer.parseInt(CommonData.Expiry_Year),
									CommonData.CVV);

							// Check if card is valid. If valid, create token
							if (card.validateCard()) {


								stripe.createToken(card, new TokenCallback() {

									@Override
									public void onSuccess(Token token) {

										Double PayAmount=(round(Double.parseDouble(dealdetail.TotalAmount),2))*100;

										String []strs=String.valueOf(PayAmount).split("\\.");


										Log.e("Payable Amount",""+PayAmount+"==="+strs[0]);
										Log.e("Token==",""+token);


										chargeCustomer(token, "" + strs[0]);


									}

									@Override
									public void onError(Exception error) {
										progressDialog.dismiss();

										showAlert("Validation Error",
												error.getLocalizedMessage());

										Log.e("token.",
												error.toString());
									}
								});
							} else {
								progressDialog.dismiss();

								showAlert("Invalid Details",
										"Card details are invalid. Enter valid data");

							}

						} catch (AuthenticationException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

//						new payment_request(""+(TotalAmounts*100))
//								.execute();

					}



				} else {
					progressDialog.dismiss();
					
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

									topInterface.topbarClick("back");

								}
							}, getActivity().getResources().getString(R.string.txterror));
					ds.setCancelable(false);
					ds.show(getFragmentManager(), "");

					

				}

			} catch (Exception e) {
				// TODO: handle exception
			}

			// http://www.zealousweb.com/?token=EC-3WE87855CR216614T&PayerID=3NMC4VMJXMM2S

		}
	}


	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

	public class payment_request extends AsyncTask<Void, Void, String> {

		String repsonse="",amount="";

		public payment_request(String Amount) {

			this.amount=Amount;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method
			// showProgressDialog("Loading...");

			super.onPreExecute();
		}

		@Override
		protected String doInBackground(Void... params) {

			try {
				Stripe stripe = new Stripe(getActivity(),vStripePublishableKey);

				// Create Card instance containing customer's payment
				// information obtained
				Card card = new Card(CommonData.credit_card, Integer.parseInt(CommonData.Expiry_month), Integer.parseInt(CommonData.Expiry_Year),
						CommonData.CVV);

				// Check if card is valid. If valid, create token
				if (card.validateCard()) {


					stripe.createToken(card, new TokenCallback() {

						@Override
						public void onSuccess(Token token) {

							chargeCustomer(token,amount);

						}

						@Override
						public void onError(Exception error) {
							progressDialog.dismiss();

							showAlert("Validation Error",
									error.getLocalizedMessage());

							Log.e("token.",
									error.toString());
						}
					});
				} else {
					progressDialog.dismiss();

					showAlert("Invalid Details",
							"Card details are invalid. Enter valid data");

				}

			} catch (AuthenticationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// return getResponsewithUrlEncoding();
			return repsonse;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			try {

				Log.e("response==", result);


			} catch (Exception e) {
				// TODO: handle exception
			}

			// http://www.zealousweb.com/?token=EC-3WE87855CR216614T&PayerID=3NMC4VMJXMM2S

		}
	}


	public void chargeCustomer(final Token token,final String amount) {


		final Map<String, Object> chargeParams = new HashMap<String, Object>();

		try{

			chargeParams.put("amount", Integer.parseInt(amount));
			chargeParams.put("currency", Currency);


//			chargeParams.put("description", ""+URLEncoder.encode(dealdetail.Dealdesc, "UTF-8").replace("+", " ")+"("+URLEncoder.encode(dealdetail.Dealname, "UTF-8").replace("+", " ")+")");

			chargeParams.put("description", ""+CommonData.Url_EncodeWithOUTBlank(dealdetail.Dealdesc).replace("+", " ")+"("+CommonData.Url_EncodeWithOUTBlank(dealdetail.Dealname).replace("+", " ")+")");
			chargeParams.put("card", token.getId());

		}catch (Exception e){

		}



		new AsyncTask<Void, Void, Void>() {

			Charge charge;
			String msg="";

			@Override
			protected Void doInBackground(Void... params) {
				try {
					com.stripe.Stripe.apiKey = vStripeSecretKey;

					charge = Charge.create(chargeParams, vStripeSecretKey);

					Log.e("Balance", "===" + charge.getId());
					Log.e("Charge ",""+token.getId());


				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					progressDialog.dismiss();
					msg=e.getMessage().toString();
					Log.e("Exceptions===",e.getMessage().toString());

//					showAlert("Exception while charging the card!",
//							e.getLocalizedMessage());
				}
				return null;
			}

			protected void onPostExecute(Void result) {


				progressDialog.dismiss();

				try{
					if(charge!=null){

						if(charge.getId().trim().length()>0){
							TransactionId=charge.getId();

							OrderApi2(dealdetail.orderquantityval,
									dealdetail.streetnumberval,
									dealdetail.streetaddressval,
									dealdetail.citynameval, dealdetail.zipcodeval,
									dealdetail.billingstreetnumberval,
									dealdetail.billingcitynameval,
									dealdetail.billingstreetaddressval,
									dealdetail.billingzipcodeval, "", "", "", "",
									"", "", "", dealdetail.shippingstatename,
									dealdetail.billingstatename);
						}
						else{
							if(msg.trim().length()>0){

								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface.topbarClick("back");

											}
										}, msg);
								ds.setCancelable(false);
								ds.show(getFragmentManager(), "");


							}
						}

					}else{
						if(msg.trim().length()>0){
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");

										}
									},msg);
							ds.setCancelable(false);
							ds.show(getFragmentManager(), "");

						}

					}


				}catch(Exception e){

				}

//				showAlert("Simple Payment", charge.getId());




			};

		}.execute();

	}

	private class ChainPayment extends AsyncTask<String, Void, String> {
		String token="",amount="",applicationfee="";



		public ChainPayment(String Token,String Amount,String ApplicationFee){
			token=Token;
			amount=Amount;
			applicationfee=ApplicationFee;

		}

		@Override
		protected String doInBackground(String... params) {

			Log.e("Amount in background",""+Integer.parseInt(amount));
			Log.e("Application fee",applicationfee);
			Log.e("Token",token);

			String response=ChainedPayment(token,""+(Integer.parseInt(amount)),""+(Integer.parseInt(applicationfee)));

			Log.e("Response", response);

			if(response.trim().length()>0){

				try{

					if(response.contains("error")){

					}else{


						JSONObject jsobj=new JSONObject(response);


						transcationId=jsobj.getString("id");

						application_fee=jsobj.getString("application_fee");

						balance=jsobj.getString("balance_transaction");

						mimcoupstransactionid=applicationfee;
						merchanttransactionid=balance;

						OrderApi2(dealdetail.orderquantityval,
								dealdetail.streetnumberval,
								dealdetail.streetaddressval,
								dealdetail.citynameval, dealdetail.zipcodeval,
								dealdetail.billingstreetnumberval,
								dealdetail.billingcitynameval,
								dealdetail.billingstreetaddressval,
								dealdetail.billingzipcodeval, "", "", "", "",
								"", "", "", dealdetail.shippingstatename,
								dealdetail.billingstatename);

					}



				}catch (Exception e){
					Log.e("Exception==",e.getMessage());

				}


			}

			return response;
		}

		@Override
		protected void onPostExecute(String result) {

			progressDialog.dismiss();

//			showAlert("Chained Payment", "Customer Transaction Id: " + transcationId + " \n MiMCoups Transaction Id : " + application_fee + " \n Transaction Id: " + balance);



			// might want to change "executed" for the returned string passed
			// into onPostExecute() but that is upto you
		}

		@Override
		protected void onPreExecute() {}

		@Override
		protected void onProgressUpdate(Void... values) {}
	}


	private void showAlert(String title, String message) {



		DialogFragment ds = new SingleButtonAlert(
				new DialogInterfaceClick() {

					@Override
					public void dialogClick(String tag) {

						topInterface.topbarClick("back");

					}
				}, message);
		ds.setCancelable(false);
		ds.show(getFragmentManager(), "");

	}

	
public void CancelOrder(){

		
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					String urlResponsereward = "";

					@Override
					public void onPreExecute() {
						
					}

					@SuppressWarnings("deprecation")
					@Override
					public void doInBackground() {

						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						nameValuePairs.add(new BasicNameValuePair("customerid",
								"" + globalClass.CustomerId));
						nameValuePairs.add(new BasicNameValuePair("languageid",
								"" + globalClass.languageId));
						
						nameValuePairs.add(new BasicNameValuePair("ordertempid",
								"" + dealdetail.iOrderTempId));

						

						urlResponsereward = web_service.makeorder(
								"OrderService.svc/DeleteOrderTemp", nameValuePairs);
						Log.e("called url response===", urlResponsereward);

						try {
							JSONObject jobj = new JSONObject(urlResponsereward);

							vStatusCode = jobj.getString("vStatusCode");
							if (vStatusCode.equals("100")) {

								

								vMessageResponse = jobj
										.getString("vMessageResponse");

							} else {
								vMessageResponse = jobj
										.getString("vMessageResponse");

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {
						

					}
				});
		as.execute();
	
	}



	public void showDialog(String transId) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		// set title
		alertDialogBuilder.setTitle("Payment Completed");

		// set dialog message
		alertDialogBuilder
				.setMessage(
						"Payment recieved successfully. \n Your TransactionID :: "
								+ transId).setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// if this button is clicked, close
						// current activity
						dialog.dismiss();

						OrderApi(dealdetail.orderquantityval,
								dealdetail.streetnumberval,
								dealdetail.streetaddressval,
								dealdetail.citynameval, dealdetail.zipcodeval,
								dealdetail.billingstreetnumberval,
								dealdetail.billingcitynameval,
								dealdetail.billingstreetaddressval,
								dealdetail.billingzipcodeval, "", "", "", "",
								"", "", "", dealdetail.shippingstatename,
								dealdetail.billingstatename);
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.show();
	}

	//  call api for chained payment

	public String ChainedPayment(String token , String amount,String applicationfee){
		String response="";

		try {

			String dealname=dealdetail.Dealdesc+"("+dealdetail.Dealname+")";
			Log.e("Deal name",dealname);


			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("amount", amount));
			nameValuePairs.add(new BasicNameValuePair("currency", Currency));
			nameValuePairs.add(new BasicNameValuePair("source", token));


//			nameValuePairs.add(new BasicNameValuePair("description", URLEncoder.encode(dealdetail.Dealdesc, "UTF-8").replace("+", " ")+"("+URLEncoder.encode(dealdetail.Dealname, "UTF-8").replace("+", " ")+")"+" - Fee"));

			nameValuePairs.add(new BasicNameValuePair("description", CommonData.Url_EncodeWithOUTBlank(dealdetail.Dealdesc).replace("+", " ")+"("+CommonData.Url_EncodeWithOUTBlank(dealdetail.Dealname).replace("+", " ")+")"+" - Fee"));
			nameValuePairs.add(new BasicNameValuePair("application_fee", applicationfee));


			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							40000);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, 40000);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(vStripeChargesURL);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpPost.setHeader("Stripe-Account", dealdetail.vStripeMerchantAccountId);
			httpPost.setHeader("Authorization", "Bearer "+vStripeSecretKey);

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);



		}
		catch (Exception e) {
			e.printStackTrace();

		}
		return response;

	}


	public void OrderApi2(final String orderquantityval,
						 final String streetnumberval, final String streetaddressval,
						 final String citynameval, final String zipcodeval,
						 final String billingstreetnumberval,
						 final String billingcitynameval,
						 final String billingstreetaddressval,
						 final String billingzipcodeval, final String cardtype,
						 final String nameoncard, final String creditcardno,
						 final String monthexp, final String yearexp, final String cvv,
						 final String Types, final String shippingstatename,
						 final String billingstatename) {

		shippingstate = shippingstatename;
		billingstate = billingstatename;

		if (globalClass.languageId.equalsIgnoreCase("2")) {
			shippingstate = "";
			billingstate = "";

		}

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					String urlResponsereward = "";

					@Override
					public void onPreExecute() {
						topInterface.showDialog("Loading");
					}

					@SuppressWarnings("deprecation")
					@Override
					public void doInBackground() {

//						@iCustomerId int,@iMerchantDealId int,@iMerchantStoreId int,@iQ' expects the parameter '@vMIMCoupsTransactionId', which was not supplied.'.

						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
						nameValuePairs.add(new BasicNameValuePair("customerid",
								"" + globalClass.CustomerId));
						nameValuePairs.add(new BasicNameValuePair("languageid",
								"" + globalClass.languageId));
						nameValuePairs.add(new BasicNameValuePair("dealid", ""
								+ dealdetail.DealId));
						nameValuePairs.add(new BasicNameValuePair("locationid",
								"" + dealdetail.locationId));
						nameValuePairs.add(new BasicNameValuePair("quantity",
								"" + orderquantityval));
						nameValuePairs.add(new BasicNameValuePair(
								"ordertempid", "" + dealdetail.iOrderTempId));

						if(dealdetail.vStripeMerchantAccountId.trim().length()>0){

							nameValuePairs.add(new BasicNameValuePair(
									"stripemerchantaccountid", "" + dealdetail.vStripeMerchantAccountId));

						}else{

							nameValuePairs.add(new BasicNameValuePair(
									"stripemerchantaccountid", " " ));

						}

						if(mimcoupstransactionid.trim().length()>0){

							nameValuePairs.add(new BasicNameValuePair(
									"mimcoupstransactionid", ""
									+ mimcoupstransactionid));

						}else{
							nameValuePairs.add(new BasicNameValuePair(
									"mimcoupstransactionid", " "));

						}

						if(merchanttransactionid.trim().length()>0){


							nameValuePairs.add(new BasicNameValuePair(
									"merchanttransactionid", " "
									+ merchanttransactionid));

						}else{

							nameValuePairs.add(new BasicNameValuePair(
									"merchanttransactionid", " "));

						}



						nameValuePairs.add(new BasicNameValuePair(
								"transactionid", "" + TransactionId));

						nameValuePairs
								.add(new BasicNameValuePair("purchasedamount",
										"" + dealdetail.TotalAmount));


						nameValuePairs.add(new BasicNameValuePair(
								"cardtype", "" + CommonData.Type));

						nameValuePairs.add(new BasicNameValuePair(
								"nameoncard", "" + CommonData.credit_card_holder));
						nameValuePairs.add(new BasicNameValuePair(
								"creditcardno", "" + CommonData.credit_card));
						nameValuePairs.add(new BasicNameValuePair(
								"monthexp", "" + CommonData.Expiry_month));
						nameValuePairs.add(new BasicNameValuePair(
								"yearexp", "" + CommonData.Expiry_Year));


						nameValuePairs.add(new BasicNameValuePair(
								"shippingstreetnumber", "" + streetnumberval));
						nameValuePairs
								.add(new BasicNameValuePair(
										"shippingstreetaddress", ""
										+ streetaddressval));
						nameValuePairs.add(new BasicNameValuePair(
								"shippingcityname", "" + citynameval));
						nameValuePairs.add(new BasicNameValuePair(
								"shippingzipcode", "" + zipcodeval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingstreetnumber", ""
								+ billingstreetnumberval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingstreetcityname", ""
								+ billingcitynameval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingstreetaddress", ""
								+ billingstreetaddressval));
						nameValuePairs.add(new BasicNameValuePair(
								"billingzipcode", "" + billingzipcodeval));

						nameValuePairs.add(new BasicNameValuePair(
								"billingstatename", "" + billingstate));

						nameValuePairs.add(new BasicNameValuePair(
								"shippingstatename", "" + shippingstate));



						if (Types.equalsIgnoreCase("free")) {
							CommonData.Type = "";
							CommonData.credit_card_holder = "";
							CommonData.credit_card = "";
							CommonData.Expiry_month = "";
							CommonData.Expiry_Year = "";
							CommonData.CVV = "";

						}

						Log.e("customerid", "" + globalClass.CustomerId);
						Log.e("languageid", "" + globalClass.languageId);
						Log.e("dealid", "" + dealdetail.DealId);
						Log.e("locationid", "" + dealdetail.locationId);
						Log.e("quantity", "" + orderquantityval);
						Log.e("purchasedamount", "" + dealdetail.TotalAmount);
						Log.e("shippingstreetnumber", "" + streetnumberval);
						Log.e("shippingstreetaddress", "" + streetaddressval);
						Log.e("shippingcityname", "" + citynameval);
						Log.e("shippingzipcode", "" + zipcodeval);
						Log.e("billingstreetnumber", ""
								+ billingstreetnumberval);
						Log.e("billingstreetcityname", "" + billingcitynameval);
						Log.e("billingstreetaddress", ""
								+ billingstreetaddressval);
						Log.e("billingzipcode", "" + billingzipcodeval);
						Log.e("transactionid", "" + TransactionId);
						Log.e("Shippingstate", shippingstate);
						Log.e("Billing state", billingstate);
						Log.e("Mimcoups ID",mimcoupstransactionid);
						Log.e("Merchant ID",merchanttransactionid);
						Log.e("Order ID",dealdetail.iOrderTempId);




						try
						{
							HttpClient httpClient = new DefaultHttpClient();
							HttpEntity httpEntity = null;
							HttpResponse httpResponse = null;

							httpClient.getParams()
									.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
											35000);
							httpClient.getParams().setParameter(
									CoreConnectionPNames.SO_TIMEOUT, 35000);

							// Checking http request method type
							HttpPost httpPost = new HttpPost(CommonData.Base_Url + "OrderService.svc/AddOrder");
							// adding post params

							httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
							httpPost.setHeader("X-Auth-Token", "MIMCOUPS"
									+ globalClass.CustomerId);

							httpResponse = httpClient.execute(httpPost);

							httpEntity = httpResponse.getEntity();
							urlResponsereward = EntityUtils.toString(httpEntity);
						}
						catch (Exception ex){
							Log.e("Debug", "error: " + ex.getMessage(), ex);
						}



//						urlResponsereward = web_Service.makeorder_local(
//								globalClass.Base_Url
//										+ "OrderService.svc/AddOrder",
//								nameValuePairs);
						Log.e("called url response===", urlResponsereward);

						try {
							JSONObject jobj = new JSONObject(urlResponsereward);

							vStatusCode = jobj.getString("vStatusCode");
							if (vStatusCode.equals("100")) {

								vMessageResponse = jobj
										.getString("vMessageResponse");

							} else if (vStatusCode.equals("122")
									|| (vStatusCode.equals("121"))
									|| (vStatusCode.equals("123"))) {
								vMessageResponse = jobj
										.getString("vMessageResponse");

							} else if (vStatusCode.equals("102")) {
								vMessageResponse = jobj
										.getString("vMessageResponse");

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onPostExecute() {

						topInterface.hideDialog();

						if(vMessageResponse.trim().length()>0){
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");

										}
									}, vMessageResponse);
							ds.setCancelable(false);
							ds.show(getFragmentManager(), "");

						}else{
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");

										}
									}, getResources().getString(R.string.txterror));
							ds.setCancelable(false);
							ds.show(getFragmentManager(), "");
						}

					}
				});
		as.execute();
	}


}
