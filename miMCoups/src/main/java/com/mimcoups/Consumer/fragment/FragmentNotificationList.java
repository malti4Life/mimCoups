package com.mimcoups.Consumer.fragment;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.notificationDetailInteface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentNotificationList extends Fragment {

	private ImageView imgNotificationListSearch, imgNotificationListQuestions,
			imgNotificationListSetting;
	private RelativeLayout relNotificationListCategory,
			relNotificationListLocation, relNotificationListDeal,
			relNotificationListLoyality, relNotificationListwishlist,
			relNotificationListFriendRequest,
			relNotificationListMimcoupNotification;
	private topBarInteface topbarinterface;
	private notificationDetailInteface notificationDetail;
	private CommonData commonClass;
	private String getUrl = "", strMessaegResponse = "", strStatusCode = "",
			url_Response = "";
	private WebService_Call web_service = new WebService_Call();
	private String strFavoriteCategoryCount = "",
			strFavoriteLocationCount = "", strFavouriteDealCount = "";

	private String iFavoriteCategoryUnReadCount = "",
			iFavoriteLocationUnReadCount = "", iFavoriteDealUnReadCount = "",
			iLoyalDealUnReadCount = "", iWishListUnReadCount = "",
			iMimsUnReadCount = "", iFriendRequestCount = "";
	private String strLoyalDealCount = "", strMimsCount = "",
			StrFriendRequestCount = "", strWishListCount = "";
	private TextView txtCategoryNotificationCount,
			txtLocationNotificationCount, txtDealNotificationCount,
			txtLoyalityNotificationCount, txtWishlistNotificationCount,
			txtFriendRequestNotificationCount, txtMimsNotificationCount;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	async_deel_feed_list as;
	RelativeLayout relmainfragment_notificationlist;

	TextView txtCategoryNotificationunreadCount,
			txtLocationNotificationunreadCount,
			txtDealNotificationunreadunreadCount,
			txtLoyalityNotificationunreadCount,
			txtWishlistNotificationunreadCount, txtMimsNotificationunreadCount,
			txtFriendRequestNotificationunreadCount;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_notificationlist,
				container, false);

		initControls(view);
		try {
			setNotificationCount();
		} catch (Exception e) {
			// TODO: handle exception
		}

		clickEvents();

		return view;
	}

	public void setNotificationCount() {
		if (!(commonClass.checkInternetConnection())) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub

						}
					}, getActivity().getResources().getString(
							R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);
		} else {
			as = new async_deel_feed_list(new asynctaskloaddata() {

				@Override
				public void onPreExecute() {
					// TODO Auto-generated method stub
					if (!DeelFeedActivity.progressbarshowing) {
						topbarinterface.showDialog("Loading");
					}

				}

				@Override
				public void doInBackground() {
					// TODO Auto-generated method stub
					getUrl = "NotificationService.svc/GetAllNotificationCount?languageid="
							+ commonClass.languageId
							+ "&customerid="
							+ commonClass.CustomerId;

					// Log.d("notification count>>", "" + getUrl);
					url_Response = web_service.makeServicegetRequest(getUrl);
					try {
						JSONObject jsonObject = new JSONObject(url_Response);
						strStatusCode = jsonObject.getString("vStatusCode");
						strMessaegResponse = jsonObject
								.getString("vMessageResponse");

						if (strStatusCode.equals("100")) {
							strFavoriteCategoryCount = jsonObject
									.getString("iFavoriteCategoryCount");
							strFavoriteLocationCount = jsonObject
									.getString("iFavoriteLocationCount");
							strFavouriteDealCount = jsonObject
									.getString("iFavoriteDealCount");
							strLoyalDealCount = jsonObject
									.getString("iLoyalDealCount");
							strMimsCount = jsonObject.getString("iMimsCount");
							StrFriendRequestCount = jsonObject
									.getString("iFriendRequestCount");
							strWishListCount = jsonObject
									.getString("iWishListCount");

							iFavoriteCategoryUnReadCount = jsonObject
									.getString("iFavoriteCategoryUnReadCount");

							iFavoriteLocationUnReadCount = jsonObject
									.getString("iFavoriteLocationUnReadCount");

							iFavoriteDealUnReadCount = jsonObject
									.getString("iFavoriteDealUnReadCount");

							iLoyalDealUnReadCount = jsonObject
									.getString("iLoyalDealUnReadCount");

							iWishListUnReadCount = jsonObject
									.getString("iWishListUnReadCount");
							iMimsUnReadCount = jsonObject
									.getString("iMimsUnReadCount");
							iFriendRequestCount = jsonObject
									.getString("iFriendRequestUnReadCount");

						}
					} catch (Exception e) {

					}
				}

				@Override
				public void onPostExecute() {
					// TODO Auto-generated method stub
					topbarinterface.hideDialog();

					if (!(commonClass.checkInternetConnection())) {
						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub

									}
								}, getActivity().getResources().getString(
										R.string.InternetConnect));
						dsp.show(getFragmentManager(), "");
						dsp.setCancelable(false);
					} else {
						if (strStatusCode.equals("420")) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											// topbarinterface.topbarClick("back");
										}
									}, getActivity().getResources().getString(
											R.string.PoorInternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						} else if (strStatusCode.equals("100")) {
							txtCategoryNotificationCount
									.setText(strFavoriteCategoryCount
											+ "  "
											+ getActivity()
													.getResources()
													.getString(
															R.string.dealfeedbottomNotifications));
							txtLocationNotificationCount
									.setText(strFavoriteLocationCount
											+ "  "
											+ getActivity()
													.getResources()
													.getString(
															R.string.dealfeedbottomNotifications));
							txtDealNotificationCount
									.setText(strFavouriteDealCount
											+ "  "
											+ getActivity()
													.getResources()
													.getString(
															R.string.dealfeedbottomNotifications));
							txtLoyalityNotificationCount
									.setText(strLoyalDealCount
											+ "  "
											+ getActivity()
													.getResources()
													.getString(
															R.string.dealfeedbottomNotifications));
							txtWishlistNotificationCount
									.setText(strWishListCount
											+ "  "
											+ getActivity()
													.getResources()
													.getString(
															R.string.dealfeedbottomNotifications));
							txtFriendRequestNotificationCount
									.setText(StrFriendRequestCount
											+ "  "
											+ getActivity()
													.getResources()
													.getString(
															R.string.dealfeedbottomNotifications));
							txtMimsNotificationCount
									.setText(strMimsCount
											+ "  "
											+ getActivity()
													.getResources()
													.getString(
															R.string.dealfeedbottomNotifications));

							txtCategoryNotificationunreadCount
									.setText(getActivity().getResources()
											.getString(R.string.unread)
											+ " : "
											+ iFavoriteCategoryUnReadCount);

							txtLocationNotificationunreadCount
									.setText(getActivity().getResources()
											.getString(R.string.unread)
											+ " : "
											+ iFavoriteLocationUnReadCount);

							txtDealNotificationunreadunreadCount
									.setText(getActivity().getResources()
											.getString(R.string.unread)
											+ " : "
											+ iFavoriteDealUnReadCount);

							txtLoyalityNotificationunreadCount
									.setText(getActivity().getResources()
											.getString(R.string.unread)
											+ " : "
											+ iLoyalDealUnReadCount);

							txtWishlistNotificationunreadCount
									.setText(getActivity().getResources()
											.getString(R.string.unread)
											+ " : "
											+ iWishListUnReadCount);

							txtMimsNotificationunreadCount
									.setText(getActivity().getResources()
											.getString(R.string.unread)
											+ " : "
											+ iMimsUnReadCount);

							txtFriendRequestNotificationunreadCount
									.setText(getActivity().getResources()
											.getString(R.string.unread)
											+ " : "
											+ iFriendRequestCount);
						} else {

							if (!strMessaegResponse.equalsIgnoreCase("")) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
											}
										}, strMessaegResponse);
								dsp.show(getFragmentManager(), "");
							}
						}
					}

				}
			});
			as.execute();
		}
	}

	private void initControls(View view) {

		commonClass = (CommonData) getActivity().getApplicationContext();

		relmainfragment_notificationlist = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_notificationlist);
		imgNotificationListSearch = (ImageView) view
				.findViewById(R.id.imgNotificationListSearch);
		imgNotificationListQuestions = (ImageView) view
				.findViewById(R.id.imgNotificationListQuestions);
		imgNotificationListSetting = (ImageView) view
				.findViewById(R.id.imgNotificationListSetting);
		relNotificationListCategory = (RelativeLayout) view
				.findViewById(R.id.relNotificationListCategory);
		relNotificationListLocation = (RelativeLayout) view
				.findViewById(R.id.relNotificationListLocation);
		relNotificationListDeal = (RelativeLayout) view
				.findViewById(R.id.relNotificationListDeal);
		relNotificationListLoyality = (RelativeLayout) view
				.findViewById(R.id.relNotificationListLoyality);
		relNotificationListwishlist = (RelativeLayout) view
				.findViewById(R.id.relNotificationListwishlist);
		relNotificationListFriendRequest = (RelativeLayout) view
				.findViewById(R.id.relNotificationListFriendRequest);
		relNotificationListMimcoupNotification = (RelativeLayout) view
				.findViewById(R.id.relNotificationListMimcoupNotification);
		txtCategoryNotificationCount = (TextView) view
				.findViewById(R.id.txtCategoryNotificationCount);
		txtLocationNotificationCount = (TextView) view
				.findViewById(R.id.txtLocationNotificationCount);
		txtDealNotificationCount = (TextView) view
				.findViewById(R.id.txtDealNotificationCount);
		txtLoyalityNotificationCount = (TextView) view
				.findViewById(R.id.txtLoyalityNotificationCount);
		txtWishlistNotificationCount = (TextView) view
				.findViewById(R.id.txtWishlistNotificationCount);
		txtFriendRequestNotificationCount = (TextView) view
				.findViewById(R.id.txtFriendRequestNotificationCount);
		txtMimsNotificationCount = (TextView) view
				.findViewById(R.id.txtMimsNotificationCount);

		txtCategoryNotificationunreadCount = (TextView) view
				.findViewById(R.id.txtCategoryNotificationunreadCount);

		txtLocationNotificationunreadCount = (TextView) view
				.findViewById(R.id.txtLocationNotificationunreadCount);
		txtDealNotificationunreadunreadCount = (TextView) view
				.findViewById(R.id.txtDealNotificationunreadunreadCount);
		txtLoyalityNotificationunreadCount = (TextView) view
				.findViewById(R.id.txtLoyalityNotificationunreadCount);
		txtWishlistNotificationunreadCount = (TextView) view
				.findViewById(R.id.txtWishlistNotificationunreadCount);
		txtMimsNotificationunreadCount = (TextView) view
				.findViewById(R.id.txtMimsNotificationunreadCount);
		txtFriendRequestNotificationunreadCount = (TextView) view
				.findViewById(R.id.txtFriendRequestNotificationunreadCount);
	}

	private void clickEvents() {
		imgNotificationListSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topbarinterface.topbarClick("search");
				}

			}
		});
		imgNotificationListQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topbarinterface.topbarClick("questions");
				}

			}
		});
		imgNotificationListSetting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topbarinterface.topbarClick("notificationSetting");
				}

			}
		});
		relNotificationListCategory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					notificationDetail.notificationType("Cateogry");
				}

			}
		});
		relNotificationListLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					notificationDetail.notificationType("onsite");
				}
			}
		});
		relNotificationListDeal.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					notificationDetail.notificationType("Deal");
				}
			}
		});
		relNotificationListLoyality.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					notificationDetail.notificationType("Loyality");
				}
			}
		});
		relNotificationListwishlist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					notificationDetail.notificationType("wishlist");
				}
			}
		});
		relNotificationListFriendRequest
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							lastClickTime = currentTime;
							notificationDetail
									.notificationType("FriendRequest");
						}
					}
				});

		relNotificationListMimcoupNotification
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							lastClickTime = currentTime;
							notificationDetail.notificationType("Mimcoups");
						}
					}
				});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topbarinterface = (topBarInteface) activity;
		notificationDetail = (notificationDetailInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
		if (as != null) {
			// Log.d("async cancel", "async canel");
			as.cancel(true);
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
}