package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.DealReviewAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.customClass.ReviewDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.squareup.picasso.Picasso;

public class FragmentWriteReviewLocation extends Fragment {

	private ImageView imgReviewBack, imgReviewQuestions, imgDealImage;
	private topBarInteface topInterface;
	private ListView lstReview;
	private ArrayList<ReviewDetail> arrReviews = new ArrayList<ReviewDetail>();
	private DealReviewAdapter adapter;
	private LocationList locationlist;
	private CommonData commondata;
	private String url_response = "", geturl = "", statusCode = "",
			messgeResponse = "", strPageIndex = "1";
	private RatingBar RtRatings;
	private EditText edtReviews;
	private Button btnSubmit;
	// private TextView txtdealfeeddicountprice, txtStoreName, txtDealType,
	// txtDealDetail;

	private TextView txtDealName_rowlocation, txtDealDtane_rowLocation,
			txtLocationDeals_rowlocation, txtloc_desc_rowlocation, txtDeelFeed,
			txtDealDistane_rowLocation;

	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	LinearLayout txtReviewLocationNoData;
	private WebService_Call web_Service = new WebService_Call();
	ReviewDetail Reviewcustomer = new ReviewDetail();

	View ListHeaderView;
	TextView txt_writereview_star;

	RelativeLayout relmainfragment_writereview_location;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_writereview_location,
				container, false);
		locationlist = (LocationList) getArguments().getSerializable(
				"LocationDetail");

		lstReview = (ListView) view.findViewById(R.id.lstReviews);

		LayoutInflater inflate1 = (LayoutInflater) getActivity()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ListHeaderView = inflate1.inflate(
				R.layout.fragment_writereview_location_header, null);
		lstReview.addHeaderView(ListHeaderView);
		adapter = new DealReviewAdapter(getActivity(), arrReviews);
		lstReview.setAdapter(adapter);

		initControls(ListHeaderView, view);
		clickEvents();
		getAllRatings();

		return view;
	}

	private void initControls(View view, View MainView) {

		relmainfragment_writereview_location = (RelativeLayout) MainView
				.findViewById(R.id.relmainfragment_writereview_location);

		txtDealDistane_rowLocation = (TextView) MainView
				.findViewById(R.id.txtDealDistane_rowLocation);

		imgDealImage = (ImageView) view
				.findViewById(R.id.imgLocationImage_row_location);

		txt_writereview_star = (TextView) MainView
				.findViewById(R.id.txt_writereview_star);
		txtDealName_rowlocation = (TextView) view
				.findViewById(R.id.txtDealName_rowlocation);
		txtDealDtane_rowLocation = (TextView) view
				.findViewById(R.id.txtDealDtane_rowLocation);
		txtLocationDeals_rowlocation = (TextView) view
				.findViewById(R.id.txtLocationDeals_rowlocation);
		txtloc_desc_rowlocation = (TextView) view
				.findViewById(R.id.txtloc_desc_rowlocation);
		txtReviewLocationNoData = (LinearLayout) view
				.findViewById(R.id.txtReviewLocationNoData);

		txtDeelFeed = (TextView) MainView.findViewById(R.id.txtDeelFeed);
		imgReviewBack = (ImageView) MainView.findViewById(R.id.imgReviewBack);
		imgReviewQuestions = (ImageView) MainView
				.findViewById(R.id.imgReviewQuestions);
		lstReview = (ListView) MainView.findViewById(R.id.lstReviews);
		RtRatings = (RatingBar) MainView
				.findViewById(R.id.Rt_writereview_rating);
		edtReviews = (EditText) MainView.findViewById(R.id.edtReviews);
		btnSubmit = (Button) MainView.findViewById(R.id.btn_writereview_submit);
		commondata = (CommonData) getActivity().getApplicationContext();

		// set data

		Picasso.with(getActivity()).load(locationlist.vStoreListImage_A_70X70)
				.placeholder(R.drawable.fashion_header_icon_star_selected)
				.into(imgDealImage);
		txtDealName_rowlocation.setText(locationlist.vMerchantStoreName);
		txtDeelFeed.setText(locationlist.vMerchantStoreName);
		txtDealDistane_rowLocation.setText(locationlist.dDistance);
		txtLocationDeals_rowlocation.setText(locationlist.iDealCount);

		txtloc_desc_rowlocation.setText(locationlist.vShortDescription);

		// txtdealfeeddicountprice.setText(dealdetail.dDealOrigianlAmount);
		// txtStoreName.setText(dealdetail.StoreName);
		// txtDealType.setText(dealdetail.dealName);
		// txtDealDetail.setText(dealdetail.shortDescription);
	}

	private void clickEvents() {

		relmainfragment_writereview_location
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		RtRatings.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					float touchPositionX = event.getX();
					float width = RtRatings.getWidth();
					float starsf = (touchPositionX / width) * 5.0f;
					int stars = (int) starsf + 1;

					if (stars >= 1 && stars <= 5) {
						txt_writereview_star.setText(String.valueOf(stars)
								+ " Stars");
						RtRatings.setRating(stars);
					}
					if (stars == 0) {
						txt_writereview_star.setText(String.valueOf(stars)
								+ " Star");
						RtRatings.setRating(stars);
					}

					v.setPressed(false);
				}
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					v.setPressed(true);
				}

				if (event.getAction() == MotionEvent.ACTION_CANCEL) {
					v.setPressed(false);
				}

				return true;
			}
		});
		imgReviewBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("back");
			}
		});
		imgReviewQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				View view = getActivity().getCurrentFocus();
				if (view != null) {
					InputMethodManager inputManager = (InputMethodManager) getActivity()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
				}

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("questions");
				}
			}
		});
		btnSubmit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					if (txt_writereview_star.getText().toString()
							.equals("0 Star")) {
						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {

									}
								}, getActivity().getResources().getString(
										R.string.ReviewStar));
						dsp.show(getFragmentManager(), "");
					} else if (edtReviews.getText().toString().trim()
							.equals("")) {
						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub

									}
								}, getResources().getString(
										R.string.reviewValidation));
						dsp.show(getFragmentManager(), "");
					} else if (!(commondata.checkInternetConnection())) {
						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub

									}
								}, getResources().getString(
										R.string.InternetConnect));
						dsp.show(getFragmentManager(), "");
						dsp.setCancelable(false);
					} else {
						View view = getActivity().getCurrentFocus();
						if (view != null) {
							InputMethodManager inputManager = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							inputManager.hideSoftInputFromWindow(
									view.getWindowToken(),
									InputMethodManager.HIDE_NOT_ALWAYS);
						}

						if (!commondata.checkInternetConnection()) {
							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							dsp.show(getFragmentManager(), "");
							dsp.setCancelable(false);
						} else {
							addRatingWebservice();
						}
					}
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void addRatingWebservice() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						topInterface.showDialog("Loading");
						url_response = "";
						geturl = "";
						statusCode = "";
						messgeResponse = "";
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						geturl = "LocationService.svc/AddStoreRatingANDReview";
						url_response = web_Service
								.makeServiceAddLocationFavourites(geturl,
										locationlist.iMerchantStoreId,
										commondata.CustomerId,
										commondata.languageId,
										"" + Math.round(RtRatings.getRating()),
										edtReviews.getText().toString().trim());
						try {
							JSONObject mainObject = new JSONObject(url_response);
							statusCode = mainObject.getString("vStatusCode");
							messgeResponse = mainObject
									.getString("messgeResponse");
						} catch (Exception e) {

						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						topInterface.hideDialog();
						if (statusCode.equals("420")) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.PoorInternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						} else if (statusCode.equals("100")) {

							strPageIndex = "1";
							getAllRatings();

							edtReviews.setText("");

							((DeelFeedActivity) getActivity()).flDealRatingCount = RtRatings
									.getRating();
							((DeelFeedActivity) getActivity()).isReviewUpdated = true;
						} else {
							DialogFragment dsp = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
											edtReviews.setText("");
										}
									}, url_response);
							dsp.show(getFragmentManager(), "");
						}

					}
				});
		as.execute();
	}

	public void getAllRatings() {
		if (!(commondata.checkInternetConnection())) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub

						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						Float totalrating = 0f;
						float averagerating = 0f;

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInterface.showDialog("Loading");
							arrReviews.clear();
							Reviewcustomer = new ReviewDetail();
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							geturl = "LocationService.svc/MerchantStoreAllRatingsANDReviews?customerid="
									+ commondata.CustomerId
									+ "&languageid="
									+ commondata.languageId
									+ "&merchantstoreid="
									+ locationlist.iMerchantStoreId
									+ "&startindex=" + strPageIndex;
							url_response = web_Service
									.makeServicegetRequest(geturl);
							try {
								JSONObject mainObject = new JSONObject(
										url_response);
								statusCode = mainObject
										.getString("vStatusCode");
								JSONArray arrayFriend = mainObject
										.getJSONArray("Reviews");
								strPageIndex = mainObject
										.getString("iLastIndex");

								if (statusCode.equals("100")) {
									JSONObject jsonObject = mainObject
											.getJSONObject("CustomerReview");
									Reviewcustomer.strRatingCount = jsonObject
											.getString("iRating");
									Reviewcustomer.strCustomerName = jsonObject
											.getString("vCustomerName");
									Reviewcustomer.strProfileImageUrl = jsonObject
											.getString("vProfileImageName");
									Reviewcustomer.strReviewMessage = jsonObject
											.getString("vReviewMessage");
									for (int i = 0; i < arrayFriend.length(); i++) {
										JSONObject tempObject = arrayFriend
												.getJSONObject(i);
										ReviewDetail reviewDetail = new ReviewDetail();
										reviewDetail.strCustomerName = tempObject
												.getString("vCustomerName");
										reviewDetail.strProfileImageUrl = tempObject
												.getString("vProfileImageName");
										reviewDetail.strRatingCount = tempObject
												.getString("iRating");
										reviewDetail.strReviewMessage = tempObject
												.getString("vReviewMessage");

										totalrating = totalrating
												+ Float.parseFloat(reviewDetail.strRatingCount);
										arrReviews.add(reviewDetail);
									}
								}
								averagerating = totalrating
										/ arrayFriend.length();

							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {

							topInterface.hideDialog();

							if (!(commondata.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub

											}
										}, getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							} else {
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topInterface
															.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {

									int averagerating_int = (int) averagerating;

									((DeelFeedActivity) getActivity()).flDealRatingCount = Float
											.parseFloat("" + averagerating_int);

									((DeelFeedActivity) getActivity()).flDealReviewCount = arrReviews
											.size();

									txt_writereview_star.setText(""
											+ Reviewcustomer.strRatingCount
											+ " Star");
									RtRatings.setRating(Float
											.parseFloat(Reviewcustomer.strRatingCount));
									edtReviews
											.setText(Reviewcustomer.strReviewMessage);
									adapter = new DealReviewAdapter(
											getActivity(), arrReviews);
									lstReview.setAdapter(adapter);
									txtReviewLocationNoData
											.setVisibility(View.GONE);
								} else {
									txtReviewLocationNoData
											.setVisibility(View.VISIBLE);
								}
							}

						}
					});
			as.execute();
		}
	}
}
