package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.DoneDeelFeedAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.topBarInteface;

public class FragmentSliderDoneDeals extends Fragment {

	private int preLast;
	private boolean loadmore = true;
	private topBarInteface topInterface;
	private ListView lstDealFeedDeals;
	private DoneDeelFeedAdapter adapterDealFeed;
	private ArrayList<DealFeedDetail> arrDealFeeds = new ArrayList<DealFeedDetail>();
	private ImageView imgDealFeedQuestions, imgDealFeedSearch,
			imgDealFeedClose;
	private TextView txtDoneDealFeed;
	private FrameLayout dealfeedbgcontainer;
	// private SwipeRefreshLayout swipeLayout;
	private String strGeturl = "", strStatusCode = "", strMessageResponse,
			urlResponse = "", vExpiredDealMessage = "", StrPageIndex = "1";
	private CommonData commonClass;
	private WebService_Call web_service = new WebService_Call();
	private ArrayList<Integer> arrselectedCategory = new ArrayList<Integer>();
	RelativeLayout relmainfragment_slider_donedeals;
	LinearLayout lnEmptyView, lnLoadMore;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	View views;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_donedeals,
				container, false);

		initControls(view);
		views = inflater.inflate(R.layout.loadmore, null);

		
		Clickevents();

		setblankAdapter();
		if (!commonClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topInterface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			loadDeals();
		}
		return view;
	}

	@SuppressLint("NewApi")
	private void initControls(View view) {
		relmainfragment_slider_donedeals = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_donedeals);

		commonClass = (CommonData) getActivity().getApplicationContext();
		imgDealFeedClose = (ImageView) view.findViewById(R.id.imgDealFeedClose);
		// txtDeelFeedNoData = (TextView) view
		// .findViewById(R.id.txtDeelFeedNoData);

		imgDealFeedQuestions = (ImageView) view
				.findViewById(R.id.imgDealFeedQuestions);
		imgDealFeedSearch = (ImageView) view
				.findViewById(R.id.imgDealFeedSearch);
		lstDealFeedDeals = (ListView) view.findViewById(R.id.list);
		lnEmptyView = (LinearLayout) view.findViewById(R.id.empty);
		lnLoadMore = (LinearLayout) view.findViewById(R.id.lnLoadMore);

		// image view android

		txtDoneDealFeed = (TextView) view.findViewById(R.id.txtDoneDealFeed);
		dealfeedbgcontainer = (FrameLayout) view
				.findViewById(R.id.dealfreedbgcontainer);

		// swipeLayout = (SwipeRefreshLayout) view
		// .findViewById(R.id.swipe_container);

		// swipeLayout.setColorScheme(android.R.color.holo_blue_bright,
		// android.R.color.holo_green_light,
		// android.R.color.holo_orange_light,
		// android.R.color.holo_red_light);

	}

	private void Clickevents() {

		lstDealFeedDeals.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// TODO Auto-generated method stub

				final int lastItem = firstVisibleItem + visibleItemCount;
				if (lastItem == totalItemCount) {

					if ((preLast != lastItem) && loadmore) {

						if (!commonClass.checkInternetConnection()) {
							// DialogFragment ds = new SingleButtonAlert(
							// new DialogInterfaceClick() {
							//
							// @Override
							// public void dialogClick(String tag) {
							//
							// topInterface.topbarClick("back");
							// }
							// }, getResources().getString(
							// R.string.InternetConnect));
							// ds.show(getFragmentManager(), "");
						} else {

							// lnLoadMore.setVisibility(View.VISIBLE);
							loadmore = false;
							loadmoredonedeals();
						}
					}
				}

			}
		});

		relmainfragment_slider_donedeals
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});

		imgDealFeedSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("search");
			}
		});

		imgDealFeedQuestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					topInterface.topbarClick("questions");
				}
			}
		});

		imgDealFeedClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				imgDealFeedClose.setVisibility(View.GONE);
				imgDealFeedQuestions.setVisibility(View.VISIBLE);
				txtDoneDealFeed.setText(getResources().getString(
						R.string.dealfeedname));

				setblankAdapter();
				loadDeals();
			}
		});

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void loadDeals() {

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {
						
						if (!DeelFeedActivity.progressbarshowing) {
							topInterface.showDialog("Loading");
						}

					}

					@Override
					public void doInBackground() {

						strGeturl = "OrderService.svc/GetCustomerDoneDealDetails?customerid="
								+ commonClass.CustomerId
								+ "&languageid="
								+ commonClass.languageId
								+ "&startindex="
								+ StrPageIndex;
						final String strTempUrl = commonClass
								.Url_EncodeWithBlank(strGeturl);
						// Log.e("Done deal", strTempUrl);

						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);

						try {

							JSONObject json_Object = new JSONObject(urlResponse);

							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");

							if (strStatusCode.equals("100")) {
								vExpiredDealMessage = json_Object
										.getString("vExpiredDealMessage");
								StrPageIndex = json_Object
										.getString("iLastIndex");

								JSONArray json_result = json_Object
										.getJSONArray("vObjCustomerDetails");
								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail.iMerchantDealId = json_deal
											.getString("iMerchantDealId");
									dealDetail.iLocationId = json_deal
											.getString("iMerchantStoreId");
									dealDetail.vMerchantStoreName = json_deal
											.getString("vStorename");
									dealDetail.vExpiredDealMessage = vExpiredDealMessage;

									dealDetail.vMerchantDealName = json_deal
											.getString("vDealName");
									dealDetail.dDealOrigianlAmount = json_deal
											.getString("vPurchaseamount");
									dealDetail.dDealDiscountedAmount = json_deal
											.getString("vOrderamount");
									dealDetail.vShortDescriptionHeading = json_deal
											.getString("vShortDescriptionHeading");
									dealDetail.vShortDescription = json_deal
											.getString("vDealShotDescription");
									dealDetail.vDealListImage_A_350X350 = json_deal
											.getString("vDealListImage_A_350X350");
									dealDetail.vBoughtTime = json_deal
											.getString("vBoughtTime");
									dealDetail.bDealStatus = json_deal
											.getBoolean("bDealStatus");

									arrDealFeeds.add(dealDetail);

								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {

						// lnLoadMore.setVisibility(View.GONE);
						topInterface.hideDialog();
						
						

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (strStatusCode.equals("100")) {
								lstDealFeedDeals.addFooterView(views);

								loadmore = true;

								adapterDealFeed.notifyDataSetChanged();
							}

							else if (strStatusCode.equals("118")) {
								loadmore = false;
							} else if (strStatusCode.equals("102")) {

								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, strMessageResponse);
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							if (adapterDealFeed.getCount() == 0) {
								lnEmptyView.setVisibility(View.VISIBLE);
								lstDealFeedDeals.setVisibility(View.GONE);
							}
						}

					}
				});
		as.execute();
	}
	
	public void loadmoredonedeals(){


		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					@Override
					public void onPreExecute() {

					}

					@Override
					public void doInBackground() {

						strGeturl = "OrderService.svc/GetCustomerDoneDealDetails?customerid="
								+ commonClass.CustomerId
								+ "&languageid="
								+ commonClass.languageId
								+ "&startindex="
								+ StrPageIndex;
						final String strTempUrl = commonClass
								.Url_EncodeWithBlank(strGeturl);
						// Log.e("Done deal", strTempUrl);

						urlResponse = web_service
								.makeServicegetRequest(strTempUrl);

						try {

							JSONObject json_Object = new JSONObject(urlResponse);

							strStatusCode = json_Object
									.getString("vStatusCode");
							strMessageResponse = json_Object
									.getString("vMessageResponse");

							if (strStatusCode.equals("100")) {
								vExpiredDealMessage = json_Object
										.getString("vExpiredDealMessage");
								StrPageIndex = json_Object
										.getString("iLastIndex");

								JSONArray json_result = json_Object
										.getJSONArray("vObjCustomerDetails");
								for (int i = 0; i < json_result.length(); i++) {
									JSONObject json_deal = json_result
											.getJSONObject(i);
									DealFeedDetail dealDetail = new DealFeedDetail();
									dealDetail.iMerchantDealId = json_deal
											.getString("iMerchantDealId");
									dealDetail.iLocationId = json_deal
											.getString("iMerchantStoreId");
									dealDetail.vMerchantStoreName = json_deal
											.getString("vStorename");
									dealDetail.vExpiredDealMessage = vExpiredDealMessage;

									dealDetail.vMerchantDealName = json_deal
											.getString("vDealName");
									dealDetail.dDealOrigianlAmount = json_deal
											.getString("vPurchaseamount");
									dealDetail.dDealDiscountedAmount = json_deal
											.getString("vOrderamount");
									dealDetail.vShortDescriptionHeading = json_deal
											.getString("vShortDescriptionHeading");
									dealDetail.vShortDescription = json_deal
											.getString("vDealShotDescription");
									dealDetail.vDealListImage_A_350X350 = json_deal
											.getString("vDealListImage_A_350X350");
									dealDetail.vBoughtTime = json_deal
											.getString("vBoughtTime");
									dealDetail.bDealStatus = json_deal
											.getBoolean("bDealStatus");

									arrDealFeeds.add(dealDetail);

								}
							}
						} catch (Exception e) {
						}
					}

					@Override
					public void onPostExecute() {

						// lnLoadMore.setVisibility(View.GONE);
						try {
							lstDealFeedDeals.removeFooterView(views);	
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						

						if (!commonClass.checkInternetConnection()) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

										}
									}, getResources().getString(
											R.string.InternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);
						} else {
							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topInterface
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							else if (strStatusCode.equals("100")) {
								lstDealFeedDeals.addFooterView(views);

								loadmore = true;

								adapterDealFeed.notifyDataSetChanged();
							}

							else if (strStatusCode.equals("118")) {
								loadmore = false;
							} else if (strStatusCode.equals("102")) {

								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												
											}
										}, strMessageResponse);
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							}

							if (adapterDealFeed.getCount() == 0) {
								lnEmptyView.setVisibility(View.VISIBLE);
								lstDealFeedDeals.setVisibility(View.GONE);
							}
						}

					}
				});
		as.execute();
	
	}

	public void setblankAdapter() {
		adapterDealFeed = new DoneDeelFeedAdapter(getActivity(), arrDealFeeds);
		// lstDealFeedDeals.setAdapter(adapterDealFeed);
		// setListAdapter(adapterDealFeed);

		lstDealFeedDeals.setAdapter(adapterDealFeed);
	}

}