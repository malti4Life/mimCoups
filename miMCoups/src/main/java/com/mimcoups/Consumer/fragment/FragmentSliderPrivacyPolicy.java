package com.mimcoups.Consumer.fragment;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

public class FragmentSliderPrivacyPolicy extends Fragment {
	private ImageView imgSliderPrivacySearch, imgSliderPrivacyPolicyQuestions;
	private topBarInteface topInterface;
	private WebView txtSliderPrivacyDetail;
	private CommonData commondata;
	private WebService_Call web_service = new WebService_Call();
	private String url_response = "";
	private String strPrivacyPolicy = "", StatusCode = "", strMessage = "";
	RelativeLayout relmainfragment_slider_privacy;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_slider_privacy,
				container, false);
		initControls(view);
		clickEvents();
		return view;
	}

	private void initControls(View view) {

		relmainfragment_slider_privacy = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_privacy);

		imgSliderPrivacySearch = (ImageView) view
				.findViewById(R.id.imgSliderPrivacySearch);
		imgSliderPrivacyPolicyQuestions = (ImageView) view
				.findViewById(R.id.imgSliderPrivacyPolicyQuestions);
		txtSliderPrivacyDetail = (WebView) view
				.findViewById(R.id.txtSliderPrivacyDetail);
		commondata = (CommonData) getActivity().getApplicationContext();
		if (commondata.checkInternetConnection()) {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {
						@Override
						public void onPreExecute() {
							topInterface.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							String getUrl = "CustomerService.svc/GetPrivacyPolicyAgreement?languageid="
									+ commondata.languageId;
							url_response = web_service
									.makeServicegetRequest(getUrl);
							try {
								JSONObject js_obj = new JSONObject(url_response);
								commondata.languageId = js_obj
										.getString("iLanguageId");
								strPrivacyPolicy = js_obj
										.getString("vAgreementDescription");
								StatusCode = js_obj.getString("vStatusCode");
								strMessage = js_obj
										.getString("vMessageResponse");
							} catch (Exception e) {
							}
						}

						@Override
						public void onPostExecute() {
							topInterface.hideDialog();

							if (!commondata.checkInternetConnection()) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub
												topInterface
														.topbarClick("back");
											}
										},
										getActivity()
												.getResources()
												.getText(
														R.string.InternetConnect)
												.toString());
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);
							} else {
								if (StatusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topInterface
															.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (StatusCode.equals("100")) {
									txtSliderPrivacyDetail.loadData(
											strPrivacyPolicy, "text/html",
											"UTF-8");

								} else {
									txtSliderPrivacyDetail.loadData(strMessage,
											"text/html", "UTF-8");

								}
							}

						}
					});
			as.execute();
		} else {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub
							topInterface.topbarClick("back");
						}
					}, getActivity().getResources()
							.getText(R.string.InternetConnect).toString());
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		}
	}

	private void clickEvents() {

		relmainfragment_slider_privacy
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgSliderPrivacySearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.topbarClick("search");
			}
		});
		imgSliderPrivacyPolicyQuestions
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							lastClickTime = currentTime;

							topInterface.topbarClick("questions");
						}
					}
				});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

}