package com.mimcoups.Consumer.fragment;

import java.util.Timer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.inteface.discusschatinteface;

public class FragmentDiscussMerchantChat extends Fragment {
	Button cancel;
	Timer timer;
	CountDownTimer countdowntimer;
	Boolean isrunning;
	Boolean onfinish;
	SharedPreferences preferences;
	public CommonData commonData;
	private Context context;
	private topBarInteface topbarInterface;
	private discusschatinteface discussChatInterface;
	public final String BROADCAST = "com.mimcoups.android.action.broadcast";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_discuss_merchantchat,
				container, false);

		// async_deel_feed_list as=new async_deel_feed_list(new
		// asynctaskloaddata() {
		//
		// @Override
		// public void onPreExecute() {
		// // TODO Auto-generated method stub
		// topbarInterface.showDialog("Loadiing");
		// }
		//
		// @Override
		// public void doInBackground() {
		// // TODO Auto-generated method stub
		// IntentFilter intentFilter = new IntentFilter(BROADCAST);
		// context.registerReceiver(((DeelFeedActivity)getActivity()).myReceiver,
		// intentFilter);
		// Intent i = new Intent(context, Connector.class);
		// context.startService(i);
		// }
		// @Override
		// public void onPostExecute()
		// {
		// }
		// });
		// as.execute();
		isrunning = true;
		preferences = PreferenceManager.getDefaultSharedPreferences(context);

		// cancel = (Button)view.findViewById(R.id.cancel);
		// cancel.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		//
		// if (isrunning) {
		//
		// if (countdowntimer != null)
		// {
		// commonData.logDisplay("first", "t"+isrunning);
		// countdowntimer.cancel();
		// countdowntimer = null;
		// isrunning = false;
		// sendrejectmessage();
		// commonData.logDisplay("isRunning", "t"+isrunning);
		// topbarInterface.topbarClick("back");
		// }
		// }
		// }
		// });
		return view;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topbarInterface = (topBarInteface) activity;
		discussChatInterface = (discusschatinteface) activity;
		context = activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	public void counterTimerCancelback() {
		if (isrunning) {
			if (countdowntimer != null) {
				countdowntimer.cancel();
				countdowntimer = null;
				// sendrejectmessage();
			}

		}
	}
}