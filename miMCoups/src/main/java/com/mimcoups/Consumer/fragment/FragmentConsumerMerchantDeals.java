package com.mimcoups.Consumer.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.Consumer.adapter.ConsumerMerchantShowDealsAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.LocationDetail;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.inteface.LocationDetailInterface;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.locationmapinterface;
import com.mimcoups.Consumer.inteface.sharinglocation;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class FragmentConsumerMerchantDeals extends Fragment {
    long MIN_CLICK_INTERVAL = 800; // in millis
    long lastClickTime = 0;
    private ArrayList<DealFeedDetail> arrDealFeeds = new ArrayList<DealFeedDetail>();
    private String strGeturl = "", strStatusCode = "", strMessageResponse,
            urlResponse = "", StrPageIndex = "1", strLastRunDate = "";
    Dialog ImageDialog;
    TextView txtLocAddress_LocationDetail,txt_LocDetail_writereview,txtDeelFeedNoData,txtMerchantStoreName;
    RatingBar ratingBar_locationdetail;
    private LinearLayout ll_LocDetail_Discuss, ll_LocDetail_Contact,
            ll_LocDetail_Connect;
    CommonData globalClass;
    String locationStoreId;
    String serverResponse;
    WebService_Call call_webservice_SliderLocationDetail;
    Context context;
    LocationDetail locationdetail;
    private String statusCode = "", statusMessage = "";
    private LocationList LocationsliderPassDetail;
    DealFeedDetail dealfeed;
    String from;
    String share = "";
    private LocationDetailInterface locationDetailClick;
    private ConsumerMerchantShowDealsAdapter adapterDealFeed;
    private GridView lstDealFeedDeals;
    private ImageView imgLocationdetailBack;
    private topBarInteface topinteface;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

            super.onCreateView(inflater, container, savedInstanceState);
            View view = inflater.inflate(R.layout.fragment_deal_detail_more,
                    container, false);
        Sharing_Dialog();

        call_webservice_SliderLocationDetail = new WebService_Call();
        LocationsliderPassDetail = new LocationList();
        txtMerchantStoreName=(TextView)view
                .findViewById(R.id.merchantStoreName);
        imgLocationdetailBack = (ImageView) view
                .findViewById(R.id.imgLocationdetailBack);
        txtLocAddress_LocationDetail = (TextView) view
                .findViewById(R.id.txtLocAddress_LocationDetail);
        txt_LocDetail_writereview = (TextView) view
                .findViewById(R.id.txt_LocDetail_writereview);
        ratingBar_locationdetail = (RatingBar) view
                .findViewById(R.id.ratingBar_locationdetail);
        ll_LocDetail_Discuss = (LinearLayout) view
                .findViewById(R.id.ll_LocDetail_Discuss);
        ll_LocDetail_Contact = (LinearLayout) view
                .findViewById(R.id.ll_LocDetail_Contact);
        ll_LocDetail_Connect = (LinearLayout) view
                .findViewById(R.id.ll_LocDetail_Connect);

        from = getArguments().getString("from");
        if (from.equalsIgnoreCase("dealsdetail")) {

            dealfeed = (DealFeedDetail) getArguments().getSerializable(
                    "LocationDetail");

            LocationsliderPassDetail = new LocationList();

            locationStoreId = dealfeed.iLocationId;

        } else {
            LocationsliderPassDetail = (LocationList) getArguments()
                    .getSerializable("LocationDetail");
            locationStoreId = LocationsliderPassDetail.iMerchantStoreId;
        }

        if (CommonData.languageId.equalsIgnoreCase("1")) {
            share = CommonData.locationshareUrl_au
                    + locationStoreId;

        } else {
            share = CommonData.locationshareUrl_no
                    + locationStoreId;
        }
        GetData();

        lstDealFeedDeals = (GridView) view.findViewById(R.id.lstDealFeedDeals);

        txtDeelFeedNoData = (TextView) view.findViewById(R.id.textNoDataaa);


            StrPageIndex = "1";
            strGeturl = "LocationService.svc/GetDealsByLocation?customerid="
                    + globalClass.CustomerId + "&languageid="
                    + globalClass.languageId + "&merchantstoreid=" + locationStoreId;
            strGeturl = globalClass.Url_EncodeWithBlank(strGeturl);
        setblankAdapter();
            loadDeals();

        OnClickEvents(view);
        return view;
    }
    public void setblankAdapter() {
        adapterDealFeed = new ConsumerMerchantShowDealsAdapter(getActivity(), arrDealFeeds);
        lstDealFeedDeals.setAdapter(adapterDealFeed);
    }
    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        context = activity;
        topinteface = (topBarInteface) activity;
        locationDetailClick = (LocationDetailInterface) activity;

    }

    private void GetData() {

        async_deel_feed_list as = new async_deel_feed_list(
                new asynctaskloaddata() {

                    @Override
                    public void onPreExecute() {
                    }

                    @Override
                    public void doInBackground() {
                        // TODO Auto-generated method stub

                        try {

                            String serverUrl = "LocationService.svc/GetLocationDetail?customerid="
                                    + globalClass.CustomerId
                                    + "&languageid="
                                    + globalClass.languageId
                                    + "&merchantstoreid="
                                    + locationStoreId
                                    + "&latitude="
                                    + globalClass.currentLatitude
                                    + "&longitude="
                                    + globalClass.currentLongitude;
                            serverResponse = call_webservice_SliderLocationDetail
                                    .makeServicegetRequest(serverUrl);
                            serverResponse = serverResponse
                                    .replace("\\n", "\n");

                            JSONObject js_getres = new JSONObject(
                                    serverResponse);
                            statusCode = js_getres.getString("vStatusCode");
                            if (statusCode.equals("100")) {
                                JSONObject js = new JSONObject(js_getres
                                        .getString("Location"));
                                locationdetail = new LocationDetail();
                                locationdetail.bIsFavorite = js
                                        .getString("bIsFavorite");
                                locationdetail.dDistance = js
                                        .getString("dDistance");
                                locationdetail.dLatitude = js
                                        .getString("dLatitude");
                                locationdetail.dLongitude = js
                                        .getString("dLongitude");
                                locationdetail.iDealCount = js
                                        .getString("iDealCount");
                                locationdetail.iMerchantStoreId = js
                                        .getString("iMerchantStoreId");
                                locationdetail.iMerchantStoreRatings = js
                                        .getString("iMerchantStoreRatings");
                                locationdetail.vAddress = js
                                        .getString("vAddress");
                                locationdetail.vFacebookPageURL = js
                                        .getString("vFacebookPageURL");
                                locationdetail.vGooglePlusURL = js
                                        .getString("vGooglePlusURL");
                                locationdetail.vInstagramURL = js
                                        .getString("vInstagramURL");
                                locationdetail.vMerchantStoreEmail = js
                                        .getString("vMerchantStoreEmail");
                                locationdetail.vMerchantStoreName = js
                                        .getString("vMerchantStoreName");
                                locationdetail.vShortDescription = js
                                        .getString("vShortDescription");
                                locationdetail.vStoreDetailImage_A_650X325 = js
                                        .getString("vStoreDetailImage_A_650X325");
                                locationdetail.vTwitterPageURL = js
                                        .getString("vTwitterPageURL");
                                LocationsliderPassDetail.vStoreDetailImage_A_650X325 = js
                                        .getString("vStoreDetailImage_A_650X325");
                                LocationsliderPassDetail.vStoreListImage_A_70X70 = js
                                        .getString("vStoreListImage_A_70X70");

                                // //location list set Nirmal

                                LocationsliderPassDetail.bIsFavorite = locationdetail.bIsFavorite;
                                LocationsliderPassDetail.dDistance = locationdetail.dDistance;
                                LocationsliderPassDetail.dLatitude = locationdetail.dLatitude;
                                LocationsliderPassDetail.dLongitude = locationdetail.dLongitude;
                                LocationsliderPassDetail.iDealCount = locationdetail.iDealCount;
                                LocationsliderPassDetail.iMerchantStoreId = locationdetail.iMerchantStoreId;
                                // LocationsliderPassDetail.iNextIndex=locationdetail.i
                                // LocationsliderPassDetail.iRowNumber=locationdetail.i

                                LocationsliderPassDetail.vAddress = locationdetail.vAddress;
                                LocationsliderPassDetail.vMerchantStoreName = locationdetail.vMerchantStoreName;
                                LocationsliderPassDetail.vShortDescription = locationdetail.vShortDescription;
                                // LocationsliderPassDetail.vStoreListImage_A_70X70=locationdetail.vSt
                                LocationsliderPassDetail.vStoreDetailImage_A_650X325 = locationdetail.vStoreDetailImage_A_650X325;

                            } else {
                                statusMessage = js_getres
                                        .getString("vMessageResponse");
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onPostExecute() {
                        // TODO Auto-generated method stub


                       if (statusCode.equals("100")) {
                                SetData();
                            }

                    }

                    private void SetData() {

                        ratingBar_locationdetail.setRating(Float
                                .parseFloat(locationdetail.iMerchantStoreRatings));

                        txtLocAddress_LocationDetail
                                .setText(locationdetail.vAddress);
                        txtMerchantStoreName.setText(locationdetail.vMerchantStoreName);



                    }
                });
        as.execute();
    }
    private void OnClickEvents(View view) {
        ll_LocDetail_Discuss.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                long currentTime = SystemClock.elapsedRealtime();

                if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                    lastClickTime = currentTime;
                    locationDetailClick.loncationClick("discuss",
                            LocationsliderPassDetail);
                }
            }
        });

        ll_LocDetail_Connect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                long currentTime = SystemClock.elapsedRealtime();
                if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                    lastClickTime = currentTime;

                    ImageDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_PopUpMenu_Center;
                    ImageDialog.show();
                }

            }
        });
        ll_LocDetail_Contact.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                long currentTime = SystemClock.elapsedRealtime();
                if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
                    lastClickTime = currentTime;
                    locationDetailClick.loncationClick("locationContact",
                            LocationsliderPassDetail);
                }
            }
        });
        imgLocationdetailBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                topinteface.topbarClick("back");
            }
        });
    }
    private void Sharing_Dialog() {

        try{
            ImageDialog = new Dialog(getActivity());
            ImageDialog.setContentView(R.layout.share_white_vertical_dialog);
            ImageDialog.setCancelable(true);
            ImageDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            int divierId = ImageDialog.getContext().getResources()
                    .getIdentifier("android:id/titleDivider", null, null);
            View divider = ImageDialog.findViewById(divierId);
            divider.setBackgroundColor(android.graphics.Color.TRANSPARENT);

            Button facebookcontainer = (Button) ImageDialog
                    .findViewById(R.id.btnfacebookcontainer);
            Button twittercontainer = (Button) ImageDialog
                    .findViewById(R.id.btntwittercontainer);
            Button googlepluscontainer = (Button) ImageDialog
                    .findViewById(R.id.btngooglepluscontainer);
            Button instagamcontainer = (Button) ImageDialog
                    .findViewById(R.id.btninstagramcontainer);
            Button mimcoupsharing = (Button) ImageDialog
                    .findViewById(R.id.btnmimcoupsharing);
            Button emailsharing = (Button) ImageDialog
                    .findViewById(R.id.btnemailsharing);

            mimcoupsharing.setVisibility(View.GONE);
            emailsharing.setVisibility(View.GONE);

            facebookcontainer.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (!locationdetail.vFacebookPageURL.equals("")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                                .parse(locationdetail.vFacebookPageURL));
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, "Sorry, no media link available !",
                                Toast.LENGTH_LONG).show();
                    }

                }
            });

            twittercontainer.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (!locationdetail.vTwitterPageURL.equals("")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                                .parse(locationdetail.vTwitterPageURL));
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, "Sorry, no media link available !",
                                Toast.LENGTH_LONG).show();
                    }

                }
            });

            googlepluscontainer.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (!locationdetail.vGooglePlusURL.equals("")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                                .parse(locationdetail.vGooglePlusURL));
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, "Sorry, no media link available !",
                                Toast.LENGTH_LONG).show();
                    }

                }
            });

            instagamcontainer.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    if (!locationdetail.vInstagramURL.equals("")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                                .parse(locationdetail.vInstagramURL));
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, "Sorry, no media link available !",
                                Toast.LENGTH_LONG).show();
                    }

                }
            });

            Button btncancel = (Button) ImageDialog.findViewById(R.id.btnCancel);
            btncancel.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    ImageDialog.dismiss();

                }
            });
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(ImageDialog.getWindow().getAttributes());
            DisplayMetrics displaymetrics = new DisplayMetrics();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.gravity = Gravity.BOTTOM;
            getActivity().getWindowManager().getDefaultDisplay()
                    .getMetrics(displaymetrics);
            ImageDialog.getWindow().setAttributes(lp);

        }catch (Exception e){

        }
    }
    public void loadDeals() {

        // GPSTracker gps = new GPSTracker();
        // Location currentloc = gps.getCurrentLocation();
        // double latitude = 0.0;
        // double longiutde = 0.0;
        //
        // if (currentloc != null) {
        // latitude = currentloc.getLatitude();
        // longiutde = currentloc.getLongitude();
        // } else {
        // latitude = commonClass.currentLatitude;
        // longiutde = commonClass.currentLongitude;
        // }

        double latitude = globalClass.currentLatitude;
        double longiutde = globalClass.currentLongitude;

        ArrayList<Double> arrLatitude = ((DeelFeedActivity) getActivity())
                .getLatitudeLongitude();

        latitude = arrLatitude.get(0);
        longiutde = arrLatitude.get(1);

        final String strTempUrl = globalClass.Url_EncodeWithBlank(strGeturl
                + "&startindex=" + StrPageIndex + "&latitude=" + latitude
                + "&longitude=" + longiutde + "&dlastrundate=");

        arrDealFeeds.clear();
        async_deel_feed_list as = new async_deel_feed_list(
                new asynctaskloaddata() {
                    @Override
                    public void onPreExecute() {

                        strStatusCode = "";
                    }

                    @Override
                    public void doInBackground() {

                        try {

                            urlResponse = call_webservice_SliderLocationDetail
                                    .makeServicegetRequest(strTempUrl);
                            Log.e("urlllll>>>", "" + urlResponse);

                            JSONObject json_Object = new JSONObject(urlResponse);
                            JSONArray json_result = json_Object
                                    .getJSONArray("Deals");
                            strStatusCode = json_Object
                                    .getString("vStatusCode");
                            strMessageResponse = json_Object
                                    .getString("vMessageResponse");

                            if (strStatusCode.equals("100")) {

                                StrPageIndex = json_Object
                                        .getString("iLastIndex");
                                for (int i = 0; i < json_result.length(); i++) {
                                    JSONObject json_deal = json_result
                                            .getJSONObject(i);
                                    DealFeedDetail dealDetail = new DealFeedDetail();
                                    dealDetail = dealDetail
                                            .DelFeedDetail(json_deal);
                                    arrDealFeeds.add(dealDetail);
                                }
                            }
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onPostExecute() {
                                lstDealFeedDeals.setVisibility(View.VISIBLE);
                                txtDeelFeedNoData.setVisibility(View.GONE);
                                adapterDealFeed.notifyDataSetChanged();
                        }
                });
        as.execute();
    }

}
