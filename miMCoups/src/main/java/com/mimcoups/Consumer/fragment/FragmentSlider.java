package com.mimcoups.Consumer.fragment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.Session;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.SplashActivity;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.Unbindservice;
import com.mimcoups.Consumer.inteface.sliderinterface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.smackchat.Connector;
import com.squareup.picasso.Picasso;

public class FragmentSlider extends Fragment {

	private LinearLayout llLocation, llCategory, llFavourites, llWishlist,
			llSavings, llDoneDeals, llCampaign, llPrivcayPolicy, llFriend,
			llContactus;
	private sliderinterface sliderInterface;
	private LinearLayout lnLogout, lnUserProfile;
	private CommonData commonData;
	private String url_response = "", strStatusCode = "", strMessage = "";
	private WebService_Call web_service = new WebService_Call();
	private SharedPreferences preference;
	private topBarInteface topInterface;
	Unbindservice unbind;
	static boolean isUserProfile_Updated = false;
	TextView txtUserName;
	ImageView imgUserPic;
	RelativeLayout relmainfragment_slider;
	long MIN_CLICK_INTERVAL = 1000; // in millis
	long lastClickTime = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater
				.inflate(R.layout.fragment_slider, container, false);
		// getActivity().getWindow().setSoftInputMode(
		// WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		initControls(view);
		View v = getActivity().getCurrentFocus();
		if (v != null) {
			InputMethodManager inputManager = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(view.getWindowToken(),
					InputMethodManager.HIDE_IMPLICIT_ONLY);
		}
		clickEvents();
		return view;
	}

	private void initControls(final View view) {
		commonData = (CommonData) getActivity().getApplicationContext();
		llLocation = (LinearLayout) view.findViewById(R.id.llLocation);
		llCategory = (LinearLayout) view.findViewById(R.id.llCategory);
		llFavourites = (LinearLayout) view.findViewById(R.id.llFavourite);
		llWishlist = (LinearLayout) view.findViewById(R.id.llWishlist);
		llSavings = (LinearLayout) view.findViewById(R.id.llsavings);
		llDoneDeals = (LinearLayout) view.findViewById(R.id.llDoneDals);
		llCampaign = (LinearLayout) view.findViewById(R.id.llCampaign);
		llPrivcayPolicy = (LinearLayout) view
				.findViewById(R.id.llPrivacyPolicy);
		relmainfragment_slider = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider);
		llFriend = (LinearLayout) view.findViewById(R.id.llFriends);
		llContactus = (LinearLayout) view.findViewById(R.id.llContactus);
		lnLogout = (LinearLayout) view.findViewById(R.id.lnSliderLogout);
		lnUserProfile = (LinearLayout) view.findViewById(R.id.lnUserProfile);
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						preference = getActivity().getSharedPreferences(
								"UserDetail", Context.MODE_PRIVATE);
						commonData.CustomerId = preference.getString(
								"customerid", "");
						commonData.languageId = preference.getString(
								"languageid", "");
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						try {
							String geturl = "CustomerService.svc/GetProfile?customerid="
									+ commonData.CustomerId
									+ "&languageid="
									+ commonData.languageId;
							geturl = commonData.Url_EncodeWithBlank(geturl);
							url_response = web_service
									.makeServicegetRequest(geturl);
							JSONObject js_obj_getprofileresult = new JSONObject(
									url_response);
							strStatusCode = js_obj_getprofileresult
									.getString("vStatusCode");
							if (strStatusCode.equals("100")) {
								commonData.ProfiileImagePath = js_obj_getprofileresult
										.getString("vProfileImageName");

								SharedPreferences sp = getActivity()
										.getSharedPreferences("UserDetail",
												Context.MODE_PRIVATE);
								sp.edit()
										.putString(
												"profileimagepath",
												js_obj_getprofileresult
														.getString("vProfileImageName"))
										.commit();
								commonData.ProfileName = js_obj_getprofileresult
										.getString("vCustomerName");
								commonData.strUsername = preference.getString(
										"username", "");
								commonData.strPassword = preference.getString(
										"password", "");
							}
						} catch (Exception e) {

						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub

						// async_deel_feed_list as = new async_deel_feed_list(
						// new asynctaskloaddata() {
						// Bitmap bm = null;
						//
						// @Override
						// public void onPreExecute() {
						// // TODO Auto-generated method stub
						//
						// }
						//
						// @Override
						// public void doInBackground() {
						// // TODO Auto-generated method stub
						// bm = getBitmapFromURL(commonData.ProfiileImagePath);
						// }
						//
						// @Override
						// public void onPostExecute() {
						// // TODO Auto-generated method stub
						//
						// if (bm != null) {
						// Bitmap roundshape = getRoundedShape(bm);
						// imgUserPic
						// .setImageBitmap(roundshape);
						//
						// }
						//
						// }
						// });
						// as.execute();

						try {
							// Picasso.with(getActivity())
							// .load(commonData.ProfiileImagePath)
							// .transform(new RoundedTransformation(15, 3))
							// .placeholder(R.drawable.app_icon)
							// .into(imgUserPic);

							txtUserName = (TextView) view
									.findViewById(R.id.txtUsername_slider);
							preference = getActivity().getSharedPreferences(
									"UserDetail", Context.MODE_PRIVATE);
							String customername = preference.getString(
									"customername", "");
							txtUserName.setText(customername);
							imgUserPic = (ImageView) view
									.findViewById(R.id.imguserpic);

							SharedPreferences sp = getActivity()
									.getSharedPreferences("UserDetail",
											Context.MODE_PRIVATE);
							String profileimagepath = sp.getString(
									"profileimagepath", "");

							if (profileimagepath != null
									&& profileimagepath != "") {
								Picasso.with(getActivity())
										.load(profileimagepath)
										.skipMemoryCache()
										.transform(
												new RoundedTransformation(15, 3))
										.placeholder(R.drawable.app_icon)
										.into(imgUserPic);
							}

						} catch (Exception e) {

						}
					}
				});
		as.execute();
	}

	private void clickEvents() {

		relmainfragment_slider.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		llLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("location");
			}
		});
		lnUserProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("userprofile");
			}
		});
		llCategory.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("category");
			}
		});

		llFavourites.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("favourites");
			}
		});
		llWishlist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("wishlist");
			}
		});
		llSavings.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("saving");
			}
		});
		llDoneDeals.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("done deals");

			}
		});
		llCampaign.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("campaign");
			}
		});
		llPrivcayPolicy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("privacy policy");
			}
		});
		llFriend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("Friend");
			}
		});
		llContactus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sliderInterface.sliderclick("Contact us");
			}
		});
		lnLogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					topInterface.showDialog("Loading");

					if (commonData.checkInternetConnection()) {

						CalllogoutWebService();
					} else {
						topInterface.hideDialog();

						DialogFragment dsp = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {
										// TODO Auto-generated method stub

									}
								}, getResources().getString(
										R.string.InternetConnect));
						dsp.setCancelable(false);
						dsp.show(getFragmentManager(), "");

					}
				}

			}
		});
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		sliderInterface = (sliderinterface) activity;
		topInterface = (topBarInteface) activity;
		unbind = (Unbindservice) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void CalllogoutWebService() {
		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
						// topInterface.showDialog("Loading");
						strMessage = "";
						strStatusCode = "";
					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub
						try {
							String geturl = "CustomerService.svc/LogOut";

							url_response = web_service.makeServicePostLogout(
									geturl, CommonData.deviceId,
									commonData.languageId);

							// Log.e("logout===", geturl);
							// Log.e("logout===Response", url_response);

							JSONObject js_obj_getprofileresult = new JSONObject(
									url_response);
							strStatusCode = js_obj_getprofileresult
									.getString("vStatusCode");
							strMessage = js_obj_getprofileresult
									.getString("vMessageResponse");
						} catch (Exception e) {
							// commonData.logDisplay("exception e",
							// e.getMessage()
							// .toString());
						}
					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						topInterface.hideDialog();

						if (strStatusCode.equals("420")) {
							DialogFragment ds = new SingleButtonAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {

											topInterface.topbarClick("back");
										}
									}, getResources().getString(
											R.string.PoorInternetConnect));
							ds.show(getFragmentManager(), "");
							ds.setCancelable(false);

						}

						else if (strStatusCode.equals("100")) {

							try {
								if (commonData.xmppconnection.isConnected()
										&& commonData.checkInternetConnection()) {
									commonData.xmppconnection.disconnect();
								}

								preference = getActivity()
										.getSharedPreferences("UserDetail",
												Context.MODE_PRIVATE);
								Editor editior = preference.edit();
								editior.clear();
								editior.commit();

								SharedPreferences badgepreference = getActivity()
										.getSharedPreferences("Badgecount",
												Context.MODE_PRIVATE);

								badgepreference.edit().putInt("Badge", 0)
										.commit();

								SharedPreferences prefs = getActivity()
										.getPreferences(Context.MODE_PRIVATE);
								Editor editior1 = prefs.edit();
								editior1.clear();
								editior1.commit();

								SharedPreferences preference_friends = getActivity()
										.getSharedPreferences("FriendList",
												Context.MODE_PRIVATE);

								Editor frdeditor = preference_friends.edit();
								frdeditor.clear();
								frdeditor.commit();

								removefolder();
								commonData.islogout = true;

								if (commonData.xmppconnection.isConnected()
										&& commonData.checkInternetConnection()) {
									commonData.xmppconnection.disconnect();
								}

								NotificationManager notificationManager = (NotificationManager) getActivity()
										.getSystemService(
												Context.NOTIFICATION_SERVICE);
								notificationManager.cancelAll();

								unbind.unbindservice();
								getActivity().stopService(
										new Intent(getActivity(),
												Connector.class));

								callFacebookLogout(getActivity()
										.getApplicationContext());
								Intent intent = new Intent(getActivity(),
										SplashActivity.class);
								startActivity(intent);
								getActivity().finish();

							} catch (Exception e) {
								// TODO: handle exception
							}

							// sliderInterface.sliderclick("logout");

						} else {

							if (strMessage.trim().length() != 0) {

								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub

											}
										}, strMessage);
								dsp.show(getFragmentManager(), "");
							}
						}
					}
				});
		as.execute();
	}

	public void Referesh_image() {

	}

	@Override
	public void onResume() {
		super.onResume();

		if (isUserProfile_Updated) {
			isUserProfile_Updated = false;

			async_deel_feed_list as2 = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							preference = getActivity().getSharedPreferences(
									"UserDetail", Context.MODE_PRIVATE);
							commonData.CustomerId = preference.getString(
									"customerid", "");
							commonData.languageId = preference.getString(
									"languageid", "");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							try {
								String geturl = "CustomerService.svc/GetProfile?customerid="
										+ commonData.CustomerId
										+ "&languageid="
										+ commonData.languageId;
								geturl = commonData.Url_EncodeWithBlank(geturl);
								url_response = web_service
										.makeServicegetRequest(geturl);
								JSONObject js_obj_getprofileresult = new JSONObject(
										url_response);
								strStatusCode = js_obj_getprofileresult
										.getString("vStatusCode");
								if (strStatusCode.equals("100")) {
									commonData.ProfiileImagePath = js_obj_getprofileresult
											.getString("vProfileImageName");
									SharedPreferences sp = getActivity()
											.getSharedPreferences("UserDetail",
													Context.MODE_PRIVATE);
									sp.edit()
											.putString(
													"profileimagepath",
													js_obj_getprofileresult
															.getString("vProfileImageName"))
											.commit();
									commonData.ProfileName = js_obj_getprofileresult
											.getString("vCustomerName");
									commonData.strUsername = preference
											.getString("username", "");
									commonData.strPassword = preference
											.getString("password", "");
								}
							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub

							txtUserName.setText(commonData.ProfileName);

							try {
								// Picasso.with(getActivity())
								// .load(commonData.ProfiileImagePath)
								// .transform(new RoundedTransformation(15, 3))
								// .placeholder(R.drawable.app_icon)
								// .into(imgUserPic);

								String dir = Environment
										.getExternalStorageDirectory()
										+ File.separator
										+ "."
										+ getResources().getString(
												R.string.app_name);
								File jpgfile = new File(dir, "1.jpg");

								Picasso.with(getActivity()).load(jpgfile)

								.transform(new RoundedTransformation(15, 3))
										.placeholder(R.drawable.app_icon)
										.skipMemoryCache().into(imgUserPic);

							} catch (Exception e) {

							}
						}
					});
			as2.execute();

		}

	}

	public void removefolder() {
		String dir = Environment.getExternalStorageDirectory() + File.separator
				+ "." + getResources().getString(R.string.app_name);
		File file = new File(dir);
		if (file.isDirectory()) {
			String[] children = file.list();
			for (int i = 0; i < children.length; i++) {
				new File(dir, children[i]).delete();
			}
			file.delete();
		}

	}

	public Bitmap getBitmapFromURL(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void callFacebookLogout(Context context) {
		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

		}

	}

	// public Bitmap getRoundedShape(Bitmap scaleBitmapImage) {
	// int targetWidth = 200;
	// int targetHeight = 200;
	// Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight,
	// Bitmap.Config.ARGB_8888);
	//
	// Canvas canvas = new Canvas(targetBitmap);
	// Path path = new Path();
	// path.addCircle(((float) targetWidth - 1) / 2,
	// ((float) targetHeight - 1) / 2,
	// (Math.min(((float) targetWidth), ((float) targetHeight)) / 2),
	// Path.Direction.CCW);
	//
	// canvas.clipPath(path);
	// Bitmap sourceBitmap = scaleBitmapImage;
	// canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(),
	// sourceBitmap.getHeight()), new Rect(0, 0, targetWidth,
	// targetHeight), null);
	// return targetBitmap;
	// }
}