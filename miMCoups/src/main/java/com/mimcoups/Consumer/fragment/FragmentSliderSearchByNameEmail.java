package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.FriendsSearchAdpater;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.inteface.FriendSearchAdapterSendRequest;

public class FragmentSliderSearchByNameEmail extends Fragment implements
		FriendSearchAdapterSendRequest {

	private ImageView imgFriendsSearchEmailBack,
			imgFriendsSearchEmailQuestions;
	private ListView lstFriendsSearchFriends;
	private topBarInteface topIntefacee;
	private ArrayList<FriendsDetail> arrFriendDetail = new ArrayList<FriendsDetail>();
	private FriendsSearchAdpater adapterFriendSearch;
	public EditText edtFriendsSearchEmailSearch;
	private WebService_Call webServiceCall = new WebService_Call();
	private CommonData commonData;
	public static String strSearchByNameEmail = "";
	public String strWerServiceUrl = "";
	private String url_response = "", statusCode, statusMessage = "";
	private FriendSearchAdapterSendRequest friendsearchadapter = this;
	private String strGeturl = "", strStatusCode = "", strMessageResponse,
			urlResponse = "", StrPageIndex = "1", strLastRunDate = "";
	topBarInteface topinterface;
	RelativeLayout relmainfragment_slider_search_name_email;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	String searchkeyword = "";
	LinearLayout txtReviewsNoData;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(
				R.layout.fragment_slider_search_name_email, container, false);
		initControls(view);

		clickEvents();
		strSearchByNameEmail = this.getArguments().getString("search");
		if (strSearchByNameEmail.equals("searchByName")) {
			strWerServiceUrl = "FriendService.svc/CustomersSearchByName?languageid="
					+ commonData.languageId
					+ "&customerid="
					+ commonData.CustomerId + "&name=";
		} else {
			strWerServiceUrl = "FriendService.svc/CustomersSearchByEmail?languageid="
					+ commonData.languageId
					+ "&customerid="
					+ commonData.CustomerId + "&email=";
		}
		searchByNameEmail(strWerServiceUrl);

		edtFriendsSearchEmailSearch
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {

						if (actionId == EditorInfo.IME_ACTION_SEARCH) {

							// do something
							InputMethodManager imm = (InputMethodManager) getActivity()
									.getSystemService(
											Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(
									edtFriendsSearchEmailSearch
											.getWindowToken(), 0);

							searchkeyword = edtFriendsSearchEmailSearch
									.getText().toString().trim().toLowerCase();
							
							
							try {
								adapterFriendSearch.getFilter().filter(searchkeyword);

							} catch (Exception e) {

							}

//							if (searchkeyword.trim().length() == 0) {
//
//								
//
//							} else {
//								if (strSearchByNameEmail.equals("searchByName")) {
//									String url = strWerServiceUrl + "&name="
//											+ searchkeyword;
//									arrFriendDetail = new ArrayList<FriendsDetail>();
//
//									searchByNameEmail(commonData
//											.Url_EncodeWithBlank(url));
//
//								} else {
//									String url = strWerServiceUrl + "&email="
//											+ searchkeyword;
//
//									arrFriendDetail = new ArrayList<FriendsDetail>();
//
//									searchByNameEmail(commonData
//											.Url_EncodeWithBlank(url));
//								}
//							}

							// if (strSearchByNameEmail.equals("searchByName"))
							// {
							// arrBackUpArray.clear();
							// arrBackUpArray = new ArrayList<FriendsDetail>();
							//
							// for (int i = 0; i < arrFriendDetail.size(); i++)
							// {
							// FriendsDetail friends = arrFriendDetail
							// .get(i);
							// Log.e("caleld email " + i, ""
							// + friends.freindsName.toString()
							// .trim().toLowerCase()
							// + "==" + searchkeyword);
							// if (friends.freindsName.toString().trim()
							// .toLowerCase()
							// .equalsIgnoreCase(searchkeyword)) {
							//
							// arrBackUpArray.add(friends);
							// }
							// }
							// } else {
							//
							// arrBackUpArray.clear();
							// arrBackUpArray = new ArrayList<FriendsDetail>();
							//
							// for (int i = 0; i < arrFriendDetail.size(); i++)
							// {
							// FriendsDetail friends = arrFriendDetail
							// .get(i);
							// Log.e("caleld email " + i, ""
							// + friends.friendEmailAddress
							// .toString().trim()
							// .toLowerCase() + "=="
							// + searchkeyword);
							// if (friends.friendEmailAddress.toString()
							// .trim().toLowerCase()
							// .equalsIgnoreCase(searchkeyword)) {
							//
							// arrBackUpArray.add(friends);
							// }
							// }
							// }

							// adapterFriendSearch.getFilter().filter(
							// searchkeyword);
							// if (adapterSearchList.getCount() != 0) {
							//
							// String searchkeyword = edtSearchSearch.getText()
							// .toString();
							// searchInteface.searchDetail(seachfrom,
							// searchkeyword);
							//
							// } else {
							// edtSearchSearch.setFocusable(true);
							// edtSearchSearch.setCursorVisible(true);
							// edtSearchSearch.setSelection(edtSearchSearch.getText()
							// .length());
							// }

						}
						return false;
					}
				});

		edtFriendsSearchEmailSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				try {
					adapterFriendSearch.getFilter().filter(s);

				} catch (Exception e) {

				}

				// String strSendData = s.toString();
				// arrFriendDetail.clear();
				// if (strSendData.equals("")) {
				// arrFriendDetail = arrBackUpArray;
				// Toast.makeText(getActivity(),
				// "called" + arrFriendDetail.size(),
				// Toast.LENGTH_LONG).show();
				// adapterFriendSearch = new FriendsSearchAdpater(
				// getActivity(), arrFriendDetail, friendsearchadapter);
				// lstFriendsSearchFriends.setAdapter(adapterFriendSearch);
				// } else {
				// String lowercase = strSendData.toLowerCase();
				//
				// for (int i = 0; i < arrBackUpArray.size(); i++) {
				// FriendsDetail friend = arrBackUpArray.get(i);
				// String friendlowercase = friend.userName.toLowerCase();
				// if (friendlowercase.contains(lowercase)) {
				// arrFriendDetail.add(friend);
				// }
				// }
				// adapterFriendSearch.notifyDataSetChanged();
				// }
				//
				// if (strSearchByNameEmail.equals("searchByName")) {
				// strWerServiceUrl =
				// "FriendService.svc/CustomersSearchByName?languageid="
				// + commonData.languageId
				// + "&customerid="
				// + commonData.CustomerId + "&name=" + strSendData;
				// } else {
				// strWerServiceUrl =
				// "FriendService.svc/CustomersSearchByEmail?languageid="
				// + commonData.languageId
				// + "&customerid="
				// + commonData.CustomerId + "&email=" + strSendData;
				// }
				// searchByNameEmail(strWerServiceUrl);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		return view;
	}

	private void initControls(View view) {
		commonData = (CommonData) getActivity().getApplicationContext();

		relmainfragment_slider_search_name_email = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_slider_search_name_email);
		imgFriendsSearchEmailBack = (ImageView) view
				.findViewById(R.id.imgFriendsSearchEmailBack);
		imgFriendsSearchEmailQuestions = (ImageView) view
				.findViewById(R.id.imgFriendsSearchEmailQuestions);
		lstFriendsSearchFriends = (ListView) view
				.findViewById(R.id.lstFriendsSearchFriends);

		edtFriendsSearchEmailSearch = (EditText) view
				.findViewById(R.id.edtFriendsSearchEmailSearch);
		txtReviewsNoData = (LinearLayout) view
				.findViewById(R.id.txtReviewsNoData);

	}

	private void clickEvents() {

		relmainfragment_slider_search_name_email
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		imgFriendsSearchEmailBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topIntefacee.topbarClick("back");
			}
		});
		imgFriendsSearchEmailQuestions
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						long currentTime = SystemClock.elapsedRealtime();
						if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
							lastClickTime = currentTime;
							topIntefacee.topbarClick("questions");
						}
					}
				});

	}

	private void searchByNameEmail(final String url) {
		if (commonData.checkInternetConnection()) {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topIntefacee.showDialog("Loading");

						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub

							try {

								url_response = webServiceCall
										.makeServicegetRequest(url);

								Log.e("called url", "" + url);
								Log.e("called response", "" + url_response);
								JSONObject jsonObject = new JSONObject(
										url_response);
								statusCode = jsonObject
										.getString("vStatusCode");
								statusMessage = jsonObject
										.getString("vMessageResponse");
								if (statusCode.equals("100")) {
									JSONArray jsonArray = jsonObject
											.getJSONArray("Customers");
									for (int i = 0; i < jsonArray.length(); i++) {
										JSONObject tempObject = jsonArray
												.getJSONObject(i);
										FriendsDetail fd = new FriendsDetail();
										fd.freindsName = tempObject
												.getString("vCustomerName");
										fd.friendsId = tempObject
												.getString("iCustomerId");
										fd.profileImageName = tempObject
												.getString("vProfileImageName");
										fd.userName = tempObject
												.getString("vUserName");
										// if (tempObject.has("vEmail")) {
										fd.friendEmailAddress = tempObject
												.getString("vEmail");
										fd.vMessage = tempObject
												.getString("vMessage");
										fd.status = Boolean
												.parseBoolean(tempObject
														.getString("bStatus"));
										// }
										arrFriendDetail.add(fd);
									}
								}
							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topIntefacee.hideDialog();
							if (statusCode.equals("100")
									&& arrFriendDetail.size() > 0) {

								if (arrFriendDetail.size() > 0) {
									txtReviewsNoData.setVisibility(View.GONE);
									lstFriendsSearchFriends
											.setVisibility(View.VISIBLE);

									adapterFriendSearch = new FriendsSearchAdpater(
											getActivity(),
											arrFriendDetail,
											friendsearchadapter,
											FragmentSliderSearchByNameEmail.this);
									lstFriendsSearchFriends
											.setAdapter(adapterFriendSearch);
								} else {
									txtReviewsNoData
											.setVisibility(View.VISIBLE);
									lstFriendsSearchFriends
											.setVisibility(View.GONE);
								}

								// adapterFriendSearch = new
								// FriendsSearchAdpater(
								// getActivity(), arrFriendDetail,
								// friendsearchadapter,
								// FragmentSliderSearchByNameEmail.this);
								// lstFriendsSearchFriends
								// .setAdapter(adapterFriendSearch);
							} else {
								txtReviewsNoData.setVisibility(View.VISIBLE);
								lstFriendsSearchFriends
										.setVisibility(View.GONE);

							}
						}
					});
			as.execute();
		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topIntefacee = (topBarInteface) activity;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	@Override
	public void sendRequest(final FriendsDetail friendsdetail) {
		// TODO Auto-generated method stub

		if (!commonData.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							topIntefacee.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topIntefacee.showDialog("Loading");
							// arrFriends.clear();
						}

						@Override
						public void doInBackground() {
							String getUrl = "FriendService.svc/SendFriendRequest?languageid="
									+ commonData.languageId
									+ "&customerid="
									+ commonData.CustomerId
									+ "&friendid="
									+ friendsdetail.friendsId;
							url_response = webServiceCall
									.makeServicegetRequest(getUrl);
							try {
								JSONObject jsonObject = new JSONObject(
										url_response);
								strStatusCode = jsonObject
										.getString("vStatusCode");
								strMessageResponse = jsonObject
										.getString("vMessageResponse");

							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topIntefacee.hideDialog();
							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

												topIntefacee
														.topbarClick("back");
											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (strStatusCode.equals("100")) {
								Toast.makeText(getActivity(),
										"" + strMessageResponse, 9000).show();
							} else {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO
												// Auto-generated
												// method stub
											}
										}, strMessageResponse);
								dsp.show(getFragmentManager(), "");
							}
						}
					});
			as.execute();
		}

	}
}
