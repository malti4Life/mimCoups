package com.mimcoups.Consumer.fragment;

import io.card.payment.CardIOActivity;
import io.card.payment.CardType;
import io.card.payment.CreditCard;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.UUID;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.CreditcardUtility;
import com.mimcoups.Consumer.commonClass.MonthYearPicker;
import com.mimcoups.Consumer.commonClass.WebService_Call;

import com.stripe.android.model.Card;

public class FragmentCreditcard extends Fragment {

	private topBarInteface topInterface;
	private ImageView imgCreditCardBack, imgscanyourcard;
	private TextView txtcreditcarddone, edtcreditcardexpirationdate;
	private EditText edtcreditcardfullname, edtcreditcardnumber,
			edtcreditcardcvvnumber, edtcreditcardstreetnumber,
			edtcreditcardstreetaddress, edtcreditcardcitytown,
			edtcreditcardzipcode;
	LinearLayout relmainfragment_creditcard, lincreditcardexpirationdate;
	RelativeLayout rellayout;
	DatePickerDialog dpd;
	private int MY_SCAN_REQUEST_CODE = 100;
	public static String uuid = "";
	SharedPreferences prefs;
	CommonData commondata;
	WebService_Call web_Service;
	String geturl = "", url_response = "", statusCode = "",
			vMessageResponse = "";
	private MonthYearPicker myp;
	private String creditcardcardnumber = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_creditcard, container,
				false);
		commondata = (CommonData) getActivity().getApplicationContext();

		web_Service = new WebService_Call();
		initControls(view);

		get_CardDetails();
		clickEvents();
		return view;
	}

	private void initControls(View view) {

		lincreditcardexpirationdate = (LinearLayout) view
				.findViewById(R.id.lincreditcardexpirationdate);

		relmainfragment_creditcard = (LinearLayout) view
				.findViewById(R.id.relmainfragment_creditcard);
		rellayout = (RelativeLayout) view.findViewById(R.id.rellayout);
		imgCreditCardBack = (ImageView) view
				.findViewById(R.id.imgCreditCardBack);
		imgscanyourcard = (ImageView) view.findViewById(R.id.imgscanyourcard);
		txtcreditcarddone = (TextView) view
				.findViewById(R.id.txtcreditcarddone);

		edtcreditcardfullname = (EditText) view
				.findViewById(R.id.edtcreditcardfullname);

		edtcreditcardnumber = (EditText) view
				.findViewById(R.id.edtcreditcardnumber);

		edtcreditcardcvvnumber = (EditText) view
				.findViewById(R.id.edtcreditcardcvvnumber);
		edtcreditcardexpirationdate = (TextView) view
				.findViewById(R.id.edtcreditcardexpirationdate);
		edtcreditcardstreetnumber = (EditText) view
				.findViewById(R.id.edtcreditcardstreetnumber);

		edtcreditcardstreetaddress = (EditText) view
				.findViewById(R.id.edtcreditcardstreetaddress);

		edtcreditcardcitytown = (EditText) view
				.findViewById(R.id.edtcreditcardcitytown);

		edtcreditcardzipcode = (EditText) view
				.findViewById(R.id.edtcreditcardzipcode);

		uuid = UUID.randomUUID().toString();

		prefs = getActivity().getPreferences(Context.MODE_PRIVATE);

		// setBillingAddress();

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		View view = getActivity().getCurrentFocus();
		if (view != null) {
			InputMethodManager inputManager = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(view.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		}
	}

	public static String replaceLastFour(String s) {
		int length = (s.length() - 4);
		String astricks = "";

		for (int i = 0; i < length; i++) {

			astricks = astricks + "X";

		}

		return astricks;
	}

	public void setBillingAddress() {
		prefs = getActivity().getPreferences(Context.MODE_PRIVATE);

		if (prefs.getString("billingstreetaddress", "").trim().length() != 0) {

			edtcreditcardstreetnumber.setText(prefs.getString(
					"billingstreetnumber", ""));
			edtcreditcardstreetaddress.setText(prefs.getString(
					"billingstreetaddress", ""));
			edtcreditcardcitytown.setText(prefs.getString("billingcity", ""));
			edtcreditcardzipcode.setText(prefs.getString("billingzip", ""));
		}

		String cardnumber = commondata.credit_card;

		edtcreditcardfullname.setText(commondata.credit_card_holder);

		if (cardnumber.toString().trim().length() > 0) {
			edtcreditcardnumber.setText(replaceLastFour(cardnumber)
					+ cardnumber.substring(cardnumber.length() - 4));
		}

		if (commondata.Expiry_month.trim().length() > 0
				&& commondata.Expiry_Year.trim().length() > 0) {
			edtcreditcardexpirationdate.setText(commondata.Expiry_month + "-"
					+ commondata.Expiry_Year);
		}
		edtcreditcardcvvnumber.setText("");

		// if (prefs.getString("cardName", "").trim().length() != 0) {
		// edtcreditcardfullname.setText(prefs.getString("cardName", ""));
		// edtcreditcardnumber
		// .setText(prefs.getString("securecardNumber", ""));
		// edtcreditcardexpirationdate.setText(prefs.getString(
		// "cardExpiredate", ""));
		// edtcreditcardcvvnumber.setText(prefs.getString("cardCVV", ""));
		// }
	}

	private void clickEvents() {

		rellayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

			}
		});
		edtcreditcardnumber.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

				if (edtcreditcardnumber.getText().toString().trim()
						.contains("X")) {
					if (edtcreditcardnumber.getText().toString().trim()
							.length() < commondata.credit_card.trim().length()) {
						edtcreditcardnumber.setText("");
					}
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		imgscanyourcard.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

//				onScanPress();

			}
		});

		relmainfragment_creditcard
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});

		imgCreditCardBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				topInterface.topbarClick("back");
			}
		});
		txtcreditcarddone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String cardnumber = edtcreditcardnumber.getText().toString()
						.trim();

				if (cardnumber.contains("X")) {
					cardnumber = CommonData.credit_card;


				}
				if (creditcardcardnumber.toString().trim().length() != 0) {
					CommonData.credit_card = creditcardcardnumber;

				}

				String cardtype = CreditcardUtility.CardType(cardnumber
						.toString().trim());

				Log.e("Card Type ==", "" + cardtype);



				if (edtcreditcardfullname.getText().toString().trim().length() == 0) {

					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getResources().getString(
									R.string.user_cardname_null_validate));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);

				} else if (cardnumber.toString().trim().length() == 0) {

					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getResources().getString(
									R.string.user_cardno_null_validate));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);

				}

//				else if (!CreditcardUtility.validatCardNumber(cardnumber
//						.toString().trim())) {
//
//					DialogFragment ds = new SingleButtonAlert(
//							new DialogInterfaceClick() {
//
//								@Override
//								public void dialogClick(String tag) {
//
//								}
//							}, getResources().getString(
//									R.string.user_cardno_validate));
//					ds.show(getFragmentManager(), "");
//					ds.setCancelable(false);
//
//				}

//				else if (!((cardtype.equals("mastercard")) || (cardtype
//						.equals("visa")))) {
//
//					DialogFragment ds = new SingleButtonAlert(
//							new DialogInterfaceClick() {
//
//								@Override
//								public void dialogClick(String tag) {
//
//								}
//							}, getResources().getString(
//									R.string.user_cardno_Type_validate));
//					ds.show(getFragmentManager(), "");
//					ds.setCancelable(false);
//
//				}

				else if (edtcreditcardexpirationdate.getText().toString()
						.trim().length() == 0) {

					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getResources().getString(
									R.string.user_cardno_expire_validate));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);

				}


				// Check if card is valid. If valid, create token

				else if (edtcreditcardcvvnumber.getText().toString().trim()
						.length() ==0 ) {

					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, getResources().getString(
									R.string.user_cardno_cvv_validate));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);

				}
				else {

					Log.e("card type", "we are fetching Else");
					//
					// Log.e("Card Type", ""+CreditcardUtility
					// .CardType(edtcreditcardnumber.getText().toString()
					// .trim()));

					String dates[] = edtcreditcardexpirationdate.getText()
							.toString().trim().split("-");

					if (dates[0].toString().trim().length() <= 1) {
						commondata.Expiry_month = "0" + dates[0];
					} else {
						commondata.Expiry_month = dates[0];
					}

					commondata.Expiry_Year = String.valueOf(dates[1]);

					if (!edtcreditcardnumber.getText().toString().contains("X")) {
						commondata.credit_card = edtcreditcardnumber.getText()
								.toString().trim();
					}
					Log.e("card type", "common data start");
					//
					// Log.e("Card Type", ""+CreditcardUtility
					// .CardType(edtcreditcardnumber.getText().toString()
					// .trim()));

					commondata.ispayment = true;
					commondata.CVV = edtcreditcardcvvnumber.getText()
							.toString().trim();
					Log.e("card type", "1");
					commondata.credit_card_holder = edtcreditcardfullname
							.getText().toString().trim();

					commondata.Type = CreditcardUtility.CardType(cardnumber);

					Card card = new Card(commondata.credit_card , Integer.parseInt(CommonData.Expiry_month), Integer.parseInt(CommonData.Expiry_Year),
							commondata.CVV);

					 if (!(card.validateCard())) {

						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {

									}
								}, getResources().getString(
								R.string.txtstripevalidation));
						ds.show(getFragmentManager(), "");
						ds.setCancelable(false);




					}else{
						 topInterface.topbarClick("back");
					 }




					Log.e("card type",
							"" + CreditcardUtility.CardType(cardnumber));

					// SharedPreferences.Editor edit = prefs.edit();
					//
					//
					//
					// edit.putBoolean("payment", true);
					//
					// // edit.putString("cardName",
					// edtcreditcardfullname.getText()
					// // .toString().trim());
					// // edit.putString("cardNumber", commondata.credit_card);
					// // edit.putString("cardExpiredate",
					// // edtcreditcardexpirationdate.getText().toString()
					// // .trim());
					// // edit.putString("securecardNumber", edtcreditcardnumber
					// // .getText().toString().trim());
					// //
					// // edit.putString("cardCVV",
					// edtcreditcardcvvnumber.getText()
					// // .toString().trim());
					//
					//
					//
					//
					// edit.commit();
					// set common data



				}

			}
		});

		lincreditcardexpirationdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Calendar c = Calendar.getInstance();
				int month = c.get(Calendar.MONTH);
				int year = c.get(Calendar.YEAR);
				int minyears = c.get(Calendar.YEAR);
				if (edtcreditcardexpirationdate.getText().toString().trim()
						.length() > 0) {
					String[] dates = edtcreditcardexpirationdate.getText()
							.toString().trim().split("-");

					month = Integer.parseInt(dates[0]) - 1;
					year = Integer.parseInt(dates[1]);

				}
				myp = new MonthYearPicker(getActivity());
				myp.build(minyears, month, year,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								Log.e("Date", myp.getSelectedMonth() + " >> "
										+ myp.getSelectedYear());

								int year = myp.getSelectedYear();
								int month = myp.getSelectedMonth();

								month = month + 1;

								String month1 = "" + month;
								if (month < 10) {
									month1 = "0" + month;
								}
								StringBuilder date = new StringBuilder()
										.append(month1).append("-")
										.append(year);

								Calendar c = Calendar.getInstance();
								int months = c.get(Calendar.MONTH);
								int years = c.get(Calendar.YEAR);

								Log.e("called months", "" + (months + 1)
										+ "====" + month1);
								Log.e("called Years", "" + year + "===="
										+ years);

								if ((year == years)) {
									if (((months + 1) > Integer
											.parseInt(month1))) {

										Toast.makeText(
												getActivity(),
												""
														+ getResources()
																.getString(
																		R.string.user_cardno_expire_validate),
												Toast.LENGTH_SHORT).show();

									} else {
										edtcreditcardexpirationdate
												.setText(date);
									}

								} else {
									edtcreditcardexpirationdate.setText(date);
								}

								dialog.dismiss();

							}
						}, new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub

								dialog.dismiss();
							}
						});
				myp.show();

				// createDialogWithoutDateField(year, month).show();
				// dpd.setTitle("Expiration Date");

			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		topInterface = (topBarInteface) activity;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	private DatePickerDialog createDialogWithoutDateField(int year, int month) {


		try{
			dpd = new DatePickerDialog(getActivity(),
					android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
					datePickerListener, year, month, 0);
			dpd.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			dpd.setTitle("Expiration Date");

			try {
				java.lang.reflect.Field[] datePickerDialogFields = dpd.getClass()
						.getDeclaredFields();
				for (java.lang.reflect.Field datePickerDialogField : datePickerDialogFields) {
					if (datePickerDialogField.getName().equals("mDatePicker")) {
						datePickerDialogField.setAccessible(true);
						DatePicker datePicker = (DatePicker) datePickerDialogField
								.get(dpd);
						Field[] datePickerFields = datePickerDialogField.getType()
								.getDeclaredFields();

						for (Field datePickerField : datePickerFields) {

							Log.e("Called date picker",
									"" + datePickerField.getName());
							if ("mDaySpinner".equals(datePickerField.getName())) {
								datePickerField.setAccessible(true);

								Object dayPicker = new Object();
								dayPicker = datePickerField.get(datePicker);
								((View) dayPicker).setVisibility(View.GONE);

							}

							if (datePickerDialogField.getName().equals(
									"MODE_SPINNER")) {
								datePickerDialogField.setAccessible(true);
								DatePicker datePicker1 = (DatePicker) datePickerDialogField
										.get(dpd);
								Field datePickerFields1[] = datePickerDialogField
										.getType().getDeclaredFields();
								for (Field datePickerField1 : datePickerFields1) {
									if ("mDayPicker".equals(datePickerField1
											.getName())) {
										datePickerField1.setAccessible(true);
										Object dayPicker = new Object();
										dayPicker = datePickerField1
												.get(datePicker1);
										((View) dayPicker).setVisibility(View.GONE);
									}
								}
							}
						}

						Calendar cal = Calendar.getInstance();
						cal.set(Calendar.MONTH, 0);
						cal.set(Calendar.YEAR, cal.get(cal.YEAR));
						datePicker.setMinDate(cal.getTimeInMillis());
						dpd.setTitle("Expiration Date");
						datePicker.init(year, month, 1,
								new OnDateChangedListener() {

									@Override
									public void onDateChanged(DatePicker view,
															  int year, int monthOfYear,
															  int dayOfMonth) {
										// TODO Auto-generated method stub

										dpd.setTitle("Expiration Date");
									}
								});

					}

				}
			} catch (Exception ex) {
			}

			dpd.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

		}catch (Exception e){

		}

		return dpd;

	}

	private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {

			int year = selectedYear;
			int month = selectedMonth;
			int day = selectedDay;

			month = month + 1;

			String date1 = "" + day;
			if (day < 10) {
				date1 = "0" + day;
			}

			String month1 = "" + month;
			if (month < 10) {
				month1 = "0" + month;
			}
			StringBuilder date = new StringBuilder().append(month1).append("-")
					.append(year);

			Calendar c = Calendar.getInstance();
			int months = c.get(Calendar.MONTH);
			int years = c.get(Calendar.YEAR);

			Log.e("called months", "" + months + "====" + month1);
			Log.e("called Years", "" + year + "====" + years);

			if ((year == years) && (months > Integer.parseInt(month1))) {
				Toast.makeText(
						getActivity(),
						""
								+ getResources().getString(
										R.string.user_cardno_expire_validate),
						Toast.LENGTH_SHORT).show();
			} else {
				edtcreditcardexpirationdate.setText(date);
			}

		}
	};

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub

		if (requestCode == MY_SCAN_REQUEST_CODE) {

			View view = getActivity().getCurrentFocus();
			if (view != null) {
				InputMethodManager inputManager = (InputMethodManager) getActivity()
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(view.getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
			}

			String resultStr;
			if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
				CreditCard scanResult = data
						.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);

				// Never log a raw card number. Avoid displaying it, but if
				// necessary use getFormattedCardNumber()
				resultStr = "Card Number: "
						+ scanResult.getRedactedCardNumber() + "\n";
				CardType crd = scanResult.getCardType();
				CommonData.Type = crd.toString();

				if (scanResult.isExpiryValid()) {
					resultStr += "Expiration Date: " + scanResult.expiryMonth
							+ "/" + scanResult.expiryYear + "\n";
				}

				if (scanResult.cvv != null) {
					// Never log or display a CVV
					resultStr += "CVV has " + scanResult.cvv.length()
							+ " digits.\n";
				}

				creditcardcardnumber = scanResult.cardNumber;

				if (String.valueOf(scanResult.expiryMonth).toString().trim()
						.length() <= 1) {
					CommonData.Expiry_month = "0" + scanResult.expiryMonth;
				}

				CommonData.Expiry_Year = String.valueOf(scanResult.expiryYear);
				CommonData.CVV = String.valueOf(scanResult.cvv);



				if (CommonData.Expiry_month.toString().trim().length() <= 1) {
					CommonData.Expiry_month = "0" + CommonData.Expiry_month;
				}
				String cardnumber = creditcardcardnumber;

				edtcreditcardcvvnumber.setText(CommonData.CVV);
				edtcreditcardnumber.setText(replaceLastFour(cardnumber)
						+ cardnumber.substring(cardnumber.length() - 4));
				edtcreditcardexpirationdate.setText(CommonData.Expiry_month
						+ "-" + CommonData.Expiry_Year);

				// set values

				// edt_cardno.setText(CommonData.credit_card);
				// edt_expiredate.setText(CommonData.Expiry_month + "-"
				// + CommonData.Expiry_Year);
				// edt_cvvno.setText(CommonData.CVV);

				if (scanResult.postalCode != null) {
					// Log.e("Postal code", scanResult.postalCode);
					resultStr += "Postal Code: " + scanResult.postalCode + "\n";
				}
			} else {

				resultStr = "Scan was canceled.";

			}

		}

	}

	// scan creditcard

	public void onScanPress() {

		Intent scanIntent = new Intent(getActivity(), CardIOActivity.class);

		// required for authentication with card.io
		scanIntent.putExtra(CardIOActivity.EXTRA_APP_TOKEN,
				CommonData.MY_CARDIO_APP_TOKEN);

		// customize these values to suit your needs.

		scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true);

		scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true);

		scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false);

		// if set, developers should provide their own manual entry mechanism in
		// the app
		scanIntent.putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, false); // default:
																				// false

		// MY_SCAN_REQUEST_CODE is arbitrary and is only used within this
		// activity.
		startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
	}

	public void get_CardDetails() {

		if (!(commondata.checkInternetConnection())) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// TODO Auto-generated method stub

						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
		} else {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub

							topInterface.showDialog("Loading");

						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							geturl = "OrderService.svc/GetLastUsedCreditCardInfoByCustomerId?languageid="
									+ commondata.languageId
									+ "&customerid="
									+ commondata.CustomerId;

							// "vStatusCode": 100,
							// "vMessageResponse": "Credit card details found.",
							// "vCardType": "visa",
							// "vNameonCard": "henry adams",
							// "vCreditCardNo": "4556754390907337",
							// "vMonthExp": "07",
							// "vYearExp": "2018"

							geturl = CommonData.Url_EncodeWithBlank(geturl);
							Log.e("URl review", geturl);

							url_response = web_Service
									.makeServicegetRequest(geturl);

							Log.e("response==", url_response);
							try {
								JSONObject mainObject = new JSONObject(
										url_response);
								statusCode = mainObject
										.getString("vStatusCode");
								vMessageResponse = mainObject
										.getString("vMessageResponse");

								if (statusCode.equals("100")) {

									Log.e("called ", "100");

									if (commondata.credit_card.toString()
											.trim().length() == 0) {

										Log.e("called ", "100 IF");

										commondata.credit_card = mainObject
												.getString("vCreditCardNo");
										commondata.Expiry_month = mainObject
												.getString("vMonthExp");
										commondata.Expiry_Year = mainObject
												.getString("vYearExp");
										commondata.Type = mainObject
												.getString("vCardType");
										commondata.credit_card_holder = mainObject
												.getString("vNameonCard");
									}

								}

							} catch (Exception e) {

							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topInterface.hideDialog();

							if (!(commondata.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method
												// stub

											}
										}, getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
							} else {
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topInterface
															.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {

									Log.e("called ", "Post");

									if (commondata.credit_card.trim().length() > 0) {

										Log.e("called ", "post1");
										edtcreditcardfullname
												.setText(commondata.credit_card_holder);
										edtcreditcardnumber
												.setText(commondata.credit_card);
										edtcreditcardexpirationdate
												.setText(commondata.Expiry_month
														+ "-"
														+ commondata.Expiry_Year);
									}

								} else {

									if (vMessageResponse.trim().length() > 0) {

										if (commondata.credit_card.toString()
												.trim().length() == 0) {

											DialogFragment ds = new SingleButtonAlert(
													new DialogInterfaceClick() {

														@Override
														public void dialogClick(
																String tag) {

															// topInterface
															// .topbarClick("back");
														}
													}, vMessageResponse);
											ds.show(getFragmentManager(), "");
											ds.setCancelable(false);

										}

									}
								}

								setBillingAddress();
							}

						}
					});
			as.execute();
		}

	}
}
