package com.mimcoups.Consumer.fragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.DialogFragment.SelectionYNButtonAlert;
import com.mimcoups.Consumer.customClass.NotificationDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.LocationListInterface;
import com.mimcoups.Consumer.inteface.NotificaiondetailadapterToFragment;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.adapter.NotificationDetailAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;

public class FragmentNotificationDetail extends Fragment implements
        NotificaiondetailadapterToFragment {

	private NotificationDetailAdapter adapter;
	private ListView lstNotificationDetail;
	private ArrayList<NotificationDetail> arrNotifications = new ArrayList<NotificationDetail>();
	private ImageView imgNotificationDetailBack, imgNotificationDetailRefresh;
	private topBarInteface topBarDetail;
	private LinearLayout llNotificationdetailOnsite,
			llNotificationdetailNearBy, llNotificationdetailFavourite;
	private String isLocation = "", notificationtype = "";
	private TextView txtFavouriteFavourite, txtFavouriteNearBy,
			txtFavouriteOnsite, txtNotiifcationDetailNotifications;
	LinearLayout txtNotificationDetailNoData;
	WebService_Call webservicecall;
	String serverResponse;
	CommonData globalClass;
	private String statusCode = "", statusMessage = "";
	NotificationDetail notificaitondetail;
	ArrayList<NotificationDetail> arrNotificationDetail;
	LocationListInterface locationlistinterface;
	DealFeedDetailInterface dealfeeddetailinterface;
	ArrayList<NotificationDetail> arrLocationfavoriteDetail;
	ArrayList<NotificationDetail> arrLocationOnsiteDetail;
	ArrayList<NotificationDetail> arrLocationNearbyDetail;
	RelativeLayout relNotifcaitonDetailYellowHeader;
	int locationtag = 0;
	NotificaiondetailadapterToFragment notificationdeleteinterface;
	RelativeLayout relmainfragment_notification_detail;
	ImageView deleteicon;
	private WebService_Call web_Service = new WebService_Call();
	String type = "";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		super.onCreateView(inflater, container, savedInstanceState);
		globalClass = (CommonData) getActivity().getApplicationContext();
		View view = inflater.inflate(R.layout.fragment_notification_detail,
				container, false);
		notificationdeleteinterface = this;
		InitControl(view);
		ControlClickEvent(view);
		return view;
	}

	private void ControlClickEvent(View view) {

		deleteicon.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!globalClass.checkInternetConnection()) {
					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO
									// Auto-generated
									// method
									// stub

								}
							}, getResources().getString(
									R.string.InternetConnect));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);
				} else {
					final async_deel_feed_list as = new async_deel_feed_list(
							new asynctaskloaddata() {

								JSONObject json_Obj = null;
								String url_response = "";

								@Override
								public void onPreExecute() {
									// TODO Auto-generated method stub
									topBarDetail.showDialog("Loading");
								}

								@Override
								public void doInBackground() {
									// TODO Auto-generated method stub
									// TODO Auto-generated method stub
									String geturl = "notificationservice.svc/DeleteAllNotificationByCustomerIdForApp?languageid="
											+ globalClass.languageId
											+ "&customerid="
											+ globalClass.CustomerId
											+ "&type="
											+ type;

									url_response = web_Service
											.makeServicegetRequest(geturl);
									try {
										json_Obj = new JSONObject(url_response);

									} catch (Exception e) {

									}
								}

								@Override
								public void onPostExecute() {
									topBarDetail.hideDialog();

									if (!globalClass.checkInternetConnection()) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// TODO
														// Auto-generated
														// method
														// stub

													}
												},
												getResources()
														.getString(
																R.string.InternetConnect));
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);

									} else {
										String vstatuscode;
										try {
											vstatuscode = json_Obj
													.getString("vStatusCode");
											String vMessageResponse = json_Obj
													.getString("vMessageResponse");

											if (vstatuscode.equals("100")) {
												DialogFragment ds = new SingleButtonAlert(
														new DialogInterfaceClick() {

															@Override
															public void dialogClick(
																	String tag) {
																// TODO
																// Auto-generated
																// method
																// stub

																if (type.equals("onsite")) {
																	arrLocationOnsiteDetail
																			.clear();
																} else if (type
																		.equals("nearby")) {
																	arrLocationNearbyDetail
																			.clear();
																} else if (type
																		.equals("favloc")) {
																	arrLocationfavoriteDetail
																			.clear();
																} else {
																	arrNotificationDetail
																			.clear();
																}

																adapter.notifyDataSetChanged();

																deleteicon
																		.setVisibility(View.INVISIBLE);

															}
														}, vMessageResponse);
												ds.show(getFragmentManager(),
														"");
												ds.setCancelable(false);
											} else {
												
												
//													DialogFragment ds = new SingleButtonAlert(
//															new DialogInterfaceClick() {
//
//																@Override
//																public void dialogClick(
//																		String tag) {
//																	// TODO
//																	// Auto-generated
//																	// method
//																	// stub
//
//																}
//															}, vMessageResponse);
//													ds.show(getFragmentManager(),
//															"");
//													ds.setCancelable(false);
												}
												
											

										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}

								}
							});

					DialogFragment ds = new SelectionYNButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO
									// Auto-generated
									// method
									// stub

									as.execute();

								}
							}, getActivity().getResources().getString(
									R.string.txtclearnotificationMSG));
					ds.show(getFragmentManager(), "");
					ds.setCancelable(false);

				}

			}
		});

		relmainfragment_notification_detail
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

					}
				});

		txtNotificationDetailNoData
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
					}
				});

		imgNotificationDetailRefresh.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				loaddata();
			}
		});
		imgNotificationDetailBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				((DeelFeedActivity) getActivity()).commonClass.getObserver()
						.setValue("updatenotificationsettings");
				topBarDetail.topbarClick("back");
			}
		});
		llNotificationdetailFavourite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				locationtag = 3;
				type = "favloc";

				llNotificationdetailFavourite.setBackgroundColor(Color.BLACK);
				llNotificationdetailNearBy
						.setBackgroundColor(Color.TRANSPARENT);
				llNotificationdetailOnsite
						.setBackgroundColor(Color.TRANSPARENT);

				txtFavouriteFavourite.setTextColor(Color.WHITE);
				txtFavouriteNearBy.setTextColor(Color.BLACK);

				txtFavouriteOnsite.setTextColor(Color.BLACK);

				setLocationData("favorite");

			}
		});

		llNotificationdetailNearBy.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				locationtag = 2;

				type = "nearby";

				llNotificationdetailNearBy.setBackgroundColor(Color.BLACK);
				llNotificationdetailFavourite
						.setBackgroundColor(Color.TRANSPARENT);
				llNotificationdetailOnsite
						.setBackgroundColor(Color.TRANSPARENT);

				txtFavouriteNearBy.setTextColor(Color.WHITE);
				txtFavouriteFavourite.setTextColor(Color.BLACK);

				txtFavouriteOnsite.setTextColor(Color.BLACK);

				setLocationData("nearby");

			}
		});

		llNotificationdetailOnsite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				locationtag = 1;
				type = "onsite";
				llNotificationdetailOnsite.setBackgroundColor(Color.BLACK);
				llNotificationdetailNearBy
						.setBackgroundColor(Color.TRANSPARENT);
				llNotificationdetailFavourite
						.setBackgroundColor(Color.TRANSPARENT);

				txtFavouriteOnsite.setTextColor(Color.WHITE);
				txtFavouriteFavourite.setTextColor(Color.BLACK);
				txtFavouriteNearBy.setTextColor(Color.BLACK);

				setLocationData("onsite");
			}
		});

	}

	private void InitControl(View view) {

		webservicecall = new WebService_Call();
		locationtag = 0;
		arrNotificationDetail = new ArrayList<NotificationDetail>();
		deleteicon = (ImageView) view.findViewById(R.id.deleteicon);
		arrLocationfavoriteDetail = new ArrayList<NotificationDetail>();
		arrLocationOnsiteDetail = new ArrayList<NotificationDetail>();
		arrLocationNearbyDetail = new ArrayList<NotificationDetail>();

		relmainfragment_notification_detail = (RelativeLayout) view
				.findViewById(R.id.relmainfragment_notification_detail);
		txtFavouriteOnsite = (TextView) view
				.findViewById(R.id.txtFavouriteOnsite);
		txtFavouriteNearBy = (TextView) view
				.findViewById(R.id.txtFavouriteNearBy);

		txtFavouriteFavourite = (TextView) view
				.findViewById(R.id.txtFavouriteFavourite);

		txtNotiifcationDetailNotifications = (TextView) view
				.findViewById(R.id.txtNotiifcationDetailNotifications);

		txtNotificationDetailNoData = (LinearLayout) view
				.findViewById(R.id.txtNotificationDetailNoData);

		imgNotificationDetailBack = (ImageView) view
				.findViewById(R.id.imgNotificationDetailBack);
		lstNotificationDetail = (ListView) view
				.findViewById(R.id.lstNotificationDetail);
		imgNotificationDetailRefresh = (ImageView) view
				.findViewById(R.id.imgNotificationDetailRefresh);

		// loadNotifications();

		llNotificationdetailFavourite = (LinearLayout) view
				.findViewById(R.id.llNotificationdetailFavourite);
		llNotificationdetailNearBy = (LinearLayout) view
				.findViewById(R.id.llNotificationdetailNearBy);
		llNotificationdetailOnsite = (LinearLayout) view
				.findViewById(R.id.llNotificationdetailOnsite);
		isLocation = getArguments().getString("isLocation");
		notificationtype = getArguments().getString("notificationtype");
		// if(isLocation){
		//
		// if(notificationtype.equalsIgnoreCase("favloc")){
		//
		// || notificationType.equalsIgnoreCase("onsite")
		// || notificationType.equalsIgnoreCase("nearby"){
		//
		// setLocationData("onsite");
		// }
		// }
		//
		// }
		if (isLocation.equals("false")) {
			relNotifcaitonDetailYellowHeader = (RelativeLayout) view
					.findViewById(R.id.relNotifcaitonDetailYellowHeader);
			relNotifcaitonDetailYellowHeader.setVisibility(View.GONE);
		}
		loaddata();
	}

	private void loaddata() {
		// TODO Auto-generated method stub
		locationtag = 0;
		arrNotificationDetail = new ArrayList<NotificationDetail>();
		arrLocationfavoriteDetail = new ArrayList<NotificationDetail>();
		arrLocationOnsiteDetail = new ArrayList<NotificationDetail>();
		arrLocationNearbyDetail = new ArrayList<NotificationDetail>();

		if (isLocation.equals("false")) {
			relNotifcaitonDetailYellowHeader.setVisibility(View.GONE);
		}

		if (!isLocation.equals("false")) {
			llNotificationdetailOnsite.setBackgroundColor(Color.BLACK);
			llNotificationdetailNearBy.setBackgroundColor(Color.TRANSPARENT);
			llNotificationdetailFavourite.setBackgroundColor(Color.TRANSPARENT);

			txtFavouriteOnsite.setTextColor(Color.WHITE);
			txtFavouriteFavourite.setTextColor(Color.BLACK);
			txtFavouriteNearBy.setTextColor(Color.BLACK);
		}
		if (notificationtype.equals("Cateogry")) {

			txtNotiifcationDetailNotifications.setText(getActivity()
					.getResources()
					.getString(R.string.notification_by_Category));
			type = "favcat";
			getAllCategory();
		} else if (notificationtype.equalsIgnoreCase("favloc")) {
			locationtag = 3;
			llNotificationdetailFavourite.setBackgroundColor(Color.BLACK);
			llNotificationdetailNearBy.setBackgroundColor(Color.TRANSPARENT);
			llNotificationdetailOnsite.setBackgroundColor(Color.TRANSPARENT);
			txtFavouriteFavourite.setTextColor(Color.WHITE);
			txtFavouriteNearBy.setTextColor(Color.BLACK);
			txtFavouriteOnsite.setTextColor(Color.BLACK);
			// setLocationData("favorite");
			type = "favloc";
			getAllLocation();
		} else if (notificationtype.equalsIgnoreCase("onsite")) {
			locationtag = 1;
			llNotificationdetailOnsite.setBackgroundColor(Color.BLACK);
			llNotificationdetailNearBy.setBackgroundColor(Color.TRANSPARENT);
			llNotificationdetailFavourite.setBackgroundColor(Color.TRANSPARENT);
			txtFavouriteOnsite.setTextColor(Color.WHITE);
			txtFavouriteFavourite.setTextColor(Color.BLACK);
			txtFavouriteNearBy.setTextColor(Color.BLACK);
			type = "onsite";
			// setLocationData("onsite");
			getAllLocation();
		} else if (notificationtype.equalsIgnoreCase("nearby")) {
			locationtag = 2;
			llNotificationdetailNearBy.setBackgroundColor(Color.BLACK);
			llNotificationdetailFavourite.setBackgroundColor(Color.TRANSPARENT);
			llNotificationdetailOnsite.setBackgroundColor(Color.TRANSPARENT);
			txtFavouriteNearBy.setTextColor(Color.WHITE);
			txtFavouriteFavourite.setTextColor(Color.BLACK);
			txtFavouriteOnsite.setTextColor(Color.BLACK);
			// setLocationData("nearby");
			type = "nearby";
			getAllLocation();
		} else if (notificationtype.equals("Deal")) {
			txtNotiifcationDetailNotifications.setText(getActivity()
					.getResources().getString(R.string.notification_by_Deal));
			type = "deal";
			getAllDeals();
		} else if (notificationtype.equals("Loyality")) {
			txtNotiifcationDetailNotifications
					.setText(getActivity().getResources().getString(
							R.string.notification_by_Loyalty));
			type = "loyalty";

			getAllLoyality();
		} else if (notificationtype.equals("wishlist")) {
			txtNotiifcationDetailNotifications.setText(getActivity()
					.getResources().getString(R.string.wishlist_title));
			type = "wishlist";

			getAllWishList();
		} else if (notificationtype.equals("FriendRequest")) {
			txtNotiifcationDetailNotifications.setText(getActivity()
					.getResources().getString(
							R.string.notification_by_Friend_request));
			getAllFriendRequest();
		} else if (notificationtype.equals("Mimcoups")) {
			txtNotiifcationDetailNotifications.setText(getActivity()
					.getResources().getString(
							R.string.notification_by_mimcoupsnotification));
			// Log.e("called mimcoups notification", notificationtype);
			type = "mims";
			getAllMimcoups();
		}
	}

	public void getAllCategory() {
		// TODO Auto-generated method stub

		if (!globalClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							// topBarDetail.topbarClick("back");

							getFragmentManager().popBackStack(null,
									FragmentManager.POP_BACK_STACK_INCLUSIVE);

						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topBarDetail.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							String serverUrl = "NotificationService.svc/GetAllFavCatNotificationByCustomerIdForApp?customerid="
									+ globalClass.CustomerId
									+ "&languageid="
									+ globalClass.languageId;

							// Log.e("called===", serverUrl);

							// String serverUrl =
							// "NotificationService.svc/GetAllFavCatNotificationByCustomerIdForApp?customerid="
							// + globalClass.CustomerId
							// + "&languageid="
							// + globalClass.languageId;

							serverResponse = webservicecall
									.makeServicegetRequest(serverUrl);
							// Log.e("Response notificatiomn", serverResponse);

							try {
								JSONObject js_getres = new JSONObject(
										serverResponse);
								statusCode = js_getres.getString("vStatusCode");

								if (statusCode.equals("100")) {
									JSONArray json_result = js_getres
											.getJSONArray("Notifications");

									for (int j = 0; j < json_result.length(); j++) {

										JSONObject js = json_result
												.getJSONObject(j);
										notificaitondetail = new NotificationDetail();
										notificaitondetail.iCustomerRecievedNotificationId = js
												.getString("iCustomerRecievedNotificationId");
										notificaitondetail.vNotificationMessage = js
												.getString("vNotificationMessage");
										notificaitondetail.iLocationId = js
												.getString("iLocationId");
										notificaitondetail.vType = js
												.getString("vType");
										notificaitondetail.vRedirectTag = js
												.getString("vRedirectTag");
										notificaitondetail.iCategoryId = js
												.getString("iCategoryId");
										notificaitondetail.iMerchantDealId = js
												.getString("iMerchantDealId");
										notificaitondetail.vStatus = js
												.getString("vStatus");

										arrNotificationDetail
												.add(notificaitondetail);

									}

								} else {
									statusMessage = js_getres
											.getString("vMessageResponse");
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topBarDetail.hideDialog();
							
							
							
							
							if (!(globalClass.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method stub

											}
										}, getActivity().getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							}else{
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(String tag) {

													topBarDetail
															.topbarClick("back");
												}
											}, getResources().getString(
													R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {
									SetData();
									txtNotificationDetailNoData
											.setVisibility(View.GONE);
									lstNotificationDetail
											.setVisibility(View.VISIBLE);
									deleteicon
									.setVisibility(View.VISIBLE);

								} else if (statusCode.equals("101")) {
									txtNotificationDetailNoData
											.setVisibility(View.VISIBLE);
									lstNotificationDetail.setVisibility(View.GONE);
									deleteicon
									.setVisibility(View.GONE);
//									DialogFragment ds = new SingleButtonAlert(
//											new DialogInterfaceClick() {
//
//												@Override
//												public void dialogClick(String tag) {
//													// TODO Auto-generated method
//													// stub
//													getFragmentManager()
//															.popBackStack(
//																	null,
//																	FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//												}
//											}, statusMessage);
//									ds.setCancelable(false);
//									ds.show(getFragmentManager(), "");

								}

								else {

									if (!statusMessage.equalsIgnoreCase("")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// TODO Auto-generated
														// method
														// stub
														getFragmentManager()
																.popBackStack(
																		null,
																		FragmentManager.POP_BACK_STACK_INCLUSIVE);

													}
												}, statusMessage);
										ds.setCancelable(false);
										ds.show(getFragmentManager(), "");
									}

								}

								if (arrNotificationDetail.size() == 0) {
									deleteicon.setVisibility(View.INVISIBLE);
								} else {
									deleteicon.setVisibility(View.VISIBLE);
								}
							}
							
							
						}

					});
			as.execute();
		}

	}

	private void getAllLocation() {
		// TODO Auto-generated method stub
		if (!globalClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							// topBarDetail.topbarClick("back");

							getFragmentManager().popBackStack(null,
									FragmentManager.POP_BACK_STACK_INCLUSIVE);
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topBarDetail.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							String serverUrl = "NotificationService.svc/GetAllLocNotificationByCustomerIdForApp?languageid="
									+ globalClass.languageId
									+ "&customerid="
									+ globalClass.CustomerId;

							// Log.d("server url>>", "" + serverUrl);

							serverResponse = webservicecall
									.makeServicegetRequest(serverUrl);
							try {
								JSONObject js_getres = new JSONObject(
										serverResponse);
								statusCode = js_getres.getString("vStatusCode");
								if (statusCode.equals("100")) {
									JSONArray json_result = js_getres
											.getJSONArray("Notifications");

									for (int j = 0; j < json_result.length(); j++) {

										JSONObject js = json_result
												.getJSONObject(j);
										notificaitondetail = new NotificationDetail();
										notificaitondetail.iCustomerRecievedNotificationId = js
												.getString("iCustomerRecievedNotificationId");
										notificaitondetail.vNotificationMessage = js
												.getString("vNotificationMessage");
										notificaitondetail.iLocationId = js
												.getString("iLocationId");
										notificaitondetail.vType = js
												.getString("vType");
										notificaitondetail.vRedirectTag = js
												.getString("vRedirectTag");

										Log.d("redirect>>",
												""
														+ notificaitondetail.vRedirectTag
														+ "---->>>"
														+ notificaitondetail.vNotificationMessage);
										notificaitondetail.iCategoryId = js
												.getString("iCategoryId");
										notificaitondetail.iMerchantDealId = js
												.getString("iMerchantDealId");
										notificaitondetail.vStatus = js
												.getString("vStatus");

										if (notificaitondetail.vType
												.equals("favloc")) {
											arrLocationfavoriteDetail
													.add(notificaitondetail);

										}

										if (notificaitondetail.vType
												.equals("onsite")) {
											arrLocationOnsiteDetail
													.add(notificaitondetail);

										}

										if (notificaitondetail.vType
												.equals("nearby")) {
											arrLocationNearbyDetail
													.add(notificaitondetail);

										}

									}

								} else {
									statusMessage = js_getres
											.getString("vMessageResponse");
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topBarDetail.hideDialog();
							
							if (!(globalClass.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method stub

											}
										}, getActivity().getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							}else{
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(String tag) {

													topBarDetail
															.topbarClick("back");
												}
											}, getResources().getString(
													R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {

									if (locationtag == 0) {
										locationtag = 1;
										setLocationData("onsite");
									}
									if (locationtag == 1) {
										locationtag = 1;
										setLocationData("onsite");
									}
									if (locationtag == 2) {
										locationtag = 2;
										setLocationData("nearby");
									}
									if (locationtag == 3) {
										locationtag = 3;
										setLocationData("favorite");
									}
									txtNotificationDetailNoData
											.setVisibility(View.GONE);
									lstNotificationDetail
											.setVisibility(View.VISIBLE);
									deleteicon
									.setVisibility(View.VISIBLE);
								} else if (statusCode.equals("101")) {

									txtNotificationDetailNoData
											.setVisibility(View.VISIBLE);
									lstNotificationDetail.setVisibility(View.GONE);
									deleteicon
									.setVisibility(View.GONE);
//									DialogFragment ds = new SingleButtonAlert(
//											new DialogInterfaceClick() {
//
//												@Override
//												public void dialogClick(String tag) {
//													// TODO Auto-generated method
//													// stub
//													getFragmentManager()
//															.popBackStack(
//																	null,
//																	FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//												}
//											}, statusMessage);
//									ds.setCancelable(false);
//									ds.show(getFragmentManager(), "");

								} else {

									if (!statusMessage.trim().equalsIgnoreCase("")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// TODO Auto-generated
														// method
														// stub
														getFragmentManager()
																.popBackStack(
																		null,
																		FragmentManager.POP_BACK_STACK_INCLUSIVE);

													}
												}, statusMessage);
										ds.setCancelable(false);
										ds.show(getFragmentManager(), "");
									}
								}
							}
							
							

						}

					});
			as.execute();
		}
	}

	private void getAllDeals() {
		// TODO Auto-generated method stub
		if (!globalClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							// topBarDetail.topbarClick("back");
							getFragmentManager().popBackStack(null,
									FragmentManager.POP_BACK_STACK_INCLUSIVE);
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topBarDetail.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							String serverUrl = "NotificationService.svc/GetAllFavDealNotificationByCustomerIdForApp?languageid="
									+ globalClass.languageId
									+ "&customerid="
									+ globalClass.CustomerId;

							serverResponse = webservicecall
									.makeServicegetRequest(serverUrl);
							try {
								JSONObject js_getres = new JSONObject(
										serverResponse);
								statusCode = js_getres.getString("vStatusCode");
								if (statusCode.equals("100")) {
									JSONArray json_result = js_getres
											.getJSONArray("Notifications");

									for (int j = 0; j < json_result.length(); j++) {

										JSONObject js = json_result
												.getJSONObject(j);
										notificaitondetail = new NotificationDetail();
										notificaitondetail.iCustomerRecievedNotificationId = js
												.getString("iCustomerRecievedNotificationId");
										notificaitondetail.iMerchantDealId = js
												.getString("iMerchantDealId");
										notificaitondetail.vNotificationMessage = js
												.getString("vNotificationMessage");
										notificaitondetail.iLocationId = js
												.getString("iLocationId");
										notificaitondetail.vType = js
												.getString("vType");
										notificaitondetail.vRedirectTag = js
												.getString("vRedirectTag");
										notificaitondetail.iCategoryId = js
												.getString("iCategoryId");
										notificaitondetail.vStatus = js
												.getString("vStatus");

										arrNotificationDetail
												.add(notificaitondetail);

									}

								} else {
									statusMessage = js_getres
											.getString("vMessageResponse");
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topBarDetail.hideDialog();
							
							if (!(globalClass.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method stub

											}
										}, getActivity().getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							}else{
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(String tag) {

													topBarDetail
															.topbarClick("back");
												}
											}, getResources().getString(
													R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {
									SetData();
									txtNotificationDetailNoData
											.setVisibility(View.GONE);
									lstNotificationDetail
											.setVisibility(View.VISIBLE);
									deleteicon
									.setVisibility(View.VISIBLE);
								} else if (statusCode.equals("101")) {

									txtNotificationDetailNoData
											.setVisibility(View.VISIBLE);
									lstNotificationDetail.setVisibility(View.GONE);
									deleteicon
									.setVisibility(View.GONE);
//									DialogFragment ds = new SingleButtonAlert(
//											new DialogInterfaceClick() {
//
//												@Override
//												public void dialogClick(String tag) {
//													// TODO Auto-generated method
//													// stub
//													getFragmentManager()
//															.popBackStack(
//																	null,
//																	FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//												}
//											}, statusMessage);
//									ds.show(getFragmentManager(), "");
//									ds.setCancelable(false);

								}

								else {

									if (!statusMessage.trim().equalsIgnoreCase("")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// TODO Auto-generated
														// method
														// stub
														getFragmentManager()
																.popBackStack(
																		null,
																		FragmentManager.POP_BACK_STACK_INCLUSIVE);

													}
												}, statusMessage);
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);
									}
								}
							}
							
							
						}

					});
			as.execute();
		}

	}

	private void getAllWishList() {
		// TODO Auto-generated method stub

		if (!globalClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							// topBarDetail.topbarClick("back");
							getFragmentManager().popBackStack(null,
									FragmentManager.POP_BACK_STACK_INCLUSIVE);
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							topBarDetail.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							String serverUrl = "NotificationService.svc/GetAllWishListNotificationByCustomerIdForApp?languageid="
									+ globalClass.languageId
									+ "&customerid="
									+ globalClass.CustomerId;

							// String serverUrl =
							// "NotificationService.svc/GetAllFavCatNotificationByCustomerIdForApp?customerid="
							// + globalClass.CustomerId
							// + "&languageid="
							// + globalClass.languageId;

							serverResponse = webservicecall
									.makeServicegetRequest(serverUrl);
							try {
								JSONObject js_getres = new JSONObject(
										serverResponse);
								statusCode = js_getres.getString("vStatusCode");
								if (statusCode.equals("100")) {
									JSONArray json_result = js_getres
											.getJSONArray("Notifications");

									for (int j = 0; j < json_result.length(); j++) {

										JSONObject js = json_result
												.getJSONObject(j);
										notificaitondetail = new NotificationDetail();
										notificaitondetail.iCustomerRecievedNotificationId = js
												.getString("iCustomerRecievedNotificationId");
										notificaitondetail.vNotificationMessage = js
												.getString("vNotificationMessage");
										notificaitondetail.iLocationId = js
												.getString("iLocationId");
										notificaitondetail.vType = js
												.getString("vType");
										notificaitondetail.vRedirectTag = js
												.getString("vRedirectTag");
										notificaitondetail.iCategoryId = js
												.getString("iCategoryId");
										notificaitondetail.iMerchantDealId = js
												.getString("iMerchantDealId");
										notificaitondetail.vStatus = js
												.getString("vStatus");

										arrNotificationDetail
												.add(notificaitondetail);

									}

								} else {
									statusMessage = js_getres
											.getString("vMessageResponse");
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							topBarDetail.hideDialog();
							
							if (!(globalClass.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method stub

											}
										}, getActivity().getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							}else{
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(String tag) {

													topBarDetail
															.topbarClick("back");
												}
											}, getResources().getString(
													R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {
									SetData();

									txtNotificationDetailNoData
											.setVisibility(View.GONE);
									lstNotificationDetail
											.setVisibility(View.VISIBLE);
									deleteicon
									.setVisibility(View.VISIBLE);
								} else if (statusCode.equals("101")) {

									txtNotificationDetailNoData
											.setVisibility(View.VISIBLE);
									lstNotificationDetail.setVisibility(View.GONE);
									deleteicon
									.setVisibility(View.GONE);
//
//									DialogFragment ds = new SingleButtonAlert(
//											new DialogInterfaceClick() {
//
//												@Override
//												public void dialogClick(String tag) {
//													// stub
//													getFragmentManager()
//															.popBackStack(
//																	null,
//																	FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//												}
//											}, statusMessage);
//									ds.show(getFragmentManager(), "");
//									ds.setCancelable(false);

								} else {

									if (!statusMessage.trim().equalsIgnoreCase("")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// stub
														getFragmentManager()
																.popBackStack(
																		null,
																		FragmentManager.POP_BACK_STACK_INCLUSIVE);

													}
												}, statusMessage);
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);
									}
								}
							}
							
							
						}

					});
			as.execute();
		}

	}

	private void getAllLoyality() {
		// TODO Auto-generated method stub

		if (!globalClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {

							// topBarDetail.topbarClick("back");

							getFragmentManager().popBackStack(null,
									FragmentManager.POP_BACK_STACK_INCLUSIVE);
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							topBarDetail.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							
							try {
								
								String serverUrl = "NotificationService.svc/GetAllLoyaltyNotificationByCustomerIdForApp?languageid="
										+ globalClass.languageId
										+ "&customerid="
										+ globalClass.CustomerId;

								// String serverUrl =
								// "NotificationService.svc/GetAllFavCatNotificationByCustomerIdForApp?customerid="
								// + globalClass.CustomerId
								// + "&languageid="
								// + globalClass.languageId;

								serverResponse = webservicecall
										.makeServicegetRequest(serverUrl);
								
								JSONObject js_getres = new JSONObject(
										serverResponse);
								statusCode = js_getres.getString("vStatusCode");
								if (statusCode.equals("100")) {
									JSONArray json_result = js_getres
											.getJSONArray("Notifications");

									for (int j = 0; j < json_result.length(); j++) {

										JSONObject js = json_result
												.getJSONObject(j);
										notificaitondetail = new NotificationDetail();
										notificaitondetail.iCustomerRecievedNotificationId = js
												.getString("iCustomerRecievedNotificationId");
										notificaitondetail.vNotificationMessage = js
												.getString("vNotificationMessage");
										notificaitondetail.iLocationId = js
												.getString("iLocationId");
										notificaitondetail.vType = js
												.getString("vType");
										notificaitondetail.vRedirectTag = js
												.getString("vRedirectTag");
										notificaitondetail.iCategoryId = js
												.getString("iCategoryId");
										notificaitondetail.iMerchantDealId = js
												.getString("iMerchantDealId");
										notificaitondetail.vStatus = js
												.getString("vStatus");

										arrNotificationDetail
												.add(notificaitondetail);

									}

								} else {
									statusMessage = js_getres
											.getString("vMessageResponse");
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							topBarDetail.hideDialog();
							
							
							if (!(globalClass.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method stub

											}
										}, getActivity().getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							}else{
							
								
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(String tag) {

													topBarDetail
															.topbarClick("back");
												}
											}, getResources().getString(
													R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {
									SetData();

									txtNotificationDetailNoData
											.setVisibility(View.GONE);
									lstNotificationDetail
											.setVisibility(View.VISIBLE);
									deleteicon
									.setVisibility(View.VISIBLE);
								} else if (statusCode.equals("101")) {

									txtNotificationDetailNoData
											.setVisibility(View.VISIBLE);
									lstNotificationDetail.setVisibility(View.GONE);
									deleteicon
									.setVisibility(View.GONE);
//									DialogFragment ds = new SingleButtonAlert(
//											new DialogInterfaceClick() {
//
//												@Override
//												public void dialogClick(String tag) {
//													// stub
//													getFragmentManager()
//															.popBackStack(
//																	null,
//																	FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//												}
//											}, statusMessage);
//									ds.show(getFragmentManager(), "");
//									ds.setCancelable(false);

								}

								else {
									if (statusMessage.trim().length() != 0) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// stub
														getFragmentManager()
																.popBackStack(
																		null,
																		FragmentManager.POP_BACK_STACK_INCLUSIVE);

													}
												}, statusMessage);
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);

									}
								}
							}
							
							
						}

					});
			as.execute();
		}

	}

	private void getAllFriendRequest() {
		// TODO Auto-generated method stub

	}

	private void getAllMimcoups() {
		// TODO Auto-generated method stub
		if (!globalClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							// topBarDetail.topbarClick("back");
							getFragmentManager().popBackStack(null,
									FragmentManager.POP_BACK_STACK_INCLUSIVE);
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {

			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topBarDetail.showDialog("Loading");
						}

						@Override
						public void doInBackground() {
							// TODO Auto-generated method stub
							String serverUrl = "NotificationService.svc/GetAllMimsNotificationByCustomerIdForApp?languageid="
									+ globalClass.languageId
									+ "&customerid="
									+ globalClass.CustomerId;
							// Log.e("called mimcoups", serverUrl);

							// String serverUrl =
							// "NotificationService.svc/GetAllFavCatNotificationByCustomerIdForApp?customerid="
							// + globalClass.CustomerId
							// + "&languageid="
							// + globalClass.languageId;

							serverResponse = webservicecall
									.makeServicegetRequest(serverUrl);
							try {
								JSONObject js_getres = new JSONObject(
										serverResponse);
								statusCode = js_getres.getString("vStatusCode");
								if (statusCode.equals("100")) {
									JSONArray json_result = js_getres
											.getJSONArray("Notifications");

									for (int j = 0; j < json_result.length(); j++) {

										JSONObject js = json_result
												.getJSONObject(j);

										notificaitondetail = new NotificationDetail();

										notificaitondetail.iCustomerRecievedNotificationId = js
												.getString("iCustomerRecievedNotificationId");
										notificaitondetail.vNotificationMessage = js
												.getString("vNotificationMessage");
										notificaitondetail.iLocationId = js
												.getString("iLocationId");
										notificaitondetail.vType = js
												.getString("vType");
										notificaitondetail.vRedirectTag = js
												.getString("vRedirectTag");
										notificaitondetail.iCategoryId = js
												.getString("iCategoryId");
										notificaitondetail.iMerchantDealId = js
												.getString("iMerchantDealId");
										notificaitondetail.vStatus = js
												.getString("vStatus");

										arrNotificationDetail
												.add(notificaitondetail);

									}

								} else {
									statusMessage = js_getres
											.getString("vMessageResponse");
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated method stub
							topBarDetail.hideDialog();
							
							if (!(globalClass.checkInternetConnection())) {
								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {
												// TODO Auto-generated method stub

											}
										}, getActivity().getResources().getString(
												R.string.InternetConnect));
								dsp.show(getFragmentManager(), "");
								dsp.setCancelable(false);
							}else{
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(String tag) {

													topBarDetail
															.topbarClick("back");
												}
											}, getResources().getString(
													R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {
									SetData();

									txtNotificationDetailNoData
											.setVisibility(View.GONE);
									lstNotificationDetail
											.setVisibility(View.VISIBLE);
									deleteicon
									.setVisibility(View.VISIBLE);
								}

								else if (statusCode.equals("101")) {

									txtNotificationDetailNoData
											.setVisibility(View.VISIBLE);
									lstNotificationDetail.setVisibility(View.GONE);
									deleteicon
									.setVisibility(View.GONE);
//									DialogFragment ds = new SingleButtonAlert(
//											new DialogInterfaceClick() {
//
//												@Override
//												public void dialogClick(String tag) {
//													// TODO Auto-generated method
//													// stub
//													getFragmentManager()
//															.popBackStack(
//																	null,
//																	FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//												}
//											}, statusMessage);
//									ds.show(getFragmentManager(), "");
//									ds.setCancelable(false);

								} else {
									
									

									if (!statusMessage.trim().equalsIgnoreCase("")) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// TODO Auto-generated
														// method
														// stub
														getFragmentManager()
																.popBackStack(
																		null,
																		FragmentManager.POP_BACK_STACK_INCLUSIVE);

													}
												}, statusMessage);
										ds.show(getFragmentManager(), "");
										ds.setCancelable(false);
									}
								}
							}

							
							
						}

					});
			as.execute();
		}

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		topBarDetail = (topBarInteface) activity;
		locationlistinterface = (LocationListInterface) activity;
		dealfeeddetailinterface = (DealFeedDetailInterface) activity;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}

	private void SetData() {
		if (arrNotificationDetail.size() == 0) {
			deleteicon.setVisibility(View.VISIBLE);
		} else {
			deleteicon.setVisibility(View.VISIBLE);
		}

		adapter = new NotificationDetailAdapter(getActivity(),
				notificationdeleteinterface, arrNotificationDetail,
				locationlistinterface, dealfeeddetailinterface,
				notificationtype);
		lstNotificationDetail.setAdapter(adapter);
	}

	private void setLocationData(String tag) {
		// TODO Auto-generated method stub

		if (tag.equals("nearby")) {

			adapter = new NotificationDetailAdapter(getActivity(),
					notificationdeleteinterface, arrLocationNearbyDetail,
					locationlistinterface, dealfeeddetailinterface,
					notificationtype);
		}

		if (tag.equals("favorite")) {

			adapter = new NotificationDetailAdapter(getActivity(),
					notificationdeleteinterface, arrLocationfavoriteDetail,
					locationlistinterface, dealfeeddetailinterface,
					notificationtype);
		}

		if (tag.equals("onsite")) {

			adapter = new NotificationDetailAdapter(getActivity(),
					notificationdeleteinterface, arrLocationOnsiteDetail,
					locationlistinterface, dealfeeddetailinterface,
					notificationtype);
		}

		if (adapter.getCount() == 0) {
			txtNotificationDetailNoData.setVisibility(View.VISIBLE);
			lstNotificationDetail.setVisibility(View.GONE);

			deleteicon.setVisibility(View.INVISIBLE);

		} else {
			txtNotificationDetailNoData.setVisibility(View.GONE);
			lstNotificationDetail.setVisibility(View.VISIBLE);
			deleteicon.setVisibility(View.VISIBLE);
			lstNotificationDetail.setAdapter(adapter);
		}

	}

	@Override
	public void notificationDelete(final NotificationDetail notificationlist) {

		// TODO Auto-generated method stub
		if (!globalClass.checkInternetConnection()) {
			DialogFragment ds = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
						}
					}, getResources().getString(R.string.InternetConnect));
			ds.show(getFragmentManager(), "");
			ds.setCancelable(false);
		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							topBarDetail.showDialog("Loading");
							serverResponse = "";
						}

						@Override
						public void doInBackground() {
							try {

								String geturl = "NotificationService.svc/UpdateCustomerReceivedNotificationStatus?languageid="
										+ globalClass.languageId
										+ "&customerid="
										+ globalClass.CustomerId
										+ "&customerreceivednotificationid="
										+ notificationlist.iCustomerRecievedNotificationId;

								serverResponse = webservicecall
										.makeServicegetRequest(geturl);

							} catch (Exception e) {

							}

						}

						@Override
						public void onPostExecute() {
							// TODO Auto-generated
							// method stub
							topBarDetail.hideDialog();
							try {
								JSONObject js_deleteobj = new JSONObject(
										serverResponse);
								String vStatusCode = js_deleteobj
										.getString("vStatusCode");
								String vMessageResponse = js_deleteobj
										.getString("vMessageResponse");

								if (vStatusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topBarDetail
															.topbarClick("back");
												}
											},
											getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (vStatusCode.equals("100")) {

									if (notificationtype.equals("Cateogry")) {

										arrNotificationDetail
												.remove(notificationlist);

										if (arrNotificationDetail.size() == 0) {
											adapter.notifyDataSetChanged();
											// txtNotificationDetailNoData
											// .setText(getResources()
											// .getString(
											// R.string.notificationvalidation));
											txtNotificationDetailNoData
													.setVisibility(View.VISIBLE);
											lstNotificationDetail
													.setVisibility(View.GONE);
										} else {
											adapter.notifyDataSetChanged();
											lstNotificationDetail
													.setVisibility(View.VISIBLE);
											txtNotificationDetailNoData
													.setVisibility(View.GONE);
										}
									}

									if (notificationtype.equals("onsite")
											|| notificationtype
													.equals("nearby")
											|| notificationtype
													.equals("favloc")) {
										if (locationtag == 1) {
											arrLocationOnsiteDetail
													.remove(notificationlist);

											if (arrLocationOnsiteDetail.size() == 0) {
												adapter.notifyDataSetChanged();
												// txtNotificationDetailNoData
												// .setText(getResources()
												// .getString(
												// R.string.notificationvalidation));
												txtNotificationDetailNoData
														.setVisibility(View.VISIBLE);
												lstNotificationDetail
														.setVisibility(View.GONE);
											} else {
												adapter.notifyDataSetChanged();
												lstNotificationDetail
														.setVisibility(View.VISIBLE);
												txtNotificationDetailNoData
														.setVisibility(View.GONE);
											}

										}
										if (locationtag == 2) {
											arrLocationNearbyDetail
													.remove(notificationlist);

											if (arrLocationNearbyDetail.size() == 0) {
												adapter.notifyDataSetChanged();
												// txtNotificationDetailNoData
												// .setText(getResources()
												// .getString(
												// R.string.notificationvalidation));
												txtNotificationDetailNoData
														.setVisibility(View.VISIBLE);
												lstNotificationDetail
														.setVisibility(View.GONE);
											} else {
												adapter.notifyDataSetChanged();
												lstNotificationDetail
														.setVisibility(View.VISIBLE);
												txtNotificationDetailNoData
														.setVisibility(View.GONE);
											}
										}
										if (locationtag == 3) {
											arrLocationfavoriteDetail
													.remove(notificationlist);

											if (arrLocationfavoriteDetail
													.size() == 0) {
												adapter.notifyDataSetChanged();
												// txtNotificationDetailNoData
												// .setText(getResources()
												// .getString(
												// R.string.notificationvalidation));
												txtNotificationDetailNoData
														.setVisibility(View.VISIBLE);
												lstNotificationDetail
														.setVisibility(View.GONE);
											} else {
												adapter.notifyDataSetChanged();
												lstNotificationDetail
														.setVisibility(View.VISIBLE);
												txtNotificationDetailNoData
														.setVisibility(View.GONE);
											}
										}
									}
									if (notificationtype.equals("Deal")) {

										arrNotificationDetail
												.remove(notificationlist);

										if (arrNotificationDetail.size() == 0) {
											adapter.notifyDataSetChanged();
											// txtNotificationDetailNoData
											// .setText(getResources()
											// .getString(
											// R.string.notificationvalidation));
											txtNotificationDetailNoData
													.setVisibility(View.VISIBLE);
											lstNotificationDetail
													.setVisibility(View.GONE);
										} else {
											adapter.notifyDataSetChanged();
											lstNotificationDetail
													.setVisibility(View.VISIBLE);
											txtNotificationDetailNoData
													.setVisibility(View.GONE);
										}
									}

									if (notificationtype.equals("wishlist")) {

										arrNotificationDetail
												.remove(notificationlist);

										if (arrNotificationDetail.size() == 0) {
											adapter.notifyDataSetChanged();
											// txtNotificationDetailNoData
											// .setText(getResources()
											// .getString(
											// R.string.notificationvalidation));
											txtNotificationDetailNoData
													.setVisibility(View.VISIBLE);
											lstNotificationDetail
													.setVisibility(View.GONE);
										} else {
											adapter.notifyDataSetChanged();
											lstNotificationDetail
													.setVisibility(View.VISIBLE);
											txtNotificationDetailNoData
													.setVisibility(View.GONE);
										}
									}
									if (notificationtype.equals("Loyality")) {

										arrNotificationDetail
												.remove(notificationlist);

										if (arrNotificationDetail.size() == 0) {
											adapter.notifyDataSetChanged();
											// txtNotificationDetailNoData
											// .setText(getResources()
											// .getString(
											// R.string.notificationvalidation));
											txtNotificationDetailNoData
													.setVisibility(View.VISIBLE);
											lstNotificationDetail
													.setVisibility(View.GONE);
										} else {
											adapter.notifyDataSetChanged();
											lstNotificationDetail
													.setVisibility(View.VISIBLE);
											txtNotificationDetailNoData
													.setVisibility(View.GONE);
										}
									}

									if (notificationtype.equals("Mimcoups")) {

										arrNotificationDetail
												.remove(notificationlist);

										if (arrNotificationDetail.size() == 0) {
											adapter.notifyDataSetChanged();
											// txtNotificationDetailNoData
											// .setText(getResources()
											// .getString(
											// R.string.notificationvalidation));
											txtNotificationDetailNoData
													.setVisibility(View.VISIBLE);
											lstNotificationDetail
													.setVisibility(View.GONE);
										} else {
											adapter.notifyDataSetChanged();
											lstNotificationDetail
													.setVisibility(View.VISIBLE);
											txtNotificationDetailNoData
													.setVisibility(View.GONE);
										}
									}

								}

							} catch (Exception e) {
							}

						}
					});
			as.execute();
		}

	}

}