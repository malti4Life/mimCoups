package com.mimcoups.Consumer.asynctask;

import android.os.AsyncTask;

import com.mimcoups.Consumer.inteface.asynctaskloaddata;

public class async_deel_feed_list extends AsyncTask<Void, Void, Void> {
	private asynctaskloaddata loaddata;

	public async_deel_feed_list(asynctaskloaddata loaddata) {
		this.loaddata = loaddata;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		loaddata.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
		loaddata.doInBackground();
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		loaddata.onPostExecute();

	}
}
