package com.mimcoups.Consumer;

import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mimcoups.Merchant.MerchantActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.GPSTracker;

public class SplashActivity extends Activity {

	private CommonData commonData;
	private SharedPreferences sharedPreference;
	private GoogleCloudMessaging _gcm;
	private String _devideId;
	GPSTracker gps;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		commonData = (CommonData) getApplicationContext();



		try {
			clearBackStack();
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.mimcoups", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("keyhash ",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		} finally {

			// gps = new GPSTracker(SplashActivity.this);
			//
			// // check if GPS enabled
			// if (gps.canGetLocation()) {
			//
			// double latitude = gps.getLatitude();
			// double longitude = gps.getLongitude();
			//
			// commonData.currentLatitude = latitude;
			// commonData.currentLongitude = longitude;
			//
			// }

		}
		String idd = Secure.getString(this.getContentResolver(),
				Secure.ANDROID_ID);

		// startService(new Intent(getApplicationContext(), GPSTracker.class));

		Log.i("secure id>>", "" + idd);
		Thread splash_thread = new Thread(new Runnable() {
			//
			@Override
			public void run() {
				try {
					getDeviceId();
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {

					sharedPreference = getSharedPreferences("UserDetail",
							Context.MODE_PRIVATE);
					boolean isFirstTime = sharedPreference.getBoolean(
							"firsttime", true);

					Editor edt = sharedPreference.edit();
					edt.putBoolean("launch", true);
					edt.commit();

					// sharedPreference = getSharedPreferences("UserDetail",
					// Context.MODE_PRIVATE);
					// boolean isFirstTime = sharedPreference.getBoolean(
					// "firsttime", true);
					if (isFirstTime) {
						removefolder();
						createDirectory();
						Intent intent = new Intent(SplashActivity.this,
								RegisterActivity.class);
						startActivity(intent);
						finish();
					} else {
						commonData.currentLatitude = Double
								.valueOf(sharedPreference.getString("latitude",
										"" + commonData.currentLatitude));
						commonData.currentLongitude = Double
								.valueOf(sharedPreference.getString(
										"longitude", ""
												+ commonData.currentLongitude));
						// Log.e("called spalsh==", ""
						// + commonData.currentLatitude + "==="
						// + commonData.currentLongitude);
						createDirectory();
						commonData.languageId = sharedPreference.getString(
								"languageid", "1");
						// Log.i("splash languageid>>", "" +
						// commonData.languageId);
						if (commonData.languageId.equals("1")) {
							changeLanguage("en");
						} else {
							changeLanguage("no");
						}
                        Intent intent = new Intent(SplashActivity.this,
                                DeelFeedActivity.class);
						intent.putExtra("notification", false);
						intent.putExtra("type", "");
						startActivity(intent);
						finish();
					}
				}
			}
		});
		splash_thread.start();
	}

	public void getDeviceId() {
		// new GetDeviceID().execute();
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				CommonData.DISPLAY_MESSAGE_ACTION));
		final String regId = GCMRegistrar.getRegistrationId(this);

		 Log.e("compare id>>>", "" + regId);

		CommonData.deviceId = regId;

		if (regId.equals("")) {
			GCMRegistrar.register(this, CommonData.SENDER_ID);
			// Log.d("GCM ID Register>>", "GCM ID REGISTER");

		} else {
			// Log.d("ELSE>>", "ELSE");
			// Device is already registered on GCM
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				// Skips registration.
				Log.d("SKIP ELSE", "SKIP ELSE");
			}
		}
	}

	private void createDirectory() {
		// commonData.SdCardPath = Environment.getExternalStorageDirectory()
		// .getAbsolutePath();
		String dir = Environment.getExternalStorageDirectory() + File.separator
				+ "." + getResources().getString(R.string.app_name);
		File file = new File(dir);
		// commonData.SdCardPath = file.getAbsolutePath();

		if (!file.exists()) {
			file.mkdirs();
			Bitmap bm = BitmapFactory.decodeResource(getResources(),
					R.drawable.noimage_profile);

			FileOutputStream outStream;
			try {
				File jpgfile = new File(dir, "1.jpg");
				outStream = new FileOutputStream(jpgfile);
				bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
				outStream.flush();
				outStream.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.i("picture exception>>", "exception");
				e.printStackTrace();
			}

		}

		String dir1 = Environment.getExternalStorageDirectory()
				+ File.separator + "myDirectory";
		// create folder
		File folder = new File(dir1); // folder name
		folder.mkdirs();

		// create file
		File file1 = new File(dir, "filename.extension");
	}

	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString("message");
			WakeLocker.acquire(getApplicationContext());
			WakeLocker.release();
		}
	};

	@Override
	protected void onDestroy() {
		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
		}
		super.onDestroy();
	}

	// private class GetDeviceID extends AsyncTask<String, String, String> {
	// @Override
	// protected void onPreExecute() {
	// // progress bar
	// super.onPreExecute();
	// }
	//
	// @Override
	// protected String doInBackground(String... params) {
	// if (_gcm == null) {
	// _gcm = GoogleCloudMessaging.getInstance(SplashActivity.this);
	// }
	//
	// try {
	// _devideId = _gcm.register(CommonData.SENDER_ID);
	// System.out.println("my id is\n" + _devideId
	// + _devideId.length());
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(String result) {
	//
	// // dismiss dialouge here
	//
	// }
	// }

	private void changeLanguage(String languageName) {
		Locale myLocale = new Locale(languageName);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);

	}

	public void removefolder() {
		String dir = Environment.getExternalStorageDirectory() + File.separator
				+ "." + getResources().getString(R.string.app_name);
		File file = new File(dir);
		if (file.isDirectory()) {
			String[] children = file.list();
			for (int i = 0; i < children.length; i++) {
				new File(dir, children[i]).delete();
			}
			file.delete();
		}

	}

	private void clearBackStack() {
		FragmentManager manager = getFragmentManager();
		if (manager.getBackStackEntryCount() > 0) {
			FragmentManager.BackStackEntry first = manager
					.getBackStackEntryAt(0);
			manager.popBackStack(first.getId(),
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
	}
}
