package com.mimcoups.Consumer.twitter;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;

/**
 * An asynchronous task that communicates with Twitter to retrieve a request
 * token. (OAuthGetRequestToken)
 * 
 * After receiving the request token from Twitter, pop a browser to the user to
 * authorize the Request Token. (OAuthAuthorizeToken)
 * 
 */
public class OAuthRequestTokenTask extends AsyncTask<Void, Void, Void> {

	final String TAG = getClass().getName();
	private Context context;
	private OAuthProvider provider;
	private OAuthConsumer consumer;
	ProgressDialog progressdialog;

	/**
	 * 
	 * We pass the OAuth consumer and provider.
	 * 
	 * @param context
	 *            Required to be able to start the intent to launch the browser.
	 * @param provider
	 *            The OAuthProvider object
	 * @param consumer
	 *            The OAuthConsumer object
	 */
	public OAuthRequestTokenTask(Context context, OAuthConsumer consumer,
			OAuthProvider provider) {
		this.context = context;
		this.consumer = consumer;
		this.provider = provider;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();

		progressdialog = new ProgressDialog(this.context);
		progressdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressdialog = ProgressDialog.show(this.context, "", "Please Wait");
	}

	/**
	 * 
	 * Retrieve the OAuth Request Token and present a browser to the user to
	 * authorize the token.
	 * 
	 */
	@Override
	protected Void doInBackground(Void... params) {

		try {

			final String url = provider.retrieveRequestToken(consumer,
					PrepareRequestTokenActivity.OAUTH_CALLBACK_URL);
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url))
					.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
							| Intent.FLAG_ACTIVITY_NO_HISTORY
							| Intent.FLAG_FROM_BACKGROUND);
			context.startActivity(intent);
		} catch (Exception e) {

		}

		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);

		progressdialog.dismiss();
	}
}