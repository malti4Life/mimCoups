package com.mimcoups.Consumer.twitter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.mimcoups.R;

public class PrepareRequestTokenActivity extends Activity {

	// for twitter
	// public static final String CONSUMER_KEY = "BfTD77R2mJJeijaeiakKl2yMb";
	// public static final String CONSUMER_SECRET =
	// "782woDCWVuWBFzrFtFsrOrN9U2XylvOXJIUYW72u4MI6VxYjPD";

	public static final String CONSUMER_KEY = "EW4aHaBSllSpu2cJxMfZ0Lq4e";
	public static final String CONSUMER_SECRET = "iUSxiiKkWUPueVSZ06HOY7rlVdflH3b79Oy3HLWfzonOzRmnJn";
	public static final String REQUEST_URL = "https://api.twitter.com/oauth/request_token";
	public static final String ACCESS_URL = "https://api.twitter.com/oauth/access_token";
	public static final String AUTHORIZE_URL = "https://api.twitter.com/oauth/authorize";
	public static final String TWIT_PIC_KEY = "453dbb23909c191aa62327f2e9804654";
	public static final String OAUTH_CALLBACK_SCHEME = "x-oauthflow-twitter";
	public static final String OAUTH_CALLBACK_HOST = "callback";
	public static final String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME
			+ "://" + OAUTH_CALLBACK_HOST;

	final String TAG = getClass().getName();
	String viewname = "";
	private OAuthConsumer consumer;
	private OAuthProvider provider;
	boolean cancel_screen = true;
	String accesstoken;
	String strimage, strdealname, strdesc;
	private final Handler mTwitterHandler = new Handler();
	int sharingfrom;

	final Runnable mUpdateTwitterNotification = new Runnable() {
		public void run() {

			Toast.makeText(getApplicationContext(),
					"Message posted to your twitter account.", 2000).show();
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		viewname = getIntent().getExtras().getString("view");
		setContentView(R.layout.fbsample);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			sharingfrom = extras.getInt("sharingfrom");
			strimage = extras.getString("Image");
			strdesc = extras.getString("desc");
			strdealname = extras.getString("dealname");

			if (sharingfrom == 0) {
				// Toast.makeText(getApplicationContext(), "Deal Detail", 9000)
				// .show();
			}
			if (sharingfrom == 1) {
				// Toast.makeText(getApplicationContext(), "Saving",
				// 9000).show();
			}
			if (sharingfrom == 2) {
				// Toast.makeText(getApplicationContext(), "Location", 9000)
				// .show();
			}

		}

		cancel_screen = true;
		try {
			this.consumer = new CommonsHttpOAuthConsumer(CONSUMER_KEY,
					CONSUMER_SECRET);
			this.provider = new CommonsHttpOAuthProvider(REQUEST_URL,
					ACCESS_URL, AUTHORIZE_URL);
		} catch (Exception e) {

		}

		new OAuthRequestTokenTask(this, consumer, provider).execute();

	}

	/**
	 * Called when the OAuthRequestTokenTask finishes (user has authorized the
	 * request token). The callback URL will be intercepted here.
	 */
	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		final Uri uri = intent.getData();
		if (uri != null && uri.getScheme().equals(OAUTH_CALLBACK_SCHEME)) {
			new RetrieveAccessTokenTask(this, consumer, provider, prefs)
					.execute(uri);
			finish();
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (cancel_screen) {
			cancel_screen = false;
		} else {
			onBackPressed();
		}
	}

	public class RetrieveAccessTokenTask extends AsyncTask<Uri, Void, Void> {

		private Context context;
		private OAuthProvider provider;
		private OAuthConsumer consumer;
		private SharedPreferences prefs;

		public RetrieveAccessTokenTask(Context context, OAuthConsumer consumer,
				OAuthProvider provider, SharedPreferences prefs) {
			this.context = context;
			this.consumer = consumer;
			this.provider = provider;
			this.prefs = prefs;
		}

		/**
		 * Retrieve the oauth_verifier, and store the oauth and
		 * oauth_token_secret for future API calls.
		 */
		@Override
		protected Void doInBackground(Uri... params) {
			final Uri uri = params[0];
			System.out.println("uri" + uri.toString());
			final String oauth_verifier = uri
					.getQueryParameter(OAuth.OAUTH_VERIFIER);
			final Editor edit = prefs.edit();
			edit.putString(OAuth.OAUTH_TOKEN, consumer.getToken());
			edit.putString(OAuth.OAUTH_TOKEN_SECRET, consumer.getTokenSecret());
			edit.commit();
			String token1 = prefs.getString(OAuth.OAUTH_TOKEN, "");
			String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");

			try {
				provider.retrieveAccessToken(consumer, oauth_verifier);

				String RFC1738encodedconsumerkey = URLEncode(CONSUMER_KEY);
				String RFC1738encodedconsumerkeySecreat = URLEncode(CONSUMER_SECRET);
				String Barriertoken = (CONSUMER_KEY + ":" + CONSUMER_SECRET);

				byte[] data = Barriertoken.getBytes("UTF-8");
				String token = Base64.encodeToString(data, Base64.NO_WRAP);

				if (true) {
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(
							"https://api.twitter.com/oauth2/token");
					httppost.addHeader("Authorization", "Basic " + token);
					httppost.addHeader("Content-Type",
							"application/x-www-form-urlencoded;charset=UTF-8");

					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							1);
					nameValuePairs.add(new BasicNameValuePair("grant_type",
							"client_credentials"));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					org.apache.http.HttpResponse response = httpclient
							.execute(httppost);
					HttpEntity httpEntity = response.getEntity();
					InputStream is = httpEntity.getContent();

					BufferedReader reader = new BufferedReader(
							new InputStreamReader(is));
					StringBuilder sb = new StringBuilder();
					String line = null;
					while ((line = reader.readLine()) != null) {
						sb.append(line + "\n");
					}
					is.close();
					System.out.println("response " + sb.toString());
					JSONObject obj = new JSONObject(sb.toString());
					accesstoken = obj.getString("access_token");
				}

				System.out.println("Acces token" + accesstoken);
				System.out.println("Outh Token = " + token1);
				System.out.println("Outh Secreat = " + secret);

				Twitter twitter = new TwitterFactory().getInstance();

				AccessToken mAccessToken = new AccessToken(consumer.getToken(),
						consumer.getTokenSecret());

				twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
				twitter.setOAuthAccessToken(mAccessToken);

				Log.e("TETS", "" + mAccessToken);

				StatusUpdate ad = new StatusUpdate(strdealname + "\n" + strdesc
						+ "\n" + strimage);

				Log.e("TETETETETEE", "---->");

				twitter4j.Status response = twitter.updateStatus(ad);
				System.out.println("here" + response.getText());
			} catch (Exception e) {

			}
			return null;
		}

		public String URLEncode(String s) {
			StringBuffer build = new StringBuffer(s.length());

			for (int i = 0; i < s.length(); ++i) {
				switch (s.charAt(i)) {
				case ' ':
					build.append("%20");
					break;
				case '+':
					build.append("%2b");
					break;
				case '\'':
					build.append("%27");
					break;
				case '<':
					build.append("%3c");
					break;
				case '>':
					build.append("%3e");
					break;
				case '#':
					build.append("%23");
					break;
				case '%':
					build.append("%25");
					break;
				case '{':
					build.append("%7b");
					break;
				case '}':
					build.append("%7d");
					break;
				case '\\':
					build.append("%5c");
					break;
				case '^':
					build.append("%5e");
					break;
				case '~':
					build.append("%73");
					break;
				case '[':
					build.append("%5b");
					break;
				case ']':
					build.append("%5d");
					break;
				default:
					build.append(s.charAt(i));
					break;
				}
			}

			return build.toString();
		}
	}
}
