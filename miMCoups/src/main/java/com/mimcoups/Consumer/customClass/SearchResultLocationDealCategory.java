package com.mimcoups.Consumer.customClass;

public class SearchResultLocationDealCategory {
	// public String LocationName;
	// public String LocationPin;

	public String iMerchantDealId;
	public String vName;
	public String dDealDiscountedAmount;
	public String vMerchantStoreMapImage_A_90X90;
	public String vMerchantStoreMapImage_I_60X60;
	public String dLatitude;
	public String dLongitude;
	public String iMerchantStoreId;
	public String iCategoryId;
	public String iRowNumber;
	public String iNextIndex;
	public String pinLocation;

}
