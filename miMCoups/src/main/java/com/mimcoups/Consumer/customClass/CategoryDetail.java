package com.mimcoups.Consumer.customClass;

import org.json.JSONObject;

public class CategoryDetail {
	public String categoryId;
	// public String categoryImageUrl;

	public String vCategoryImage_A_90X90;
	public String vCategoryImage_A_70X70;

	public String categoryName;
	public boolean CategoryFavourite;
	public String categoryBackGround;

	public CategoryDetail parseJson(JSONObject json_Category) throws Exception {
		this.categoryId = json_Category.getString("iCategoryId");
		this.CategoryFavourite = json_Category.getBoolean("bIsFavorite");
		this.categoryName = json_Category.getString("vCategoryName");
		this.vCategoryImage_A_70X70 = json_Category
				.getString("vCategoryImage_A_70X70");

		this.vCategoryImage_A_90X90 = json_Category
				.getString("vCategoryImage_A_90X90");

		this.categoryBackGround = json_Category
				.getString("vCategoryBackgroundColorInHashCode");
		return this;
	}
}
