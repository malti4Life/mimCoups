package com.mimcoups.Consumer.customClass;

import java.io.Serializable;

import org.json.JSONObject;

public class DealFeedDetail implements Serializable {

	public String DealImageUrl;
	public String StoreName;
	public String dealName;
	public String shortDescription;
	public String shortDescriptionHeading;
	public String DealOrignalPrice;
	public String dealDiscountPrice;
	public String dealDistanse;
	public String locationId;
	public String categoryId;
	public String DealId;
	
	public boolean bBuyNowStatus;
	public String vBuyNowMessage;
	
	public double dLatitude=0.0;
	public double dLongitude=0.0;

	public String bIsFavorite;
	public String dDealDiscountedAmount;
	public String dDealOrigianlAmount;
	public String iCategoryId;
	public String iDealPurchasedCount;
	public String iLocationId;
	public String iMerchantDealId;
	public String iRating;
	public String iReviews;
	public String vCategoryName;
	public String vDealCondition;
	public String vDealDetailImage1_A_500X500;

	public String vDealDetailImage2_A_500X500;
	public String vDealDetailImage3_A_500X500;
	public String vDealDetailImage4_A_500X500;
	public String vDealListImage_A_350X350;
	public String vDeal_Completion_time;
	public String vHighlights;
	public String vMediaURL;
	public String vMerchantDealName;
	public String vMerchantStoreName;
	public String vShortDescription;
	public String vShortDescriptionHeading;
	public String vStoreListImage_A_70X70;
	public String vDescription;
	public String vRemainingSeconds;
	public String vFriendName;
	public String vFriendImage_A_100x100;
	public String vPurchaseamount;
	public String vOrderamount;
	public String vSavingamount;
	public String vPurchaseDate;
	public String vBoughtTime;
	public Boolean bDealStatus;
	public String vExpiredDealMessage;
	public String iOrderId;
	public String dTotalSavingAmount;
	public double dSavingamount = 0;
	public String vOrderNumber;
	public String vCustomerOrderStatus;
	public String savingAmounttxt = "";
	public String vAddress = "";
	public String dCurrentSalesCommission="";
	public String vMaximumRewardsUsablePercentage="";
	public String vMaximumRewardsUsablePercentageMessage="";
	
	
	// Payment
	
	
	public String Dealname;
	public String Quantity;
	public String DealAmount;
	public String TotalAmount;
	public String Dealdesc;
	public String iOrderTempId;
	public String vStripeMerchantAccountId;
	
	
	public String orderquantityval;
	public String streetnumberval;
	public String streetaddressval;
	public String citynameval;
	public String billingzipcodeval;
	public String billingstreetnumberval;
	public String billingcitynameval;
	public String billingstreetaddressval;
	public String zipcodeval;
	public String shippingstatename="";
	public String billingstatename="";
	

	public DealFeedDetail DelFeedDetail(JSONObject json_deal) throws Exception {

		StoreName = json_deal.getString("vMerchantStoreName");
		dealName = json_deal.getString("vMerchantDealName");
		shortDescription = json_deal.getString("vShortDescription");
		shortDescriptionHeading = json_deal
				.getString("vShortDescriptionHeading");
		DealImageUrl = json_deal.getString("vDealListImage_A_350X350");

		DealOrignalPrice = json_deal.getString("dDealOrigianlAmount");
		dealDiscountPrice = json_deal.getString("dDealDiscountedAmount");
		dealDistanse = json_deal.getString("dDistance");
		locationId = json_deal.getString("iLocationId");
		categoryId = json_deal.getString("iCategoryId");
		DealId = json_deal.getString("iMerchantDealId");
		iMerchantDealId = json_deal.getString("iMerchantDealId");

		return this;
	}

	public DealFeedDetail FriendsDeal(JSONObject json_deal) throws Exception {

		vFriendName = json_deal.getString("vFriendName");
		vFriendImage_A_100x100 = json_deal.getString("vFriendImage_A_100x100");
		locationId = json_deal.getString("iLocationId");
		StoreName = json_deal.getString("vMerchantStoreName");
		categoryId = json_deal.getString("iCategoryId");
		DealId = json_deal.getString("iMerchantDealId");
		iMerchantDealId = json_deal.getString("iMerchantDealId");
		dealName = json_deal.getString("vMerchantDealName");
		DealOrignalPrice = json_deal.getString("vOrignalamount");
		dealDiscountPrice = json_deal.getString("vDiscountamount");

		shortDescription = json_deal.getString("vShortDescription");
		shortDescriptionHeading = json_deal
				.getString("vShortDescriptionHeading");
		vPurchaseamount = json_deal.getString("vPurchaseamount");
		dealDistanse = json_deal.getString("dDistance");
		DealImageUrl = json_deal.getString("vDealListImage_A_350X350");
		vBoughtTime = json_deal.getString("vBoughtTime");

		return this;
	}

}
