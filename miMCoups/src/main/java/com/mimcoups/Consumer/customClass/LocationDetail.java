package com.mimcoups.Consumer.customClass;

public class LocationDetail {

	public String LocationName;
	public String bIsFavorite;
	public String dDistance;
	public String dLatitude;
	public String dLongitude;
	public String iDealCount;
	public String iMerchantStoreId;
	public String iMerchantStoreRatings;
	public String vAddress;
	public String vFacebookPageURL;
	public String vGooglePlusURL;
	public String vInstagramURL;
	public String vMerchantStoreEmail;
	public String vMerchantStoreName;
	public String vShortDescription;
	public String vStoreDetailImage_A_650X325;
	public String vTwitterPageURL;

}
