package com.mimcoups.Consumer.customClass;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyItem implements ClusterItem {
	private final LatLng mPosition;
	private final String pinlocationname;

	// public MyItem(double lat, double lng, String pinlocationname) {
	// mPosition = new LatLng(lat, lng);
	// this.pinlocationname = pinlocationname;
	// }

	public MyItem(LatLng lng, String pinlocationname) {
		mPosition = lng;
		this.pinlocationname = pinlocationname;
	}

	@Override
	public LatLng getPosition() {
		return mPosition;
	}
}
