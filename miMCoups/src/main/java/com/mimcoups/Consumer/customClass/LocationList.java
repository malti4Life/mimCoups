package com.mimcoups.Consumer.customClass;

import java.io.Serializable;

import org.json.JSONObject;

public class LocationList implements Serializable {
	public String bIsFavorite;
	public String dDistance;
	public String dLatitude;
	public String dLongitude;
	public String iDealCount;
	public String iMerchantStoreId;
	public String iNextIndex;
	public String iRowNumber;
	public String vAddress;
	public String vMerchantStoreName;
	public String vShortDescription;
	public String vStoreListImage_A_70X70;
	public String vStoreDetailImage_A_650X325;

	public boolean isFavorite = true;

	public LocationList convertObjecttoJson(JSONObject jsobj) throws Exception {
		bIsFavorite = jsobj.getString("bIsFavorite");
		dDistance = jsobj.getString("dDistance");
		dLatitude = jsobj.getString("dLatitude");
		dLongitude = jsobj.getString("dLongitude");
		iDealCount = jsobj.getString("iDealCount");
		iMerchantStoreId = jsobj.getString("iMerchantStoreId");
		iRowNumber = jsobj.getString("iRowNumber");
		vAddress = jsobj.getString("vAddress");
		vMerchantStoreName = jsobj.getString("vMerchantStoreName");
		vShortDescription = jsobj.getString("vShortDescription");
		vStoreListImage_A_70X70 = jsobj.getString("vStoreListImage_I_120X120");
		return this;
	}
}
