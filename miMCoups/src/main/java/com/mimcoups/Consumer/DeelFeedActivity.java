package com.mimcoups.Consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.mimcoups.Consumer.DialogFragment.MurchantConnectDialog;
import com.mimcoups.Consumer.DialogFragment.ProgressDialogFragment;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.DialogFragment.UpdateapplicationDialog;
import com.mimcoups.R;
import com.mimcoups.Consumer.Service.FusedLocationService;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.mimcoups.Consumer.customClass.ChatUser;
import com.mimcoups.Consumer.customClass.DateCustomeClass;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.customClass.WishListDetail;
import com.mimcoups.Consumer.customClass.supportchat;
import com.mimcoups.Consumer.fragment.ChatDetailActivity;
import com.mimcoups.Consumer.fragment.FragmentChatList;
import com.mimcoups.Consumer.fragment.FragmentCreditcard;
import com.mimcoups.Consumer.fragment.FragmentDealDetail;
import com.mimcoups.Consumer.fragment.FragmentDealDetailsMap;
import com.mimcoups.Consumer.fragment.FragmentDealFeed;
import com.mimcoups.Consumer.fragment.FragmentDealShare;
import com.mimcoups.Consumer.fragment.FragmentDiscussDeal;
import com.mimcoups.Consumer.fragment.FragmentFriendPurchase;
import com.mimcoups.Consumer.fragment.FragmentFriendsChat;
import com.mimcoups.Consumer.fragment.FragmentLocationDealFeed;
import com.mimcoups.Consumer.fragment.FragmentLocationShare;
import com.mimcoups.Consumer.fragment.FragmentNotificationDetail;
import com.mimcoups.Consumer.fragment.FragmentNotificationList;
import com.mimcoups.Consumer.fragment.FragmentNotificationSetting;
import com.mimcoups.Consumer.fragment.FragmentPaypalExpressCheckout;
import com.mimcoups.Consumer.fragment.FragmentPurchase;
import com.mimcoups.Consumer.fragment.FragmentQuestionDetail;
import com.mimcoups.Consumer.fragment.FragmentQuestionList;
import com.mimcoups.Consumer.fragment.FragmentSLiderFavouriteListDeelCategory;
import com.mimcoups.Consumer.fragment.FragmentSLiderLocationContact;
import com.mimcoups.Consumer.fragment.FragmentSearch;
import com.mimcoups.Consumer.fragment.FragmentSearchResult;
import com.mimcoups.Consumer.fragment.FragmentShareViaMail;
import com.mimcoups.Consumer.fragment.FragmentSliderAddCampain;
import com.mimcoups.Consumer.fragment.FragmentSliderAddWishlist;
import com.mimcoups.Consumer.fragment.FragmentSliderCampaign;
import com.mimcoups.Consumer.fragment.FragmentSliderCategory;
import com.mimcoups.Consumer.fragment.FragmentSliderCategoryDealFeed;
import com.mimcoups.Consumer.fragment.FragmentSliderContactUs;
import com.mimcoups.Consumer.fragment.FragmentSliderDoneDeals;
import com.mimcoups.Consumer.fragment.FragmentSliderFavouriteList;
import com.mimcoups.Consumer.fragment.FragmentSliderFriendNotificationRequest;
import com.mimcoups.Consumer.fragment.FragmentSliderFriendsList;
import com.mimcoups.Consumer.fragment.FragmentSliderLocationDealFeed;
import com.mimcoups.Consumer.fragment.FragmentSliderLocationDetail;
import com.mimcoups.Consumer.fragment.FragmentSliderLocationDiscuss;
import com.mimcoups.Consumer.fragment.FragmentSliderLocationMap;
import com.mimcoups.Consumer.fragment.FragmentSliderLocations;
import com.mimcoups.Consumer.fragment.FragmentSliderPrivacyPolicy;
import com.mimcoups.Consumer.fragment.FragmentSliderSaving;
import com.mimcoups.Consumer.fragment.FragmentSliderSearchByNameEmail;
import com.mimcoups.Consumer.fragment.FragmentSliderWishlist;
import com.mimcoups.Consumer.fragment.FragmentStripePayment;
import com.mimcoups.Consumer.fragment.FragmentWriteReviewDeel;
import com.mimcoups.Consumer.fragment.FragmentWriteReviewLocation;
import com.mimcoups.Consumer.fragment.Fragmentbottom;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.FavouriteListInterface;
import com.mimcoups.Consumer.inteface.LocationDetailInterface;
import com.mimcoups.Consumer.inteface.LocationListInterface;
import com.mimcoups.Consumer.inteface.QuestionDetailInterface;
import com.mimcoups.Consumer.inteface.SearchMapLocationDealFeedInterface;
import com.mimcoups.Consumer.inteface.SearchResultInterface;
import com.mimcoups.Consumer.inteface.Setchatconnection;
import com.mimcoups.Consumer.inteface.Unbindservice;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.bottombarClickInterface;
import com.mimcoups.Consumer.inteface.categoryDetailInterface;
import com.mimcoups.Consumer.inteface.discusschatinteface;
import com.mimcoups.Consumer.inteface.locationmapinterface;
import com.mimcoups.Consumer.inteface.notificationDetailInteface;
import com.mimcoups.Consumer.inteface.shareingdeal;
import com.mimcoups.Consumer.inteface.sharinglocation;
import com.mimcoups.Consumer.inteface.sliderinterface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.inteface.wrongdatedialoginterface;
import com.mimcoups.Consumer.smackchat.Connector;

public class DeelFeedActivity extends Activity implements
		bottombarClickInterface, topBarInteface, sliderinterface,
		discusschatinteface, notificationDetailInteface,
		QuestionDetailInterface, SearchResultInterface,
		SearchMapLocationDealFeedInterface, DealFeedDetailInterface,
		FavouriteListInterface, LocationListInterface, categoryDetailInterface,
		LocationDetailInterface, Observer, wrongdatedialoginterface,
		Unbindservice, shareingdeal, sharinglocation,
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, LocationListener,
		locationmapinterface, FrameLayoutThatDetectsSoftKeyboard.Listener,
		Setchatconnection {
	// private CountDownTimer countdowntimer;
	private boolean timerRunning = true;
	private DrawerLayout drawerLayout;
	private String strFragmentTag;
	private String sliderMessage = "";
	private DialogFragment progressDialogFragment;
	private SharedPreferences preference,updatepreferences;
	public CommonData commonClass;
	public ArrayList<CategoryDetail> arrCategory = new ArrayList<CategoryDetail>();
	private WebService_Call web_Service = new WebService_Call();
	private String url_response = "";
	Messenger mService = null;
	final Messenger mMessenger = new Messenger(new IncomingHandler());
	private String merchantid = "";
	String dealimagepathtemp = "";
	String dealnametemp = "";
	String type;
	// //changes
	public boolean isReviewUpdated = false, isshow = false;
	public boolean isDealListingFavourite = false;
	public boolean isDealfavourite = false;
	public boolean IsFriendRequest = false;
	public boolean isLocationFavourite = false;
	public int flDealReviewCount = 0;
	public float flDealRatingCount = 0;
	public LocationClient locationclient;
	private LocationRequest locationrequest;
	private Intent mIntentService;
	private PendingIntent mPendingIntent;
	private boolean iskeyBoardShowing = false;
	public final String RECEIVEBROADCAST = "ChatDetailActivity.android.action.broadcast";
	public final String PRESENCEBROADCAST = "ChatDetailActivity.android.action.broadcast";
	public static boolean progressbarshowing = false;
	public boolean is_chatscreen_neddto_refresh = false;
	DialogFragment dialogforconnect;
	private int counter = 0;
	Timer timer;
	MyTimerTask myTimerTask;
	String dealimagepathvalue = "";
	Thread timerThread;
	private Context context;
	boolean notification = false;
	String serverResponse="";
	String notificationmsg="";
	public class IncomingHandler extends Handler {


		@Override
		public void handleMessage(android.os.Message msg) {

			super.handleMessage(msg);
			switch (msg.what) {
			case Connector.SERVICE_CONNECT:
				try {
					if (commonClass != null) {
						if (commonClass.checkInternetConnection()) {
							getApplicationContext().unbindService(mConnection);
							getApplicationContext().bindService(
									new Intent(getApplicationContext(),
											Connector.class), mConnection,
									Context.BIND_AUTO_CREATE);
						}
					}
				} catch (Exception e) {
				}
				break;
			case Connector.MSG_REGISTER_CLIENT:
				break;
			case Connector.MSG_SET_STRING_VALUE:
				String strsucessfully = msg.getData().getString("send_data");
				if (strsucessfully != null) {
					if (strsucessfully.equalsIgnoreCase("userdeleted")) {
						Fragment fragment = getFragmentManager()
								.findFragmentById(R.id.frmDealFeed);
						String username = msg.getData().getString("username");
						if (fragment instanceof FragmentChatList) {
							try {
								((FragmentChatList) fragment)
										.deletefriend(username);
								// ((FragmentChatList) fragment).getData();
							} catch (Exception e) {
								// TODO: handle exception
							}

						}

						else if (fragment instanceof FragmentSliderFriendsList) {
							try {

								// ((FragmentSliderFriendsList) fragment)
								// .removefriends();
							} catch (Exception e) {
								Log.d("remove interface>>", "remove interface");
							}
						}
					} else {
						if (strsucessfully.equals("singlechat")) {
							supportchat chatmsg = (supportchat) msg.getData()
									.getSerializable("chatobj");
							Fragment fragment = getFragmentManager()
									.findFragmentById(R.id.frmDealFeed);
							if (fragment instanceof FragmentFriendsChat) {
								((FragmentFriendsChat) fragment)
										.MessageReceive(chatmsg);
							} else {
								String[] arrfrmid = chatmsg.from_id.split("@");
								String username = arrfrmid[0];
								SharedPreferences preference = DeelFeedActivity.this
										.getSharedPreferences("FriendList",
												Context.MODE_PRIVATE);

								int countval = preference.getInt(username, 0);

								countval = countval + 1;

								username = username.toLowerCase();

								preference.edit().putInt(username, countval)
										.commit();

								SharedPreferences badgepreference = DeelFeedActivity.this
										.getSharedPreferences("Badgecount",
												Context.MODE_PRIVATE);

								int badgecountval = badgepreference.getInt(
										"Badge", 0);
								badgecountval = badgecountval + 1;

								badgepreference.edit()
										.putInt("Badge", badgecountval)
										.commit();
								Log.e("Badge Count===", "" + badgecountval);

								try {

									Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
											.findFragmentById(
													R.id.bottomFragment);

									bottomFragment.SetBadgeCount(badgecountval);
								} catch (Exception e) {

								}

								if (fragment instanceof FragmentChatList) {
									((FragmentChatList) fragment)
											.MessageReceive();
								} else {
									Log.d("else ", "eeelse");
									// Fragment bottomfragment =
									// getFragmentManager()
									// .findFragmentById(R.id.frmBottom);
									// ((Fragmentbottom)
									// bottomfragment).badgeReceive(countval,username);

								}
							}

						} else if (strsucessfully.equals("presencechange")) {

							String username = msg.getData().getString(
									"username");
							String presence = msg.getData().getString(
									"presence");

							Fragment fragment = getFragmentManager()
									.findFragmentById(R.id.frmDealFeed);

							if (fragment instanceof FragmentChatList) {

								((FragmentChatList) fragment).Presencechange(
										username, presence);
							}

							if (fragment instanceof FragmentFriendsChat) {

								((FragmentFriendsChat) fragment)
										.Presencechange(username, presence);
							}

							if (fragment instanceof ChatDetailActivity) {

								Roster roster = commonClass.xmppconnection
										.getRoster();

								Presence availability = roster
										.getPresence(username);
								Presence.Type type = availability.getType();

								if ((type == Presence.Type.unavailable)) {

									// Intent intent = new Intent(
									// PRESENCEBROADCAST);
									// intent.putExtra("merchantusername", ""
									// + username);
									// sendBroadcast(intent);

									ChatDetailActivity chatfragment = (ChatDetailActivity) getFragmentManager()
											.findFragmentById(R.id.frmDealFeed);
									chatfragment.offlinepresence(username);
									View view = getCurrentFocus();
									if (view != null) {
										InputMethodManager inputManager = (InputMethodManager) getApplicationContext()
												.getSystemService(
														Context.INPUT_METHOD_SERVICE);
										inputManager
												.hideSoftInputFromWindow(
														view.getWindowToken(),
														InputMethodManager.HIDE_NOT_ALWAYS);
									}

									ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
									List<RunningTaskInfo> taskInfo = am
											.getRunningTasks(1);

									String Cur_Activity = taskInfo.get(0).topActivity
											.getClassName();

									if (!(Cur_Activity.contains(context
											.getPackageName()))) {
										commonClass.ismerchantoffline = true;

									} else {
										onBackPressed();
									}

								}

							}
						} else if (strsucessfully.equals("0")) {

						} else if (strsucessfully.equals("1")) {

							Log.i("connection successfully",
									"connection successfull");

							if (merchantid.equals("")) {
								// login
							} else {
								// Toast.makeText(getApplicationContext(),
								// "Try to connect", 9000).show();
								// connectwithMerchant(dealimagepathtemp,
								// dealnametemp);
							}

						} else if (strsucessfully.equals("2")) {

							Fragment fragment = getFragmentManager()
									.findFragmentById(R.id.frmDealFeed);
							if (fragment instanceof ChatDetailActivity) {
								ChatDetailActivity
										.MessageReceive(commonClass.supportChatsend);
							}

							// Intent intent = new Intent(RECEIVEBROADCAST);
							// intent.putExtra("supportchat",
							// commonClass.supportChatsend);
							// sendBroadcast(intent);
							// ChatDetailActivity
							// .MessageReceive(commonClass.supportChatsend);
						}
						
						
						else if (strsucessfully.equals("merchantreject")) {
							
							
							Log.e("Counter", ""+commonClass.connectCount);
							if(commonClass.connectCount==0){
								
								if (timerRunning) {
									commonClass.acceptrequest = 2;
									timerRunning = false;
									// hideDialog();
									hideDialog_forconnecting();

									if (timer != null) {
										timer.cancel();
										// timer = null;
									}

									runOnUiThread(new Runnable() {

										@Override
										public void run() {
											// textCounter.setText(strDate);

											if (!dealimagepathvalue.equals("")) {
												dicussDetailChatClick(
														"",
														dealimagepathvalue);
												// dealimagepathvalue = "";
											}

										}
									});
								}
								
							}

							

							// intent.putExtra("supportchat",
							// commonClass.supportChatsend);
							// sendBroadcast(intent);
							// ChatDetailActivity
							// .MessageReceive(commonClass.supportChatsend);
						}

						else if (strsucessfully.equals("merchantconnect")) {

							if (timerRunning) {
								commonClass.acceptrequest = 2;
								timerRunning = false;
								// hideDialog();
								hideDialog_forconnecting();

								if (timer != null) {
									timer.cancel();
									// timer = null;
								}

								runOnUiThread(new Runnable() {

									@Override
									public void run() {
										// textCounter.setText(strDate);

										if (!dealimagepathvalue.equals("")) {
											dicussDetailChatClick(
													commonClass.representativeid,
													dealimagepathvalue);
											// dealimagepathvalue = "";
										}

									}
								});
							}

							// intent.putExtra("supportchat",
							// commonClass.supportChatsend);
							// sendBroadcast(intent);
							// ChatDetailActivity
							// .MessageReceive(commonClass.supportChatsend);
						}
					}

				}

				hideDialog();
				break;
			default:
				super.handleMessage(msg);
			}
		}
	}

	private ServiceConnection mConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder service) {
			try {
				mService = new Messenger(service);
				android.os.Message msg = android.os.Message.obtain(null,
						Connector.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);

				startService(new Intent(getApplicationContext(),
						Connector.class));
			} catch (Exception e) {

			}
		}

		public void onServiceDisconnected(ComponentName className) {
			mService = null;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		preference = getSharedPreferences("UserDetail", Context.MODE_PRIVATE);
		updatepreferences = getSharedPreferences("updateapp", Context.MODE_PRIVATE);
		commonClass = (CommonData) getApplicationContext();

		Log.d("on create", "on create");
		commonClass.setObserver();
		commonClass.getObserver().addObserver(this);
		if (preference.getString("username", " ").equals(" ")) {
			Intent intent = new Intent(DeelFeedActivity.this,
					RegisterActivity.class);
			startActivity(intent);
			finish();
		} else {
			if (commonClass.languageId.equals("1")) {
				changeLanguage("en");
			} else {
				changeLanguage("no");
			}

			setContentView(R.layout.activity_deel_feed_detail);
			FrameLayoutThatDetectsSoftKeyboard mainLayout = (FrameLayoutThatDetectsSoftKeyboard) findViewById(R.id.frmsoftkeyboard);
			mainLayout.setListener(this);
			initControls();

			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			notification = getIntent().getExtras().getBoolean("notification");
			type = getIntent().getExtras().getString("type");
			notificationmsg=getIntent().getExtras().getString("msg");

			Log.e("Oncrete called=====", "" + notification + "====" + type);

			/* Location GOOGLE PLAY START */

			int resp = GooglePlayServicesUtil
					.isGooglePlayServicesAvailable(this);
			if (resp == ConnectionResult.SUCCESS) {
				// Toast.makeText(this, "Connection Success" + resp,
				// Toast.LENGTH_LONG)
				// .show();
				locationclient = new LocationClient(this, this, this);
				locationclient.connect();

			} else {
				Toast.makeText(this, "Google Play Service Error " + resp,
						Toast.LENGTH_LONG).show();
			}

			mIntentService = new Intent(this, FusedLocationService.class);
			mPendingIntent = PendingIntent.getService(this, 1, mIntentService,
					0);

			/* LOCATION GOOGLE PLAY END */

			Fragment newFragment = null;
			if (notification) {

				try {
					commonClass = (CommonData) getApplicationContext();
					if (commonClass.xmppconnection.isConnected()
							&& commonClass.checkInternetConnection()) {
						commonClass.xmppconnection.disconnect();
					}
					if (mConnection != null) {
						getApplicationContext().unbindService(mConnection);
					}

				} catch (Exception e) {
					// TODO: handle exception


				}

				initopenfireconnection();

				Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
						.findFragmentById(R.id.bottomFragment);
				bottomFragment.selectNotification();

				Log.d("type is>>", "" + type);


				if(type.trim().equalsIgnoreCase("newappversion")){

					newFragment = new FragmentDealFeed();
					strFragmentTag = newFragment.toString();
					transaction.replace(R.id.frmDealFeed, newFragment,
							strFragmentTag);
					transaction.commitAllowingStateLoss();

					DialogFragment ds = new UpdateapplicationDialog(new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							if(tag.equalsIgnoreCase("update")){

								SharedPreferences.Editor  editor = getSharedPreferences("updateapp", MODE_PRIVATE).edit();
								editor.putLong("updateapptime",0);
								editor.commit();

								final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
								try {
									startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
								} catch (android.content.ActivityNotFoundException anfe) {
									startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
								}

							}else{

								long current=System.currentTimeMillis();

								SharedPreferences.Editor  editor = getSharedPreferences("updateapp", MODE_PRIVATE).edit();
								editor.putLong("updateapptime",current);
								editor.putString("updatemsg",notificationmsg);
								editor.commit();

								Toast.makeText(getApplicationContext(),"Cancel Notification",Toast.LENGTH_SHORT).show();

							}

						}
					}, notificationmsg);

					ds.setCancelable(false);
					ds.show(getFragmentManager(), "");

				}
				else if (type.equalsIgnoreCase("favcatloc")
						|| type.equalsIgnoreCase("favcatdeal")) {
					NotificationNavigation("Cateogry");

					Fragment currentFragment = getFragmentManager()
							.findFragmentById(R.id.frmDealFeed);

					if (currentFragment instanceof ChatDetailActivity) {

						Log.e("Oncreate Type====", type);
						((ChatDetailActivity) currentFragment)
								.rejectAsynctask();
					}

					// notificationType("Cateogry");

				} else if (type.equalsIgnoreCase("favloc")
						|| type.equalsIgnoreCase("onsite")
						|| type.equalsIgnoreCase("nearby")) {
					// notificationType("Location");
					NotificationNavigation(type);

					Fragment currentFragment = getFragmentManager()
							.findFragmentById(R.id.frmDealFeed);

					if (currentFragment instanceof ChatDetailActivity) {

						Log.e("Oncreate Type====", type);
						((ChatDetailActivity) currentFragment)
								.rejectAsynctask();
					}

				} else if (type.equalsIgnoreCase("favlocdeal")
						|| type.equalsIgnoreCase("favdeal")) {
					// notificationType("Deal");

					NotificationNavigation("Deal");

					Fragment currentFragment = getFragmentManager()
							.findFragmentById(R.id.frmDealFeed);

					if (currentFragment instanceof ChatDetailActivity) {
						Log.e("Oncreate Type====", type);
						((ChatDetailActivity) currentFragment)
								.rejectAsynctask();
					}
				}

				else if (type.equalsIgnoreCase("loyalty")) {
					// notificationType("Loyality");
					NotificationNavigation("Loyality");

					Fragment currentFragment = getFragmentManager()
							.findFragmentById(R.id.frmDealFeed);

					if (currentFragment instanceof ChatDetailActivity) {
						Log.e("Oncreate Type====", type);
						((ChatDetailActivity) currentFragment)
								.rejectAsynctask();
					}
				}

				else if (type.equalsIgnoreCase("wishlist")) {
					// notificationType("wishlist");
					NotificationNavigation("wishlist");

					Fragment currentFragment = getFragmentManager()
							.findFragmentById(R.id.frmDealFeed);

					if (currentFragment instanceof ChatDetailActivity) {
						Log.e("Oncreate Type====", type);
						((ChatDetailActivity) currentFragment)
								.rejectAsynctask();
					}
				}

				else if (type.equalsIgnoreCase("friend")) {
					// notificationType("FriendRequest");
					NotificationNavigation("FriendRequest");

					Fragment currentFragment = getFragmentManager()
							.findFragmentById(R.id.frmDealFeed);

					if (currentFragment instanceof ChatDetailActivity) {
						Log.e("Oncreate Type====", type);
						((ChatDetailActivity) currentFragment)
								.rejectAsynctask();
					}

				}

				else if (type.equalsIgnoreCase("mims")) {
					Log.d("type is mims", "mims");
					// notificationType("Mimcoups");
					NotificationNavigation("Mimcoups");

					Fragment currentFragment = getFragmentManager()
							.findFragmentById(R.id.frmDealFeed);

					if (currentFragment instanceof ChatDetailActivity) {
						Log.e("Oncreate Type====", type);
						((ChatDetailActivity) currentFragment)
								.rejectAsynctask();
					}

				} else if (type.equalsIgnoreCase("chat")) {

					bottomFragment.selectDiscussNotification();

					NotificationNavigation("chat");

					Fragment currentFragment = getFragmentManager()
							.findFragmentById(R.id.frmDealFeed);

					if (currentFragment instanceof ChatDetailActivity) {
						Log.e("Oncreate Type====", type);
						((ChatDetailActivity) currentFragment)
								.rejectAsynctask();
					}

				}

				// if (type.equalsIgnoreCase("mim")) {
				// Log.d("type is mims", "mims");
				// notificationType("Mimcoups");
				//
				// }

			} else {
				newFragment = new FragmentDealFeed();
				strFragmentTag = newFragment.toString();
				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);
				transaction.commitAllowingStateLoss();
			}


			long applaunchtime=updatepreferences.getLong("updateapptime",0); // get previous lauchtime

			if(applaunchtime>0){

				long current=System.currentTimeMillis();

				long difference=current-applaunchtime;

				Log.e("Count Day",""+TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS));

				long countday=TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS);

				if(countday >3){

					final String updatemsg=updatepreferences.getString("updatemsg","");

					DialogFragment ds = new UpdateapplicationDialog(new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							if(tag.equalsIgnoreCase("update")){

								SharedPreferences.Editor  editor = getSharedPreferences("updateapp", MODE_PRIVATE).edit();
								editor.putLong("updateapptime",0);
								editor.commit();

								final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
								try {
									startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
								} catch (android.content.ActivityNotFoundException anfe) {
									startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
								}

							}else{

								long current=System.currentTimeMillis();

								SharedPreferences.Editor  editor = getSharedPreferences("updateapp", MODE_PRIVATE).edit();
								editor.putLong("updateapptime",current);
								editor.putString("updatemsg",updatemsg);
								editor.commit();


							}

						}
					}, updatemsg);

					ds.setCancelable(false);
					ds.show(getFragmentManager(), "");


				}


			}
			setDrawer();

			Log.d("call conneciton", "connection");
			initopenfireconnection();

			// startService(new Intent(getApplicationContext(),
			// GPSTracker.class));
			// for login in opnfire
			String languageName = Locale.getDefault().getDisplayLanguage();

			if (commonClass.checkInternetConnection()) {
				async_deel_feed_list as = new async_deel_feed_list(
						new asynctaskloaddata() {

							@Override
							public void onPreExecute() {

							}

							@Override
							public void doInBackground() {
								String get_url = "CategoryService.svc/GetAllCategories?customerid="
										+ commonClass.CustomerId
										+ "&languageid="
										+ commonClass.languageId;
								url_response = web_Service
										.makeServicegetRequest(get_url);
								try {
									JSONObject json_result = new JSONObject(
											url_response);
									JSONArray json_allDetails = json_result
											.getJSONArray("Categories");
									for (int i = 0; i < json_allDetails
											.length(); i++) {
										CategoryDetail category = new CategoryDetail();
										JSONObject json_Category = json_allDetails
												.getJSONObject(i);
										category = category
												.parseJson(json_Category);
										if (category != null) {
											arrCategory.add(category);
										}
									}
								} catch (Exception e) {

								}
							}

							@Override
							public void onPostExecute() {

							}
						});
				as.execute();
			}

			// location Services

			context = DeelFeedActivity.this;

			preference = getSharedPreferences("UserDetail",
					Context.MODE_PRIVATE);
			if (preference.getBoolean("launch", false)) {
				// if (!checkLocationService()) {
				// DialogFragment dsp = new SingleButtonAlert(
				// new DialogInterfaceClick() {
				//
				// @Override
				// public void dialogClick(String tag) {
				// // TODO Auto-generated method stub
				//
				// }
				// }, getResources().getString(
				// R.string.LocationService));
				// dsp.show(getFragmentManager(), "");
				// }
				Editor edt = preference.edit();
				edt.putBoolean("launch", false);
				edt.commit();
			}
		}

	}

	private void initControls() {
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

	}

	@Override
	public void bottombarClick(String string) {

		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		Fragment fragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		isshow = false;

		if (string.equals("DeelFeed")) {

			if (fragment instanceof FragmentDealFeed) {
			} else if (fragment instanceof ChatDetailActivity) {
				// getFragmentManager().popBackStack(null,
				// FragmentManager.POP_BACK_STACK_INCLUSIVE);
				// ((ChatDetailActivity) fragment).rejectAsynctask();

				timerRunning = false;
				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Fragment newFragment = new FragmentDealFeed();
				strFragmentTag = newFragment.toString();
				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);
				transaction.commit();

			} else {
				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Fragment newFragment = new FragmentDealFeed();
				strFragmentTag = newFragment.toString();
				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);

				transaction.commit();
			}
		} else if (string.equals("FriendPurchase")) {

			if (fragment instanceof FragmentFriendPurchase) {
			} else if (fragment instanceof ChatDetailActivity) {
				// getFragmentManager().popBackStack(null,
				// FragmentManager.POP_BACK_STACK_INCLUSIVE);
				// ((ChatDetailActivity) fragment).rejectAsynctask();

				timerRunning = false;
				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Fragment newFragment = new FragmentFriendPurchase();
				strFragmentTag = newFragment.toString();
				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);
				transaction.commit();
			} else {
				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Fragment newFragment = new FragmentFriendPurchase();
				strFragmentTag = newFragment.toString();
				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);
				transaction.commit();
			}
		} else if (string.equals("Discuss")) {

			if (fragment instanceof FragmentChatList) {
			}

			else if (fragment instanceof ChatDetailActivity) {
				// getFragmentManager().popBackStack(null,
				// FragmentManager.POP_BACK_STACK_INCLUSIVE);
				// ((ChatDetailActivity) fragment).rejectAsynctask();
				Log.d("chat detail-> -> -> -> ", "discuss");

				timerRunning = false;
				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Fragment newFragment = new FragmentChatList();
				strFragmentTag = newFragment.toString();
				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);
				transaction.commit();

			} else {
				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Fragment newFragment = new FragmentChatList();
				strFragmentTag = newFragment.toString();
				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);
				transaction.commit();
			}
		} else if (string.equals("Notification")) {

			if (fragment instanceof FragmentNotificationList) {
			} else if (fragment instanceof ChatDetailActivity) {

				Log.d("chat detail-> -> -> -> ", "notification");
				// ((ChatDetailActivity) fragment).rejectAsynctask();

				timerRunning = false;

				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Fragment newFragment = new FragmentNotificationList();
				strFragmentTag = newFragment.toString();
				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);
				transaction.commit();

			} else {

				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);
				Fragment newFragment = new FragmentNotificationList();
				strFragmentTag = newFragment.toString();
				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);
				transaction.commit();
			}
		} else if (string.equals("More")) {
			drawerLayout.openDrawer(Gravity.RIGHT);
		}

	}

	@Override
	public void topbarClick(String message) {

		Fragment currentFragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		if (message.equals("questions")) {
			if (currentFragment instanceof FragmentQuestionList) {

			} else {

				View view = this.getCurrentFocus();
				if (view != null) {
					InputMethodManager inputManager = (InputMethodManager) this
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
				}
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentQuestionList();
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		} else if (message.equals("search")) {
			if (currentFragment instanceof FragmentSearch) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSearch();
				strFragmentTag = newFragment.toString();
				String searchname = "";

				if (currentFragment.toString().contains(
						"FragmentSliderLocations")
						|| currentFragment.toString().contains(
								"FragmentSliderLocationMap")) {
					searchname = "location";
				} else {
					searchname = "deal";
				}

				Bundle bundler = new Bundle();
				bundler.putString("search", searchname);
				newFragment.setArguments(bundler);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("notificationSetting")) {

			if (currentFragment instanceof FragmentNotificationSetting) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentNotificationSetting();
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		} else if (message.equals("searchByName")) {
			if (currentFragment instanceof FragmentSliderSearchByNameEmail) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderSearchByNameEmail();
				strFragmentTag = newFragment.toString();
				Bundle bundler = new Bundle();
				bundler.putString("search", "searchByName");
				newFragment.setArguments(bundler);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		} else if (message.equals("searchByEmail")) {

			if (currentFragment instanceof FragmentSliderSearchByNameEmail) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderSearchByNameEmail();
				strFragmentTag = newFragment.toString();
				Bundle bundler = new Bundle();
				bundler.putString("search", "searchByEmail");
				newFragment.setArguments(bundler);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("back")) {
			
			Log.e("Current Frragment back", currentFragment.toString());
			
			
			View view = this.getCurrentFocus();
			if (view != null) {
				InputMethodManager inputManager = (InputMethodManager) this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(view.getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
			}

			if ((currentFragment instanceof FragmentNotificationDetail)
					|| (currentFragment instanceof FragmentSliderFriendNotificationRequest)) {
				try {
					commonClass.getObserver().setValue(
							"updatenotificationsettings");
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			onBackPressed();
		} else if (message.equals("locationMap")) {
			if (currentFragment instanceof FragmentSliderLocationMap) {

			} else {

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderLocationMap();
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("wishlistadd")) {

			if (currentFragment instanceof FragmentSliderAddWishlist) {

			} else {
				// new wishlist added screen
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderAddWishlist();
				Bundle bundle = new Bundle();
				bundle.putSerializable("UpdateWishlist", null);
				newFragment.setArguments(bundle);
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("wishlist Sucessfully")) {
			onBackPressed();
		} else if (message.equals("campaignAdd")) {

			if (currentFragment instanceof FragmentSliderAddCampain) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderAddCampain();
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("sendInvites")) {

			View view = this.getCurrentFocus();
			if (view != null) {
				InputMethodManager inputManager = (InputMethodManager) this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(view.getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
			}
			onBackPressed();
		} else if (message.equals("notificationsettingsetting")) {
			onBackPressed();
		} else if (message.equals("searchCancel")) {
			onBackPressed();
		} else if (message.equals("Locations")) {
			onBackPressed();
		}
	}

	@Override
	public void sliderclick(final String message) {

		drawerLayout.closeDrawer(Gravity.RIGHT);
		// Toast.makeText(getApplicationContext(), "slider click" + message,
		// 9000)
		// .show();

		sliderMessage = message;
		// FragmentTransaction transaction = getFragmentManager()
		// .beginTransaction();
		// Fragment fragment = getFragmentManager().findFragmentById(
		// R.id.frmDealFeed);
		// ((ChatDetailActivity) fragment).rejectAsynctask();

		setDrawer();
	}

	public void setDrawer() {
		drawerLayout.setDrawerListener(new DrawerListener() {

			@Override
			public void onDrawerStateChanged(int arg0) {
			}

			@Override
			public void onDrawerSlide(View arg0, float arg1) {
				View view = DeelFeedActivity.this.getCurrentFocus();
				if (view != null) {
					InputMethodManager inputManager = (InputMethodManager) DeelFeedActivity.this
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(view.getWindowToken(),
							InputMethodManager.HIDE_NOT_ALWAYS);
				}

			}

			@Override
			public void onDrawerOpened(View arg0) {

			}

			@Override
			public void onDrawerClosed(View arg0) {

				if (sliderMessage.equals("logout")) {

					Intent intent = new Intent(getApplicationContext(),
							RegisterActivity.class);
					startActivity(intent);
					finish();
				} else {
					FragmentTransaction transaction = getFragmentManager()
							.beginTransaction();
					Fragment addFragment = null;
					Fragment currentFragment = getFragmentManager()
							.findFragmentById(R.id.frmDealFeed);

					if (sliderMessage.equals("location")) {

						// if (!(currentFragment instanceof
						// FragmentSliderLocations)) {



						slidebarchatclosed();

						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();

						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderLocations();
						// }
					} else if (sliderMessage.equals("category")) {

						// if (!(currentFragment instanceof
						// FragmentSliderCategory)) {
						slidebarchatclosed();
						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();

						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderCategory();
						// }
					} else if (sliderMessage.equals("favourites")) {

						// if (!(currentFragment instanceof
						// FragmentSliderFavouriteList)) {
						slidebarchatclosed();
						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();
						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderFavouriteList();
						// }
					} else if (sliderMessage.equals("wishlist")) {

						// if (!(currentFragment instanceof
						// FragmentSliderWishlist)) {
						slidebarchatclosed();
						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();
						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderWishlist();
						// }
					} else if (sliderMessage.equals("saving")) {

						// if (!(currentFragment instanceof
						// FragmentSliderSaving)) {
						slidebarchatclosed();
						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();
						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderSaving();
						// }

						// if (!(currentFragment instanceof
						// FragmentFriendPurchase)) {
						// getFragmentManager().popBackStack(null,
						// FragmentManager.POP_BACK_STACK_INCLUSIVE);
						// addFragment = new FragmentFriendPurchase();
						// }

					} else if (sliderMessage.equals("done deals")) {

						// if (!(currentFragment instanceof
						// FragmentSliderDoneDeals)) {
						slidebarchatclosed();
						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();
						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderDoneDeals();
						// }

					} else if (sliderMessage.equals("campaign")) {

						// if (!(currentFragment instanceof
						// FragmentSliderCampaign)) {
						slidebarchatclosed();
						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();
						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderCampaign();
						// }

					} else if (sliderMessage.equals("privacy policy")) {

						// if (!(currentFragment instanceof
						// FragmentSliderPrivacyPolicy)) {
						slidebarchatclosed();
						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();
						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderPrivacyPolicy();
						// }
					} else if (sliderMessage.equals("Friend")) {

						// if (!(currentFragment instanceof
						// FragmentSliderFriendsList)) {
						slidebarchatclosed();
						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();
						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderFriendsList();
						// }
					} else if (sliderMessage.equals("Contact us")) {

						// if (!(currentFragment instanceof
						// FragmentSliderContactUs)) {
						slidebarchatclosed();
						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();
						getFragmentManager().popBackStack(null,
								FragmentManager.POP_BACK_STACK_INCLUSIVE);
						addFragment = new FragmentSliderContactUs();
						// }
					} else if (sliderMessage.equals("userprofile")) {

						Fragment fragment = getFragmentManager()
								.findFragmentById(R.id.frmDealFeed);
						if (fragment instanceof ChatDetailActivity) {
							// getFragmentManager().popBackStack(null,
							// FragmentManager.POP_BACK_STACK_INCLUSIVE);

							slidebarchatclosed();
							getFragmentManager().popBackStack(null,
									FragmentManager.POP_BACK_STACK_INCLUSIVE);
						}

						sliderMessage = "";
						Intent intent = new Intent(getApplicationContext(),
								UserProfileActivity.class);
						startActivity(intent);
					}
					if (addFragment != null) {

						Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
								.findFragmentById(R.id.bottomFragment);
						bottomFragment.unclickableButtons();
						strFragmentTag = addFragment.toString();
						transaction.replace(R.id.frmDealFeed, addFragment,
								strFragmentTag);
						transaction.commit();
					}
					if (!(sliderMessage.equals("userprofile"))) {

						sliderMessage = "";
						// Fragmentbottom bottomFragment = (Fragmentbottom)
						// getFragmentManager()
						// .findFragmentById(R.id.bottomFragment);
						// bottomFragment.unclickableButtons();
					}
				}
			}
		});
	}

	// listview click Events
	@Override
	public void dicusschatclick(String dealimagepath, String dealname) {
		merchantid = dealimagepath;

		dealimagepathtemp = dealimagepath;
		dealnametemp = dealname;

		if (commonClass.xmppconnection != null
				&& commonClass.xmppconnection.isConnected()
				&& commonClass.checkInternetConnection()) {

			// Toast.makeText(getApplicationContext(), "xmpp connection ", 9000)
			// .show();

			connectwithMerchant(dealimagepath, dealname);

		} else {

			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.connectionsorrymsg), Toast.LENGTH_LONG)
					.show();

			try {

				setchatconnection();

				// getApplicationContext().unbindService(mConnection);
				// getApplicationContext().bindService(
				// new Intent(getApplicationContext(), Connector.class),
				// mConnection, Context.BIND_AUTO_CREATE);
			} catch (Exception e) {

			}

		}
	}

	@Override
	public void dicussDetailChatClick(String id, String dealimagepath) {
		try {
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment fragment = getFragmentManager().findFragmentById(
					R.id.frmDealFeed);
			if (fragment instanceof ChatDetailActivity) {

			} else {
				
				if(id.trim().length()==0){
					sendrejectmessage();
					
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.connectionsorrymsg), Toast.LENGTH_SHORT).show();
					
				}else{
					
					makeFriend(id, commonClass.strUsername);
					Fragment newFragment = new ChatDetailActivity();
					Bundle bundle = new Bundle();
					bundle.putString("chatid", id);
					bundle.putString("dealimagepath", dealimagepath);
					newFragment.setArguments(bundle);
					// strFragmentTag = newFragment.toString();
					transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
					transaction.addToBackStack(strFragmentTag);
					transaction.commit();
				}
				
				// Pendingrequest p = new Pendingrequest();
				// p.dealimagepath = dealimagepath;
				// p.chatid = id;
				// commonClass.arrpendingrequest.add(p);

			}

		} catch (Exception e) {
			Log.e("called==", e.getMessage().toString());
		}

	}

	private void makeFriend(final String to_id, final String username) {
		// TODO Auto-generated method stub

		class friendhandshake extends AsyncTask<Void, Void, Void> {

			String response = "", response1 = "";

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				String url = "http://"
						+ commonClass.strServerName
						+ ":9090/plugins/userService/userservice?type=add_roster&secret=YhzIocug&username="
						+ to_id + "&item_jid=" + username + "@"
						+ commonClass.strServerName + "&name=&subscription=3";

				WebService_Call web = new WebService_Call();
				response = web.makeServicegetRequestFriend(url);

				String url1 = "http://"
						+ commonClass.strServerName
						+ ":9090/plugins/userService/userservice?type=add_roster&secret=YhzIocug&username="
						+ username + "&item_jid=" + to_id + "@"
						+ commonClass.strServerName + "&name=&subscription=3";

				response1 = web.makeServicegetRequestFriend(url1);

				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				// TODO Auto-generated method stub

				super.onPostExecute(result);
			}

		}

		new friendhandshake().execute();

	}

	@Override
	public void notificationType(String notificationType) {

		Fragment tempFragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);
		Bundle bundle = null;
		if (notificationType.equalsIgnoreCase("onsite")) {

			if (!(tempFragment instanceof FragmentNotificationDetail)) {
				bundle = new Bundle();
				bundle.putString("isLocation", "true");
				bundle.putString("notificationtype", "onsite");
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentNotificationDetail();
				newFragment.setArguments(bundle);
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (notificationType.equalsIgnoreCase("FriendRequest")) {
			if (!(tempFragment instanceof FragmentSliderFriendNotificationRequest)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderFriendNotificationRequest();
				newFragment.setArguments(bundle);
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else {

			if (!(tempFragment instanceof FragmentNotificationDetail)) {
				bundle = new Bundle();
				bundle.putString("isLocation", "false");
				if (notificationType.equals("Cateogry")) {
					bundle.putString("notificationtype", "Cateogry");
				}

				if (notificationType.equals("Deal")) {
					bundle.putString("notificationtype", "Deal");
				}
				if (notificationType.equals("Loyality")) {
					bundle.putString("notificationtype", "Loyality");
				}
				if (notificationType.equals("wishlist")) {
					bundle.putString("notificationtype", "wishlist");
				}
				if (notificationType.equals("FriendRequest")) {
					bundle.putString("notificationtype", "FriendRequest");
				}
				if (notificationType.equals("Mimcoups")) {
					bundle.putString("notificationtype", "Mimcoups");
				}

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentNotificationDetail();
				newFragment.setArguments(bundle);
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		}

		// if (notificationType.equals("Location")) {
		// bundle = new Bundle();
		// bundle.putString("isLocation", "true");
		// } else {
		// bundle = new Bundle();
		// bundle.putString("isLocation", "false");
		// }
		// FragmentTransaction transaction = getFragmentManager()
		// .beginTransaction();
		// Fragment newFragment = new FragmentNotificationDetail();
		// newFragment.setArguments(bundle);
		// strFragmentTag = newFragment.toString();
		// transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
		// transaction.addToBackStack(strFragmentTag);
		// transaction.commit();
	}

	@Override
	public void onBackPressed() {

		Fragment currentFragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		Log.e("called current fragmnet", "" + currentFragment.toString());
		if (currentFragment instanceof FragmentPaypalExpressCheckout) {
			super.onBackPressed();
			
			 currentFragment = getFragmentManager().findFragmentById(
					R.id.frmDealFeed);
			 if(currentFragment instanceof FragmentDealFeed){
				 
			 }else{
				 super.onBackPressed();
			 }
			
			
		}else if (currentFragment instanceof FragmentStripePayment) {
			super.onBackPressed();

			currentFragment = getFragmentManager().findFragmentById(
					R.id.frmDealFeed);
			if(currentFragment instanceof FragmentDealFeed){

			}else{
				super.onBackPressed();
			}



		}

		else if (currentFragment instanceof FragmentSearchResult) {
			super.onBackPressed();
			super.onBackPressed();
		} else if (currentFragment instanceof FragmentSliderContactUs
				|| currentFragment instanceof FragmentSliderFriendsList
				|| currentFragment instanceof FragmentSliderPrivacyPolicy
				|| currentFragment instanceof FragmentSliderCampaign
				|| currentFragment instanceof FragmentSliderDoneDeals
				|| currentFragment instanceof FragmentSliderSaving
				|| currentFragment instanceof FragmentSliderWishlist
				|| currentFragment instanceof FragmentSliderFavouriteList
				|| currentFragment instanceof FragmentSliderCategory
				|| currentFragment instanceof FragmentSliderLocations) {

			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentDealFeed();
			strFragmentTag = newFragment.toString();
			transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.commit();

			Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
					.findFragmentById(R.id.bottomFragment);
			bottomFragment.setColor();

		}

		else if (currentFragment instanceof ChatDetailActivity) {

			// ((ChatDetailActivity) currentFragment).rejectAsynctask();
			timerRunning = false;
			super.onBackPressed();
		} else if (currentFragment instanceof FragmentFriendsChat) {

			if (is_chatscreen_neddto_refresh) {
				commonClass.getObserver().setValue("updatedChatList");
			} else {
				super.onBackPressed();

			}

		}

		else if (currentFragment instanceof FragmentDealFeed
				|| currentFragment instanceof FragmentFriendPurchase
				|| currentFragment instanceof FragmentChatList
				|| currentFragment instanceof FragmentNotificationList
				|| currentFragment instanceof FragmentLocationDealFeed) {

			if (isshow) {

				finish();

			} else {

				final Dialog dialog = new Dialog(DeelFeedActivity.this);
				isshow = true;

				dialog.setTitle(getResources().getString(R.string.app_name));
				// //
				dialog.setContentView(R.layout.exitapp_dialog);

				Button btnok = (Button) dialog.findViewById(R.id.btnok);

				btnok.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						dialog.dismiss();

					}
				});

				dialog.setOnCancelListener(new OnCancelListener() {

					@Override
					public void onCancel(DialogInterface dialog) {
						// TODO Auto-generated method stub

						dialog.dismiss();

					}
				});

				dialog.show();

			}

		} else if (currentFragment instanceof FragmentNotificationDetail) {

			// if (notification) {
			// FragmentTransaction transaction = getFragmentManager()
			// .beginTransaction();
			// Fragment newFragment = new FragmentNotificationList();
			// strFragmentTag = newFragment.toString();
			// transaction.replace(R.id.frmDealFeed, newFragment,
			// strFragmentTag);
			// transaction.commit();
			// } else {
			// super.onBackPressed();
			// }

			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentNotificationList();
			strFragmentTag = newFragment.toString();
			transaction.replace(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.commit();

		} else if (currentFragment instanceof FragmentSliderFriendNotificationRequest) {

			// if (notification) {
			// FragmentTransaction transaction = getFragmentManager()
			// .beginTransaction();
			// Fragment newFragment = new FragmentNotificationList();
			// strFragmentTag = newFragment.toString();
			// transaction.replace(R.id.frmDealFeed, newFragment,
			// strFragmentTag);
			// transaction.commit();
			// } else {
			// super.onBackPressed();
			// }
			//

			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentNotificationList();
			strFragmentTag = newFragment.toString();
			transaction.replace(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.commit();
		}

		else {

			if (isReviewUpdated) {

				isReviewUpdated = false;
				Fragment tempFragment = getFragmentManager().findFragmentById(
						R.id.frmDealFeed);
				if (tempFragment instanceof FragmentDealDetail) {
					((FragmentDealDetail) tempFragment).ratingupdated(
							flDealRatingCount, flDealReviewCount);
				} else if (tempFragment instanceof FragmentSliderLocationDetail) {
					((FragmentSliderLocationDetail) tempFragment)
							.ratingupdated(flDealRatingCount);
				}
			} else if (isDealListingFavourite) {

				isDealListingFavourite = false;
				Fragment tempFragment = getFragmentManager().findFragmentById(
						R.id.frmDealFeed);
				if (tempFragment instanceof FragmentSliderFavouriteList) {

					((FragmentSliderFavouriteList) tempFragment).getAllData();
				}
			} else if (isLocationFavourite) {

				isLocationFavourite = false;
				isDealListingFavourite = true;
				Fragment tempFragment = getFragmentManager().findFragmentById(
						R.id.frmDealFeed);
				if (tempFragment instanceof FragmentSLiderFavouriteListDeelCategory) {
					((FragmentSLiderFavouriteListDeelCategory) tempFragment).strpageIndex = "1";
					((FragmentSLiderFavouriteListDeelCategory) tempFragment)
							.getAllFavouriteLocation();
				}
			} else if (isDealfavourite) {
				isDealfavourite = false;
				isDealListingFavourite = true;
				isLocationFavourite = true;
				Fragment tempFragment = getFragmentManager().findFragmentById(
						R.id.frmDealFeed);
				if (tempFragment instanceof FragmentSLiderFavouriteListDeelCategory) {
					((FragmentSLiderFavouriteListDeelCategory) tempFragment).strpageIndex = "1";
					((FragmentSLiderFavouriteListDeelCategory) tempFragment)
							.getAllFavouriteDeals();
				}
			} else if (IsFriendRequest) {
				IsFriendRequest = false;
				isDealListingFavourite = true;
				isLocationFavourite = true;
				Fragment tempFragment = getFragmentManager().findFragmentById(
						R.id.frmDealFeed);
				if (tempFragment instanceof FragmentNotificationList) {
					((FragmentNotificationList) tempFragment)
							.setNotificationCount();
				}

			}
			Log.e("called fragment",
					IsFriendRequest + "===" + currentFragment.toString());
			super.onBackPressed();
		}

	}

	@Override
	public void questionDetailClick(String id) {

		Fragment tempFragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);
		if (!(tempFragment instanceof FragmentQuestionDetail)) {

			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentQuestionDetail();
			Bundle bundler = new Bundle();
			bundler.putString("questionId", id);
			newFragment.setArguments(bundler);
			strFragmentTag = newFragment.toString();
			transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.addToBackStack(strFragmentTag);
			transaction.commit();
		}
	}

	@Override
	public void searchDetail(String message, String searchkeyword) {
		Fragment tempFragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		if (!(tempFragment instanceof FragmentSearchResult)) {

			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentSearchResult();
			strFragmentTag = newFragment.toString();
			String searchname = "";
			if (message.equals("location")) {
				searchname = "store";
			} else {
				searchname = "deal";
			}
			Bundle bundler = new Bundle();
			bundler.putString("search", searchname);
			bundler.putString("searchkeyword", searchkeyword);
			newFragment.setArguments(bundler);
			transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.addToBackStack(strFragmentTag);
			transaction.commit();
		}
	}

	@Override
	public void searchmapLocationDeal(String message) {

		Fragment tempFragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		if (!(tempFragment instanceof FragmentDealFeed)) {

			getFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentDealFeed();
			strFragmentTag = newFragment.toString();
			transaction.replace(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.commit();
		}
	}

	@Override
	public void dealFeedClick(String message, DealFeedDetail dealFeedDetail) {
		Fragment tempFragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		if (message.equals("dealmap")) {

			if (!(tempFragment instanceof FragmentDealDetailsMap)) {

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();

				Fragment newFragment = new FragmentDealDetailsMap();
				strFragmentTag = newFragment.toString();

				Bundle bundler = new Bundle();
				bundler.putSerializable("location_map", dealFeedDetail);
				bundler.putBoolean("key", false);
				newFragment.setArguments(bundler);

				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		}
		
		else if(message.equals("Payment")){
			
			if (tempFragment instanceof FragmentPaypalExpressCheckout) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentPaypalExpressCheckout();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("DealFeedDetail", dealFeedDetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
			
		}

		else if(message.equals("StripePayment")){

			if (tempFragment instanceof FragmentStripePayment) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentStripePayment();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("DealFeedDetail", dealFeedDetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		}

		else if (message.equals("DealShare")) {

			if (tempFragment instanceof FragmentDealShare) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentDealShare();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("DealFeedDetail", dealFeedDetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		} else if (message.equals("dealBuy")) {
			if (tempFragment instanceof FragmentDiscussDeal) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentDiscussDeal();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("DealFeedDetail", dealFeedDetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		} else if (message.equals("DealDetail")) {
			if (tempFragment instanceof FragmentDealDetail) {

			} else {
				
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentDealDetail();
				Bundle bundle = new Bundle();
				bundle.putSerializable("DealFeedDetail", dealFeedDetail);
				newFragment.setArguments(bundle);

				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("DealPurchase")) {

			if (tempFragment instanceof FragmentPurchase) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentPurchase();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("DealFeedDetail", dealFeedDetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		} else if (message.equals("DealCreditcard")) {
			if (tempFragment instanceof FragmentCreditcard) {

			} else {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentCreditcard();
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("FavouriteDealDetail")) {
			if (tempFragment instanceof FragmentDealDetail) {

			} else {
				
				
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentDealDetail();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("DealFeedDetail", dealFeedDetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("writereview")) {
			if (tempFragment instanceof FragmentWriteReviewDeel) {

			} else {

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentWriteReviewDeel();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("DealFeedDetail", dealFeedDetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		}
	}

	@Override
	public void favouriteDealClick(String message) {
		Fragment tempfragmnet = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);
		if (!(tempfragmnet instanceof FragmentSLiderFavouriteListDeelCategory)) {

			Bundle bundle = new Bundle();
			bundle.putString("message", message);
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentSLiderFavouriteListDeelCategory();
			newFragment.setArguments(bundle);
			strFragmentTag = newFragment.toString();
			transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.addToBackStack(strFragmentTag);
			transaction.commit();
		}
	}

	@Override
	public void locationDetail(LocationList locationlist, String from) {

		Fragment tempfragmnet = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		if (from.equals("map")) {
			if (!(tempfragmnet instanceof FragmentSliderLocationDetail)) {

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderLocationDetail();
				Bundle bundle = new Bundle();
				bundle.putSerializable("LocationDetail", locationlist);
				bundle.putString("from", from);
				newFragment.setArguments(bundle);
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		} else {

			if (!(tempfragmnet instanceof FragmentSliderLocationDetail)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderLocationDetail();
				Bundle bundle = new Bundle();
				bundle.putSerializable("LocationDetail", locationlist);
				bundle.putString("from", from);
				newFragment.setArguments(bundle);
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		}

	}

	@Override
	public void categoryClick(String categoryId, String categoryName) {
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();

		Fragment tempfragmnet = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);
		if (!(tempfragmnet instanceof FragmentSliderCategoryDealFeed)) {

			Fragment newFragment = new FragmentSliderCategoryDealFeed();
			Bundle bundle = new Bundle();
			bundle.putString("categoryId", categoryId);
			bundle.putString("categoryName", categoryName);
			newFragment.setArguments(bundle);
			strFragmentTag = newFragment.toString();
			transaction.addToBackStack(strFragmentTag);
			transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.commit();
		}
	}

	@Override
	public void loncationClick(String message, LocationList locationlist) {

		Fragment tempfragmnet = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);
		if (message.equals("showDeals_DealDetails")) {

			if (!(tempfragmnet instanceof FragmentSliderLocationDealFeed)) {

				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentLocationDealFeed();
				Bundle bundle = new Bundle();
				bundle.putString("locationId", locationlist.iMerchantStoreId);
				bundle.putString("locationName",
						locationlist.vMerchantStoreName);
				newFragment.setArguments(bundle);
				strFragmentTag = newFragment.toString();

				getFragmentManager().popBackStack(null,
						FragmentManager.POP_BACK_STACK_INCLUSIVE);

				transaction.replace(R.id.frmDealFeed, newFragment,
						strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("showDeals")) {

			if (!(tempfragmnet instanceof FragmentSliderLocationDealFeed)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderLocationDealFeed();
				Bundle bundle = new Bundle();
				bundle.putString("locationId", locationlist.iMerchantStoreId);
				bundle.putString("locationName",
						locationlist.vMerchantStoreName);
				newFragment.setArguments(bundle);
				strFragmentTag = newFragment.toString();
				transaction.addToBackStack(strFragmentTag);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("writereview")) {

			if (!(tempfragmnet instanceof FragmentWriteReviewLocation)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentWriteReviewLocation();
				strFragmentTag = newFragment.toString();
				Bundle bundler = new Bundle();
				bundler.putSerializable("LocationDetail", locationlist);
				newFragment.setArguments(bundler);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("locationContact")) {

			if (!(tempfragmnet instanceof FragmentSLiderLocationContact)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSLiderLocationContact();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("locationlist", locationlist);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else if (message.equals("discuss")) {

			if (!(tempfragmnet instanceof FragmentSliderLocationDiscuss)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderLocationDiscuss();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("locationlist", locationlist);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		} else {
			if (!(tempfragmnet instanceof FragmentSliderLocationMap)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentSliderLocationMap();
				strFragmentTag = newFragment.toString();
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		}
	}

	@Override
	public void showDialog(String message) {

		// StartTimer();
		String msg = "";

		progressDialogFragment = new ProgressDialogFragment(getResources()
				.getString(R.string.loading));
		progressDialogFragment.show(getFragmentManager(), "");
		progressDialogFragment.setCancelable(false);
		progressbarshowing = true;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
				.findFragmentById(R.id.bottomFragment);

		SharedPreferences badgepreference = DeelFeedActivity.this
				.getSharedPreferences("Badgecount", Context.MODE_PRIVATE);

		int badgecountval = badgepreference.getInt("Badge", 0);

		bottomFragment.SetBadgeCount(badgecountval);

	}

	@Override
	public void hideDialog() {

		try {
			if (progressDialogFragment != null) {
				progressDialogFragment.dismiss();
				progressbarshowing = false;
				try {
					timerThread.stop();
				} catch (Exception e) {

				}
			}

		} catch (Exception e) {
		}

	}

	@Override
	public void sendDatatoFragment(WishListDetail wishlist) {

		Fragment tempfragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);
		if (!(tempfragment instanceof FragmentSliderAddWishlist)) {
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentSliderAddWishlist();
			strFragmentTag = newFragment.toString();
			Bundle bundle = new Bundle();
			bundle.putSerializable("UpdateWishlist", wishlist);
			newFragment.setArguments(bundle);
			transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.addToBackStack(strFragmentTag);
			transaction.commit();
		}

	}

	public void sendRequestToRepresentative(final String dealimagepath) {

		// showDialog("Connecting...");

		dealimagepathvalue = "";
		dealimagepathvalue = dealimagepath;
		showDialog_forconnecting("Connecting...");
		timerRunning = true;
		commonClass.acceptrequest = 0;

		if (timer != null) {
			timer.cancel();
		}
		counter = 0;

		// re-schedule timer here
		// otherwise, IllegalStateException of
		// "TimerTask is scheduled already"
		// will be thrown
		timer = new Timer();
		myTimerTask = new MyTimerTask(dealimagepath);

		timer.schedule(myTimerTask, 1000, 1000);

		// if (countdowntimer != null) {
		// countdowntimer.cancel();
		// }
		//
		// countdowntimer = new CountDownTimer(20000, 2000) {
		//
		// @Override
		// public void onTick(long millisUntilFinished) {
		// Log.i("onTick", "onTick");
		// if (commonClass.acceptrequest) {
		// commonClass.acceptrequest = false;
		// timerRunning = false;
		// // hideDialog();
		// hideDialog_forconnecting();
		//
		// if (countdowntimer != null) {
		// countdowntimer.cancel();
		// countdowntimer = null;
		// }
		//
		// dicussDetailChatClick(commonClass.representativeid,
		// dealimagepath);
		// }
		// }
		//
		// @Override
		// public void onFinish() {
		// Log.i("finish", "called" + timerRunning);
		// // Toast.makeText(getApplicationContext(), "on finish", 9000)
		// // .show();
		// if (timerRunning) {
		// Toast.makeText(
		// getApplicationContext(),
		// "kamothi"
		// + getResources().getString(
		// R.string.connectionsorrymsg), 15000)
		// .show();
		// Log.i("send reject>>", "send reject");
		// sendrejectmessage();
		// }
		// // if (!timerRunning) {
		// // Toast.makeText(
		// // getApplicationContext(),
		// //
		// "We are sorry. Right now we could not connect you to any merchant. Please try again.",
		// // 9000).show();
		// //
		// // }
		// // hideDialog();
		// hideDialog_forconnecting();
		// timerRunning = false;
		// }
		// }.start();
	}

	public void sendrejectmessage() {

		int rpstorelen = commonClass.arrRpListStore.size();
		for (int i = 0; i < rpstorelen; i++) {
			String rpmember = commonClass.arrRpListStore.get(i);
			long currenttimestamp = System.currentTimeMillis();
			String rpmemberid = rpmember + "@" + commonClass.strServerName;
			Message msg = new Message(rpmemberid, Message.Type.chat);
			msg.setBody("");
			msg.setProperty("support_team_id", "1");
			msg.setProperty("type_of_message", "reject_request_for_chat");
			msg.setProperty("msg_id", commonClass.strUsername);
			if (commonClass.xmppconnection != null
					&& commonClass.xmppconnection.isConnected()
					&& commonClass.checkInternetConnection()) {
				commonClass.xmppconnection.sendPacket(msg);
			}
		}
	}
	
	
	
	
	
	public void get_Merchantonline(final String merchant){
		 
		

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {

					@Override
					public void onPreExecute() {
						
					}

					@Override
					public void doInBackground() {
						String serverUrl = "http://"+commonClass.strServerName+":9090/plugins/presence/status?jid="+merchant+"&type=text";
						
						

						serverResponse= web_Service
								.GetOffline(serverUrl);
						Log.e("Url requestss", serverUrl+"==="+serverResponse);
						if(serverResponse.trim().equalsIgnoreCase("Unavailable")){
							
						}else{
							commonClass.connectCount+=1;
						}

					}

					@Override
					public void onPostExecute() {
						
					}

					
				});
		as.execute();
	
	}

	public void connectwithMerchant(String dealimagepath, String dealname) {

		int len = commonClass.arrRpListStore.size();
		
		commonClass.connectCount=0;
		
//		commonClass.connectCount=len;
		
		
		for(int j=0;j<len;j++){
			
			String representativeid = commonClass.arrRpListStore.get(j) + "@" + commonClass.strServerName;
			
			
			get_Merchantonline(representativeid);
	        
		}
		
		
		for (int i = 0; i < len; i++) {

			long currenttimestamp = System.currentTimeMillis();
			String number = commonClass.arrRpListStore.get(i);
			String representativeid = number + "@" + commonClass.strServerName;
			Message smackMessage = new Message(representativeid,
					Message.Type.chat);
			smackMessage.setBody("");
			smackMessage.setProperty("support_team_id", "1");
			smackMessage.setProperty("type_of_message", "request_for_chat");
			smackMessage.setProperty("msg_id", commonClass.strUsername);
			smackMessage.setProperty("Product_name", dealname);
			smackMessage.setProperty("Product_URL", dealimagepath);
			smackMessage.setProperty("Profile_URL",
					commonClass.ProfiileImagePath);
			if (commonClass.xmppconnection != null
					&& commonClass.xmppconnection.isConnected()
					&& commonClass.checkInternetConnection()) {
				commonClass.xmppconnection.sendPacket(smackMessage);
			}

		}
		if (commonClass.xmppconnection != null
				&& commonClass.xmppconnection.isConnected()
				&& commonClass.checkInternetConnection()) {
			sendRequestToRepresentative(dealimagepath);
		}

	}

	@Override
	public void discussOneToOneChatClick(ChatUser ch) {

		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		Fragment newFragment = new FragmentFriendsChat();
		Bundle bundle = new Bundle();
		bundle.putSerializable("chat", ch);
		newFragment.setArguments(bundle);
		strFragmentTag = newFragment.toString();
		transaction.add(R.id.frmDealFeed, newFragment, newFragment.getTag());
		transaction.addToBackStack(strFragmentTag);
		transaction.commit();
	}

	@Override
	public void update(Observable arg0, Object arg1) {

		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		Fragment addFragment = null;
		if (commonClass.getObserver().getValue().equals("updatedwishlist")) {
			addFragment = new FragmentSliderWishlist();

		} else if (commonClass.getObserver().getValue()
				.equals("updatenotificationsettings")) {
			addFragment = new FragmentNotificationList();
		} else if (commonClass.getObserver().getValue().equals("Rating")) {

			addFragment = new FragmentDealDetail();
		} else {

			addFragment = new FragmentChatList();
		}
		strFragmentTag = addFragment.toString();
		transaction.replace(R.id.frmDealFeed, addFragment, strFragmentTag);
		transaction.commit();
	}

	@Override
	public void selecteddateiswrongedialog(String message) {

		DialogFragment ds = new SingleButtonAlert(new DialogInterfaceClick() {

			@Override
			public void dialogClick(String tag) {

			}
		}, message);

		ds.show(getFragmentManager(), "");
	}

	@Override
	public void unbindservice() {

		// try {
		// Log.d("unbind service>>", "unbind service");
		// if (mConnection != null) {
		// getApplicationContext().unbindService(mConnection);
		// }
		//
		// } catch (Exception e) {
		// // TODO: handle exception
		// Log.e("called deelfeed", e.getMessage());
		// }

	}

	@Override
	public void onsharedealclicked(String message, DealFeedDetail dealdetail) {

		Fragment tempfragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		if (message.equals("mail")) {

			if (!(tempfragment instanceof FragmentShareViaMail)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentShareViaMail();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putString("sharevia", "deal");
				bundle.putSerializable("DealFeedDetail", dealdetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		}

		else if (message.equals("mimcup")) {

			if (!(tempfragment instanceof FragmentDealShare)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentDealShare();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();

				bundle.putSerializable("DealFeedDetail", dealdetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		}

	}

	private void check_From_Where_It_Come() {

		Fragment tempfragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);
		if (!(tempfragment instanceof FragmentNotificationList)) {
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment fragment = getFragmentManager().findFragmentById(
					R.id.frmDealFeed);

			getFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
			Fragment newFragment = new FragmentNotificationList();
			strFragmentTag = newFragment.toString();
			transaction.replace(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.commit();

		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub

		super.onDestroy();
		try {

			Fragment currentFragment = getFragmentManager().findFragmentById(
					R.id.frmDealFeed);

			if (currentFragment instanceof ChatDetailActivity) {
				Log.e("Onnew Intent Type====", type);
				((ChatDetailActivity) currentFragment).rejectAsynctask();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

		// setchatconnection();
		if (!isApplicationBroughtToBackground()) {
			commonClass.setNull();
		}
	}

	@Override
	public void onsharelocationclick(String message, LocationList locationdetail) {
		// TODO Auto-generated method stub

		Fragment tempfragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		if (message.equals("mail")) {

			if (!(tempfragment instanceof FragmentShareViaMail)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentShareViaMail();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putString("sharevia", "location");
				bundle.putSerializable("LocationDetail", locationdetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}

		} else if (message.equals("mimcup")) {

			if (!(tempfragment instanceof FragmentLocationShare)) {
				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragment newFragment = new FragmentLocationShare();
				strFragmentTag = newFragment.toString();
				Bundle bundle = new Bundle();
				bundle.putSerializable("LocationDetail", locationdetail);
				newFragment.setArguments(bundle);
				transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
				transaction.addToBackStack(strFragmentTag);
				transaction.commit();
			}
		}

	}

	private void changeLanguage(String languageName) {
		Locale myLocale = new Locale(languageName);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = myLocale;
		res.updateConfiguration(conf, dm);
		// Intent intent = new Intent(getActivity(), RegisterActivity.class);
		// startActivity(intent);
		// getActivity().finish();
	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		try {
			locationrequest = LocationRequest.create();
			locationrequest.setInterval(20000);
			locationclient.requestLocationUpdates(locationrequest,
					mPendingIntent);

			Location loc = locationclient.getLastLocation();
			if (loc != null) {

				commonClass.currentLatitude = loc.getLatitude();
				commonClass.currentLongitude = loc.getLongitude();

			} else {
			}

		} catch (Exception e) {
			// TODO: handle exception
			Log.d("fused location service", "get exception"
					+ e.getMessage().toString());
		}

	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
	}

	@Override
	public void go_to_map_screen_withlist(ArrayList<LocationList> location_list) {

		Fragment tempfragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		if (!(tempfragment instanceof FragmentSliderLocationMap)) {
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();

			Fragment newFragment = new FragmentSliderLocationMap();
			strFragmentTag = newFragment.toString();

			Bundle bundler = new Bundle();
			bundler.putSerializable("location_map", location_list);
			bundler.putBoolean("key", true);
			newFragment.setArguments(bundler);

			transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.addToBackStack(strFragmentTag);
			transaction.commit();
		}

	}

	@Override
	public void go_to_map_screen_single_icon(LocationList location_details) {

		Fragment tempfragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);

		if (!(tempfragment instanceof FragmentSliderLocationMap)) {

			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();

			Fragment newFragment = new FragmentSliderLocationMap();
			strFragmentTag = newFragment.toString();

			Bundle bundler = new Bundle();
			bundler.putSerializable("location_map", location_details);
			bundler.putBoolean("key", false);
			newFragment.setArguments(bundler);

			transaction.add(R.id.frmDealFeed, newFragment, strFragmentTag);
			transaction.addToBackStack(strFragmentTag);
			transaction.commit();
		}

	}

	@Override
	public void onSoftKeyboardShown(boolean isShowing) {

		try {

			if (isShowing) {
				iskeyBoardShowing = true;

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();
				Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
						.findFragmentById(R.id.bottomFragment);
				transaction.hide(bottomFragment);
				transaction.commit();

			} else {
				if (iskeyBoardShowing) {
					iskeyBoardShowing = false;
					FragmentTransaction transaction = getFragmentManager()
							.beginTransaction();
					Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
							.findFragmentById(R.id.bottomFragment);
					// Toast.makeText(getApplicationContext(),
					// "keyboard is visible",
					// Toast.LENGTH_LONG).show();
					transaction.show(bottomFragment);
					transaction.commit();
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public void Starttimerexample() {

	}

	private void StartTimer() {
		timerThread = new Thread(new Runnable() {

			//
			@Override
			public void run() {
				try {
					Thread.sleep(20 * 1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					if (progressbarshowing) {

						progressDialogFragment.dismiss();
						progressbarshowing = false;
						DialogFragment ds = new SingleButtonAlert(
								new DialogInterfaceClick() {

									@Override
									public void dialogClick(String tag) {

									}
								}, "Timer Error");
						ds.show(getFragmentManager(), "");

					}
				}
			}
		});

		timerThread.start();

	}

	private void showDialog_forconnecting(String string) {
		dialogforconnect = new MurchantConnectDialog(
				new DialogInterfaceClick() {

					@Override
					public void dialogClick(String tag) {
						// TODO Auto-generated method stub

						// if (countdowntimer != null) {
						// countdowntimer.cancel();
						// }
						timerRunning = false;

						if (timer != null) {
							timer.cancel();
							timer = null;
						}

						// FragmentTransaction transaction =
						// getFragmentManager()
						// .beginTransaction();
						// Fragment fragment = getFragmentManager()
						// .findFragmentById(R.id.frmDealFeed);
						// ((ChatDetailActivity) fragment).rejectAsynctask();
						// FragmentTransaction transaction =
						// getFragmentManager()
						// .beginTransaction();
						// Fragment fragment =
						// getFragmentManager().findFragmentById(
						// R.id.frmDealFeed);
						//
						// ((ChatDetailActivity) fragment).rejectAsynctask();
						// timerRunning = false;

						sendrejectmessage();

					}
				});
		dialogforconnect.show(getFragmentManager(), "");
		dialogforconnect.setCancelable(false);
	}

	private void hideDialog_forconnecting() {

		try {

			if (dialogforconnect != null) {
				dialogforconnect.dismiss();
			}

		} catch (Exception e) {
		}

	}

	@Override
	public void ondateselectedevent(DateCustomeClass dateclass) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setchatconnection() {
		// TODO Auto-generated method stub
		try {
			commonClass.xmppconnection.disconnect();
			getApplicationContext().unbindService(mConnection);
			getApplicationContext().bindService(
					new Intent(getApplicationContext(), Connector.class),
					mConnection, Context.BIND_AUTO_CREATE);
		} catch (Exception e) {
			Log.d("exception>>", "exception e");

		}
	}

	private boolean isApplicationBroughtToBackground() {
		boolean isback = false;
		ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (topActivity.getClassName().toString()
					.equalsIgnoreCase("com.mimcoups.Consumer.DeelFeedActivity")) {
				isback = true;
			} else {
				isback = false;
			}
		}
		return isback;
	}

	public void slidebarchatclosed() {

		Fragment fragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);
		if (fragment instanceof ChatDetailActivity) {
			// getFragmentManager().popBackStack(null,
			// FragmentManager.POP_BACK_STACK_INCLUSIVE);
			// ((ChatDetailActivity) fragment).rejectAsynctask();
			timerRunning = false;
		}
	}

	class MyTimerTask extends TimerTask {

		String dealimagepath;

		public MyTimerTask(String dealimagepath) {
			this.dealimagepath = dealimagepath;
		}

		@Override
		public void run() {

			if (commonClass.acceptrequest == 1) {

			}

			if (++counter == 20) {

				if (timerRunning) {

					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							// textCounter.setText(strDate);
							Toast.makeText(
									getApplicationContext(),
									getResources().getString(
											R.string.connectionsorrymsg), Toast.LENGTH_LONG)
									.show();
						}
					});
					sendrejectmessage();
				}

				hideDialog_forconnecting();
				timerRunning = false;

				if (timer != null) {
					timer.cancel();
					timer = null;
				}
			}

			// runOnUiThread(new Runnable() {
			//
			// @Override
			// public void run() {
			// // textCounter.setText(strDate);
			// for (int i = 0; i < 5000; i++) {
			// Log.i("counter", "" + i);
			// dem.setText("" + i);
			// }
			// }
			// });

		}

	}

	// location service to get lat long

	public ArrayList<Double> getLatitudeLongitude() {
		ArrayList<Double> arrLatitude = new ArrayList<Double>();

		try {

			locationclient = new LocationClient(getApplicationContext(), this,
					this);
			locationclient.connect();
			if (locationclient != null && locationclient.isConnected()) {
				Location loc = locationclient.getLastLocation();

				Log.e("caleld getlatlong", "1" + commonClass.currentLatitude
						+ "==" + commonClass.currentLongitude);
				if (loc != null) {
					commonClass.currentLatitude = loc.getLatitude();
					commonClass.currentLongitude = loc.getLongitude();

					Log.e("caleld getlatlong", "2"
							+ commonClass.currentLatitude + "=="
							+ commonClass.currentLongitude);
					arrLatitude.add(loc.getLatitude());
					arrLatitude.add(loc.getLongitude());
				} else {

					Log.e("caleld getlatlong", "3"
							+ commonClass.currentLatitude + "=="
							+ commonClass.currentLongitude);
					arrLatitude.add(commonClass.currentLatitude);
					arrLatitude.add(commonClass.currentLongitude);
				}
			} else {
				Log.e("caleld getlatlong", "4" + commonClass.currentLatitude
						+ "==" + commonClass.currentLongitude);
				arrLatitude.add(commonClass.currentLatitude);
				arrLatitude.add(commonClass.currentLongitude);
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.e("Exception ===", e.getMessage().toString());
		}

		return arrLatitude;
	}

	public boolean checkLocationService() {
		LocationManager lm = null;
		boolean gps_enabled = false, network_enabled = false;
		if (lm == null)
			lm = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);
		try {
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}

		if (!gps_enabled && !network_enabled) {
			return false;

		}
		return true;
	}

	public void initopenfireconnection() {

		if (commonClass.xmppconnection != null
				&& commonClass.xmppconnection.isConnected()
				&& commonClass.checkInternetConnection()) {

		} else {

			try {
				getApplicationContext().bindService(
						new Intent(getApplicationContext(), Connector.class),
						mConnection, Context.BIND_AUTO_CREATE);
			} catch (Exception e) {

			}

		}
	}

	public void NotificationNavigation(String notificationType) {

		Fragment tempFragment = getFragmentManager().findFragmentById(
				R.id.frmDealFeed);
		Bundle bundle = new Bundle();
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		Fragment newFragment = null;
		if (notificationType.equalsIgnoreCase("favloc")
				|| notificationType.equalsIgnoreCase("onsite")
				|| notificationType.equalsIgnoreCase("nearby")) {

			bundle = new Bundle();
			bundle.putString("isLocation", "true");
			bundle.putString("notificationtype", notificationType);

			newFragment = new FragmentNotificationDetail();

		} else if (notificationType.equalsIgnoreCase("FriendRequest")) {

			newFragment = new FragmentSliderFriendNotificationRequest();

			// transaction.commit();

		} else if (notificationType.equalsIgnoreCase("chat")) {

			newFragment = new FragmentChatList();

			// transaction.commit();

		} else {

			bundle.putString("isLocation", "false");
			if (notificationType.equalsIgnoreCase("Cateogry")) {
				bundle.putString("notificationtype", "Cateogry");
			}

			if (notificationType.equalsIgnoreCase("Deal")) {
				bundle.putString("notificationtype", "Deal");
			}
			if (notificationType.equalsIgnoreCase("Loyality")) {
				bundle.putString("notificationtype", "Loyality");
			}
			if (notificationType.equalsIgnoreCase("wishlist")) {
				bundle.putString("notificationtype", "wishlist");
			}
			// if (notificationType.equalsIgnoreCase("FriendRequest")) {
			// bundle.putString("notificationtype", "FriendRequest");
			// }
			if (notificationType.equalsIgnoreCase("Mimcoups")) {
				bundle.putString("notificationtype", "Mimcoups");
			}

			newFragment = new FragmentNotificationDetail();

		}
		// if (tempFragment instanceof FragmentNotificationList) {

		newFragment.setArguments(bundle);
		strFragmentTag = newFragment.toString();
		transaction.replace(R.id.frmDealFeed, newFragment, strFragmentTag);
		transaction.commit();

		// }

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);

		Log.e("on new intent", "on new intent");

		notification = intent.getExtras().getBoolean("notification");
		type = intent.getExtras().getString("type");

		notificationmsg=intent.getExtras().getString("msg");

		Log.e("OnNewIntent called=====", "" + notification + "====" + type);

		Fragment newFragment = null;
		if (notification) {

			// try {
			// commonClass = (CommonData) getApplicationContext();
			// if (commonClass.xmppconnection.isConnected()
			// && commonClass.checkInternetConnection()) {
			// commonClass.xmppconnection.disconnect();
			// }
			// if (mConnection != null) {
			// getApplicationContext().unbindService(mConnection);
			// }
			//
			// } catch (Exception e) {
			// // TODO: handle exception
			// Log.e("Notification get Exception",
			// "Notification Exception");
			//
			// }

			// initopenfireconnection();

			Fragmentbottom bottomFragment = (Fragmentbottom) getFragmentManager()
					.findFragmentById(R.id.bottomFragment);
			bottomFragment.selectNotification();

			Log.d("type is>>", "" + type);


			if(type.trim().equalsIgnoreCase("newappversion")){
				DialogFragment ds = new UpdateapplicationDialog(new DialogInterfaceClick() {

					@Override
					public void dialogClick(String tag) {
						if(tag.equalsIgnoreCase("update")){

							SharedPreferences.Editor  editor = getSharedPreferences("updateapp", MODE_PRIVATE).edit();
							editor.putLong("updateapptime",0);
							editor.commit();

							final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
							try {
								startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
							} catch (android.content.ActivityNotFoundException anfe) {
								startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
							}

						}else{

							long current=System.currentTimeMillis();

							SharedPreferences.Editor  editor = getSharedPreferences("updateapp", MODE_PRIVATE).edit();
							editor.putLong("updateapptime",current);
							editor.putString("updatemsg",notificationmsg);
							editor.commit();



						}

					}
				}, notificationmsg);

				ds.setCancelable(false);
				ds.show(getFragmentManager(), "");

			}
			else if (type.equalsIgnoreCase("favcatloc")
					|| type.equalsIgnoreCase("favcatdeal")) {
				NotificationNavigation("Cateogry");

				Fragment currentFragment = getFragmentManager()
						.findFragmentById(R.id.frmDealFeed);

				if (currentFragment instanceof ChatDetailActivity) {
					Log.e("Onnew Intent Type====", type);
					((ChatDetailActivity) currentFragment).rejectAsynctask();
				}
				// notificationType("Cateogry");

			} else if (type.equalsIgnoreCase("favloc")
					|| type.equalsIgnoreCase("onsite")
					|| type.equalsIgnoreCase("nearby")) {
				// notificationType("Location");
				NotificationNavigation(type);

				Fragment currentFragment = getFragmentManager()
						.findFragmentById(R.id.frmDealFeed);

				if (currentFragment instanceof ChatDetailActivity) {
					Log.e("Onnew Intent Type====", type);
					((ChatDetailActivity) currentFragment).rejectAsynctask();
				}

			} else if (type.equalsIgnoreCase("favlocdeal")
					|| type.equalsIgnoreCase("favdeal")) {
				// notificationType("Deal");

				NotificationNavigation("Deal");

				Fragment currentFragment = getFragmentManager()
						.findFragmentById(R.id.frmDealFeed);

				if (currentFragment instanceof ChatDetailActivity) {
					Log.e("Onnew Intent Type====", type);
					((ChatDetailActivity) currentFragment).rejectAsynctask();
				}
			}

			else if (type.equalsIgnoreCase("loyalty")) {
				// notificationType("Loyality");
				NotificationNavigation("Loyality");

				Fragment currentFragment = getFragmentManager()
						.findFragmentById(R.id.frmDealFeed);

				if (currentFragment instanceof ChatDetailActivity) {
					Log.e("Onnew Intent Type====", type);
					((ChatDetailActivity) currentFragment).rejectAsynctask();
				}
			}

			else if (type.equalsIgnoreCase("wishlist")) {
				// notificationType("wishlist");
				NotificationNavigation("wishlist");

				Fragment currentFragment = getFragmentManager()
						.findFragmentById(R.id.frmDealFeed);

				if (currentFragment instanceof ChatDetailActivity) {
					Log.e("Onnew Intent Type====", type);
					((ChatDetailActivity) currentFragment).rejectAsynctask();
				}

			}

			else if (type.equalsIgnoreCase("friend")) {
				// notificationType("FriendRequest");
				NotificationNavigation("FriendRequest");

				Fragment currentFragment = getFragmentManager()
						.findFragmentById(R.id.frmDealFeed);

				if (currentFragment instanceof ChatDetailActivity) {
					Log.e("Onnew Intent Type====", type);
					((ChatDetailActivity) currentFragment).rejectAsynctask();
				}

			}

			else if (type.equalsIgnoreCase("mims")) {
				Log.d("type is mims", "mims");
				// notificationType("Mimcoups");
				NotificationNavigation("Mimcoups");

				Fragment currentFragment = getFragmentManager()
						.findFragmentById(R.id.frmDealFeed);

				if (currentFragment instanceof ChatDetailActivity) {
					Log.e("Onnew Intent Type====", type);
					((ChatDetailActivity) currentFragment).rejectAsynctask();
				}
			} else if (type.equalsIgnoreCase("chat")) {

				Log.e("chat reject", "chat reject");

				NotificationNavigation("chat");
				Fragment currentFragment = getFragmentManager()
						.findFragmentById(R.id.frmDealFeed);

				if (currentFragment instanceof ChatDetailActivity) {

					Log.e("Onnew Intent Type====", type);
					((ChatDetailActivity) currentFragment).rejectAsynctask();
				}
				// timerRunning = false;

				bottomFragment.selectDiscussNotification();

			}

			// if (type.equalsIgnoreCase("mim")) {
			// Log.d("type is mims", "mims");
			// notificationType("Mimcoups");
			//
			// }

		}

	}

}
