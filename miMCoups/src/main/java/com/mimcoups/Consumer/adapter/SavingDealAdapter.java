package com.mimcoups.Consumer.adapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.Facebook;
import com.google.android.gms.plus.PlusShare;
import com.mimcoups.Consumer.FacebookShare;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.fragment.FragmentSliderSaving;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.shareingdeal;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.squareup.picasso.Picasso;

public class SavingDealAdapter extends BaseAdapter {

	ArrayList<DealFeedDetail> im;
	String Data;
	LayoutInflater inflate1;
	topBarInteface topbarinterface;
	Dialog ImageDialog;
	Context context;
	ArrayList<DealFeedDetail> arrdealdetail;
	Button mimcoupsharing, btnemailsharing, facebookcontainer,
			twittercontainer, googlepluscontainer, instagamcontainer;

	shareingdeal sharedeal;
	private CommonData globalClass;
	private FragmentSliderSaving saving;
	String share = "";

	private Facebook mFaceBook;
	private static final String[] PERMISSIONS = new String[] {
			"publish_stream", "read_stream", "offline_access" };

	public SavingDealAdapter(Activity context, ArrayList<DealFeedDetail> im,
			FragmentSliderSaving saving) {
		this.im = im;
		this.context = context;
		this.arrdealdetail = im;
		this.saving = saving;
		sharedeal = (shareingdeal) context;
		globalClass = (CommonData) context.getApplicationContext();
		mFaceBook = new Facebook(globalClass.FACEBOOK_APPID);
		topbarinterface = (topBarInteface) context;

	}

	@Override
	public String getItem(int position) {
		return im.get(position).toString();
	}

	public static class ViewHolder {
		// public ImageView imgDealImage, imgsavingshare;
		// public TextView txtSavingTotalPrice, txtSavingDiscountPrice,
		// txtSavingDealDistance, txtDealName, txtDealTypeDiscription,
		// txtDealDetail, txtSavingAmount;

		ImageView imgDealImage, imgsavingshare;
		TextView txtSavingTotalPrice, txtSavingDiscountPrice, txtstorename,
				txtdealname, dealviewdescriptionheader, txtDealDetail,
				txtSavingAmount, txtOrderId;

	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final DealFeedDetail dealdetail = arrdealdetail.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_saving, null);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.imgDealImage = (ImageView) convertView
				.findViewById(R.id.imgDealImage);
		holder.txtOrderId = (TextView) convertView
				.findViewById(R.id.txtOrderId);

		holder.txtSavingTotalPrice = (TextView) convertView
				.findViewById(R.id.txtSavingTotalPrice);
		holder.txtSavingDiscountPrice = (TextView) convertView
				.findViewById(R.id.txtSavingDiscountPrice);
		holder.txtstorename = (TextView) convertView
				.findViewById(R.id.txtstorename);
		holder.txtdealname = (TextView) convertView
				.findViewById(R.id.txtdealname);
		holder.dealviewdescriptionheader = (TextView) convertView
				.findViewById(R.id.dealviewdescriptionheader);
		holder.txtDealDetail = (TextView) convertView
				.findViewById(R.id.txtDealDetail);
		holder.txtSavingAmount = (TextView) convertView
				.findViewById(R.id.txtSavingAmount);
		holder.imgsavingshare = (ImageView) convertView
				.findViewById(R.id.imgsavingshare);

		convertView.setTag(holder);

		holder.txtSavingTotalPrice.setText(dealdetail.vPurchaseamount);
		holder.txtSavingTotalPrice.setPaintFlags(holder.txtSavingTotalPrice
				.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

		holder.txtSavingDiscountPrice.setText(dealdetail.vOrderamount);

		holder.txtstorename.setText(dealdetail.StoreName);
		holder.txtdealname.setText(dealdetail.dealName);
		holder.dealviewdescriptionheader
				.setText(dealdetail.shortDescriptionHeading);
		holder.txtOrderId.setText(dealdetail.vOrderNumber);

		dealdetail.shortDescription = dealdetail.shortDescription.replace(
				"\\n", "\n");

		dealdetail.shortDescription = dealdetail.shortDescription.replace(">",
				"●");
		holder.txtDealDetail.setText(dealdetail.shortDescription);

		holder.txtSavingAmount.setText(dealdetail.vSavingamount);

		Picasso.with(context).load(dealdetail.vDealListImage_A_350X350)
				.placeholder(R.drawable.noimage_deal).into(holder.imgDealImage);

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// if
				// (dealdetail.vCustomerOrderStatus.equalsIgnoreCase("Pending"))
				// {
				//
				// // TODO Auto-generated method stub
				// DialogFragment ds = new PurchaseProofCodeFragment(
				// new DialogInterfaceClick() {
				// @Override
				// public void dialogClick(
				// final String purchasecode) {
				//
				// async_deel_feed_list as = new async_deel_feed_list(
				// new asynctaskloaddata() {
				//
				// String serverResponse,
				// statusCode,
				// statusMessage;
				// private WebService_Call web_service = new WebService_Call();
				//
				// @Override
				// public void onPreExecute() {
				//
				// topbarinterface
				// .showDialog("Loading");
				// }
				//
				// @Override
				// public void doInBackground() {
				// String serverUrl = "/OrderService.svc/UpdateToDoneDeal?"
				// + "customerid="
				// + globalClass.CustomerId
				// + "&languageid="
				// + globalClass.languageId
				// + "&orderid="
				// + dealdetail.iOrderId
				// + "&proofofpurchasecode="
				// + purchasecode;
				//
				// // Log.e("Url saving called",
				// // serverUrl);
				//
				// serverResponse = web_service
				// .makeServicegetRequest(serverUrl);
				// Log.e("called response",
				// serverResponse);
				//
				// try {
				// JSONObject js_getres = new JSONObject(
				// serverResponse);
				// statusCode = js_getres
				// .getString("vStatusCode");
				// statusMessage = js_getres
				// .getString("vMessageResponse");
				// if (statusCode
				// .equals("100")
				// || statusCode
				// .equals("102")) {
				//
				// statusMessage = js_getres
				// .getString("vMessageResponse");
				//
				// } else {
				// statusMessage = js_getres
				// .getString("vMessageResponse");
				// }
				//
				// } catch (Exception e) {
				// e.printStackTrace();
				// }
				//
				// }
				//
				// @Override
				// public void onPostExecute() {
				// topbarinterface
				// .hideDialog();
				// if (statusCode
				// .equals("420")) {
				// DialogFragment ds = new SingleButtonAlert(
				// new DialogInterfaceClick() {
				//
				// @Override
				// public void dialogClick(
				// String tag) {
				// topbarinterface
				// .topbarClick("back");
				// }
				// },
				// context.getResources()
				// .getString(
				// R.string.PoorInternetConnect));
				// ds.show(((Activity) context)
				// .getFragmentManager(),
				// "");
				// ds.setCancelable(false);
				//
				// } else if (statusCode
				// .equals("100")) {
				//
				// // Log.d("total saving amount>>>",
				// // ""
				// // + saving.dTotalSavingAmount);
				// // Log.d("saving amount>>>",
				// // ""
				// // + arrdealdetail
				// // .get(position).dSavingamount);
				// double saving_total = (saving.dTotalSavingAmount)
				// - (arrdealdetail
				// .get(position).dSavingamount);
				//
				// Log.e("Total Saving===",
				// ""
				// + saving.dTotalSavingAmount);
				// Log.e("Saving===",
				// ""
				// + arrdealdetail
				// .get(position).dSavingamount);
				// Log.e("Total Saving Reduce===",
				// ""
				// + saving_total);
				//
				// dealdetail.vCustomerOrderStatus = "Received";
				//
				// // arrdealdetail
				// // .remove(position);
				//
				// notifyDataSetChanged();
				//
				// // DecimalFormat df =
				// // new
				// // DecimalFormat(
				// // "#.##");
				// // saving_total =
				// // Double.valueOf(df
				// // .format(saving_total));
				//
				// String str = String
				// .format("%.2f",
				// saving_total);
				// Log.d("after decimal >>",
				// ""
				// + saving_total);
				//
				// // saving.set_savingdata(saving_total);
				//
				// DialogFragment ds = new SingleButtonAlert(
				// new DialogInterfaceClick() {
				//
				// @Override
				// public void dialogClick(
				// String tag) {
				//
				// }
				// },
				// statusMessage);
				// ds.show(((Activity) context)
				// .getFragmentManager(),
				// "");
				// ds.setCancelable(false);
				//
				// } else if (statusCode
				// .equals("102")) {
				// DialogFragment ds = new SingleButtonAlert(
				// new DialogInterfaceClick() {
				//
				// @Override
				// public void dialogClick(
				// String tag) {
				//
				// }
				// },
				// statusMessage);
				// ds.show(((Activity) context)
				// .getFragmentManager(),
				// "");
				// ds.setCancelable(false);
				// }
				// }
				//
				// });
				// as.execute();
				//
				// }
				// });
				// ds.show(((Activity) context).getFragmentManager(), "");
				//
				// }
			}
		});

		holder.imgsavingshare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (dealdetail.bDealStatus) {

					DialogFragment ds = new SingleButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {

								}
							}, context.getResources().getString(
									R.string.SavingDealExpired));
					ds.show(((Activity) context).getFragmentManager(), "");
					ds.setCancelable(false);
				} else {
					Sharing_Dialog(dealdetail);
					ImageDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_PopUpMenu_Center;
					ImageDialog.show();

					mimcoupsharing.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							sharedeal.onsharedealclicked("mimcup", dealdetail);

							ImageDialog.dismiss();

						}
					});

					btnemailsharing.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							// Log.e("saving==", dealdetail.locationId);

							sharedeal.onsharedealclicked("mail", dealdetail);

							ImageDialog.dismiss();

						}
					});
				}
			}
		});

		convertView.setTag(holder);
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return im.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private void Sharing_Dialog(final DealFeedDetail dealdetail) {

		try{
			ImageDialog = new Dialog(context);
			ImageDialog.setContentView(R.layout.share_white_vertical_dialog);
			ImageDialog.setCancelable(true);
			ImageDialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			int divierId = ImageDialog.getContext().getResources()
					.getIdentifier("android:id/titleDivider", null, null);
			View divider = ImageDialog.findViewById(divierId);
//			divider.setBackgroundColor(Color.TRANSPARENT);

			mimcoupsharing = (Button) ImageDialog
					.findViewById(R.id.btnmimcoupsharing);

			btnemailsharing = (Button) ImageDialog
					.findViewById(R.id.btnemailsharing);

			facebookcontainer = (Button) ImageDialog
					.findViewById(R.id.btnfacebookcontainer);
			twittercontainer = (Button) ImageDialog
					.findViewById(R.id.btntwittercontainer);
			googlepluscontainer = (Button) ImageDialog
					.findViewById(R.id.btngooglepluscontainer);
			instagamcontainer = (Button) ImageDialog
					.findViewById(R.id.btninstagramcontainer);

			Button btncancel = (Button) ImageDialog.findViewById(R.id.btnCancel);
			if (CommonData.languageId.equalsIgnoreCase("1")) {
				share = CommonData.dealshareUrl_au
						+ dealdetail.DealId;

			} else {
				share = CommonData.dealshareUrl_no
						+ dealdetail.DealId;
			}

			btncancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ImageDialog.dismiss();

				}
			});

			facebookcontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent ii = new Intent(context,
							FacebookShare.class);
					ii.putExtra("dealname", dealdetail.dealName + " With "
							+ dealdetail.vSavingamount);
					ii.putExtra("desc", dealdetail.shortDescription);
					ii.putExtra("Image", share);
					((Activity) context).startActivity(ii);
					ImageDialog.dismiss();
				}
			});

			twittercontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					try {

						Uri uri = Uri.fromFile(new File(dealdetail.DealImageUrl));
						// Uri uri = Uri.fromFile(new File(share));
						Intent tweetIntent = new Intent(Intent.ACTION_SEND);
						// tweetIntent.putExtra(Intent.EXTRA_TEXT,
						// dealdetail.dealName
						// + " With " + dealdetail.vSavingamount + "\n\n"
						// + dealdetail.vShortDescription);
						tweetIntent.putExtra(Intent.EXTRA_TEXT, dealdetail.dealName
								+ " With " + dealdetail.vSavingamount + "\n\n"
								+ share);
						tweetIntent.putExtra(Intent.EXTRA_STREAM, uri);
						tweetIntent.setType("*/*");

						PackageManager packManager = context.getPackageManager();
						List<ResolveInfo> resolvedInfoList = packManager
								.queryIntentActivities(tweetIntent,
										PackageManager.MATCH_DEFAULT_ONLY);

						boolean resolved = false;
						for (ResolveInfo resolveInfo : resolvedInfoList) {
							if (resolveInfo.activityInfo.packageName
									.startsWith("com.twitter.android")) {
								tweetIntent.setClassName(
										resolveInfo.activityInfo.packageName,
										resolveInfo.activityInfo.name);
								resolved = true;
								break;
							}
						}
						if (resolved) {
							context.startActivity(tweetIntent);
						} else {
							try {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("market://details?id=com.twitter.android")));
							} catch (android.content.ActivityNotFoundException anfe) {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android")));
							}
						}

						// Uri uri = Uri.parse(dealdetail.DealImageUrl);
						// // Uri uri =
						// //
						// Uri.parse("android.resource://com.gobaby.app/drawable/back");
						// Intent intent = new Intent(Intent.ACTION_SEND);
						// intent.setType("/*");
						// intent.setClassName("com.twitter.android",
						// "com.twitter.android.PostActivity");
						// intent.putExtra(Intent.EXTRA_TEXT, dealdetail.dealName
						// + " With " + dealdetail.vSavingamount + "\n\n "
						// + dealdetail.vShortDescription);
						// intent.putExtra(Intent.EXTRA_STREAM, uri);
						// context.startActivity(intent);

					} catch (Exception e) {

					}
					//

					// Intent tp2 = new Intent(context,
					// PrepareRequestTokenActivity.class);
					// tp2.putExtra("sharingfrom", 0);
					// tp2.putExtra("dealname", dealdetail.dealName + " With "
					// + dealdetail.vSavingamount);
					// tp2.putExtra("desc", dealdetail.vShortDescription);
					// tp2.putExtra("Image", dealdetail.DealImageUrl);
					//
					// ((Activity) context).startActivity(tp2);
					ImageDialog.dismiss();
				}
			});

			googlepluscontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Intent iv = new Intent(context, SignInActivity.class);
					// iv.putExtra("sharingfrom", 1);
					// context.startActivity(iv);

					// Intent shareIntent = new PlusShare.Builder(context)
					// .setType("text/plain")
					// .setText(
					// "I like to share Deel From MimCup , i Love THis Deel  ,Please You Also Try MimCup")
					// .setContentUrl(Uri.parse("http://www.mimcoup.com"))
					// .getIntent();
					// context.startActivity(shareIntent);

					Intent shareIntent = new PlusShare.Builder(context)
							.setType("text/plain")
							.setText(
									"MiMCoups APP:: \n " + dealdetail.dealName
											+ " With " + dealdetail.vSavingamount
											+ "\n " + dealdetail.vShortDescription)
							.setContentUrl(Uri.parse(share))
									// .setContentUrl(Uri.parse(dealdetail.DealImageUrl))
							.getIntent();
					((Activity) context).startActivityForResult(shareIntent, 0);

				}
			});

			instagamcontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Intent i = new Intent(context, InstaActivity.class);
					// i.putExtra("sharingfrom", 1);
					// context.startActivity(i);

					if (!appInstalledOrNot()) {
						Toast.makeText(
								context,
								"Please install Instagram Application from Play Store"
										+ "", Toast.LENGTH_LONG).show();
						return;
					} else {
						Intent shareIntent = new Intent(
								android.content.Intent.ACTION_SEND);
						shareIntent.setType("image/*"); // set mime type
						Uri path = Uri.parse("android.resource://com.mimcoups/"
								+ R.drawable.app_icon);
						shareIntent.putExtra(Intent.EXTRA_TEXT,"MiMCoups APP:: \n " + dealdetail.dealName

								+ "\n " + dealdetail.shortDescription+"\n"+share);
						shareIntent.putExtra(Intent.EXTRA_STREAM, path); // set uri
						shareIntent.setPackage("com.instagram.android");
						context.startActivity(shareIntent);
					}

				}
			});

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(ImageDialog.getWindow().getAttributes());
			DisplayMetrics displaymetrics = new DisplayMetrics();
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.BOTTOM;
			((Activity) context).getWindowManager().getDefaultDisplay()
					.getMetrics(displaymetrics);

			ImageDialog.getWindow().setAttributes(lp);


		}catch (Exception e){

		}

	}

	private boolean appInstalledOrNot() {

		boolean app_installed = false;
		try {
			ApplicationInfo info = context.getPackageManager()
					.getApplicationInfo("com.instagram.android", 0);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}
}
