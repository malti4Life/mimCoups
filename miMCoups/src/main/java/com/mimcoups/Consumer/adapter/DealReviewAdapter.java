package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.customClass.ReviewDetail;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.squareup.picasso.Picasso;

public class DealReviewAdapter extends BaseAdapter {

	ArrayList<ReviewDetail> arrReviewDetails;
	DealFeedDetailInterface dealInterface;
	String Data;
	LayoutInflater inflate1;
	private Activity activity;

	public DealReviewAdapter(Activity context,
			ArrayList<ReviewDetail> arrReviewDetails) {
		this.arrReviewDetails = arrReviewDetails;
		dealInterface = (DealFeedDetailInterface) context;
		activity = context;
	}

	@Override
	public String getItem(int position) {
		return arrReviewDetails.get(position).toString();
	}

	public static class ViewHolder {
		public TextView txtUserName, txtReviews;
		public RatingBar rtRatingCount;
		public ImageView imgUserPhoto;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		ReviewDetail review = arrReviewDetails.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_reviews, null);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txtUserName = (TextView) convertView
				.findViewById(R.id.txtUserName);
		holder.txtReviews = (TextView) convertView
				.findViewById(R.id.txtReviews);
		holder.rtRatingCount = (RatingBar) convertView
				.findViewById(R.id.rtRatingCount);
		holder.imgUserPhoto = (ImageView) convertView
				.findViewById(R.id.imgUserPhoto);

		convertView.setTag(R.id.txtUserName, holder.txtUserName);
		convertView.setTag(R.id.txtReviews, holder.txtReviews);
		convertView.setTag(R.id.txtReviews, holder.txtReviews);
		convertView.setTag(R.id.imgUserPhoto, holder.imgUserPhoto);

		convertView.setTag(holder);
		holder.rtRatingCount.setRating(Float.parseFloat(review.strRatingCount));
		holder.txtUserName.setText(review.strCustomerName);
		holder.txtReviews.setText(review.strReviewMessage);
		Picasso.with(activity).load(review.strProfileImageUrl)
				.placeholder(R.drawable.icon_perchasers_photo)
				.transform(new RoundedTransformation(15, 0))
				.into(holder.imgUserPhoto);
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrReviewDetails.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
