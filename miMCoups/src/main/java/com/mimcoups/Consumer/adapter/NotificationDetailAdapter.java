package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SelectionButtonAlert;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.customClass.NotificationDetail;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.LocationListInterface;
import com.mimcoups.Consumer.inteface.NotificaiondetailadapterToFragment;
import com.mimcoups.Consumer.inteface.topBarInteface;

public class NotificationDetailAdapter extends BaseAdapter {

	ArrayList<NotificationDetail> arrNotification;
	String Data;
	LayoutInflater inflate1;
	LocationListInterface locationnlistinterface;
	DealFeedDetailInterface dealfeeddetailinterface;
	Context context;
	NotificaiondetailadapterToFragment notificationdeleteinterface;
	topBarInteface topInterface;
	long MIN_CLICK_INTERVAL = 800; // in millis
	long lastClickTime = 0;
	String notificationtype;

	public NotificationDetailAdapter(Activity context,
			NotificaiondetailadapterToFragment notificationdeleteinterface,
			ArrayList<NotificationDetail> arrNotification,
			LocationListInterface locationnlistinterface,
			DealFeedDetailInterface dealfeeddetailinterface,
			String notificationtype) {
		this.context = context;
		topInterface = (topBarInteface) context;
		this.notificationdeleteinterface = notificationdeleteinterface;
		this.arrNotification = arrNotification;
		this.locationnlistinterface = locationnlistinterface;
		this.dealfeeddetailinterface = dealfeeddetailinterface;
		this.notificationtype = notificationtype;
	}

	@Override
	public String getItem(int position) {
		return arrNotification.get(position).toString();
	}

	public static class ViewHolder {
		public ImageView image, notify_icon_image;
		public TextView notificationtext;
		public LinearLayout layoutMain;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		final NotificationDetail notificationdetail = arrNotification
				.get(position);

		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_notification_detail,
					null);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.notify_icon_image = (ImageView) convertView
				.findViewById(R.id.notifiy_icon_image);

		if (notificationtype.equals("Cateogry")) {

			holder.notify_icon_image
					.setImageResource(R.drawable.icon_notification_by_category);

		} else if (notificationtype.equalsIgnoreCase("favloc")
				|| notificationtype.equalsIgnoreCase("onsite")
				|| notificationtype.equalsIgnoreCase("nearby")) {
			holder.notify_icon_image
					.setImageResource(R.drawable.icon_notification_by_location);
		} else if (notificationtype.equals("Deal")) {
			holder.notify_icon_image
					.setImageResource(R.drawable.icon_notification_by_deal);
		} else if (notificationtype.equals("Loyality")) {
			holder.notify_icon_image
					.setImageResource(R.drawable.icon_notification_by_loyalty);
		} else if (notificationtype.equals("wishlist")) {
			holder.notify_icon_image
					.setImageResource(R.drawable.icon_notification_by_wishlist);
		} else if (notificationtype.equals("Mimcoups")) {
			holder.notify_icon_image
					.setImageResource(R.drawable.icon_notification_by_mimcupos);
		}
		holder.notificationtext = (TextView) convertView
				.findViewById(R.id.notificationtext);
		holder.layoutMain = (LinearLayout) convertView
				.findViewById(R.id.layout_main);

		holder.notificationtext
				.setText(notificationdetail.vNotificationMessage);
		Log.d("Status ", notificationdetail.vStatus);

		if (notificationdetail.vStatus.equalsIgnoreCase("UnRead")) {
			Log.e("called unread", notificationdetail.vNotificationMessage
					+ ">>" + notificationdetail.vRedirectTag);
			holder.layoutMain.setBackgroundColor(Color.parseColor("#50808080"));
		} else {
			Log.e("called read", notificationdetail.vNotificationMessage);
			holder.layoutMain.setBackgroundColor(Color.WHITE);
		}
		convertView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {

				final int customerwishlistid = v.getId();

				DialogFragment s = new SelectionButtonAlert(
						new DialogInterfaceClick() {

							@Override
							public void dialogClick(String tag) {
								// TODO Auto-generated method stub
								if (tag.equals("OK")) {

									notificationdeleteinterface
											.notificationDelete(notificationdetail);
								}
							}
						}, context.getResources().getString(
								R.string.wishlistdeletemsg));

				s.show(((Activity) context).getFragmentManager(), "");
				return true;
			}
		});

		convertView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;

					// Log.e("clicks notification detail", notificationtype);
					String redirectmsg = notificationdetail.vRedirectTag;
					// Log.e("clicks Redirect detail", redirectmsg);
					if (redirectmsg.equals("store")) {
						LocationList list = new LocationList();
						list.iMerchantStoreId = notificationdetail.iLocationId;
						locationnlistinterface.locationDetail(list, "list");
					} else if (redirectmsg.equals("deal")) {

						DealFeedDetail dealfeedddetail = new DealFeedDetail();
						dealfeedddetail.categoryId = notificationdetail.iCategoryId;
						dealfeedddetail.DealId = notificationdetail.iMerchantDealId;
						dealfeedddetail.locationId = notificationdetail.iLocationId;
						dealfeeddetailinterface.dealFeedClick("DealDetail",
								dealfeedddetail);
					}
				}

				// Handler handle = new Handler();
				// handle.postDelayed(new Runnable() {
				// @Override
				// public void run() {
				//
				// String redirectmsg = notificationdetail.vRedirectTag;
				// if (redirectmsg.equals("store")) {
				// LocationList list = new LocationList();
				// list.iMerchantStoreId = notificationdetail.iLocationId;
				// topInterface.hideDialog();
				// locationnlistinterface.locationDetail(list, "list");
				//
				// }
				//
				// else if (redirectmsg.equals("deal")) {
				//
				// DealFeedDetail dealfeedddetail = new DealFeedDetail();
				// dealfeedddetail.categoryId = notificationdetail.iCategoryId;
				// dealfeedddetail.DealId = notificationdetail.iMerchantDealId;
				// dealfeedddetail.locationId = notificationdetail.iLocationId;
				// topInterface.hideDialog();
				// dealfeeddetailinterface.dealFeedClick("DealDetail",
				// dealfeedddetail);
				//
				// }
				// }
				// }, 3000);

				// topInterface.hideDialog();
				// String redirectmsg = notificationdetail.vRedirectTag;
				// if (redirectmsg.equals("store")) {
				// LocationList list = new LocationList();
				// list.iMerchantStoreId = notificationdetail.iLocationId;
				// // topInterface.hideDialog();
				// locationnlistinterface.locationDetail(list, "list");
				//
				// }
				//
				// else if (redirectmsg.equals("deal")) {
				//
				// DealFeedDetail dealfeedddetail = new DealFeedDetail();
				// dealfeedddetail.categoryId = notificationdetail.iCategoryId;
				// dealfeedddetail.DealId = notificationdetail.iMerchantDealId;
				// dealfeedddetail.locationId = notificationdetail.iLocationId;
				// // topInterface.hideDialog();
				// dealfeeddetailinterface.dealFeedClick("DealDetail",
				// dealfeedddetail);
				//
				// }

				// topInterface.showDialog("Wait..");

			}
		});

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrNotification.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
