package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.DialogFragment.ConfirmAlert;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.discusschatinteface;
import com.mimcoups.Consumer.inteface.unFriendInterface;
import com.squareup.picasso.Picasso;

public class SliderFriendsListAdapter extends BaseAdapter {

	ArrayList<FriendsDetail> arrFriends;
	String Data;
	LayoutInflater inflate1;
	discusschatinteface discussinterface;
	private Activity activity;
	private unFriendInterface unFriend;

	public SliderFriendsListAdapter(Activity activity,
			ArrayList<FriendsDetail> arrFriends, unFriendInterface unFriend) {
		this.activity = activity;
		this.unFriend = unFriend;
		discussinterface = (discusschatinteface) activity;
		this.arrFriends = arrFriends;
	}

	@Override
	public String getItem(int position) {
		return arrFriends.get(position).freindsName;
	}

	public static class ViewHolder {
		public ImageView imgFriendPicture, imgOnlineOfflineIcon;
		public TextView txtFriendName;

	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final FriendsDetail friendsDetail = arrFriends.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_slider_friend_list,
					null);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txtFriendName = (TextView) convertView
				.findViewById(R.id.txtFriendName);
		holder.imgFriendPicture = (ImageView) convertView
				.findViewById(R.id.imgFriendPicture);
		holder.imgOnlineOfflineIcon = (ImageView) convertView
				.findViewById(R.id.imgOnlineOfflineIcon);
		convertView.setTag(holder);
		holder.txtFriendName.setText(friendsDetail.freindsName);
		Picasso.with(activity).load(friendsDetail.profileImageName)
				.transform(new RoundedTransformation(18, 0))
				.into(holder.imgFriendPicture);
		holder.imgOnlineOfflineIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stu
				DialogFragment dsp = new ConfirmAlert(
						new DialogInterfaceClick() {

							@Override
							public void dialogClick(String tag) {
								// TODO Auto-generated method stub
								if (tag.equals("Ok")) {

									if (((DeelFeedActivity) activity).commonClass
											.checkInternetConnection()) {
										
										unFriend.unFriendClick(friendsDetail.userName,position);
										
									}
									

								}
							}
						}, activity.getResources().getString(
								R.string.unfriendUser));
				dsp.show(activity.getFragmentManager(), "");
			}
		});

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrFriends.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}
