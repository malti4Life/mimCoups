package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.mimcoups.Consumer.inteface.categoryDetailInterface;
import com.mimcoups.Consumer.inteface.favouriteCategoryInteface;
import com.squareup.picasso.Picasso;

public class CategoryAdpater extends BaseAdapter {

	private ArrayList<CategoryDetail> arrCategory;
	private LayoutInflater inflate1;
	private categoryDetailInterface infCategroyDetail;
	private favouriteCategoryInteface favouriteCategory;
	private Context context;
	private int lastPosition = 0;

	public CategoryAdpater(Activity activity,
			ArrayList<CategoryDetail> arrCategory,
			favouriteCategoryInteface favouriteCategory) {
		infCategroyDetail = (categoryDetailInterface) activity;
		this.arrCategory = arrCategory;
		this.favouriteCategory = favouriteCategory;
		context = activity;
	}

	@Override
	public String getItem(int position) {
		return arrCategory.get(position).toString();
	}

	public static class ViewHolder {
		public RelativeLayout rlCategoryBackGround;
		public ImageView imgCategoryIcon;
		public TextView txtCategoryName;
		public CheckBox cbCategoryFavourite;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final CategoryDetail categroyDetail = arrCategory.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_categories, null);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.rlCategoryBackGround = (RelativeLayout) convertView
				.findViewById(R.id.rlCategoryBackGround);
		holder.imgCategoryIcon = (ImageView) convertView
				.findViewById(R.id.imgCategoryIcon);

		holder.cbCategoryFavourite = (CheckBox) convertView
				.findViewById(R.id.cbCategoryFavourite);

		holder.txtCategoryName = (TextView) convertView
				.findViewById(R.id.txtCategoryName);

		convertView.setTag(R.id.rlCategoryBackGround,
				holder.rlCategoryBackGround);
		convertView.setTag(R.id.imgCategoryIcon, holder.imgCategoryIcon);
		convertView
				.setTag(R.id.cbCategoryFavourite, holder.cbCategoryFavourite);
		convertView.setTag(R.id.txtCategoryName, holder.txtCategoryName);
		convertView.setTag(holder);

		holder.txtCategoryName.setText(categroyDetail.categoryName);
		Picasso.with(context).load(categroyDetail.vCategoryImage_A_90X90)
				.into(holder.imgCategoryIcon);

		holder.cbCategoryFavourite.setChecked(categroyDetail.CategoryFavourite);

		if (categroyDetail.CategoryFavourite) {
			holder.cbCategoryFavourite.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.icon_rate_mimcoups_app_selected, 0, 0, 0);

		}
		if (!categroyDetail.CategoryFavourite) {
			holder.cbCategoryFavourite.setCompoundDrawablesWithIntrinsicBounds(
					R.drawable.icon_rate_mimcoups_app, 0, 0, 0);
		}

		try {

			holder.rlCategoryBackGround.setBackgroundColor(Color
					.parseColor(categroyDetail.categoryBackGround));

		} catch (Exception e) {
			// TODO: handle exception
		}

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				infCategroyDetail.categoryClick(categroyDetail.categoryId,
						categroyDetail.categoryName);
			}
		});

		holder.cbCategoryFavourite
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						categroyDetail.CategoryFavourite = !categroyDetail.CategoryFavourite;
						arrCategory.set(position, categroyDetail);
						favouriteCategory.favouriteCategoryClick(
								categroyDetail.categoryId,
								holder.cbCategoryFavourite,
								categroyDetail.CategoryFavourite, position);
						notifyDataSetChanged();

					}
				});

		// holder.cbCategoryFavourite
		// .setOnCheckedChangeListener(new OnCheckedChangeListener() {
		//
		// @Override
		// public void onCheckedChanged(CompoundButton buttonView,
		// boolean isChecked) {
		// // TODO Auto-generated method stub
		// // holder.cbCategoryFavourite.setClickable(false);
		// favouriteCategory.favouriteCategoryClick(
		// categroyDetail.categoryId,
		// holder.cbCategoryFavourite, isChecked);
		// }
		// });
		// Animation animation = AnimationUtils.loadAnimation(context,
		// (position > lastPosition) ? R.anim.up_from_bottom
		// : R.anim.down_from_top);
		// convertView.startAnimation(animation);
		lastPosition = position;
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrCategory.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
