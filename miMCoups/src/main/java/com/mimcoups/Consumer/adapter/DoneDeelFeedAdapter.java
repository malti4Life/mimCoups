package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.squareup.picasso.Picasso;

public class DoneDeelFeedAdapter extends BaseAdapter {

	ArrayList<DealFeedDetail> arrDeals;
	DealFeedDetailInterface dealInterface;
	String Data;
	LayoutInflater inflate1;
	private Context context;

	public DoneDeelFeedAdapter(Activity context,
			ArrayList<DealFeedDetail> arrDeals) {
		this.context = context;
		this.arrDeals = arrDeals;
		dealInterface = (DealFeedDetailInterface) context;
	}

	@Override
	public String getItem(int position) {
		return arrDeals.get(position).toString();
	}

	public class ViewHolder {
		public ImageView imgDealImage, imgDealBoughtIcon;
		public TextView txtdealfeedDiscountprice, txtDealDetail,
				txtDealTypeDescription, txtDealType, txtDealBought;
		public LinearLayout lnDealBought;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final DealFeedDetail dealFeedDetail = arrDeals.get(position);
		if (convertView == null) {

			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_donedeal, null);
			holder.imgDealImage = (ImageView) convertView
					.findViewById(R.id.imgDealImage);
			holder.imgDealBoughtIcon = (ImageView) convertView
					.findViewById(R.id.imgDealBoughtIcon);

			holder.txtdealfeedDiscountprice = (TextView) convertView
					.findViewById(R.id.txtdealfeedDiscountprice);
			holder.txtDealDetail = (TextView) convertView
					.findViewById(R.id.txtDealDetail);
			holder.txtDealTypeDescription = (TextView) convertView
					.findViewById(R.id.txtDealTypeDescription);
			holder.txtDealType = (TextView) convertView
					.findViewById(R.id.txtDealType);
			holder.txtDealBought = (TextView) convertView
					.findViewById(R.id.txtDealBought);

			holder.lnDealBought = (LinearLayout) convertView
					.findViewById(R.id.lnDealBought);

			convertView.setTag(R.id.imgDealImage, holder.imgDealImage);
			convertView
					.setTag(R.id.imgDealBoughtIcon, holder.imgDealBoughtIcon);
			convertView.setTag(R.id.txtdealfeedDiscountprice,
					holder.txtdealfeedDiscountprice);
			convertView.setTag(R.id.txtDealDetail, holder.txtDealDetail);
			convertView.setTag(R.id.txtDealTypeDescription,
					holder.txtDealTypeDescription);
			convertView.setTag(R.id.txtDealType, holder.txtDealType);
			convertView.setTag(R.id.txtDealBought, holder.txtDealBought);
			convertView.setTag(R.id.lnDealBought, holder.lnDealBought);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// ///////////////////Set All Text and Image Here

		Picasso.with(context).load(dealFeedDetail.vDealListImage_A_350X350)
				.placeholder(R.drawable.noimage_deal).into(holder.imgDealImage);

		holder.txtDealBought.setText(dealFeedDetail.vBoughtTime);
		holder.txtDealType.setText(dealFeedDetail.vMerchantDealName);
		holder.txtDealTypeDescription
				.setText(dealFeedDetail.vShortDescriptionHeading);

		dealFeedDetail.vShortDescription = dealFeedDetail.vShortDescription
				.replace("\\n", "\n");

		dealFeedDetail.vShortDescription = dealFeedDetail.vShortDescription
				.replace(">", "●");

		holder.txtDealDetail.setText(dealFeedDetail.vShortDescription);

		holder.txtdealfeedDiscountprice
				.setText(dealFeedDetail.dDealDiscountedAmount);

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// dealInterface.dealFeedClick("DealDetail", dealFeedDetail);

			}
		});
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrDeals.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}
