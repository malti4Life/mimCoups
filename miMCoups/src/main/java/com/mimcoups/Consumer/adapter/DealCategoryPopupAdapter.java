package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.squareup.picasso.Picasso;

public class DealCategoryPopupAdapter extends BaseAdapter {
	ArrayList<CategoryDetail> arrCategory;
	// dealCategoryPopupInterface categorypopupdetail;
	LayoutInflater inflate;
	Context context;

	public DealCategoryPopupAdapter(Context context,
			ArrayList<CategoryDetail> arrCategory) {
		this.arrCategory = arrCategory;
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrCategory.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arrCategory.get(position).toString();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static class ViewHolder {
		public RelativeLayout rl_categoryblog;
		public ImageView popupcategoryimg;
		public TextView popupcategoryname;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;

		CategoryDetail categoryInfo = arrCategory.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate
					.inflate(R.layout.row_deal_popupcategory, null);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.popupcategoryname = (TextView) convertView
				.findViewById(R.id.dealpopupcategoryname);

		convertView
				.setTag(R.id.dealpopupcategoryname, holder.popupcategoryname);

		holder.popupcategoryimg = (ImageView) convertView
				.findViewById(R.id.dealpopupcategoryimg);

		convertView.setTag(R.id.dealpopupcategoryimg, holder.popupcategoryimg);

		holder.rl_categoryblog = (RelativeLayout) convertView
				.findViewById(R.id.rl_categoryblog);
		convertView.setTag(holder);
		Picasso.with(context).load(categoryInfo.vCategoryImage_A_70X70)
				.into(holder.popupcategoryimg);
		holder.popupcategoryname.setText(categoryInfo.categoryName);
		return convertView;
	}

}
