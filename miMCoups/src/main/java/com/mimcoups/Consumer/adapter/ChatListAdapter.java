package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.SectionedBaseAdapter;
import com.mimcoups.Consumer.customClass.ChatUser;
import com.mimcoups.Consumer.inteface.discusschatinteface;
import com.squareup.picasso.Picasso;

public class ChatListAdapter extends SectionedBaseAdapter {

	ArrayList<ChatUser> arrChatUsers;
	String Data;
	LayoutInflater inflate1;
	discusschatinteface discussinterface;
	Boolean onlineheader;
	Boolean offlineheader;
	Context context;
	ArrayList<ChatUser> arrOnlineChats;
	ArrayList<ChatUser> arrOfflineChats;
	discusschatinteface dealInterface;
	SharedPreferences preference;
	ArrayList<String> dynamicsection;

	public ChatListAdapter(Activity activity,
			ArrayList<ChatUser> arrOnlineChats,
			ArrayList<ChatUser> arrOfflineChats) {

		discussinterface = (discusschatinteface) activity;
		this.arrChatUsers = arrChatUsers;
		this.arrOnlineChats = arrOnlineChats;
		this.arrOfflineChats = arrOfflineChats;
		onlineheader = false;
		offlineheader = false;
		context = activity;
		dealInterface = (discusschatinteface) context;
		preference = activity.getSharedPreferences("FriendList",
				Context.MODE_PRIVATE);

		dynamicsection = new ArrayList<String>();

		dynamicsection.add("online");
		dynamicsection.add("offline");

	}

	@Override
	public Object getItem(int section, int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int section, int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getSectionCount() {
		// TODO Auto-generated method stub

		if (arrOnlineChats.size() == 0) {
			int buddyindex = dynamicsection.indexOf("online");
			dynamicsection.remove(buddyindex);
		}
		if (arrOfflineChats.size() == 0) {
			int groupindex = dynamicsection.indexOf("offline");
			dynamicsection.remove(groupindex);
		}

		return dynamicsection.size();
	}

	@Override
	public int getCountForSection(int section) {
		// TODO Auto-generated method stub
		int sectionitem = 0;

		if (section == 0) {
			String zeroposition = dynamicsection.get(0).toString();
			if (zeroposition.equals("online")) {
				sectionitem = arrOnlineChats.size();
			}
			if (zeroposition.equals("offline")) {
				sectionitem = arrOfflineChats.size();
			}

		}
		if (section == 1) {
			String firstposition = dynamicsection.get(1).toString();
			if (firstposition.equals("online")) {
				sectionitem = arrOnlineChats.size();
			}
			if (firstposition.equals("offline")) {
				sectionitem = arrOfflineChats.size();
			}
		}

		return sectionitem;
	}

	@Override
	public View getItemView(int section, final int position, View convertView,
			ViewGroup parent) {

		RelativeLayout layout = null;

		if (convertView == null) {
			LayoutInflater inflator = (LayoutInflater) parent.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			layout = (RelativeLayout) inflator.inflate(
					R.layout.row_chat_onlinefriends, null);
		} else {
			layout = (RelativeLayout) convertView;
		}

		ImageView imgProfilePicture = (ImageView) layout
				.findViewById(R.id.imgProfilePicture);
		ImageView imgChatListOnlineOfflineIcon = (ImageView) layout
				.findViewById(R.id.imgChatListOnlineOfflineIcon);
		TextView txtUsername = (TextView) layout.findViewById(R.id.txtUsername);

		LinearLayout badgecontainer = (LinearLayout) layout
				.findViewById(R.id.badgecontainer);

		TextView badgecount = (TextView) layout.findViewById(R.id.badgecount);

		if (section == 0) {

			String zeroposition = dynamicsection.get(0).toString();

			if (zeroposition.equals("online")) {

				txtUsername.setText(arrOnlineChats.get(position).customername);

				String username = arrOnlineChats.get(position).userName;
				username = username.toLowerCase();

				int notificationcount = preference.getInt(username, 0);

				if (notificationcount == 0) {
					badgecontainer.setVisibility(View.GONE);
				} else {
					badgecontainer.setVisibility(View.VISIBLE);
					badgecount.setText("" + notificationcount);
				}

				Log.i("ADAPTER ONLINE USERNAME>>>>",
						"" + arrOnlineChats.get(position).userName + "  ->   "
								+ notificationcount);

				String profileurl = arrOnlineChats.get(position).profileUrl;

				Picasso.with(context).load(profileurl)
						.transform(new RoundedTransformation(18, 0))
						.into(imgProfilePicture);

				imgChatListOnlineOfflineIcon
						.setImageResource(R.drawable.icon_online);

				layout.setTag("0");

			}

			if (zeroposition.equals("offline")) {

				txtUsername.setText(arrOfflineChats.get(position).customername);

				String username = arrOfflineChats.get(position).userName;
				username = username.toLowerCase();
				int notificationcount = preference.getInt(username, 0);

				if (notificationcount == 0) {
					badgecontainer.setVisibility(View.GONE);
				} else {
					badgecontainer.setVisibility(View.VISIBLE);
					badgecount.setText("" + notificationcount);
				}

				Log.i("ADAPTER OFFLINE USERNAME>>>>",
						"" + arrOfflineChats.get(position).userName + "  ->   "
								+ notificationcount);
				String profileurl = arrOfflineChats.get(position).profileUrl;

				Picasso.with(context).load(profileurl)
						.transform(new RoundedTransformation(18, 0))
						.into(imgProfilePicture);

				imgChatListOnlineOfflineIcon
						.setImageResource(R.drawable.icon_offline);

				layout.setTag("1");

			}

		}

		if (section == 1) {
			txtUsername.setText(arrOfflineChats.get(position).customername);

			String username = arrOfflineChats.get(position).userName;
			username = username.toLowerCase();

			int notificationcount = preference.getInt(username, 0);

			if (notificationcount == 0) {
				badgecontainer.setVisibility(View.GONE);
			} else {
				badgecontainer.setVisibility(View.VISIBLE);
				badgecount.setText("" + notificationcount);
			}

			String profileurl = arrOfflineChats.get(position).profileUrl;

			Picasso.with(context).load(profileurl).into(imgProfilePicture);

			imgChatListOnlineOfflineIcon
					.setImageResource(R.drawable.icon_offline);

			layout.setTag("1");

		}

		layout.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String tag = v.getTag().toString();

				if (tag.equals("0")) {
//					Log.d("position>>", "" + position);
//					Log.d("array size>>", "" + arrOnlineChats.size());
					ChatUser ch = arrOnlineChats.get(position);
					dealInterface.discussOneToOneChatClick(ch);

				}

				if (tag.equals("1")) {
					ChatUser ch = arrOfflineChats.get(position);
					dealInterface.discussOneToOneChatClick(ch);
				}

			}
		});

		return layout;
	}

	@Override
	public View getSectionHeaderView(int section, View convertView,
			ViewGroup parent) {

		// TODO Auto-generated method stub

		LinearLayout layout = null;
		if (convertView == null) {
			LayoutInflater inflator = (LayoutInflater) parent.getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			layout = (LinearLayout) inflator.inflate(
					R.layout.row_chat_online_header, null);
		} else {
			layout = (LinearLayout) convertView;
		}

		TextView headertext = (TextView) layout
				.findViewById(R.id.txtOnlineFriends);

		if (section == 0) {

			if (arrOnlineChats.size() > 0) {
				headertext.setText(context.getResources().getString(
						R.string.onlinefriends));

			} else {
				headertext.setText(context.getResources().getString(
						R.string.offlinefriends));
			}

		}
		if (section == 1) {
			headertext.setText(context.getResources().getString(
					R.string.offlinefriends));
		}

		return layout;
	}
}
