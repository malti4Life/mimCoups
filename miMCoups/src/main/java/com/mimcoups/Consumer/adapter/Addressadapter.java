package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.Address;

public class Addressadapter extends BaseAdapter {

	ArrayList<Address> arrAddress=new ArrayList<Address>();
	LayoutInflater inflate1;
	private Activity activity;

	String Type;

	public Addressadapter(Activity activity, ArrayList<Address> arrAddress,
			String Type) {
		this.activity = activity;
		this.arrAddress = arrAddress;
		this.Type = Type;

	}

	@Override
	public Address getItem(int position) {
		return arrAddress.get(position);
	}

	@Override
	public int getItemViewType(int position) {
		return 0;

	}

	public static class ViewHolder {

		public TextView txtAddressName;

	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_address_list, null);

			holder.txtAddressName = (TextView) convertView
					.findViewById(R.id.txtaddress);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (Type.equalsIgnoreCase("Shipping")) {

			holder.txtAddressName
					.setText(arrAddress.get(position).vShippingAddress);
		} else {
			holder.txtAddressName
					.setText(arrAddress.get(position).vBillingAddress);
		}

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		// return arrAddress.size();
		return arrAddress.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}