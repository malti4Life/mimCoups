package com.mimcoups.Consumer.adapter;

import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.customClass.supportchat;
import com.squareup.picasso.Picasso;

public class ChatDetailAdapter extends BaseAdapter {

	String Data;
	LayoutInflater inflate1;
	Context context;
	ArrayList<supportchat> arrchat;
	CommonData globalClass;
	String to_profileurl;
	Bitmap frompic, topic;

	public ChatDetailAdapter(final Context context,
			ArrayList<supportchat> arrchat, final String to_profileurl) {
		this.context = context;
		this.arrchat = arrchat;
		this.to_profileurl = to_profileurl;
		globalClass = (CommonData) context.getApplicationContext();

		Thread th = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				try {
					topic = Picasso.with(context).load(to_profileurl)
							.transform(new RoundedTransformation(20, 0)).get();
					frompic = Picasso.with(context)
							.load(globalClass.ProfiileImagePath)
							.transform(new RoundedTransformation(20, 0)).get();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		th.start();
	}

	@Override
	public String getItem(int position) {
		return null;
	}

	public static class ViewHolder {
		public ImageView imgChatDetailUserPicture;
		public TextView txtChatDetailMessage;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return position;

	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		holder = new ViewHolder();
		inflate1 = (LayoutInflater) parent.getContext().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		if (arrchat.get(position).flag == 0) {
			convertView = inflate1.inflate(R.layout.row_chat_detail_sender,
					null);
			convertView.setTag(holder);
			holder.imgChatDetailUserPicture = (ImageView) convertView
					.findViewById(R.id.imgChatSenderDetailUserPicture);
			holder.txtChatDetailMessage = (TextView) convertView
					.findViewById(R.id.txtChatSenderDetailMessage);
			
			Picasso.with(context).load(globalClass.ProfiileImagePath).placeholder(R.drawable.icon_perchasers_photo)
			.transform(new RoundedTransformation(20, 0)).into(holder.imgChatDetailUserPicture);
			

//			holder.imgChatDetailUserPicture.setImageBitmap(frompic);

			holder.txtChatDetailMessage.setText(arrchat.get(position).message);

		} else {
			convertView = inflate1.inflate(R.layout.row_chat_detail_receiver,
					null);
			convertView.setTag(holder);

			holder.imgChatDetailUserPicture = (ImageView) convertView
					.findViewById(R.id.imgChatDetailUserPicture);
			holder.txtChatDetailMessage = (TextView) convertView
					.findViewById(R.id.txtChatDetailMessage);
			holder.txtChatDetailMessage.setText(arrchat.get(position).message);
			
			Picasso.with(context).load(to_profileurl).placeholder(R.drawable.icon_perchasers_photo)
			.transform(new RoundedTransformation(20, 0)).into(holder.imgChatDetailUserPicture);

//			holder.imgChatDetailUserPicture.setImageBitmap(topic);

		}
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrchat.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
