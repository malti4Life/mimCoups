package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.inteface.LocationListInterface;
import com.squareup.picasso.Picasso;

public class LocationListAdpater extends BaseAdapter {

	ArrayList<LocationList> arrLocation;
	String Data;
	LayoutInflater inflate1;
	private LocationListInterface locationListInterface;
	Context context;
	private int lastPosition = 0;

	public LocationListAdpater(Activity activity,
			ArrayList<LocationList> arr_locationlist) {
		locationListInterface = (LocationListInterface) activity;
		this.arrLocation = arr_locationlist;
		context = activity;
	}

	@Override
	public String getItem(int position) {
		return arrLocation.get(position).vMerchantStoreName;
	}

	public static class ViewHolder {
		public ImageView imgLocationImage;
		public TextView txtDealName, txtDealDistane, txtLocationDeals,
				txtDealDetail;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final LocationList locationList = arrLocation.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_location, null);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.imgLocationImage = (ImageView) convertView
				.findViewById(R.id.imgLocationImage_row_location);

		holder.txtDealName = (TextView) convertView
				.findViewById(R.id.txtDealName_rowlocation);

		holder.txtDealDistane = (TextView) convertView
				.findViewById(R.id.txtDealDistane_rowLocation);

		holder.txtLocationDeals = (TextView) convertView
				.findViewById(R.id.txtLocationDeals_rowlocation);

		holder.txtDealDetail = (TextView) convertView
				.findViewById(R.id.txtloc_desc_rowlocation);

		convertView.setTag(holder);

		// set Data
		Picasso.with(context).load(locationList.vStoreListImage_A_70X70)
				.placeholder(R.drawable.noimage_locationlist)
				.into(holder.imgLocationImage);

		holder.txtDealDetail.setText(locationList.vShortDescription);
		holder.txtDealDistane.setText(locationList.dDistance);
		holder.txtDealName.setText(locationList.vMerchantStoreName);
		holder.txtLocationDeals.setText(locationList.iDealCount);

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				locationListInterface.locationDetail(locationList, "list");
			}
		});
		// Animation animation = AnimationUtils.loadAnimation(context,
		// (position > lastPosition) ? R.anim.up_from_bottom
		// : R.anim.down_from_top);
		// convertView.startAnimation(animation);
		lastPosition = position;
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrLocation.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
