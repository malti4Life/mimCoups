package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SelectionButtonAlert;
import com.mimcoups.Consumer.customClass.WishListDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.inteface.wishlistadpaterToFragment;

public class WishListAdapter extends BaseAdapter {

	ArrayList<WishListDetail> arrWishList;
	String Data;
	LayoutInflater inflate1;
	private topBarInteface topInterface;
	Context context;
	private wishlistadpaterToFragment wishlistadapterInterface;

	public WishListAdapter(Activity context,
			ArrayList<WishListDetail> arrWishList,
			wishlistadpaterToFragment wishlistadapterInterface) {
		this.context = context;
		topInterface = (topBarInteface) context;
		this.arrWishList = arrWishList;
		this.wishlistadapterInterface = wishlistadapterInterface;
	}

	@Override
	public String getItem(int position) {
		return arrWishList.get(position).toString();
	}

	public static class ViewHolder {
		public ImageView imgWishListImage,imgDelete;
		public TextView txtWishListName, txtKeyword, txtZipCode,
				txtNotificationOn;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final WishListDetail wishlistobj = arrWishList.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_wishlist, null);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.imgWishListImage = (ImageView) convertView
				.findViewById(R.id.imgWishListImage);
		holder.imgDelete=(ImageView)convertView.findViewById(R.id.imgDelete);
		holder.txtWishListName = (TextView) convertView
				.findViewById(R.id.txtWishListName);
		holder.txtKeyword = (TextView) convertView
				.findViewById(R.id.txtKeyword);
		holder.txtZipCode = (TextView) convertView
				.findViewById(R.id.txtZipCode);
		holder.txtNotificationOn = (TextView) convertView
				.findViewById(R.id.txtNotificationOn);

		convertView.setTag(holder);

		// set data

		holder.txtWishListName.setText(wishlistobj.categoryName);
		holder.txtKeyword.setText(wishlistobj.Keyword);
		holder.txtZipCode.setText(wishlistobj.zipCode);

		if (wishlistobj.sendNotificaion.equals("true")) {
			holder.txtNotificationOn.setText("ON");
			holder.txtNotificationOn.setTextColor(Color.parseColor("#329D43"));
		} else {
			holder.txtNotificationOn.setText("OFF");
			holder.txtNotificationOn.setTextColor(Color.parseColor("#DE444A"));
		}
		
		holder.imgDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

				final int customerwishlistid = v.getId();

				DialogFragment s = new SelectionButtonAlert(
						new DialogInterfaceClick() {

							@Override
							public void dialogClick(String tag) {
								// TODO Auto-generated method stub
								if (tag.equals("OK")) {
									wishlistadapterInterface
											.wishlistDelete(wishlistobj);
								}
							}
						}, context.getResources().getString(
								R.string.wishlistdeletemsg));

				s.show(((Activity) context).getFragmentManager(), "");
			
				
			}
		});
//		convertView.setOnLongClickListener(new OnLongClickListener() {
//
//			@Override
//			public boolean onLongClick(View v) {
//				final int customerwishlistid = v.getId();
//
//				DialogFragment s = new SelectionButtonAlert(
//						new DialogInterfaceClick() {
//
//							@Override
//							public void dialogClick(String tag) {
//								// TODO Auto-generated method stub
//								if (tag.equals("OK")) {
//									wishlistadapterInterface
//											.wishlistDelete(wishlistobj);
//								}
//							}
//						}, context.getResources().getString(
//								R.string.wishlistdeletemsg));
//
//				s.show(((Activity) context).getFragmentManager(), "");
//				return true;
//			}
//		});

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				topInterface.sendDatatoFragment(wishlistobj);
			}
		});
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrWishList.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
