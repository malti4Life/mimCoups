package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.FontFitTextView;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.mimcoups.Consumer.inteface.favouriteDealInteface;
import com.squareup.picasso.Picasso;

public class FavouriteDoneDealFeedAdapter extends BaseAdapter {

	ArrayList<DealFeedDetail> arrDeals;

	String Data;
	LayoutInflater inflate1;
	DealFeedDetailInterface dealInterface;
	private favouriteDealInteface favouriteDealinterface;

	Activity activity;

	public FavouriteDoneDealFeedAdapter(Activity activity,
			ArrayList<DealFeedDetail> arrDeals,
			favouriteDealInteface favouriteDealinterface) {
		this.activity = activity;
		dealInterface = (DealFeedDetailInterface) activity;
		this.arrDeals = arrDeals;
		this.favouriteDealinterface = favouriteDealinterface;

	}

	@Override
	public String getItem(int position) {
		return arrDeals.get(position).toString();
	}

	public static class ViewHolder {
		public ImageView imgDealImage;
		public TextView txtdealfeedDistance;
		public TextView txtDealName, txtDealType, txtDealDiscription,
				txtDealDetail, txtrow;

		public FontFitTextView txtdealfeedtotalprice, txtdealfeedDiscountPrice;
		View buyview;
		LinearLayout lnDealFeedBuyNow;
		public CheckBox cbFavourite;
		public LinearLayout lnDealShare, lnDealDiscuss, lnDealBuyNow;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final DealFeedDetail dealDetail = arrDeals.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_done_deals_favourite,
					null);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.imgDealImage = (ImageView) convertView
				.findViewById(R.id.imgDealImage);
		holder.lnDealFeedBuyNow = (LinearLayout) convertView
				.findViewById(R.id.lnDealBuyNow);

		holder.txtdealfeedDistance = (TextView) convertView
				.findViewById(R.id.txtdealfeedDistance);

		holder.txtdealfeedtotalprice = (FontFitTextView) convertView
				.findViewById(R.id.txtdealfeedtotalprice);

		holder.txtdealfeedDiscountPrice = (FontFitTextView) convertView
				.findViewById(R.id.txtdealfeedDiscountPrice);

		holder.txtDealName = (TextView) convertView
				.findViewById(R.id.txtDealName);
		holder.buyview = (View) convertView.findViewById(R.id.rawbuy);

		holder.txtDealType = (TextView) convertView
				.findViewById(R.id.txtDealType);

		holder.txtDealDiscription = (TextView) convertView
				.findViewById(R.id.txtDealDiscription);

		holder.txtDealDetail = (TextView) convertView
				.findViewById(R.id.txtDealDetail);

		holder.lnDealShare = (LinearLayout) convertView
				.findViewById(R.id.lnDealShare);

		holder.lnDealDiscuss = (LinearLayout) convertView
				.findViewById(R.id.lnDealDiscuss);

		holder.lnDealBuyNow = (LinearLayout) convertView
				.findViewById(R.id.lnDealBuyNow);

		holder.cbFavourite = (CheckBox) convertView
				.findViewById(R.id.cbdealFavourite);

		holder.txtrow = (TextView) convertView.findViewById(R.id.txtrow);
		convertView.setTag(holder);

		holder.cbFavourite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				// ((DeelFeedActivity) activity).isDealFavorite = true;
				// arrDeals.remove(position);
				// notifyDataSetChanged();
				favouriteDealinterface.favouriteDealClick(dealDetail.DealId,
						holder.cbFavourite, false, dealDetail);

			}
		});

		holder.txtDealName.setText(dealDetail.StoreName);
		holder.txtDealType.setText(dealDetail.dealName);

		holder.txtDealDiscription.setText(dealDetail.shortDescriptionHeading);

		dealDetail.shortDescription = dealDetail.shortDescription.replace(
				"\\n", "\n");
		holder.txtDealDetail.setText(dealDetail.shortDescription);
		// holder.txtdealfeedDistance.setText(dealDetail.dealDistanse+" "
		// + activity.getResources().getString(R.string.locallocationkm));

		holder.txtdealfeedDistance.setText(dealDetail.dealDistanse);
		Picasso.with(activity).load(dealDetail.DealImageUrl)
				.placeholder(R.drawable.noimage_deal).into(holder.imgDealImage);

		holder.txtdealfeedtotalprice.setPaintFlags(holder.txtdealfeedtotalprice
				.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

		if (dealDetail.dealDiscountPrice.trim().toString().contains("$0")
				|| dealDetail.dealDiscountPrice.trim().toString()
						.contains("kr0")) {
			
			String price="";

			if (dealDetail.dealDiscountPrice.trim().toString().contains("$0")) {

				price = dealDetail.dealDiscountPrice.trim().toString()
						.replace("$", "").trim();

			} else if (dealDetail.dealDiscountPrice.trim().toString()
					.contains("kr0")) {
				price = dealDetail.dealDiscountPrice.trim().toString()
						.replace("kr", "").trim();

			}

			if (Double.parseDouble(price) > 0) {
				
				holder.txtrow.setVisibility(View.VISIBLE);
				holder.txtdealfeedtotalprice.setVisibility(View.VISIBLE);

				holder.txtdealfeedtotalprice.setText(dealDetail.DealOrignalPrice);
				holder.txtdealfeedDiscountPrice
						.setText(dealDetail.dealDiscountPrice);

				holder.buyview.setVisibility(View.VISIBLE);
				holder.lnDealFeedBuyNow.setVisibility(View.VISIBLE);

			} else {
				
				holder.buyview.setVisibility(View.GONE);
				holder.lnDealFeedBuyNow.setVisibility(View.GONE);

				holder.txtrow.setVisibility(View.GONE);
				holder.txtdealfeedtotalprice.setVisibility(View.GONE);

				holder.txtdealfeedDiscountPrice.setText(activity.getResources()
						.getString(R.string.txtdealprice));

			}

			

		} else {

			holder.txtrow.setVisibility(View.VISIBLE);
			holder.txtdealfeedtotalprice.setVisibility(View.VISIBLE);

			holder.txtdealfeedtotalprice.setText(dealDetail.DealOrignalPrice);
			holder.txtdealfeedDiscountPrice
					.setText(dealDetail.dealDiscountPrice);

			holder.buyview.setVisibility(View.VISIBLE);
			holder.lnDealFeedBuyNow.setVisibility(View.VISIBLE);

		}

		// holder.txtDealfeedType.setText(dealDetail.dealName);
		// holder.txtDealfeedTypeDescription
		// .setText(dealDetail.shortDescriptionHeading);
		//
		// holder.txtDealfeedDescription.setText(dealDetail.shortDescription);
		// holder.txtdealfeedtotalprice.setText(dealDetail.DealOrignalPrice);
		// holder.txtDealFeedDiscoutPrice.setText(dealDetail.dealDiscountPrice);
		// holder.txtDealFeedDistance.setText(dealDetail.dealDistanse);
		// Picasso.with().load(dealDetail.DealImageUrl)
		// .placeholder(R.drawable.aamir).into(holder.imgDealImage);
		//
		// holder.txtdealfeedtotalprice.setPaintFlags(holder.txtdealfeedtotalprice
		// .getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG)
		;
		holder.lnDealBuyNow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dealInterface.dealFeedClick("DealPurchase", dealDetail);
			}
		});
		holder.lnDealShare.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dealInterface.dealFeedClick("DealShare", dealDetail);
			}
		});
		holder.lnDealDiscuss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dealInterface.dealFeedClick("dealBuy", dealDetail);
			}
		});

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dealInterface.dealFeedClick("FavouriteDealDetail", dealDetail);
			}
		});
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrDeals.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
