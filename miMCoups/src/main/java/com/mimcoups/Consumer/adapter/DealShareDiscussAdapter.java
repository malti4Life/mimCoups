package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.DialogFragment.ConfirmAlert;
import com.mimcoups.Consumer.customClass.ChatUser;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.DiscussdealSendRequestInterface;
import com.mimcoups.Consumer.inteface.bottombarClickInterface;
import com.mimcoups.Consumer.inteface.discusschatinteface;
import com.squareup.picasso.Picasso;

public class DealShareDiscussAdapter extends BaseAdapter {

	ArrayList<FriendsDetail> arrFriends;
	String Data;
	LayoutInflater inflate1;
	bottombarClickInterface bottomInterface;
	discusschatinteface discussinterface;
	Context context;
	private DiscussdealSendRequestInterface discussdealinterfaceobj;
	discusschatinteface dealInterface;
	private Activity activity;

	public DealShareDiscussAdapter(Activity activity,
			ArrayList<FriendsDetail> arrFriends,
			DiscussdealSendRequestInterface discussdealinterfaceobj,
			Context context) {
		// context = activity;
		context = context;
		this.activity = activity;
		bottomInterface = (bottombarClickInterface) activity;
		discussinterface = (discusschatinteface) activity;
		this.arrFriends = arrFriends;
		this.discussdealinterfaceobj = discussdealinterfaceobj;
		dealInterface = (discusschatinteface) context;
	}

	@Override
	public String getItem(int position) {
		return arrFriends.get(position).userName;
	}

	public static class ViewHolder {
		public ImageView image, imgsendrequest, imgProfilePicture;
		public TextView friendname;
		public TextView frindusername;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final FriendsDetail friendInfo = arrFriends.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(
					R.layout.row_discuss_dummy_offlinereomveicon, null);

			holder.friendname = (TextView) convertView
					.findViewById(R.id.txtUsername);

			holder.imgProfilePicture = (ImageView) convertView
					.findViewById(R.id.imgProfilePicture);

			holder.image = (ImageView) convertView
					.findViewById(R.id.imgDiscussOnlineOfflineIcon);

			holder.imgsendrequest = (ImageView) convertView
					.findViewById(R.id.imgsendrequest);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		convertView.setTag(holder);

		holder.friendname.setText(friendInfo.freindsName);
		final String urlpath = friendInfo.profileImageName;

		if (!friendInfo.isfriend) {
			holder.imgsendrequest.setVisibility(View.VISIBLE);

			holder.imgsendrequest
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub.

							DialogFragment ds = new ConfirmAlert(
									new DialogInterfaceClick() {

										@Override
										public void dialogClick(String tag) {
											// TODO Auto-generated method stub
											if (tag.equals("Ok")) {
												discussdealinterfaceobj
														.discussdealsendRequest(friendInfo);
											} else {

											}

										}
									}, activity.getResources().getString(
											R.string.makefriendalert));

							ds.show(activity.getFragmentManager(), "");

						}
					});
		} else {
			holder.imgsendrequest.setVisibility(View.GONE);
		}

		Picasso.with(context).load(urlpath)
				.transform(new RoundedTransformation(20, 0))
				.into(holder.imgProfilePicture);

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if (friendInfo.isfriend) {

				

					ChatUser ch = new ChatUser();
					ch.userName = friendInfo.userName;
					ch.id = friendInfo.friendsId;
					ch.profileUrl = friendInfo.profileImageName;
					ch.islonlie = false;
					dealInterface.discussOneToOneChatClick(ch);
				}

			}
		});
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrFriends.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
