package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.fragment.FragmentSliderSearchByNameEmail;
import com.mimcoups.Consumer.inteface.FriendSearchAdapterSendRequest;
import com.mimcoups.Consumer.inteface.discusschatinteface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.squareup.picasso.Picasso;

public class FriendsSearchAdpater extends BaseAdapter {

	ArrayList<FriendsDetail> arrFriends;
	String Data;
	LayoutInflater inflate1;
	discusschatinteface discussinterface;
	private Activity activity;

	ArrayList<FriendsDetail> BackupFriends;
	Context context;
	topBarInteface topInterface;
	private FriendSearchAdapterSendRequest friendsearchadapter;
	FragmentSliderSearchByNameEmail fragment;
	String msg = "";

	public FriendsSearchAdpater(Activity activity,
			ArrayList<FriendsDetail> arrFriends,
			FriendSearchAdapterSendRequest friendsearchadapter,
			FragmentSliderSearchByNameEmail fragment) {
		discussinterface = (discusschatinteface) activity;
		this.activity = activity;
		this.arrFriends = arrFriends;
		context = this.activity;
		this.BackupFriends = arrFriends;
		this.friendsearchadapter = friendsearchadapter;
		this.fragment = fragment;

	}

	@Override
	public String getItem(int position) {
		return arrFriends.get(position).freindsName;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public static class ViewHolder {
		public ImageView imgFriendPicture, imgAddFriendRequestIcon;
		public TextView txtFriendName, txt_msg;

	}

	public View getView(int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;
		final FriendsDetail friendsDetail = arrFriends.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_friend_search_list,
					null);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txt_msg = (TextView) convertView.findViewById(R.id.txt_msg);
		holder.txtFriendName = (TextView) convertView
				.findViewById(R.id.txtUsername);
		holder.imgFriendPicture = (ImageView) convertView
				.findViewById(R.id.imgProfilePicture);
		holder.imgAddFriendRequestIcon = (ImageView) convertView
				.findViewById(R.id.imgAddFriendRequestIcon);
		convertView.setTag(holder);
		holder.txtFriendName.setText(friendsDetail.freindsName);
		Picasso.with(activity).load(friendsDetail.profileImageName)
				.transform(new RoundedTransformation(18, 0))
				.into(holder.imgFriendPicture);

		if (!friendsDetail.status) {
			holder.imgAddFriendRequestIcon.setVisibility(View.GONE);
			holder.txt_msg.setVisibility(View.VISIBLE);
			holder.txt_msg.setText(friendsDetail.vMessage);
			msg = friendsDetail.vMessage;
		} else {
			holder.txt_msg.setVisibility(View.GONE);
			holder.imgAddFriendRequestIcon.setVisibility(View.VISIBLE);
		}

		holder.imgAddFriendRequestIcon
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						friendsDetail.status = false;
						friendsDetail.vMessage = msg;
						fragment.edtFriendsSearchEmailSearch.setText("");
						friendsearchadapter.sendRequest(friendsDetail);
//						arrFriends.remove(friendsDetail);
						notifyDataSetChanged();

					}
				});
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrFriends.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				FilterResults results = new FilterResults();

				if (charSequence == null || charSequence.length() == 0) {

					ArrayList<FriendsDetail> filterResultsData = new ArrayList<FriendsDetail>();

					for (int i = 0; i < BackupFriends.size(); i++) {
						FriendsDetail data = BackupFriends.get(i);

								filterResultsData.add(data);						

					}
					results.values = filterResultsData;
					results.count = filterResultsData.size();
				} else {

					ArrayList<FriendsDetail> filterResultsData = new ArrayList<FriendsDetail>();

					for (int i = 0; i < BackupFriends.size(); i++) {
						FriendsDetail data = BackupFriends.get(i);

						if (FragmentSliderSearchByNameEmail.strSearchByNameEmail
								.equals("searchByName")) {

							if (data.freindsName
									.toString()
									.toLowerCase()
									.contains(
											charSequence.toString()
													.toLowerCase())) {

								filterResultsData.add(data);
							}
						} else {

							if (data.friendEmailAddress.toString()
									.toLowerCase().contains(charSequence)
									|| data.friendEmailAddress.toString()
											.toUpperCase()
											.contains(charSequence)) {

								filterResultsData.add(data);
							}
						}

					}
					results.values = filterResultsData;
					results.count = filterResultsData.size();
				}

				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence charSequence,
					FilterResults filterResults) {
				arrFriends = (ArrayList<FriendsDetail>) filterResults.values;
				notifyDataSetChanged();
			}
		};
	}
}
