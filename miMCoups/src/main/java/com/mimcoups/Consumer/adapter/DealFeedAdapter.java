package com.mimcoups.Consumer.adapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.android.Facebook;
import com.google.android.gms.plus.PlusShare;
import com.mimcoups.Consumer.FacebookShare;
import com.mimcoups.Consumer.commonClass.CircleTransform;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.FontFitTextView;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.mimcoups.Consumer.inteface.shareingdeal;
import com.squareup.picasso.Picasso;

public class DealFeedAdapter extends BaseAdapter {

	ArrayList<DealFeedDetail> arrDeals;
	DealFeedDetailInterface dealInterface;
	String Data;
	LayoutInflater inflate1;
	private Context context;
	private int lastPosition = -1;
	long MIN_CLICK_INTERVAL = 300; // in millis
	long lastClickTime = 0;
	private CommonData globalClass;
	shareingdeal sharedeal;
	Dialog ImageDialog;
	AQuery aq;
	Button mimcoupsharing, btnemailsharing, facebookcontainer,
			twittercontainer, googlepluscontainer, instagamcontainer;
	String share = "";

	private File FileImage;
	DealFeedDetail feeddetails;

	private Facebook mFaceBook;
	private static final String[] PERMISSIONS = new String[] {
			"publish_stream", "read_stream", "offline_access" };

	public DealFeedAdapter(Activity context, ArrayList<DealFeedDetail> arrDeals) {
		this.arrDeals = arrDeals;
		dealInterface = (DealFeedDetailInterface) context;
		globalClass = (CommonData) context.getApplicationContext();
		mFaceBook = new Facebook(globalClass.FACEBOOK_APPID);
		sharedeal = (shareingdeal) context;
		this.context = context;
		aq = new AQuery(this.context);
		String dir = Environment.getExternalStorageDirectory() + File.separator
				+ "." + context.getResources().getString(R.string.app_name);
		FileImage = new File(dir, "2.jpg");
	}

	@Override
	public String getItem(int position) {
		return arrDeals.get(position).toString();
	}

	public class ViewHolder {
		public ImageView imgDealFeedImage;
		public TextView txtdealfeedtotalprice, txtDealFeedDiscoutPrice;
//		public View buyview;

		public TextView txtDealFeedName,
				txtDealFeedDistance,txtrow;
		public LinearLayout lnDealFeedShare, lnDealFeedDiscuss,
				lnDealFeedBuyNow;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final DealFeedDetail dealDetail = arrDeals.get(position);
		String price = "";
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_deel_feed_test, null);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.imgDealFeedImage = (ImageView) convertView
				.findViewById(R.id.imgDealFeedImage);
	holder.txtDealFeedName = (TextView) convertView
				.findViewById(R.id.txtDealFeedName);
//		holder.txtDealfeedType = (TextView) convertView
//				.findViewById(R.id.txtDealfeedType);
//		holder.txtDealfeedTypeDescription = (TextView) convertView
//				.findViewById(R.id.txtDealfeedTypeDescription);
//		holder.txtDealfeedDescription = (TextView) convertView
//				.findViewById(R.id.txtDealfeedDescription);
		holder.txtdealfeedtotalprice = (TextView) convertView
				.findViewById(R.id.txtdealfeedtotalprice);
		holder.txtDealFeedDiscoutPrice = (TextView) convertView
				.findViewById(R.id.txtDealFeedDiscoutPrice);
		holder.txtDealFeedDistance = (TextView) convertView
				.findViewById(R.id.txtDealFeedDistance);
		holder.lnDealFeedShare = (LinearLayout) convertView
				.findViewById(R.id.lnDealFeedShare);
		holder.lnDealFeedDiscuss = (LinearLayout) convertView
				.findViewById(R.id.lnDealFeedDiscuss);
		holder.lnDealFeedBuyNow = (LinearLayout) convertView
				.findViewById(R.id.lnDealFeedBuyNow);
		holder.txtrow=(TextView)convertView.findViewById(R.id.edtrow);

//		holder.buyview = (View) convertView.findViewById(R.id.rawbuy);

		//

		convertView.setTag(R.id.imgDealFeedImage, holder.imgDealFeedImage);
		convertView.setTag(R.id.edtrow, holder.txtrow);
		convertView.setTag(R.id.txtDealFeedName, holder.txtDealFeedName);
//		convertView.setTag(R.id.txtDealfeedType, holder.txtDealfeedType);
//		convertView.setTag(R.id.txtDealfeedTypeDescription,
//				holder.txtDealfeedTypeDescription);
//		convertView.setTag(R.id.txtDealfeedDescription,
//				holder.txtDealfeedDescription);
		convertView.setTag(R.id.txtdealfeedtotalprice,
				holder.txtdealfeedtotalprice);
		convertView.setTag(R.id.txtDealFeedDiscoutPrice,
				holder.txtDealFeedDiscoutPrice);
		convertView
				.setTag(R.id.txtDealFeedDistance, holder.txtDealFeedDistance);
		convertView.setTag(R.id.lnDealFeedShare, holder.lnDealFeedShare);
		convertView.setTag(R.id.lnDealFeedDiscuss, holder.lnDealFeedDiscuss);
		convertView.setTag(R.id.lnDealFeedBuyNow, holder.lnDealFeedBuyNow);

		convertView.setTag(holder);
		holder.txtDealFeedName.setText(dealDetail.StoreName);
//		holder.txtDealfeedType.setText(dealDetail.dealName);
//		holder.txtDealfeedTypeDescription
//				.setText(dealDetail.shortDescriptionHeading);
		// Log.d("Emnem deal", dealDetail.shortDescription);

		dealDetail.shortDescription = dealDetail.shortDescription.replace(
				"\\n", "\n");
		dealDetail.shortDescription = dealDetail.shortDescription.replace(">",
				"●");
		// Log.d("p56i", dealDetail.shortDescription);

//		holder.txtDealfeedDescription.setText(dealDetail.shortDescription);
		holder.txtdealfeedtotalprice.setText(dealDetail.DealOrignalPrice.trim()
				.toString());
		holder.txtDealFeedDiscoutPrice.setText(dealDetail.dealDiscountPrice
				.trim().toString());

		price = dealDetail.dealDiscountPrice.trim().toString();

		if (dealDetail.dealDiscountPrice.trim().toString().contains("$0") || dealDetail.dealDiscountPrice.trim().toString().contains("kr0")) {
			
			if(dealDetail.dealDiscountPrice.trim().toString().contains("$0")){
				
				price=dealDetail.dealDiscountPrice.trim().toString().replace("$", "").trim();
				
				
			}else if(dealDetail.dealDiscountPrice.trim().toString().contains("kr0")){
				price=dealDetail.dealDiscountPrice.trim().toString().replace("kr", "").trim();
				
			}
			
			if(Double.parseDouble(price)>0){
				
//				holder.buyview.setVisibility(View.VISIBLE);
				holder.lnDealFeedBuyNow.setVisibility(View.VISIBLE);
				
				
				holder.txtrow.setVisibility(View.VISIBLE);
				holder.txtdealfeedtotalprice.setVisibility(View.VISIBLE);
				
				
				holder.txtdealfeedtotalprice.setPaintFlags(holder.txtdealfeedtotalprice
						.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
				
			}else{
//				holder.buyview.setVisibility(View.GONE);
				holder.lnDealFeedBuyNow.setVisibility(View.GONE);

				holder.txtrow.setVisibility(View.GONE);
				holder.txtdealfeedtotalprice.setVisibility(View.GONE);
				
				holder.txtDealFeedDiscoutPrice.setText(context.getResources().getString(R.string.txtdealprice));
			}

			
			
			
		} else {
			
//			holder.buyview.setVisibility(View.VISIBLE);
			holder.lnDealFeedBuyNow.setVisibility(View.VISIBLE);
			
			
			holder.txtrow.setVisibility(View.VISIBLE);
			holder.txtdealfeedtotalprice.setVisibility(View.VISIBLE);
			
			
			holder.txtdealfeedtotalprice.setPaintFlags(holder.txtdealfeedtotalprice
					.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

		}
		holder.txtDealFeedDistance.setText(dealDetail.dealDistanse);

		// holder.txtDealFeedDiscoutPrice.setText("$ 165.00");
		// holder.txtdealfeedtotalprice.setText("$ 192.00");
		// holder.txtDealFeedDistance.setText(dealDetail.dealDistanse +
		// " "+context.getResources().getString(R.string.locallocationkm));


		// aq.id(holder.imgDealFeedImage)
		// .image(dealDetail.DealImageUrl);

		Picasso.with(context).load(dealDetail.DealImageUrl)
				.placeholder(R.drawable.noimage_deal)
				.into(holder.imgDealFeedImage);
		

		holder.lnDealFeedBuyNow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dealInterface.dealFeedClick("DealPurchase", dealDetail);

			}
		});

		holder.lnDealFeedShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// dealInterface.dealFeedClick("DealShare", dealDetail);


					Sharing_Dialog(dealDetail);
					ImageDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_PopUpMenu_Center;
					ImageDialog.show();

					mimcoupsharing.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							sharedeal.onsharedealclicked("mimcup", dealDetail);

							ImageDialog.dismiss();

						}
					});

					btnemailsharing.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {

							// Log.e("saving==", dealdetail.locationId);

							sharedeal.onsharedealclicked("mail", dealDetail);

							ImageDialog.dismiss();

						}
					});


			}
		});
		holder.lnDealFeedDiscuss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dealInterface.dealFeedClick("dealBuy", dealDetail);
			}
		});
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dealInterface.dealFeedClick("DealDetail", dealDetail);
			}
		});
		// Animation animation = AnimationUtils.loadAnimation(context,
		// (position > lastPosition) ? R.anim.up_from_bottom
		// : R.anim.down_from_top);
		// convertView.startAnimation(animation);
		lastPosition = position;
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrDeals.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private void Sharing_Dialog(final DealFeedDetail dealdetail) {


		try{

			ImageDialog = new Dialog(context);
			ImageDialog.setContentView(R.layout.share_white_vertical_dialog);
			ImageDialog.setCancelable(true);
			ImageDialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			int divierId = ImageDialog.getContext().getResources()
					.getIdentifier("android:id/titleDivider", null, null);
			View divider = ImageDialog.findViewById(divierId);
//			divider.setBackgroundColor(Color.TRANSPARENT);

			mimcoupsharing = (Button) ImageDialog
					.findViewById(R.id.btnmimcoupsharing);

			btnemailsharing = (Button) ImageDialog
					.findViewById(R.id.btnemailsharing);

			facebookcontainer = (Button) ImageDialog
					.findViewById(R.id.btnfacebookcontainer);
			twittercontainer = (Button) ImageDialog
					.findViewById(R.id.btntwittercontainer);
			googlepluscontainer = (Button) ImageDialog
					.findViewById(R.id.btngooglepluscontainer);
			instagamcontainer = (Button) ImageDialog
					.findViewById(R.id.btninstagramcontainer);

			Button btncancel = (Button) ImageDialog.findViewById(R.id.btnCancel);

			if (CommonData.languageId.equalsIgnoreCase("1")) {
				share = CommonData.dealshareUrl_au
						+ dealdetail.DealId;

			} else {
				share = CommonData.dealshareUrl_no
						+ dealdetail.DealId;
			}

			btncancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ImageDialog.dismiss();

				}
			});

			facebookcontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					ImageDialog.dismiss();

					Intent ii = new Intent(context,
							FacebookShare.class);
					ii.putExtra("dealname", dealdetail.dealName);
					ii.putExtra("desc", dealdetail.shortDescription);
					ii.putExtra("Image", share);
					((Activity) context).startActivity(ii);
					ImageDialog.dismiss();
				}
			});

			twittercontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					ImageDialog.dismiss();

					try {

						Log.e("cdeeel feedadapter", "" + dealdetail.DealImageUrl);
						Uri uri = Uri.fromFile(new File(dealdetail.DealImageUrl));
						Intent tweetIntent = new Intent(Intent.ACTION_SEND);
						tweetIntent.setType("*/*");
						// tweetIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						// dealdetail.dealName + "\n "
						// + dealdetail.shortDescription);

						tweetIntent.putExtra(android.content.Intent.EXTRA_TEXT,
								dealdetail.dealName + "\n " + share);
						tweetIntent.putExtra(android.content.Intent.EXTRA_STREAM,
								Uri.parse(share));
						// tweetIntent.setType("image/*");

						tweetIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

						PackageManager packManager = context.getPackageManager();
						List<ResolveInfo> resolvedInfoList = packManager
								.queryIntentActivities(tweetIntent,
										PackageManager.MATCH_DEFAULT_ONLY);

						boolean resolved = false;
						for (ResolveInfo resolveInfo : resolvedInfoList) {
							if (resolveInfo.activityInfo.packageName
									.startsWith("com.twitter.android")) {
								tweetIntent.setClassName(
										resolveInfo.activityInfo.packageName,
										resolveInfo.activityInfo.name);
								resolved = true;
								break;
							}
						}
						if (resolved) {
							context.startActivity(tweetIntent);
						} else {
							try {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("market://details?id=com.twitter.android")));
							} catch (android.content.ActivityNotFoundException anfe) {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android")));
							}
						}



					} catch (Exception e) {

					}

					ImageDialog.dismiss();
				}
			});

			googlepluscontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					ImageDialog.dismiss();

					Intent shareIntent = new PlusShare.Builder(context)
							.setType("text/plain")
							.setText("MiMCoups APP:: \n " + dealdetail.dealName

									+ "\n " + dealdetail.shortDescription)
							.setContentUrl(Uri.parse(share)).getIntent();
					((Activity) context).startActivityForResult(shareIntent, 0);

				}
			});

			instagamcontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Intent i = new Intent(context, InstaActivity.class);
					// i.putExtra("sharingfrom", 1);
					// context.startActivity(i);
					ImageDialog.dismiss();

					if (!appInstalledOrNot()) {
						Toast.makeText(
								context,
								context.getResources().getString(
										R.string.InstallInstagram)
										+ "", Toast.LENGTH_LONG).show();
						return;
					} else {

						feeddetails = dealdetail;

						new getbitmap().execute();


					}

				}
			});

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(ImageDialog.getWindow().getAttributes());
			DisplayMetrics displaymetrics = new DisplayMetrics();
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.BOTTOM;
			((Activity) context).getWindowManager().getDefaultDisplay()
					.getMetrics(displaymetrics);

			ImageDialog.getWindow().setAttributes(lp);

		}catch (Exception e){

			Log.e("Exceptions==",e.getMessage());

		}


	}

	public Bitmap getBitmapFromURL(String src) {
		try {
			URL url = new URL(src);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void convertFileToBitmap(Bitmap resized) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(FileImage);
			resized.compress(Bitmap.CompressFormat.PNG, 90, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (Throwable ignore) {
			}
		}
	}

	public class getbitmap extends AsyncTask<String, String, String> {
		Bitmap bm;

		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected String doInBackground(String... params) {
			bm = getBitmapFromURL(feeddetails.DealImageUrl);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			// imgDealImage.setImageBitmap(bm);
			convertFileToBitmap(bm);
			Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
			shareIntent.setType("image/*"); // set mime type
			// Uri path = Uri
			// .parse("android.resource://com.android.MimCoup/"
			// + R.drawable.app_icon);
			Uri path = Uri.parse("file:///" + FileImage.getPath());
			shareIntent.putExtra(Intent.EXTRA_TEXT, "MiMCoups APP:: \n "
					+ feeddetails.dealName

					+ "\n " + feeddetails.shortDescription + "\n" + share);
			shareIntent.putExtra(Intent.EXTRA_STREAM, path);
			shareIntent.setPackage("com.instagram.android");
			context.startActivity(shareIntent);

		}
	}

	private boolean appInstalledOrNot() {

		boolean app_installed = false;
		try {
			ApplicationInfo info = context.getPackageManager()
					.getApplicationInfo("com.instagram.android", 0);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

}
