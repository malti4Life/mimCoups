package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.QuestionList;
import com.mimcoups.Consumer.inteface.QuestionDetailInterface;

public class QuestionListAdapter extends BaseAdapter {

	ArrayList<QuestionList> arrQuestions;
	private LayoutInflater inflater = null;
	private QuestionDetailInterface questionDetail;
	long MIN_CLICK_INTERVAL = 1000; // in millis
	long lastClickTime = 0;

	public QuestionListAdapter(Context context,
			ArrayList<QuestionList> arrQuestions) {
		this.arrQuestions = arrQuestions;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		questionDetail = (QuestionDetailInterface) context;
	}

	@Override
	public String getItem(int position) {
		return arrQuestions.get(position).toString();
	}

	@Override
	public int getViewTypeCount() {
		try {
			int i = arrQuestions.size();
			return i;
		} catch (Exception e) {
			// TODO: handle exception
			return 0;
		}
	}

	@Override
	public int getCount() {
		int i;
		try {
			return arrQuestions.size();
		} catch (Exception e) {
			return 0;
		}
	}

	public long getItemId(int position) {

		return position;
	}

	public static class ViewHolder {
		public TextView txtQuestions, txtCategoryName;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		if (arrQuestions.size() != 0) {

			if (arrQuestions.get(position).isCategory) {
				return 0;
			} else {
				return 1;
			}
		}
		return 2;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		int i = position;
		final QuestionList question = arrQuestions.get(position);
		final ViewHolder holder;
		final int type = getItemViewType(position);
		try {

			if (convertView == null) {
				holder = new ViewHolder();
				if (type == 0) {
					convertView = inflater.inflate(
							R.layout.row_question_category_header, null);
				} else {
					convertView = inflater.inflate(
							R.layout.row_faq_questions_list, null);
				}
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			if (question.isCategory) {
				holder.txtCategoryName = (TextView) convertView
						.findViewById(R.id.txtCateogryName);
				convertView
						.setTag(R.id.txtCateogryName, holder.txtCategoryName);
			} else {
				holder.txtQuestions = (TextView) convertView
						.findViewById(R.id.txtQuestions);
				convertView.setTag(R.id.txtQuestions, holder.txtQuestions);
			}
			convertView.setTag(holder);
			if (question.isCategory) {
				holder.txtCategoryName.setText(question.categoryName.trim());
			} else {
				holder.txtQuestions.setText(question.questionName.trim());
			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (type != 0) {

					long currentTime = SystemClock.elapsedRealtime();
					if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
						lastClickTime = currentTime;

						questionDetail.questionDetailClick(question.questionId);

					}
				}
			}
		});
		return convertView;
	}

}