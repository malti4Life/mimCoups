package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.SearchLocationListCateogry;
import com.mimcoups.Consumer.fragment.FragmentSearch;
import com.mimcoups.Consumer.inteface.SearchResultInterface;

public class SearchListAdapter extends BaseAdapter {

	private ArrayList<SearchLocationListCateogry> arrSearchList;
	private LayoutInflater inflate1;
	private SearchResultInterface searchInteface;
	ArrayList<SearchLocationListCateogry> BackuparrSearchList;
	Context context;
	String searchfrom;
	long MIN_CLICK_INTERVAL = 1000; // in millis
	long lastClickTime = 0;

	public SearchListAdapter(Activity context,
			ArrayList<SearchLocationListCateogry> arrSearchList,
			String searchfrom) {
		this.context = context;
		this.arrSearchList = arrSearchList;
		this.BackuparrSearchList = arrSearchList;
		this.searchfrom = searchfrom;
		searchInteface = (SearchResultInterface) context;
	}

	@Override
	public String getItem(int position) {
		return arrSearchList.get(position).toString();
	}

	public static class ViewHolder {
		public ImageView imgSearchLocationIcon;
		public TextView txtSearchLocationCatgoryName;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;

		final SearchLocationListCateogry searchList = arrSearchList
				.get(position);

		if (convertView == null) {

			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_search_list, null);
			holder.imgSearchLocationIcon = (ImageView) convertView
					.findViewById(R.id.imgSearchLocationIcon);
			holder.txtSearchLocationCatgoryName = (TextView) convertView
					.findViewById(R.id.txtSearchLocationCatgoryName);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.txtSearchLocationCatgoryName.setText(searchList.vKeywordName);

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				long currentTime = SystemClock.elapsedRealtime();
				if (currentTime - lastClickTime > MIN_CLICK_INTERVAL) {
					lastClickTime = currentTime;
					FragmentSearch.issearch = false;

					FragmentSearch.edtSearchSearch
							.setText(searchList.vKeywordName);

					InputMethodManager imm = (InputMethodManager) context
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(
							FragmentSearch.edtSearchSearch.getWindowToken(), 0);
					searchInteface.searchDetail(searchfrom,
							searchList.vKeywordName);

				}

			}
		});

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrSearchList.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Filter getFilter() {
		return new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence charSequence) {
				
				

				FilterResults results = new FilterResults();

				if (charSequence == null || charSequence.length() == 0) {

					results.values = BackuparrSearchList;
					results.count = BackuparrSearchList.size();
				} else {

					ArrayList<SearchLocationListCateogry> filterResultsData = new ArrayList<SearchLocationListCateogry>();

					for (int i = 0; i < BackuparrSearchList.size(); i++) {
						SearchLocationListCateogry data = BackuparrSearchList
								.get(i);

						if (data.vKeywordName
								.toString()
								.toLowerCase()
								.contains(charSequence.toString().toLowerCase())) {

							filterResultsData.add(data);
						}

					}
					results.values = filterResultsData;
					results.count = filterResultsData.size();
				}

				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence charSequence,
					FilterResults filterResults) {
				arrSearchList = (ArrayList<SearchLocationListCateogry>) filterResults.values;

				notifyDataSetChanged();
			}
		};
	}

}
