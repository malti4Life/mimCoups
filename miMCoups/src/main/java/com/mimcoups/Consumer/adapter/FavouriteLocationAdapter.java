package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.inteface.LocationListInterface;
import com.mimcoups.Consumer.inteface.favouriteLocationInteface;
import com.squareup.picasso.Picasso;

public class FavouriteLocationAdapter extends BaseAdapter {

	ArrayList<LocationList> arrLocation;
	private LocationListInterface locationListInterface;
	String Data;
	LayoutInflater inflate1;
	private favouriteLocationInteface favouriteLocation;
	private Context context;

	Activity activity;

	public FavouriteLocationAdapter(Activity activity,
			ArrayList<LocationList> arrLocation,
			favouriteLocationInteface favouriteLocation) {
		this.activity = activity;
		locationListInterface = (LocationListInterface) activity;
		this.arrLocation = arrLocation;
		this.favouriteLocation = favouriteLocation;
	}

	@Override
	public String getItem(int position) {
		return arrLocation.get(position).toString();
	}

	public static class ViewHolder {
		public ImageView imgLocationImage;
		public CheckBox cbFavourite;
		public TextView txtDealName, txtLocationDistance, txtLocationDeals,
				txtDealDiscription;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final LocationList locationlist = arrLocation.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_location_favourite,
					null);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.imgLocationImage = (ImageView) convertView
				.findViewById(R.id.imgLocationImage_row_location);

		holder.txtDealName = (TextView) convertView
				.findViewById(R.id.txtDealName);

		holder.txtLocationDistance = (TextView) convertView
				.findViewById(R.id.txtLocationDistance);

		holder.txtLocationDeals = (TextView) convertView
				.findViewById(R.id.txtLocationDeals);

		holder.txtDealDiscription = (TextView) convertView
				.findViewById(R.id.txtDealDiscription);

		holder.cbFavourite = (CheckBox) convertView
				.findViewById(R.id.cbFavourite);

		// holder.cbFavourite.setChecked(true);
		convertView.setTag(holder);

		Picasso.with(context).load(locationlist.vStoreListImage_A_70X70)
				.placeholder(R.drawable.app_icon).into(holder.imgLocationImage);

		holder.txtDealDiscription.setText(locationlist.vShortDescription);
		holder.txtLocationDistance.setText(locationlist.dDistance);
		holder.txtDealName.setText(locationlist.vMerchantStoreName);
		holder.txtLocationDeals.setText(locationlist.iDealCount);

		holder.cbFavourite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				

				favouriteLocation.favouriteLocationClick(
						locationlist.iMerchantStoreId, holder.cbFavourite,
						false,locationlist);
			}
		});

		

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				locationListInterface.locationDetail(locationlist,"list");
			}
		});

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrLocation.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
