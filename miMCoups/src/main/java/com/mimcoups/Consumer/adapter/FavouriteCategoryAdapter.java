package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.mimcoups.Consumer.inteface.categoryDetailInterface;
import com.mimcoups.Consumer.inteface.favouriteCategoryInteface;
import com.squareup.picasso.Picasso;

public class FavouriteCategoryAdapter extends BaseAdapter {

	ArrayList<CategoryDetail> arrCategory;
	String Data;
	LayoutInflater inflate1;
	categoryDetailInterface infCategroyDetail;
	private Context context;
	private favouriteCategoryInteface favouriteCategory;
	private int lastPosition = 0;
	Activity activity;

	public FavouriteCategoryAdapter(Activity activity,
			ArrayList<CategoryDetail> arrCategory,
			favouriteCategoryInteface favouriteCategory) {
		this.activity = activity;
		this.arrCategory = arrCategory;
		infCategroyDetail = (categoryDetailInterface) activity;
		context = activity;
		this.favouriteCategory = favouriteCategory;
	}

	@Override
	public String getItem(int position) {
		return arrCategory.get(position).toString();
	}

	public static class ViewHolder {
		public RelativeLayout rlCategoryBackground;
		public ImageView imgCategoryIcon, imgCategoryFavourite;
		public TextView txtCateogryName;
		public CheckBox cbFavourite;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final CategoryDetail categroyDetail = arrCategory.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_category_favourite,
					null);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.rlCategoryBackground = (RelativeLayout) convertView
				.findViewById(R.id.rlCategoryBackground);

		holder.imgCategoryIcon = (ImageView) convertView
				.findViewById(R.id.imgCategoryIcon);

		holder.txtCateogryName = (TextView) convertView
				.findViewById(R.id.txtCategoryName);
		holder.cbFavourite = (CheckBox) convertView
				.findViewById(R.id.cbcategoryFavourite);
		convertView.setTag(holder);
		
		try {
		
			holder.rlCategoryBackground.setBackgroundColor(Color
					.parseColor(categroyDetail.categoryBackGround));
		} catch (Exception e) {
			// TODO: handle exception
		}

		
		Picasso.with(context).load(categroyDetail.vCategoryImage_A_90X90)
				.into(holder.imgCategoryIcon);
		holder.txtCateogryName.setText(categroyDetail.categoryName);
		// holder.cbFavourite.setChecked(categroyDetail.CategoryFavourite);

		holder.cbFavourite.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				favouriteCategory.favouriteCategoryClick(
						categroyDetail.categoryId, holder.cbFavourite, false,position);

			}
		});

	

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				infCategroyDetail.categoryClick(categroyDetail.categoryId,
						categroyDetail.categoryName);
			}
		});

		
		return convertView;
	}

	@Override
	public int getCount() {
		return arrCategory.size();
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}
}
