package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.customClass.ChatUser;
import com.mimcoups.Consumer.inteface.discusschatinteface;
import com.squareup.picasso.Picasso;

public class ChatListSingleAdapter extends BaseAdapter {

	String Data;
	LayoutInflater inflate1;
	Context context;
	ArrayList<ChatUser> arrchat;
	CommonData globalClass;
	String to_profileurl;
	Bitmap frompic, topic;
	discusschatinteface dealInterface;
	SharedPreferences preference;
	discusschatinteface discussinterface;
	int notificationcount = 0;

	public ChatListSingleAdapter(Activity activity, ArrayList<ChatUser> arrchat) {
		this.context = activity;
		this.arrchat = arrchat;
		globalClass = (CommonData) context.getApplicationContext();

		dealInterface = (discusschatinteface) context;
		preference = activity.getSharedPreferences("FriendList",
				Context.MODE_PRIVATE);

	}

	@Override
	public String getItem(int position) {
		return null;
	}

	public class ViewHolder {
		public ImageView imgProfilePicture, imgChatListOnlineOfflineIcon;
		public TextView txtUsername;
		public TextView badgecount;
		public LinearLayout badgecontainer;

	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return position;

	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_chat_onlinefriends,
					null);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.badgecontainer = (LinearLayout) convertView
				.findViewById(R.id.badgecontainer);
		holder.imgChatListOnlineOfflineIcon = (ImageView) convertView
				.findViewById(R.id.imgChatListOnlineOfflineIcon);
		holder.imgProfilePicture = (ImageView) convertView
				.findViewById(R.id.imgProfilePicture);
		holder.txtUsername = (TextView) convertView
				.findViewById(R.id.txtUsername);
		holder.badgecount = (TextView) convertView
				.findViewById(R.id.badgecount);

		convertView.setTag(R.id.badgecontainer, holder.badgecontainer);
		convertView.setTag(R.id.imgProfilePicture, holder.imgProfilePicture);
		convertView.setTag(R.id.txtUsername, holder.txtUsername);
		convertView.setTag(R.id.badgecount, holder.badgecount);
		convertView.setTag(R.id.imgChatListOnlineOfflineIcon,
				holder.imgChatListOnlineOfflineIcon);

		convertView.setTag(holder);

		holder.txtUsername.setText(arrchat.get(position).customername);

		String username = arrchat.get(position).userName;
		username = username.toLowerCase();

		final int notificationcount = preference.getInt(username, 0);

		if (notificationcount == 0) {
			holder.badgecontainer.setVisibility(View.GONE);
		} else {
			holder.badgecontainer.setVisibility(View.VISIBLE);
			holder.badgecount.setText("" + notificationcount);
		}

		Log.i("ADAPTER ONLINE USERNAME>>>>", ""
				+ arrchat.get(position).userName + "  ->   "
				+ notificationcount);

		String profileurl = arrchat.get(position).profileUrl;

		if (arrchat.get(position).islonlie) {

			int sdk = android.os.Build.VERSION.SDK_INT;
			if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				holder.imgChatListOnlineOfflineIcon
						.setBackgroundDrawable(context.getResources()
								.getDrawable(R.drawable.icon_online));
			} else {
				holder.imgChatListOnlineOfflineIcon.setBackground(context
						.getResources().getDrawable(R.drawable.icon_online));
			}

		} else {
			int sdk = android.os.Build.VERSION.SDK_INT;
			if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
				holder.imgChatListOnlineOfflineIcon
						.setBackgroundDrawable(context.getResources()
								.getDrawable(R.drawable.icon_offline));
			} else {
				holder.imgChatListOnlineOfflineIcon.setBackground(context
						.getResources().getDrawable(R.drawable.icon_offline));
			}
		}

		Picasso.with(context).load(profileurl)
				.transform(new RoundedTransformation(18, 0))
				.into(holder.imgProfilePicture);

		convertView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				preference = context.getSharedPreferences("Badgecount",
						Context.MODE_PRIVATE);

				int badgecountval = preference.getInt("Badge", 0);


				badgecountval = badgecountval - notificationcount;

				preference.edit().putInt("Badge", badgecountval).commit();
				Log.e("Badge Count===", "" + badgecountval);

				String tag = v.getTag().toString();
				// Log.d("position>>", "" + position);
				// Log.d("array size>>", "" + arrchat.size());
				ChatUser ch = arrchat.get(position);
				dealInterface.discussOneToOneChatClick(ch);

			}
		});

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrchat.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
