package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.LocationList;
import com.mimcoups.Consumer.customClass.SearchResultLocationDealCategory;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.mimcoups.Consumer.inteface.LocationListInterface;
import com.mimcoups.Consumer.inteface.SearchMapLocationDealFeedInterface;
import com.squareup.picasso.Picasso;

public class SearchResultLocationListAdapter extends BaseAdapter {

	ArrayList<SearchResultLocationDealCategory> arrSearchMapLocations;
	String Data;
	LayoutInflater inflate1;
	private SearchMapLocationDealFeedInterface searchMapInterface;
	Context c;
	String searchfrom;
	LocationListInterface locationnlistinterface;
	DealFeedDetailInterface dealfeeddetailinterface;
	public static int selected = 0;

	public SearchResultLocationListAdapter(Activity activity,
			ArrayList<SearchResultLocationDealCategory> arrSearchMapLocations,
			String searchfrom, LocationListInterface locationnlistinterface,
			DealFeedDetailInterface dealfeeddetailinterface) {
		searchMapInterface = (SearchMapLocationDealFeedInterface) activity;
		this.arrSearchMapLocations = arrSearchMapLocations;
		this.searchfrom = searchfrom;
		this.c = activity;
		this.locationnlistinterface = locationnlistinterface;
		this.dealfeeddetailinterface = dealfeeddetailinterface;
	}

	@Override
	public String getItem(int position) {
		return arrSearchMapLocations.get(position).toString();
	}

	public static class ViewHolder {
		public TextView txtPinName, txtLocationName, txtDealPrice;
		public ImageView imgStoreLogo;
		RelativeLayout mainrel;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final SearchResultLocationDealCategory searchMap = arrSearchMapLocations
				.get(position);

		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_search_result_location,
					null);

			holder.mainrel = (RelativeLayout) convertView
					.findViewById(R.id.mainrel);

			holder.txtPinName = (TextView) convertView
					.findViewById(R.id.txtPinName);
			holder.txtLocationName = (TextView) convertView
					.findViewById(R.id.txtLocationName);
			holder.txtDealPrice = (TextView) convertView
					.findViewById(R.id.txtDealPrice);
			holder.imgStoreLogo = (ImageView) convertView
					.findViewById(R.id.imgStoreLogo);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		if (selected == position) {
			holder.mainrel.setBackgroundColor(Color.parseColor("#D9D5CD"));
		} else {
			holder.mainrel.setBackgroundColor(Color.parseColor("#FFFFFF"));

		}
//		Log.e("Pinlocation====", searchMap.pinLocation);
//		int j = Integer.valueOf(searchMap.pinLocation) + (65);
		holder.txtPinName.setText(Character.toString((char) (65+position)));
		holder.txtLocationName.setText(searchMap.vName);
		
			Picasso.with(c).load(searchMap.vMerchantStoreMapImage_A_90X90).placeholder(R.drawable.noimage_deal).error(R.drawable.noimage_deal)
					.into(holder.imgStoreLogo);
			
			
			
			
			
			if (searchMap.dDealDiscountedAmount.trim().toString().contains("$0") || searchMap.dDealDiscountedAmount.trim().toString().contains("kr0")) {
				
				String price="";
				
				if(searchMap.dDealDiscountedAmount.trim().toString().contains("$0")){
					
					price=searchMap.dDealDiscountedAmount.trim().toString().replace("$", "").trim();
					
					
				}else if(searchMap.dDealDiscountedAmount.trim().toString().contains("kr0")){
					price=searchMap.dDealDiscountedAmount.trim().toString().replace("kr", "").trim();
					
				}
				
				if(Double.parseDouble(price)>0){
					holder.txtDealPrice.setText(searchMap.dDealDiscountedAmount);
				
				}else{
					holder.txtDealPrice.setText(c.getResources().getString(R.string.txtnotapplicable));	
				}
			}else{
				holder.txtDealPrice.setText(searchMap.dDealDiscountedAmount);
			}
		

		

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				searchMapInterface.searchmapLocationDeal("DealFeeds");
			}
		});

		convertView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (searchfrom.equals("store")) {
					LocationList list = new LocationList();
					list.iMerchantStoreId = searchMap.iMerchantStoreId;
					locationnlistinterface.locationDetail(list, "list");
				} else {
					DealFeedDetail dealfeedddetail = new DealFeedDetail();
					dealfeedddetail.categoryId = searchMap.iCategoryId;
					dealfeedddetail.DealId = searchMap.iMerchantDealId;
					dealfeedddetail.locationId = searchMap.iMerchantStoreId;
					dealfeeddetailinterface.dealFeedClick("DealDetail",
							dealfeedddetail);
				}

			}
		});

		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrSearchMapLocations.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
