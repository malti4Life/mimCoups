package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.fragment.FragmentDealShare;
import com.mimcoups.Consumer.fragment.FragmentLocationShare;
import com.mimcoups.Consumer.inteface.discusschatinteface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.squareup.picasso.Picasso;

public class AlertFriendlistAdapter extends BaseAdapter {

	ArrayList<FriendsDetail> arrFriends;
	LayoutInflater inflate1;
	discusschatinteface discussinterface;
	private Activity activity;

	topBarInteface topInterface;
	String loadfrom;

	public AlertFriendlistAdapter(Activity activity,
			ArrayList<FriendsDetail> arrFriends, String loadfrom) {
		discussinterface = (discusschatinteface) activity;
		this.activity = activity;
		this.arrFriends = arrFriends;
		this.loadfrom = loadfrom;

	}

	@Override
	public String getItem(int position) {
		return arrFriends.get(position).freindsName;
	}

	@Override
	public int getItemViewType(int position) {
		return 0;

	}

	public static class ViewHolder {
		public ImageView imgFriendPicture, imgAddFriendRequestIcon;
		public TextView txtFriendName;

	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		final ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1
					.inflate(R.layout.row_alert_friend_list, null);

			holder.txtFriendName = (TextView) convertView
					.findViewById(R.id.txtUsername);
			holder.imgFriendPicture = (ImageView) convertView
					.findViewById(R.id.imgProfilePicture);
			holder.imgAddFriendRequestIcon = (ImageView) convertView
					.findViewById(R.id.imgAddFriendRequestIcon);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// //setdata

		final FriendsDetail friendsDetail = arrFriends.get(position);
		holder.txtFriendName.setText(friendsDetail.freindsName);
		Picasso.with(activity).load(friendsDetail.profileImageName)
				.transform(new RoundedTransformation(18, 0))
				.into(holder.imgFriendPicture);
		if (friendsDetail.isadd) {
			holder.imgAddFriendRequestIcon.setTag("remove");
			holder.imgAddFriendRequestIcon
					.setImageResource(R.drawable.icon_true);
		} else {
			holder.imgAddFriendRequestIcon.setTag("add");
			holder.imgAddFriendRequestIcon
					.setImageResource(R.drawable.icon_blueplus);
		}

		holder.imgAddFriendRequestIcon
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {

						if (holder.imgAddFriendRequestIcon.getTag().equals(
								"add")) {
							holder.imgAddFriendRequestIcon.setTag("remove");
							holder.imgAddFriendRequestIcon
									.setImageResource(R.drawable.icon_true);
							friendsDetail.isadd = true;
							arrFriends.set(position, friendsDetail);
							notifyDataSetChanged();

							if (loadfrom.equals("location")) {

								FragmentLocationShare.ListofFriendsEmailtoshare
										.add(friendsDetail);

							}
							if (loadfrom.equals("deal")) {

								FragmentDealShare.ListofFriendsEmailtoshare
										.add(friendsDetail);
							}

						} else {
							holder.imgAddFriendRequestIcon.setTag("add");
							holder.imgAddFriendRequestIcon
									.setImageResource(R.drawable.icon_blueplus);
							friendsDetail.isadd = false;
							arrFriends.set(position, friendsDetail);
							notifyDataSetChanged();

							if (loadfrom.equals("location")) {
								int pos1 = getpostionofindexlocation(friendsDetail.friendEmailAddress);
								FragmentLocationShare.ListofFriendsEmailtoshare
										.remove(pos1);

							}

							if (loadfrom.equals("deal")) {

								int pos = getpostionofindexdeal(friendsDetail.friendEmailAddress);
								FragmentDealShare.ListofFriendsEmailtoshare
										.remove(pos);

							}

						}

					}

				});
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrFriends.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private int getpostionofindexlocation(String email) {

		for (int i = 0; i < FragmentLocationShare.ListofFriendsEmailtoshare
				.size(); i++) {

			if (FragmentLocationShare.ListofFriendsEmailtoshare.get(i).friendEmailAddress
					.equals(email)) {

				return i;
			}

		}

		return 0;
	}

	private int getpostionofindexdeal(String email) {

		for (int i = 0; i < FragmentDealShare.ListofFriendsEmailtoshare.size(); i++) {

			if (FragmentDealShare.ListofFriendsEmailtoshare.get(i).friendEmailAddress
					.equals(email)) {

				return i;
			}

		}

		return 0;
	}

}