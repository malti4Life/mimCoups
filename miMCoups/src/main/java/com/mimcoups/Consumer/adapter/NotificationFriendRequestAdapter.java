package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.R;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.squareup.picasso.Picasso;

public class NotificationFriendRequestAdapter extends BaseAdapter {

	Context context;
	ArrayList<FriendsDetail> arrFriends;
	LayoutInflater inflate1;
	topBarInteface topInterface;
	CommonData commonClass;
	private String statusCode = "", statusMessage = "";

	private WebService_Call webservicecall = new WebService_Call();
	private String serverResponse = "";

	public NotificationFriendRequestAdapter(Activity activity,
			ArrayList<FriendsDetail> arrFriends) {

		context = activity;
		this.arrFriends = arrFriends;
		topInterface = (topBarInteface) activity;
		commonClass = (CommonData) context.getApplicationContext();

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrFriends.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public class ViewHolder {
		public ImageView imgProfilePicture, imgOnlineOfflineIcon,
				imgrejctrequest, imgacceptrequest;
		public TextView txtUsername;
		public RelativeLayout layoutMain;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		final ViewHolder holder;
		final FriendsDetail friendsdetail = arrFriends.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(
					R.layout.row_notification_friendrequest, null);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.imgProfilePicture = (ImageView) convertView
				.findViewById(R.id.imgProfilePicture);
		holder.layoutMain = (RelativeLayout) convertView
				.findViewById(R.id.layoutmain);

		holder.txtUsername = (TextView) convertView
				.findViewById(R.id.txtUsername);

		holder.imgOnlineOfflineIcon = (ImageView) convertView
				.findViewById(R.id.imgOnlineOfflineIcon);

		holder.imgrejctrequest = (ImageView) convertView
				.findViewById(R.id.imgrejctrequest);

		holder.imgacceptrequest = (ImageView) convertView
				.findViewById(R.id.imgacceptrequest);

		Picasso.with(context).load(friendsdetail.profileImageName)
				.into(holder.imgProfilePicture);

		holder.txtUsername.setText(friendsdetail.freindsName);

		if (friendsdetail.vstatus.equalsIgnoreCase("UnRead")) {
			holder.layoutMain.setBackgroundColor(Color.parseColor("#50808080"));
		} else {
			holder.layoutMain.setBackgroundColor(Color.WHITE);
		}

		holder.imgrejctrequest.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				async_deel_feed_list as = new async_deel_feed_list(
						new asynctaskloaddata() {

							String vLocationName = "";

							@Override
							public void onPreExecute() {
								// TODO Auto-generated method stub
								topInterface.showDialog("Loading");
							}

							@Override
							public void doInBackground() {
								// TODO Auto-generated method stub

								String serverUrl = "FriendService.svc/RejectFriendRequest?languageid="
										+ commonClass.languageId
										+ "&customerrecievednotificationid="
										+ friendsdetail.iCustomerRecievedNotificationId;

								serverResponse = webservicecall
										.makeServicegetRequest(serverUrl);
								// Log.e("Friends Reject", serverUrl
								// + "\n Response====" + serverResponse);
								try {
									JSONObject js_getres = new JSONObject(
											serverResponse);
									statusCode = js_getres
											.getString("vStatusCode");
									statusMessage = js_getres
											.getString("vMessageResponse");

								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onPostExecute() {
								// TODO Auto-generated method stub
								topInterface.hideDialog();

								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topInterface
															.topbarClick("back");
												}
											},
											context.getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(((Activity) context)
											.getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {

									((DeelFeedActivity) context).IsFriendRequest = true;

									arrFriends.remove(friendsdetail);
									notifyDataSetChanged();

									if (arrFriends.size() == 0) {
										DialogFragment ds = new SingleButtonAlert(
												new DialogInterfaceClick() {

													@Override
													public void dialogClick(
															String tag) {
														// TODO Auto-generated
														// method
														// stub

														((DeelFeedActivity) ((Activity) context)).commonClass
																.getObserver()
																.setValue(
																		"updatenotificationsettings");
														// ((DeelFeedActivity)
														// getActivity()).commonClass.getObserver()
														// .setValue("updatenotificationsettings");

														((Activity) context)
																.getFragmentManager()
																.popBackStack(
																		null,
																		FragmentManager.POP_BACK_STACK_INCLUSIVE);

													}
												},
												((Activity) context)
														.getResources()
														.getString(
																R.string.NoFriendsMsg));
										ds.show(((Activity) context)
												.getFragmentManager(), "");
										ds.setCancelable(false);
									}

								} else {
									// Toast.makeText(context, "" +
									// statusMessage,
									// 9000).show();
								}

							}
						});
				as.execute();

			}
		});

		holder.imgacceptrequest.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				async_deel_feed_list as = new async_deel_feed_list(
						new asynctaskloaddata() {

							String vLocationName = "";

							@Override
							public void onPreExecute() {
								// TODO Auto-generated method stub
								topInterface.showDialog("Loading");
							}

							@Override
							public void doInBackground() {
								// TODO Auto-generated method stub

								String serverUrl = "FriendService.svc/AcceptFriendRequest?languageid="
										+ commonClass.languageId
										+ "&customerrecievednotificationid="
										+ friendsdetail.iCustomerRecievedNotificationId;

								serverResponse = webservicecall
										.makeServicegetRequest(serverUrl);

								// Log.e("Friends request", serverUrl
								// + "\n Response====" + serverResponse);
								try {
									JSONObject js_getres = new JSONObject(
											serverResponse);
									statusCode = js_getres
											.getString("vStatusCode");
									statusMessage = js_getres
											.getString("vMessageResponse");

								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}

							@Override
							public void onPostExecute() {
								// TODO Auto-generated method stub
								topInterface.hideDialog();
								if (statusCode.equals("420")) {
									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

													topInterface
															.topbarClick("back");
												}
											},
											context.getResources()
													.getString(
															R.string.PoorInternetConnect));
									ds.show(((Activity) context)
											.getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("100")) {
									// Toast.makeText(context, "" +
									// statusMessage,
									// 9000).show();

									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													((DeelFeedActivity) context).IsFriendRequest = true;

													arrFriends
															.remove(friendsdetail);
													// notifyDataSetInvalidated();
													notifyDataSetChanged();

													if (arrFriends.size() == 0) {
														DialogFragment ds = new SingleButtonAlert(
																new DialogInterfaceClick() {

																	@Override
																	public void dialogClick(
																			String tag) {

																		topInterface
																				.topbarClick("back");
																	}
																},
																((Activity) context)
																		.getResources()
																		.getString(
																				R.string.NoFriendsMsg));
														ds.show(((Activity) context)
																.getFragmentManager(),
																"");
														ds.setCancelable(false);
													}
												}
											}, statusMessage);
									ds.show(((Activity) context)
											.getFragmentManager(), "");
									ds.setCancelable(false);

								} else if (statusCode.equals("116")) {

									DialogFragment ds = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {
													//
													// topInterface
													// .topbarClick("back");
												}
											}, statusMessage);
									ds.show(((Activity) context)
											.getFragmentManager(), "");
									ds.setCancelable(false);

								}

							}
						});
				as.execute();

			}
		});

		return convertView;
	}
}
