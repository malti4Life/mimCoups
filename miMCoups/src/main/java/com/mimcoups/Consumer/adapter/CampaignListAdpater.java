package com.mimcoups.Consumer.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.CampaignDetail;

public class CampaignListAdpater extends BaseAdapter {

	ArrayList<CampaignDetail> arrCampaign;
	String Data;
	LayoutInflater inflate1;
	private int lastPosition = 0;

	public CampaignListAdpater(ArrayList<CampaignDetail> arrCampaign) {
		this.arrCampaign = arrCampaign;
	}

	@Override
	public String getItem(int position) {
		return arrCampaign.get(position).toString();
	}

	public static class ViewHolder {
		public TextView txtRowCampaignCount, txtRowCampaignUserName,
				txtRowCampaignDate,txtRowCampaignexpireDate;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		CampaignDetail campaign = arrCampaign.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_slider_campaign, null);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.txtRowCampaignCount = (TextView) convertView
				.findViewById(R.id.txtRowCampaignCount);
		holder.txtRowCampaignUserName = (TextView) convertView
				.findViewById(R.id.txtRowCampaignUserName);
		holder.txtRowCampaignDate = (TextView) convertView
				.findViewById(R.id.txtRowCampaignDate);
		
		holder.txtRowCampaignexpireDate = (TextView) convertView
				.findViewById(R.id.txtRowCampaignexpireDate);

		convertView
				.setTag(R.id.txtRowCampaignCount, holder.txtRowCampaignCount);
		convertView.setTag(R.id.txtRowCampaignUserName,
				holder.txtRowCampaignUserName);
		convertView.setTag(R.id.txtRowCampaignDate, holder.txtRowCampaignDate);
		convertView.setTag(holder);
		holder.txtRowCampaignUserName.setText(campaign.vCustomerFriendEmail);
		holder.txtRowCampaignCount.setText(campaign.dRewardAmount);
		holder.txtRowCampaignDate.setText(campaign.dConvertedDate);
		holder.txtRowCampaignexpireDate .setText(campaign.dRewardUseLimitDate);
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

			}
		});
		// Animation animation =
		// AnimationUtils.loadAnimation(parent.getContext(),
		// (position > lastPosition) ? R.anim.up_from_bottom
		// : R.anim.down_from_top);
		// convertView.startAnimation(animation);
		lastPosition = position;
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrCampaign.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

}
