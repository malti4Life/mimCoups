package com.mimcoups.Consumer.adapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.Facebook;
import com.google.android.gms.plus.PlusShare;
import com.mimcoups.Consumer.FacebookShare;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.R;
import com.mimcoups.Consumer.RoundedTransformation;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.customClass.DealFeedDetail;
import com.mimcoups.Consumer.customClass.FontFitTextView;
import com.mimcoups.Consumer.inteface.DealFeedDetailInterface;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.shareingdeal;
import com.squareup.picasso.Picasso;

public class FriendPurchaseDealFeedAdapter extends BaseAdapter {

	ArrayList<DealFeedDetail> arrFriendPurhcase;
	DealFeedDetailInterface dealInterface;
	String Data;
	LayoutInflater inflate1;
	private Activity activity;
	Context context;
	private CommonData globalClass;
	shareingdeal sharedeal;
	Dialog ImageDialog;
	String share = "";
	Button mimcoupsharing, btnemailsharing, facebookcontainer,
			twittercontainer, googlepluscontainer, instagamcontainer;
	private Facebook mFaceBook;
	private static final String[] PERMISSIONS = new String[] {
			"publish_stream", "read_stream", "offline_access" };

	public FriendPurchaseDealFeedAdapter(Activity activity,
			ArrayList<DealFeedDetail> arrFriendPurhcase) {
		this.activity = activity;
		this.context = activity;
		dealInterface = (DealFeedDetailInterface) activity;
		this.arrFriendPurhcase = arrFriendPurhcase;

		globalClass = (CommonData) context.getApplicationContext();
		mFaceBook = new Facebook(globalClass.FACEBOOK_APPID);
		sharedeal = (shareingdeal) context;
	}

	@Override
	public String getItem(int position) {
		return arrFriendPurhcase.get(position).toString();
	}

	public static class ViewHolder {
		public ImageView imgDealFeedImage, imgFriendImage;
		public TextView txtFriendName, txtDealName, txtDealTypeDiscription,
				txtDealDetail, txtDealTime;
		FontFitTextView txtdealfeeddiscountprice;
		public View buyview;

		public LinearLayout lnFriendShare, lnFriendDiscuss, lnFriendBuyNow;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 0;

	}

	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final DealFeedDetail dealDetail = arrFriendPurhcase.get(position);
		if (convertView == null) {
			holder = new ViewHolder();
			inflate1 = (LayoutInflater) parent.getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflate1.inflate(R.layout.row_friend_purchase_deal,
					null);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.imgDealFeedImage = (ImageView) convertView
				.findViewById(R.id.imgDealFeedImage);
		holder.imgFriendImage = (ImageView) convertView
				.findViewById(R.id.imgFriendImage);
		holder.txtDealTime = (TextView) convertView
				.findViewById(R.id.txtDealTime);

		holder.txtFriendName = (TextView) convertView
				.findViewById(R.id.txtFriendName);
		holder.txtDealName = (TextView) convertView
				.findViewById(R.id.txtDealName);
		holder.txtDealTypeDiscription = (TextView) convertView
				.findViewById(R.id.txtDealTypeDiscription);
		holder.txtDealDetail = (TextView) convertView
				.findViewById(R.id.txtDealDetail);
		holder.buyview = (View) convertView.findViewById(R.id.rawbuy);

		holder.txtdealfeeddiscountprice = (FontFitTextView) convertView
				.findViewById(R.id.txtdealfeeddiscountprice);

		holder.lnFriendShare = (LinearLayout) convertView
				.findViewById(R.id.lnFriendShare);
		holder.lnFriendDiscuss = (LinearLayout) convertView
				.findViewById(R.id.lnFriendDiscuss);

		holder.lnFriendBuyNow = (LinearLayout) convertView
				.findViewById(R.id.lnFriendBuyNow);

		convertView.setTag(R.id.imgDealFeedImage, holder.imgDealFeedImage);
		convertView.setTag(R.id.imgDealFeedImage, holder.imgDealFeedImage);

		convertView.setTag(R.id.txtFriendName, holder.txtFriendName);
		convertView.setTag(R.id.txtDealName, holder.txtDealName);
		convertView.setTag(R.id.txtDealTypeDiscription,
				holder.txtDealTypeDiscription);
		convertView.setTag(R.id.txtDealDetail, holder.txtDealDetail);

		convertView.setTag(R.id.txtDealTime, holder.txtDealTime);

		convertView.setTag(R.id.txtdealfeeddiscountprice,
				holder.txtdealfeeddiscountprice);
		convertView.setTag(R.id.lnFriendShare, holder.lnFriendShare);
		convertView.setTag(R.id.lnFriendDiscuss, holder.lnFriendDiscuss);
		convertView.setTag(R.id.lnFriendBuyNow, holder.lnFriendBuyNow);
		convertView.setTag(holder);
		holder.txtDealTime.setText(dealDetail.vBoughtTime);
		holder.txtDealName.setText(dealDetail.dealName);
		holder.txtDealTypeDiscription
				.setText(dealDetail.shortDescriptionHeading);

		dealDetail.shortDescription = dealDetail.shortDescription.replace(
				"\\n", "\n");

		dealDetail.shortDescription = dealDetail.shortDescription.replace(">",
				"●");

		holder.txtDealDetail.setText(dealDetail.shortDescription);

		holder.txtdealfeeddiscountprice.setText(" "
				+ dealDetail.dealDiscountPrice);

		holder.txtFriendName.setText(dealDetail.vFriendName);

		Picasso.with(activity).load(dealDetail.DealImageUrl)
				.placeholder(R.drawable.noimage_deal)
				.into(holder.imgDealFeedImage);

		async_deel_feed_list as = new async_deel_feed_list(
				new asynctaskloaddata() {
					Bitmap topic = null;

					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub

					}

					@Override
					public void doInBackground() {
						// TODO Auto-generated method stub.

						try {
							topic = Picasso
									.with(context)
									.load(dealDetail.vFriendImage_A_100x100)
									.transform(new RoundedTransformation(20, 0))
									.get();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					@Override
					public void onPostExecute() {
						// TODO Auto-generated method stub
						if (topic != null) {
							holder.imgFriendImage.setImageBitmap(topic);
						}

					}
				});

		as.execute();

		holder.lnFriendBuyNow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dealInterface.dealFeedClick("DealPurchase", dealDetail);
			}
		});

		holder.lnFriendShare.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// dealInterface.dealFeedClick("DealShare", dealDetail);

				Sharing_Dialog(dealDetail);
				ImageDialog.getWindow().getAttributes().windowAnimations = R.style.Animations_PopUpMenu_Center;
				ImageDialog.show();

				mimcoupsharing.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						sharedeal.onsharedealclicked("mimcup", dealDetail);

						ImageDialog.dismiss();

					}
				});

				btnemailsharing.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						// Log.e("saving==", dealdetail.locationId);

						sharedeal.onsharedealclicked("mail", dealDetail);

						ImageDialog.dismiss();

					}
				});

			}
		});
		holder.lnFriendDiscuss.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dealInterface.dealFeedClick("dealBuy", dealDetail);
			}
		});
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dealInterface.dealFeedClick("DealDetail", dealDetail);
			}
		});
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrFriendPurhcase.size();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private void Sharing_Dialog(final DealFeedDetail dealdetail) {
		try{
			ImageDialog = new Dialog(context);
			ImageDialog.setContentView(R.layout.share_white_vertical_dialog);
			ImageDialog.setCancelable(true);
			ImageDialog.getWindow().setBackgroundDrawable(
					new ColorDrawable(android.graphics.Color.TRANSPARENT));
			int divierId = ImageDialog.getContext().getResources()
					.getIdentifier("android:id/titleDivider", null, null);
			View divider = ImageDialog.findViewById(divierId);
//			divider.setBackgroundColor(Color.TRANSPARENT);

			mimcoupsharing = (Button) ImageDialog
					.findViewById(R.id.btnmimcoupsharing);

			btnemailsharing = (Button) ImageDialog
					.findViewById(R.id.btnemailsharing);

			facebookcontainer = (Button) ImageDialog
					.findViewById(R.id.btnfacebookcontainer);
			twittercontainer = (Button) ImageDialog
					.findViewById(R.id.btntwittercontainer);
			googlepluscontainer = (Button) ImageDialog
					.findViewById(R.id.btngooglepluscontainer);
			instagamcontainer = (Button) ImageDialog
					.findViewById(R.id.btninstagramcontainer);

			Button btncancel = (Button) ImageDialog.findViewById(R.id.btnCancel);

			if (CommonData.languageId.equalsIgnoreCase("1")) {
				share = CommonData.dealshareUrl_au
						+ dealdetail.DealId;

			} else {
				share = CommonData.dealshareUrl_no
						+ dealdetail.DealId;
			}

			btncancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ImageDialog.dismiss();

				}
			});

			facebookcontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent ii = new Intent(context,
							FacebookShare.class);
					ii.putExtra("dealname", dealdetail.dealName);
					ii.putExtra("desc", dealdetail.shortDescription);
					ii.putExtra("Image", share);
					((Activity) context).startActivity(ii);
					ImageDialog.dismiss();
				}
			});

			twittercontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					try {

						Log.e("cdeeel feedadapter", "" + dealdetail.DealImageUrl);
						Uri uri = Uri.parse(dealdetail.DealImageUrl);
						Intent tweetIntent = new Intent(Intent.ACTION_SEND);
						// tweetIntent.putExtra(Intent.EXTRA_TEXT,
						// dealdetail.dealName
						// + "\n " + dealdetail.shortDescription);
						tweetIntent.putExtra(Intent.EXTRA_TEXT, dealdetail.dealName
								+ "\n " + share);
						tweetIntent.putExtra(Intent.EXTRA_STREAM, uri);
						tweetIntent.setType("*/*");

						PackageManager packManager = context.getPackageManager();
						List<ResolveInfo> resolvedInfoList = packManager
								.queryIntentActivities(tweetIntent,
										PackageManager.MATCH_DEFAULT_ONLY);

						boolean resolved = false;
						for (ResolveInfo resolveInfo : resolvedInfoList) {
							if (resolveInfo.activityInfo.packageName
									.startsWith("com.twitter.android")) {
								tweetIntent.setClassName(
										resolveInfo.activityInfo.packageName,
										resolveInfo.activityInfo.name);
								resolved = true;
								break;
							}
						}
						if (resolved) {
							context.startActivity(tweetIntent);
						} else {
							try {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("market://details?id=com.twitter.android")));
							} catch (android.content.ActivityNotFoundException anfe) {
								context.startActivity(new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("https://play.google.com/store/apps/details?id=com.twitter.android")));
							}
						}

						// Uri uri = Uri.parse(dealdetail.DealImageUrl);
						// // Uri uri =
						// //
						// Uri.parse("android.resource://com.gobaby.app/drawable/back");
						// Intent intent = new Intent(Intent.ACTION_SEND);
						// intent.setType("/*");
						// intent.setClassName("com.twitter.android",
						// "com.twitter.android.PostActivity");
						// intent.putExtra(Intent.EXTRA_TEXT, dealdetail.dealName
						// + " With " + dealdetail.vSavingamount + "\n\n "
						// + dealdetail.vShortDescription);
						// intent.putExtra(Intent.EXTRA_STREAM, uri);
						// context.startActivity(intent);

					} catch (Exception e) {

					}
					ImageDialog.dismiss();
				}

			});

			googlepluscontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Intent iv = new Intent(context, SignInActivity.class);
					// iv.putExtra("sharingfrom", 1);
					// context.startActivity(iv);

					// Intent shareIntent = new PlusShare.Builder(context)
					// .setType("text/plain")
					// .setText(
					// "I like to share Deel From MimCup , i Love THis Deel  ,Please You Also Try MimCup")
					// .setContentUrl(Uri.parse("http://www.mimcoup.com"))
					// .getIntent();
					// context.startActivity(shareIntent);

					// Intent shareIntent = new PlusShare.Builder(context)
					// .setType("text/plain")
					// .setText("MimCoup APP:: \n " + dealdetail.dealName
					//
					// + "\n " + dealdetail.shortDescription)
					// .setContentUrl(Uri.parse(dealdetail.DealImageUrl))
					// .getIntent();

					Intent shareIntent = new PlusShare.Builder(context)
							.setType("text/plain")
							.setText("MiMCoups APP:: \n " + dealdetail.dealName

									+ "\n " + dealdetail.shortDescription)
							.setContentUrl(Uri.parse(share)).getIntent();
					((Activity) context).startActivityForResult(shareIntent, 0);

				}
			});

			instagamcontainer.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					// Intent i = new Intent(context, InstaActivity.class);
					// i.putExtra("sharingfrom", 1);
					// context.startActivity(i);

					if (!appInstalledOrNot()) {
						Toast.makeText(
								context,
								"Please install Instagram Application from Play Store"
										+ "", Toast.LENGTH_LONG).show();
						return;
					} else {
						Intent shareIntent = new Intent(
								android.content.Intent.ACTION_SEND);
						shareIntent.setType("image/*"); // set mime type
						Uri path = Uri.parse("android.resource://com.mimcoups/"
								+ R.drawable.app_icon);
						shareIntent.putExtra(Intent.EXTRA_TEXT,"MiMCoups APP:: \n " + dealdetail.dealName

								+ "\n " + dealdetail.shortDescription+"\n"+share);
						shareIntent.putExtra(Intent.EXTRA_STREAM,
								dealdetail.DealImageUrl); // set uri
						shareIntent.setPackage("com.instagram.android");
						context.startActivity(shareIntent);
					}

				}
			});

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(ImageDialog.getWindow().getAttributes());
			DisplayMetrics displaymetrics = new DisplayMetrics();
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.BOTTOM;
			((Activity) context).getWindowManager().getDefaultDisplay()
					.getMetrics(displaymetrics);

			ImageDialog.getWindow().setAttributes(lp);
		}catch (Exception e){

		}


	}

	private boolean appInstalledOrNot() {

		boolean app_installed = false;
		try {
			ApplicationInfo info = context.getPackageManager()
					.getApplicationInfo("com.instagram.android", 0);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}
}
