package com.mimcoups.Consumer.DialogFragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mimcoups.Consumer.inteface.DialogAddressInterfaceClick;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.Statesadapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;

@SuppressLint("ValidFragment")
public class StatesListDialog extends DialogFragment {

	DialogAddressInterfaceClick dialogClick;
	private LayoutInflater layouInflator;
	ListView lstAddress;
	Statesadapter arraylistadapter;
	ArrayList<String> arrAddress = new ArrayList<String>();
	private WebService_Call web_service = new WebService_Call();
	topBarInteface topInteface;
	private CommonData commonData;
	private String getUrl = "", url_response = "", strStatusCode = "",
			strMessageResponse = "";
	String Type;

	@SuppressLint("ValidFragment")
	public StatesListDialog(DialogAddressInterfaceClick dialogClick,
			topBarInteface topInteface, String Type) {
		this.dialogClick = dialogClick;
		this.topInteface = topInteface;
		this.Type = Type;

		// if (this.Type.equalsIgnoreCase("Shipping")) {
		// getUrl = "OrderService.svc/GetCustomerShippingAddress?customerid="
		// + commonData.CustomerId + "&languageid="
		// + commonData.languageId;
		// } else {
		// getUrl = "OrderService.svc/GetCustomerBillingAddress?"
		// + commonData.CustomerId + "&languageid="
		// + commonData.languageId;
		//
		// }
	}

	//
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		layouInflator = LayoutInflater.from(getActivity());
		View promptsView = layouInflator.inflate(R.layout.address_listdialog,
				null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		Log.e("called ", Type);

		if (Type.equalsIgnoreCase("Shipping")) {

			alertDialogBuilder.setTitle(getResources().getString(
					R.string.shippingstates));

		} else {

			alertDialogBuilder.setTitle(getResources().getString(
					R.string.billingstates));

		}

		getUrl = commonData.StateUrl;

		// /set listview

		alertDialogBuilder.setView(promptsView);

		initControls(promptsView);
		Load_Address();

		lstAddress.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				dismiss();

				dialogClick.stateDialogClick("OK", arrAddress.get(position));

			}
		});

		// alertDialogBuilder.setPositiveButton(
		// getResources().getString(R.string.Done),
		// new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog, int whichButton) {
		// dialogClick.dialogClick("OK");
		// }
		//
		// });
		AlertDialog alertDialog = alertDialogBuilder.create();
		return alertDialog;
	}

	private void initControls(View view) {
		commonData = (CommonData) getActivity().getApplicationContext();
		lstAddress = (ListView) view.findViewById(R.id.Alertfriendlist);

	}

	private void Load_Address() {

		if (!commonData.checkInternetConnection()) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							topInteface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);

		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInteface.showDialog("Loading");

						}

						@Override
						public void doInBackground() {

							try {

								String url_response = web_service
										.makeServicegetRequest(getUrl);
								Log.e("caleld address", getUrl + ""
										+ url_response);
								JSONObject jsonObject = new JSONObject(
										url_response);
								strStatusCode = jsonObject
										.getString("vStatusCode");
								strMessageResponse = jsonObject
										.getString("vMessageResponse");

								if (strStatusCode.equals("100")) {

									JSONArray jsonarray = jsonObject
											.getJSONArray("States");
									for (int i = 0; i < jsonarray.length(); i++) {
										JSONObject obj=jsonarray.getJSONObject(i);
										
										arrAddress.add(obj.getString("vStateName"));
										
									}
								}
							} catch (Exception e) {
								strMessageResponse = e.getMessage();
							}
						}

						@Override
						public void onPostExecute() {
							topInteface.hideDialog();

							if (arrAddress.size() == 0) {
								dismiss();
							}

							// TODO Auto-generated method stub

							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (strStatusCode.equals("100")) {

								arraylistadapter = new Statesadapter(
										getActivity(), arrAddress, Type);

								lstAddress.setAdapter(arraylistadapter);

							} else {
								if (strMessageResponse.trim().length() != 0) {
									DialogFragment dsp = new SingleButtonAlert(
											new DialogInterfaceClick() {

												@Override
												public void dialogClick(
														String tag) {

												}
											}, strMessageResponse);
									dsp.show(getFragmentManager(), "");
								}
							}
						}
					});
			as.execute();
		}
	}
}