package com.mimcoups.Consumer.DialogFragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.mimcoups.R;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;

@SuppressLint("ValidFragment")
public class UpdateapplicationDialog extends DialogFragment {

	DialogInterfaceClick dialogClick;
	private String strMSG;

	public UpdateapplicationDialog(DialogInterfaceClick dialogClick, String strMSG) {
		this.dialogClick = dialogClick;
		this.strMSG = strMSG;

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());

		alertdialog.setTitle(getResources().getString(R.string.app_name));

		alertdialog.setMessage(strMSG);

		alertdialog.setPositiveButton(
				getResources().getString(R.string.txtupdate),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {

						dialogClick.dialogClick("update");
					} // End of onClick(DialogInterface dialog, int whichButton)
				}); // End of alert.setPositiveButton
		alertdialog.setNegativeButton(
				getResources().getString(R.string.txtremindme),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.

						dialogClick.dialogClick("remind");

					}
				}); // End of alert.setNegativeButton

		AlertDialog alertDialog = alertdialog.create();

		return alertDialog;
	}

}
