package com.mimcoups.Consumer.DialogFragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.R;

@SuppressLint("ValidFragment")
public class SingleButtonAlert extends DialogFragment  {
	DialogInterfaceClick dialogClick;
	private String strMSG;

	public SingleButtonAlert() {

	}

	public SingleButtonAlert(DialogInterfaceClick dialogClick, String strMSG) {
		this.dialogClick = dialogClick;
		this.strMSG = strMSG;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());

		alertdialog.setTitle(getResources().getString(R.string.app_name));
		alertdialog
				.setMessage(strMSG)
				.setCancelable(false)

				.setNegativeButton(getResources().getString(R.string.DialogOK),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialogClick.dialogClick("Ok");
								dialog.cancel();
							}
						});

		AlertDialog alertDialog = alertdialog.create();

		return alertDialog;
	}
}