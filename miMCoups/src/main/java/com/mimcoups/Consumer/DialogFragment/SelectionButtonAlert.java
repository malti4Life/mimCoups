package com.mimcoups.Consumer.DialogFragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.R;

@SuppressLint("ValidFragment")
public class SelectionButtonAlert extends DialogFragment {

	DialogInterfaceClick dialogClick;
	private String strMSG;

	public SelectionButtonAlert(DialogInterfaceClick dialogClick, String strMSG) {
		this.dialogClick = dialogClick;
		this.strMSG = strMSG;

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());

		alertdialog.setTitle(getResources().getString(R.string.app_name));

		alertdialog.setMessage(strMSG);

		alertdialog.setPositiveButton(
				getResources().getString(R.string.DialogOK),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {

						dialogClick.dialogClick("OK");
					} // End of onClick(DialogInterface dialog, int whichButton)
				}); // End of alert.setPositiveButton
		alertdialog.setNegativeButton(
				getResources().getString(R.string.DialogCancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
						dialog.cancel();
					}
				}); // End of alert.setNegativeButton

		AlertDialog alertDialog = alertdialog.create();

		return alertDialog;
	}

}
