package com.mimcoups.Consumer.DialogFragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mimcoups.R;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;

@SuppressLint("ValidFragment")
public class CurrencyDailogFragment extends DialogFragment {
	DialogInterfaceClick dialogClick;
	private String languageId = "1";
	private LayoutInflater layouInflator;

	public CurrencyDailogFragment(DialogInterfaceClick dialogClick,
			String languageId) {
		this.dialogClick = dialogClick;
		this.languageId = languageId;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		layouInflator = LayoutInflater.from(getActivity());
		View promptsView = layouInflator.inflate(R.layout.alert_language, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());
		alertDialogBuilder
				.setTitle(getResources().getString(R.string.app_name));
		final RadioGroup rg = (RadioGroup) promptsView
				.findViewById(R.id.radiogroup);
		final RadioButton rbt_English = (RadioButton) promptsView
				.findViewById(R.id.rbENglish);
		final RadioButton rbt_Norwegian = (RadioButton) promptsView
				.findViewById(R.id.rbNorway);
		alertDialogBuilder.setView(promptsView);
		if (languageId.equals("1")) {
			rbt_English.setChecked(true);
		} else {
			rbt_Norwegian.setChecked(true);
		}

		alertDialogBuilder.setPositiveButton(getResources().getString(R.string.DialogOK),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						int radioButtonID = rg.getCheckedRadioButtonId();
						View radioButton = rg.findViewById(radioButtonID);
						int idx = rg.indexOfChild(radioButton);
						if (idx == 1) {
							dialogClick.dialogClick("no");
						} else {
							dialogClick.dialogClick("en");
						}
					}
				});
		alertDialogBuilder.setNegativeButton(getResources().getString(R.string.DialogCancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
						dialog.cancel();
					}
				}); // End of alert.setNegativeButton
		AlertDialog alertDialog = alertDialogBuilder.create();
		return alertDialog;
	}
}