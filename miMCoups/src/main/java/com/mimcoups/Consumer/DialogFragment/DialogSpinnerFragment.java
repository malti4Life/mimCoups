package com.mimcoups.Consumer.DialogFragment;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;

import com.mimcoups.Consumer.customClass.CategoryDetail;
import com.mimcoups.R;
import com.mimcoups.Consumer.inteface.DialogNotificationPoupInterface;

@SuppressLint("ValidFragment")
public class DialogSpinnerFragment extends DialogFragment {
	private DialogNotificationPoupInterface dialogInterface;
	private ListView lstNotifications;
	private ArrayList<CategoryDetail> arrNotifcationCategory;
	private adapterpoupNotications adapter;
	private AlertDialog alertDialog;

	public DialogSpinnerFragment(
			DialogNotificationPoupInterface dialogInterface,
			ArrayList<CategoryDetail> arrNotifcationCategory) {
		this.dialogInterface = dialogInterface;
		this.arrNotifcationCategory = arrNotifcationCategory;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater li = LayoutInflater.from(getActivity());
		View view = li.inflate(R.layout.alert_poup_spinner, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());
		alertDialogBuilder.setView(view);

		lstNotifications = (ListView) view.findViewById(R.id.lstAllList);
		adapter = new adapterpoupNotications(getActivity(),
				R.layout.row_popup_notifications, arrNotifcationCategory);
		lstNotifications.setAdapter(adapter);
		alertDialog = alertDialogBuilder.create();

		return alertDialog;
	}

	public class adapterpoupNotications extends ArrayAdapter<CategoryDetail> {

		Context context;
		ViewHolder holder;
		private boolean userSelected = false;
		private RadioButton mCurrentlyCheckedRB;

		public adapterpoupNotications(Context context, int textViewResourceId,
				List<CategoryDetail> items) {
			super(context, textViewResourceId, items);
			this.context = context;
		}

		private class ViewHolder {
			RadioButton radioBtn;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			holder = null;
			final CategoryDetail rowItem = getItem(position);
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			if (convertView == null) {
				convertView = mInflater.inflate(
						R.layout.row_popup_notifications, null);
				holder = new ViewHolder();
				holder.radioBtn = (RadioButton) convertView
						.findViewById(R.id.rdb_payment_method);
				convertView.setTag(holder);
			} else
				holder = (ViewHolder) convertView.getTag();

			// if (position == getCount() - 1 && userSelected == false) {
			// holder.radioBtn.setChecked(true);
			// mCurrentlyCheckedRB = holder.radioBtn;
			// } else {
			// holder.radioBtn.setChecked(false);
			// }

			if (rowItem.CategoryFavourite) {

				holder.radioBtn.setChecked(true);
			} else {
				holder.radioBtn.setChecked(false);
			}

			holder.radioBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					alertDialog.dismiss();

					for (int i = 0; i < arrNotifcationCategory.size(); i++) {
						CategoryDetail rowItems = getItem(i);
						rowItems.CategoryFavourite = false;
					}

					rowItem.CategoryFavourite = true;
					dialogInterface.DialogNotificationClick(rowItem);
				}
			});
			holder.radioBtn.setText(rowItem.categoryName);
			return convertView;
		}

	}
}