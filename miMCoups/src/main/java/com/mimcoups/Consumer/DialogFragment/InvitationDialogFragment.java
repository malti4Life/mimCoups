package com.mimcoups.Consumer.DialogFragment;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.FriendsDetail;

@SuppressLint("ValidFragment")
public class InvitationDialogFragment extends DialogFragment {

	DialogInterfaceClick dialogClick;
	private String languageId = "1";
	ArrayList<FriendsDetail> arrFrinedList;

	public InvitationDialogFragment(DialogInterfaceClick dialogClick,
			String languageId, ArrayList<FriendsDetail> arrFrinedLists) {
		this.dialogClick = dialogClick;
		this.languageId = languageId;
		arrFrinedList = arrFrinedLists;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		alert.setTitle(getResources().getString(R.string.invitationdialog));
		final RadioGroup rg = new RadioGroup(getActivity());

		for (int i = 0; i < arrFrinedList.size(); i++) {
			final RadioButton rbt_friend = new RadioButton(getActivity());
			int id = Integer.valueOf(arrFrinedList.get(i).friendsId);
			String friendname = arrFrinedList.get(i).freindsName;
			rbt_friend.setId(id);
			rbt_friend.setText(friendname);
			rg.addView(rbt_friend);
			rg.setPadding(20, 20, 0, 10);
		}

		alert.setView(rg);

		alert.setPositiveButton(getResources().getString(R.string.DialogOK), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				int radioButtonID = rg.getCheckedRadioButtonId();
				View radioButton = rg.findViewById(radioButtonID);
				int idx = rg.indexOfChild(radioButton);
				// RadioButton btn = (RadioButton) rg.getChildAt(idx);
				// String selection = (String) btn.getText();

				String id = arrFrinedList.get(idx).friendsId;

				dialogClick.dialogClick(id);

				

			} // End of onClick(DialogInterface dialog, int whichButton)
		}); // End of alert.setPositiveButton

		alert.setNegativeButton(getResources().getString(R.string.DialogCancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Canceled.
						
						dialogClick.dialogClick("Cancel");
						
						
						dialog.cancel();
						
						
						
					}
				}); // End of alert.setNegativeButton

		AlertDialog alertDialog = alert.create();

		return alertDialog;
	}
}