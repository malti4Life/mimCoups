package com.mimcoups.Consumer.DialogFragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.mimcoups.R;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;

@SuppressLint("ValidFragment")
public class PurchaseProofCodeFragment extends DialogFragment {
	private DialogInterfaceClick dialogInterface;

	public PurchaseProofCodeFragment(DialogInterfaceClick dialogInterface) {
		this.dialogInterface = dialogInterface;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater li = LayoutInflater.from(getActivity());
		View promptsView = li.inflate(R.layout.alert_purchase_code, null);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());
		alertDialogBuilder.setView(promptsView);

		final EditText edtPurchasecode = (EditText) promptsView
				.findViewById(R.id.edtPurchaseCode);
		// set dialog message
		alertDialogBuilder
				.setTitle(getResources().getString(R.string.app_name))
				.setCancelable(false)
				.setPositiveButton(getResources().getString(R.string.DialogSubmit),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

							}
						})
				.setNegativeButton(getResources().getString(R.string.DialogCancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create alert dialog
		final AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
		alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (edtPurchasecode.getText().toString().trim()
								.equals("")) {
							edtPurchasecode.setError(getResources().getString(
									R.string.purchasecodevalidation));
						} else {
							alertDialog.dismiss();
							dialogInterface.dialogClick(edtPurchasecode
									.getText().toString().trim());
						}
					}
				});

		return alertDialog;
	}
}