package com.mimcoups.Consumer.DialogFragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

@SuppressLint("ValidFragment")
public class ProgressDialogFragment extends DialogFragment {
	private String message;

	public ProgressDialogFragment() {
		this.message = "";
	}

	public ProgressDialogFragment(String message) {
		this.message = message;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		ProgressDialog pbarDialog = new ProgressDialog(getActivity());
		pbarDialog.setMessage(message);
		pbarDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		// pbarDialog.setIndeterminate(true);
		pbarDialog.setCancelable(false);
		pbarDialog.show();
		return pbarDialog;
	}
}
