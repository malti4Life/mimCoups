package com.mimcoups.Consumer.DialogFragment;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.R;

@SuppressLint("ValidFragment")
public class FriendsDialogFragment extends DialogFragment {

	DialogInterfaceClick dialogClick;
	private String languageId = "1";
	ArrayList<FriendsDetail> arrFrinedList;
	ArrayList<String> friendinvitation;

	public FriendsDialogFragment(DialogInterfaceClick dialogClick,
			String languageId, ArrayList<FriendsDetail> arrFrinedLists) {
		this.dialogClick = dialogClick;
		this.languageId = languageId;
		arrFrinedList = arrFrinedLists;
		friendinvitation = new ArrayList<String>();
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		alert.setTitle(getResources().getString(R.string.makefriendlist));

		CheckBox checkBox;
		LinearLayout ll = new LinearLayout(getActivity());
		ll.setOrientation(LinearLayout.VERTICAL);
		for (int i = 0; i < arrFrinedList.size(); i++) {
			checkBox = new CheckBox(getActivity());
			int id = Integer.valueOf(arrFrinedList.get(i).friendsId);
			String friendname = arrFrinedList.get(i).freindsName;
			checkBox.setId(id);
			checkBox.setText(friendname);
			checkBox.setOnClickListener(getOnClickDoSomething(checkBox));

			// checkBox.setPadding(20, 20, 0, 10);

			ll.addView(checkBox);

		}
		alert.setView(ll);
		alert.setPositiveButton(getResources().getString(R.string.DialogOK), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				String frienlist = friendinvitation.toString();

				dialogClick.dialogClick(frienlist);

			}
		});

		alert.setNegativeButton(getResources().getString(R.string.DialogCancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {

						dialogClick.dialogClick("Cancel");
						dialog.cancel();
					}
				});

		AlertDialog alertDialog = alert.create();

		return alertDialog;
	}

	View.OnClickListener getOnClickDoSomething(final CheckBox ch) {
		return new View.OnClickListener() {
			public void onClick(View v) {

				if (ch.isChecked()) {
					String id = String.valueOf(ch.getId());
					friendinvitation.add(id);
				} else {
					String checkid = String.valueOf(ch.getId());
					if (friendinvitation.contains(checkid)) {
						friendinvitation.remove(checkid);
					}
				}
			}
		};
	}
}