package com.mimcoups.Consumer.DialogFragment;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.asynctaskloaddata;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.R;
import com.mimcoups.Consumer.adapter.AlertFriendlistAdapter;
import com.mimcoups.Consumer.asynctask.async_deel_feed_list;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.WebService_Call;
import com.mimcoups.Consumer.customClass.FriendsDetail;
import com.mimcoups.Consumer.fragment.FragmentDealShare;
import com.mimcoups.Consumer.fragment.FragmentLocationShare;

@SuppressLint("ValidFragment")
public class ListDialog extends DialogFragment {

	DialogInterfaceClick dialogClick;
	private LayoutInflater layouInflator;
	ListView Alertfriendlist;
	AlertFriendlistAdapter arraylistadapter;
	ArrayList<FriendsDetail> arrFriends = new ArrayList<FriendsDetail>();
	private WebService_Call web_service = new WebService_Call();
	topBarInteface topInteface;
	private CommonData commonData;
	private String getUrl = "", url_response = "", strStatusCode = "",
			strMessageResponse = "";
	String loadfrom;

	@SuppressLint("ValidFragment")
	public ListDialog(DialogInterfaceClick dialogClick,
			topBarInteface topInteface, String loadfrom) {
		this.dialogClick = dialogClick;
		this.topInteface = topInteface;
		this.loadfrom = loadfrom;
	}

	//
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		layouInflator = LayoutInflater.from(getActivity());
		View promptsView = layouInflator.inflate(R.layout.listdialog, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				getActivity());

		if (loadfrom.equalsIgnoreCase("location")) {
			alertDialogBuilder.setTitle(getResources().getString(
					R.string.sharelocationdialogtitle));

		} else {

			alertDialogBuilder.setTitle(getResources().getString(
					R.string.sharedealdialogtitle));
		}
		// /set listview
		Alertfriendlist = (ListView) promptsView
				.findViewById(R.id.Alertfriendlist);
		alertDialogBuilder.setView(promptsView);
		//
		// for (int i = 0; i < 10; i++) {
		// FriendsDetail friend = new FriendsDetail();
		// friend.freindsName = "Nirmal";
		// friend.friendsId = "5";
		// friend.profileImageName =
		// "http://www.macmillandictionaryblog.com/wp-content/uploads/2011/07/Small-Talk-image.jpg";
		// friend.userName = "Nirmal";
		// friend.iCustomerRecievedNotificationId = "ids";
		// friend.iRequestFromId = "request id";
		// friend.vNotificationMessage = "notification message";
		// friend.isfriend = true;
		// friend.friendEmailAddress = "Address";
		// arrFriends.add(friend);
		//
		// }
		initControls(promptsView);
		Load_Friends_List();

		alertDialogBuilder.setPositiveButton(
				getResources().getString(R.string.Done),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialogClick.dialogClick("OK");
					}

				});
		// alertDialogBuilder.setNegativeButton("CANCEL",
		// new DialogInterface.OnClickListener() {
		// public void onClick(DialogInterface dialog, int whichButton) {
		// // Canceled.
		//
		// dialogClick.dialogClick("OK CLicked");
		// dialog.cancel();
		// }
		// }); // End of alert.setNegativeButton
		AlertDialog alertDialog = alertDialogBuilder.create();
		return alertDialog;
	}

	private void initControls(View view) {
		commonData = (CommonData) getActivity().getApplicationContext();

	}

	private void Load_Friends_List() {

		if (!commonData.checkInternetConnection()) {
			DialogFragment dsp = new SingleButtonAlert(
					new DialogInterfaceClick() {

						@Override
						public void dialogClick(String tag) {
							topInteface.topbarClick("back");
						}
					}, getResources().getString(R.string.InternetConnect));
			dsp.show(getFragmentManager(), "");
			dsp.setCancelable(false);

		} else {
			async_deel_feed_list as = new async_deel_feed_list(
					new asynctaskloaddata() {

						@Override
						public void onPreExecute() {
							// TODO Auto-generated method stub
							topInteface.showDialog("Loading");
							arrFriends.clear();
						}

						@Override
						public void doInBackground() {
							String getUrl = "FriendService.svc/GetAllFriends?customerid="
									+ commonData.CustomerId
									+ "&languageid="
									+ commonData.languageId;

							String url_response = web_service
									.makeServicegetRequest(getUrl);
							try {
								JSONObject jsonObject = new JSONObject(
										url_response);
								strStatusCode = jsonObject
										.getString("vStatusCode");
								strMessageResponse = jsonObject
										.getString("vMessageResponse");

								if (strStatusCode.equals("100")) {
									JSONArray jsonarray = jsonObject
											.getJSONArray("Customers");
									for (int i = 0; i < jsonarray.length(); i++) {
										JSONObject tempObject = jsonarray
												.getJSONObject(i);
										FriendsDetail friendDetail = new FriendsDetail();
										friendDetail.friendsId = tempObject
												.getString("iCustomerId");
										friendDetail.profileImageName = tempObject
												.getString("vProfileImageName");
										friendDetail.userName = tempObject
												.getString("vUserName");
										friendDetail.freindsName = tempObject
												.getString("vCustomerName");
										friendDetail.friendEmailAddress = tempObject
												.getString("vEmail");

										if (loadfrom.equals("location")) {

											for (int j = 0; j < FragmentLocationShare.ListofFriendsEmailtoshare
													.size(); j++) {

												FriendsDetail fj = FragmentLocationShare.ListofFriendsEmailtoshare
														.get(j);

												if (fj.friendEmailAddress
														.equals(friendDetail.friendEmailAddress)) {

													friendDetail.isadd = true;

												}

											}
										}

										if (loadfrom.equals("deal")) {

											for (int j = 0; j < FragmentDealShare.ListofFriendsEmailtoshare
													.size(); j++) {

												FriendsDetail fj = FragmentDealShare.ListofFriendsEmailtoshare
														.get(j);

												if (fj.friendEmailAddress
														.equals(friendDetail.friendEmailAddress)) {
													friendDetail.isadd = true;

												}

											}
										}

										arrFriends.add(friendDetail);
									}
								}
							} catch (Exception e) {
								strMessageResponse = e.getMessage();
							}
						}

						@Override
						public void onPostExecute() {

							if (arrFriends.size() == 0) {
								dismiss();
							}

							// TODO Auto-generated method stub
							topInteface.hideDialog();
							if (strStatusCode.equals("420")) {
								DialogFragment ds = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

											}
										}, getResources().getString(
												R.string.PoorInternetConnect));
								ds.show(getFragmentManager(), "");
								ds.setCancelable(false);

							} else if (strStatusCode.equals("100")) {

								arraylistadapter = new AlertFriendlistAdapter(
										getActivity(), arrFriends, loadfrom);

								Alertfriendlist.setAdapter(arraylistadapter);

							} else {

								DialogFragment dsp = new SingleButtonAlert(
										new DialogInterfaceClick() {

											@Override
											public void dialogClick(String tag) {

											}
										}, strMessageResponse);
								dsp.show(getFragmentManager(), "");
							}
						}
					});
			as.execute();
		}
	}
}