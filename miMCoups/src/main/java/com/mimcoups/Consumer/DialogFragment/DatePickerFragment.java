package com.mimcoups.Consumer.DialogFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TextView;

import com.mimcoups.Consumer.commonClass.Age;
import com.mimcoups.Consumer.commonClass.AgeCalculator;
import com.mimcoups.Consumer.customClass.DateCustomeClass;
import com.mimcoups.Consumer.inteface.wrongdatedialoginterface;

@SuppressLint("ValidFragment")
public class DatePickerFragment extends DialogFragment implements
		DatePickerDialog.OnDateSetListener {

	TextView txtBirthDate;
	private String languageId;
	wrongdatedialoginterface dateinterface;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	// public static int totalage = 0;
	public static String text_to_upload = "";
	public String languageid;
	int year, day, month;

	public DatePickerFragment(wrongdatedialoginterface dateinterface,Context context, TextView textview,
			String languageId, int year, int month, int day) {
		txtBirthDate = textview;
		this.languageId = languageId;
		this.dateinterface = dateinterface;

		this.year = year;
		this.month = month - 1;
		this.day = day;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the current date as the default date in the picker

		return new DatePickerDialog(getActivity(), this, year, month, day);
	}

	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		if (view.isShown()) {

			Date current = null;
			Date date2 = null;
			int month = monthOfYear + 1;
			try {
				current = new Date();
				date2 = sdf.parse(year + "-" + month + "-" + dayOfMonth);

			} catch (ParseException e) {
				e.printStackTrace();
			}

//			Log.e("called date 1", ""+current);
//			Log.e("called date2", ""+date2);
			
			int datecompare = current.compareTo(date2);
//			if (datecompare == -1) {
////				dateinterface.selecteddateiswrongedialog(getResources()
////						.getString(R.string.wrongdatemessage));
//			}
//			else {

				// 14/08/2014
				int main_month = monthOfYear + 1;


				if (languageId.equals("1")) {

					txtBirthDate.setText("" + convert_proper_date(dayOfMonth)
							+ "/" + convert_proper_date(main_month) + "/"
							+ convert_proper_date(year));
					DateCustomeClass date=new DateCustomeClass();
					date.year=year;
					date.month=month;
					date.date=dayOfMonth;
					
//					Log.d("called date===", date.year+"/"+convert_proper_date(date.month)+"/"+convert_proper_date(date.date));
					dateinterface.ondateselectedevent(date);

				} else {

					txtBirthDate.setText("" + convert_proper_date(dayOfMonth)
							+ "." + convert_proper_date(main_month) + "."
							+ convert_proper_date(year));
					DateCustomeClass date=new DateCustomeClass();
					date.year=year;
					date.month=month;
					date.date=dayOfMonth;
					
					Log.d("called date===", date.toString());
					dateinterface.ondateselectedevent(date);


				}

				text_to_upload = String.valueOf(year) + "-"
						+ String.valueOf(monthOfYear + 1) + "-"
						+ String.valueOf(dayOfMonth);

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Date birthDate;

				try {
					String my_birthdate = dayOfMonth + "/" + main_month + "/"
							+ year;

					birthDate = sdf.parse(my_birthdate);

					AgeCalculator agecalculator = new AgeCalculator();
					Age age = agecalculator.calculateAge(birthDate);


				} catch (ParseException e) {
					e.printStackTrace();
				}

//			}

		}
	

	}

	public String convert_proper_date(int value) {

		String converted = null;
		if (value > 0 && value < 10) {
			converted = "0" + value;
			return converted;
		} else {
			converted = "" + value;
			return converted;
		}

	}

}