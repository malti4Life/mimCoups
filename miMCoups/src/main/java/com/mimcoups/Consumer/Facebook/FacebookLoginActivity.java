package com.mimcoups.Consumer.Facebook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.facebook.Session;
import com.mimcoups.R;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.customClass.UserDetail;

public class FacebookLoginActivity extends Activity {
	private String AccessTocken;
	private ProgressDialog PD_Dialog;
	String User_ID = "";
	boolean isfacebookavail = false;
	public static Session session;
	private static String MSG = "";
	private FacebookConnector facebookConnector;
	SessionEvents.AuthListener listener;
	boolean isface = false;
	private String facebookId;
	private String emailAddress;
	private UserDetail userDetail = new UserDetail();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fbsample);

		SessionStore.clear(getApplicationContext());

		callFacebookLogout(getApplicationContext());

		facebookConnector = new FacebookConnector(CommonData.FACEBOOK_APPID,
				FacebookLoginActivity.this, getApplicationContext(),
				new String[] { "email","user_birthday" }, "MiMCoups");
		postMessage();

	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public static void callFacebookLogout(Context context) {
		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

		}

	}

	public void postMessage() {
		if (facebookConnector.getFacebook().isSessionValid()) {

			AccessTocken = facebookConnector.getFacebook().getAccessToken();
			session = facebookConnector.getFacebook().getSession();
			new GetUserDetail().execute();

		} else {
			listener = new SessionEvents.AuthListener() {
				public void onAuthSucceed() {
					if (isface == false) {
						isface = true;
						AccessTocken = facebookConnector.getFacebook()
								.getAccessToken();
						session = facebookConnector.getFacebook().getSession();
						try {
							new GetUserDetail().execute();
						} catch (Exception e) {
							// TODO: handle exception
							// onBackPressed();
						}

						// onBackPressed();
					} else {

					}
				}

				public void onAuthFail(String error) {
					SessionEvents.removeAuthListener(listener);
					onBackPressed();
				}
			};

			SessionEvents.addAuthListener(listener);
			facebookConnector.login();
		}
	}

	public boolean IsFacebookAvailable() {
		try {
			ApplicationInfo info = getPackageManager().getApplicationInfo(
					"com.facebook.katana", 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	public void alert(String msg) {
		AlertDialog.Builder builder1 = new AlertDialog.Builder(
				FacebookLoginActivity.this);
		builder1.setTitle("MiMCoups");
		builder1.setMessage(msg);
		builder1.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		builder1.show();
	}

	@Override
	public void onBackPressed() {

		Log.e("onbackpress facebook", ""+userDetail.toString());
		Intent i = new Intent();
		i.putExtra("userDetail", userDetail);
		setResult(Activity.RESULT_OK, i);

		super.onBackPressed();
	}

	public class GetUserDetail extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			PD_Dialog = new ProgressDialog(FacebookLoginActivity.this);
			PD_Dialog.setMessage("Please wait...");
			PD_Dialog.setCancelable(false);
			PD_Dialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			GetUserDetail();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			try {
				if (PD_Dialog.isShowing()) {
					PD_Dialog.dismiss();
				}
				onBackPressed();
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

		private void GetUserDetail() {

			// TODO Auto-generated method stub
			try {

				URL myurl = new URL(
						"https://graph.facebook.com/me?access_token="
								+ AccessTocken);

				// Log.i("facebook url", "" + myurl);
				HttpURLConnection connection = (HttpURLConnection) myurl
						.openConnection();
				connection.connect();
				int code = connection.getResponseCode();
				if (code != 200) {
					MSG = "Internet connection not available";
					// setContentView(R.layout.webimg);
				} else {
					BufferedReader dis = new BufferedReader(
							new InputStreamReader(connection.getInputStream()));

					String myString, Result = dis.readLine();
					while ((myString = dis.readLine()) != null) {

						Result += myString;
					}
					// Log.d("Result", "" + Result);
					JSONObject jsonObject = new JSONObject(Result);

					Log.e("Json response",jsonObject.toString());

					userDetail.emailAddress = jsonObject.getString("email");
					userDetail.facebookId = jsonObject.getString("id");
					User_ID = userDetail.facebookId;
					userDetail.name = "" + jsonObject.getString("name");

					if (jsonObject.has("username")) {
						userDetail.username = ""
								+ jsonObject.getString("username");
					}
					if (jsonObject.has("birthday")) {

						Log.e("Birthday ==",jsonObject.getString("birthday"));

					}

					userDetail.gender = jsonObject.getString("gender");
				}

			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				MSG = "Internet connection not available";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				MSG = "Internet connection not available";
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				MSG = "Internet connection not available";
			}
			try {
				if (!User_ID.equals("")) {
					URL myurl = new URL("https://graph.facebook.com/" + User_ID
							+ "?fields=picture&access_token=" + AccessTocken);

					HttpURLConnection connection = (HttpURLConnection) myurl
							.openConnection();
					connection.connect();
					int code = connection.getResponseCode();
					if (code != 200) {
						MSG = "Internet connection not available";
						// setContentView(R.layout.webimg);
					} else {
						BufferedReader dis = new BufferedReader(
								new InputStreamReader(
										connection.getInputStream()));
						String myString, Result = dis.readLine();
						while ((myString = dis.readLine()) != null) {
							Result += myString;
						}
						JSONObject jsonObject = new JSONObject(Result);

						JSONObject jsonObject1 = jsonObject
								.getJSONObject("picture");
						JSONObject jsonObject2 = jsonObject1
								.getJSONObject("data");
						userDetail.profileUrl = jsonObject2.getString("url");
					}
				}
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				MSG = "Internet connection not available";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				MSG = "Internet connection not available";
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				MSG = "Internet connection not available";
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (data == null) {
			onBackPressed();
		} else {
			this.facebookConnector.getFacebook().authorizeCallback(requestCode,
					resultCode, data);
		}

	}
}