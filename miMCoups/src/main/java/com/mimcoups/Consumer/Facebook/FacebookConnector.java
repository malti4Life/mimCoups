package com.mimcoups.Consumer.Facebook;

import java.io.IOException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class FacebookConnector {

	private Facebook facebook = null;
	private Context context;
	private String[] permissions;
	private Handler mHandler, handlr;
	private Activity activity;
	private SessionListener mSessionListener = new SessionListener();
	ProgressDialog pdialog;
	String MSG;

	public FacebookConnector(String appId, Activity activity,
			final Context context, String[] permissions, String tag) {

		
		this.facebook = new Facebook(appId);

		SessionStore.restore(facebook, context);
		SessionEvents.addAuthListener(mSessionListener);
		SessionEvents.addLogoutListener(mSessionListener);
		this.MSG = tag;
		this.context = context;
		this.permissions = permissions;
		this.mHandler = new Handler();
		this.handlr = new Handler();
		this.activity = activity;
	}

	public void login() {

		if (!facebook.isSessionValid()) {
			Log.i("in login function>>", "authorize");

			facebook.authorize(this.activity, this.permissions,
					new LoginDialogListener());
		}
	}

	public void logout() {

		SessionEvents.onLogoutBegin();
		AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(this.facebook);
		// asyncRunner.logout(this.context, (RequestListener) new
		// LogoutRequestListener());

	}

	public String postMessageOnWall(String message) {
		String response = "";
		if (facebook.isSessionValid()) {

			// Log.i("validdd>>", "valid>>");
		} else {
			// Log.i("login>>", "login");

			login();
		}
		return response;
	}

	private final class LoginDialogListener implements DialogListener {

		public void onComplete(Bundle values) {

			SessionEvents.onLoginSuccess();
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {

			try {
				SessionEvents.onLoginError("Action Canceled");
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
	}

	// public class LogoutRequestListener extends BaseRequestListener {
	//
	// public void onComplete(String response, final Object state) {
	//
	// mHandler.post(new Runnable() {
	//
	// public void run() {
	//
	// SessionEvents.onLogoutFinish();
	// }
	// });
	// }
	// }
	private class SessionListener implements SessionEvents.AuthListener, SessionEvents.LogoutListener {

		public void onAuthSucceed() {

			SessionStore.save(facebook, context);
		}

		public void onAuthFail(String error) {
		}

		public void onLogoutBegin() {
		}

		public void onLogoutFinish() {
			SessionStore.clear(context);
		}
	}

	public Facebook getFacebook() {
		return this.facebook;
	}

	public class validSession extends AsyncTask<Void, Void, Void> {

		String message = "", response = "";
		Context context;

		public validSession(String mes, String res, Context mCon) {
			message = mes;
			response = res;
			context = mCon;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}

		@Override
		protected Void doInBackground(Void... params) {// TODO Auto-generated
														// method stub
			Bundle bundle = new Bundle();

			bundle.putString("message", message);
			bundle.putString("link", MSG);

			try {
				response = facebook.request("me/feed", bundle, "POST");

			} catch (IOException e) {
				response = "exception";
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// pdialog.dismiss();
		}

	}
}
