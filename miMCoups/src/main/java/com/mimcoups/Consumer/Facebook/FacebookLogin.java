package com.mimcoups.Consumer.Facebook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.NewPermissionsRequest;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.UserDetail;

public class FacebookLogin extends Activity {

	private UserDetail userDetail = new UserDetail();
	private String AccessTocken;
	private ProgressDialog PD_Dialog;
	String link = "";
	String User_ID = "";

	private static final List<String> PERMISSIONS = new ArrayList<String>() {
		{
			add("user_friends");
			add("public_profile");
			add("email");
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fbsample);

		ensureOpenSession();

	}

	private boolean ensureOpenSession() {

		boolean installed = appInstalledOrNot("com.facebook.katana");
		if (installed) {
			callFacebookLogout(getApplicationContext());
		}

		if (Session.getActiveSession() == null
				|| !Session.getActiveSession().isOpened()) {
			Session.openActiveSession(this, true, new Session.StatusCallback() {
				@Override
				public void call(Session session, SessionState state,
						Exception exception) {
					onSessionStateChanged(session, state, exception);
				}
			});
			return false;
		}
		return true;
	}

	private boolean appInstalledOrNot(String uri) {
		PackageManager pm = getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	private boolean sessionHasNecessaryPerms(Session session) {
		if (session != null && session.getPermissions() != null) {
			for (String requestedPerm : PERMISSIONS) {
				if (!session.getPermissions().contains(requestedPerm)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	private List<String> getMissingPermissions(Session session) {
		List<String> missingPerms = new ArrayList<String>(PERMISSIONS);
		if (session != null && session.getPermissions() != null) {
			for (String requestedPerm : PERMISSIONS) {
				if (session.getPermissions().contains(requestedPerm)) {
					missingPerms.remove(requestedPerm);
				}
			}
		}
		return missingPerms;
	}

	@SuppressWarnings("deprecation")
	private void onSessionStateChanged(final Session session,
			SessionState state, Exception exception) {
		final Session sessions = Session.getActiveSession();
		if (state.isOpened() && !sessionHasNecessaryPerms(session)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.need_perms_alert_text);
			builder.setPositiveButton(R.string.need_perms_alert_button_ok,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							session.requestNewReadPermissions(new NewPermissionsRequest(
									FacebookLogin.this,
									getMissingPermissions(session)));
						}
					});
			builder.setNegativeButton(R.string.need_perms_alert_button_quit,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					});
			builder.show();
		}
		if (sessions != null && sessions.isOpened()) {
			// Get the user's data

			AccessTocken = sessions.getAccessToken();

			Request.executeMeRequestAsync(sessions,
					new Request.GraphUserCallback() {

						@Override
						public void onCompleted(GraphUser user,
								Response response) {
							// TODO Auto-generated method stub

//							Log.d("Called ====>>>", sessions.getAccessToken());

							if (user != null) {
								// Display the parsed user info

//								Log.d("Facebook Data===>>",
//										user.getId()
//												+ "; "
//												+ user.getName()
//												+ "; "
//												+ (String) user
//														.getProperty("gender")
//												+ "; "
//												+ (String) user
//														.getProperty("email")
//												+ "; "
//												+ user.getBirthday()
//												+ "; "
//												+ (String) user
//														.getProperty("locale")
//												+ "; " + user.getLocation());
								try {

									userDetail.emailAddress = (String) user
											.getProperty("email").toString();
									userDetail.facebookId = response
											.getGraphObject()
											.getInnerJSONObject()
											.getString("id");
									;
									User_ID = userDetail.facebookId;
									userDetail.name = ""
											+ response.getGraphObject()
													.getInnerJSONObject()
													.getString("first_name")
											+ " "
											+ response.getGraphObject()
													.getInnerJSONObject()
													.getString("last_name");

									userDetail.username = ""
											+ response.getGraphObject()
													.getInnerJSONObject()
													.getString("name");
									;

									userDetail.gender = response
											.getGraphObject()
											.getInnerJSONObject()
											.getString("gender");
									link = response.getGraphObject()
											.getInnerJSONObject()
											.getString("link");

									new GetUserDetail().execute();

								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								// call back to register fragmnet

							}

						}
					});
		}

	}

	public static void callFacebookLogout(Context context) {
		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {

			session = new Session(context);
			Session.setActiveSession(session);

			session.closeAndClearTokenInformation();
			// clear your preferences if saved

		}

	}

	public class GetUserDetail extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			PD_Dialog = new ProgressDialog(FacebookLogin.this);
			PD_Dialog.setMessage("Please wait...");
			PD_Dialog.setCancelable(false);
			PD_Dialog.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			GetUserDetail();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			try {
				if (PD_Dialog.isShowing()) {
					PD_Dialog.dismiss();
				}
				onBackPressed();
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.onPostExecute(result);
		}

		private void GetUserDetail() {

			// TODO Auto-generated method stub
			try {

				URL myurl = new URL(
						"https://graph.facebook.com/me?access_token="
								+ AccessTocken);

//				Log.i("facebook url", "" + myurl);
				HttpURLConnection connection = (HttpURLConnection) myurl
						.openConnection();
				connection.connect();
				int code = connection.getResponseCode();
				if (code != 200) {

					// setContentView(R.layout.webimg);
				} else {
					BufferedReader dis = new BufferedReader(
							new InputStreamReader(connection.getInputStream()));

					String myString, Result = dis.readLine();
					while ((myString = dis.readLine()) != null) {

						Result += myString;
					}
					// Log.d("Result", "" + Result);
					JSONObject jsonObject = new JSONObject(Result);

					userDetail.emailAddress = jsonObject.getString("email");
					userDetail.facebookId = jsonObject.getString("id");
					User_ID = userDetail.facebookId;
					userDetail.name = "" + jsonObject.getString("name");

					if (jsonObject.has("username")) {
						userDetail.username = ""
								+ jsonObject.getString("username");
					}
					if (jsonObject.has("birthday")) {

					}

					userDetail.gender = jsonObject.getString("gender");
				}

			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block

			} catch (IOException e) {
				// TODO Auto-generated catch block

			} catch (JSONException e) {
				// TODO Auto-generated catch block

			}
			try {
				if (!User_ID.equals("")) {
					URL myurl = new URL("https://graph.facebook.com/" + User_ID
							+ "?fields=picture&access_token=" + AccessTocken);

					HttpURLConnection connection = (HttpURLConnection) myurl
							.openConnection();
					connection.connect();
					int code = connection.getResponseCode();
					if (code != 200) {

						// setContentView(R.layout.webimg);
					} else {
						BufferedReader dis = new BufferedReader(
								new InputStreamReader(
										connection.getInputStream()));
						String myString, Result = dis.readLine();
						while ((myString = dis.readLine()) != null) {
							Result += myString;
						}
						JSONObject jsonObject = new JSONObject(Result);

						JSONObject jsonObject1 = jsonObject
								.getJSONObject("picture");
						JSONObject jsonObject2 = jsonObject1
								.getJSONObject("data");
						userDetail.profileUrl = jsonObject2.getString("url");
					}
				}
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block

			} catch (IOException e) {
				// TODO Auto-generated catch block

			} catch (JSONException e) {
				// TODO Auto-generated catch block

			}
		}

	}

	@Override
	public void onBackPressed() {

		Intent i = new Intent();
		i.putExtra("userDetail", userDetail);
		setResult(Activity.RESULT_OK, i);

		super.onBackPressed();
	}
}