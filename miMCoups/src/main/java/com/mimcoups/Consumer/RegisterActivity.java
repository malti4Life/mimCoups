package com.mimcoups.Consumer;

import java.util.ArrayList;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.mimcoups.Consumer.DialogFragment.ProgressDialogFragment;
import com.mimcoups.Consumer.DialogFragment.SelectionButtonAlert;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.Consumer.Facebook.FacebookLoginActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.Service.FusedLocationService;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.customClass.DateCustomeClass;
import com.mimcoups.Consumer.customClass.WishListDetail;
import com.mimcoups.Consumer.fragment.FragmentAlreadyRegister;
import com.mimcoups.Consumer.fragment.FragmentRegister;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.alreadyRegisterInteface;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.inteface.wrongdatedialoginterface;

public class RegisterActivity extends Activity implements
		alreadyRegisterInteface, topBarInteface, wrongdatedialoginterface,
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, LocationListener {
	private DialogFragment progressDialogFragment;
	public LocationClient locationclient;
	private LocationRequest locationrequest;
	private Intent mIntentService;
	private PendingIntent mPendingIntent;
	private CommonData commondata;
	private SharedPreferences sharedPreference;
	private Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		commondata = (CommonData) getApplicationContext();
		context = RegisterActivity.this;
		sharedPreference = getSharedPreferences("UserDetail",
				Context.MODE_PRIVATE);
		if (sharedPreference.getBoolean("launch", false)) {
			if (!checkLocationService()) {
				if (!commondata.islogout) {

					DialogFragment dsp = new SelectionButtonAlert(
							new DialogInterfaceClick() {

								@Override
								public void dialogClick(String tag) {
									// TODO Auto-generated method stub

									Intent myIntent = new Intent(
											Settings.ACTION_LOCATION_SOURCE_SETTINGS);
									context.startActivity(myIntent);

								}
							}, getResources().getString(
									R.string.LocationService));
					dsp.show(getFragmentManager(), "");

				}

			}
			Editor edt = sharedPreference.edit();
			edt.putBoolean("launch", false);
			edt.commit();
		}

		/* Location GOOGLE PLAY START */

		mIntentService = new Intent(this, FusedLocationService.class);
		mPendingIntent = PendingIntent.getService(this, 1, mIntentService, 0);

		int resp = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resp == ConnectionResult.SUCCESS) {
			// Toast.makeText(this, "Connection Success" + resp,
			// Toast.LENGTH_LONG)
			// .show();
			Log.i("connection success", "connection success");
			locationclient = new LocationClient(this, this, this);
			locationclient.connect();

		} else {
			Toast.makeText(this, "Google Play Service Error " + resp,
					Toast.LENGTH_LONG).show();
		}

		/* LOCATION GOOGLE PLAY END */

		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		Fragment newFragment = new FragmentAlreadyRegister();
		String tag = newFragment.toString();
		transaction.replace(R.id.frmRegister, newFragment, tag);
		transaction.commit();

	}

	@Override
	public void alreadyRegisterClick(String message) {

		if (message.equals("Register")) {
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentRegister();
			String tag = newFragment.toString();
			transaction.replace(R.id.frmRegister, newFragment, tag);
			transaction.addToBackStack(tag);
			transaction.commit();
		} else if (message.equals("alreadyRegister")) {
			FragmentTransaction transaction = getFragmentManager()
					.beginTransaction();
			Fragment newFragment = new FragmentAlreadyRegister();
			String tag = newFragment.toString();
			transaction.replace(R.id.frmRegister, newFragment, tag);
			transaction.addToBackStack(tag);
			transaction.commit();
		} else if (message.equals("FacebookRegister")) {
			Intent iv = new Intent(this, FacebookLoginActivity.class);
			startActivity(iv);
		} else {

			View view = this.getCurrentFocus();
			if (view != null) {
				InputMethodManager inputManager = (InputMethodManager) this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(view.getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
			}
			Intent intent = new Intent(getApplicationContext(),
					DeelFeedActivity.class);
			intent.putExtra("notification", false);
			intent.putExtra("type", "");
			startActivity(intent);
			finish();
		}
		// workable

	}

	@Override
	public void topbarClick(String message) {
		// TODO Auto-generated method stub
		if (message.equals("back")) {

			View view = this.getCurrentFocus();
			if (view != null) {
				InputMethodManager inputManager = (InputMethodManager) this
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(view.getWindowToken(),
						InputMethodManager.HIDE_NOT_ALWAYS);
			}
			onBackPressed();
		}
	}

	@Override
	public void showDialog(String message) {
		// TODO Auto-generated method stub
		
		progressDialogFragment = new ProgressDialogFragment(getResources().getString(R.string.loading));
		progressDialogFragment.show(getFragmentManager(), "");
		progressDialogFragment.setCancelable(false);
	}

	@Override
	public void hideDialog() {
		// TODO Auto-generated method stub
		progressDialogFragment.dismiss();
	}

	@Override
	public void sendDatatoFragment(WishListDetail wishlist) {
		// TODO Auto-generated method stub

	}

	@Override
	public void selecteddateiswrongedialog(String message) {
		// TODO Auto-generated method stub
		DialogFragment ds = new SingleButtonAlert(new DialogInterfaceClick() {

			@Override
			public void dialogClick(String tag) {
				// TODO Auto-generated method stub

			}
		}, message);

		ds.show(getFragmentManager(), "");

	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		Log.i("LOCATION", "on location change");
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		Log.i("LOCATION", "on connection failed");
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		Log.i("LOCATION", "on connection successfull");
		locationrequest = LocationRequest.create();
		locationrequest.setInterval(180000);
		locationclient.requestLocationUpdates(locationrequest, mPendingIntent);

	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		Log.i("LOCATION", "on connection disconnect");
	}

	@Override
	public void ondateselectedevent(DateCustomeClass dateclass) {
		// TODO Auto-generated method stub

	}

	// location services

	public ArrayList<Double> getLatitudeLongitude() {
		ArrayList<Double> arrLatitude = new ArrayList<Double>();
		if (locationclient != null && locationclient.isConnected()) {
			Location loc = locationclient.getLastLocation();
			if (loc != null) {
				commondata.currentLatitude = loc.getLatitude();
				commondata.currentLongitude = loc.getLongitude();
				arrLatitude.add(loc.getLatitude());
				arrLatitude.add(loc.getLongitude());
			} else {
				arrLatitude.add(commondata.currentLatitude);
				arrLatitude.add(commondata.currentLongitude);
			}
		} else {
			arrLatitude.add(commondata.currentLatitude);
			arrLatitude.add(commondata.currentLongitude);
		}
		return arrLatitude;
	}

	public boolean checkLocationService() {
		LocationManager lm = null;
		boolean gps_enabled = false, network_enabled = false;
		if (lm == null)
			lm = (LocationManager) context
					.getSystemService(Context.LOCATION_SERVICE);
		try {
			gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
		} catch (Exception ex) {
		}
		try {
			network_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		} catch (Exception ex) {
		}

		if (!gps_enabled && !network_enabled) {
			return false;

		}
		return true;
	}

}
