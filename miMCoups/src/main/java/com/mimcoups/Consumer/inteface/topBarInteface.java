package com.mimcoups.Consumer.inteface;

import com.mimcoups.Consumer.customClass.WishListDetail;

public interface topBarInteface {
	void topbarClick(String message);

	void sendDatatoFragment(WishListDetail wishlist);

	void showDialog(String message);

	void hideDialog();
}
