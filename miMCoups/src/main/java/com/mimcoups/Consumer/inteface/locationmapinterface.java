package com.mimcoups.Consumer.inteface;

import java.util.ArrayList;

import com.mimcoups.Consumer.customClass.LocationList;

public interface locationmapinterface {
	
	
	public void go_to_map_screen_withlist(ArrayList<LocationList> location_list);
	public void go_to_map_screen_single_icon(LocationList location_details);

}
