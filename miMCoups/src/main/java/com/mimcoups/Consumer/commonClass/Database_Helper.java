package com.mimcoups.Consumer.commonClass;

import com.mimcoups.Consumer.customClass.supportchat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

public class Database_Helper {

	private Context context;
	public static String DB_name = "MimCoupsFriendDB.sqlite";
	MimCoups_DBHelper dh;
	SQLiteDatabase sdb;

	private static class MimCoups_DBHelper extends SQLiteOpenHelper {
		public MimCoups_DBHelper(Context context) {

			super(context, DB_name, null, 1);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			System.out.println("On Create Called");
			db.execSQL("Create TABLE IF NOT EXISTS Message_Detail(mid INTEGER PRIMARY KEY AUTOINCREMENT,from_id VARCHAR,to_id VARCHAR,msg TEXT,time DEFAULT CURRENT_TIMESTAMP,Msg_id VARCHAR,flag INT,isinternet INT)");
			db.execSQL("Create TABLE IF NOT EXISTS Reject_Queue(mid INTEGER PRIMARY KEY AUTOINCREMENT,rejectid VARCHAR,username VARCHAR)");

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub

			db.execSQL("DROP TABLE IF EXISTS Message_Detail");
			db.execSQL("DROP TABLE IF EXISTS Reject_Queue");
			// db.execSQL("DROP TABLE IF EXISTS CommunityDetails");
		}

	}

	public Database_Helper(Context c) {
		context = c;
	}

	public Database_Helper Open() throws SQLiteException {
		dh = new MimCoups_DBHelper(context);
		sdb = dh.getWritableDatabase();
		return this;
	}

	public void close() {
		dh.close();
	}

	public void clearData() {
		sdb.execSQL("DROP TABLE IF EXISTS Message_Detail");

	}

	public Cursor get_chatsorting() {

		String st = "select BD.id, BD.buddy_name,BD.imageurl,BD.number,BD.isbold,(select MD.msg from Message_Detail MD where MD.from_id = BD.number || '@srv1.catchbuddies.com' or MD.to_id = BD.number || '@srv1.catchbuddies.com' order by MD.time desc limit 1) as 'msg',(select MD.time from Message_Detail MD where MD.from_id = BD.number || '@srv1.catchbuddies.com' or MD.to_id = BD.number || '@srv1.catchbuddies.com' order by MD.time desc limit 1) as 'time',(select MD.msgtype from Message_Detail MD where MD.from_id = BD.number || '@srv1.catchbuddies.com' or MD.to_id = BD.number || '@srv1.catchbuddies.com' order by MD.time desc limit 1) as 'msgtype' from buddy_Detail BD order by time desc";
		Cursor rs_details = sdb.rawQuery(st, null);
		return rs_details;

	}

	public void add_Chat(supportchat chatobj) {

		ContentValues cv = new ContentValues();
		cv.put("from_id", chatobj.from_id);
		cv.put("to_id", chatobj.to_id);
		cv.put("msg", chatobj.message);
		cv.put("time", chatobj.time);
		cv.put("Msg_id", chatobj.Msg_id);
		cv.put("flag", chatobj.flag);
		cv.put("isinternet", chatobj.isinternet);

		sdb.insert("Message_Detail", null, cv);

	}

	public void add_queue(String rejectid, String username) {

		ContentValues cv = new ContentValues();
		cv.put("rejectid", rejectid);
		cv.put("username", username);

		sdb.insert("Reject_Queue", null, cv);

	}

	public Cursor getQueue() {
		String st = "SELECT * FROM Reject_Queue";
		Cursor rs_message = sdb.rawQuery(st, null);
		return rs_message;
	}

	public void removeQueue(int mid) {

		String str = "SELECT * FROM Reject_Queue where mid='" + mid + "' ";
		Cursor rs_message = sdb.rawQuery(str, null);
		if (rs_message.moveToFirst()) {

			String st = "DELETE FROM Reject_Queue where mid='" + mid + "' ";
			sdb.execSQL(st);
		}

	}

	public Cursor getAll_Message() {
		String st = "SELECT * FROM Message_Detail ORDER BY time DESC LIMIT 20";
		Cursor rs_message = sdb.rawQuery(st, null);
		return rs_message;
	}

	public Cursor get_Message(String msg_id) {
		String st = "SELECT * FROM Message_Detail where Msg_id=" + msg_id;
		Cursor rs_message = sdb.rawQuery(st, null);
		return rs_message;
	}

	public Cursor get_offinternetMessage() {
		String st = "SELECT * FROM Message_Detail where isinternet=0";
		Cursor rs_message = sdb.rawQuery(st, null);
		return rs_message;
	}

	public void remove_offlineMessage(String msgid) {

		String str = "SELECT * FROM Message_Detail where isinternet=0 and Msg_id='"
				+ msgid + "' ";
		Cursor rs_message = sdb.rawQuery(str, null);
		if (rs_message.moveToFirst()) {

			String st = "DELETE FROM Message_Detail WHERE isinternet=0 and Msg_id='"
					+ msgid + "' ";
			sdb.execSQL(st);
		}

	}

	public void update_offlineMessage(String Msg_id) {

		ContentValues cv = new ContentValues();
		cv.put("isinternet", 1);

		sdb.update("Message_Detail", cv, "Msg_id=?", new String[] { Msg_id });

	}

	public Cursor getOfflineMessage() {
		String str = "SELECT * FROM Message_Detail where isinternet=0";
		Cursor rs_message = sdb.rawQuery(str, null);
		return rs_message;
	}

	public Cursor getMessage(String from_id, String to_id) {

		String st = "SELECT * FROM (select * from Message_Detail where from_id='"
				+ from_id
				+ "' AND to_id='"
				+ to_id
				+ "' or from_id='"
				+ to_id
				+ "' AND to_id='"
				+ from_id
				+ "' ORDER BY time DESC) tmp order by tmp.mid asc";

		Cursor rs_message = sdb.rawQuery(st, null);
		return rs_message;

	}

}
