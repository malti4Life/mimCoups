package com.mimcoups.Consumer.commonClass;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.mimcoups.R;
import com.squareup.okhttp.OkHttpClient;

public class WebService_Call {

	public String response;
	public int CONNECTION_TIMEOUT = 35000;

	public String makeServiceCall_Get(String url, String custId) {
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			// appending params to url
			// if (url != null) {
			//
			// url += "?" + url;
			// }

			System.out.println("url====>" + CommonData.Base_Url + url);
			HttpGet httpGet = new HttpGet(CommonData.Base_Url + url);
			httpGet.setHeader("X-Auth-Token", custId);

			httpResponse = httpClient.execute(httpGet);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ConnectTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();
		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return response;

	}

	// get
	public String makeServicegetRequest(String url) {

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			// appending params to url
			// if (url != null) {
			//
			// url += "?" + url;
			// }
			// System.out.println("url====>" + CommonData.Base_Url + url);
			HttpGet httpGet = new HttpGet(CommonData.Base_Url + url);
			httpResponse = httpClient.execute(httpGet);
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ConnectTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();
		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}


		return response;


	}
	
	public String GetOffline(String url) {

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			// appending params to url
			// if (url != null) {
			//
			// url += "?" + url;
			// }
			// System.out.println("url====>" + CommonData.Base_Url + url);
			HttpGet httpGet = new HttpGet(url);
			httpResponse = httpClient.execute(httpGet);
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ConnectTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();
		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}


		return response;

	
	}

	public String makeServicegetRequestFriend(String url) {

		String response = null;

		OkHttpClient client = new OkHttpClient();

		// Ignore invalid SSL endpoints.
		client.setHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String s, SSLSession sslSession) {
				return true;
			}
		});
		HttpURLConnection connection;
		try {
			connection = client.open(new URL(url));
			connection.setConnectTimeout(CONNECTION_TIMEOUT);
			InputStream is = connection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);

			BufferedReader r = new BufferedReader(isr);
			StringBuilder total = new StringBuilder();
			String line;
			while ((line = r.readLine()) != null) {
				total.append(line);
			}

			response = total.toString();
		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;

	}

	public String makeServiceImageUpload(String url, File file, boolean isupdate) {


		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);
			HttpPost httpPost;

			if (isupdate) {
				MultipartEntity mpEntity = new MultipartEntity();
				ContentBody cbFile = new FileBody(file, "image/jpeg");
				// Add the data to the multipart entity
				mpEntity.addPart("profileimage", cbFile);
				httpPost = new HttpPost(CommonData.Base_Url + url);
				httpPost.setEntity(mpEntity);
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
			} else {
				httpPost = new HttpPost(CommonData.Base_Url + url);
				httpResponse = httpClient.execute(httpPost);
				httpEntity = httpResponse.getEntity();
			}

			Log.e("Image Upload",CommonData.Base_Url + url);

			response = EntityUtils.toString(httpEntity);
		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Exceptions ==",e.getMessage());
		}
		return response;
	}

	public String makeServicePostContactUs(String url, String emailAddress,
			String message, String customerid, String languageid) {
		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("customerid", customerid));
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));
		nameValuePairs.add(new BasicNameValuePair("email", emailAddress));
		nameValuePairs.add(new BasicNameValuePair("message", message));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServicePostLocationContactUs(String url,
			String customerid, String languageid, String locationid,
			String emailAddress, String message) {
		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("customerid", customerid));
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));
		nameValuePairs.add(new BasicNameValuePair("locationid", locationid));
		nameValuePairs.add(new BasicNameValuePair("email", emailAddress));
		nameValuePairs.add(new BasicNameValuePair("message", message));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);
		} catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServiceFavouriteCategory(String url, String category,
			String languageid, String customerid) {
		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("customerid", customerid));
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));
		nameValuePairs.add(new BasicNameValuePair("category", category));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServiceFavouriteDeals(String url, String category,
			String languageid, String customerid) {
		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("customerid", customerid));
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));
		nameValuePairs.add(new BasicNameValuePair("deal", category));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServiceFavouriteLocations(String url, String category,
			String languageid, String customerid) {
		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("customerid", customerid));
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));
		nameValuePairs.add(new BasicNameValuePair("location", category));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServiceAddCampaign(String url, String json)
			throws Exception {

		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("bringininvitations", json));
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeFriendAcceptService(String url, String languageid,
			String customerid, String invitedcustomerid,
			String friendscustomerid) throws Exception {

		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));
		nameValuePairs.add(new BasicNameValuePair("customerid", customerid));
		nameValuePairs.add(new BasicNameValuePair("invitedcustomerid",
				invitedcustomerid));
		nameValuePairs.add(new BasicNameValuePair("friendscustomerid",
				friendscustomerid));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServiceAddDealFavourites(String url, String dealid,
			String customerid, String languageid, String ratingCount,
			String strReview) {
		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("dealid", dealid));
		nameValuePairs.add(new BasicNameValuePair("customerid", customerid));
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));
		nameValuePairs.add(new BasicNameValuePair("rating", ratingCount));
		nameValuePairs.add(new BasicNameValuePair("review", strReview));
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServiceAddLocationFavourites(String url,
			String merchantStoreId, String customerid, String languageid,
			String ratingCount, String strReview) {

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("merchantstoreid",
				merchantStoreId));
		nameValuePairs.add(new BasicNameValuePair("customerid", customerid));
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));
		nameValuePairs.add(new BasicNameValuePair("rating", ratingCount));
		nameValuePairs.add(new BasicNameValuePair("reviewmessage", strReview));
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServicePostLogout(String url, String deviceid,
			String languageid) {
		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("deviceid", deviceid));
		nameValuePairs.add(new BasicNameValuePair("device", "android"));
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServiceShareViaEmail(String url, String json)
			throws Exception {
		// TODO Auto-generated method stub
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("shareviaemail", json));
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);
			// Checking http request method type
			// Log.e("Share Deal",
			// CommonData.Base_Url + url + "=====" + json.toString());
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpResponse = httpClient.execute(httpPost);
			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);
		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeServiceShareViaMimCup(String url, String json)
			throws Exception {

		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("shareviamims", json));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String makeorder(String url, List<NameValuePair> nameValuePairs) {

		// List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		// nameValuePairs
		// .add(new BasicNameValuePair("customerid", "" + customerid));
		// nameValuePairs
		// .add(new BasicNameValuePair("languageid", "" + languageid));
		// nameValuePairs.add(new BasicNameValuePair("dealid", "" + dealid));
		// nameValuePairs.add(new BasicNameValuePair("quantity", "" +
		// quantity));
		// nameValuePairs.add(new BasicNameValuePair("purchasedamount", ""
		// + purchasedamount));
		// nameValuePairs.add(new BasicNameValuePair("shippingstreetnumber", ""
		// + shippingstreetnumber));
		// nameValuePairs.add(new BasicNameValuePair("shippingstreetaddress", ""
		// + shippingstreetaddress));
		// nameValuePairs.add(new BasicNameValuePair("shippingcityname", ""
		// + shippingcityname));
		// nameValuePairs.add(new BasicNameValuePair("shippingzipcode", ""
		// + shippingzipcode));
		// nameValuePairs.add(new BasicNameValuePair("billingstreetnumber", ""
		// + billingstreetnumber));
		// nameValuePairs.add(new BasicNameValuePair("billingstreetcityname", ""
		// + billingstreetcityname));
		// nameValuePairs.add(new BasicNameValuePair("billingzipcode", ""
		// + billingzipcode));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(CommonData.Base_Url + url);

			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("X-Auth-Token", "MIMCOUPS"
					+ CommonData.CustomerId);

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}
	
	
	public String makeorder_local(String url, List<NameValuePair> nameValuePairs) {

		// List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		// nameValuePairs
		// .add(new BasicNameValuePair("customerid", "" + customerid));
		// nameValuePairs
		// .add(new BasicNameValuePair("languageid", "" + languageid));
		// nameValuePairs.add(new BasicNameValuePair("dealid", "" + dealid));
		// nameValuePairs.add(new BasicNameValuePair("quantity", "" +
		// quantity));
		// nameValuePairs.add(new BasicNameValuePair("purchasedamount", ""
		// + purchasedamount));
		// nameValuePairs.add(new BasicNameValuePair("shippingstreetnumber", ""
		// + shippingstreetnumber));
		// nameValuePairs.add(new BasicNameValuePair("shippingstreetaddress", ""
		// + shippingstreetaddress));
		// nameValuePairs.add(new BasicNameValuePair("shippingcityname", ""
		// + shippingcityname));
		// nameValuePairs.add(new BasicNameValuePair("shippingzipcode", ""
		// + shippingzipcode));
		// nameValuePairs.add(new BasicNameValuePair("billingstreetnumber", ""
		// + billingstreetnumber));
		// nameValuePairs.add(new BasicNameValuePair("billingstreetcityname", ""
		// + billingstreetcityname));
		// nameValuePairs.add(new BasicNameValuePair("billingzipcode", ""
		// + billingzipcode));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost( url);

			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("X-Auth-Token", "MIMCOUPS"
					+ CommonData.CustomerId);

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();

			Log.e("Exceptions",e.getMessage());

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", e.getMessage().toString());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();
		}

		return response;

	}

	// send notification
	public String Send_Notification(String url, String customerid,
			String languageid, String message) {
		// TODO Auto-generated method stub

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("customerid", customerid));
		nameValuePairs.add(new BasicNameValuePair("languageid", languageid));
		nameValuePairs.add(new BasicNameValuePair("message", message));

		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;

			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);

			// Checking http request method type
			HttpPost httpPost = new HttpPost(url);
			// adding post params

			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			httpResponse = httpClient.execute(httpPost);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

		}

		catch (ConnectTimeoutException e) {

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", R.string.PoorInternetConnect);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	public String Get_Payment_Details(String url) {

		String response = "";
		int CONNECTION_TIMEOUT = 35000;
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			// appending params to url
			// if (url != null) {
			//
			// url += "?" + url;
			// }

			HttpGet httpGet = new HttpGet(url);

			httpResponse = httpClient.execute(httpGet);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

			// JSONObject obj = new JSONObject();
			// try {
			// obj.put("vStatusCode", "100");
			// obj.put("vMessageResponse", response);
			// } catch (JSONException e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// // }
			//
			// response = obj.toString();

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (ConnectTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", "no Connections");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();
		} catch (SocketTimeoutException e) {
			// Here Connection TimeOut excepion
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", "No Connections");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return response;

	}

	public String Get_Payment(String url) throws SocketTimeoutException, ConnectTimeoutException, UnsupportedEncodingException, ClientProtocolException {

		String response = "";
		int CONNECTION_TIMEOUT = 35000;
		try {
			// http client
			DefaultHttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams()
					.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
							CONNECTION_TIMEOUT);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, CONNECTION_TIMEOUT);
			HttpEntity httpEntity = null;
			HttpResponse httpResponse = null;
			// appending params to url
			// if (url != null) {
			//
			// url += "?" + url;
			// }

			HttpGet httpGet = new HttpGet(url);

			httpResponse = httpClient.execute(httpGet);

			httpEntity = httpResponse.getEntity();
			response = EntityUtils.toString(httpEntity);

			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "100");
				obj.put("vMessageResponse", response);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				// }

				response = obj.toString();

			}

		} catch (Exception e) {
			Log.e("Exceptions", e.getMessage());
			
			JSONObject obj = new JSONObject();
			try {
				obj.put("vStatusCode", "420");
				obj.put("vMessageResponse", "No Connections");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			response = obj.toString();

		}
		return response;
	}

}
