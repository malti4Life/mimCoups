package com.mimcoups.Consumer.commonClass;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import org.jivesoftware.smack.Connection;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.widget.Toast;

import com.mimcoups.R;
import com.mimcoups.Consumer.customClass.ChatUser;
import com.mimcoups.Consumer.customClass.Pendingrequest;
import com.mimcoups.Consumer.customClass.supportchat;
import com.mimcoups.Consumer.inteface.RefereshPrevieousFragment;
import com.paypal.android.sdk.payments.PayPalConfiguration;

public class CommonData extends Application {
	
	// Used below  mail id for facebook,GCM and map key
	// email : mimcoups@gmail.com	
	// pass  :  this.admin
	// SH1 Hash key :: 34:13:67:81:CF:B0:11:EC:E8:85:F4:6C:75:5D:E9:B5:8B:9A:7E:E5



	/*
	 * 
	 * mail id::- mimcoups.android@gmail.com password::- this.admin
	 * 
	 * hash key for map::-
	 * 
	 * 34:13:67:81:CF:B0:11:EC:E8:85:F4:6C:75:5D:E9:B5:8B:9A:7E:E5
	 * 
	 * map key ::- AIzaSyBIcC0mURJtEBRBwUM4DviXHMUvslE5yMM    //  AIzaSyB3noxEw7Ytdbf3mUW5LGJuUzs67D4iGzQ
	 */
	// GCM FROM android.zealousweb email   -  android.admin
	public static String FACEBOOK_APPID = "1010256142337872";
	// Facebook mimcoups@gmail.com -    this.admin
	
	//public static String SENDER_ID = "333435711326"; // AIzaSyAVGCbhuS6vrapNpujxByk1BD6D33wFsQw
	
	public static String SENDER_ID = "225205483336"; // AIzaSyDEV-K1BUP_0tKex-ob13m2an6rfCk-Npg
	
	
	 public static String Base_Url = "http://35.162.148.219/";
//	public static String Base_Url = "http://192.168.1.123/mimcoups/";


	 
	 public static String dealshareUrl_au="http://www.mimcoups.com.au/deal-share?id=";
	 public static String dealshareUrl_no=" http://www.mimcoups.no/deal-share?id=";
	 
	 
	 public static String locationshareUrl_au="http://www.mimcoups.com.au/location-share?id=";
	 public static String locationshareUrl_no="http://www.mimcoups.no/location-share?id=";

//	public static String Base_Url = "http://54.148.117.181/";
	public static String StateUrl="CustomerService.svc/GetAllStatesByLanguageId?languageid=1";
	
	public static String deviceId = "";
	public static double currentLatitude = 0.0;
	public static double currentLongitude = 0.0;
	public static String languageId = "1";
	public static String CustomerId = "";
	public String ProfiileImagePath;
	public String ProfileName;
	public boolean islogout = false;
	// public String SdCardPath = Environment.getExternalStorageDirectory()
	// .getAbsolutePath();
	public static final String DISPLAY_MESSAGE_ACTION = "com.mimcoups.commonClass.DISPLAY_MESSAGE";
	// hardik
	public String strPassword;
	public String strUsername;
	public String strServerName = "mimcoupschat.com";
	public Connection xmppconnection;
	public String representativeid = "";
	// public boolean acceptrequest;
	public ArrayList<Pendingrequest> arrpendingrequest = new ArrayList<Pendingrequest>();
	public ArrayList<supportchat> arrpausechat = new ArrayList<supportchat>();
	public ArrayList<supportchat> pendingsupportchat = new ArrayList<supportchat>();

	public ArrayList<ChatUser> arrchats;
	public String currentfriendstoid = "";
	// acceptrequest 0 mean false
	// accept request 1 mean true
	public int acceptrequest;
	public supportchat supportChatsend;
	// paypal and cardio ::-
	public static String access_token = "";
	public static String card_id = "";
	public static String credit_card = "";
	public static String credit_card_holder = "";
	public static String Expiry_month = "", Expiry_Year = "", CVV = "",
			Addres = "", Type = "";
	public static String authorization_code = "";
	public static String type = "";
	public static boolean ispayment = true;
	public static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
	// card io
	public static final String MY_CARDIO_APP_TOKEN = "93015019f1d94211b9a5c45c67cafe4e";

	public boolean ismerchantoffline = false;
	
	public static int connectCount=0;

	// live
	// public static String Get_Token_Access =
	// "https://api.paypal.com/v1/oauth2/token";
	// public static String Get_Card_ID =
	// "https://api.paypal.com/v1/vault/credit-card";
	// public static String Get_Payment
	// ="https://api.paypal.com/v1/payments/payment";

	// sandbox
	public static String Get_Token_Access = "https://api.sandbox.paypal.com/v1/oauth2/token";
	public static String Get_Card_ID = "https://api.sandbox.paypal.com/v1/vault/credit-card";
	public static String Get_Payment = "https://api.sandbox.paypal.com/v1/payments/payment";

	public static final String CONFIG_CLIENT_ID = "AcHlMRBZEvUITvV31X3pBynRZjYVcKs__1hiYfO_89L9rWDVFq8oz_mr0Q1K";
	public static final String SECRET = "ENl6UxBxkd20UjgexV0TdqzZgWNxwohgoqb2LUeWblSQ_VCOCMOY7OiZRlHc";
	
	public static String Payment_details=Base_Url+"CustomerService.svc/GetPaypalDetails?languageid=";
	

	RefereshPrevieousFragment mfragment;

	public void setObserver() {
		mfragment = new RefereshPrevieousFragment();
	}

	public void setNull() {
		mfragment = null;
	}

	public RefereshPrevieousFragment getObserver() {
		return mfragment;
	}

	public ArrayList<String> arrRpListStore = new ArrayList<String>();

	public static void displayMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra("message", message);
		context.sendBroadcast(intent);
	}

	public void toastDisplay(String message) {
		Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_LONG)
				.show();
	}

	public boolean checkInternetConnection() {
		ConnectivityManager cm = (ConnectivityManager) getApplicationContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetwork = cm
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetwork != null && wifiNetwork.isConnected()) {
			return true;
		}
		NetworkInfo mobileNetwork = cm
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		if (mobileNetwork != null && mobileNetwork.isConnected()) {
			return true;
		}
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if (activeNetwork != null && activeNetwork.isConnected()) {
			return true;
		}
		return false;
	}

	public void NotificationClick(String message) {
		// TODO Auto-generated method stub

	}

	public String getMD5EncryptedString(String encTarget) {
		MessageDigest mdEnc = null;
		try {
			mdEnc = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} // Encryption algorithm
		mdEnc.update(encTarget.getBytes(), 0, encTarget.length());
		String md5 = new BigInteger(1, mdEnc.digest()).toString(16);
		while (md5.length() < 32) {
			md5 = "0" + md5;
		}
		return md5;
	}

	public static String Url_EncodeWithBlank(String s) {
		StringBuffer build = new StringBuffer(s.length());
		for (int i = 0; i < s.length(); ++i) {
			switch (s.charAt(i)) {
			case ' ':
				build.append("%20");
				break;
			case '+':
				build.append("%2b");
				break;
			case '\'':
				build.append("%27");
				break;
			case '<':
				build.append("%3c");
				break;
			case '>':
				build.append("%3e");
				break;
			case '#':
				build.append("%23");
				break;
			// case '%':
			// build.append("%25");
			// break;
			case '{':
				build.append("%7b");
				break;
			
			case '}':
				build.append("%7d");
				break;
			case '\\':
				build.append("%5c");
				break;
			case '^':
				build.append("%5e");
				break;
			case '~':
				build.append("%73");
				break;
			case '[':
				build.append("%5b");
				break;
			case ']':
				build.append("%5d");
				break;

			case '\n':
				build.append("i%0A");
				break;

			default:
				build.append(s.charAt(i));
				break;
			}
		}
		return build.toString();
	}

	public static String Url_EncodeWithOUTBlank(String s) {
		StringBuffer build = new StringBuffer(s.length());
		for (int i = 0; i < s.length(); ++i) {
			switch (s.charAt(i)) {

				case '+':
					build.append("%2b");
					break;
				case '\'':
					build.append("%27");
					break;
				case '<':
					build.append("%3c");
					break;
				case '>':
					build.append("%3e");
					break;
				case '#':
					build.append("%23");
					break;
				// case '%':
				// build.append("%25");
				// break;
				case '{':
					build.append("%7b");
					break;

				case '}':
					build.append("%7d");
					break;
				case '\\':
					build.append("%5c");
					break;
				case '^':
					build.append("%5e");
					break;
				case '~':
					build.append("%73");
					break;
				case '[':
					build.append("%5b");
					break;
				case ']':
					build.append("%5d");
					break;

				case '\n':
					build.append("i%0A");
					break;

				default:
					build.append(s.charAt(i));
					break;
			}
		}
		return build.toString();
	}
	
	
	public static String Url_Encode(String s) {
		StringBuffer build = new StringBuffer(s.length());
		for (int i = 0; i < s.length(); ++i) {
			switch (s.charAt(i)) {
			
			
			 case '%':
			 build.append("%25");
			 break;
			

			default:
				build.append(s.charAt(i));
				break;
			}
		}
		return build.toString();
	}
	
	public String Get_ReturnUrl(String langId){
		if(langId.equalsIgnoreCase("1")){
			
			return "http://www.mimcoups.com.au/thank-you";
		}else{
			return "http://www.mimcoups.no/takk";
		}
		
	}
	public String Get_CancelUrl(String langId){
		if(langId.equalsIgnoreCase("1")){
			
			return "http://www.mimcoups.com.au/error";
		}else{
			return "http://www.mimcoups.no/feil";
		}
		
	}

	public void removefolder() {
		String dir = Environment.getExternalStorageDirectory() + File.separator
				+ "." + getResources().getString(R.string.app_name);
		File file = new File(dir);
		if (file.isDirectory()) {
			String[] children = file.list();
			for (int i = 0; i < children.length; i++) {
				new File(dir, children[i]).delete();
			}
			file.delete();
		}

	}
}
