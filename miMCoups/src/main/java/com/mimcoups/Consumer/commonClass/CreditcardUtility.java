package com.mimcoups.Consumer.commonClass;

import java.util.ArrayList;

public class CreditcardUtility {

//	American Express 34, 37;====15
//	Discover 6011, 622126 to 622925, 644, 645, 646, 647, 648, 649, 65 =====16
//	MasterCard	51, 52, 53, 54, 55 =======16 to 19
//	Visa	4=====13====16
	
	
//	visa, mastercard, discover, amex
	
	
	public static ArrayList<String> GetDiscoverLimit(){
		
		
		ArrayList<String> arr_limit = new ArrayList<String>();
		arr_limit.add("6011");
		arr_limit.add("648");
		arr_limit.add("648");
		arr_limit.add("647");
		arr_limit.add("646");
		arr_limit.add("645");
		arr_limit.add("644");
		arr_limit.add("65");
		for(int i = 622126; i<=622925; i++){
			arr_limit.add(i+"");
		}
		
		return arr_limit;
		
	}
	
	public static  Boolean validatCardNumber(String CardNumber){
		
		Boolean is_valid = false;
		if(CardNumber.trim().length() >13){
			
			//Visa Card Section
			if(CardNumber.substring(0, 1).equals("4")){
				
				if(CardNumber.length() >= 13 && CardNumber.length() <= 16){
					is_valid = true;
				}else{
					is_valid = false;
				}
				
			}
			
			
			// Master Card Section
			else if(CardNumber.substring(0, 2).equalsIgnoreCase("51") || CardNumber.substring(0, 2).equalsIgnoreCase("52") || CardNumber.substring(0, 2).equalsIgnoreCase("53") ||
					CardNumber.substring(0, 2).equalsIgnoreCase("54") || CardNumber.substring(0, 2).equalsIgnoreCase("55")){
				
				if(CardNumber.length() >= 16 && CardNumber.length() <= 19){
					is_valid = true;
				}else{
					is_valid = false;
				}
			}
			
			
			//American Express
			else if(CardNumber.substring(0, 2).equalsIgnoreCase("34") ||
					CardNumber.substring(0, 2).equalsIgnoreCase("37")){
				if(CardNumber.length() == 15){
					is_valid = true;
				}else{
					is_valid = false;
				}
			}
			
			//Discover 
			else if(GetDiscoverLimit().contains(CardNumber.substring(0, 2)) || 
					GetDiscoverLimit().contains(CardNumber.substring(0, 3)) || 
					GetDiscoverLimit().contains(CardNumber.substring(0, 4)) || 
					GetDiscoverLimit().contains(CardNumber.substring(0, 6))){
				if(CardNumber.length() == 16){
					is_valid =  true;
				}else{
					is_valid =  false;
				}
			}
			
		}else{
			
			is_valid =  false;
			
		}
		return is_valid;
	
		
		
	}
	
	
	public static String  CardType(String CardNumber){
		
		String TYPE = "";
		if(CardNumber.trim().length() >13){
			
			//Visa Card Section
			if(CardNumber.substring(0, 1).equals("4")){
				TYPE = "visa";
			}
			
			// Master Card Section
			else if(CardNumber.substring(0, 2).equalsIgnoreCase("51") || CardNumber.substring(0, 2).equalsIgnoreCase("52") || CardNumber.substring(0, 2).equalsIgnoreCase("53") ||
					CardNumber.substring(0, 2).equalsIgnoreCase("54") || CardNumber.substring(0, 2).equalsIgnoreCase("55")){
			
				TYPE = "mastercard";
			}
			
			//American Express
			else if(CardNumber.substring(0, 2).equalsIgnoreCase("34") ||
					CardNumber.substring(0, 2).equalsIgnoreCase("37")){
				
				
				TYPE = "amex";
			}
			
			//Discover 
			else if(GetDiscoverLimit().contains(CardNumber.substring(0, 2)) || 
					GetDiscoverLimit().contains(CardNumber.substring(0, 3)) || 
					GetDiscoverLimit().contains(CardNumber.substring(0, 4)) || 
					GetDiscoverLimit().contains(CardNumber.substring(0, 6))){
				
				TYPE = "discover";
			}
			
			
			
			
		}else{
			TYPE ="";
			
		}
		
		return TYPE;
	
		
		
	}
}
