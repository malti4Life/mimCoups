package com.mimcoups.Consumer.instragramSharing;

import java.io.FileNotFoundException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;

import com.mimcoups.R;

public class InstaActivity extends Activity {

	private InstaImpl mInstaImpl;
	private Context mContext;
	private ResponseListener mResponseListener;
	int sharingfrom;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		setContentView(R.layout.fbsample);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			sharingfrom = extras.getInt("sharingfrom");

			if (sharingfrom == 0) {

			}
			if (sharingfrom == 1) {
			}
			if (sharingfrom == 2) {
			}

		}

		mResponseListener = new ResponseListener();
		mInstaImpl = new InstaImpl(this);
		mInstaImpl.setAuthAuthenticationListener(new AuthListener());
	}

	public class AuthListener implements
			InstaImpl.AuthAuthenticationListener {
		@Override
		public void onSuccess() {
			Toast.makeText(InstaActivity.this,
					"Instagram Authorization Successful", Toast.LENGTH_SHORT)
					.show();
		}

		@Override
		public void onFail(String error) {
			Toast.makeText(InstaActivity.this, "Authorization Failed",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		IntentFilter filter = new IntentFilter();
		filter.addAction("com.varundroid.instademo.responselistener");
		filter.addCategory("com.varundroid.instademo");
		registerReceiver(mResponseListener, filter);
	}

	@Override
	protected void onPause() {
		unregisterReceiver(mResponseListener);
		super.onPause();
	}

	public class ResponseListener extends BroadcastReceiver {

		public static final String ACTION_RESPONSE = "com.varundroid.instademo.responselistener";
		public static final String EXTRA_NAME = "90293d69-2eae-4ccd-b36c-a8d0c4c1bec6";
		public static final String EXTRA_ACCESS_TOKEN = "bed6838a-65b0-44c9-ab91-ea404aa9eefc";

		@Override
		public void onReceive(Context context, Intent intent) {
			mInstaImpl.dismissDialog();
			Bundle extras = intent.getExtras();
			String name = extras.getString(EXTRA_NAME);
			String accessToken = extras.getString(EXTRA_ACCESS_TOKEN);
			final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
					mContext);
			alertDialog.setTitle("Your Details");
			alertDialog.setMessage("Name - " + name + ", Access Token - "
					+ accessToken);
			alertDialog.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = getPackageManager()
									.getLaunchIntentForPackage(
											"com.instagram.android");
							if (intent != null) {
								Intent shareIntent = new Intent();
								shareIntent.setAction(Intent.ACTION_SEND);
								shareIntent.setPackage("com.instagram.android");
								try {
									String path = Environment
											.getExternalStorageDirectory()
											.getAbsolutePath()
											+ "/CatchBuddies/Images/Profile_Pic.jpg";
									shareIntent.putExtra(
											Intent.EXTRA_STREAM,
											Uri.parse(MediaStore.Images.Media
													.insertImage(
															getContentResolver(),
															path, "I am Happy",
															"Share happy !")));
								} catch (FileNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								shareIntent.setType("image/jpeg");

								startActivity(shareIntent);
							} else {
								// bring user to the market to download the app.
								// or let them choose an app?
//								Log.i("DATA", "calll");

								intent = new Intent(Intent.ACTION_VIEW);
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								intent.setData(Uri.parse("market://details?id="
										+ "com.instagram.android"));
								startActivity(intent);
							}

						}
					});
			alertDialog.show();

		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
}
