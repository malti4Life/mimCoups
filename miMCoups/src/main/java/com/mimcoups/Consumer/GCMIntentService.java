package com.mimcoups.Consumer;

import static com.mimcoups.Consumer.commonClass.CommonData.SENDER_ID;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.mimcoups.R;
import com.mimcoups.Consumer.commonClass.CommonData;

public class GCMIntentService extends GCMBaseIntentService {

	public GCMIntentService() {
		super(SENDER_ID);
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		CommonData.deviceId = registrationId;
		Log.e("registration id is>>	", "" + registrationId);
	}

	@Override
	protected void onUnregistered(Context context, String registrationId) {
	}

	/**
	 * Method called on Receiving a new message
	 * */
	@Override
	protected void onMessage(Context context, Intent intent) {

		String message = intent.getExtras().getString("message");
		String customerid = intent.getExtras().getString("customerid");
		// String languageid = intent.getExtras().getString("languageid");
		String type = intent.getExtras().getString("type");
		generateNotification(context, message, customerid, type);

	}

	/**
	 * Method called on receiving a deleted message
	 * */
	@Override
	protected void onDeletedMessages(Context context, int total) {
	}

	/**
	 * Method called on Error
	 * */
	@Override
	public void onError(Context context, String errorId) {
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */

	private static void generateNotification(Context context, String message,
			String custId, String type) {
		// Log.e("Notifications test", message);
		// Log.e("customerID", custId);
		// Log.e("Type", type);
		SharedPreferences preference = context.getSharedPreferences(
				"UserDetail", Context.MODE_PRIVATE);

		String customerid = preference.getString("customerid", "");
		// Log.d("customerid is>>", "" + customerid);

		if (customerid != "" && custId.trim().equalsIgnoreCase(customerid)) {

			// if (!isApplicationBroughtToBackground(context)) {

			int icon = R.drawable.app_icon;
			long when = System.currentTimeMillis();
			NotificationManager notificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			Notification notification = new Notification(icon, message, when);
			notification.sound = Uri.parse("android.resource://"
					+ context.getPackageName() + R.raw.mimcoupssound);
			notification.defaults = Notification.DEFAULT_LIGHTS
					| Notification.DEFAULT_VIBRATE;
			String title = context.getString(R.string.app_name);

			// ((CommonData) context).NotificationClick(message);

			Intent notificationIntent = new Intent(context,
					DeelFeedActivity.class);
			notificationIntent.putExtra("notification", true);

			Log.d("put type>>", "" + type);
			notificationIntent.putExtra("type", "" + type);
			notificationIntent.putExtra("msg", "" + message);

			// set intent so it does not start a new activity
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			PendingIntent intent = PendingIntent.getActivity(context, 0,
					notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
			notification.setLatestEventInfo(context, title, message, intent);
			notification.flags |= Notification.FLAG_AUTO_CANCEL;

			// Play default notification sound
			notification.defaults |= Notification.DEFAULT_SOUND;

			// Vibrate if vibrate is enabled
			notification.defaults |= Notification.DEFAULT_VIBRATE;
			notificationManager.notify(0, notification);

			Object sbservice = context.getSystemService("statusbar");
			Class<?> statusbarManager;
			try {
				statusbarManager = Class
						.forName("android.app.StatusBarManager");
				Method showsb = statusbarManager.getMethod("expand");
				showsb.invoke(sbservice);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		// }

	}

	// private static boolean isApplicationBroughtToBackground(Context ctx) {
	// boolean isback = false;
	// ActivityManager am = (ActivityManager) ctx
	// .getSystemService(Context.ACTIVITY_SERVICE);
	// List<RunningTaskInfo> tasks = am.getRunningTasks(1);
	// if (!tasks.isEmpty()) {
	// ComponentName topActivity = tasks.get(0).topActivity;
	// if (topActivity.getClassName().toString()
	// .equalsIgnoreCase("com.android.MimCoup.DeelFeedActivity")) {
	// isback = true;
	// } else {
	// isback = false;
	// }
	// }
	// return isback;
	// }
}
