package com.mimcoups.Consumer;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.model.people.Person;
import com.mimcoups.Consumer.customClass.UserDetail;
import com.mimcoups.R;

public class SignInActivity extends Activity implements ConnectionCallbacks,
		OnConnectionFailedListener {

	private GoogleApiClient mGoogleApiClient;

	private boolean mIntentInProgress;
	private static final int RC_SIGN_IN = 0;
	private UserDetail userDetail = new UserDetail();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gplus);
		// startService(new Intent(getApplicationContext(), GPSTracker.class));

		try {
			mGoogleApiClient = new GoogleApiClient.Builder(this)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this).addApi(Plus.API)
					.addScope(Plus.SCOPE_PLUS_LOGIN).build();

			mGoogleApiClient.connect();
			int arg = ConnectionResult.SIGN_IN_REQUIRED;
			int flag = getIntent().getFlags();
			if (flag == 1) {
				PlusShare.Builder builder = new PlusShare.Builder(this);

				// Set call-to-action metadata.
				builder.addCallToAction("CREATE_ITEM", /**
				 * call-to-action button
				 * label
				 */
				Uri.parse("http://plus.google.com/pages/create"), /**
				
				 */
				"/pages/create"/**
				 * call to action deep-link ID (for mobile use),
				 * 512 characters or fewer
				 */
				);

				// Set the content url (for desktop use).
				builder.setContentUrl(Uri
						.parse("https://plus.google.com/pages/"));

				// Set the target deep-link ID (for mobile use).
				builder.setContentDeepLinkId("/pages/", null, null, null);

				// Set the share text.
				builder.setText("Create your Google+ Page too!");

				startActivityForResult(builder.getIntent(), 0);
			}

		} catch (Exception e) {
			Log.i("exception ", "exception");
		}

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

	}

	private void getProfileInformation() {

		try {
			if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
				userDetail = new UserDetail();
				Person currentPerson = Plus.PeopleApi
						.getCurrentPerson(mGoogleApiClient);
				String personName = currentPerson.getDisplayName();

				String personGooglePlusProfile = currentPerson.getImage()
						.getUrl();
				;
				String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
				String birthday = currentPerson.getBirthday();
				String currentlocation = currentPerson.getCurrentLocation();
				String googleplusId = currentPerson.getId();

				int gender = currentPerson.getGender();

				// Log.i("GOOGLE PLUS>>", "" + "Birthdate : " + birthday
				// + "\n Gender : " + gender + "\n  Name :" + personName
				// + " \n email :" + email + " \n current Location :"
				// + currentlocation + "\n \n Profile :"
				// + personGooglePlusProfile);

				userDetail.emailAddress = email;
				userDetail.googleplusId = googleplusId;
				userDetail.name = personName;
				userDetail.birthDate = birthday;

				String genderval = "";

				if (gender == 0) {
					genderval = "male";
				}
				if (gender == 1) {
					genderval = "female";
				}
				userDetail.gender = genderval;
				userDetail.profileUrl = personGooglePlusProfile;

				onBackPressed();

			} else {
				// Toast.makeText(this, "Person information is null",
				// Toast.LENGTH_LONG).show();

				Toast.makeText(this, "Plz Try Later", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		if (!mIntentInProgress && arg0.hasResolution()) {
			try {
				mIntentInProgress = true;
				arg0.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				// The intent was canceled before it was sent. Return to the
				// default
				// state and attempt to connect to get an updated
				// ConnectionResult.
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}

		}

		// Log.e("error", "error code" + arg0.getResolution());
		// getProfileInformation();
		// onBackPressed();

	}

	@Override
	public void onConnected(Bundle arg0) {

		getProfileInformation();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		try {

			Log.d("on activity re", "on activity" + resultCode);

			if (resultCode == 0) {
				Intent i = new Intent();
				i.putExtra("userDetail", userDetail);
				setResult(Activity.RESULT_CANCELED, i);
				finish();
			} else {

				if (requestCode == RC_SIGN_IN) {
					mIntentInProgress = false;
					if (!mGoogleApiClient.isConnecting()) {
						mGoogleApiClient.connect();
					}
				}
			}

		} catch (Exception e) {

			Log.i("exception >>", "" + e.toString());

		}
	}

	@Override
	public void onBackPressed() {

		Log.d("on backkkk", "backk");

		Intent i = new Intent();
		i.putExtra("userDetail", userDetail);
		setResult(Activity.RESULT_OK, i);
		super.onBackPressed();
	}

	@Override
	public void onConnectionSuspended(int arg0) {

	}

}
