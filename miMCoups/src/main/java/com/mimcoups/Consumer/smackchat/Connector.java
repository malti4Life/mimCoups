package com.mimcoups.Consumer.smackchat;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Messenger;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.mimcoups.Consumer.DeelFeedActivity;
import com.mimcoups.R;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.commonClass.Database_Helper;
import com.mimcoups.Consumer.customClass.supportchat;

public class Connector extends Service {

	String TAG = "Service";
	Handler mHandler;
	public String HOST;
	public int PORT = 5222;
	public String SERVICE;
	ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	public static final int MSG_REGISTER_CLIENT = 1;
	public static final int MSG_SET_STRING_VALUE = 2;
	public static final int SERVICE_CONNECT = 3;
	public static boolean myServiceIsRunning = false;
	String Current_Activity = "";
	Context c;
	CommonData commonClass;
	SharedPreferences preferences;
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	private static NotificationManager mNotificationManager;
	private final static int CUSTOM_VIEW = 0x04;
	PowerManager pm;

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(android.os.Message msg) {

			switch (msg.what) {
			case MSG_REGISTER_CLIENT:
				mClients.add(msg.replyTo);
				break;

			default:
				super.handleMessage(msg);
			}
			super.handleMessage(msg);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		myServiceIsRunning = false;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		commonClass = (CommonData) getApplicationContext();
		HOST = commonClass.strServerName;
		SERVICE = commonClass.strServerName;
		HandlerThread thread = new HandlerThread("connection",
				android.os.Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();
		Looper looper = thread.getLooper();
		mHandler = new OurHandler(looper);
		commonClass.strUsername = getSharedPreferences("UserDetail",
				Context.MODE_PRIVATE).getString("username", "");
		commonClass.strPassword = getSharedPreferences("UserDetail",
				Context.MODE_PRIVATE).getString("password", "");

		myServiceIsRunning = true;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		if (intent != null) {

			c = this;

			pm = (PowerManager) c.getSystemService(Context.POWER_SERVICE);
			android.os.Message msg = mHandler.obtainMessage();
			msg.arg1 = startId;
			msg.obj = intent.getStringExtra("something");
			mHandler.sendMessage(msg);
		}
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	public class OurHandler extends Handler {

		public OurHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(android.os.Message msg) {
			super.handleMessage(msg);
			int startId = msg.arg1;
			Object someObject = msg.obj;

			boolean stopped = stopSelfResult(startId);
			// stopped is true if the service is stopped

			preferences = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());

			if (commonClass.xmppconnection != null
					&& commonClass.checkInternetConnection()) {
				commonClass.xmppconnection.disconnect();
				commonClass.xmppconnection = null;

			}

			try {
				ConnectionConfiguration connConfig = new ConnectionConfiguration(
						HOST, PORT, SERVICE);
				connConfig.setReconnectionAllowed(true);
				commonClass.xmppconnection = new XMPPConnection(connConfig);

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {

					connConfig.setTruststoreType("AndroidCAStore");
					connConfig.setTruststorePassword(null);
					connConfig.setTruststorePath(null);

				} else {

					connConfig.setTruststoreType("BKS");
					String path = System
							.getProperty("javax.net.ssl.trustStore");
					if (path == null)
						path = System.getProperty("java.home") + File.separator
								+ "etc" + File.separator + "security"
								+ File.separator + "cacerts.bks";
					connConfig.setTruststorePath(path);
				}
			} catch (Exception e) {
			}

			if (commonClass.xmppconnection != null
					&& commonClass.checkInternetConnection()) {

				try {

					Log.d("xmpp connection connect>>", "connect>>");
					commonClass.xmppconnection.connect();

					Roster roster = commonClass.xmppconnection.getRoster();
					// Presence availability = roster.getPresence("tomcook");
					// Presence.Type type = availability.getType();
					// if (type == Presence.Type.available) {
					// Log.i("tomcook avilable", "tomcook available");
					// }
					roster.addRosterListener(new RosterListener() {
						// Ignored events public void
						// entriesAdded(Collection<String> addresses) {}
						public void entriesDeleted(Collection<String> addresses) {
							Bundle b = new Bundle();
							String[] usernamearr = addresses.toString().split(
									"@");
							String username = usernamearr[0];
							b.putString("send_data", "userdeleted");
							b.putString("username", username);

							android.os.Message msgs = android.os.Message
									.obtain(null, MSG_SET_STRING_VALUE);
							msgs.setData(b);
							try {
								mClients.get(mClients.size() - 1).send(msgs);
							} catch (Exception e) {
							}
						}

						public void entriesUpdated(Collection<String> addresses) {
							System.out.println("entriesUpdated "
									+ addresses.toString());
						}

						public void presenceChanged(Presence presence) {
							System.out.println("Presence changed roster: "
									+ presence.getFrom() + " " + presence);

							String fromid = presence.getFrom();
							String[] fromidemail = fromid.split("/");
							String[] usernamearr = fromidemail[0].split("@");
							String username = usernamearr[0];

							Bundle b = new Bundle();
							b.putString("send_data", "presencechange");
							b.putString("username", username);
							b.putString("presence", "" + presence);
							android.os.Message msgs = android.os.Message
									.obtain(null, MSG_SET_STRING_VALUE);
							msgs.setData(b);
							try {
								mClients.get(mClients.size() - 1).send(msgs);
							} catch (Exception e) {
							}

						}

						@Override
						public void entriesAdded(Collection<String> arg0) {

							String tovalue = arg0.toString();
							tovalue = tovalue.replace("[", "");
							tovalue = tovalue.replace("]", "");

							// Presence p = new
							// Presence(Presence.Type.available);
							// p.setMode(Presence.Mode.available);
							//
							// String fromuser = commonClass.strUsername;
							// String fromuservalue = fromuser + "@"
							// + commonClass.strServerName;
							//
							// Log.d("entries added>>",
							// "entries added"+fromuservalue);
							//
							// p.setFrom("" + fromuservalue);
							// p.setTo(tovalue);
							// p.setProperty("sentrequest", "done");
							// commonClass.xmppconnection.sendPacket(p);

						}
					});

					commonClass.xmppconnection
							.addConnectionListener(new ConnectionListener() {

								@Override
								public void reconnectionSuccessful() {

									Thread th = new Thread(new Runnable() {

										@Override
										public void run() {

											Database_Helper db = new Database_Helper(
													c);
											db.Open();
											Cursor cursor = db
													.getOfflineMessage();

											if (cursor.moveToFirst()) {
												do {

													String msg = cursor.getString(cursor
															.getColumnIndex("msg"));

													String from_id = cursor.getString(cursor
															.getColumnIndex("from_id"));
													String to_id = cursor.getString(cursor
															.getColumnIndex("to_id"));

													String Msg_id = cursor.getString(cursor
															.getColumnIndex("Msg_id"));

													Message message = new Message(
															to_id,
															Message.Type.chat);
													message.setBody(msg);

													SimpleDateFormat sdf = new SimpleDateFormat(
															"MMMM dd yyyy HH:mm:ss aa",
															Locale.US);
													String currentDateandTime = sdf
															.format(new Date());

													supportchat chatobj = new supportchat();
													chatobj.from_id = from_id;
													chatobj.to_id = to_id;
													chatobj.message = msg;
													chatobj.Msg_id = Msg_id;
													chatobj.flag = 0;
													chatobj.isinternet = 1;
													chatobj.time = currentDateandTime;

													if (commonClass.xmppconnection != null
															&& commonClass.xmppconnection
																	.isConnected()
															&& commonClass
																	.checkInternetConnection()) {

														commonClass.xmppconnection
																.sendPacket(message);

														db.update_offlineMessage(chatobj.Msg_id);

													}

												} while (cursor.moveToNext());

											}
											db.close();
										}
									});

									th.start();

									Thread th1 = new Thread(new Runnable() {

										@Override
										public void run() {

											Database_Helper db = new Database_Helper(
													c);
											db.Open();
											Cursor cursor = db.getQueue();

											if (cursor.moveToFirst()) {
												do {

													int mid = cursor.getInt(cursor
															.getColumnIndex("mid"));

													String rejectid = cursor.getString(cursor
															.getColumnIndex("rejectid"));

													String username = cursor.getString(cursor
															.getColumnIndex("username"));

													long currenttimestamp = System
															.currentTimeMillis();
													String rpmemberid = rejectid;
													Message msg = new Message(
															rpmemberid,
															Message.Type.chat);
													msg.setBody("");
													msg.setProperty(
															"support_team_id",
															"1");
													msg.setProperty(
															"type_of_message",
															"reject_request_for_chat");

													// String customerid =
													// preferences.getString("customerid",
													// "");
													msg.setProperty("msg_id",
															username);

													if (commonClass.xmppconnection != null
															&& commonClass.xmppconnection
																	.isConnected()
															&& commonClass
																	.checkInternetConnection()) {
														commonClass.xmppconnection
																.sendPacket(msg);

														db.remove_offlineMessage(String
																.valueOf(mid));

													}

												} while (cursor.moveToNext());

											}
											db.close();
										}
									});

									th1.start();

								}

								@Override
								public void reconnectionFailed(Exception arg0) {
								}

								@Override
								public void reconnectingIn(int arg0) {

								}

								@Override
								public void connectionClosedOnError(
										Exception arg0) {

									String conflictmessage = arg0.toString()
											.trim();
									if (conflictmessage.contains("conflict")) {
										Log.d("CONFLICT>>", "CONFLICT");
									} else {
										android.os.Message msgs = android.os.Message
												.obtain(null, SERVICE_CONNECT);
										try {
											mClients.get(mClients.size() - 1)
													.send(msgs);
										} catch (Exception e) {
										}
									}
								}

								@Override
								public void connectionClosed() {

								}
							});

				} catch (Exception ex) {
					Log.d("exception>>>", "1");
					Bundle b = new Bundle();
					b.putString("send_data", ex.getMessage());
					android.os.Message msgs = android.os.Message.obtain(null,
							MSG_SET_STRING_VALUE);
					msgs.setData(b);
					try {
						mClients.get(mClients.size() - 1).send(msgs);
					} catch (Exception e) {
					}
				}
				try {
					// commonClass.xmppconnection.login(commonClass.strUsername,
					// commonClass.strPassword);
					Log.d("username >>", "" + commonClass.strUsername);
					Log.d("password>>", "" + commonClass.strUsername);
					commonClass.xmppconnection.login(commonClass.strUsername,
							commonClass.strUsername);

					// Set the status to available
					if (commonClass.xmppconnection != null
							&& commonClass.xmppconnection.isConnected()
							&& commonClass.checkInternetConnection()) {
						Presence presence = new Presence(
								Presence.Type.available);
						presence.setMode(Presence.Mode.available);
						presence.setPriority(24);
						commonClass.xmppconnection.sendPacket(presence);
					}

					PacketFilter filter = new MessageTypeFilter(
							Message.Type.chat);
					commonClass.xmppconnection.addPacketListener(
							new PacketListener() {
								@Override
								public void processPacket(Packet packet) {
									ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
									List<ActivityManager.RunningTaskInfo> taskInfo = am
											.getRunningTasks(1);

									Current_Activity = taskInfo.get(0).topActivity
											.getClassName();

									String xmlMessage = packet.toXML();
									new getreceiver(packet).execute(xmlMessage);
								}
							}, filter);
					Bundle b = new Bundle();
					b.putString("send_data", "1");
					// Toast.makeText(getApplicationContext(),
					// "connector called",
					// Toast.LENGTH_LONG).show();
					android.os.Message msgs = android.os.Message.obtain(null,
							MSG_SET_STRING_VALUE);
					msgs.setData(b);
					try {
						mClients.get(mClients.size() - 1).send(msgs);
					} catch (Exception e) {
					}
				} catch (Exception ex) {
					Log.d("exception>>", "2");

					Bundle b = new Bundle();
					b.putString("send_data", ex.getMessage());
					android.os.Message msgs = android.os.Message.obtain(null,
							MSG_SET_STRING_VALUE);
					msgs.setData(b);
					try {
						mClients.get(mClients.size() - 1).send(msgs);
					} catch (Exception e) {
					}
				}
			}
		}
	}

	public class getreceiver extends AsyncTask<String, Void, Void> {

		Packet packet;

		public getreceiver(Packet p) {
			packet = p;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
		@Override
		protected Void doInBackground(String... params) {

			String xmlMessage = params[0];

			// GET CURRENT ACTIVITY

			Message messageresponse = (Message) packet;

			ArrayList<String> arr_property = new ArrayList<String>(
					messageresponse.getPropertyNames());

			if (arr_property.size() == 0) {

				SimpleDateFormat sdf = new SimpleDateFormat(
						"MMMM dd yyyy HH:mm:ss aa", Locale.US);
				String currentDateandTime = sdf.format(new Date());

				String fromid = messageresponse.getFrom();
				String to_id = messageresponse.getTo();
				String[] arrfrm = fromid.split("/");
				String fromidval = arrfrm[0];
				String frmid = fromidval;
				String toid = messageresponse.getTo();
				String message = messageresponse.getBody();
				String messageid = messageresponse.getPacketID();
				int flag = 1;

				supportchat chatmsg = new supportchat();
				chatmsg.from_id = frmid;
				chatmsg.message = messageresponse.getBody().toString();
				chatmsg.to_id = toid;
				chatmsg.flag = 1;
				chatmsg.isinternet = 1;
				chatmsg.Msg_id = messageresponse.getPacketID();
				chatmsg.time = currentDateandTime;

				Database_Helper db = new Database_Helper(c);
				db.Open();
				db.add_Chat(chatmsg);
				db.close();

				Log.e("current activity", "" + Current_Activity);

				if (Current_Activity.contains(c.getPackageName())) {
					// Log.e("Ifffff==", "IF");

					Bundle b = new Bundle();
					b.putString("send_data", "singlechat");
					b.putSerializable("chatobj", chatmsg);

					android.os.Message msgs = android.os.Message.obtain(null,
							MSG_SET_STRING_VALUE);
					msgs.setData(b);
					try {
						mClients.get(mClients.size() - 1).send(msgs);
					} catch (Exception e) {
					}

					if (!pm.isScreenOn()) {
						String[] arrfrmid = frmid.split("@");
						String username = arrfrmid[0];
						String customername = username;

						if (commonClass.arrchats != null) {
							if (commonClass.arrchats.size() > 0) {
								for (int i = 0; i < commonClass.arrchats.size(); i++) {
									if (username
											.equalsIgnoreCase(commonClass.arrchats
													.get(i).userName)) {
										customername = commonClass.arrchats
												.get(i).customername;

									}
								}
							}
						}

						Intent intent = new Intent(c, DeelFeedActivity.class);
						intent.putExtra("notification", true);
						intent.putExtra("type", "chat");
						intent.setAction("android.intent.action.MAIN");
						intent.addCategory("android.intent.category.LAUNCHER");

						PendingIntent contentIntent = PendingIntent
								.getActivity(c, 0, intent,
										PendingIntent.FLAG_UPDATE_CURRENT);
						Notification.Builder bui = new Notification.Builder(c);
						bui.setAutoCancel(true)
								.setDefaults(Notification.DEFAULT_ALL)
								.setWhen(System.currentTimeMillis())
								.setSmallIcon(R.drawable.app_icon)
								.setTicker(customername)
								.setContentTitle("MiMCoups")
								.setContentText(customername + " : " + message)
								.setDefaults(
										Notification.DEFAULT_LIGHTS
												| Notification.DEFAULT_SOUND
												| Notification.DEFAULT_VIBRATE)
								.setContentIntent(contentIntent)
								.setContentInfo("");
						NotificationManager notificationManager = (NotificationManager) c
								.getSystemService(Context.NOTIFICATION_SERVICE);
						notificationManager.notify(0, bui.build());

					}

				}

				else {
					Log.e("Elase== Activity", Current_Activity);

					String[] arrfrmid = frmid.split("@");
					String username = arrfrmid[0];

					String customername = username;

					if (commonClass.arrchats != null) {
						if (commonClass.arrchats.size() > 0) {
							for (int i = 0; i < commonClass.arrchats.size(); i++) {
								if (username
										.equalsIgnoreCase(commonClass.arrchats
												.get(i).userName)) {
									customername = commonClass.arrchats.get(i).customername;

								}
							}
						}
					}

					if (!Current_Activity.contains("com.mimcoups")) {
						Log.e("username>>", "" + username + "========"
								+ commonClass.currentfriendstoid.length());

						if (commonClass.pendingsupportchat != null
								&& commonClass.currentfriendstoid.trim()
										.equals(username.trim())) {
							Log.e("add chat>>", "pending chat");

							commonClass.pendingsupportchat.add(chatmsg);
						}
					}

					Intent intent = new Intent(c, DeelFeedActivity.class);
					intent.putExtra("notification", true);
					intent.putExtra("type", "chat");
					intent.setAction("android.intent.action.MAIN");
					intent.addCategory("android.intent.category.LAUNCHER");

					PendingIntent contentIntent = PendingIntent.getActivity(c,
							0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
					Notification.Builder bui = new Notification.Builder(c);
					bui.setAutoCancel(true)
							.setDefaults(Notification.DEFAULT_ALL)
							.setWhen(System.currentTimeMillis())
							.setSmallIcon(R.drawable.app_icon)
							.setTicker(customername)
							.setContentTitle("MiMCoups")
							.setContentText(customername + " : " + message)
							.setDefaults(
									Notification.DEFAULT_LIGHTS
											| Notification.DEFAULT_SOUND
											| Notification.DEFAULT_VIBRATE)
							.setContentIntent(contentIntent).setContentInfo("");
					NotificationManager notificationManager = (NotificationManager) c
							.getSystemService(Context.NOTIFICATION_SERVICE);
					notificationManager.notify(0, bui.build());

					SharedPreferences preference = c.getApplicationContext()
							.getSharedPreferences("FriendList",
									Context.MODE_PRIVATE);

					int countval = preference.getInt(username, 0);
					countval = countval + 1;

					preference.edit().putInt(username, countval).commit();

					SharedPreferences badgepreference = c
							.getApplicationContext().getSharedPreferences(
									"Badgecount", Context.MODE_PRIVATE);

					int badgecountval = badgepreference.getInt("Badge", 0);
					badgecountval = badgecountval + 1;

					badgepreference.edit().putInt("Badge", badgecountval)
							.commit();
					Log.e("Badge Count===", "" + badgecountval);

				}

			}

			if (arr_property.contains("type_of_message")) {

				String type_of_message = messageresponse.getProperty(
						"type_of_message").toString();
				Log.e("Message Type=", type_of_message);
				if (type_of_message.trim().equalsIgnoreCase("accept_request_for_chat")) {

					// Log.d("accpet request", "accept request");
					
						commonClass.acceptrequest = 1;

						// Log.d("accept 1", "1");

						String rpacceptmember = messageresponse.getFrom();
						String[] rparr = rpacceptmember.split("@");
						String rpacceptmemberval = rparr[0];
						commonClass.representativeid = "";
						commonClass.representativeid = rpacceptmemberval;

						int rpstorelen = commonClass.arrRpListStore.size();
						for (int i = 0; i < rpstorelen; i++) {
							String rpmember = commonClass.arrRpListStore.get(i);
							if (!rpmember.toLowerCase().equals(
									rpacceptmemberval.toLowerCase())) {
								long currenttimestamp = System
										.currentTimeMillis();
								String rpmemberid = rpmember + "@"
										+ commonClass.strServerName;
								Message msg = new Message(rpmemberid,
										Message.Type.chat);
								msg.setBody("reject_request_for_chat");
								msg.setProperty("support_team_id", "1");
								msg.setProperty("type_of_message",
										"reject_request_for_chat");
								msg.setProperty("msg_id",
										commonClass.strUsername);

								if (commonClass.xmppconnection != null
										&& commonClass.xmppconnection
												.isConnected()
										&& commonClass
												.checkInternetConnection()) {
									commonClass.xmppconnection.sendPacket(msg);
								}
							}
						}

						Bundle b = new Bundle();
						b.putString("send_data", "merchantconnect");
						android.os.Message msgs = android.os.Message.obtain(
								null, MSG_SET_STRING_VALUE);
						msgs.setData(b);
						try {
							mClients.get(mClients.size() - 1).send(msgs);
						} catch (Exception e) {
						}

					

				}
				
				else if (type_of_message.equals("reject_request_merchant")) {

					 Log.e("Reject request", "Reject request");
					 commonClass.connectCount=commonClass.connectCount-1;
					 
					 if(commonClass.connectCount==0){
						 String rpacceptmember = messageresponse.getFrom();
							String[] rparr = rpacceptmember.split("@");
							String rpacceptmemberval = rparr[0];
							commonClass.representativeid = "";
							commonClass.representativeid = rpacceptmemberval;

							int rpstorelen = commonClass.arrRpListStore.size();
							for (int i = 0; i < rpstorelen; i++) {
								String rpmember = commonClass.arrRpListStore.get(i);
								if (!rpmember.toLowerCase().equals(
										rpacceptmemberval.toLowerCase())) {
									long currenttimestamp = System
											.currentTimeMillis();
									String rpmemberid = rpmember + "@"
											+ commonClass.strServerName;
									Message msg = new Message(rpmemberid,
											Message.Type.chat);
									msg.setBody("reject_request_for_chat");
									msg.setProperty("support_team_id", "1");
									msg.setProperty("type_of_message",
											"reject_request_merchant");
									msg.setProperty("msg_id",
											commonClass.strUsername);

									if (commonClass.xmppconnection != null
											&& commonClass.xmppconnection
													.isConnected()
											&& commonClass
													.checkInternetConnection()) {
										commonClass.xmppconnection.sendPacket(msg);
									}
								}
							}

							Bundle b = new Bundle();
							b.putString("send_data", "merchantreject");
							android.os.Message msgs = android.os.Message.obtain(
									null, MSG_SET_STRING_VALUE);
							msgs.setData(b);
							try {
								mClients.get(mClients.size() - 1).send(msgs);
							} catch (Exception e) {
							}
					 }
					 
					if (commonClass.acceptrequest == 0) {
						commonClass.acceptrequest = 1;

						// Log.d("accept 1", "1");

						String rpacceptmember = messageresponse.getFrom();
						String[] rparr = rpacceptmember.split("@");
						String rpacceptmemberval = rparr[0];
						commonClass.representativeid = "";
						commonClass.representativeid = rpacceptmemberval;

						int rpstorelen = commonClass.arrRpListStore.size();
						for (int i = 0; i < rpstorelen; i++) {
							String rpmember = commonClass.arrRpListStore.get(i);
							if (!rpmember.toLowerCase().equals(
									rpacceptmemberval.toLowerCase())) {
								long currenttimestamp = System
										.currentTimeMillis();
								String rpmemberid = rpmember + "@"
										+ commonClass.strServerName;
								Message msg = new Message(rpmemberid,
										Message.Type.chat);
								msg.setBody("reject_request_for_chat");
								msg.setProperty("support_team_id", "1");
								msg.setProperty("type_of_message",
										"reject_request_merchant");
								msg.setProperty("msg_id",
										commonClass.strUsername);

								if (commonClass.xmppconnection != null
										&& commonClass.xmppconnection
												.isConnected()
										&& commonClass
												.checkInternetConnection()) {
									commonClass.xmppconnection.sendPacket(msg);
								}
							}
						}

						Bundle b = new Bundle();
						b.putString("send_data", "merchantreject");
						android.os.Message msgs = android.os.Message.obtain(
								null, MSG_SET_STRING_VALUE);
						msgs.setData(b);
						try {
							mClients.get(mClients.size() - 1).send(msgs);
						} catch (Exception e) {
						}

					}

				}
				
				else if (type_of_message.equals("support_team_chat")) {

					SimpleDateFormat sdf = new SimpleDateFormat(
							"MMMM dd yyyy HH:mm:ss aa", Locale.US);
					String currentDateandTime = sdf.format(new Date());

					String fromid = messageresponse.getFrom();
					String to_id = messageresponse.getTo();
					String message = messageresponse.getBody();
					String messageid = messageresponse.getPacketID();
					int flag = 1;

					String[] arrfrm = fromid.split("/");
					String fromidval = arrfrm[0];
					supportchat chatobj = new supportchat();
					chatobj.from_id = fromidval;
					chatobj.to_id = to_id;
					chatobj.message = message;
					chatobj.Msg_id = messageid;
					chatobj.flag = flag;
					chatobj.time = currentDateandTime;

					commonClass.supportChatsend = chatobj;

					Bundle b = new Bundle();
					b.putString("send_data", "2");
					android.os.Message msgs = android.os.Message.obtain(null,
							MSG_SET_STRING_VALUE);
					msgs.setData(b);
					try {

						mClients.get(mClients.size() - 1).send(msgs);
					} catch (Exception e) {
					}

					Log.e("current activity	", "123" + Current_Activity + "==="
							+ pm.isScreenOn());

					if (!pm.isScreenOn()) {

						String[] from = fromid.split("@");
						String username = from[0];
						// new SupportChatNotification(CUSTOM_VIEW, username,
						// message).execute();

						Intent intent = new Intent(c, DeelFeedActivity.class);
						intent.putExtra("notification", true);
						intent.putExtra("type", "");
						intent.setAction("android.intent.action.MAIN");
						intent.addCategory("android.intent.category.LAUNCHER");

						PendingIntent contentIntent = PendingIntent
								.getActivity(c, 0, intent,
										PendingIntent.FLAG_UPDATE_CURRENT);
						Notification.Builder bui = new Notification.Builder(c);
						bui.setAutoCancel(true)
								.setDefaults(Notification.DEFAULT_ALL)
								.setWhen(System.currentTimeMillis())
								.setSmallIcon(R.drawable.app_icon)
								.setTicker(username)
								.setContentTitle("MiMCoups")
								.setContentText(username + " : " + message)
								.setDefaults(
										Notification.DEFAULT_LIGHTS
												| Notification.DEFAULT_SOUND
												| Notification.DEFAULT_VIBRATE)
								.setContentIntent(contentIntent)
								.setContentInfo("");
						NotificationManager notificationManager = (NotificationManager) c
								.getSystemService(Context.NOTIFICATION_SERVICE);
						notificationManager.notify(0, bui.build());

					}

					if (!Current_Activity.contains("com.mimcoups")) {

						String[] from = fromid.split("@");
						String username = from[0];
						//
						// new SupportChatNotification(CUSTOM_VIEW, username,
						// message).execute();

						Intent intent = new Intent(c, DeelFeedActivity.class);
						intent.putExtra("notification", true);
						intent.putExtra("type", "");
						intent.setAction("android.intent.action.MAIN");
						intent.addCategory("android.intent.category.LAUNCHER");

						PendingIntent contentIntent = PendingIntent
								.getActivity(c, 0, intent,
										PendingIntent.FLAG_UPDATE_CURRENT);
						Notification.Builder bui = new Notification.Builder(c);
						bui.setAutoCancel(true)
								.setDefaults(Notification.DEFAULT_ALL)
								.setWhen(System.currentTimeMillis())
								.setSmallIcon(R.drawable.app_icon)
								.setTicker(username)
								.setContentTitle("MiMCoups")
								.setContentText(username + " : " + message)
								.setDefaults(
										Notification.DEFAULT_LIGHTS
												| Notification.DEFAULT_SOUND
												| Notification.DEFAULT_VIBRATE)
								.setContentIntent(contentIntent)
								.setContentInfo("");
						NotificationManager notificationManager = (NotificationManager) c
								.getSystemService(Context.NOTIFICATION_SERVICE);
						notificationManager.notify(0, bui.build());

					}

				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
		}

	}

	static {
		try {
			Class.forName("org.jivesoftware.smack.ReconnectionManager");
		} catch (ClassNotFoundException ex) {
			// problem loading reconnection manager
		}
	}

}
