package com.mimcoups.Consumer;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.mimcoups.Consumer.DialogFragment.ProgressDialogFragment;
import com.mimcoups.Consumer.DialogFragment.SingleButtonAlert;
import com.mimcoups.R;
import com.mimcoups.Consumer.commonClass.CommonData;
import com.mimcoups.Consumer.customClass.DateCustomeClass;
import com.mimcoups.Consumer.customClass.WishListDetail;
import com.mimcoups.Consumer.fragment.FragmentSliderUserProfile;
import com.mimcoups.Consumer.inteface.DialogInterfaceClick;
import com.mimcoups.Consumer.inteface.topBarInteface;
import com.mimcoups.Consumer.inteface.wrongdatedialoginterface;

public class UserProfileActivity extends Activity implements topBarInteface,
		wrongdatedialoginterface {

	private DialogFragment progressDialogFragment;
	CommonData commonclass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_detail);
		commonclass = (CommonData) getApplicationContext();
		FragmentTransaction transaction = getFragmentManager()
				.beginTransaction();
		Fragment newFragment = new FragmentSliderUserProfile();
		String strFragmentTag = newFragment.toString();
		transaction.replace(R.id.frmSlider, newFragment, strFragmentTag);
		transaction.commit();
	}

	@Override
	public void topbarClick(String message) {
		// TODO Auto-generated method stub
		if (message.equals("back")) {
			onBackPressed();
		}
	}

	@Override
	public void showDialog(String message) {
		// TODO Auto-generated method stub

		progressDialogFragment = new ProgressDialogFragment(getResources()
				.getString(R.string.loading));
		progressDialogFragment.show(getFragmentManager(), "");
		progressDialogFragment.setCancelable(false);

	}

	@Override
	public void hideDialog() {
		// TODO Auto-generated method stub

		progressDialogFragment.dismiss();

	}

	@Override
	public void sendDatatoFragment(WishListDetail wishlist) {
		// TODO Auto-generated method stub

	}

	@Override
	public void selecteddateiswrongedialog(String message) {
		// TODO Auto-generated method stub
		DialogFragment ds = new SingleButtonAlert(new DialogInterfaceClick() {

			@Override
			public void dialogClick(String tag) {
				// TODO Auto-generated method stub

			}
		}, message);

		ds.show(getFragmentManager(), "");

	}

	@Override
	public void ondateselectedevent(DateCustomeClass dateclass) {
		// TODO Auto-generated method stub

	}

	// @Override
	// public void unbindservice() {
	// // TODO Auto-generated method stub
	// if (commonclass.xmppconnection != null) {
	// getApplicationContext().unbindService(commonclass.xmppconnection);
	// }
	// }

}
