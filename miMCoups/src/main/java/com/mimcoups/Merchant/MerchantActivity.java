package com.mimcoups.Merchant;

import android.support.v4.app.FragmentTransaction;

import com.mimcoups.R;

public class MerchantActivity extends BaseActivity {

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_merchant;
    }

    @Override
    protected void initView() {
        final DealsCurrentFragment dealsCurrentFragment = new DealsCurrentFragment();
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.activity_user_container,dealsCurrentFragment,dealsCurrentFragment.getClass().getSimpleName());
        ft.commit();
    }


}
