package com.mimcoups.Merchant;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import com.mimcoups.R;
import org.json.JSONException;

public abstract class BaseActivity extends AppCompatActivity {
    protected abstract int getLayoutResourceId();
    protected abstract void initView();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        initView();
    }
    public void addFragment(Fragment currentFragment, Fragment fragment, boolean isAddToBackStack) {
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide(currentFragment);
        ft.add(R.id.activity_user_container, fragment, fragment.getClass().getSimpleName());
        if (isAddToBackStack)
            ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

}
